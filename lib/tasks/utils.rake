namespace :utils do
  task :generate_insurance_account_status_report => :environment do
    current_date = ApplicationHelper.current_working_date.to_date
    Branch.all.each do |branch| 
      ::Reports::GenerateDailyReportInsuranceAccountStatuses.new(
        branch: branch,
        as_of: current_date
      ).execute!
    end
  end
 
  task :set_loan_insurance_record_status_to_inactive => :environment do
    puts "Updating loan insurance records status"
    loan_insurance_records = LoanInsuranceRecord.all
    current_date = ApplicationHelper.current_working_date.to_date
    loan_insurance_records.each do |lir|
      if lir.maturity_date == current_date || lir.maturity_date <= current_date
        lir.update(status: "inactive")
      end
    end
    puts "Done!"
  end

  task :set_is_reinstate_to_false => :environment do
    puts "Updating"
    members = Member.all
    members.each do |member|
      if member.is_reinstate.nil?
        member.update!(is_reinstate: false)
      end
    end
    puts "Done!"
  end

  task :set_legal_dependents_is_deceased_and_is_tpd_to_false => :environment do
    puts "Updating"
    legal_dependents = LegalDependent.all
    legal_dependents.each do |dep|
      if dep.is_deceased.nil?
        dep.update!(is_deceased: false)
      end

      if dep.is_tpd.nil?
        dep.update!(is_tpd: false)
      end
    end
    puts "Done!"
  end

  task :update_member_insurance_status => :environment do
    puts "Updating member insurance status"
    members = Member.all.order("members.id DESC")
    
    if ENV['CURRENT_DATE'].present?
      current_date = ENV['CURRENT_DATE'].to_date
    else
      current_date = ApplicationHelper.current_working_date.to_date
    end

    insurance_account_code = Settings.default_insurance_account_code ||= "LIF"

    insurance_accounts              = InsuranceAccount.joins(:insurance_type).where("insurance_types.code = ? AND member_id IN (?)", insurance_account_code, members.pluck(:id))
    insurance_account_transactions  = InsuranceAccountTransaction.where("amount > 0 AND insurance_account_id IN (?)", insurance_accounts.pluck(:id)).order("id ASC")

    default_periodic_payment = InsuranceType.where(code: insurance_account_code).first.default_periodic_payment

    size  = members.size

    members.each_with_index do |member, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Validating #{member.id}... #{progress}%%")

      puts "Updating #{member.id} - #{member.full_name}"
      start_date = member.previous_mii_member_since.try(:to_date)

      if start_date.blank?
        insurance_membership_type_name = Settings.insurance_membership_type_name
        member.memberships.each do |membership|
          if membership[:membership_type][:name] == insurance_membership_type_name
            if membership[:paid] == true 
              if !membership[:membership_payment][:paid_at].nil?
                start_date = membership[:membership_payment][:paid_at].try(:to_date)
              end
            end          
          end
        end
      end

      if start_date.present?
        #insurance_account = member.insurance_accounts.joins(:insurance_type).where("insurance_types.code = ?", insurance_account_code).first
        insurance_account = insurance_accounts.select{ |o| o[:member_id] == member.id }.first

        transactions  = insurance_account_transactions.select{ |o| o[:insurance_account_id] == insurance_account.id }
        
        #if insurance_account.insurance_account_transactions.count >= 1
        if transactions.size > 0
#          latest_payment           = insurance_account.insurance_account_transactions.where("insurance_account_id = ? and amount > 0", insurance_account.id).last
#          last_payment_date        = insurance_account.insurance_account_transactions.where("insurance_account_id = ? and amount > 0", insurance_account.id).last.transacted_at.to_date

          latest_payment    = transactions.last
          last_payment_date = transactions.last[:transacted_at].to_date

          current_balance          = latest_payment.ending_balance.to_i

          # Code
          num_days                 = (current_date - start_date).to_i
          num_weeks                = (num_days / 7).to_i
          insured_amount           = num_weeks * default_periodic_payment
          latest_transaction_date  = latest_payment ? latest_payment.transacted_at.to_date : start_date

          num_days_insured         = (latest_transaction_date.to_date  - start_date).to_i
          num_weeks_insured        = (num_days_insured / 7).to_i

          insured_amount           = num_weeks  * default_periodic_payment
          coverage_date            = (start_date + ((current_balance / default_periodic_payment).to_i).weeks).strftime("%B %d, %Y")
          amt_past_due             = (current_balance - insured_amount) * -1
          num_weeks_past_due       = (amt_past_due / default_periodic_payment).to_i

          days_lapsed = (current_date - last_payment_date).to_i

          
          if current_balance == 0.00 && latest_payment.for_resignation == true
            member.update(insurance_status: "resigned")
          elsif days_lapsed <= 45 && current_balance >= insured_amount
            member.update(insurance_status: "inforce")
          elsif days_lapsed > 45 && current_balance >= insured_amount
            member.update(insurance_status: "inforce")
          elsif days_lapsed <= 45 && current_balance < insured_amount && amt_past_due < 97
            member.update(insurance_status: "inforce")
          elsif days_lapsed <= 45 && current_balance < insured_amount && amt_past_due >= 97
            member.update(insurance_status: "lapsed")  
          elsif days_lapsed > 45 && current_balance < insured_amount && amt_past_due >= 97
            member.update(insurance_status: "lapsed")
          elsif days_lapsed > 45 && current_balance < insured_amount && amt_past_due < 97
            member.update(insurance_status: "inforce")  
          end
        elsif transactions.size == 0
          member.update(insurance_status: "dormant")
        end
      else
        member.update(insurance_status: "pending") 
      end

      if member.member_type == "GK"
        member.update(insurance_status: "resigned")
      elsif member.status == "resigned"
        if member.previous_mii_member_since.nil?
          member.update(insurance_status: "pending")
        else
          member.update(insurance_status: "resigned")
        end
      elsif member.status == "pending"
        member.update(insurance_status: "pending")
      elsif member.status == "archived"
        member.update(insurance_status: "dormant")
      elsif member.status == "cleared"
        member.update(insurance_status: "cleared")
      end
    end
    puts "Done!"
  end

  task :repair_maintaining_balance => :environment do
    puts "Repairing maintaining balance..."
    num_invalid = Loans::FetchInvalidMaintainingBalanceLoans.new.execute!
    puts "Repairing #{num_invalid.size} loans..."
    Loans::RepairMaintainingBalance.new.execute!
    puts "Done!"
  end

  task :update_date_approved_for_active_and_paid_loans => :environment do
    puts "Updating date approved for active and paid loans"
    loans = Loan.active_and_paid

    loans.each do |loan|
      date_approved = Voucher.where(book: loan.book, reference_number: loan.voucher_reference_number).try(:first).try(:date_posted)

      if date_approved
        puts "Updating loan with id #{loan.id} (#{loan.loan_product.to_s}) with approval date #{date_approved}"
        loan.update(date_approved: date_approved)
      else
        puts "WARNING! No accounting entry found for loan with id #{loan.id}"
        loan.update(date_approved: date_approved)
      end
    end

    puts "Done!"
  end

  task :reload_settings => :environment do
    puts "Reloading settings..."
    Settings.reload!
  end

  task :update_id_payment_records => :environment do
    PaymentCollection.membership.each do |payment_collection|
      payment_collection.payment_collection_records.each do |payment_collection_record|
        if payment_collection_record.collection_transactions.where(account_type: "ID_PAYMENT").count == 0
          if Settings.id_payment_activated == true
            collection_transaction =  CollectionTransaction.new(
                                        amount: 0.00,
                                        account_type_code: "ID_PAYMENT",
                                        account_type: "ID_PAYMENT",
                                      )

            payment_collection_record.collection_transactions << collection_transaction

            payment_collection_record.save!
          end
        end
      end
    end
  end

  task :reset_transactions => :environment do
    Voucher.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'vouchers'")

    VoucherOption.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'voucher_options'")

    JournalEntry.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'journal_entries'")

    SavingsAccountTransaction.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'savings_account_transactions'")

    SavingsAccount.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'savings_accounts'")

    InsuranceAccountTransaction.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'insurance_account_transactions'")
    
    InsuranceAccount.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'insurance_accounts'")

    EquityAccountTransaction.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'equity_account_transactions'")

    EquityAccount.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'equity_accounts'")

    AmmortizationScheduleEntry.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'ammortization_schedule_entries'")
  
    LoanPaymentAmortizationScheduleEntry.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'loan_payment_amortization_schedule_entries'")

    LoanPaymentInsuranceAccountDeposit.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'loan_payment_insurance_account_deposits'")

    LoanPayment.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'loan_payments'")

    LoanMembershipPayment.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'loan_membership_payments'")

    Loan.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'loans'")

    MemberLoanCycle.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'member_loan_cycles'")

    MembershipType.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'membership_types'")

    MembershipPayment.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'membership_payments'")

    MembershipAccountPayment.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'membership_account_payments'")

    PaymentCollection.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'payment_collections'")

    PaymentCollectionRecord.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'payment_collection_records'")

    CollectionTransaction.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'collection_transactions'")

    LegalDependent.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name='legal_dependents'")

    Beneficiary.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name='beneficiaries'")

    # Clear setting
    Setting.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name='settings'")

    # Recreate accounts
    Member.all.each do |member|
      SavingsType.all.each do |savings_type|
        SavingsAccount.create!(savings_type: savings_type, member: member, account_number: "#{SecureRandom.hex(4)}")
      end
      InsuranceType.all.each do |insurance_type|
        InsuranceAccount.create!(insurance_type: insurance_type, member: member, account_number: "#{SecureRandom.hex(4)}")
      end
      EquityType.all.each do |equity_type|
        EquityAccount.create!(equity_type: equity_type, member: member, account_number: "#{SecureRandom.hex(4)}")
      end
    end

    # Reset members to PENDING
    Member.all.each do |member|
      member.update!(status: "pending")
    end
    
    #MembershipType.create!(name: "K-KOOP", fee: 20.00, dr_accounting_entry: AccountingCode.find(7), cr_accounting_entry: AccountingCode.find(7))
    #MembershipType.create!(name: "K-MBA", fee: 30.00, dr_accounting_entry: AccountingCode.find(7), cr_accounting_entry: AccountingCode.find(91))
  end

  task :reset_members => :environment do
    Member.destroy_all
    ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'members'")
  end

  task :cluster_ping => :environment do
    ClusterOperation.cluster_ping!
  end

  task :update_loan_info => :environment do
    Loan.all.each do |loan|
      loan.update!(member_full_name: " ")
    end
  end

  task :update_loan_uuid => :environment do
    Loan.all.each do |loan|
      if loan.uuid.nil?
        puts "Updating uuid for loan: #{loan.pn_number}"
        loan.update!(uuid: SecureRandom.uuid)
      end
    end
  end

  task :update_loan_maturity_dates => :environment do
    Loan.active.each do |loan|
      puts "Updating maturity date for loan #{loan.uuid}"
      if loan.update(maturity_date: loan.ammortization_schedule_entries.last.due_at)
        puts "Maturity Date: #{loan.maturity_date.strftime('%b %d, %Y')}"
      else
        puts "Error for loan #{loan.uuid}"
      end
    end
  end

  task :create_member_accounts => :environment do
    Member.all.each do |member|
      SavingsType.all.each do |savings_type|
        if SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id).count == 0
          SavingsAccount.create!(savings_type: savings_type, member: member, account_number: "#{SecureRandom.hex(4)}")
        end
      end
      InsuranceType.all.each do |insurance_type|
        if InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).count == 0
          InsuranceAccount.create!(insurance_type: insurance_type, member: member, account_number: "#{SecureRandom.hex(4)}")
        end
      end
      EquityType.all.each do |equity_type|
        if EquityAccount.where(member_id: member.id, equity_type_id: equity_type.id).count == 0
          EquityAccount.create!(equity_type: equity_type, member: member, account_number: "#{SecureRandom.hex(4)}")
        end
      end
    end
  end

  task :insert_equity_value_to_insurance_account_transaction => :environment do
    
    branches = Branch.all  
    insurance_type = InsuranceType.where("name = ? AND code = ?", "Life Insurance Fund", "LIF").ids
    insurance_accounts    = InsuranceAccount.where("insurance_type_id = ? AND status = ? AND branch_id IN (?)", insurance_type, "active", branches.ids).ids.uniq
    insurance_account_transactions  = InsuranceAccountTransaction.where("amount > 0 AND insurance_account_id IN (?)", insurance_accounts)

    insurance_account_transactions.each do |at|
      puts "Inserting #{at.id}"
        ev_amount = at.ending_balance.to_f / 2 
        at.equity_value = ev_amount
        at.save!       
    end

    insurance_accounts_for_rehash  = InsuranceAccount.where(id: insurance_accounts)
    insurance_accounts_for_rehash.each do |account|
      puts "Rehashing #{account.id}"
      ::Insurance::RehashAccount.new(insurance_account: account).execute!
    end

    puts "Done!"
  end

  task :insert_equity_value_to_insurance_account => :environment do 
    branches = Branch.all  
    insurance_type = InsuranceType.where("name = ? AND code = ?", "Life Insurance Fund", "LIF").ids
    insurance_accounts = InsuranceAccount.where("insurance_type_id = ? AND status = ? AND branch_id IN (?)", insurance_type, "active", branches.ids)

    insurance_accounts.each do |ia|
      puts "Inserting #{ia.id}"
      last_transaction = InsuranceAccountTransaction.where("insurance_account_id IN (?) AND status = ?", ia, "approved").order("id ASC").last   
        
      if !last_transaction.nil?  
        latest_ev_amount = last_transaction.equity_value 
        
        ia.equity_value = latest_ev_amount 
        ia.save!
      end
    end 

    puts "Done!"
  end

  task :bulk_rehash => :environment do
    branch  = Branch.find(ENV['BRANCH_ID'])

    ::Insurance::BulkRehash.new(
      config: {
        branch: branch
      }
    ).execute!

    puts "Done for #{branch.id}"
  end

  task :insert_equity_value_to_life_last_transaction => :environment do
    puts "Inserting ..."

    branches = Branch.all  
    insurance_type = InsuranceType.where("name = ? AND code = ?", "Life Insurance Fund", "LIF").ids
    insurance_accounts = InsuranceAccount.where("insurance_type_id = ? AND status = ? AND branch_id IN (?)", insurance_type, "active", branches.ids)

    insurance_accounts.each do |ia|
      puts "Inserting #{ia.id}"
      last_transaction = InsuranceAccountTransaction.where("insurance_account_id IN (?) AND status = ?", ia, "approved").order("id ASC").last   
        
      if !last_transaction.nil?  
        ev_amount = last_transaction.ending_balance 
        
        last_transaction.update!(equity_value: ev_amount)
      end
    end 

    puts "Done!"
  end
end


