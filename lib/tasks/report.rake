namespace :report do
task :project_type => :environment do
  branch = Branch.first.name
  CSV.open("#{Rails.root}/tmp/#{branch}_project_type.csv","w",:write_headers=> true, :headers =>["MEMBER FULL NAME","CENTER","PROJECT TYPE","STREET","BARANGAY","CITY"]) do |csv|
  loans= Loan.active
   loans.each do |ln|
      @member= Loan.find(ln.id).member_full_name
      @member_id= Loan.find(ln.id).member_id
      #@home_no= Member.find(@member_id).home_number
      @center_id= Member.find(@member_id).center_id
      @center_name= Center.find(@center_id).name
      @address_street= Member.find(@member_id).address_street
      @address_brgy= Member.find(@member_id).address_barangay
      @address_city= Member.find(@member_id).address_city
      project_type=  Loan.find(ln.id).project_type_id
      if project_type != nil
      @pt_id= ProjectType.find(project_type).name
      end
      csv << [@member,@center_name,@pt_id,@address_street,@address_brgy,@address_city]
   
    end 
  end
end


task :mba_number => :environment do
    member = Member.pure_active
    insurance_status = member.pluck(:insurance_status).uniq
    insurance_count = []
    insurance_status.each do |is|
      data = {}
      data[:list] = []
      member_insurance_details = member.where("insurance_status = ?", is)
      member_insurance_count =  member_insurance_details.count
    
      member_insurance_type = member_insurance_details.pluck(:member_type).uniq
      member_insurance_type.each do |mitype|
        x =  Member.pure_active.where("insurance_status = ? and member_type = ?", is,mitype).count
        j = "#{mitype} | #{x}"
        data[:list] << j
      end
      
      data = "#{is} | #{member_insurance_count} | #{data[:list]}"

      insurance_count << data
    end
    puts insurance_count
  end

  task :equity_data => :environment do
    CSV.open("#{Rails.root}/tmp/equity_data.csv", "w",:write_headers=> true, :headers => ["ID NUMBER" , "NAME" , "DATE OF MEMBERSHIP" , "CENTER" , "EQUITY"] ) do |csv|
      mem = Member.joins(:membership_payments, :equity_accounts , :center).where("equity_accounts.balance=400").order(:previous_mfi_member_since).pluck(:identification_number , "CONCAT_WS(', ',members.last_name,members.first_name)" , :previous_mfi_member_since , 'centers.name' , 'equity_accounts.balance').uniq
      mem.each do |mems|
        csv << mems
      end
      puts "done"
    end
  end
  task :loan_form_data => :environment do 
    s_date = ENV['START_DATE']
    e_date = ENV['END_DATE']

    puts "NAME|LOAN PRODUCT|DATE RELEASED|CENTER|MOBILE NUMBER|URI NG NEGOSYO|PRODUKTO / SERBISYO|LOAN CYCLE|HALAGA NG HINIHIRAM|LOAN TERM|TERM"
    x = Loan.joins(:member , :center).where("date_approved >= ? and date_approved <= ?" , s_date , e_date).order(:date_approved).uniq
    x.each do |y|
      lproduct = LoanProduct.find(y.loan_product_id).name
      mem_name = y.member.full_name
      center = Center.find(y.member.center_id).name
      mobile = y.member.mobile_number
      if y.project_type_id == nil
        lprod = "NULL"
      else
        lprod = ProjectType.find(y.project_type_id).name
      end

      if y.project_type_category_id == nil
        lprodcat = "NULL"
      else
        lprodcat = ProjectTypeCategory.find(y.project_type_category_id).name
      end

      puts "#{mem_name}|#{lproduct}|#{y.date_approved}|#{center}|#{mobile}|#{lprodcat}|#{lprod}|#{y.loan_cycle_count}|#{y.amount}|#{y.num_installments}|#{y.term}"
    end
  end
  
  task :membership_form_data => :environment do
    puts "|ID NO|PANGALAN|DATE OF MEMBERSHIP|CENTER|TIRAHAN|URI NG PANINIRAHAN|TAGAL NA SiA TIRAHAN|IPINAKITANG KATIBAYAN|KAPANGANAKAN|EDAD|LUGAR NG KAPANGANAKAN|KASARIAN|KATAYUANG SIBIL|RELIHIYON|BILANG NG ANAK|ELEMENTARY|HIGHSCHOOL|COLLEGE/VOCATIONAL|CELLPHONE NUMBER|TELEPHONE NUMBER|BANKO|KLASE NG ACCOUNT|SSS #|PAG-IBIG #|PHILHEALTH #|TIN #|ASAWA|KAPANGANAKAN|EDAD|TRABAHO"
    x = Member.where("previous_mfi_member_since >= '2017-01-01' and status != 'pending'")
    x.each_with_index do |y, i|
      cent = Center.find(y.center_id).name
      tagal_yrs = y.num_years_housing
      tagal_mnts = y.num_months_housing
      if tagal_yrs == nil
        tagal_yrs = 0
      end
      if tagal_mnts == nil
        tagal_mnts = 0
      end
      tagal = (tagal_yrs).to_s + " years" + "," + (tagal_mnts).to_s + " months"
      puts "#{i + 1}|#{y.identification_number}|#{y.full_name}|#{y.previous_mfi_member_since}|#{cent}|#{y.full_address}|#{y.type_of_housing}|#{tagal}|#{y.proof_of_housing}|#{y.date_of_birth}|#{y.age}|#{y.place_of_birth}|#{y.gender}|#{y.civil_status}|#{y.religion}|#{y.num_children}|#{y.num_children_elementary}|#{y.num_children_high_school}|#{y.num_children_college}|#{y.mobile_number}|#{y.home_number}|#{y.bank}|#{y.bank_account_type}|#{y.sss_number_formatted}|#{y.pag_ibig_number_formatted}|#{y.phil_health_number_formatted}|#{y.tin_number}|#{y.spouse_full_name}|#{y.spouse_date_of_birth}|#{y.spouse_age}|#{y.spouse_occupation}"

      y.beneficiaries.each do |z|
        if z.is_primary == true
          ben = "PRIMARY BENEFICIARY"
        else
          ben = "SECONDARY BENEFICIARY"
        end
        ben_neym = z.last_name + " , " + z.first_name + " " + z.middle_name
        puts "||#{ben}|#{ben_neym}|#{z.date_of_birth}|#{z.relationship}"
      end

      y.legal_dependents.each do |xy|
        ld_neym = xy.last_name + " , " + xy.first_name + " " + xy.middle_name

        puts "||DEPENDENT|#{ld_neym}|#{xy.date_of_birth}|#{xy.age}|#{xy.educational_attainment}|#{xy.course}"
      end

    end
  end
  task :share_book => :environment do
    x = Member.where("status = 'active'").each do |ms|
      serial_number = MemberShare.where(member_id: ms.id).pluck(:certificate_number).to_s
      no_of_share = 1
      value_of_share = 100
      puts "#{ms.previous_mfi_member_since} | #{ms.full_name} | #{ms.full_address} | #{serial_number} | #{no_of_share} | #{value_of_share}"
    end
  end
  task :project_type => :environment do
    ProjectType.all.each do |pt|
      puts "#{pt.id}|#{pt.name}"
    end
  end
  task :new_and_balik_kasapi => :environment do
    mp = MembershipPayment.joins(:member).where("membership_type_id = ?  and extract(year from paid_at) = ? ",1, "2018").pluck(:member_id).uniq
    mp.each do |m|
      if MembershipPayment.where(member_id: m, membership_type_id: 1).count > 1
        puts m
      end
    end

    #mem = Member.where("status = ? and extract(year from previous_mfi_member_since) = ? ", "active", "2018")
    #mem =  Member.where("date_resigned >= ? and date_resigned <= ? and status = ?", "2018-01-01", "2018-12-31","resigned" )
    #mp = MembershipPayment.where("member_id IN (?)  and membership_type_id = ?  and extract(year from paid_at) = ? ", mem.ids,1, "2018").pluck(:member_id).uniq
    #mem.each do |m|
      #puts "#{m.id} | #{m.previous_mfi_member_since} | #{m.date_resigned}"
    #end
      #puts mem.count


  end

  task :repair_membership_payment => :environment do
    mem = Member.where(status: "active")
    mem.each do |m|
      mp = MembershipPayment.where("member_id = ?  and membership_type_id = ? ", m.id,1)
      if mp.count > 0
        puts "#{m.id} | #{mp.order("paid_at ASC").last.paid_at}"
      else
        MembershipPayment.create!(membership_type_id: 1, member_id: m.id, paid_at: m.previous_mfi_member_since, amount_paid: 20, status: "paid")
        puts "#{m.id}" 
      end
    end
  end

  task :age_bracket_details => :environment do
    month           = ENV['INPUT_MONTH'].to_i
    year            = ENV['INPUT_YEAR'].to_i
    threshold_date  = Date.civil(year, month, -1)
    members         = Member.active
    member_ids      = []
    total           = 0
    Settings.cda_age_brackets.each do |b|
      min_age       = b.first
      max_age       = b.last
      member_ages = []

      counter = 0
      ActiveRecord::Base.connection.execute("SELECT id, status, AGE('#{threshold_date.to_s}'::DATE, date_of_birth) FROM members WHERE previous_mfi_member_since < '#{threshold_date}'").each do |tuple| 
        #if ["active", "for-resignation", "resign", "resigned"].include?(tuple["status"])
        if ["active"].include?(tuple["status"])
          if tuple["age"].to_i >= min_age and tuple["age"].to_i <= max_age
            counter = counter + 1
            #member_ages << { id: tuple["id"], age: tuple["age"].to_i } 
            member_ids << tuple["id"]
          end
        end
      end

      puts "Member Count for ages #{min_age} to #{max_age}: #{counter} as of #{threshold_date}"
      total += counter
    end

    #invalid_members = Member.where(status: ["active"]).where.not(id: member_ids)
    invalid_members = Member.find(member_ids)
    
    if invalid_members.size > 0
      puts "INVALID MEMBERS"
      puts "counter | status | member name | date_of_birth | Address | membership_date | center | Project Type | Loan"
      invalid_members.each_with_index do |im, i|
        loanDetailsName = []
        projType = Loan.where(member_id: im.id).pluck(:project_type_category_id).uniq
        activeLoan = Loan.where(member_id: im.id, status: "active")
        loanSize = activeLoan.size
        if loanSize > 0 
          activeLoan.pluck(:loan_product_id).uniq.each do |al|
             j = LoanProduct.find(al).name
             
             loanDetailsName << j
          end
        end
        
        projType.each do |a|
          if a
            jef =  ProjectTypeCategory.find(a).name
          end
            
            puts "#{i + 1} | #{im.status} | #{im.full_name} | #{im.date_of_birth}| #{im.full_address} | #{im.previous_mfi_member_since} | #{im.center.name} |  #{jef} | #{loanDetailsName }"
        end
      end
    end

    puts "Total Members: #{total}"
    puts "Total Invalid: #{invalid_members.size}"
    puts "Total Active: #{members.count}"
  end

task :generate_icpr_savings_deduction => :environment do
  iosccr_id = ENV['IOSCCR']
  iosccr_details = InterestOnShareCapitalCollectionRecord.where(interest_on_share_capital_collection_id: iosccr_id)
  @data = []
  puts "member_name|current_balance | deduct_amount"
  iosccr_details.each do |iosccr|
    savings_account_id = SavingsAccount.where(member_id: iosccr.data["member_id"] , savings_type_id: 1)
    cbu_account_id = SavingsAccount.where(member_id: iosccr.data["member_id"] , savings_type_id: 3)
    if iosccr.data["first_loop"] > 0
      savings_account_id.each do |sai|
      
        if sai.balance < iosccr.data["member_savings_amount_distribute"]
          compTotal = (sai.balance).to_f - (iosccr.data["member_savings_amount_distribute"]).to_f
          j ="#{ sai.member.full_name} | #{sai.balance} | #{iosccr.data["member_savings_amount_distribute"]} | #{compTotal.round(2)} "
          @data  << j
        end
      end
    end
  end

  puts @data
end

task :generate_icpr_cbu_deduction => :environment do
  iosccr_id = ENV['IOSCCR']
  iosccr_details = InterestOnShareCapitalCollectionRecord.where(interest_on_share_capital_collection_id: iosccr_id)
  @data = []
  puts "member_name|current_balance | deduct_amount"
  iosccr_details.each do |iosccr|
    savings_account_id = SavingsAccount.where(member_id: iosccr.data["member_id"] , savings_type_id: 1)
    cbu_account_id = SavingsAccount.where(member_id: iosccr.data["member_id"] , savings_type_id: 3)
    if iosccr.data["first_loop"] > 0
      cbu_account_id.each do |sai|
      
        if sai.balance < iosccr.data["member_cbu_amount_distribute"]
          j ="#{ sai.member.full_name} | #{sai.balance} | #{iosccr.data["member_cbu_amount_distribute"]}"
          @data  << j
        end
      end
    end
  end

  puts @data
end

##### LIST OF ADDITIONAL SHARE CAPITAL #####

task :generate_list_for_additional_share_cap => :environment do 
  require 'csv'
  s_date= ENV['start_date']
  e_date= ENV['end_date']
  CSV.open("#{Rails.root}/tmp/#{s_date}_to_#{e_date}_list_of_additioanl_shares.csv", "w",:write_headers=> true, :headers => ["IDENTIFICATION_NUMBER","NAMES", "MEMBERSHIP DATE", "STATUS"] ) do |csv|
      mem= Member.joins(:membership_payments, :equity_accounts).where('members.status= ? and membership_payments.membership_type_id=? and membership_payments.paid_at >= ? 
      and membership_payments.paid_at <= ? and equity_accounts.balance= ? and equity_accounts.status= ?','active', '1',s_date,e_date,'100','active').order('members.last_name ASC').pluck(:identification_number,"CONCAT_WS(', ',members.last_name,members.first_name)", 'membership_payments.paid_at', 'members.status')
    mem.each do |mems|
        csv << mems
    end
    puts "done"
  end
end
  task :member_year_payment => :environment do
    year = ENV['YEAR']
    member_count = MembershipPayment.joins(:member).where("extract(year from membership_payments.paid_at ) = ? and members.status =? and membership_payments.status = ? and membership_payments.membership_type_id = ?",year,"active","paid",1).count
    puts member_count

  end

  task :member_ages_report => :environment do
    year = ENV['YEAR']

    member = member = member = Member.where("extract(year from date_of_birth) <= ? OR extract(year from date_of_birth) >= ?", "1942",year)
    member_list = []
    puts "Name | Date of Birth"
    member.each do |ml|
      b ="#{ml.full_name} | #{ml.date_of_birth} "
      member_list << b
    end
    puts member_list
  
  end


  task :member_loans_for_land_bank => :environment do 
    #loan_id = ENV['LOAN_ID']
    member = Loan.joins(:member).where("loans.num_installments = ? and loans.date_of_release >= ? and loans.date_of_release <= ? and maturity_date >= ?", 25, "2018-10-01","2018-10-31","2019-04-01").order("members.last_name")
    #member = Loan.joins(:member).where("loans.loan_product_id = ? and loans.date_of_release >= ? and loans.date_of_release <= ?", 14, "2018-07-01","2018-08-31").order("members.last_name")
    member_details = []
   # puts  "Last Name | First Name | MI | ADDRESS | DATE OF NOTE | DUE DATE | LOAN FACE AMOUNT | O/S BALANCE AS_OF AUGUST 31, 2018 | LOAN VALUE(85% OF 0/S BALANCE) | PURPOSE | ASSET SIZE   "
     puts  "Last Name | First Name | MI | ADDRESS | DATE OF NOTE | DUE DATE | ASSET SIZE | LOAN FACE AMOUNT | O/S BALANCE AS_OF AUGUST 31, 2018 | LOAN VALUE(85% OF 0/S BALANCE) | PURPOSE"
    member.each do |m|
      a = " #{m.try(:member).try(:last_name)} | #{m.try(:member).try(:first_name)} | #{m.try(:member).try(:middle_name).chr} | #{m.try(:member).try(:full_address)} | #{m.date_approved} | #{m.maturity_date} | | #{m.amount} | #{m.principal_balance} | #{((m.principal_balance) * 0.85).round(2)} | #{LoanProduct.find(m.loan_product_id)}"
      member_details << a
    end
    puts member_details
  end

  task :member_loans => :environment do
    s_date = ENV['START_DATE']
    e_date = ENV['END_DATE']


    #member = Loan.joins(:member).where("loans.status = 'active' and loans.loan_product_id = ?",34).order("members.last_name")
    member = Loan.joins(:member).where("loans.status = 'active' and loans.date_of_release >= ? and loans.date_of_release <= ? and amount >= 5000 and loans.loan_product_id != ?", s_date , e_date , 14)
    #member = Loan.joins(:member).where(" loans.loan_product_id = ? and loans.status = ?", 14, "active").order("members.last_name")
    member_details = []
    puts "Last Name| First Name | Middle Name | Barangay | Municipality | Province | Gender | PN no. | Loan Amount | Realease Date | Term | Maturity | Mode of Payment | Type of Projects | Loan Type | Cycle Count"
    member.each do |m|
      if m.num_installments == 15
        term_mos = 3
      elsif m.num_installments == 25
        term_mos = 6
      elsif m.num_installments = 50
        term_mos = 12
      end
      l = LoanProduct.find(m.loan_product_id).name
      pCateg = Loan.where(member_id: m.member_id , loan_product_id: 14).last
      if pCateg == nil
        x = ""
      else
        stat = pCateg.status
        y = pCateg.project_type_id
        if y.present?
          z = ProjectType.find(y).project_type_category_id
          x = ProjectTypeCategory.find(z).name
        else
          x = ""
        end
      end
      #a = "#{m.try(:member).try(:full_name)} | #{m.try(:member).try(:full_address)} | #{m.try(:member).try(:gender)} | #{m.pn_number} | #{m.amount} | #{m.date_of_release} | #{m.num_installments} | #{m.maturity_date} | #{m.term} | #{m.try(:project_type).try(:name)} | #{m.try(:loan_product).try(:name)} | #{m.loan_cyclei_count} "
      
      #a = "#{m.try(:member).last_name} |#{m.try(:member).first_name} |#{m.try(:member).middle_name} | #{m.try(:member).address_barangay} |#{m.try(:member).address_city} || #{m.try(:member).try(:gender)} | #{m.pn_number} | #{m.amount} | #{m.date_of_release} | #{term_mos} | #{m.maturity_date} | #{m.term} | |#{m.try(:project_type).try(:name)}"
      
      a = "#{m.try(:member).first_name}|#{m.try(:member).middle_name}|#{m.try(:member).last_name}| #{m.try(:member).try(:gender)} | #{m.try(:member).try(:date_of_birth).strftime("%m/%d/%Y")}||#{x}|#{m.try(:member).address_city}||#{m.amount} | #{m.num_installments} | #{m.interest_rate} | #{m.date_approved}|#{l}|#{stat}"
 
      member_details << a
    end
      puts member_details


  end  


  task :invalid_for_cic => :environment do
      member_status = ["active","resigned","resign"]
      member_details = Member.where("status IN (?)", member_status).order(:last_name)
      member_details_for_cic = []
      puts "ID | Member Name | SSS number | TIN number | cp# | Status"
      member_details.each do |md|
        sss_number = md.sss_number.split("-").join("").to_s
        tin_number = md.tin_number.split("-").join("").to_s
        mobile_number = md.mobile_number.split("-").join("").to_s
        sss_number.length == 10 ? sNumber = sss_number : sNumber = "Invalid SSS Number"
        mobile_number.length == 11 ? mNumber = md.mobile_number : mNumber = "Invalid Number"
        tin_number.length >= 9 && tin_number.length <= 12 ? tNumber = tin_number : tNumber = "Invalid Tin Number"
        
        sNumber == "Invalid SSS Number" || mNumber == "Invalid Number" || tNumber == "Invalid Tin Number" ? stat_det = "Invalid" : stat_det = "Valid"

        m = "#{md.id} | #{md.full_name} | #{sNumber},#{sss_number} | #{tNumber},#{tin_number} | #{mNumber},#{mobile_number} | #{stat_det} "  
        
        member_details_for_cic << m
      end

      puts member_details_for_cic
  
  end


  task :list_of_sub_borrowers => :environment do
    member_status = ["active"]
    member_loan_details = Loan.joins(:member).where("loans.status IN (?) and date_approved >= ? and date_approved <= ?", member_status,"2018-06-01", "2018-06-30")
    member_loan = []
    puts "Name | Address | Gender | PN no | Loan Amount | Realease Date | Term | Maturity | Mode of Payment | Type of project |"
    member_loan_details.each do |mld|
      
      if mld.project_type_id
        project_type_category = ProjectTypeCategory.find(mld.project_type_category_id).name
      else
        project_type_category = "-"
      end

      m = "#{mld.try(:member).try(:full_name)} | #{mld.try(:member).try(:full_address)} | #{mld.try(:member).try(:gender)} | #{mld.pn_number} |  #{mld.amount} | #{mld.date_approved} | #{mld.num_installments} | #{mld.maturity_date} | #{mld.term} | #{project_type_category} "

      member_loan << m
    
    end
    puts member_loan

  end


  task :cda_member_count_savings_types => :environment do
    data  = ::Special::Reports::CdaMemberCountSavingsTypes.new(
              year: ENV['INPUT_YEAR'].to_i, 
              month: ENV['INPUT_MONTH'].to_i
            ).execute!

    ap data
  end

  task :cda_member_count_age_brackets => :environment do
    month           = ENV['INPUT_MONTH'].to_i
    year            = ENV['INPUT_YEAR'].to_i
    threshold_date  = Date.civil(year, month, -1)
    members         = Member.active
    member_ids      = []
    total           = 0
    Settings.cda_age_brackets.each do |b|
      min_age       = b.first
      max_age       = b.last
      member_ages = []

      counter = 0
      ActiveRecord::Base.connection.execute("SELECT id, status, AGE('#{threshold_date.to_s}'::DATE, date_of_birth) FROM members WHERE previous_mfi_member_since < '#{threshold_date}' and gender = 'male'").each do |tuple| 
        #if ["active", "for-resignation", "resign", "resigned"].include?(tuple["status"])
        if ["active"].include?(tuple["status"])
          if tuple["age"].to_i >= min_age and tuple["age"].to_i <= max_age
            counter = counter + 1
            #member_ages << { id: tuple["id"], age: tuple["age"].to_i } 
            member_ids << tuple["id"]
          end
        end
      end

      puts "Member Count for ages #{min_age} to #{max_age}: #{counter} as of #{threshold_date}"
      total += counter
    end

    invalid_members = Member.where(status: ["active"]).where.not(id: member_ids)

    if invalid_members.size > 0
      puts "INVALID MEMBERS"
      puts "counter,id,status,last_name,first_name,middle_name,date_of_birth,gender,age"
      invalid_members.order("last_name ASC").each_with_index do |im, i|
        #puts "#{i + 1},#{im.id},#{im.status},#{im.last_name},#{im.first_name},#{im.middle_name},#{im.date_of_birth},#{im.gender},#{im.age}"
        puts "#{i+ 1}|#{im.last_name},#{im.first_name} , #{im.middle_name}|#{im.center}|#{im.date_of_birth}|#{im.age}|#{im.gender}"
      end
    end

    puts "Total Members: #{total}"
    puts "Total Invalid: #{invalid_members.size}"
    puts "Total Active: #{members.count}"
  end
end
