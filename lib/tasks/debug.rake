namespace :debug do
  desc "Get invalid Loans without proper loan_product_type_id"
  task :check_invalid_loan_product_type_in_loans => :environment do
    valid_loan_product_type_ids = LoanProductType.pluck(:id)
    current_loan_product_type_ids = Loan.pluck(:loan_product_type_id).uniq

    invalid_loan_product_type_ids = current_loan_product_type_ids - valid_loan_product_type_ids

    invalid_loans = Loan.where(loan_product_type_id: invalid_loan_product_type_ids)

    if invalid_loans.count > 0
      puts "WARNING! Invalid loans detected"
      invalid_loans.each do |invalid_loan|
        puts "Loan ID: #{invalid_loan.id} | Status: #{invalid_loan.status} | Invalid Loan Product Type ID: #{invalid_loan.loan_product_type_id} | Member: #{invalid_loan.member.to_s}"
      end
    else
      puts "No loans with invalid loan_product_type_id"
    end
  end
end
