namespace :special do
  task :load_loans_csv => :environment do
    csv_file  = ENV['CSV_FILE']

    i = 0
    CSV.foreach(csv_file, :headers => true) do |row|
      #branch        = Branch.where(name: "BRANCH-#{row[0]}").first
      branch        = Branch.where(id: row[0]).first
      if !branch
        branch  = Branch.new(
                    name: "BRANCH-#{row[0]}", 
                    code: "#{"#{SecureRandom.hex}"}", 
                    cluster: Cluster.first, 
                    abbreviation: "BRANCH-#{row[0]}", 
                    bank: Bank.first, 
                    address: "#{SecureRandom.hex}"
                  )

        branch.save!
      end

      loan_product  = LoanProduct.find(row[1])

      center_name   = row[3]
      center        = Center.where(name: center_name).first
      if !center
        center  = Center.new(
                    name: row[3], 
                    code: row[3], 
                    branch: branch, 
                    meeting_day: "Monday", 
                    officer_in_charge: "System"
                  )
        if !center.save
          center = branch.centers.first
        end
      end

      last_name             = row[5]
      first_name            = row[6]
      middle_name           = row[7]
      date_released         = Date.strptime(row[8], "%m/%d/%Y")
      pn_number             = "#{SecureRandom.hex}"
      original_loan_amount  = row[10].to_f
      principal_paid        = row[11].to_f
      principal_balance     = row[12].to_f
      original_interest     = row[13].to_f
      interest_paid         = row[14].to_f
      interest_balance      = row[15].to_f
      total_paid            = row[16].to_f
      term                  = row[20].to_i
      mode_of_payment       = row[21].to_s.downcase
      identification_number = "#{SecureRandom.hex}"

      member  = Member.where(
                  first_name:   first_name,
                  middle_name:  middle_name,
                  last_name:    last_name
                ).first

      if !member
        # Defaults
        address_street    = "#{SecureRandom.hex}"
        address_barangay  = "#{SecureRandom.hex}"
        address_city      = "#{SecureRandom.hex}"
        civil_status      = "#{SecureRandom.hex}"
        date_of_birth     = Date.today - 20.years
        member_type       = "Regular"
        status            = "active"
        gender            = "Female"

        member  = Member.new(
                    first_name: first_name,
                    middle_name: middle_name,
                    last_name: last_name,
                    branch: branch,
                    center: center,
                    status: status,
                    date_of_birth: date_of_birth,
                    address_street: address_street,
                    address_barangay: address_barangay,
                    address_city: address_city,
                    civil_status: "single",
                    member_type: member_type,
                    gender: gender,
                    identification_number: "#{SecureRandom.hex}"
                  )

        if !member.save
          raise member.errors.messages.inspect
        else
          ::Members::GenerateMissingAccounts.new(member: member).execute!
        end
      end

      member.savings_accounts.each do |savings_account|
      end

      # Create loans
      loan  = Loan.new(
                bank: branch.bank,
                voucher_date_requested: Date.today,
                branch: branch,
                center: center,
                member: member,
                pn_number: "#{SecureRandom.hex}",
                book: "JVB",
                loan_cycle_count: 1,
                loan_product: loan_product,
                loan_product_type: loan_product.loan_product_types.first,
                #interest_rate: 0.6,
                term: mode_of_payment,
                original_loan_amount: original_loan_amount,
                original_num_installments: term,
                old_principal_balance: principal_balance,
                old_interest_balance: interest_balance,
                installment_override: 1,
                override_installment_interval: true,
                payment_type: "cash",
                project_type_category: ProjectTypeCategory.first,
                project_type: ProjectType.first,
                voucher_particular: "Loans",
                status: 'active',
                first_date_of_payment: Date.today,
                amount: (principal_balance + interest_balance),
                remaining_balance: (principal_balance + interest_balance)
              )

      if !loan.save
        puts "Error in saving loan #{i+1}"
      else
        #puts "Successfully saved loan #{i+1}"
      end

      i += 1
    end
  end

  task :load_savings_csv => :environment do
  end
end
