namespace :sync do
  task :setup => :environment do
    puts "Master Server: #{Settings.master_server}"
    puts "API Key: #{Settings.api_key}"

    Sync::SyncMajorGroups.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/finance/accounting/major_groups").execute!
    puts "Done syncing major groups (#{MajorGroup.count})"

    Sync::SyncMajorAccounts.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/finance/accounting/major_accounts").execute!
    puts "Done syncing major accounts (#{MajorAccount.count})"

    Sync::SyncMotherAccountingCodes.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/finance/accounting/mother_accounting_codes").execute!
    puts "Done syncing mother accounting codes (#{MotherAccountingCode.count})"

    Sync::SyncAccountingCodeCategories.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/finance/accounting/accounting_code_categories").execute!
    puts "Done syncing accounting code categories (#{AccountingCodeCategory.count})"

    Sync::SyncAccountingCodes.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/finance/accounting/accounting_codes").execute!
    puts "Done syncing accounting codes (#{AccountingCode.count})"

    Sync::SyncClusterInformation.sync_banks!
    puts "Done syncing banks (#{Bank.count})"

    #Sync::SyncClusterInformation.sync_branches_and_centers!

    Sync::SyncMembershipTypes.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/membership_types").execute!
    puts "Done syncing membership types (#{MembershipType.count})"

    Sync::SyncAccounts.sync_account_types!
    puts "Done syncing account types (SavingsType: #{SavingsType.count} InsuranceType: #{InsuranceType.count} EquityType: #{EquityType.count})"

    Sync::SyncLoanProducts.sync_loan_products!
    puts "Done syncing loan products (#{LoanProduct.count})"

    Sync::SyncLoanProductMembershipPayments.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/finance/loans/loan_product_membership_payments").execute!
    puts "Done syncing loan product membership payments (#{LoanProductMembershipPayment.count})"

    Sync::SyncClusterInformation.sync_cluster_information!
    puts "Done syncing cluster information (Cluster: #{Cluster.count} Branch: #{Branch.count} Area: #{Area.count} Center: #{Center.count})"

    Sync::SyncAccountingFunds.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/accounting_funds").execute!
    puts "Done syncing accounting funds (#{AccountingFund.count})"

    Sync::SyncReligions.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/religions").execute!
    puts "Done syncing religions (#{Religion.count})"

    Sync::SyncProjectTypeCategories.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/project_type_categories").execute!
    puts "Done syncing project type categories (#{ProjectTypeCategory.count})"

    Sync::SyncProjectTypes.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/project_types").execute!
    puts "Done syncing project types (#{ProjectType.count})"

    Sync::SyncSurveys.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/surveys").execute!
    puts "Done syncing survey (#{Survey.count})"

    Sync::SyncSurveyQuestions.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/survey_questions").execute!
    puts "Done syncing survey questions (#{SurveyQuestion.count})"
  
    Sync::SyncSurveyQuestionOptions.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/survey_question_options").execute!
    puts "Done syncing survey question options (#{SurveyQuestionOption.count})"

    Sync::SyncRegularDepositIntervals.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/regular_deposit_intervals").execute!
    puts "Done syncing regular deposits intervals (#{RegularDepositInterval.count})"

    Sync::SyncRegularDepositAmounts.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/cluster_office/information/regular_deposit_amounts").execute!
    puts "Done syncing regular deposit amounts (#{RegularDepositAmount.count})"
  
    Sync::SyncLoanInsuranceTypes.new(api_key: Settings.api_key, url: "#{Settings.master_server}/api/v1/sync/finance/accounts/loan_insurance_types").execute!
    puts "Done syncing loan insurance types (#{LoanInsuranceType.count})"
  end
end
