namespace :v2 do
  task :update_date_completed => :environment do
    loan_list = Loan.where("status = ? and date_completed IS NULL", "paid")
    size = loan_list.size

    loan_list.each_with_index do |l, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Assigning date completed #{l.id}... #{progress}%%")
      last_payment = LoanPayment.where(loan_id: l.id).order(:paid_at).last
      Loan.find(l.id).update(date_completed: last_payment.paid_at)
    end

  end
  task :update_journal_entries_uuid => :environment do
    journal_entries = JournalEntry.all
    sets            = journal_entries.map{ |o|
                        "(#{o.id}, '#{SecureRandom.uuid}')"
                      }.join(",")

    if sets.present?
      query = "
        UPDATE journal_entries SET
          uuid  = temp.uuid
        FROM (values
          #{sets}
        ) AS temp(id, uuid)
        WHERE temp.id::int = journal_entries.id
      "

      ActiveRecord::Base.connection.execute(query)
    end
    puts "Done."
  end

  task :update_amortization_uuid => :environment do
    amorts  = AmmortizationScheduleEntry.all
    sets    = amorts.map{ |o|
                "('#{o.id}', '#{SecureRandom.uuid}')"
              }.join(",")

    if sets.present?
      query = "
        UPDATE ammortization_schedule_entries SET
          uuid  = temp.uuid
        FROM (values
          #{sets}
        ) AS temp(id, uuid)
        WHERE temp.id::int = ammortization_schedule_entries.id
      "

      ActiveRecord::Base.connection.execute(query)
    end

    puts "Done."
  end

  task :save_loan_cycles => :environment do
    filename  = "loan-cycles-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      loan_cycles: []
    }

    loans = Loan.where(status: ["active", "paid"])

    if ENV['DATE_OFFSET'].present?
      loans = loans.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    loans.each do |o|
      data[:loan_cycles] << {
        id: o.uuid,
        cycle: o.loan_cycle_count
      }
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_member_loan_cycles => :environment do
    filename  = "member-loan-cycles-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      member_loan_cycles: []
    }

    records = MemberLoanCycle.all

    if ENV['DATE_OFFSET'].present?
      records = records.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    records.each do |o|
      data[:member_loan_cycles] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_member_shares => :environment do
    filename  = "member-shares-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      member_shares: []
    }

    records = MemberShare.all

    if ENV['DATE_OFFSET'].present?
      records = records.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    records.each do |o|
      data[:member_shares] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_beneficiaries => :environment do
    filename  = "beneficiaries-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      beneficiaries: []
    }

    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      records = Beneficiary.joins(:member).where("members.branch_id = ? AND members.status IN (?)", branch.id, ["active", "resigned", "pending"]) 
    else
      records = Beneficiary.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    end

    if ENV['DATE_OFFSET'].present?
      records = records.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    records.each do |o|
      data[:beneficiaries] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_legal_dependents => :environment do
    filename  = "legal-dependents-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      legal_dependents: []
    }

    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      records = LegalDependent.joins(:member).where("members.branch_id = ? AND members.status IN (?)", branch.id, ["active", "resigned", "pending"]) 
    else
      records = LegalDependent.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    end

    if ENV['DATE_OFFSET'].present?
      records = records.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    records.each do |o|
      data[:legal_dependents] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  # TODO: Partition this. Hangs at a certain volume
  task :save_billing_collection_records => :environment do
    filename  = "billing-collection-records-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      collection_records: []
    }

    records = PaymentCollectionRecord.where(payment_collection_id: PaymentCollection.approved_billing.pluck(:id))
    size    = records.size

    # Reset counter for every 10000 records
    counter       = 0
    file_counter  = 0
    records.each_with_index do |o, i|
      counter = counter + 1

      printf("\r[#{i+1} / #{size}] Processing collection record #{o.id}... Progress: #{(((i+1).to_f / size.to_f) * 100).round(2)}%%")
      data[:collection_records] << o.to_version_2_billing_hash
      sleep(0.01)

      if counter == 10000
        puts ""

        full_path = "#{Rails.root}/db_backup/#{file_counter}-#{filename}"
        puts "Saving file to #{full_path}#{}..."

        File.write(full_path, JSON.pretty_generate(data))

        file_counter  = file_counter + 1
        counter = 0
        data[:collection_records] = []
      end
    end

    puts ""

    full_path = "#{Rails.root}/db_backup/#{file_counter}-#{filename}"
    puts "Saving file to #{full_path}#{}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_billings => :environment do
    filename  = "billing-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      account_transaction_collections: []
    }

    PaymentCollection.approved_billing.each do |o|
      data[:account_transaction_collections] << o.to_version_2_billing_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_loan_payments => :environment do
    filename  = "loan-payments-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      account_transactions: []
    }

    #valid_loan_payment_ids  = LoanPaymentAmortizationScheduleEntry.all.pluck(:loan_payment_id).uniq
    #loan_payments           = LoanPayment.approved.where("loan_payment_amount > 0 AND is_void IS NULL AND id IN (?)", valid_loan_payment_ids)

    #loans                   = Loan.where(status: ["active", "paid"])
    loans = Loan.joins(:member).where("loans.status = ? and members.status = ?", "active","cleared")
    #loans                   = Loan.where(id: 2407)
    #loan_payments           = LoanPayment.approved.where("loan_payment_amount > 0 AND is_void IS NULL AND loan_id IN (?)", loans.pluck(:id))
    loan_payments           = LoanPayment.approved.where("is_void IS NULL AND loan_id IN (?)", loans.pluck(:id))

    if ENV['DATE_OFFSET'].present?
      loan_payments = loan_payments.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    size  = loan_payments.size

    loan_payments.each_with_index do |o, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Saving loan_payment #{o.uuid}... #{progress}%%")
      data[:account_transactions] << o.to_version_2_hash
    end

    puts ""
    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_member_account_transactions => :environment do
    filename  = "member-account-transactions-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"


    data  = {
      account_transactions: []
    }

    # SAVINGS
    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      savings_accounts = SavingsAccount.where(branch_id: branch.id)
      savings_accounts = savings_accounts.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"]) 
    else
      savings_accounts  = SavingsAccount.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    end

    savings_account_transactions  = SavingsAccountTransaction.approved.where(savings_account_id: savings_accounts.pluck(:id))

    if ENV['DATE_OFFSET'].present?
      savings_account_transactions = savings_account_transactions.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    if ENV['START_DATE'].present? && ENV['END_DATE'].present?
      savings_account_transactions = savings_account_transactions.where("DATE(created_at) >= ? AND DATE(created_at) <= ?", ENV['START_DATE'].to_date, ENV['END_DATE'].to_date)
    end

    savings_account_transactions.each do |o|
      data[:account_transactions] << o.to_version_2_hash
    end

    # INSURANCE
    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      insurance_accounts = InsuranceAccount.where(branch_id: branch.id)
      insurance_accounts = insurance_accounts.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    else
      insurance_accounts = InsuranceAccount.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    end

    insurance_account_transactions  = InsuranceAccountTransaction.approved.where(insurance_account_id: insurance_accounts.pluck(:id))

    if ENV['DATE_OFFSET'].present?
      insurance_account_transactions = insurance_account_transactions.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    if ENV['START_DATE'].present? && ENV['END_DATE'].present?
      insurance_account_transactions = insurance_account_transactions.where("DATE(created_at) >= ? AND DATE(created_at) <= ?", ENV['START_DATE'].to_date, ENV['END_DATE'].to_date)
    end

    insurance_account_transactions.each do |o|
      data[:account_transactions] << o.to_version_2_hash
    end

    # EQUITY
    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      equity_accounts = EquityAccount.where(branch_id: branch.id)
      equity_accounts = equity_accounts.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    else
      equity_accounts = EquityAccount.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    end

    equity_account_transactions = EquityAccountTransaction.approved.where(equity_account_id: equity_accounts.pluck(:id))

    if ENV['DATE_OFFSET'].present?
      equity_account_transactions = equity_account_transactions.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    if ENV['START_DATE'].present? && ENV['END_DATE'].present?
      equity_account_transactions = equity_account_transactions.where("DATE(created_at) >= ? AND DATE(created_at) <= ?", ENV['START_DATE'].to_date, ENV['END_DATE'].to_date)
    end

    equity_account_transactions.each do |o|
      data[:account_transactions] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_member_accounts => :environment do
    filename  = "member-accounts-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      member_accounts: []
    }

    # SAVINGS
    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      savings_accounts = SavingsAccount.where(branch_id: branch.id)
      savings_accounts = savings_accounts.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"]) 
    else
      savings_accounts  = SavingsAccount.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    end

    if ENV['DATE_OFFSET'].present?
      savings_accounts = savings_accounts.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    savings_accounts.each do |o|
      data[:member_accounts] << o.to_version_2_hash
    end

    # INSURANCE
    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      insurance_accounts = InsuranceAccount.where(branch_id: branch.id)
      insurance_accounts = insurance_accounts.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    else
      insurance_accounts = InsuranceAccount.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    end

    if ENV['DATE_OFFSET'].present?
      insurance_accounts = insurance_accounts.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    insurance_accounts.each do |o|
      data[:member_accounts] << o.to_version_2_hash
    end

    # EQUITY
    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      equity_accounts = EquityAccount.where(branch_id: branch.id)
      equity_accounts = equity_accounts.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    else
      equity_accounts = EquityAccount.joins(:member).where("members.status IN (?)", ["active", "resigned", "pending"])
    end

    if ENV['DATE_OFFSET'].present?
      equity_accounts = equity_accounts.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    equity_accounts.each do |o|
      data[:member_accounts] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_amortization_schedule_entries => :environment do
    #loans                         = Loan.where(status: ["active", "paid"])
    loans = Loan.joins(:member).where("loans.status = ? and members.status = ?", "active","cleared")
    amortization_schedule_entries = AmmortizationScheduleEntry.where(is_void: nil, loan_id: loans.pluck(:id).uniq) 

    if ENV['DATE_OFFSET'].present?
      amortization_schedule_entries = amortization_schedule_entries.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    filename  = "amortization-schedule-entries-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      amortization_schedule_entries: []
    }

    size  = amortization_schedule_entries.size

    amortization_schedule_entries.each_with_index do |o, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Saving amortization #{o.uuid}... #{progress}%%")
      data[:amortization_schedule_entries] << o.to_version_2_hash
    end

    puts ""
    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_loans => :environment do
    #loans = Loan.where(status: ["active", "paid"])
    loans = Loan.joins(:member).where("loans.status = ? and members.status = ?", "active","cleared")
    if ENV['DATE_OFFSET'].present?
      loans = loans.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    filename  = "loans-v2.json"
    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      loans: []
    }

    loans.each do |o|
      data[:loans] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_loan_products => :environment do
    loan_products = LoanProduct.all

    filename  = "loan-products-v2.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    data  = {
      loan_products: []
    }

    loan_products.each do |o|
      data[:loan_products] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_project_types => :environment do
    project_types = ProjectType.all

    filename  = "project-types-v2.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    data  = {
      project_types: []
    }

    project_types.each do |o|
      data[:project_types] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_project_type_categories => :environment do
    project_type_categories = ProjectTypeCategory.all

    filename  = "project-type-categories-v2.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    data  = {
      project_type_categories: []
    }

    project_type_categories.each do |o|
      data[:project_type_categories] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_journal_entries => :environment do
    accounting_entries  = Voucher.where(status: ["approved", "pending"])
    journal_entries     = JournalEntry.where(voucher_id: accounting_entries.pluck(:id).uniq)

    if ENV['DATE_OFFSET'].present?
      journal_entries = journal_entries.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    filename            = "journal-entries-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      journal_entries: []
    }

    journal_entries.each do |o|
      data[:journal_entries] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_accounting_entries => :environment do
    accounting_entries  = Voucher.where("status IN (?)", ["approved", "pending"])

    if ENV['DATE_OFFSET'].present?
      accounting_entries = accounting_entries.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    filename  = "accounting-entries-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      accounting_entries: []
    }

    accounting_entries.each do |o|
      data[:accounting_entries] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_members => :environment do
    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      members = Member.where(branch_id: branch.id)
      filename  = "#{branch}-members-v2.json" 
    else
      members   = Member.all
      filename  = "members-v2.json"
    end

    if ENV['DATE_OFFSET'].present?
      members = members.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      members: []
    }

    members.each do |o|
      data[:members] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_centers => :environment do
    if ENV['BRANCH_ID'].present?
      branch    = Branch.find(ENV['BRANCH_ID'])
      centers   = Center.where(branch_id: branch.id)
      filename  = "#{branch}-centers-v2.json"
    else
      centers   = Center.all
      filename  = "centers-v2.json"
    end

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      centers: []
    }

    centers.each do |o|
      data[:centers] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_branches => :environment do
    branches  = Branch.all
    filename  = "branches-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      branches: []
    }

    branches.each do |o|
      data[:branches] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_clusters => :environment do
    clusters  = Cluster.all
    filename  = "clusters-v2.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      clusters: []
    }

    clusters.each do |o|
      data[:clusters] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_areas => :environment do
    areas     = Area.all
    filename  = "areas-v2.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      areas: []
    }

    areas.each do |o|
      data[:areas] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_users => :environment do
    users     = User.all
    filename  = "users-v2.json"

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end
    full_path = "#{Rails.root}/db_backup/#{filename}"

    data = {
      users: []
    }

    users.each do |o|
      data[:users] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_accounting_codes => :environment do
    accounting_codes  = AccountingCode.all
    filename          = "accounting-codes-v2.json"
    full_path         = "#{Rails.root}/db_backup/#{filename}"

    data = {
      accounting_codes: []
    }

    accounting_codes.each do |o|
      data[:accounting_codes] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :load_accounting_code_uuids => :environment do
    data  = JSON.parse(File.read(ENV['FILENAME'])).with_indifferent_access

    size      = data[:accounting_codes].size

    data[:accounting_codes].each_with_index do |o, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      #AccountingCode.where(code: o[:code]).first.update!(uuid: o[:id])
      #accounting_code = AccountingCode.where("upper(name) = ?", o[:name].upcase).first
      accounting_code = AccountingCode.where(name: o[:name]).first

      if accounting_code.blank?
        puts "Can't find accounting code #{o[:name]} in database!"
      else
        accounting_code.update!(uuid: o[:id])
        #printf("\r(#{i+1}/#{size}): Updated accounting code: #{accounting_code.to_s}... #{progress}%")
      end
    end

    puts "Done!"
  end

  task :load_loan_product_uuids => :environment do
    data  = JSON.parse(File.read(ENV['FILENAME'])).with_indifferent_access

    data[:loan_products].each do |o|
      LoanProduct.where(name: o[:name]).first.update!(uuid: o[:id])
    end

    puts "Done!"
  end

  task :save_accounting_funds => :environment do
    accounting_funds     = AccountingFund.all
    filename  = "accounting-funds-v2.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      accounting_funds: []
    }

    accounting_funds.each do |o|
      data[:accounting_funds] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_clipclaims => :environment do
    clip_claims      = ClipClaim.all
    filename        = "clipclaims-v2.json"
    full_path       = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      clip_claims: []
    }

    clip_claims.each do |o|
      data[:clip_claims] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end
  task :save_claims => :environment do
    claims          = Claim.all
    filename        = "claims-v2.json"
    full_path       = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      claims: []
    }

    claims.each do |o|
      data[:claims] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end
  task :save_validations => :environment do
    validations     = InsuranceAccountValidation.where(status: 'approved')
    filename        = "validations-v2.json"
    full_path       = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      validations: []
    }

    validations.each do |o|
      data[:validations] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_validation_records => :environment do
    validation_records      = InsuranceAccountValidationRecord.where(status: 'approved')
    filename                = "validation-records-v2.json"
    full_path               = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      validation_records: []
    }

    validation_records.each do |o|
      data[:validation_records] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :update_validations_uuid => :environment do
    validations     = InsuranceAccountValidation.all
    size            = validations.size

    validations.each_with_index do |o, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Assigning new uuid to journal_entry #{o.id}... #{progress}%%")
      o.update!(uuid: "#{SecureRandom.uuid}")
    end

    puts ""
    puts "Done."
  end

  task :update_validation_records_uuid => :environment do
    validation_records    = InsuranceAccountValidationRecord.all
    size            = validation_records.size

    validation_records.each_with_index do |o, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Assigning new uuid to journal_entry #{o.id}... #{progress}%%")
      o.update!(uuid: "#{SecureRandom.uuid}")
    end

    puts ""
    puts "Done."
  end

  task :update_claims_uuid => :environment do
    claims          = Claim.all
    size            = claims.size

    claims.each_with_index do |o, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Assigning new uuid to journal_entry #{o.id}... #{progress}%%")
      o.update!(uuid: "#{SecureRandom.uuid}")
    end

    puts ""
    puts "Done."
  end

  task :update_clip_claims_uuid => :environment do
    clip_claims           = ClipClaim.all
    size                  = clip_claims.size

    clip_claims.each_with_index do |o, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Assigning new uuid to journal_entry #{o.id}... #{progress}%%")
      o.update!(uuid: "#{SecureRandom.uuid}")
    end

    puts ""
    puts "Done."
  end
end
