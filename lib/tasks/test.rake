namespace :test do

  task :jef => :environment do
    @jefdrilon = []
    @savings_accounts = SavingsAccount.where( savings_type_id: [1],  member_id: 1452)
    #@savings_accounts = SavingsAccount.where( savings_type_id: [1,2],  member_id: 2225)
    @savings_accounts.each do |savings_account|
    savings_type          = savings_account.savings_type
    annual_interest_rate  = savings_type.annual_interest_rate
    if savings_type.id == 1
      savings_account_transaction =  SavingsAccountTransaction.where("
                                                                        savings_account_id = ? and 
                                                                        transaction_type <> ?", 
                                                                        SavingsAccount.where(member_id: savings_account.member.id, savings_type: 1).last.id,
                                                                        "interest").last
      
      if savings_account_transaction
        member_savings_account_transaction = savings_account_transaction.transacted_at
        
        #raise member_savings_account_transaction.inspect
        final_date     = savings_account_transaction.transacted_at.to_date + 2.years 
    
        
        #raise final_date.inspect
        if member_savings_account_transaction > final_date         
           annual_interest_rate = 0
        else
          #raise "drilon".inspect
           is_dormant_loaner     = ::Members::IsDormantLoaner.new(
                                                                    member: savings_account.member, 
                                                                    threshold: Settings.dormant_loaner_threshold,
                                                                    current_date: "2019-04-30",
                                                                    savings_type: savings_type
                                                                ).execute!

            raise is_dormant_loaner.inspect
            if is_dormant_loaner
              annual_interest_rate  = Settings.dormant_loaner_annual_interest_rate || 0.02
            end
        end
      else
        annual_interest_rate = 0
      end
    else

      
      is_dormant_loaner     = ::Members::IsDormantLoaner.new(
                                                              member: savings_account.member, 
                                                              threshold: Settings.dormant_loaner_threshold,
                                                              current_date: "2019-04-30",
                                                              savings_type: savings_type
                                                            ).execute!
      if is_dormant_loaner
        annual_interest_rate  = Settings.dormant_loaner_annual_interest_rate || 0.02
      end
      
    end
    @jefdrilon << annual_interest_rate
    end
    raise @jefdrilon.inspect
  end


  task :approve_center_transfer_record => :environment do
    center_transfer_record  = CenterTransferRecord.find(ENV['ID'])
    user                    = User.first

    ::Transfer::ApproveCenterTransferRecord.new(
      center_transfer_record: center_transfer_record,
      user: user
    ).execute!
  end
end
