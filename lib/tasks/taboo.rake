namespace :taboo do
  task :correct_insurance_account_transactions => :environment do
    transactions = InsuranceAccountTransaction.where(transaction_type: "deposit")

    transactions.each do |t|
      account = t.insurance_account
      previous_balance = account.balance
      account.update(balance: account.balance - t.amount)
      puts "Account: #{account.to_s} updated! Previous balance: #{previous_balance.to_f} Current Balance: #{account.balance.to_f}"
    end

    transactions.delete_all
    puts "fin"
  end
end
