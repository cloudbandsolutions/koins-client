namespace :dev do
  task :check_invalid_journal_entries => :environment do
    accounting_entries  = Voucher.approved
    journal_entries     = JournalEntry.where(voucher_id: accounting_entries.pluck(:id).uniq)
   
    invalid_ids = []
    size        = journal_entries.size

    puts "Journal Entries to scan: #{size}"

    journal_entries.each_with_index do |o, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Scanning journal_entry #{o.id}... #{progress}%%")

      if !o.valid?
        invalid_ids << o.id
      end
    end

    if invalid_ids.size > 0
      puts "Invalid entries found: #{invalid_ids.size}"
      puts invalid_ids
    else
      puts "No invalid journal_entries found. All good."
    end
  end

  task :display_invalid_loan_amount => :environment do
    counter = 0
    Loan.where(status: ["active", "paid"]).each_with_index do |loan|
      if loan.amount != loan.ammortization_schedule_entries.sum(:principal)
        counter += 1
        puts "#{counter}: Loan ID: #{loan.id} (#{loan.status})\t\t\tLoan Product: #{loan.loan_product}\t\tMember: #{loan.member.to_s}"
      end
    end
  end

  task :update_insurance_accounts_center_id_by_branch_id => :environment do
    puts "Updating ..."
    members = Member.where(branch_id: ENV['BRANCH_ID'])
    
    members.each do |m|
      puts "Updating insurance accounts center id for #{m}"
      center = m.center_id
      m.insurance_accounts.each do |i|
        i.update!(center_id: center)
      end
    end
  end

  task :compute_insurance_account_validation_total_values => :environment do
    puts "Computing..."
    total_rf = 0.00
    total_50_percent_lif = 0.00
    total_advance_lif = 0.00
    total_advance_rf = 0.00
    total_interest = 0.00
    total = 0.00
    
    insurance_account_validations = InsuranceAccountValidation.all
    insurance_account_validations.each do |insurance_account_validation|
      puts "Insurance Account Validation: #{insurance_account_validation.id}"
      insurance_account_validation.insurance_account_validation_records.each do |insurance_account_validation_record|
        puts "Insurance Account Validation Record: #{insurance_account_validation_record.id}"
        total_rf += insurance_account_validation_record.rf
        total_50_percent_lif += insurance_account_validation_record.lif_50_percent
        total_advance_lif += insurance_account_validation_record.advance_lif
        total_advance_rf += insurance_account_validation_record.advance_rf
        total_interest += insurance_account_validation_record.interest
        total += insurance_account_validation_record.total 
      end
      puts "Updating #{insurance_account_validation.id} ..."
      insurance_account_validation.update!(
        total: total,
        total_rf: total_rf,
        total_advance_rf: total_advance_rf,
        total_advance_lif: total_advance_lif,
        total_50_percent_lif: total_50_percent_lif,
        total_interest: total_interest,
        )

      total_rf = 0.00
      total_50_percent_lif = 0.00
      total_advance_lif = 0.00
      total_advance_rf = 0.00
      total_interest = 0.00
      total = 0.00 
    end
  end

  task :repair_membership_payment_approvals => :environment do
    puts "Repairing..."
    payment_collection = PaymentCollection.find(ENV['PAYMENT_COLLECTION_ID'])
    CollectionTransaction.where(account_type: "MEMBERSHIP_PAYMENT", payment_collection_record_id: payment_collection.payment_collection_records.pluck(:id)).each do |collection_transaction|
      if collection_transaction.amount > 0
        member          = collection_transaction.payment_collection_record.member
        membership_type = MembershipType.where(name: collection_transaction.account_type_code).first
        MembershipPayment.create!(
          member: member,
          membership_type: membership_type,
          paid_at: payment_collection.paid_at,
          created_at: payment_collection.paid_at,
          amount_paid: collection_transaction.amount,
          status: "paid"
        )

        if membership_type.activated_for_loans == true
          member.update(previous_mfi_member_since: payment_collection.paid_at)
        elsif membership_type.activated_for_insurance == true
          member.update(previous_mii_member_since: payment_collection.paid_at)
        end

        member.update(status: "active")
      end
    end
  end

  task :repair_insurance_deposits => :environment do
    puts "Repairing..."
    payment_collection = PaymentCollection.find(ENV['PAYMENT_COLLECTION_ID'])
    payment_collection_records = PaymentCollectionRecord.where(payment_collection: payment_collection)
    payment_collection_records.each do |payment_collection_record| 
      puts "#{payment_collection_record.id}"
      CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record: payment_collection_record).each do |collection_transaction|
        insurance_account_transactions = InsuranceAccountTransaction.joins(:insurance_account).where("insurance_account_transactions.voucher_reference_number = ? AND insurance_accounts.member_id = ?",payment_collection.reference_number, payment_collection_record.member.id)
        if insurance_account_transactions.size != 2
          puts "..."
          if collection_transaction.amount > 0
            user = User.find(ENV['USER_ID'])

            for_resignation = nil

            if payment_collection.for_resignation == true
              for_resignation = true
            end

            member          = collection_transaction.payment_collection_record.member
            insurance_type    = InsuranceType.where(code: collection_transaction.account_type_code).first
            insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).first
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: collection_transaction.amount,
                                            transacted_at: payment_collection.paid_at,
                                            created_at: payment_collection.paid_at,
                                            particular: payment_collection.particular,
                                            transaction_type: "deposit",
                                            voucher_reference_number: payment_collection.reference_number,
                                            insurance_account: insurance_account,
                                            for_resignation: for_resignation
                                          )
            insurance_account_transaction.approve!(user)
            puts "Repairing insurance deposit of #{member} - #{insurance_account}..."
          end
        end
      end
    end
    puts "Done!"
  end

  task :repair_insurance_fund_transfer_for_45 => :environment do
    puts "Repairing..."
    payment_collection = PaymentCollection.find(ENV['PAYMENT_COLLECTION_ID'])
    payment_collection_records = PaymentCollectionRecord.where(payment_collection: payment_collection)
    payment_collection_records.each do |payment_collection_record| 
      puts "#{payment_collection_record.id}"
      CollectionTransaction.where(account_type: "INSURANCE_FUND_TRANSFER", payment_collection_record: payment_collection_record).each do |collection_transaction|
        insurance_account_transactions = InsuranceAccountTransaction.joins(:insurance_account).where("insurance_account_transactions.voucher_reference_number = ? AND insurance_accounts.member_id = ?",payment_collection.reference_number, payment_collection_record.member.id)
        if insurance_account_transactions.size < 1
          puts "..."
          if collection_transaction.amount > 0
            user = User.find(ENV['USER_ID'])

            for_resignation = nil

            if payment_collection.for_resignation == true
              for_resignation = true
            end

            member          = collection_transaction.payment_collection_record.member
            insurance_type    = InsuranceType.where(code: collection_transaction.account_type_code).first
            insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).first
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: collection_transaction.amount,
                                            transacted_at: payment_collection.paid_at,
                                            created_at: payment_collection.paid_at,
                                            particular: payment_collection.particular,
                                            transaction_type: "fund_transfer_deposit",
                                            voucher_reference_number: payment_collection.reference_number,
                                            insurance_account: insurance_account,
                                            for_resignation: for_resignation
                                          )
            insurance_account_transaction.approve!(user)
            puts "Repairing insurance fund transafer deposit of #{member} - #{insurance_account}..."
          end
        end
      end
    end
    puts "Done!"
  end

  task :insert_spouse_as_legal_dependent => :environment do
    puts "Inserting..."
    members = Member.active
    members.each do |member|
      if member.spouse_first_name.present?
        member_id = member.id
        spouse_first_name = member.spouse_first_name
        spouse_last_name = member.spouse_last_name
        spouse_middle_name = member.spouse_middle_name
         
        if member.spouse_date_of_birth.nil?
          spouse_date_of_birth = Time.now.strftime("%Y-%m-%d")
        else
          spouse_date_of_birth = member.spouse_date_of_birth
        end

        legal_dependent_record = LegalDependent.where(first_name: spouse_first_name, last_name: spouse_last_name, date_of_birth: spouse_date_of_birth).first

        if legal_dependent_record.nil?
          legal_dependent = LegalDependent.new
          legal_dependent.first_name = spouse_first_name
          legal_dependent.middle_name = spouse_middle_name
          legal_dependent.last_name = spouse_last_name
          legal_dependent.date_of_birth = spouse_date_of_birth
          legal_dependent.is_deceased = false
          legal_dependent.is_tpd = false
          legal_dependent.relationship = "Spouse"
          legal_dependent.member_id = member_id

          legal_dependent.save!
        end      
      end
      puts "Updating dependents of #{member}..."
    end
    puts "Done!"  
  end
  
  task :insert_interest_transactions => :environment do
    #puts "Hello World"
    require 'csv'
    #path_to_csv = "#{Rails.root}/csv/MarilaoInterest.csv"

    CSV.foreach("#{Rails.root}/csv/MarilaoInterest.csv", { encoding: "UTF-8", headers: true, header_converters: :symbol, converters: :all}) do |row|
      data = row.to_hash
      interest_amount     = data[:amount]
      savings_account_id  = data[:savings_account_id]
      savings_account     = SavingsAccount.find(savings_account_id)
      transacted_at       = data[:transacted_at]
      #raise interest_amount.inspect
      puts "========================================="
      puts "Inserting interest #{interest_amount} to #{savings_account}"
      savings_account_transaction = SavingsAccountTransaction.create!(
                                      amount: interest_amount,
                                      savings_account: savings_account,
                                      transacted_at: transacted_at,
                                      created_at: transacted_at,
                                      updated_at: transacted_at,
                                      transaction_type: "interest"
                                    )

      savings_account_transaction.approve!("SYSTEM")


      puts "Rehashing account #{savings_account}"
      ::Savings::RehashAccount.new(savings_account: savings_account).execute!
      puts "========================================="
    end
  end

  task :insert_interest_on_share_capital => :environment do
    require 'csv'
    
    CSV.foreach("#{Rails.root}/csv/InterestOnShareCapital.csv", { encoding: "UTF-8", headers: true, header_converters: :symbol, converters: :all}) do |row|
      data = row.to_hash
      member_id = data[:MemberId]
      jan = data[:Jan]
      feb = data[:Feb]
      march = data[:Mar]
      apr = data[:Apr]
      may = data[:May]
      june = data[:June]
      july = data[:July]
      aug = data[:Aug]
      sept = data[:Sept]
      oct = data[:Oct]
      nov = data[:Nov]
      dec = data[:Dec]

      total_share = data[:TotalShare]
      ave_share = data[:AveShare]

      #raise test.inspect
    end

  end

      
  
  task :fetch_invalid_savings_accounts => :environment do
    invalid_accounts  = []

    SavingsAccount.all.each_with_index do |savings_account, i|
      #puts "#{i+1}: Examining savings account #{savings_account}"
      savings_account_transactions  = SavingsAccountTransaction.where(
                                        "savings_account_id = ? AND amount > 0 AND status IN (?)", 
                                        savings_account.id, ["approved", "reversed"]).order("transacted_at ASC"
                                      )

      previous_ending_balance = nil
      savings_account_transactions.each do |savings_account_transaction|
        if previous_ending_balance
          if savings_account_transaction.beginning_balance != previous_ending_balance and savings_account_transaction.transacted_at.year == Date.today.year
            puts "Found invalid account transaction #{savings_account}"
            invalid_accounts <<  {
              transacted_at: savings_account_transaction.transacted_at,
              member_id: savings_account.member.id,
              member: savings_account.member.to_s,
              savings_account_id: savings_account.id,
              transaction_id: savings_account_transaction.id,
              previous_ending_balance: previous_ending_balance,
              beginning_balance: savings_account_transaction.beginning_balance,
              ending_balance: savings_account_transaction.ending_balance,
              status: savings_account_transaction.status,
              amount: savings_account_transaction.amount
            }
          end

          previous_ending_balance = savings_account_transaction.ending_balance
        else
          previous_ending_balance = savings_account_transaction.ending_balance
        end
      end
    end

    invalid_accounts =  invalid_accounts.uniq
    puts "Number of invalid accounts: #{invalid_accounts.size}"
    puts "Number of accounts: #{SavingsAccount.count}"
    ap invalid_accounts
  end

  task :repair_unsave_equity => :environment do
    collection_transaction = CollectionTransaction.find(ENV['COLLECTION_TRANSACTION_ID'])
    payment_collection = collection_transaction.payment_collection_record.payment_collection
    
    user = User.find(ENV['USER_ID'])  
    
    if collection_transaction.amount > 0
      #for_resignation = nil
      #if payment_collection.for_resignation == true
      #  for_resignation = true
      #end

      member                      = collection_transaction.payment_collection_record.member
      equity_type                 = EquityType.where(code: collection_transaction.account_type_code).first
      equity_account              = EquityAccount.where(member_id: member.id, equity_type_id: equity_type.id).first
      equity_account_transaction  = EquityAccountTransaction.create!(
                                        amount: collection_transaction.amount,
                                        transacted_at: payment_collection.paid_at,
                                        created_at: payment_collection.paid_at,
                                        particular: payment_collection.particular,
                                        transaction_type: "deposit",
                                        voucher_reference_number: payment_collection.reference_number,
                                        equity_account: equity_account,
                                        for_resignation: true,
                                        approved_by: user
                                       )
    equity_account_transaction.approve!(user)
    
    end

    puts "done"
    #puts payment_collection.inspect
    #puts user

  end


  task :reset_id_numbers => :environment do
    puts "Resetting IDs..."
    ::Special::Generator::ResetIdNumbers.new.execute!
    puts "Done..."
  end

  task :rehash_incorrect_loan_amount => :environment do
    loan              = Loan.find(ENV['LOAN_ID'])
    correct_amount    = ENV['CORRECT_AMOUNT'].to_f
    book              = loan.book
    reference_number  = loan.voucher_reference_number
    voucher           = Voucher.where(book: book, reference_number: reference_number).last
    dr_code           = AccountingCode.find(ENV['DR_CODE_ID'])
    cr_code           = AccountingCode.find(ENV['CR_CODE_ID'])

    puts "Correcting loan #{loan.id} with amount #{loan.amount} to amount #{correct_amount}"
    puts "Found Accounting Entry #{voucher.id} (visit /vouchers/#{voucher.id} to verify)"

    puts "Correcting accounting entries..."
    # Loans Receivable
    dr_journal_entry  = voucher.journal_entries.where(accounting_code_id: dr_code.id).first

    # Cash in Bank
    cr_journal_entry  = voucher.journal_entries.where(accounting_code_id: cr_code.id).first

    # CR amount prior to cash in bank
    deduction_amount  = 0.00
    voucher.journal_entries.where(post_type: 'CR').each do |je|
      if je.accounting_code_id != cr_journal_entry.accounting_code.id
        deduction_amount  += je.amount
      end
    end

    # New loans receivable
    dr_journal_entry.update!(amount: correct_amount)

    # New cash in bank
    cb_amount = correct_amount - deduction_amount
    cr_journal_entry.update!(amount: cb_amount)

    puts "Correcting loan amount and amount released..."
    # NOTE: Do not use Model.query so callbacks won't be triggered (i.e. before_validation, before_save etc...)
    ActiveRecord::Base.connection.execute("
      UPDATE loans SET amount = #{correct_amount}, amount_released = #{cb_amount} WHERE id = #{loan.id}
    ")

    puts "Done. Please reamortize loan found in /loans/#{loan.id}."
  end

  task :restore_resigned_member => :environment do
    member  = Member.find(ENV['MEMBER_ID'])
    puts "Restoring member #{member.full_name}"

    ::Members::RestoreResignedMember.new(member: member).execute!

    puts "Done..."
  end

  task :check_invalid_insurance_dates => :environment do
    members = Member.active.order("last_name ASC")

    members.each do |member|
      begin
        Date.parse(member.recognition_date_formatted)
      rescue ArgumentError
        puts "Invalid recognition date for member #{member.id}"
      end
    end
  end

  task :repair_invalid_pure_savers => :environment do
    active_loan_entry_points     = Loan.active
    active_members               = Member.pure_active
    pending_members              = Member.pending
    member_ids_with_positive_savings         = SavingsAccount.where("balance >= 0").pluck(:member_id).uniq
    member_ids_with_active_entry_point_loans = active_loan_entry_points.pluck(:member_id)
    member_ids_active_only                   = member_ids_with_positive_savings | member_ids_with_active_entry_point_loans

    members = active_members.where(id: member_ids_with_positive_savings).where.not(id: member_ids_with_active_entry_point_loans)

    members.each do |m|
      if m.active? and m.equity_accounts.sum(:balance) == 0
        puts "Invalid member #{m} (#{m.id}). Repairing..."
        m.update!(status: "pending")
      end
    end
  end

  task :repair_transferred_loans_amortization => :environment do
    loans = Loan.active

    loans.each do |loan|
      if loan.temp_total_amount_due != loan.remaining_balance and loan.ammortization_schedule_entries.paid.count == 0
        loan.ammortization_schedule_entries.unpaid.each do |ase|
          if ase.amount != (ase.principal + ase.interest)
            puts "Found invalid amortization #{ase.id} in loan #{loan.id}. Amount: #{ase.amount} Principal + Interest: #{ase.principal + ase.interest}"
            ase.update(amount: (ase.principal + ase.interest))
          end
        end
      end
    end
  end

  task :repair_transferred_loans => :environment do
    loans                 = Loan.active

    loans.each do |loan|
      if loan.old_interest_balance.present? and loan.ammortization_schedule_entries.paid.count == 0
        interest_balance      = loan.interest_balance
        old_interest_balance  = loan.old_interest_balance
        old_principal_balance = loan.old_principal_balance
        amount                = loan.amount
        remaining_balance     = loan.remaining_balance
        true_remaining_balance  = old_principal_balance + old_interest_balance
        principal_balance     = loan.principal_balance
        old_principal_balance = loan.old_principal_balance

        if remaining_balance != true_remaining_balance
          diff_remaining_balance  = remaining_balance - true_remaining_balance
          puts "Loan #{loan.id} has invalid remaining balance: Remaining Balance: #{remaining_balance} True Remaining Balance: #{true_remaining_balance}"
          puts "Repairing remaining balance..."
          loan.update!(remaining_balance: true_remaining_balance)
        end

        if loan.old_interest_balance > 0 and interest_balance != old_interest_balance
          diff_interest = interest_balance - old_interest_balance
          first_amort     = loan.ammortization_schedule_entries.unpaid.order("due_at ASC").first
          first_interest  = first_amort.interest
          puts "Loan #{loan.id} with interest balance #{interest_balance} != #{old_interest_balance} (difference: #{diff_interest}) and no payments yet"
          puts "Interest of first amortization: #{first_interest}"
          puts "Reparing interest..."

          #new_interest  = first_interest - diff_interest
          #first_amort.update!(interest: new_interest, amount: (first_amort.principal + new_interest))

          while diff_interest != 0 do
            loan.ammortization_schedule_entries.unpaid.order("due_at ASC").each do |ase|
              if diff_interest < 0
                new_interest  = ase.interest + 1
              elsif diff_interest > 0
                new_interest  = ase.interest - 1
              end

              if diff_interest != 0
                ase.update(interest: new_interest, amount: (new_interest + ase.principal))
              end

              if diff_interest < 0
                diff_interest += 1
              elsif diff_interest > 0
                diff_interest -= 1
              end
            end
          end
        end
      end
    end
  end

  task :adjust_accounting_entries_to_month_end => :environment do
    year        = ENV['YEAR'].try(:to_i)
    month_entry = ENV['MONTH_ENTRY'].try(:to_i)
    month_adj   = ENV['MONTH_ADJ'].try(:to_i)
    book        = ENV['BOOK']

    date        = Date.civil(year, month_adj, -1)

    Voucher.approved.where("extract(month from date_prepared) = ? AND book = ?", month_entry, book).each do |v|
      puts "Adjusting Accounting Entry #{book} #{v.id} dated #{v.date_prepared} to #{date}"
      v.update!(date_prepared: date)
    end
  end

  task :adjust_insurance_account_transactions_to_month_end => :environment do
    year        = ENV['YEAR'].try(:to_i)
    month_entry = ENV['MONTH_ENTRY'].try(:to_i)
    month_adj   = ENV['MONTH_ADJ'].try(:to_i)

    date        = Date.civil(year, month_adj, -1)

    InsuranceAccountTransaction.approved.where("extract(month from transacted_at) = ?", month_entry).each do |t|
      puts "Adjusting Insurance Transaction #{t.id} dated #{t.transacted_at} to #{date}"
      sql = "UPDATE insurance_account_transactions SET transacted_at='#{date.strftime("%Y-%m-%d")}', transaction_date='#{date.strftime("%Y-%m-%d")}', created_at='#{date.strftime("%Y-%m-%d")}', updated_at='#{date.strftime("%Y-%m-%d")}' WHERE extract(month from transacted_at) = #{month_entry.to_i}"
      ActiveRecord::Base.connection.execute(sql)
    end
  end

  task :adjust_payment_collections_to_month_end => :environment do
    year        = ENV['YEAR'].try(:to_i)
    month_entry = ENV['MONTH_ENTRY'].try(:to_i)
    month_adj   = ENV['MONTH_ADJ'].try(:to_i)

    date        = Date.civil(year, month_adj, -1)

    PaymentCollection.approved.where("extract(month from paid_at) = ?", month_entry).each do |pc|
      puts "Adjusting Payment Collection #{pc.id} dated #{pc.paid_at} to #{date}"
      pc.update!(paid_at: date)
    end
  end

  task :update_accounting_entries_sub_reference_number => :environment do
    Voucher.approved.order("date_prepared ASC").all.each do |v|
      sub_reference_number  = v.sub_reference_number

      if sub_reference_number.blank? and v.accounting_fund.present?
        sub_reference_number  = ::Accounting::GenerateSubReferenceNumber.new(
                                  accounting_fund: v.accounting_fund,
                                  book: v.book
                                ).execute!

        v.update!(sub_reference_number: sub_reference_number)

        puts "Updated Accounting Entry Ref. #{v.reference_number} with sub_reference_number: #{v.sub_reference_number}"
      end
    end
  end

  task :update_member_status_to_archived => :environment do
    members = Member.where("first_name LIKE ?", "%ARCHIVED%")
    puts "Updating member status to archived"
    members.each do |member|
      member.update!(status: "archived")
    end
    puts "Done!"
  end

  task :delete_personal_documents_of_resigned_and_cleared_members => :environment do
    puts "Deleting resigned members personal documents"
    Member.where(status: "resigned").each do |res|
      res.member_attachment_files.each do |att|
        if !att.title.include? "BLIP"
          att.destroy!
        end
      end
      puts "Successfully delete pesonal documents for #{res.full_name}"
    end

    puts "Deleting cleared members personal documents"
    Member.where(status: "cleared").each do |clr|
      clr.member_attachment_files.each do |att|
        if !att.title.include? "BLIP"
          att.destroy!
        end
      end  
      puts "Successfully delete pesonal documents for #{clr.full_name}"
    end
  end

  task :delete_member_using_id => :environment do
    file_location = ENV["FILE_LOCATION"]
    puts "Searching file #{file_location}"

    CSV.foreach(file_location, headers: true) do |row|
      member = Member.where(identification_number: row['identification_number']).first

      if member
        puts "Deleting #{member.full_name}!"
        member.destroy
      end
    end

    puts "Done!"
  end

  task :update_member_using_id => :environment do
    file_location = ENV["FILE_LOCATION"]
    puts "Searching file #{file_location}"

    CSV.foreach(file_location, headers: true) do |row|
      member = Member.where(identification_number: row['meta_id']).first

      if member
        puts "Updating #{member.full_name}!"
        member.update!(identification_number: row['identification_number'])
      end
    end

    puts "Done!"
  end

  task :load_insurance_account_transactions => :environment do
    file_location = ENV["FILE_LOCATION"]
    puts "Searching file #{file_location}"

    insurance_account_ids = []
    CSV.foreach(file_location, headers: true) do |row|
      uuid = row['uuid']
      insurance_account_transaction_record = InsuranceAccountTransaction.where(uuid: uuid).first

      if insurance_account_transaction_record.nil?
        puts "Creating new insurance account transaction record #{uuid}..."

        insurance_account_transaction = InsuranceAccountTransaction.new
        insurance_account_transaction.transacted_at = row['transacted_at']
        insurance_account_transaction.particular = row['particular']
        insurance_account_transaction.status = row['status']
        insurance_account_transaction.transacted_by = row['transacted_by']
        insurance_account_transaction.approved_by = row['approved_by']
        insurance_account_transaction.voucher_reference_number = row['voucher_reference_number']
        insurance_account_transaction.transaction_number = row['transaction_number']
        insurance_account_transaction.bank_id = row['bank_id']
        insurance_account_transaction.accounting_code_id = row['accounting_code_id']
        insurance_account_transaction.uuid = row['uuid']
      
        insurance_account_transaction.transaction_date = row['transaction_date']
        insurance_account_transaction.is_adjustment = row['is_adjustment']

        insurance_account_uuid = row['insurance_account_uuid']
        statuses = ["active", "inactive"]
        insurance_account = InsuranceAccount.where("uuid = ? AND status IN (?)", insurance_account_uuid, statuses).first
        # insurance_account = InsuranceAccount.where("uuid = ? AND status = ?", insurance_account_uuid, "active").first

        insurance_account_transaction.insurance_account_id = insurance_account.id
        amount = row['amount']
        transaction_type = row['transaction_type']
        insurance_account_transaction.amount = amount
        insurance_account_transaction.transaction_type = transaction_type
        
        insurance_account_transaction.save!
        insurance_account_ids << insurance_account_transaction.insurance_account.id
        puts "Done creating!"
      else
        puts "Updating existing insurance account transaction record #{uuid}..."
        insurance_account = InsuranceAccount.where(uuid: row['insurance_account_uuid']).first

        insurance_account_transaction_record.update!(
          amount: row['amount'],
          transaction_type: row['transaction_type'],
          transacted_at: row['transacted_at'],
          particular: row['particular'],
          status: row['status'],
          transacted_by: row['transacted_by'],
          approved_by: row['approved_by'],
          voucher_reference_number: row['voucher_reference_number'],
          transaction_number: row['transaction_number'],
          uuid: row['uuid'],
          transaction_date: row['transaction_date'],
          is_adjustment: row['is_adjustment'],
          insurance_account_id: insurance_account.id
        )

        insurance_account_ids << insurance_account_transaction_record.insurance_account.id
        puts "Done updating!"
      end
    end

    insurance_account_ids = insurance_account_ids.uniq

    InsuranceAccount.where(id: insurance_account_ids).each do |acc|
      ::Insurance::RehashAccount.new(insurance_account: acc).execute!
    end
    puts "Done!"
  end

  task :load_insured_loans => :environment do
    file_location = ENV["FILE_LOCATION"]
    puts "Searching file #{file_location}"

    CSV.foreach(file_location, headers: true) do |row|
      pn_number = row['pn_number']
      loan_category = row['loan_category']
      member_id = row['identification_number']
      member = Member.where(identification_number: member_id).first

      loan_insurance_record = LoanInsuranceRecord.where(pn_number: pn_number, loan_category: loan_category, member_id: member.id).first

      date_released = row['date_released']
      maturity_date = row['maturity_date']
      loan_term = row['loan_term']
      loan_amount = row['loan_amount']
      loan_insurance_amount = row['loan_insurance_amount']
      status = row['status']

      if !loan_insurance_record.nil?
        puts "Updating new loan insurance record ..."
        loan_insurance_record.update!(
            date_released: date_released,
            maturity_date: maturity_date,
            loan_term: loan_term,
            loan_amount: loan_amount,
            loan_insurance_amount: loan_insurance_amount,
            status: status
          )
      else
        puts "Creating new loan insurance record ..."
        new_loan_insurance_record = LoanInsuranceRecord.new
        new_loan_insurance_record.member = member
        new_loan_insurance_record.date_released = row['date_released']
        new_loan_insurance_record.maturity_date = row['maturity_date']
        new_loan_insurance_record.loan_term = row['loan_term']
        new_loan_insurance_record.loan_amount = row['loan_amount']
        new_loan_insurance_record.loan_insurance_amount = row['loan_insurance_amount']
        new_loan_insurance_record.pn_number = row['pn_number']
        new_loan_insurance_record.status = row['status']
        new_loan_insurance_record.loan_category = row['loan_category']
        
        new_loan_insurance_record.save!
      end
    end
    puts "Done!"
  end

  task :update_member_insurance_status_by_branch_id => :environment do
    branch_id = ENV['BRANCH_ID']
    puts "Updating member insurance status"

    members = Member.where("branch_id = ? AND status NOT IN (?)", branch_id, ["archived", "pending"]).order("id DESC")
    
    if ENV['CURRENT_DATE'].present?
      current_date = ENV['CURRENT_DATE'].to_date
    else
      current_date = ApplicationHelper.current_working_date.to_date
    end
    
    insurance_account_code = Settings.default_insurance_account_code ||= "LIF"

    insurance_accounts              = InsuranceAccount.joins(:insurance_type).where("insurance_types.code = ? AND member_id IN (?)", insurance_account_code, members.pluck(:id))
    insurance_account_transactions  = InsuranceAccountTransaction.where("amount > 0 AND insurance_account_id IN (?)", insurance_accounts.pluck(:id)).order("transacted_at ASC")

    default_periodic_payment = InsuranceType.where(code: insurance_account_code).first.default_periodic_payment

    size  = members.size

    members.each_with_index do |member, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Validating #{member.id}... #{progress}%%")
      puts "#{member.full_name}"

      #puts "Updating #{member.id} - #{member.full_name}"
      start_date = member.previous_mii_member_since.try(:to_date)

      if start_date.blank?
        insurance_membership_type_name = Settings.insurance_membership_type_name
        member.memberships.each do |membership|
          if membership[:membership_type][:name] == insurance_membership_type_name
            if !membership[:membership_payment].nil?
              if !membership[:membership_payment][:paid_at].nil?
                start_date = membership[:membership_payment][:paid_at].try(:to_date)
              end
            end
          end
        end
      end

      if start_date.present?
        #insurance_account = member.insurance_accounts.joins(:insurance_type).where("insurance_types.code = ?", insurance_account_code).first
        insurance_account = insurance_accounts.select{ |o| o[:member_id] == member.id }.first

        transactions  = insurance_account_transactions.select{ |o| o[:insurance_account_id] == insurance_account.id }
        
        #if insurance_account.insurance_account_transactions.count >= 1
        if transactions.size > 0
#          latest_payment           = insurance_account.insurance_account_transactions.where("insurance_account_id = ? and amount > 0", insurance_account.id).last
#          last_payment_date        = insurance_account.insurance_account_transactions.where("insurance_account_id = ? and amount > 0", insurance_account.id).last.transacted_at.to_date

          latest_payment    = transactions.last
          last_payment_date = transactions.last[:transacted_at].to_date

          current_balance          = latest_payment ? latest_payment.ending_balance.to_i : 0.00

          # Code
          num_days                 = (current_date - start_date).to_i
          num_weeks                = (num_days / 7).to_i
          insured_amount           = num_weeks * default_periodic_payment
          latest_transaction_date  = latest_payment ? latest_payment.transacted_at.to_date : start_date

          num_days_insured         = (latest_transaction_date.to_date  - start_date).to_i
          num_weeks_insured        = (num_days_insured / 7).to_i

          insured_amount           = num_weeks  * default_periodic_payment
          coverage_date            = (start_date + ((current_balance / default_periodic_payment).to_i).weeks).strftime("%B %d, %Y")
          amt_past_due             = (current_balance - insured_amount) * -1
          num_weeks_past_due       = (amt_past_due / default_periodic_payment).to_i

          days_lapsed = (current_date - last_payment_date).to_i

          
          if current_balance == 0.00 && latest_payment.for_resignation == true
            member.update(insurance_status: "resigned")
          elsif current_balance == 0.00
            member.update(insurance_status: "dormant")
          elsif days_lapsed <= 45 && current_balance >= insured_amount
            member.update(insurance_status: "inforce")
          elsif days_lapsed > 45 && current_balance >= insured_amount
            member.update(insurance_status: "inforce")
          elsif days_lapsed <= 45 && current_balance < insured_amount && amt_past_due < 97
            member.update(insurance_status: "inforce")
          elsif days_lapsed <= 45 && current_balance < insured_amount && amt_past_due >= 97
            member.update(insurance_status: "lapsed")  
          elsif days_lapsed > 45 && current_balance < insured_amount && amt_past_due >= 97
            member.update(insurance_status: "lapsed")
          elsif days_lapsed > 45 && current_balance < insured_amount && amt_past_due < 97
            member.update(insurance_status: "inforce")  
          end
        elsif transactions.size == 0
          member.update(insurance_status: "dormant")
        end
      else
        member.update(insurance_status: "pending") 
      end

      if member.member_type == "GK"
        member.update(insurance_status: "resigned")
      elsif member.status == "resigned"
        if member.previous_mii_member_since.nil?
          member.update(insurance_status: "pending")
        else
          member.update(insurance_status: "resigned")
        end
      elsif member.status == "pending"
        member.update(insurance_status: "pending")
      elsif member.status == "archived"
        member.update(insurance_status: "dormant")
      elsif member.status == "cleared"
        member.update(insurance_status: "cleared")
      end
    end
    puts "Done!"
  end

  task :update_member_insurance_status_by_cluster => :environment do
    Branch.where("cluster_id IN (?)", [1,2,3,4,5,6,7,8,9,10]).order("cluster_id ASC").each do |branch|

      puts "Updating member insurance status of #{branch.name}"

      members = Member.where("branch_id = ? AND status NOT IN (?)", branch.id, ["archived", "pending"]).order("id DESC")
      
      current_date = ApplicationHelper.current_working_date.to_date
      
      insurance_account_code = Settings.default_insurance_account_code ||= "LIF"

      insurance_accounts              = InsuranceAccount.joins(:insurance_type).where("insurance_types.code = ? AND member_id IN (?)", insurance_account_code, members.pluck(:id))
      insurance_account_transactions  = InsuranceAccountTransaction.where("amount > 0 AND insurance_account_id IN (?)", insurance_accounts.pluck(:id)).order("transacted_at ASC")

      default_periodic_payment = InsuranceType.where(code: insurance_account_code).first.default_periodic_payment

      size  = members.size

      members.each_with_index do |member, i|
        progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
        printf("\r(#{i+1}/#{size}): Validating #{member.id}... #{progress}%%")
        puts "#{member.full_name}"

        #puts "Updating #{member.id} - #{member.full_name}"
        start_date = member.previous_mii_member_since.try(:to_date)

        if start_date.blank?
          insurance_membership_type_name = Settings.insurance_membership_type_name
          member.memberships.each do |membership|
            if membership[:membership_type][:name] == insurance_membership_type_name
              if !membership[:membership_payment].nil?
                if !membership[:membership_payment][:paid_at].nil?
                  start_date = membership[:membership_payment][:paid_at].try(:to_date)
                end
              end
            end
          end
        end

        if start_date.present?
          insurance_account = insurance_accounts.select{ |o| o[:member_id] == member.id }.first

          transactions  = insurance_account_transactions.select{ |o| o[:insurance_account_id] == insurance_account.id }
          
          if transactions.size > 0
            latest_payment    = transactions.last
            last_payment_date = transactions.last[:transacted_at].to_date

            current_balance          = latest_payment ? latest_payment.ending_balance : 0.00

            # Code
            num_days                 = (current_date - start_date).to_i
            num_weeks                = (num_days / 7).to_i
            insured_amount           = num_weeks * default_periodic_payment
            latest_transaction_date  = latest_payment ? latest_payment.transacted_at.to_date : start_date

            num_days_insured         = (latest_transaction_date.to_date  - start_date).to_i
            num_weeks_insured        = (num_days_insured / 7).to_i

            insured_amount           = num_weeks  * default_periodic_payment
            coverage_date            = (start_date + ((current_balance / default_periodic_payment).to_i).weeks).strftime("%B %d, %Y")
            amt_past_due             = (current_balance - insured_amount) * -1
            num_weeks_past_due       = (amt_past_due / default_periodic_payment).to_i

            days_lapsed = (current_date - last_payment_date).to_i

            
            if current_balance == 0.00 && latest_payment.for_resignation == true
              member.update(insurance_status: "resigned")
            elsif current_balance == 0.00
              member.update(insurance_status: "dormant")
            elsif days_lapsed <= 45 && current_balance >= insured_amount
              member.update(insurance_status: "inforce")
            elsif days_lapsed > 45 && current_balance >= insured_amount
              member.update(insurance_status: "inforce")
            elsif days_lapsed <= 45 && current_balance < insured_amount && amt_past_due < 97
              member.update(insurance_status: "inforce")
            elsif days_lapsed <= 45 && current_balance < insured_amount && amt_past_due >= 97
              member.update(insurance_status: "lapsed")  
            elsif days_lapsed > 45 && current_balance < insured_amount && amt_past_due >= 97
              member.update(insurance_status: "lapsed")
            elsif days_lapsed > 45 && current_balance < insured_amount && amt_past_due < 97
              member.update(insurance_status: "inforce")  
            end
          elsif transactions.size == 0
            member.update(insurance_status: "dormant")
          end
        else
          member.update(insurance_status: "pending") 
        end

        if member.member_type == "GK"
          member.update(insurance_status: "resigned")
        elsif member.status == "resigned"
          if member.previous_mii_member_since.nil?
            member.update(insurance_status: "pending")
          else
            member.update(insurance_status: "resigned")
          end
        elsif member.status == "pending"
          member.update(insurance_status: "pending")
        elsif member.status == "archived"
          member.update(insurance_status: "dormant")
        elsif member.status == "cleared"
          member.update(insurance_status: "cleared")
        end
      end
      puts "Done!"
    end
  end

  task :upload_personal_documents_from_dir => :environment do
    dir_location  = ENV['DIR_LOCATION']
    puts "Searching in directory #{dir_location}"

    Dir["#{dir_location}/*"].each do |f|
      if File.directory? f
        sub_dir_name  = f.split('/').last

        member  = Member.where(identification_number: sub_dir_name).first

        if member
          puts "Found directory for member #{member.full_name}"
          Dir["#{f}/*"].each do |ff|
            if !File.directory? ff
              filename  = ff.split('/').last.split('.').first
              
              attachments = member.member_attachment_files  
              attachment = attachments.where(title: filename).first
              if attachment.nil?
                member_attachment_file  = MemberAttachmentFile.new(
                                            title: filename,
                                            member: member,
                                            file_photo: File.open(ff)
                                          )

                if member_attachment_file.save
                  puts "Successfully uploaded file #{ff} for #{member.identification_number}"
                else
                  puts "Error in attaching file #{ff}"
                end
              else
                attachment.update(
                  title: filename,
                  member: member,
                  file_photo: File.open(ff)
                  )
                puts "Successfully updated file #{ff} for #{member.identification_number}"
              end
            end
          end
        else
          puts "Member #{sub_dir_name} not found"
        end
      end
    end
  end

  task :update_member_name => :environment do
    file_location = ENV['MEMBERS_CSV']
    puts file_location

    CSV.foreach(file_location, headers: true) do |row|
      identification_number = row['identification_number']
      member = Member.where(identification_number: identification_number).first
      first_name = row['first_name'].try(:upcase)
      middle_name = row['middle_name'].try(:upcase)
      last_name = row['last_name'].try(:upcase)

      puts "Updating #{member}"   
      
      if !member.nil?
        member.update!(last_name: last_name, first_name: first_name, middle_name: middle_name)
      end
    end
  end

  task :update_member_date_of_birth => :environment do
    file_location = ENV['MEMBERS_CSV']
    puts file_location

    CSV.foreach(file_location, headers: true) do |row|
      identification_number = row['identification_number']
      member = Member.where(identification_number: identification_number).first
      dob = row['dob']
      
      puts "Updating #{member}"   
      
      if !member.nil?
        member.update!(date_of_birth: dob)
      end
    end
  end

  task :update_member_branch_and_center => :environment do
    file_location = ENV['MEMBERS_CSV']
    puts file_location

    CSV.foreach(file_location, headers: true) do |row|
      identification_number = row['identification_number']
      member = Member.where(identification_number: identification_number).first
      branch_name = row['branch']
      branch = Branch.where(name: branch_name).first
      center_name = row['center'].try(:upcase)
      center = Center.where(name: center_name, branch_id: branch.id).first
      recognition_date = row['recognition_date']

      puts "Updating #{member}"   
      
      if !member.nil?
        member.update!(previous_mii_member_since: recognition_date, branch: branch, center: center)
        member.insurance_accounts.each do |ia|
          ia.update!(center_id: center.id)
        end
      end
    end
  end

  task :upload_centers => :environment do
    file_location = ENV['CENTERS_CSV']
    puts file_location

    CSV.foreach(file_location, headers: true) do |row|
      center_name = row['center'].try(:upcase)
      branch_name = row['branch']
      
      puts "Uploading #{center_name} to #{branch_name}"   

      branch = Branch.where(name: branch_name).first
      bank = Bank.first  
      cluster = Cluster.first
      if branch.nil?
        branch =  Branch.new
        branch.name = row['branch']
        name_branch = row['branch']
        branch_code_abbreviation = name_branch[0..1] + name_branch[-1]
        branch.code = branch_code_abbreviation.upcase
        branch.abbreviation = branch_code_abbreviation.upcase
        branch.address = row['branch']
        branch.cluster = cluster
        branch.bank = bank
        branch.save!
      end
      
      center = Center.where(name: center_name, branch_id: branch.id).first
      if center.nil?
        center = Center.new
        center.name = row['center']
        name_center = row['center']
        center_abbreviation = name_center
        center.abbreviation = center_abbreviation.upcase
        center.address = row['center']
        center.meeting_day = "Monday"
        center.branch = branch
        center.save!
      end
    end
  end

  task :update_transfer_members_data => :environment do
    file_location = ENV['TRANSFER_MEMBERS_CSV']
    puts file_location

    CSV.foreach(file_location, headers: true) do |row|
      if row['meta_id'].present?
        meta_identification_number = row['meta_id']
        meta_id = meta_identification_number["identification_numbers"][0]
        member_record = Member.where(identification_number: meta_id).first

        sss_num = row['sss_number']
        if sss_num.nil?
          sss_number = ''
        else
          sss_number = sss_num
        end

        phil_health_num = row['phil_health_number']
        if phil_health_num.nil?
          phil_health_number = ''
        else
          phil_health_number = phil_health_num
        end

        pag_ibig_num = row['pag_ibig_number']
        if pag_ibig_num.nil?
          pag_ibig_number = ''
        else
          pag_ibig_number = pag_ibig_num
        end

        tin_num = row['tin_number']
        if tin_num.nil?
          tin_number = ''
        else
          tin_number = tin_num
        end

        insurance_status = row['insurance_status']

        if row['recognition_date'].present? && insurance_status.present?  
          if insurance_status == "inforce"
            status = "active"
          elsif insurance_status == "dormant"
            if row['status'] == "archived"
              status = "archived"
            elsif row['status'] == "resigned"
              status = "resigned"
              insurance_status = "resigned"
            else
              status = "dormant"
            end
          elsif row['member_type'] == "GK"
            status = "resigned"
          elsif row['status'] == "archived"
            status == "archived"
          else
            status = insurance_status
          end
        else
          status = "pending"
        end

        branch_name = row['branch']
        branch = Branch.where(name: branch_name).first
        bank = Bank.first  
        cluster = Cluster.first
        if branch.nil?
          branch =  Branch.new
          branch.name = row['branch']
          name_branch = row['branch']
          branch_code_abbreviation = name_branch[0..1] + name_branch[-1]
          branch.code = branch_code_abbreviation.upcase
          branch.abbreviation = branch_code_abbreviation.upcase
          branch.address = row['branch']
          branch.cluster = cluster
          branch.bank = bank
          branch.save!
          branch = branch
        end 

        center_name = row['center'].upcase
        center = Center.where(name: center_name).first
        if center.nil?
          center = Center.new
          center.name = row['center']
          name_center = row['center']
          center_abbreviation = name_center
          center.abbreviation = center_abbreviation.upcase
          center.address = row['center']
          center.meeting_day = "Monday"
          center.branch = branch
          center.save!
          center = center
        end

        member_record.update!(
          identification_number: row['identification_number'],
          uuid: row['uuid'],
          member_type: row['member_type'],
          status: status,
          insurance_status: insurance_status,
          first_name: row['first_name'],
          middle_name: row['middle_name'],
          last_name: row['last_name'],
          previous_mii_member_since: row['recognition_date'],
          date_of_birth: row['date_of_birth'],
          address_street: row['address_street'],
          address_barangay: row['address_barangay'],
          address_city: row['address_city'],
          gender: row['gender'],
          civil_status: row['civil_status'],
          place_of_birth: row['place_of_birth'],
          num_children: row['number_of_children'],
          spouse_first_name: row['spouse_first_name'],
          spouse_last_name: row['spouse_last_name'],
          spouse_middle_name: row['spouse_middle_name'],
          sss_number: sss_number,
          tin_number: tin_number,
          pag_ibig_number: pag_ibig_number,
          phil_health_number: phil_health_number,
          mobile_number: row['cellphone_number'],
          center: center,
          branch: branch
          )
      else
        identification_number = row['identification_number']
        member_record = Member.where(identification_number: identification_number).first

        if member_record.nil?
          member = Member.new
          insurance_status = row['insurance_status']

          member.first_name = row['first_name']
          member.middle_name = row['middle_name']
          member.last_name = row['last_name']
          member.uuid = row['uuid']

          recognition_date = row['recognition_date']
          status = row['status']

          if recognition_date.present? && insurance_status.present?
            member.previous_mii_member_since = recognition_date
            if insurance_status == "inforce"  
              member.status = "active"
              member.insurance_status = insurance_status
            elsif insurance_status == "dormant"
              if status == "archived"
                member.status = "archived"
                member.insurance_status = insurance_status
              elsif status == "resigned"
                member.status = "resigned"
                member.insurance_status = "resigned"
              else
                member.status = "dormant"
                member.insurance_status = insurance_status
              end
            elsif insurance_status == "resigned"
              member.status = "resigned"
              member.insurance_status = insurance_status
            elsif row['member_type'] == "GK"
              member.status = "resigned"
              member.insurance_status = insurance_status
            end
          elsif recognition_date.nil?
            member.previous_mii_member_since = recognition_date
            member.insurance_status = insurance_status
            member.status = "pending"
          end

          birthday = row['date_of_birth']
          if birthday.nil?
            member.date_of_birth = Date.today.to_s
          else
            member.date_of_birth = row['date_of_birth']
          end

          sss_num = row['sss_number']
          if sss_num.nil?
            member.sss_number = ''
          else
            member.sss_number = sss_num
          end

          phil_health_num = row['phil_health_number']
          if phil_health_num.nil?
            member.phil_health_number = ''
          else
            member.phil_health_number = phil_health_num
          end

          pag_ibig_num = row['pag_ibig_number']
          if pag_ibig_num.nil?
            member.pag_ibig_number = ''
          else
            member.pag_ibig_number = pag_ibig_num
          end

          tin_num = row['tin_number']
          if tin_num.nil?
            member.tin_number = ''
          else
            member.tin_number = tin_num
          end
  
          member.place_of_birth = row['place_of_birth']
          member.num_children = row['number_of_children']
          member.spouse_first_name = row['spouse_first_name']
          member.spouse_last_name = row['spouse_last_name']
          member.spouse_middle_name = row['spouse_middle_name']
          member.mobile_number = row['cellphone_number']
          member.address_street = row['address_street']
          member.address_barangay = row['address_barangay']
          member.address_city = row['address_city']
          member.gender = row['gender']
          member.civil_status = row['civil_status']
          

          member_type = row['member_type']
          if member_type.nil?
            member.member_type = "Regular"
          else
            member.member_type = member_type
          end

          branch_name = row['branch']
          branch = Branch.where(name: branch_name).first
          bank = Bank.first  
          cluster = Cluster.first
          if branch.nil?
            branch =  Branch.new
            branch.name = row['branch']
            name_branch = row['branch']
            branch_code_abbreviation = name_branch[0..1] + name_branch[-1]
            branch.code = branch_code_abbreviation.upcase
            branch.abbreviation = branch_code_abbreviation.upcase
            branch.address = row['branch']
            branch.cluster = cluster
            branch.bank = bank
            branch.save!
            member.branch = branch
          else
            member.branch = branch
          end 

          center_name = row['center'].upcase
          center = Center.where(name: center_name).first
          if center.nil?
            center = Center.new
            center.name = row['center']
            name_center = row['center']
            center_abbreviation = name_center
            center.abbreviation = center_abbreviation.upcase
            center.address = row['center']
            center.meeting_day = "Monday"
            center.branch = branch
            center.save!
            member.center = center
          else
            member.center = center
          end

          member.identification_number = row['identification_number']
          
          member.save!
          member.branch.update(member_counter: member.branch.member_counter + 1)
          Members::GenerateMissingAccounts.new(member: member).execute!
        else
          sss_num = row['sss_number']
          if sss_num.nil?
            sss_number = ''
          else
            sss_number = sss_num
          end

          phil_health_num = row['phil_health_number']
          if phil_health_num.nil?
            phil_health_number = ''
          else
            phil_health_number = phil_health_num
          end

          pag_ibig_num = row['pag_ibig_number']
          if pag_ibig_num.nil?
            pag_ibig_number = ''
          else
            pag_ibig_number = pag_ibig_num
          end

          tin_num = row['tin_number']
          if tin_num.nil?
            tin_number = ''
          else
            tin_number = tin_num
          end

          insurance_status = row['insurance_status']

          if row['recognition_date'].present? && insurance_status.present?  
            if insurance_status == "inforce"
              status = "active"
            elsif insurance_status == "dormant"
              if row['status'] == "archived"
                status = "archived"
              elsif row['status'] == "resigned"
                status = "resigned"
                insurance_status = "resigned"
              else
                status = "dormant"
              end
            elsif insurance_status == "resigned"
              status = "resigned"
            elsif row['member_type'] == "GK"
              status = "resigned"
            elsif row['status'] == "archived"
              status == "archived"
            else
              status = insurance_status
            end
          else
            status = "pending"
          end

          branch_name = row['branch']
          branch = Branch.where(name: branch_name).first
          bank = Bank.first  
          cluster = Cluster.first
          if branch.nil?
            branch =  Branch.new
            branch.name = row['branch']
            name_branch = row['branch']
            branch_code_abbreviation = name_branch[0..1] + name_branch[-1]
            branch.code = branch_code_abbreviation.upcase
            branch.abbreviation = branch_code_abbreviation.upcase
            branch.address = row['branch']
            branch.cluster = cluster
            branch.bank = bank
            branch.save!
            branch = branch
          end 

          center_name = row['center'].upcase
          center = Center.where(name: center_name).first
          if center.nil?
            center = Center.new
            center.name = row['center']
            name_center = row['center']
            center_abbreviation = name_center
            center.abbreviation = center_abbreviation.upcase
            center.address = row['center']
            center.meeting_day = "Monday"
            center.branch = branch
            center.save!
            center = center
          end
            
          member_record.update!(
            uuid: row['uuid'],
            member_type: row['member_type'],
            status: status,
            insurance_status: insurance_status,
            first_name: row['first_name'],
            middle_name: row['middle_name'],
            last_name: row['last_name'],
            previous_mii_member_since: row['recognition_date'],
            date_of_birth: row['date_of_birth'],
            address_street: row['address_street'],
            address_barangay: row['address_barangay'],
            address_city: row['address_city'],
            gender: row['gender'],
            civil_status: row['civil_status'],
            place_of_birth: row['place_of_birth'],
            num_children: row['number_of_children'],
            spouse_first_name: row['spouse_first_name'],
            spouse_last_name: row['spouse_last_name'],
            spouse_middle_name: row['spouse_middle_name'],
            sss_number: sss_number,
            tin_number: tin_number,
            pag_ibig_number: pag_ibig_number,
            phil_health_number: phil_health_number,
            mobile_number: row['cellphone_number'],
            center: center,
            branch: branch
            )
        end
      end  
    end
  end

  task :repair_mii_recognition_date => :environment do
    file_location = ENV['MEMBERS_CSV']
    puts file_location

    CSV.foreach(file_location, headers: true) do |row|
      first_name = row['first_name'].try(:upcase)
      middle_name = row['middle_name'].try(:upcase)
      last_name = row['last_name'].try(:upcase)
      recognition_date = row['recognition_date']
      status  = row['status']

      puts "#{first_name} #{middle_name} #{last_name}"

      member = Member.where(first_name: first_name, last_name: last_name).first

      puts member.inspect

      if member
        member.update!(previous_mii_member_since: recognition_date, status: "active")
      else
        member = Member.new
        member.first_name = row['first_name']
        member.middle_name = row['middle_name']
        member.last_name = row['last_name']

        recognition_date = row['recognition_date']
        mii_status = row['mii_status']

        if recognition_date.present? && mii_status.present?
          member.previous_mii_member_since = recognition_date
          member.status = mii_status
        elsif recognition_date.present? 
          member.previous_mii_member_since = recognition_date
          member.status = "active"
        elsif mii_status.present?
          member.previous_mii_member_since = recognition_date
          member.status = mii_status
        end

        birthday = row['date_of_birth']
        if birthday.nil?
          member.date_of_birth = Date.today.to_s
        else
          member.date_of_birth = row['date_of_birth']
        end

        sss_num = row['sss_number']
        if sss_num.nil?
          member.sss_number = ''
        else
          member.sss_number = sss_num
        end

        phil_health_num = row['phil_health_number']
        if phil_health_num.nil?
          member.phil_health_number = ''
        else
          member.phil_health_number = phil_health_num
        end

        pag_ibig_num = row['pag_ibig_number']
        if pag_ibig_num.nil?
          member.pag_ibig_number = ''
        else
          member.pag_ibig_number = pag_ibig_num
        end

        tin_num = row['tin_number']
        if tin_num.nil?
          member.tin_number = ''
        else
          member.tin_number = tin_num
        end

        member.place_of_birth = row['place_of_birth']
        member.num_children = row['number_of_children']
        member.spouse_first_name = row['spouse_first_name']
        member.spouse_last_name = row['spouse_last_name']
        member.spouse_middle_name = row['spouse_middle_name']
        member.mobile_number = row['cellphone_number']
        member.address_street = row['address_street']
        member.address_barangay = row['address_barangay']
        member.address_city = row['address_city']
        member.gender = row['gender']
        member.civil_status = row['civil_status']
        member.member_type = "Regular"

        branch_name = row['branch']
        branch = Branch.where(name: branch_name).first
        bank = Bank.first  
        cluster = Cluster.first
        if branch.nil?
          branch =  Branch.new
          branch.name = row['branch']
          name_branch = row['branch']
          branch_code_abbreviation = name_branch[0..1] + name_branch[-1]
          branch.code = branch_code_abbreviation.upcase
          branch.abbreviation = branch_code_abbreviation.upcase
          branch.address = row['branch']
          branch.cluster = cluster
          branch.bank = bank
          branch.save!
          member.branch = branch
        else
          member.branch = branch
        end 

        center_name = row['center'].upcase
        center = Center.where(name: center_name).first
        if center.nil?
          center = Center.new
          center.name = row['center']
          name_center = row['center']
          center_abbreviation = name_center
          center.abbreviation = center_abbreviation.upcase
          center.address = row['center']
          center.meeting_day = "Monday"
          center.branch = branch
          center.save!
          member.center = center
        else
          member.center = center
        end

        member.identification_number = row['identification_number']
        
        member.save!
        member.branch.update(member_counter: member.branch.member_counter + 1)
        Members::GenerateMissingAccounts.new(member: member).execute!
      end
    end
  end

  task :repair_insurance_account_transaction_deposits_with_nil_voucher_reference_number => :environment do
    invalid_insurance_account_transactions = InsuranceAccountTransaction.where(status: "approved", transaction_type: "deposit", voucher_reference_number: nil)

    insurance_account_ids = []
    invalid_insurance_account_transactions.each do |iat|
      insurance_account = iat.insurance_account

      if insurance_account
        iat.destroy!

        insurance_account_ids << insurance_account.id
      end
    end

    insurance_account_ids = insurance_account_ids.uniq

    insurance_accounts_for_rehash  = InsuranceAccount.where(id: insurance_account_ids)

    insurance_accounts_for_rehash.each do |account|
      ::Insurance::RehashAccount.new(insurance_account: account).execute!
    end
  end

  task :repair_negative_interest_for_amortization => :environment do
    total_interest_repaired   = 0.00
    total_principal_repaired  = 0.00
    data                      = {}
    data[:records]            = []

    AmmortizationScheduleEntry.where("is_void IS NULL AND interest < 0 AND paid = ?", true).each do |ase|
      data[:records]  <<  { loan_product: ase.loan.loan_product.to_s, interest_amount_adjusted: ase.interest.to_f }
      total_interest_repaired += ase.interest
      lpas = LoanPaymentAmortizationScheduleEntry.where(ammortization_schedule_entry_id: ase.id).last
      lpas.loan_payment.update(paid_principal: lpas.loan_payment.paid_principal + ase.interest.abs, paid_interest: lpas.loan_payment.paid_interest - ase.interest.abs)
      ase.update(paid_principal: ase.paid_principal + ase.interest.abs, paid_interest: ase.paid_interest - ase.interest.abs)
    end

    puts "Interest Amounts Adjusted:"
    data[:records].each do |d|
      puts "Loan Product: #{d[:loan_product]} Interest Amount Adjusted: #{d[:interest_amount_adjusted]}"
    end
    puts "Total Interest Repaired: #{total_interest_repaired.to_f}"
  end

  task :update_mii_date_from_memberships => :environment do
    Member.active.each do |member|
      insurance_membership_type_name = Settings.insurance_membership_type_name
      if member.previous_mii_member_since.blank?
        if !member.memberships.nil?
          member.memberships.each do |membership|
            if membership[:membership_type][:name] == insurance_membership_type_name
              if membership[:paid] == true
                member.update(previous_mii_member_since: membership[:membership_payment][:paid_at])
              end
            end
          end
        end
      end
    end
  end

  task :update_mfi_date_from_memberships => :environment do
    Member.active.each do |member|
      microinsurance_membership_type_name = "K-KOOP"
      if member.previous_mfi_member_since.blank?
        if !member.memberships.nil?
          member.memberships.each do |membership|
            if membership[:membership_type][:name] == microinsurance_membership_type_name
              if membership[:paid] == true
                member.update(previous_mfi_member_since: membership[:membership_payment][:paid_at])
              end
            end
          end
        end
      end
    end
  end

  task :update_insured_loans => :environment do
    Loan.all.each do |loan|
      if loan.loan_product.present?
        if loan.loan_product.insured == true
          puts "Updating loan #{loan.id} to be an insured loan"
          # if loan.clip_number.present?
            begin
              loan.update!(is_insured: true) if ["active", "paid", "writeoff"].include? loan.status
            rescue Exception
              puts "Invalid loan: #{loan.id}"
            end
          # end
        end
      end
    end
  end

  task :perform_insurance_payment =>  :environment do
    pc = PaymentCollection.find(ENV['KOINS_PAYMENT_COLLECTION_ID_FOR_CORRECTION'])
    transaction       = CollectionTransaction.find(ENV['ID_TO_CORRECT'])
    amount            = transaction.amount
    member            = transaction.payment_collection_record.member
    insurance_type    = InsuranceType.where(code: transaction.account_type_code).first
    insurance_account = member.insurance_accounts.where(insurance_type_id: insurance_type.id).first

    if amount > 0
      insurance_account_transaction = InsuranceAccountTransaction.create!(
                                        amount: amount,
                                        transacted_at: pc.paid_at,
                                        created_at: pc.paid_at,
                                        transaction_type: "deposit",
                                        particular: pc.particular,
                                        voucher_reference_number: pc.reference_number,
                                        insurance_account: insurance_account
                                      )

      insurance_account_transaction.approve!(pc.approved_by)

      ::Insurance::RehashAccount.new(insurance_account: insurance_account).execute!
    end
  end

  task :perform_deposit_payment =>  :environment do
    pc = PaymentCollection.find(ENV['KOINS_PAYMENT_COLLECTION_ID_FOR_CORRECTION'])
    transaction     = CollectionTransaction.find(ENV['ID_TO_CORRECT'])
    amount          = transaction.amount
    member          = transaction.payment_collection_record.member
    savings_account = member.default_savings_account

    if amount > 0
      savings_account_transaction = SavingsAccountTransaction.create!(
                                      amount: amount,
                                      transacted_at: pc.paid_at,
                                      created_at: pc.paid_at,
                                      transaction_type: "deposit",
                                      particular: pc.particular,
                                      voucher_reference_number: pc.reference_number,
                                      savings_account: savings_account
                                    )

      savings_account_transaction.approve!(pc.approved_by)

      ::Savings::RehashAccount.new(savings_account: savings_account).execute!
    end
  end

  task :perform_withdraw_payment  => :environment do
    # perform_withdraw_payments!
    pc = PaymentCollection.find(ENV['KOINS_PAYMENT_COLLECTION_ID_FOR_CORRECTION'])
    wp_transaction  = CollectionTransaction.find(ENV['ID_TO_CORRECT'])
    amount          = wp_transaction.amount
    member          = wp_transaction.payment_collection_record.member
    savings_account = member.default_savings_account

    if amount > 0
      savings_account_transaction = SavingsAccountTransaction.create!(
                                      amount: amount,
                                      transacted_at: pc.paid_at,
                                      created_at: pc.paid_at,
                                      transaction_type: "wp",
                                      is_withdraw_payment: true,
                                      particular: pc.particular,
                                      voucher_reference_number: pc.reference_number,
                                      savings_account: savings_account
                                    )

      savings_account_transaction.approve!(pc.approved_by)

      ::Savings::RehashAccount.new(savings_account: savings_account).execute!
    end
  end
  
  task :repair_wps_in_collection => :environment do
    pc = PaymentCollection.find(ENV['KOINS_PAYMENT_COLLECTION_ID_FOR_CORRECTION'])

    puts "Performing wp for #{pc.id}..."
    savings_transactions = CollectionTransaction.where(account_type: "WP", payment_collection_record_id: pc.payment_collection_records.pluck(:id))
    savings_transactions.each do |savings_transaction|
      amount            = savings_transaction.amount
      member            = savings_transaction.payment_collection_record.member
      savings_type      = SavingsType.default.first
      savings_account   = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

      if amount > 0
        savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: pc.paid_at,
                                          created_at: pc.paid_at,
                                          transaction_type: "wp",
                                          particular: pc.particular,
                                          voucher_reference_number: pc.reference_number,
                                          savings_account: savings_account
                                        )

        savings_account_transaction.approve!(pc.approved_by)

        ::Savings::RehashAccount.new(savings_account: savings_account).execute!
      end
    end
  end

  task :repair_undeposited_insurance_deposits => :environment do
    pc = PaymentCollection.find(ENV['KOINS_PAYMENT_COLLECTION_ID_FOR_CORRECTION'])

    # perform_insurance_deposit!
    puts "Performing insurance deposit for #{pc.id}..."
    insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: pc.payment_collection_records.pluck(:id))
    insurance_transactions.each do |insurance_transaction|
      amount            = insurance_transaction.amount
      member            = insurance_transaction.payment_collection_record.member
      insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
      insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

      if amount > 0
        insurance_account_transaction = InsuranceAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: pc.paid_at,
                                          created_at: pc.paid_at,
                                          transaction_type: "deposit",
                                          particular: pc.particular,
                                          voucher_reference_number: pc.reference_number,
                                          insurance_account: insurance_account
                                        )

        insurance_account_transaction.approve!(pc.approved_by)

        ::Insurance::RehashAccount.new(insurance_account: insurance_account).execute!
      end
    end
  end

  task :repair_approved_billing_with_unpaid_loan_payments => :environment do
    PaymentCollection.approved_billing.where(id: ENV['KOINS_PAYMENT_COLLECTION_ID'], paid_at: ENV['KOINS_PAID_AT']).each do |pc|
      # approve_loan_payments!
      collection_transaction_ids = []
      pc.payment_collection_records.each do |payment_collection_record|
        payment_collection_record.collection_transactions.each do |ct|
          collection_transaction_ids << ct.id
        end
      end

      puts "Performing loan payments for payment collection #{pc.id}..."
      loan_payments = LoanPayment.where(id: CollectionTransaction.where(account_type: "LOAN_PAYMENT", id: collection_transaction_ids).pluck(:loan_payment_id))
      loan_payments.each do |loan_payment|
        if loan_payment.status == 'pending'
          loan_payment.approve_payment!
        end
      end

      # perform_savings_deposit!
      puts "Performing savings deposits for #{pc.id}..."
      savings_transactions = CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: pc.payment_collection_records.pluck(:id))
      savings_transactions.each do |savings_transaction|
        amount          = savings_transaction.amount
        member          = savings_transaction.payment_collection_record.member
        savings_type    = SavingsType.where(code: savings_transaction.account_type_code).first
        savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: pc.paid_at,
                                          created_at: pc.paid_at,
                                          transaction_type: "deposit",
                                          particular: pc.particular,
                                          voucher_reference_number: pc.reference_number,
                                          savings_account: savings_account
                                        )

          savings_account_transaction.approve!(pc.approved_by)
        end
      end

      # perform_withdraw_payments!
      puts "Performing withdraw payments for #{pc.id}..."
      wp_transactions = CollectionTransaction.where(account_type: "WP", payment_collection_record_id: pc.payment_collection_records.pluck(:id))
      wp_transactions.each do |wp_transaction|
        amount          = wp_transaction.amount
        member          = wp_transaction.payment_collection_record.member
        savings_account = member.default_savings_account

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: pc.paid_at,
                                          created_at: pc.paid_at,
                                          transaction_type: "wp",
                                          is_withdraw_payment: true,
                                          particular: pc.particular,
                                          voucher_reference_number: pc.reference_number,
                                          savings_account: savings_account
                                        )

          savings_account_transaction.approve!(pc.approved_by)
        end
      end

      # perform_insurance_deposit!
      puts "Performing insurance deposit for #{pc.id}..."
      insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: pc.payment_collection_records.pluck(:id))
      insurance_transactions.each do |insurance_transaction|
        amount            = insurance_transaction.amount
        member            = insurance_transaction.payment_collection_record.member
        insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
        insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

        if amount > 0
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: pc.paid_at,
                                            created_at: pc.paid_at,
                                            transaction_type: "deposit",
                                            particular: pc.particular,
                                            voucher_reference_number: pc.reference_number,
                                            insurance_account: insurance_account
                                          )

          insurance_account_transaction.approve!(pc.approved_by)
        end
      end

      # perform_equity_deposit!
      equity_transactions = CollectionTransaction.where(account_type: "EQUITY", payment_collection_record_id: pc.payment_collection_records.pluck(:id))
      equity_transactions.each do |equity_transaction|
        amount          = equity_transaction.amount
        member          = equity_transaction.payment_collection_record.member
        equity_type     = EquityType.where(code: equity_transaction.account_type_code).first
        equity_account  = EquityAccount.where(equity_type_id: equity_type.id, member_id: member.id).first
        
        if amount > 0
          equity_account_transaction = EquityAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: pc.paid_at,
                                          created_at: pc.paid_at,
                                          transaction_type: "deposit",
                                          particular: pc.particular,
                                          voucher_reference_number: pc.reference_number,
                                          equity_account: equity_account
                                        )

          equity_account_transaction.approve!(pc.approved_by)
        end
      end
    end
  end

  task :repair_billing_with_accounting_entries => :environment do
    PaymentCollection.approved_billing.each do |pc|
      voucher = Voucher.where(reference_number: pc.reference_number, book: 'CRB').first

      if voucher
        if pc.paid_at != voucher.date_prepared
          puts "Billing #{pc.id} with date #{pc.paid_at} not aligned with CRB accounting entry #{voucher.reference_number} (#{voucher.id}) with date_prepared #{voucher.date_prepared}"
          puts "Aligning dates..."
          pc.update(paid_at: voucher.date_prepared)
          pc.payment_collection_records.each do |pcr|
            pcr.collection_transactions.each do |ct|
              if ct.account_type == "LOAN_PAYMENT" and ct.loan_payment.amount > 0
                loan_payment = ct.loan_payment
                puts "Updating Loan Payment #{loan_payment.id} paid_at #{loan_payment.paid_at} to #{voucher.date_prepared}"
                query = "UPDATE loan_payments SET paid_at = '#{voucher.date_prepared}' WHERE id = #{loan_payment.id}"
                ActiveRecord::Base.connection.execute(query)
                #loan_payment.update!(paid_at: voucher.date_prepared)
              end
            end
          end
          puts "Done for Billing #{pc.id}..."
        end
      end
    end

    puts "Done..."
  end

  task :repair_amortization_schedule_entries => :environment do
    AmmortizationScheduleEntry.unpaid.each do |ase|
      if ase.is_fully_paid?
        puts "Reparing amortization #{ase.id} Amount: #{ase.amount} Prin. Paid: #{ase.paid_principal} Int. Paid: #{ase.paid_interest} LoanID: #{ase.loan.id}"
        ase.update(paid: true)
      end
    end
  end

  task :soa_funds => :environment do
    savings_types   = SavingsType.all
    insurance_types = InsuranceType.all
    equity_types    = EquityType.all

    start_date      = Settings.fiscal_start_date
    end_date        = Settings.fiscal_end_date

    Member.active.each do |member|
      (start_date..end_date).each do |date|
        if FundTransactionView.where(fiscal_date: date, member_id: member.id).count == 0
          puts "No fund transaction found for member #{member.id} for date #{date}"
          fund_transaction_view = FundTransactionView.new(
                                    fiscal_date: date,
                                    member: member
                                  )

          savings_types.each do |savings_type|
            fund_transaction_view_record = FundTransactionViewRecord.new(
                                            debit: 0,
                                            credit: 0,
                                            account_code: savings_type.code
                                          )

            fund_transaction_view_record.debit = SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id)
                                                  .first.savings_account_transactions
                                                  .approved_withdrawals.where("date(updated_at) = ?", date).sum(:amount)
            fund_transaction_view_record.credit = SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id)
                                                  .first.savings_account_transactions
                                                  .approved_deposits.where("date(updated_at) = ?", date).sum(:amount)

            if fund_transaction_view_record.debit > 0
              puts "Ok debit"
            end

            if fund_transaction_view_record.credit > 0
              puts "Ok credit"
            end

            fund_transaction_view.fund_transaction_view_records << fund_transaction_view_record
          end

          insurance_types.each do |insurance_type|
            fund_transaction_view_record = FundTransactionViewRecord.new(
                                            debit: 0,
                                            credit: 0,
                                            account_code: insurance_type.code
                                          )

            fund_transaction_view_record.debit = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id)
                                                  .first.insurance_account_transactions
                                                  .approved_withdrawals.where("date(updated_at) = ?", date).sum(:amount)
            fund_transaction_view_record.credit = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id)
                                                    .first.insurance_account_transactions.approved_deposits
                                                    .where("date(updated_at) = ?", date).sum(:amount)

            if fund_transaction_view_record.debit > 0
              puts "Ok debit"
            end

            if fund_transaction_view_record.credit > 0
              puts "Ok credit"
            end

            fund_transaction_view.fund_transaction_view_records << fund_transaction_view_record
          end

          equity_types.each do |equity_type|
            fund_transaction_view_record = FundTransactionViewRecord.new(
                                            debit: 0,
                                            credit: 0,
                                            account_code: equity_type.code
                                          )

            fund_transaction_view_record.debit = EquityAccount.where(member_id: member.id, equity_type_id: equity_type.id)
                                                  .first.equity_account_transactions
                                                  .approved_withdrawals.where("date(updated_at) = ?", date).sum(:amount)
            fund_transaction_view_record.credit = EquityAccount.where(member_id: member.id, equity_type_id: equity_type.id)
                                                    .first.equity_account_transactions.approved_deposits
                                                    .where("date(updated_at) = ?", date).sum(:amount)

            if fund_transaction_view_record.debit > 0
              puts "Ok debit"
            end

            if fund_transaction_view_record.credit > 0
              puts "Ok credit"
            end

            fund_transaction_view.fund_transaction_view_records << fund_transaction_view_record
          end

          fund_transaction_view.save!
        end
      end
    end
  end

  task :repair_insurance_account_validations =>  :environment do
    puts "Reparing..."
    insurance_account_validations = InsuranceAccountValidation.all
    insurance_account_validations.each do |insurance_account_validation|
      puts "#{insurance_account_validation.id}"
      insurance_account_validation_records = insurance_account_validation.insurance_account_validation_records
      insurance_account_validation_records.each do |insurance_account_validation_record|
        insurance_account_validation_record.update!(equity_interest: 0.00)
      end

      insurance_account_validation.update!(
                      total_equity_interest: insurance_account_validation.insurance_account_validation_records.sum(:equity_interest),
                      total_rf: insurance_account_validation.insurance_account_validation_records.sum(:rf),
                      total_50_percent_lif: insurance_account_validation.insurance_account_validation_records.sum(:lif_50_percent),
                      total_advance_lif: insurance_account_validation.insurance_account_validation_records.sum(:advance_lif),
                      total_advance_rf: insurance_account_validation.insurance_account_validation_records.sum(:advance_rf),
                      total_interest: insurance_account_validation.insurance_account_validation_records.sum(:interest),
                      total: insurance_account_validation.insurance_account_validation_records.sum(:total)
                      )
      puts "Done!"
    end
  end

  task :update_insurance_account_validation_records_status_and_transaction_number =>  :environment do
    puts "Reparing..."
    insurance_account_validation = InsuranceAccountValidation.find(ENV['INSURANCE_ACCOUNT_VALIDATION_ID'])
    insurance_account_validation_records = insurance_account_validation.insurance_account_validation_records
    insurance_account_validation_records.each do |insurance_account_validation_record|
      transaction_number = insurance_account_validation_record.id.to_s.rjust(6, "0")
      insurance_account_validation_record.update!(status: "approved", transaction_number: transaction_number)
    end
    puts "Done!"
  end

  task :delete_daily_closing => :environment do
    if ::ActiveRecord::Base.connection_config[:adapter] == 'sqlite3'
      puts "Destroying daily_closing_member_repayments..."
      DailyClosingMemberRepayment.destroy_all
      ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'daily_closing_member_repayments'")

      puts "Destroy daily_closing_center_repayments..."
      DailyClosingCenterRepayment.destroy_all
      ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'daily_closing_center_repayments'")

      puts "Destroy daily_closing_so_repayments..."
      DailyClosingSoRepayment.destroy_all
      ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'daily_closing_so_repayments'")

      puts "Destroy daily_closing_branch_repayments..."
      DailyClosingBranchRepayment.destroy_all
      ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'daily_closing_branch_repayments'")
      
      puts "Destroying daily_closings..."
      DailyClosing.destroy_all
      ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'daily_closings'")
    end
  end

  task :insert_child_as_legal_dependent => :environment do
    file_location = ENV['MEMBERS_CSV']
    puts file_location

    CSV.foreach(file_location, headers: true) do |row|
      identification_number = row['identification_number']
      member = Member.where(identification_number: identification_number).first
      record = LegalDependent.where(first_name: row['first_name'], middle_name: row['middle_name'], last_name: row['last_name']).first

      if record.nil?
        legal_dependent = LegalDependent.new
        legal_dependent.first_name = row['first_name']
        legal_dependent.middle_name = row['middle_name']
        legal_dependent.last_name = row['last_name']
        legal_dependent.date_of_birth = row['dob']
        legal_dependent.relationship = 'Child'
        legal_dependent.member_id = member.id

        legal_dependent.save!
      else
        record.update!(date_of_birth: row['dob'])
      end
      puts "Updating dependents of #{identification_number}...#{member}..."
    end
  end

  task :update_member_center => :environment do
    file_location = ENV['MEMBERS_CSV']
    puts file_location

    CSV.foreach(file_location, headers: true) do |row|
      identification_number = row['identification_number']
      member = Member.where(identification_number: identification_number).first
      branch_name = row['branch']
      branch = Branch.where(name: branch_name).first
      center_id = row['center_id']
      center = member.center
      puts "Updating #{identification_number}...#{member}..."   
      
     if center.branch_id == branch.id
          center.update!(uuid: center_id)
      end
    end
  end

  task :void_validation_record_of_balik_kasapi => :environment do
    puts "Updating ..."
    Member.active.each do |member|
      if member.data.with_indifferent_access[:restoration_records].present?
        member_account_validation_record = MemberAccountValidationRecord.where("member_id = ? AND data ->> 'is_void' = ?", member.id, 'false').order("created_at ASC").last
        if !member_account_validation_record.nil?
          puts "Voiding member account validation record of #{member.full_name}"
          member_account_validation_record_data = member_account_validation_record.data.with_indifferent_access
          member_account_validation_record_data[:is_void] = true
          member_account_validation_record.update!(data: member_account_validation_record_data)
        end
      end
    end
    puts "Done"
  end

  task :round_savings_account_balance => :environment do
    puts "Rounding ..."
    
    savings_accounts = SavingsAccount.where("balance > ?", 0)
    
    savings_accounts.each do |sa|
      puts "Updating #{sa}"
      new_balance = sa.balance.round(2)
      sa.update!(balance: new_balance)
    end
  end

  task :insert_hiip_from_withdrawal => :environment do
    puts "Fetching withdrawal collection..."    

    withdrawal_payment_collections = ::Insurance::FetchWithdrawalCollectionForHiip.new().execute!

    if ENV['BRANCH_ID'].present?
      withdrawal_payment_collections = withdrawal_payment_collections.where(branch_id: ENV['BRANCH_ID'])
    end

    values = []

    withdrawal_payment_collections.each do |wpc|
      wpc.payment_collection_records.each do |rec|
        puts "#{rec.member_id}"
        member = Member.find(rec.member_id)
        puts "#{member.full_name}"
        hiip_account = InsuranceAccount.joins(:insurance_type).where("insurance_types.name = ? AND member_id = ? AND status = ?", "Hospital Income Insurance Plan", member.id, "active").first
        puts "#{hiip_account.id}"

        insurance_account_id      = hiip_account.id
        amount                    = rec.total_cp_amount.to_f.round(2)
        transacted_at             = wpc.paid_at
        created_at                = wpc.paid_at
        particular                = wpc.particular
        reference_number          = wpc.reference_number

        insurance_account_transaction = InsuranceAccountTransaction.create!(
                                              amount: amount,
                                              transacted_at: transacted_at,
                                              created_at: created_at,
                                              particular: particular,
                                              transaction_type: "deposit",
                                              voucher_reference_number: reference_number,
                                              insurance_account: hiip_account,
                                              for_resignation: false
                                            )
        
        insurance_account_transaction.approve!(User.last.full_name)
      end
    end
    puts "Done!"
  end

  task :insert_insurance_from_loans => :environment do
    account_subtype     = ENV['ACCOUNT_SUBTYPE']
    accounting_code_id  = ENV['ACCOUNTING_CODE_ID']
    branch              = Branch.find(ENV['BRANCH_ID'])

    puts "Fetching journal entry amounts..."
    cmd = ::Loans::FetchJournalEntries.new(
            config: {
              branch: branch,
              accounting_code_id: accounting_code_id,
              account_subtype: account_subtype
            }
          )

    data  = cmd.execute!

    values  = []

    puts "Inserting records..."
    data[:records].select{ |o| o[:insurance_account_transaction_id].blank? }.each do |r|
      insurance_account_id      = r[:insurance_account_id]
      insurance_account         = InsuranceAccount.find(insurance_account_id)
      transacted_at             = r[:date_approved]
      created_at                = r[:date_approved]
      amount                    = r[:amount].to_f.round(2)

      insurance_account_transaction = InsuranceAccountTransaction.create!(
                                              amount: amount,
                                              transacted_at: transacted_at,
                                              created_at: created_at,
                                              particular: "Deposit CLIP",
                                              transaction_type: "deposit",
                                              voucher_reference_number: r[:reference_number],
                                              insurance_account: insurance_account,
                                              for_resignation: false
                                            )
        
        insurance_account_transaction.approve!(User.last.full_name)
    end

    # puts "Rehashing branch..."
    # ::MemberAccounts::BulkRehash.new(
    #   config: {
    #     branch: branch
    #   },
    #   account_subtype: account_subtype
    # ).execute!

    puts "Done."
  end

  task :update_ev_and_policy_loan_value_for_validation_record => :environment do
    puts "Updating ..."
    insurance_account_validations = InsuranceAccountValidation.all

    insurance_account_validations.each do |insurance_account_validation|
      if insurance_account_validation.total_policy_loan.nil?
        insurance_account_validation.update!(total_policy_loan: 0.00)   
      end

      insurance_account_validation.insurance_account_validation_records.each do |insurance_account_validation_record|
        
        if insurance_account_validation_record.policy_loan.nil?
          insurance_account_validation_record.update!(policy_loan: 0.00)
        end

        if insurance_account_validation_record.equity_value.nil?
          insurance_account_validation_record.update!(equity_value: 0.00)
        end        
      end
    end
      
    puts "Done"
  end
end
  