namespace :loaders do
  task :load_equity_csv => :environment do
    
    filename                = ENV["FILE"]
    total_equity_amount     = ENV["TOTAL_EQUITY_AMOUNT"]
    total_interest_amount   = ENV["TOTAL_INTEREST_AMOUNT"]
    collection_date         = ENV["TOTAL_COLLECION_DATE"]
    equity_rate             = ENV["EQUITY_RATE"]
    savings_account_amount  = ENV["SAVINGS_ACCOUNT_AMOUNT"]
    cbu_account_amount      = ENV["CBU_ACCOUNT_AMOUNT"]
    data      = ::Loaders::EquityLoader.new(
                  filename: filename
                ).execute!
              #raise data.inspect
    # TODO: Save data to model
      #raise data.inspect 
      @data = data
      @interest_on_share_capital_collection = InterestOnShareCapitalCollection.new
      @interest_on_share_capital_collection.total_equity_amount = total_equity_amount
      @interest_on_share_capital_collection.total_interest_amount = total_interest_amount
      @interest_on_share_capital_collection.collection_date = collection_date 
      @interest_on_share_capital_collection.equity_rate = equity_rate
      @interest_on_share_capital_collection.savings_account_amount = savings_account_amount
      @interest_on_share_capital_collection.cbu_account_amount = cbu_account_amount
    
      
      @data[:records].each do |r|
       #raise r.inspect 
        iosccr = InterestOnShareCapitalCollectionRecord.new( 
          data: r
        )
        #raise iosccr.inspect
        @interest_on_share_capital_collection.interest_on_share_capital_collection_records << iosccr
        
      end

      @interest_on_share_capital_collection.save!
      puts "done"
  end

  task :load_patronage_csv => :environment do
    filename =  ENV["FILE"]


    total_loan_interest_amount  = ENV["TOTAL_LOAN_INTEREST_AMOUNT"]
    total_loan_interest_rate    = ENV["TOTAL_INTEREST_RATE_AMOUNT"]
    patronage_rate              = ENV["PATROANGE_RATE"]
    as_of                       = ENV["AS_OF"]
    total_savings_interest      = ENV["TOTAL_SAVINGS_INTEREST"]
    total_loan_interest_rate    = ENV["TOTAL_INTEREST_RATE_AMOUNT"]
    total_cbu_interest          = ENV["TOTAL_CBU_INTEREST"]


    #raise filename.inspect

    data      = ::Loaders::PatronageLoader.new(
                  filename: filename
                ).execute!

    #@data = data
    @data = data

#    total_loan_interest_amount = 100
#    total_loan_interest_rate = 200
#    patronage_rate = 1.0
#    as_of = "2016-12-31"
    @patronage_rate = patronage_rate
    @patronage_refund_collection = PatronageRefundCollection.new
    @patronage_refund_collection.total_loan_interest_amount =  total_loan_interest_amount
    @patronage_refund_collection.total_interest_amount = total_loan_interest_rate
    @patronage_refund_collection.patronage_rate = patronage_rate
    @patronage_refund_collection.as_of = as_of

    @patronage_refund_collection.total_savings_amount  = total_savings_interest
    @patronage_refund_collection.total_cbu_amount      = total_cbu_interest
    #raise @patronage_refund_collection.inspect
    #@data[:records].each do |r|
    @data[:records].each do |r|
      #raise r.inspect
      prcr = PatronageRefundCollectionRecord.new(
        data: r
      ) 
      @patronage_refund_collection.patronage_refund_collection_records << prcr
    end
    @patronage_refund_collection.save
  end
end
