namespace :legacy do
  task :save_loan_payments => :environment do
    puts "Building loan payments..."
    data  = ::Builders::BuildLoanPayments.new.execute!

    filename  = "loan-payments-#{Time.now.to_i.to_s}.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    puts "Saving file to #{full_path}..."

    File.write(full_path, data.to_json)

    puts "Done!"
  end

  task :save_loans => :environment do
    puts "Building loans..."
    data  = ::Builders::BuildLoans.new.execute!

    filename  = "loans-#{Time.now.to_i.to_s}.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    puts "Saving file to #{full_path}..."

    File.write(full_path, data.to_json)

    puts "Done!"
  end

  task :save_membership_payments => :environment do
    puts "Building membership payments..."
    data  = ::Builders::BuildMembershipPayments.new.execute!
    
    filename  = "membership-payments-#{Time.now.to_i.to_s}.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    puts "Saving file to #{full_path}..."

    File.write(full_path, data.to_json)

    puts "Done!"
  end

  task :save_account_transactions => :environment do
    puts "Building account transactions..."
    data  = ::Builders::BuildAccountTransactions.new.execute!

    filename  = "account-transactions-#{Time.now.to_i.to_s}.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    puts "Saving file to #{full_path}..."

    File.write(full_path, data.to_json)

    puts "Done!"
  end

  task :save_member_accounts => :environment do
    puts "Building member accounts..."
    data  = ::Builders::BuildMemberAccounts.new.execute!

    filename  = "members-accounts-#{Time.now.to_i.to_s}.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    puts "Saving file to #{full_path}..."

    File.write(full_path, data.to_json)

    puts "Done!"
  end

  task :save_accounting_entries => :environment do
    puts "Building accounting entries..."
    data  = ::Builders::BuildAccountingEntries.new.execute!

    filename  = "accounting-entries-#{Time.now.to_i.to_s}.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    puts "Saving file to #{full_path}..."

    File.write(full_path, data.to_json)

    puts "Done!"
  end

  task :save_members => :environment do
    puts "Building member records..."
    data  = ::Builders::BuildMembers.new.execute!

    filename  = "members-#{Time.now.to_i.to_s}.json"
    full_path = "#{Rails.root}/db_backup/#{filename}"

    puts "Saving file to #{full_path}..."

    File.write(full_path, data.to_json)

    puts "Done!"
  end
end
