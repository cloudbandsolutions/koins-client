namespace :closing do
  task :generate_daily_report => :environment do
    Reports::GenerateDailyReport.new(as_of: nil).execute!
  end

  task :generate_monthly_trial_balance => :environment do
    puts "Generating monthly trial balance for month #{Date.today.month}..."
    Closing::GenerateMonthlyTrialBalance.new(year: Date.today.year, month: Date.today.month, branch: Branch.first).execute!
    puts "Done..."
  end
end
