namespace :repair do
  task :repair_loan_payment => :environment do
    lp = LoanPayment.approved.where("loan_payment_amount > 0 AND is_void IS NULL")
    size = lp.size
    lp.each_with_index do |l, i|
      progress  = (((i + 1).to_f / size.to_f) * 100).round(2)
      printf("\r(#{i+1}/#{size}): Loan #{l.loan_id}... #{progress}%%")
      total_amount = l.paid_principal + l.paid_interest
      LoanPayment.find(l.id).update(loan_payment_amount: total_amount)
      
    end
  end
  task :repair_project_type_uuid => :environment do 
    file_location = ENV['FILENAME']
    puts file_location
    CSV.foreach(file_location, headers: true) do |row|
      pt_id = row['pt_id']
      pt_name = row['project_type']
      pt_uuid = row['pt_uuid']
      ProjectType.find(pt_id).update(uuid: pt_uuid)

      puts pt_name
    end
  end

  task :disbalance_account => :environment do
    sav = SavingsAccount.where("status IN (?) and balance != '0.0'", ['resigned','active']).pluck(:id).uniq
    sav.each do |a|
      savb = SavingsAccount.find(a).balance
      savc = SavingsAccountTransaction.where(savings_account_id: a).last.ending_balance
      if savb != savc
        puts "#{a} | #{savb} | #{savc}"
      end
    end
  end



 task :BagongSilang => :environment do
  sav = SavingsAccount.where("status IN (?) and balance != '0.0'", ['resigned','active']).pluck(:id).uniq
  sav.each do |a|
    savb = SavingsAccount.find(a).balance
    savc = SavingsAccountTransaction.where(savings_account_id: a).last.ending_balance
    if savb != savc
      puts "#{a} | #{savb} | #{savc}"
    end
  end
 end
 
 task :doble_savings => :environment do
  smp = SavingsAccountTransaction.where("transaction_type = 'withdraw' and voucher_reference_number = '0000006864' and transaction_date = '2019-06-11'")
  smp.each do |x|
    sad = SavingsAccount.where(id: x.savings_account_id)
    #SavingsAccountTransaction.find(x).delete
    sad.each do |s|
      ::Savings::RehashAccount.new(savings_account: s).execute!
      puts s
    end
  end
 end

 task :savings => :environment do
  sa = SavingsAccountTransaction.where(transaction_date: "2019-06-07").pluck(:savings_account_id).uniq
  sa.each do |x|
    sab =SavingsAccount.find(x).balance
    sav = SavingsAccountTransaction.where(savings_account_id: x).last.ending_balance

    if sab != sav
      puts x , sav , sab
    end
 end
 end
 
 task :savings_account_rep => :environment do
  sa = SavingsAccount.where("savings_type_id = ? and status = 'active' and balance != 0" , 1)
  sa.each do |x|
    xa = SavingsAccountTransaction.where(savings_account_id: x).last.ending_balance
    xas = SavingsAccountTransaction.where(savings_accounit_id: x).last.amount
    xad = SavingsAccountTransaction.where(savings_account_id: x).last.transaction_date
    xat = SavingsAccountTransaction.where(savings_account_id: x).last.transaction_type

    if xad == '2019-06-11'
      puts "#{x.member.full_name} | #{x.balance} | #{xa} | #{xas} | #{xad} | #{xat}"
    end
  end
 end

 task :repair_validation => :environment do
    val_id = ENV["VAL_ID"]
    InsuranceAccountValidationRecord.where(insurance_account_validation_id: val_id).each do |x|
      mem_id = x.member_id
      sav_type_id = 1
      sav_acct_id = SavingsAccount.where(member_id: mem_id , savings_type_id: 1)
      lif_id = InsuranceAccount.where(member_id: mem_id , insurance_type_id: 1)
      lif_amount = (x.lif_50_percent * 2) + x.advance_lif
      rf_id = InsuranceAccount.where(member_id: mem_id , insurance_type_id: 2)
      rf_amount = x.rf + x.advance_rf
      tot_amount = x.total
      ref_no = InsuranceAccountValidation.find(val_id).reference_number
      part = Voucher.where(reference_number: ref_no).last.particular
      used_date = InsuranceAccountValidation.find(val_id).date_approved
      banko = Branch.last.bank_id
      acct_code_id = Bank.find(banko).accounting_code_id

      puts mem_id

      rf_id.each do |c|
        InsuranceAccountTransaction.create!(insurance_account_id: c.id , amount: rf_amount , transaction_type: 'withdraw' , particular: 'part' , status: 'approved' , voucher_reference_number: ref_no , bank_id: banko , accounting_code_id: acct_code_id , for_resignation: 'true' , transacted_at: used_date , created_at: used_date , updated_at: used_date , transaction_date: used_date)      
        ::Insurance::RehashAccount.new(insurance_account: c).execute! 

        puts c.id
      end

      lif_id.each do |b|
        InsuranceAccountTransaction.create!(insurance_account_id: b.id , amount: lif_amount , transaction_type: 'withdraw' , particular: 'part' , status: 'approved' , voucher_reference_number: ref_no , bank_id: banko , accounting_code_id: acct_code_id , for_resignation: 'true' , transacted_at: used_date , created_at: used_date , updated_at: used_date , transaction_date: used_date)      
        ::Insurance::RehashAccount.new(insurance_account: b).execute! 
      
        puts b.id
      end

      sav_acct_id.each do |a|   
        SavingsAccountTransaction.create!(savings_account_id: a.id , savings_type_id: sav_type_id , amount: tot_amount , voucher_reference_number: ref_no , particular: part , for_resignation: 'true' , status: 'approved' , transaction_type: 'deposit' , transaction_date: used_date , created_at: used_date , updated_at: used_date , transacted_at: used_date , member_id: mem_id , bank_id: banko , accounting_code_id: acct_code_id)  
      
        ::Savings::RehashAccount.new(savings_account: a).execute!

        puts a.id
      end

    end
  end

  task :transfer_maintaining_balance => :environment do
   #center_id = ENV["CENTER_ID"]
   loan_details = Loan.where(center_id: 36, status: "active")
   loan_product = loan_details.pluck(:loan_product_id).uniq
   raise loan_product.inspect
  end
  task :loader_csv => :environment do
    filename = ENV["FILE"]
    @data = {}
    @data[:records] = []
    csv_text  = File.read(filename)
    csv       = CSV.parse(csv_text, headers: true)
    

    csv.each do |row|
      r = {
      first_id:  row["id"],
      second_id: row["loanproduct"]
      }
      @data[:records] << r
      
    end
    @data
  
  end

    
  task :repair_membership_fee => :environment do 
    invalid_member  = []
    member = Member.where(status: "active")
    member.each do |m|
      c = MembershipPayment.where(membership_type_id: 1, member_id: m.id, status: "paid").count
      if c == 0
       loan =  Loan.where(member_id: m.id, loan_product_id: 14).first
       MembershipPayment.create!(membership_type_id: 1, member_id: m.id, paid_at: loan.date_approved, status: "paid", loan_id: loan.id ) 
       invalid_member << m.id 
      end
    
    end
    puts invalid_member
  end

  task :remove_payment => :environment do
    #loan_payment_id = [93173,94171,95595,96768,97920,99440,100974,102293,103675,105073,106242,108531,109405]
    loan_payment_id = ENV['ID']
    loan_payment_id.each do |lpi|
      LoanPayment.find(lpi).update(is_void: true)
      lpase = LoanPaymentAmortizationScheduleEntry.where(loan_payment_id: lpi)
      lpase.each do |lpse_amort|
        LoanPaymentAmortizationScheduleEntry.find(lpse_amort.id).update(principal_paid: 0.0, interest_paid: 0.0)
        AmmortizationScheduleEntry.find(lpse_amort.ammortization_schedule_entry_id).update(paid: "false", paid_principal: 0.0, paid_interest: 0.0)
      end
    end
    puts "done"
  end




  task :repair_balik_kasapi_share => :environment do
    Member.where("resignation_type <> ''  and status = 'active'").each do |ms|
      MemberShare.where("member_id = ?", ms.id).update(is_void: true)
    end
  end

  task :repair_maintaining_balance => :environment do
    loan_product_id = ENV["LOAN_ID"]
    Loan.where("loan_product_id = ? and status= 'active' and amount >= 10001 and maintaining_balance_val = 0" , loan_product_id).each do |a|
      loan_id = a.id
    
      m_balance = (a.amount * 0.20).to_f

      Loan.find(loan_id).update(override_maintaining_balance: 'false', maintaining_balance_val: m_balance)
      
      puts a.voucher_payee
    end 
  end

  task :repair_maintaining_balance_below => :environment do
    loan_product_id = ENV["LOAN_ID"]
    Loan.where("loan_product_id = ? and status= 'active' and amount <= 10000 and maintaining_balance_val >= 1" , loan_product_id).each do |a|
      loan_id = a.id

      Loan.find(loan_id).update(maintaining_balance_val: 0)
      
      puts a.voucher_payee
    end 
  end


  task :insert_missing_patronage_refunds => :environment do
    patronage_refund_collection = PatronageRefundCollection.find(ENV["ID"])
    voucher                     = Voucher.where(book: "JVB", reference_number: patronage_refund_collection.reference_number).first
    approved_by                 = voucher.approved_by

    total_inserted_savings  = 0.00
    total_inserted_cbu      = 0.00

    patronage_refund_collection.patronage_refund_collection_records.each do |o|
      savings_account = SavingsAccount.where(member_id: o.data["member_id"], savings_type_id: 1).first
      cbu_account     = SavingsAccount.where(member_id: o.data["member_id"], savings_type_id: 3).first

      if o.data["member_loan_interest_rate"].to_i == 0
        savings_account_transaction = SavingsAccountTransaction.create!(
                                        amount: (o.data["dtotal_savings"].to_f).round(2),
                                        transacted_at: patronage_refund_collection.posting_date,
                                        created_at: patronage_refund_collection.posting_date,
                                        transaction_type: "interest",
                                        particular: voucher.particular,
                                        voucher_reference_number: patronage_refund_collection.reference_number,
                                        savings_account_id: savings_account.id
                                      )

        total_inserted_savings += (o.data["dtotal_savings"].to_f).round(2)

        savings_account_transaction.approve!(approved_by)

        savings_account_transaction = SavingsAccountTransaction.create!(
                                        amount: (o.data["dtotal_cbu"].to_f).round(2),
                                        transacted_at: patronage_refund_collection.posting_date,
                                        created_at: patronage_refund_collection.posting_date,
                                        transaction_type: "interest",
                                        particular: voucher.particular,
                                        voucher_reference_number: patronage_refund_collection.reference_number,
                                        savings_account_id: cbu_account.id
                                      )

        total_inserted_cbu += (o.data["dtotal_cbu"].to_f).round(2)

        savings_account_transaction.approve!(approved_by)

        # Rehash account
        ::Savings::RehashAccount.new(savings_account: savings_account).execute!
      end
    end

    puts "Total Inserted Savings: #{total_inserted_savings}"
    puts "Total Inserted CBU: #{total_inserted_cbu}"
  end

  task :repair_decimal => :environment do
    sa = []
    second_array = []
    sa = SavingsAccount.where("savings_type_id <> 2")

    sa.each do |sad|
      if sad.balance.to_s.split(".").last.size > 2
        second_array << {id: sad.id, balance: sad.balance }
      end
    end
    
    second_array_id = second_array.map{ |a| a[:id]  }    
    
    second_array_id.each do |sai|
      puts sai
      sat = SavingsAccountTransaction.where(savings_account_id: sai, status: "approved")
      sat.each do |satdetails|
        SavingsAccountTransaction.find(satdetails.id).update(amount: (satdetails.amount).round(2))
      end

      ::Savings::RehashAccount.new(savings_account: SavingsAccount.find(sai)).execute!
    end
    
    

    puts "Done"

  end



  task :repair_transaction_dates_for_savings => :environment do
    no_transaction_date_sats  = SavingsAccountTransaction.approved.where(transaction_date: nil)

    no_transaction_date_sats.each do |sat|
      ActiveRecord::Base.connection.execute("
        UPDATE savings_account_transaction SET amount = #{correct_amount}, amount_released = #{cb_amount} WHERE id = #{loan.id}
      ")
    end
  end

  task :savings_transaction_from_collection => :environment do
    savings_type            = SavingsType.find(ENV['SAVINGS_TYPE_ID'])
    pc                      = PaymentCollection.find(ENV['PAYMENT_COLLECTION_ID'])
    members                 = Member.where(id: pc.payment_collection_records.pluck(:member_id).uniq)
    collection_transactions = CollectionTransaction.where(payment_collection_record_id: pc.payment_collection_records.pluck(:id).uniq, account_type: ENV['ACCOUNT_TYPE'])

    if !pc.approved?
      raise "Collection is not approved"
    end

    counter = 0
    total   = 0
    collection_transactions.each do |ct|
      member  = ct.payment_collection_record.member
      savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

      if !savings_account
        raise "Invalid savings accout"
      end

      if ct.amount > 0
        found = false

        if ENV['ACCOUNT_TYPE']  ==  'WP'
          savings_account.savings_account_transactions.where(transaction_type: 'wp', status: 'approved').each do |sat|
            if sat.created_at.strftime("%Y-%m-%d")  ==  pc.paid_at.strftime("%Y-%m-%d")
              found = true
            end
          end
        elsif ENV['ACCOUNT_TYPE'] == 'SAVINGS'
          savings_account.savings_account_transactions.where(transaction_type: 'deposit', status: 'approved').each do |sat|
            if sat.created_at.strftime("%Y-%m-%d")  ==  pc.paid_at.strftime("%Y-%m-%d")
              found = true
            end
          end
        end

        if !found
          counter += 1
          total += ct.amount
          puts "#{counter}: Did not find transaction #{ENV['ACCOUNT_TYPE']} worth #{ct.amount.to_f} for member #{member.full_name} (#{member.id}) savings account #{savings_account.id} dated #{pc.paid_at.strftime("%Y-%m-%d")}"
          puts "Repairing..."
          transaction_type  = nil
          if ENV['ACCOUNT_TYPE'] == 'WP'
            transaction_type = 'wp' 
          elsif ENV['ACCOUNT_TYPE'] == 'SAVINGS'
            transaction_type = 'deposit'
          else
            raise "Invalid ACCOUNT_TYPE"
          end
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: ct.amount,
                                          transacted_at: pc.paid_at,
                                          created_at: pc.paid_at,
                                          transaction_type: transaction_type,
                                          particular: pc.particular,
                                          voucher_reference_number: pc.reference_number,
                                          savings_account: savings_account
                                        )

          savings_account_transaction.approve!(pc.approved_by)

          sa  = SavingsAccount.find(savings_account.id)
          ::Savings::RehashAccount.new(savings_account: sa).execute!

          puts "Done!"
        end
      end
    end

    puts "TOTAL: #{total}"
  end

end
