namespace :sanitizers do
  task :sanitize_savings_account_transactions => :environment do
    SavingsAccountTransaction.all.each do |savings_account_transaction|
      ::Sanitizers::SanitizeSavingsAccountTransaction.new(
        savings_account_transaction: savings_account_transaction
      ).execute!
    end
  end

  task :sanitize_insurance_account_transactions => :environment do
    InsuranceAccountTransaction.all.each do |insurance_account_transaction|
      ::Sanitizers::SanitizeInsuranceAccountTransaction.new(
        insurance_account_transaction: insurance_account_transaction
      ).execute!
    end
  end

  task :sanitize_equity_account_transactions => :environment do
    EquityAccountTransaction.all.each do |equity_account_transaction|
      ::Sanitizers::SanitizeEquityAccountTransaction.new(
        equity_account_transaction: equity_account_transaction
      ).execute!
    end
  end

  task :sanitize_vouchers => :environment do
    Voucher.all.each do |voucher|
      ::Sanitizers::SanitizeVoucher.new(
        voucher: voucher
      ).execute!
    end
  end
end
