namespace :db do
  task :save_member_share => :environment do
    member_shares = MemberShare.joins(:member).where("members.status = 'active' and is_void IS NULL and members.center_id = ?" , ENV['center'])
    filename  = "member_share-v2.json"
    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      member_shares: []
    }

    member_shares.each do |o|
      data[:member_shares] << o.to_transfer_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
 
  end

  task :load_save_member_share => :environment do
    data  = JSON.parse(File.read(ENV['FILENAME'])).deep_symbolize_keys!
    data = data[:member_shares]
    data.each do |d|
      x = Member.where("uuid = ?" , d[:member_id])
      x.each do |y|
        MemberShare.create!(
          uuid: d[:uuid],
          member_id: y.id,
          certificate_number: d[:certificate_number],
          number_of_shares: d[:number_of_shares],
          printed: d[:printed],
          date_printed: d[:date_printed],
          date_of_issue: d[:date_of_issue],
          is_void: d[:is_void]
        )
      end
    end
    puts "Done"
  end



  task :save_membership_payment => :environment do
    membership_payments = MembershipPayment.where(status: ["paid"])

    if ENV['DATE_OFFSET'].present?
      membership_payments = membership_payments.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    filename  = "mebership_payment-v2.json"
    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      membership_payments: []
    }

    membership_payments.each do |o|
      data[:membership_payments] << o.to_version_2_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end

  task :save_members => :environment do
    if ENV['BRANCH_ID'].present?
      branch = Branch.find(ENV['BRANCH_ID'])
      members = Member.where(branch_id: branch.id)
      filename  = "#{branch}-members-v2.json" 
    else
      members   = Member.all
      filename  = "members-v2.json"
    end

    if ENV['DATE_OFFSET'].present?
      members = members.where("DATE(created_at) >= ?", ENV['DATE_OFFSET'].to_date)
    end

    if ENV['FILENAME'].present?
      filename  = ENV['FILENAME']
    end

    full_path = "#{Rails.root}/db_backup/#{filename}"

    data  = {
      members: []
    }

    members.each do |o|
      data[:members] << o.to_membership_date_hash
    end

    puts "Saving file to #{full_path}..."

    File.write(full_path, JSON.pretty_generate(data))

    puts "Done!"
  end




  task :generate_loan_with_unpaid_amort => :environment do
    filename= ENV['Filename']
    data    = ::Loans::BuildLoanWithUnpaidAmort.new(
                filename: filename 
              ).execute!

    puts "Generating unpaid amort for loan"
    File.open("#{Rails.root}/tmp/unpaid-amort.json", "w") do |f|
      f.write(JSON.pretty_generate(data))
    end
    puts "Done."
  end


  task :load_loan_with_unpaid_amort => :environment do
    data  = JSON.parse(File.read(ENV['FILENAME'])).deep_symbolize_keys!
    data = data[:loans]
    data.each do |d|
      loan  = Loan.find(d[:id])
      loan.ammortization_schedule_entries.delete_all
      d[:ammortization_schedule_entries].each do |ase|
        AmmortizationScheduleEntry.create!(
          loan: loan,
          amount: ase[:amount],
          principal: ase[:principal],
          interest: ase[:interest],
          due_at: ase[:due_at],
          paid_principal: 0.00,
          paid_interest: 0.00
        )
      end

      # update remaining balance
      loan  = Loan.find(d[:id])
      principal_balance = loan.ammortization_schedule_entries.sum(:principal)
      interest_balance  = loan.ammortization_schedule_entries.sum(:interest)
      remaining_balance = (principal_balance + interest_balance).round(2)

      loan.update!(
        remaining_balance: remaining_balance
      )

    end
    puts "Done"
  end




  desc "Adjust centers for batch transactions"
  task :adjust_centers_for_batch_transactions => :environment do
    puts "Adjusting batch transactions..."

    BatchTransaction.where(center_id: nil).each do |bt|
      puts "Adjust BT id: #{bt.id}" 
      center = bt.savings_account_transactions.first.savings_account.member.center
      bt.update!(center_id: center.id)
    end
  end

  desc "Adjust vouchers"
  task :adjust_vouchers => :environment do
    puts "Adjusting vouchers..."

    Loan.active.each do |loan|
      voucher = Voucher.where(reference_number: loan.voucher_reference_number, branch_id: loan.branch.id).first

      if !voucher.nil?
        voucher.name_of_receiver = loan.member.to_s
        voucher.date_posted = voucher.date_prepared

        # Voucher Parameter: Name on Check
        v_name_on_check = VoucherOption.new
        v_name_on_check.name = "Name on Check"
        v_name_on_check.val = loan.member.check_name

        voucher.voucher_options << v_name_on_check

        # Voucher Parameter: Bank Check Number
        v_bank_check_number = VoucherOption.new
        v_bank_check_number.name = "Bank Check Number"
        v_bank_check_number.val = loan.bank_check_number

        voucher.voucher_options << v_bank_check_number

        # Voucher Parameter: Check Voucher Number
        v_check_voucher_number = VoucherOption.new
        v_check_voucher_number.name = "Check Voucher Number"
        v_check_voucher_number.val = loan.voucher_check_number

        voucher.voucher_options << v_check_voucher_number

        # Voucher Parameter: Check Amount
        v_check_amount = VoucherOption.new
        v_check_amount.name = "Check Amount"
        v_check_amount.val = loan.amount_released

        voucher.voucher_options << v_check_amount

        if voucher.save
          puts "Successfully adjusted voucher #{voucher.reference_number}"
        else
          puts "Error in adjusting voucher #{voucher.reference_number}"
        end
      end
    end
  end

  desc "Backup database"
  task :backup => :environment do
    puts "Backing up database..."

    if ::ActiveRecord::Base.connection_config[:adapter] == 'sqlite3'
      puts "Backing up sqlite3 database..."
      destination_directory = "#{Rails.root}/db_backup"
      destination_file = "#{destination_directory}/#{Settings.system_name}-#{Time.now.to_i.to_s}.sqlite3"
      database_file = "#{Rails.root}/db/#{ENV['RAILS_ENV']}.sqlite3"

      puts "Copying database #{database_file} to #{destination_file}"
      FileUtils.cp_r(database_file, destination_file)

      puts "Done..."
    elsif ::ActiveRecord::Base.connection_config[:adapter] == 'postgresql'
      puts "Backing up postgresql database..."
      destination_directory = "#{Rails.root}/db_backup"
      destination_file = "#{destination_directory}/#{Settings.system_name}-#{Time.now.to_i.to_s}-#{ENV['RAILS_ENV']}.dump"
      cmd = nil
      with_config do |app, host, db, user, pw|
        cmd = "PGPASSWORD=#{pw} pg_dump --host #{host} --username #{user} --verbose --clean --no-owner --no-acl --format=c #{db} > #{destination_file}"
      end
      puts cmd
      exec cmd
    else
      puts "Invalid database adapter"
    end
  end

  desc "Restore database"
  task :restore => :environment do
    puts "Restoring database..."

    if ::ActiveRecord::Base.connection_config[:adapter] == 'postgresql'
      cmd = nil
      with_config do |app, host, db, user, pw|
        cmd = "PGPASSWORD=#{pw} pg_restore --verbose --host #{host} --username #{user} --clean --no-owner --no-acl --dbname #{db} #{ENV['PG_BACKUP_DUMP']}"
      end
      Rake::Task["db:drop"].invoke
      Rake::Task["db:create"].invoke
      puts cmd
      exec cmd
    else
      puts "Invalid database adapter"
    end
  end

  private

  def with_config
    yield Rails.application.class.parent_name.underscore,
      ActiveRecord::Base.connection_config[:host],
      ActiveRecord::Base.connection_config[:database],
      ActiveRecord::Base.connection_config[:username],
      ActiveRecord::Base.connection_config[:password]
  end
end

namespace :db do
  desc "Clean accounting codes"
  task :clean_accounting_codes => :environment do
    MotherAccountingCode.all.each do |m|
      m.code = "#{m.major_account.major_group.code}#{m.major_account.code}#{m.subcode}"
      m.save!
    end

    AccountingCode.all.each do |a|
      a.main_code = "#{a.mother_accounting_code.code}"
      a.code = "#{a.main_code}#{a.sub_code}"
      a.save!
    end
  end
end

namespace :db do
  namespace :seed do
    Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].each do |filename|
      task_name = File.basename(filename, '.rb').intern
      task task_name => :environment do
        load(filename) if File.exists?(filename)
      end
    end
  end
end

namespace :db do

  desc "Update remaining balances for all loans"
  task :update_remaining_balances => :environment do
    puts "Updating remaining balances for loans..."
    Loan.all.each do |loan|
      puts "Updating loan #{loan.id}"
      total = 0
      loan.ammortization_schedule_entries.unpaid.each do |ase|
        total += ase.remaining_balance
      end

      loan.update!(remaining_balance: total)
    end
  end

  desc "Create missing savings and insurance accounts"
  task :create_missing_savings_and_insurance_accounts => :environment do
    puts "Querying all members..."
    Member.active.each do |member|
      puts "Creating savings accounts..."
      SavingsType.where(is_default: true).each do |savings_type|
        if SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id).count == 0
          puts "Creating #{savings_type} savings account for #{member}..."
          SavingsAccount.create!(
                          account_number: "#{SecureRandom.hex(4)}",
                          member: member,
                          balance: 0,
                          savings_type: savings_type
                        )
        end
      end

      puts "Creating insurance accounts..."
      InsuranceType.where(is_default: true).each do |insurance_type|
        if InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).count == 0
          puts "Creating #{insurance_type} insurance account for #{member}..."
          InsuranceAccount.create!(
                            account_number: "#{SecureRandom.hex(4)}",
                            member: member,
                            balance: 0,
                            insurance_type: insurance_type
                          )
        end
      end
    end
  end
end
