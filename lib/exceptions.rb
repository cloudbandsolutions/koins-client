module Exceptions
  class NoGlobalSettingsFoundError < StandardError; end
  class InvalidSavingsAccountTransactionTypeError < StandardError; end
  class BranchNotFoundError < StandardError; end
  class CenterNotFoundError < StandardError; end
end
