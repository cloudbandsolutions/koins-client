git pull origin master
bundle install --deployment
bundle exec rake db:migrate RAILS_ENV=production
bundle exec rake assets:precompile RAILS_ENV=production

sudo service nginx restart

bundle exec rake utils:reload_settings RAILS_ENV=production
bundle exec whenever --update-crontab --set environment=production 
echo "Done!"
