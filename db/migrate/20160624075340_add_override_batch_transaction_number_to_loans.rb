class AddOverrideBatchTransactionNumberToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :override_batch_transaction_number, :string
  end
end
