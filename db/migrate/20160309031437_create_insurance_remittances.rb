class CreateInsuranceRemittances < ActiveRecord::Migration[4.2]
  def change
    create_table :insurance_remittances do |t|
      t.string :uuid
      t.decimal :amount
      t.date :start_date
      t.date :end_date
      t.text :particular
      t.string :status
      t.string :voucher_reference_number
      t.string :or_number

      t.timestamps null: false
    end
  end
end
