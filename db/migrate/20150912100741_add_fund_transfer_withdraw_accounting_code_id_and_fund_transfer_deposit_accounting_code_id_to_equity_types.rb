class AddFundTransferWithdrawAccountingCodeIdAndFundTransferDepositAccountingCodeIdToEquityTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :equity_types, :fund_transfer_withdraw_accounting_code_id, :integer
    add_column :equity_types, :fund_transfer_deposit_accounting_code_id, :integer
  end
end
