class AddWpParticularToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :wp_particular, :text
  end
end
