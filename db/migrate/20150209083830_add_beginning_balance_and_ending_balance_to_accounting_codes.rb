class AddBeginningBalanceAndEndingBalanceToAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :accounting_codes, :beginning_balance, :decimal
    add_column :accounting_codes, :ending_balance, :decimal
  end
end
