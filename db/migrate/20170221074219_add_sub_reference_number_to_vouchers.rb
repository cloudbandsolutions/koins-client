class AddSubReferenceNumberToVouchers < ActiveRecord::Migration[4.2]
  def change
    add_column :vouchers, :sub_reference_number, :string
  end
end
