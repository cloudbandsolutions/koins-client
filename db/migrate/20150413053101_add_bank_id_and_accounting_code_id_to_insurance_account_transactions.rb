class AddBankIdAndAccountingCodeIdToInsuranceAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_account_transactions, :bank_id, :integer
    add_column :insurance_account_transactions, :accounting_code_id, :integer
  end
end
