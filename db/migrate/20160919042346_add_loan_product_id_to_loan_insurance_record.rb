class AddLoanProductIdToLoanInsuranceRecord < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurance_records, :loan_product_id, :integer
  end
end
