class AddUuidToBeneficiaries < ActiveRecord::Migration[4.2]
  def change
    add_column :beneficiaries, :uuid, :string
  end
end
