class AddAfterInterestTransToTimeDepositWithdrawal < ActiveRecord::Migration[5.2]
  def change
    add_column :time_deposit_withdrawals, :after_interest_trans, :string
  end
end
