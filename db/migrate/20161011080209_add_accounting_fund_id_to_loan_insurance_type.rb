class AddAccountingFundIdToLoanInsuranceType < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurance_types, :accounting_fund_id, :integer
  end
end
