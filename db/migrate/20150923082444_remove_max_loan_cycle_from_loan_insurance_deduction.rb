class RemoveMaxLoanCycleFromLoanInsuranceDeduction < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_insurance_deduction_entries, :max_loan_cycle
  end
end
