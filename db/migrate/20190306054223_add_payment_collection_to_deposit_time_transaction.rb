class AddPaymentCollectionToDepositTimeTransaction < ActiveRecord::Migration[5.2]
  def change
    add_reference :deposit_time_transactions, :payment_collection, foreign_key: true
  end
end
