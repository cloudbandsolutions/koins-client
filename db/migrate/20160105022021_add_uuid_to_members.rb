class AddUuidToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :uuid, :string
  end
end
