class CreateDailyClosings < ActiveRecord::Migration[4.2]
  def change
    create_table :daily_closings do |t|
      t.string :uuid
      t.date :closing_date

      t.timestamps null: false
    end
  end
end
