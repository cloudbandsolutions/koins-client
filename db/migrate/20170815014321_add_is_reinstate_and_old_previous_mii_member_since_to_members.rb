class AddIsReinstateAndOldPreviousMiiMemberSinceToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :is_reinstate, :boolean
    add_column :members, :old_previous_mii_member_since, :date
  end
end
