class AddDateApprovedToInsuranceAccountValidation < ActiveRecord::Migration[4.2]
  def change
  	add_column :insurance_account_validations, :date_approved, :date
  end
end
