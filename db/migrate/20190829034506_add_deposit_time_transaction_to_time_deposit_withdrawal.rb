class AddDepositTimeTransactionToTimeDepositWithdrawal < ActiveRecord::Migration[5.2]
  def change
    add_reference :time_deposit_withdrawals, :deposit_time_transaction, foreign_key: true
  end
end
