class AddAttachmentFilePhotoToMemberAttachmentFiles < ActiveRecord::Migration[4.2]
  def self.up
    change_table :member_attachment_files do |t|
      t.attachment :file_photo
    end
  end

  def self.down
    remove_attachment :member_attachment_files, :file_photo
  end
end
