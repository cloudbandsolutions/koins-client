class CreateInsuranceAccounts < ActiveRecord::Migration[4.2]
  def change
    create_table :insurance_accounts do |t|
      t.integer :insurance_type_id
      t.decimal :balance
      t.integer :member_id
      t.string :account_number

      t.timestamps
    end
  end
end
