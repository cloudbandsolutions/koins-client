class AddMemberFullNameToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :member_full_name, :string
  end
end
