class RemoveWriteoffAdjustmentFields < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_products, :writeoff_dr_adjustment_entry_accounting_code_id
    remove_column :loan_products, :writeoff_cr_adjustment_entry_accounting_code_id
  end
end
