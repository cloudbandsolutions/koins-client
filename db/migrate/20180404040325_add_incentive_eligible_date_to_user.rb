class AddIncentiveEligibleDateToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :incentive_eligble_date, :date
  end
end
