class AddAttachmentFileToCenterTransferRecords < ActiveRecord::Migration[4.2]
  def self.up
    change_table :center_transfer_records do |t|
      t.attachment :file
    end
  end

  def self.down
    remove_attachment :center_transfer_records, :file
  end
end
