class CreateMemberWithdrawalLoans < ActiveRecord::Migration[4.2]
  def change
    create_table :member_withdrawal_loans do |t|
      t.references :member_withdrawal, index: true, foreign_key: true
      t.references :loan, index: true, foreign_key: true
      t.decimal :total_balance
      t.decimal :principal_balance
      t.decimal :interest_balance

      t.timestamps null: false
    end
  end
end
