class AddBranchIdAndAccountingCodeIdToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :branch_id, :integer
    add_column :savings_account_transactions, :accounting_code_id, :integer
  end
end
