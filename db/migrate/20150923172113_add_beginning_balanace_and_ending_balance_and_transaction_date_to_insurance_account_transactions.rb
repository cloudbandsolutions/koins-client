class AddBeginningBalanaceAndEndingBalanceAndTransactionDateToInsuranceAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_account_transactions, :beginning_balance, :decimal
    add_column :insurance_account_transactions, :ending_balance, :decimal
    add_column :insurance_account_transactions, :transaction_date, :date
  end
end
