class AddIsTpdToLegalDependents < ActiveRecord::Migration[4.2]
  def change
    add_column :legal_dependents, :is_tpd, :boolean
  end
end
