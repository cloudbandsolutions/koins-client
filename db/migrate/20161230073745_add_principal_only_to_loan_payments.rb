class AddPrincipalOnlyToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :principal_only, :boolean
  end
end
