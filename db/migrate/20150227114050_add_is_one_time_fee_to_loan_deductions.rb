class AddIsOneTimeFeeToLoanDeductions < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_deductions, :is_one_time_fee, :boolean
  end
end
