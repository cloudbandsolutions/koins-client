class AddSpouseOccupationToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :spouse_occupation, :string
  end
end
