class CreateMemberWithdrawals < ActiveRecord::Migration[4.2]
  def change
    create_table :member_withdrawals do |t|
      t.string :uuid
      t.string :reference_number
      t.string :book
      t.date :paid_at
      t.string :status
      t.decimal :total_withdrawals
      t.decimal :total_payments

      t.timestamps null: false
    end
  end
end
