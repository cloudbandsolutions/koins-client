class AddDataAndForInsuranceToYearEndRecords < ActiveRecord::Migration[5.2]
  def change
    add_column :year_end_closing_records, :for_insurance, :boolean
  end
end
