class RemoveOriginalLoanProductTypeFromLoans < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :original_loan_product_type_id
  end
end
