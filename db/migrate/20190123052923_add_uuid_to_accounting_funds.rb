class AddUuidToAccountingFunds < ActiveRecord::Migration[5.2]
  def change
    add_column :accounting_funds, :uuid, :string
  end
end
