class CreateFileToUploads < ActiveRecord::Migration[5.2]
  def change
    create_table :file_to_uploads do |t|
    	t.string :status
    	t.attachment :file
      	t.string :process_type

      t.timestamps
    end
  end
end
