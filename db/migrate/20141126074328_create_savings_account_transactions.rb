class CreateSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    create_table :savings_account_transactions do |t|
      t.integer :savings_account_id
      t.decimal :amount
      t.string :transaction_type
      t.datetime :transacted_at
      t.text :particular
      t.string :status

      t.timestamps
    end
  end
end
