class CreateYearEndClosingRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :year_end_closing_records do |t|
      t.json :data
      t.references :voucher, index: true, foreign_key: true
      t.json :trial_balance
      t.date :date_closing
      t.integer :year

      t.timestamps null: false
    end
  end
end
