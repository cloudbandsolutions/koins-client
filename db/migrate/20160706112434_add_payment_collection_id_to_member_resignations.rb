class AddPaymentCollectionIdToMemberResignations < ActiveRecord::Migration[4.2]
  def change
    add_column :member_resignations, :payment_collection_id, :integer
  end
end
