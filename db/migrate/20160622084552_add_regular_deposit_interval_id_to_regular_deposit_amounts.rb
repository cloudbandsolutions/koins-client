class AddRegularDepositIntervalIdToRegularDepositAmounts < ActiveRecord::Migration[4.2]
  def change
    add_column :regular_deposit_amounts, :regular_deposit_interval_id, :integer
  end
end
