class AddSavingsTransactionsToTimeDepositWithdrawal < ActiveRecord::Migration[5.2]
  def change
    add_column :time_deposit_withdrawals, :savings_transactions, :string
  end
end
