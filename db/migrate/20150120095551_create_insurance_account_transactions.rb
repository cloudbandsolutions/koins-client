class CreateInsuranceAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    create_table :insurance_account_transactions do |t|
      t.integer :insurance_account_id
      t.decimal :amount
      t.string :transaction_type
      t.datetime :transacted_at
      t.text :particular
      t.string :status
      t.string :transacted_by
      t.string :approved_by
      t.string :voucher_reference_number
      t.string :transaction_number

      t.timestamps
    end
  end
end
