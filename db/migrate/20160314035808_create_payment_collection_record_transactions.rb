class CreatePaymentCollectionRecordTransactions < ActiveRecord::Migration[4.2]
  def change
    create_table :collection_transactions do |t|
      t.references :payment_collection_record, index: true, foreign_key: true
      t.references :loan_payment, index: true, foreign_key: true
      t.string :account_type_code
      t.string :account_type
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
