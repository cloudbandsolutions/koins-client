class AddWithdrawalAmountToMemberWithdrawalFundBalances < ActiveRecord::Migration[4.2]
  def change
    add_column :member_withdrawal_fund_balances, :withdrawal_amount, :decimal
  end
end
