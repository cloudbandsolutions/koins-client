class AddTotalPrincipalAmountAndTotalInterestAmountToCollectionPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :collection_payments, :total_principal_amount, :decimal
    add_column :collection_payments, :total_interest_amount, :decimal
  end
end
