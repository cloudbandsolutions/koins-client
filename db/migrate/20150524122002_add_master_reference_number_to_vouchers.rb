class AddMasterReferenceNumberToVouchers < ActiveRecord::Migration[4.2]
  def change
    add_column :vouchers, :master_reference_number, :string
  end
end
