class ModifyLoanProductFields < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_products, :bank_id
    remove_column :loan_products, :wp_accounting_code_id
    remove_column :loan_products, :deposit_accounting_code_id

    add_column :loan_products, :accounting_code_processing_fee_id, :integer
  end
end
