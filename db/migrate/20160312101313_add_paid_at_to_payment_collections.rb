class AddPaidAtToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :paid_at, :date
  end
end
