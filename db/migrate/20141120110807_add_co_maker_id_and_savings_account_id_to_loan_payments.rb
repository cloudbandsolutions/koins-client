class AddCoMakerIdAndSavingsAccountIdToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :co_maker_id, :integer
    add_column :loan_payments, :savings_account_id, :integer
  end
end
