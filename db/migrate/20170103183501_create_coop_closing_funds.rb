class CreateCoopClosingFunds < ActiveRecord::Migration[4.2]
  def change
    create_table :coop_closing_funds do |t|
      t.string :name
      t.decimal :ratio

      t.timestamps null: false
    end
  end
end
