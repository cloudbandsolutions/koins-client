class RemovePreviousMembershipDates < ActiveRecord::Migration[4.2]
  def change
	    remove_column :members, :kdci_member_since_when
	    remove_column :members, :kmba_member_since_when
  end
end
