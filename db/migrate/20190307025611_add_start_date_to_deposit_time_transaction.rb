class AddStartDateToDepositTimeTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :deposit_time_transactions, :start_date, :date
  end
end
