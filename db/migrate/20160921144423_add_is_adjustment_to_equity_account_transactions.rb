class AddIsAdjustmentToEquityAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :equity_account_transactions, :is_adjustment, :boolean
  end
end
