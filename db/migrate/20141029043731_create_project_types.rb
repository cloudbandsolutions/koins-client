class CreateProjectTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :project_types do |t|
      t.string :name
      t.string :code
      t.integer :project_type_category_id

      t.timestamps
    end
  end
end
