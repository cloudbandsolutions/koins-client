class CreateDailyClosingMemberRepayments < ActiveRecord::Migration[4.2]
  def change
    create_table :daily_closing_member_repayments do |t|
      t.string :uuid
      t.references :daily_closing, index: true, foreign_key: true
      t.string :member_identification_number
      t.string :first_name
      t.string :last_name
      t.string :middle_name
      t.string :member_uuid
      t.string :loan_product_code
      t.date :date_released
      t.decimal :loan_amount
      t.decimal :principal_paid
      t.decimal :loan_balance
      t.decimal :interest_amount
      t.decimal :interest_paid
      t.decimal :interest_balance
      t.decimal :total_paid
      t.decimal :cummulative_due
      t.decimal :amount_past_due
      t.decimal :repayment_rate

      t.timestamps null: false
    end
  end
end
