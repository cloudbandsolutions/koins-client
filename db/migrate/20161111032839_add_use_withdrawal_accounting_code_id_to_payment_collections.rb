class AddUseWithdrawalAccountingCodeIdToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :use_withdrawal_accounting_code_id, :integer
  end
end
