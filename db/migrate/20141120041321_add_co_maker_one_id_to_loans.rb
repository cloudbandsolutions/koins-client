class AddCoMakerOneIdToLoans < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :co_maker_one
    add_column :loans, :co_maker_one_id, :integer
  end
end
