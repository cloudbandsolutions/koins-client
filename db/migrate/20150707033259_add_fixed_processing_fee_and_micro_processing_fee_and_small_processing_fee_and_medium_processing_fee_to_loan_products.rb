class AddFixedProcessingFeeAndMicroProcessingFeeAndSmallProcessingFeeAndMediumProcessingFeeToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :micro_processing_fee, :decimal
    add_column :loan_products, :small_processing_fee, :decimal
    add_column :loan_products, :medium_processing_fee, :decimal
  end
end
