class AddBranchIdToInsuranceAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_accounts, :branch_id, :integer
  end
end
