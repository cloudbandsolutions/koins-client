class RemoveCollectionModels < ActiveRecord::Migration[4.2]
  def change
    drop_table :collection_payment_reversed_vouchers
    drop_table :collection_payment_insurance_amounts
  end
end
