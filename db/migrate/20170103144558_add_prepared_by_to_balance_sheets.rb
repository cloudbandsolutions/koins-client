class AddPreparedByToBalanceSheets < ActiveRecord::Migration[4.2]
  def change
    add_column :balance_sheets, :prepared_by, :string
  end
end
