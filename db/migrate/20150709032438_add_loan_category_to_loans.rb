class AddLoanCategoryToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :loan_category, :string
  end
end
