class CreateAggregatedReports < ActiveRecord::Migration[4.2]
  def change
    create_table :aggregated_reports do |t|
      t.string :uuid
      t.json :data
      t.date :report_date

      t.timestamps null: false
    end
  end
end
