class AddStatusToInsuranceAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_accounts, :status, :string
  end
end
