class ModifyMotherAccuntingCodeModel < ActiveRecord::Migration[4.2]
  def change
    remove_column :mother_accounting_codes, :normal_transaction
    remove_column :mother_accounting_codes, :major_account
    remove_column :mother_accounting_codes, :major_group

    add_column :mother_accounting_codes, :major_account_id, :integer
  end
end
