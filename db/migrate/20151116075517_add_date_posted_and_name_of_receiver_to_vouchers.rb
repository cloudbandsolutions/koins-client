class AddDatePostedAndNameOfReceiverToVouchers < ActiveRecord::Migration[4.2]
  def change
    add_column :vouchers, :date_posted, :date
    add_column :vouchers, :name_of_receiver, :string
  end
end
