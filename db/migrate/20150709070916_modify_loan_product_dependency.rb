class ModifyLoanProductDependency < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_product_dependencies, :loan_product_id
    add_column :loan_product_dependencies, :dependent_loan_product_id, :integer
  end
end
