class AddDateOfReleaseToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :date_of_release, :date
  end
end
