class AddInterestBalanceToMemberLoanTransferRequests < ActiveRecord::Migration[4.2]
  def change
    add_column :member_loan_transfer_requests, :interest_balance, :decimal
  end
end
