class CreateCollectionPayments < ActiveRecord::Migration[4.2]
  def change
    create_table :collection_payments do |t|
      t.string :reference_number
      t.decimal :amount
      t.date :paid_at
      t.string :prepared_by
      t.string :approved_by
      t.text :notes
      t.integer :branch_id
      t.string :status

      t.timestamps
    end
  end
end
