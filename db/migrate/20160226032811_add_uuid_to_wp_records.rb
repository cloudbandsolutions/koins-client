class AddUuidToWpRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :wp_records, :uuid, :string
  end
end
