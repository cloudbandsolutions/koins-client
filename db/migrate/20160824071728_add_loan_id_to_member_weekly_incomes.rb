class AddLoanIdToMemberWeeklyIncomes < ActiveRecord::Migration[4.2]
  def change
    add_column :member_weekly_incomes, :loan_id, :integer
  end
end
