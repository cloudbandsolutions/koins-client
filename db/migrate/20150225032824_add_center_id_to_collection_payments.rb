class AddCenterIdToCollectionPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :collection_payments, :center_id, :integer
  end
end
