class CreateLoans < ActiveRecord::Migration[4.2]
  def change
    create_table :loans do |t|
      t.string :field_office
      t.integer :center_id
      t.integer :branch_id
      t.date :date_prepared
      t.integer :member_id
      t.decimal :amount
      t.integer :loan_product_id
      t.string :term
      t.string :pn_number
      t.string :payment_type
      t.integer :num_installments
      t.string :voucher_check_number
      t.integer :bank_id
      t.string :co_maker_one
      t.string :co_maker_two
      t.string :voucher_payee
      t.string :voucher_particular
      t.string :voucher_reference_number
      t.date :voucher_date_requested
      t.integer :project_type_id
      t.integer :project_category_id
      t.string :status
      t.decimal :interest_rate

      t.timestamps
    end
  end
end
