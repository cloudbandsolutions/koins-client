class RemoveDailyClosingTables < ActiveRecord::Migration[4.2]
  def change
    drop_table :daily_closing_branch_repayments
    drop_table :daily_closing_center_repayments
    drop_table :daily_closing_member_repayments
    drop_table :daily_closing_so_repayments
    drop_table :daily_closings
  end
end
