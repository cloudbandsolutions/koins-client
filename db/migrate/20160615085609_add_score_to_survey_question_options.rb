class AddScoreToSurveyQuestionOptions < ActiveRecord::Migration[4.2]
  def change
    add_column :survey_question_options, :score, :integer
  end
end
