class RemoveFieldsToMember < ActiveRecord::Migration[4.2]
  def change
  	remove_column :members, :age
  	remove_column :members, :tin_number
  	remove_column :members, :email
  	remove_column :members, :occupation
  	remove_column :members, :occupation_address
  end
end
