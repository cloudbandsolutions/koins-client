class AddBookToCenterTransferRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :center_transfer_records, :book, :string
  end
end
