class AddOfficerInChargeToCenters < ActiveRecord::Migration[4.2]
  def change
    add_column :centers, :officer_in_charge, :string
  end
end
