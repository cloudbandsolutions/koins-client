class AddLoanProductCycleCountStatsToDailyReports < ActiveRecord::Migration[4.2]
  def change
    add_column :daily_reports, :loan_product_cycle_count_stats, :json
  end
end
