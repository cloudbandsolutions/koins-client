class AddSavingsAccountAmountToInterestOnShareCapitalCollection < ActiveRecord::Migration[5.2]
  def change
    add_column :interest_on_share_capital_collections, :savings_account_amount, :string
  end
end
