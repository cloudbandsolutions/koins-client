class DropBatchTransactionModels < ActiveRecord::Migration[4.2]
  def change
    drop_table :batch_transaction_models
  end
end
