class CreateBranches < ActiveRecord::Migration[4.2]
  def change
    create_table :branches do |t|
      t.string :name
      t.string :abbreviation
      t.string :code
      t.text :address
      t.integer :cluster_id

      t.timestamps
    end
  end
end
