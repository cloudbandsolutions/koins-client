class MaintainingBalanceExceptionCycleCountToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :maintaining_balance_exception_cycle_count, :integer
  end
end
