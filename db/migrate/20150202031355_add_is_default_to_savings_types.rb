class AddIsDefaultToSavingsTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_types, :is_default, :boolean
  end
end
