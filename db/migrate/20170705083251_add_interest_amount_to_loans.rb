class AddInterestAmountToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :interest_amount, :decimal
  end
end
