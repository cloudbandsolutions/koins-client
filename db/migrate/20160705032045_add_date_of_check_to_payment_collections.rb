class AddDateOfCheckToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :date_of_check, :date
  end
end
