class AddMemberIdAndSavingsTypeIdToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :member_id, :integer
    add_column :savings_account_transactions, :savings_type_id, :integer
  end
end
