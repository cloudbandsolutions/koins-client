class CreateMemberTransferRequests < ActiveRecord::Migration[4.2]
  def change
    create_table :member_transfer_requests do |t|
      t.references :member, index: true, foreign_key: true
      t.string :uuid
      t.string :status
      t.date :date_of_request
      t.date :date_transfered

      t.timestamps null: false
    end
  end
end
