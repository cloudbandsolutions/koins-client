class CreateUserCenters < ActiveRecord::Migration[4.2]
  def change
    create_table :user_centers, id: false do |t|
      t.references :user
      t.references :center
    end

    add_index :user_centers, [:user_id, :center_id], unique: true, name: 'user_center_index'
  end
end
