class CreateMemberApplicationMembershipPayments < ActiveRecord::Migration[4.2]
  def change
    create_table :member_membership_payments do |t|
      t.references :member, index: true, foreign_key: true
      t.references :membership_type, index: true, foreign_key: true
      t.decimal :amount_paid
      t.date :paid_at

      t.timestamps null: false
    end
  end
end
