class AddRequireSavingsDepositAndRequireInsuranceDepositAndRequireEquityDepositToMembershipTypes < ActiveRecord::Migration[4.2]
  def change
    remove_column :membership_types, :account_type
    remove_column :membership_types, :require_deposit
    add_column :membership_types, :require_savings_deposit, :boolean
    add_column :membership_types, :require_insurance_deposit, :boolean
    add_column :membership_types, :require_equity_deposit, :boolean
  end
end
