class RemoveFieldsAtMembers < ActiveRecord::Migration[4.2]
  def change
    remove_column :members, :date_of_membership
    remove_column :members, :subcribe_capital_share
    remove_column :members, :membership_fee
  end
end
