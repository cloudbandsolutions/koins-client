class AddTotalCbuAmountToPatronageRefundCollection < ActiveRecord::Migration[5.2]
  def change
    add_column :patronage_refund_collections, :total_cbu_amount, :string
  end
end
