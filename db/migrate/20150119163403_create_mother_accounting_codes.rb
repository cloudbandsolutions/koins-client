class CreateMotherAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    create_table :mother_accounting_codes do |t|
      t.string :name
      t.string :code

      t.timestamps
    end
  end
end
