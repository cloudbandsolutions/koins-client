class AddApprovedByAndPreparedByToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :approved_by, :string
    add_column :loan_payments, :prepared_by, :string
  end
end
