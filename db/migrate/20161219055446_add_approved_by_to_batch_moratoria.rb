class AddApprovedByToBatchMoratoria < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_moratoria, :approved_by, :string
  end
end
