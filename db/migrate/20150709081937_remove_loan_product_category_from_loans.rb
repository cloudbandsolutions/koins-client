class RemoveLoanProductCategoryFromLoans < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :loan_category
  end
end
