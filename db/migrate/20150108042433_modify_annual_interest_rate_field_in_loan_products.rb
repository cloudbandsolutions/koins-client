class ModifyAnnualInterestRateFieldInLoanProducts < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_products, :annual_interest_rate
    remove_column :loan_products, :weekly_interest_rate
    remove_column :loan_products, :monthly_interest_rate

    add_column :loan_products, :interest_rate, :decimal
  end
end
