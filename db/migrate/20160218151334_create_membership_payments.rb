class CreateMembershipPayments < ActiveRecord::Migration[4.2]
  def change
    create_table :membership_payments do |t|
      t.references :membership_type, index: true, foreign_key: true
      t.references :member, index: true, foreign_key: true
      t.date :paid_at
      t.decimal :amount_paid
      t.string :uuid

      t.timestamps null: false
    end
  end
end
