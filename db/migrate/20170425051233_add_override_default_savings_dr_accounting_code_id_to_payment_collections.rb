class AddOverrideDefaultSavingsDrAccountingCodeIdToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :override_default_savings_dr_accounting_code_id, :integer
  end
end
