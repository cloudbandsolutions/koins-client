class ChangeDailyClosingSoToDailyClosingCenterForMemberRepayment < ActiveRecord::Migration[4.2]
  def change
    remove_column :daily_closing_member_repayments, :daily_closing_so_repayment_id
    add_column :daily_closing_member_repayments, :daily_closing_center_repayment_id, :integer
  end
end
