class CreateResignationLoanBalances < ActiveRecord::Migration[4.2]
  def change
    create_table :resignation_loan_balances do |t|
      t.string :uuid
      t.decimal :balance
      t.references :loan, index: true, foreign_key: true
      t.references :member_resignation, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
