class CreateSavingsAccounts < ActiveRecord::Migration[4.2]
  def change
    create_table :savings_accounts do |t|
      t.integer :account_number
      t.integer :balance
      t.integer :savings_type_id
      t.integer :member_id

      t.timestamps
    end
  end
end
