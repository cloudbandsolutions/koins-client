class CreateLoanPaymentAmortizationScheduleEntries < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_payment_amortization_schedule_entries do |t|
      t.integer :loan_payment_id
      t.integer :ammortization_schedule_entry_id
    end
  end
end
