class AddPaymentStatusToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :payment_status, :string
  end
end
