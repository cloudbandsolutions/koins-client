class AddInsuredToLoanProductTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_product_types, :insured, :boolean
  end
end
