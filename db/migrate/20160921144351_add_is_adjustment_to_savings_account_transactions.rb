class AddIsAdjustmentToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :is_adjustment, :boolean
  end
end
