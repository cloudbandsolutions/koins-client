class AddTotalAmountDueToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :total_amount_due, :integer
  end
end
