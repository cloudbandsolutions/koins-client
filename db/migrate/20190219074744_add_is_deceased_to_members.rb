class AddIsDeceasedToMembers < ActiveRecord::Migration[5.2]
  def change
    add_column :members, :is_deceased, :boolean
  end
end
