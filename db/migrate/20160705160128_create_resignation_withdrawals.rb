class CreateResignationWithdrawals < ActiveRecord::Migration[4.2]
  def change
    create_table :resignation_withdrawals do |t|
      t.string :uuid
      t.string :account_type
      t.string :account_type_code
      t.decimal :amount
      t.references :member_resignation, index: { name: 'mem_res_w_index' }, foreign_key: true

      t.timestamps null: false
    end
  end
end
