class AddBeneficiaryFirstNameAndBeneficiaryMiddleNameAndBeneficiaryLastNameAndBeneficiaryDateOfBirthAndBeneficiaryRelationshipToLoans < ActiveRecord::Migration[4.2]
  def change
  	add_column :loans, :beneficiary_first_name, :string
  	add_column :loans, :beneficiary_middle_name, :string
  	add_column :loans, :beneficiary_last_name, :string
  	add_column :loans, :beneficiary_date_of_birth, :date
  	add_column :loans, :beneficiary_relationship, :string
  end
end
