class AddOrNumberToCollectionPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :collection_payments, :or_number, :string
  end
end
