class CreateLoanDeductionEntries < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_deduction_entries do |t|
      t.integer :loan_product_id
      t.integer :loan_deduction_id
      t.decimal :val
      t.boolean :is_percent

      t.timestamps
    end
  end
end
