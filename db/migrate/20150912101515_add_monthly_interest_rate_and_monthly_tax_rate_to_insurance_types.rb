class AddMonthlyInterestRateAndMonthlyTaxRateToInsuranceTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_types, :monthly_interest_rate, :decimal
    add_column :insurance_types, :monthly_tax_rate, :decimal
  end
end
