class CreateSubsidiaryAdjustmentEntries < ActiveRecord::Migration[4.2]
  def change
    create_table :subsidiary_adjustment_entries do |t|
      t.references :subsidiary_adjustment, index: true, foreign_key: true
      t.references :accounting_code, index: true, foreign_key: true
      t.string :post_type
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
