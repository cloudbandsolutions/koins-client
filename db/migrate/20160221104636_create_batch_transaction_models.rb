class CreateBatchTransactionModels < ActiveRecord::Migration[4.2]
  def change
    create_table :batch_transaction_models do |t|
      t.references :batch_transaction, index: true, foreign_key: true
      t.text :particular
      t.string :uuid
      t.datetime :posted_at
      t.string :posted_by
      t.string :approved_by

      t.timestamps null: false
    end
  end
end
