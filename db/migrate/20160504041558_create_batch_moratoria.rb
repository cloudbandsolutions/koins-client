class CreateBatchMoratoria < ActiveRecord::Migration[4.2]
  def change
    create_table :batch_moratoria do |t|
      t.date :date_initialized
      t.date :start_of_moratorium
      t.integer :number_of_days
      t.string :initialized_by
      t.string :status
      t.string :uuid
      t.text :reason

      t.timestamps null: false
    end
  end
end
