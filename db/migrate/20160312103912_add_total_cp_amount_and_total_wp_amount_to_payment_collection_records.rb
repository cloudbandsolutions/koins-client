class AddTotalCpAmountAndTotalWpAmountToPaymentCollectionRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collection_records, :total_cp_amount, :decimal
    add_column :payment_collection_records, :total_wp_amount, :decimal
  end
end
