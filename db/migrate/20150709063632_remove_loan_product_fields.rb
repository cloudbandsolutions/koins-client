class RemoveLoanProductFields < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_products, :micro_processing_fee
    remove_column :loan_products, :small_processing_fee
    remove_column :loan_products, :medium_processing_fee
    remove_column :loan_products, :processing_fee_15
    remove_column :loan_products, :processing_fee_25
    remove_column :loan_products, :processing_fee_35
    remove_column :loan_products, :processing_fee_50
    remove_column :loan_products, :interest_rate
    remove_column :loan_products, :dependent_loan_product_id
    remove_column :loan_products, :dependent_loan_product_cycle_count
  end
end
