class CreateRequestRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :request_records do |t|
      t.json :info
      t.string :status
      t.string :prepared_by
      t.string :request_type

      t.timestamps null: false
    end
  end
end
