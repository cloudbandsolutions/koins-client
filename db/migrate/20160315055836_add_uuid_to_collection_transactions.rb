class AddUuidToCollectionTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :collection_transactions, :uuid, :string
  end
end
