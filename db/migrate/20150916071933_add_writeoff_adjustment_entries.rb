class AddWriteoffAdjustmentEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :writeoff_dr_adjustment_entry_accounting_code_id, :integer
    add_column :loan_products, :writeoff_cr_adjustment_entry_accounting_code_id, :integer
    add_column :loan_products, :writeoff_dr_accounting_code_id, :integer
    add_column :loan_products, :writeoff_cr_accounting_code_id, :integer
  end
end
