class AddPrincipalDueAndInterestDueToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :principal_due, :decimal
    add_column :loan_payments, :interest_due, :decimal
  end
end
