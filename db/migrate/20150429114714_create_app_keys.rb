class CreateAppKeys < ActiveRecord::Migration[4.2]
  def change
    create_table :app_keys do |t|
      t.string :app_name
      t.string :api_key

      t.timestamps null: false
    end
  end
end
