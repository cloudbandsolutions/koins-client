class AddAnnualInterestRateToSavingsTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_types, :annual_interest_rate, :decimal
  end
end
