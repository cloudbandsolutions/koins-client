class AddCreditAccountingCodeIdToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
  	add_column :payment_collections, :credit_accounting_code_id, :integer
  end
end
