class AddPayeeToVouchers < ActiveRecord::Migration[4.2]
  def change
    add_column :vouchers, :payee, :string
  end
end
