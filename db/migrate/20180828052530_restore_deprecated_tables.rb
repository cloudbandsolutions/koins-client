class RestoreDeprecatedTables < ActiveRecord::Migration[5.2]
  def change
    create_table :withdraw_payment_transactions do |t|
      t.decimal :amount
      t.integer :savings_account_id
      t.integer :member_id
      t.integer :co_maker_id

      t.timestamps
    end

    create_table :wp_records do |t|
      t.references :member, index: true, foreign_key: true
      t.decimal :amount
      t.references :loan_payment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
