class AddInterestRateToLoanProductTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_product_types, :interest_rate, :decimal
  end
end
