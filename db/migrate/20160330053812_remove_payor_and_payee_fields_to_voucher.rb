class RemovePayorAndPayeeFieldsToVoucher < ActiveRecord::Migration[4.2]
  def change
  	remove_column :vouchers, :payee
  	remove_column :vouchers, :payor
  end
end
