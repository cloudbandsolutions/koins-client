class CreateMajorAccounts < ActiveRecord::Migration[4.2]
  def change
    create_table :major_accounts do |t|
      t.string :name
      t.string :code

      t.timestamps null: false
    end
  end
end
