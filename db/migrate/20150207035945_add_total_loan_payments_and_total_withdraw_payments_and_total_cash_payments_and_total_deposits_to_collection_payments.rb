class AddTotalLoanPaymentsAndTotalWithdrawPaymentsAndTotalCashPaymentsAndTotalDepositsToCollectionPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :collection_payments, :total_withdraw_payments, :decimal
    add_column :collection_payments, :total_cash_payments, :decimal
    add_column :collection_payments, :total_deposits, :decimal
  end
end
