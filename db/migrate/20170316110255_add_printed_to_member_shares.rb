class AddPrintedToMemberShares < ActiveRecord::Migration[4.2]
  def change
    add_column :member_shares, :printed, :boolean
  end
end
