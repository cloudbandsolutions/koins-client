class AddReturnedContributionToClaims < ActiveRecord::Migration[4.2]
  def change
  	add_column :claims, :returned_contribution, :decimal
  end
end
