class AddInsuredAmountToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :insured_amount, :decimal
  end
end
