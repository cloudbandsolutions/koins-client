class AddDateOfCheckToVoucher < ActiveRecord::Migration[4.2]
  def change
    add_column :vouchers, :date_of_check, :date
  end
end
