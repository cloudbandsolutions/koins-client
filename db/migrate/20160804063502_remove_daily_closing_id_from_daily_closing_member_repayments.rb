class RemoveDailyClosingIdFromDailyClosingMemberRepayments < ActiveRecord::Migration[4.2]
  def change
    remove_column :daily_closing_member_repayments, :daily_closing_id
  end
end
