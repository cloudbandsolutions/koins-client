class AddAndModifyFieldsToMembers < ActiveRecord::Migration[4.2]
  def change
  	 add_column :members, :spouse_age, :integer
  	 add_column :members, :age, :integer
  	 add_column :members, :date_of_membership, :date
  	 add_column :members, :subcribe_capital_share, :string
  	 add_column :members, :membership_fee, :string
  	 #rename_column :members, :previous_mfi_member_since, :kdci_member_since_when
  	 #rename_column :members, :previous_mli_member_since, :kmba_member_since_when
  end
end
