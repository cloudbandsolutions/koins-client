class CreateBasicReports < ActiveRecord::Migration[4.2]
  def change
    create_table :basic_reports do |t|
      t.string :uuid
      t.date :reporting_date
      t.json :data

      t.timestamps null: false
    end
  end
end
