class AddNumChildrenAndNumChildrenElementaryAndNumChildrenHighSchoolAndNumChildrenCollegeToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :num_children, :integer
    add_column :members, :num_children_elementary, :integer
    add_column :members, :num_children_high_school, :integer
    add_column :members, :num_children_college, :integer
  end
end
