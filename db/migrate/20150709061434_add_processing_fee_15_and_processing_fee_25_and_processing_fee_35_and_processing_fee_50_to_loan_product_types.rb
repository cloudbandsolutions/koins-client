class AddProcessingFee15AndProcessingFee25AndProcessingFee35AndProcessingFee50ToLoanProductTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_product_types, :processing_fee_15, :decimal
    add_column :loan_product_types, :processing_fee_25, :decimal
    add_column :loan_product_types, :processing_fee_35, :decimal
    add_column :loan_product_types, :processing_fee_50, :decimal
  end
end
