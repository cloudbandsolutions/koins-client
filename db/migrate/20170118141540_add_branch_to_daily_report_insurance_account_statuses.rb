class AddBranchToDailyReportInsuranceAccountStatuses < ActiveRecord::Migration[4.2]
  def change
    add_reference :daily_report_insurance_account_statuses, :branch, index: true, foreign_key: true
  end
end
