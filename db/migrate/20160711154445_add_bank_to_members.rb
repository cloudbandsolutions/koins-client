class AddBankToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :bank, :string
  end
end
