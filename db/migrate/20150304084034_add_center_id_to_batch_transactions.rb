class AddCenterIdToBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transactions, :center_id, :integer
  end
end
