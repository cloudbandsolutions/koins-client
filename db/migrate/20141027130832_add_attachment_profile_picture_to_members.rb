class AddAttachmentProfilePictureToMembers < ActiveRecord::Migration[4.2]
  def self.up
    change_table :members do |t|
      t.attachment :profile_picture
    end
  end

  def self.down
    remove_attachment :members, :profile_picture
  end
end
