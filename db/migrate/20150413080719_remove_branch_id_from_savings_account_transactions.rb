class RemoveBranchIdFromSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    remove_column :savings_account_transactions, :branch_id
  end
end
