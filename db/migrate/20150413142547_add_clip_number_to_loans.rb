class AddClipNumberToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :clip_number, :string
  end
end
