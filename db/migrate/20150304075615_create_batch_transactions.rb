class CreateBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    create_table :batch_transactions do |t|
      t.string :transaction_number
      t.datetime :transacted_at
      t.decimal :total_amount
      t.text :particular
      t.string :status

      t.timestamps
    end
  end
end
