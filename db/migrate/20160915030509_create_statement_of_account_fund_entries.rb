class CreateStatementOfAccountFundEntries < ActiveRecord::Migration[4.2]
  def change
    create_table :statement_of_account_fund_entries do |t|
      t.string :account_type
      t.integer :account_type_reference_id
      t.integer :account_reference_id
      t.date :date_of_transaction
      t.decimal :debit_amount
      t.decimal :credit_amount
      t.references :member, index: true, foreign_key: true
      t.references :branch, index: true, foreign_key: true
      t.references :center, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
