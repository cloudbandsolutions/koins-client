class AddVoucherReferenceNumberToBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transactions, :voucher_reference_number, :string
  end
end
