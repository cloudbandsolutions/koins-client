class AddInsuranceDepositVoucherReferenceNumberToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :insurance_deposit_voucher_reference_number, :string
  end
end
