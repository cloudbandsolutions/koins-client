class AddOrNumberToMemberResignations < ActiveRecord::Migration[4.2]
  def change
    add_column :member_resignations, :or_number, :string
  end
end
