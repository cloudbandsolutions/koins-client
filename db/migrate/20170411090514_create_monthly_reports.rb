class CreateMonthlyReports < ActiveRecord::Migration[4.2]
  def change
    create_table :monthly_reports do |t|
      t.string :report_type
      t.string :generated_by
      t.date :date_generated
      t.json :data
      t.integer :month
      t.integer :year

      t.timestamps null: false
    end
  end
end
