class ChangeSpouseageFieldsTypeInMember < ActiveRecord::Migration[4.2]
  def change
  	change_column :members, :spouse_age, :string
  end
end
