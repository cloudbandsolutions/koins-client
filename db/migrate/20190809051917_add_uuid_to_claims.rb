class AddUuidToClaims < ActiveRecord::Migration[5.2]
  def change
    add_column :claims, :uuid, :string
  end
end
