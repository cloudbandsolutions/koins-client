class AddIsWithdrawPaymentToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :is_withdraw_payment, :boolean
  end
end
