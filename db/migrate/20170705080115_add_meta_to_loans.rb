class AddMetaToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :meta, :json
  end
end
