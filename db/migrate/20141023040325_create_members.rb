class CreateMembers < ActiveRecord::Migration[4.2]
  def change
    create_table :members do |t|
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :gender
      t.date :date_of_birth
      t.string :civil_status
      t.date :spouse_date_of_birth
      t.string :home_number
      t.string :occupation
      t.string :processed_by
      t.string :approved_by
      t.string :identification_number
      t.string :address_street
      t.string :address_barangay
      t.string :address_city
      t.string :mobile_number
      t.string :email
      t.string :place_of_birth
      t.string :religion
      t.integer :number_of_children
      t.string :sss_number
      t.string :occupation_address
      t.string :spouse_first_name
      t.string :spouse_middle_name
      t.string :spouse_last_name

      t.timestamps
    end
  end
end
