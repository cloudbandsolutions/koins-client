class AddMemberClassificationToInsuranceAccountValidationRecord < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_validation_records, :member_classification, :string
  end
end
