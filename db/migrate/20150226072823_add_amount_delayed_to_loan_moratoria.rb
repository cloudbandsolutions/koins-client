class AddAmountDelayedToLoanMoratoria < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_moratoria, :amount_delayed, :decimal
  end
end
