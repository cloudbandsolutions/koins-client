class AddDrAccountingEntryIdAndCrAccountingEntryIdToMembershipTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :membership_types, :dr_accounting_entry_id, :integer
    add_column :membership_types, :cr_accounting_entry_id, :integer
  end
end
