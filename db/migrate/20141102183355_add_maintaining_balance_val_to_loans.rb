class AddMaintainingBalanceValToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :maintaining_balance_val, :decimal
  end
end
