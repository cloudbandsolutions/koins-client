class AddTotalEquityInterestToInsuranceAccountValidations < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_validations, :total_equity_interest, :decimal
  end
end
