class CreateMemberCertificates < ActiveRecord::Migration[5.2]
  def change
    create_table :member_certificates do |t|
		t.integer :member_id
      	t.string :certificate_number
      	t.string :uuid
    	t.date :date_issue
      	t.date :date_printed
      	t.boolean :is_void    	
      	t.boolean :is_printed
      	t.integer :number_of_certificates

      t.timestamps
    end
  end
end
