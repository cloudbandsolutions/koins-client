class AddOverrideInsuranceDeductionsToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :override_insurance_deductions, :boolean
  end
end
