class RemoveReligionField < ActiveRecord::Migration[4.2]
  def change
    remove_column :members, :religion
    add_column :members, :religion_id, :integer
  end
end
