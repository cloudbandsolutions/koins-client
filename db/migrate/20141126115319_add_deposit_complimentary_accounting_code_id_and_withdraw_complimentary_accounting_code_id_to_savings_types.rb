class AddDepositComplimentaryAccountingCodeIdAndWithdrawComplimentaryAccountingCodeIdToSavingsTypes < ActiveRecord::Migration[4.2]
  def change
    remove_column :savings_types, :due_accounting_code_id
    add_column :savings_types, :deposit_complimentary_accounting_code_id, :integer
    add_column :savings_types, :withdraw_complimentary_accounting_code_id, :integer
  end
end
