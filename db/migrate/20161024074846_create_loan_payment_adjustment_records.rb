class CreateLoanPaymentAdjustmentRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_payment_adjustment_records do |t|
      t.references :loan_payment_adjustment, index: { name: "index_clpar_lpa" }, foreign_key: true
      t.references :loan_payment, index: {name: "index_clpa_lp"}, foreign_key: true
      t.decimal :new_amount

      t.timestamps null: false
    end
  end
end
