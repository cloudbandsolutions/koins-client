class AddInsuranceStatusToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :insurance_status, :string
  end
end
