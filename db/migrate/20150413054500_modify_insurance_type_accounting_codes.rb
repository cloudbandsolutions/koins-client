class ModifyInsuranceTypeAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    remove_column :insurance_types, :default_withdraw_dr_account_id
    remove_column :insurance_types, :default_withdraw_cr_account_id
    remove_column :insurance_types, :default_deposit_dr_account_id
    remove_column :insurance_types, :default_deposit_cr_account_id

    add_column :insurance_types, :withdraw_accounting_code_id, :integer
    add_column :insurance_types, :deposit_accounting_code_id, :integer
  end
end
