class AddBusinessPermitAvailableToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :business_permit_available, :boolean
  end
end
