class AddCashDebitAccountingCodeIdAndCashCreditAccountingCodeIdAndUnremittedDebitAccountingCodeIdAndUnremittedCreditAccountingCodeIdToLoanInsuranceType < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurance_types, :cash_debit_accounting_code_id, :integer
  	add_column :loan_insurance_types, :cash_credit_accounting_code_id, :integer
  	add_column :loan_insurance_types, :unremitted_debit_accounting_code_id, :integer
  	add_column :loan_insurance_types, :unremitted_credit_accounting_code_id, :integer
  end
end
