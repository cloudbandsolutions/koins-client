class AddOverrideMaintainingBalanceToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :override_maintaining_balance, :boolean
  end
end
