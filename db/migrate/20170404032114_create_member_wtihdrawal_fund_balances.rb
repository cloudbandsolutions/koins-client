class CreateMemberWtihdrawalFundBalances < ActiveRecord::Migration[4.2]
  def change
    create_table :member_withdrawal_fund_balances do |t|
      t.references :member_withdrawal, index: true, foreign_key: true
      t.string :account_type
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
