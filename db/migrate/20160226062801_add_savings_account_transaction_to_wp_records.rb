class AddSavingsAccountTransactionToWpRecords < ActiveRecord::Migration[4.2]
  def change
    add_reference :wp_records, :savings_account_transaction, index: true, foreign_key: true
  end
end
