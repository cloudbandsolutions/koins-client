class ModifyLoansFieldForOverrideFiexedTerm < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :override_fixed_term
    add_column :loans, :override_installment_interval, :boolean
  end
end
