class ModifySavingsTypeAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    remove_column :savings_types, :default_dr_account_id
    remove_column :savings_types, :default_cr_account_id

    add_column :savings_types, :default_withdraw_dr_account_id, :integer
    add_column :savings_types, :default_withdraw_cr_account_id, :integer
    
    add_column :savings_types, :default_deposit_dr_account_id, :integer
    add_column :savings_types, :default_deposit_cr_account_id, :integer
  end
end
