class AddTransactionDateToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :transaction_date, :date
  end
end
