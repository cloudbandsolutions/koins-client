class AddCheckNumberAndCheckVoucherNumberToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :check_number, :string
    add_column :payment_collections, :check_voucher_number, :string
  end
end
