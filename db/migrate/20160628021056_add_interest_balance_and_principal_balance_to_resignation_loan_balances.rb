class AddInterestBalanceAndPrincipalBalanceToResignationLoanBalances < ActiveRecord::Migration[4.2]
  def change
    add_column :resignation_loan_balances, :interest_balance, :decimal
    add_column :resignation_loan_balances, :principal_balance, :decimal
  end
end
