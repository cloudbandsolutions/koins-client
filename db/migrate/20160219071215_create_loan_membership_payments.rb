class CreateLoanMembershipPayments < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_membership_payments do |t|
      t.references :loan, index: true, foreign_key: true
      t.decimal :amount_paid
      t.date :paid_at

      t.timestamps null: false
    end
  end
end
