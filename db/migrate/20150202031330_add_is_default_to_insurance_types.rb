class AddIsDefaultToInsuranceTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_types, :is_default, :boolean
  end
end
