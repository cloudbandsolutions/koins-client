class AddFundTransferWithdrawAccountingCodeIdAndFundTransferDepositAccountingCodeIdToInsuranceTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_types, :fund_transfer_withdraw_accounting_code_id, :integer
    add_column :insurance_types, :fund_transfer_deposit_accounting_code_id, :integer
  end
end
