class RemoveSomeFieldsInLoans < ActiveRecord::Migration[4.2]
  def change
  	remove_column :loans, :reason_for_joining
    remove_column :loans, :is_experienced_with_microfinance
    remove_column :loans, :experience_with_microfinance
  end
end
