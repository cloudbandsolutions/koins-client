class AddMaxLoanAmountToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :max_loan_amount, :decimal
  end
end
