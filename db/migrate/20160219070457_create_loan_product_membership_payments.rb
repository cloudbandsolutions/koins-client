class CreateLoanProductMembershipPayments < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_product_membership_payments do |t|
      t.references :loan_product, index: true, foreign_key: true
      t.references :membership_type, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
