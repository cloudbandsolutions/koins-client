class CreateSurveyRespondentAnswers < ActiveRecord::Migration[4.2]
  def change
    create_table :survey_respondent_answers do |t|
      t.references :survey_respondent, index: true, foreign_key: true
      t.references :survey_question, index: true, foreign_key: true
      t.references :survey_option, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
