class AddCenterIdAndBranchIdToInsuranceRemittances < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_remittances, :center_id, :integer
    add_column :insurance_remittances, :branch_id, :integer
  end
end
