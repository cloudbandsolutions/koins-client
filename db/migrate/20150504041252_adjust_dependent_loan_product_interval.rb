class AdjustDependentLoanProductInterval < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :dependent_loan_product_interval
    add_column :loans, :dependent_loan_product_cycle_count, :integer
  end
end
