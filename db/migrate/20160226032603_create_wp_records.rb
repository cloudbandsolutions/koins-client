class CreateWpRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :wp_records do |t|
      t.references :member, index: true, foreign_key: true
      t.decimal :amount
      t.references :loan_payment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
