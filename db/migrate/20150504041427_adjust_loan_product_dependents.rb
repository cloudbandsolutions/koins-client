class AdjustLoanProductDependents < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :dependent_loan_product_id
    remove_column :loans, :dependent_loan_product_cycle_count
    add_column :loan_products, :dependent_loan_product_id, :integer
    add_column :loan_products, :dependent_loan_product_cycle_count, :integer
    add_column :loan_products, :next_application_loan_cycle_count, :integer
  end
end
