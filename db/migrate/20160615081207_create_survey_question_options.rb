class CreateSurveyQuestionOptions < ActiveRecord::Migration[4.2]
  def change
    create_table :survey_question_options do |t|
      t.references :survey_question, index: true, foreign_key: true
      t.string :option
      t.string :uuid
      t.integer :priority

      t.timestamps null: false
    end
  end
end
