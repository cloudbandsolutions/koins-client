class CreateInsuranceAccountValidationRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :insurance_account_validation_records do |t|
    	t.integer :insurance_account_validation_id
      	t.integer :member_id
      	t.integer :center_id
      	t.string :status
      	t.string :transaction_number
      	t.decimal :rf
      	t.decimal :lif_50_percent
      	t.decimal :advance_lif
      	t.decimal :advance_rf
      	t.decimal :interest
      	t.decimal :total
      	
      t.timestamps null: false
    end
  end
end
