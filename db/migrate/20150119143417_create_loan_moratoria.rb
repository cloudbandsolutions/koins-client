class CreateLoanMoratoria < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_moratoria do |t|
      t.integer :ammortization_schedule_entry_id
      t.date :previous_due_at
      t.date :new_due_at

      t.timestamps
    end
  end
end
