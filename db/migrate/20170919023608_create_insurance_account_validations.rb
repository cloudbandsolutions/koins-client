class CreateInsuranceAccountValidations < ActiveRecord::Migration[4.2]
  def change
    create_table :insurance_account_validations do |t|
   		t.date :date_prepared
      	t.string :status
      	t.integer :branch_id
      	t.string :prepared_by
      	t.string :approved_by

      t.timestamps null: false
    end
  end
end
