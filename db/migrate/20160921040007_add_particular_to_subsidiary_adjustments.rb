class AddParticularToSubsidiaryAdjustments < ActiveRecord::Migration[4.2]
  def change
    add_column :subsidiary_adjustments, :particular, :text
  end
end
