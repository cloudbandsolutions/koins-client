class AddPreparedByToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :prepared_by, :string
  end
end
