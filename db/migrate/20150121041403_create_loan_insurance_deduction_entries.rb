class CreateLoanInsuranceDeductionEntries < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_insurance_deduction_entries do |t|
      t.integer :loan_product_id
      t.integer :insurance_type_id
      t.decimal :val
      t.boolean :is_percent
      t.integer :accounting_code_id

      t.timestamps
    end
  end
end
