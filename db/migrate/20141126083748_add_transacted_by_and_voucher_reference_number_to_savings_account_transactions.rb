class AddTransactedByAndVoucherReferenceNumberToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :transacted_by, :string
    add_column :savings_account_transactions, :voucher_reference_number, :string
  end
end
