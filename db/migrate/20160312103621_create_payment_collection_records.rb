class CreatePaymentCollectionRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :payment_collection_records do |t|
      t.references :member, index: true, foreign_key: true
      t.string :loan_product_code
      t.references :payment_collection, index: true, foreign_key: true
      t.string :uuid

      t.timestamps null: false
    end
  end
end
