class AddDepositAmountAndDepositSavingsAccountIdAndDepositParticularToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :deposit_amount, :decimal
    add_column :loan_payments, :deposit_savings_account_id, :integer
    add_column :loan_payments, :deposit_particular, :text
  end
end
