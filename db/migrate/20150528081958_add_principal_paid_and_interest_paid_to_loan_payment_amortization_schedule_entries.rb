class AddPrincipalPaidAndInterestPaidToLoanPaymentAmortizationScheduleEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payment_amortization_schedule_entries, :principal_paid, :decimal
    add_column :loan_payment_amortization_schedule_entries, :interest_paid, :decimal
  end
end
