class CreateInterestOnShareCapitalCollectionRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :interest_on_share_capital_collection_records do |t|
      t.integer :interest_on_share_capital_collection_id
      t.json :data

      t.timestamps null: false
    end
  end
end
