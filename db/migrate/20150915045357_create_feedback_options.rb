class CreateFeedbackOptions < ActiveRecord::Migration[4.2]
  def change
    create_table :feedback_options do |t|
      t.string :content

      t.timestamps null: false
    end
  end
end
