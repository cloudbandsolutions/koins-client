class AddNumYearsBusinessAndNumWorkersBusinessAndReasonForJoiningAndIsExperiencedWithMicrofinanceToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :num_years_business, :decimal
    add_column :members, :num_workers_business, :integer
    add_column :members, :reason_for_joining, :string
    add_column :members, :is_experienced_with_microfinance, :boolean
  end
end
