class AddTotalPrincipalAmountAndTotalInterestAmountToPaymentCollectionRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collection_records, :total_principal_amount, :decimal
    add_column :payment_collection_records, :total_interest_amount, :decimal
  end
end
