class AddTotalLoanWpToSnapshots < ActiveRecord::Migration[4.2]
  def change
    add_column :snapshots, :total_loan_wp, :decimal
  end
end
