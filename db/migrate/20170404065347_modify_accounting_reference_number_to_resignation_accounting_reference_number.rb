class ModifyAccountingReferenceNumberToResignationAccountingReferenceNumber < ActiveRecord::Migration[4.2]
  def change
    rename_column :members, :accounting_reference_number, :resignation_accounting_reference_number 
  end
end
