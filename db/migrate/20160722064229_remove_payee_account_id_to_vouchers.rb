class RemovePayeeAccountIdToVouchers < ActiveRecord::Migration[4.2]
  def change
  	remove_column :vouchers, :payee_account_id
  end
end
