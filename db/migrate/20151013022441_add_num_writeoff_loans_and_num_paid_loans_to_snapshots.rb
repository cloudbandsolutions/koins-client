class AddNumWriteoffLoansAndNumPaidLoansToSnapshots < ActiveRecord::Migration[4.2]
  def change
    add_column :snapshots, :num_writeoff_loans, :integer
    add_column :snapshots, :num_paid_loans, :integer
  end
end
