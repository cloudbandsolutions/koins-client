class RenameDebitExpenseAccountingCode2 < ActiveRecord::Migration[4.2]
  def change
    rename_column :savings_types, :debit_expense_accounting_code_id, :expense_accounting_code_id
  end
end
