class CreateLoanProductDependencies < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_product_dependencies do |t|
      t.integer :loan_product_id
      t.integer :cycle_count

      t.timestamps null: false
    end
  end
end
