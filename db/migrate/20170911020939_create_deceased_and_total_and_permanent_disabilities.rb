class CreateDeceasedAndTotalAndPermanentDisabilities < ActiveRecord::Migration[4.2]
  def change
    create_table :deceased_and_total_and_permanent_disabilities do |t|
    	t.date :date_prepared
     	t.string :status
     	t.integer :branch_id
     	t.string :prepared_by
     	t.string :approved_by

      t.timestamps null: false
    end
  end
end
