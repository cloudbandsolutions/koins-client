class AddAdditionalFieldsForLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :num_years_business, :integer
    add_column :loans, :num_months_business, :integer
    add_column :loans, :num_workers_business, :integer
    add_column :loans, :reason_for_joining, :text
    add_column :loans, :is_experienced_with_microfinance, :boolean
    add_column :loans, :experience_with_microfinance, :text
  end
end
