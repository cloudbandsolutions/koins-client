class AddLoanProductTypeIdToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :loan_product_type_id, :integer
  end
end
