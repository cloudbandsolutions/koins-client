class AddPaymentAmountToMemberWithdrawalLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :member_withdrawal_loans, :payment_amount, :decimal
  end
end
