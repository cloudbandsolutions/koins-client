class AddMajorGroupAndMajorAccountToAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :accounting_codes, :major_group, :string
    add_column :accounting_codes, :major_account, :string
  end
end
