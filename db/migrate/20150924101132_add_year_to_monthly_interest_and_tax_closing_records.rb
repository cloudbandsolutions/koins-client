class AddYearToMonthlyInterestAndTaxClosingRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :monthly_interest_and_tax_closing_records, :year, :integer
  end
end
