class RemoveFundTransferFromSavingsTypes < ActiveRecord::Migration[4.2]
  def change
    remove_column :savings_types, :fund_transfer_withdraw_accounting_code_id
    remove_column :savings_types, :fund_transfer_deposit_accounting_code_id

    remove_column :insurance_types, :fund_transfer_withdraw_accounting_code_id
    remove_column :insurance_types, :fund_transfer_deposit_accounting_code_id

    remove_column :equity_types, :fund_transfer_withdraw_accounting_code_id
    remove_column :equity_types, :fund_transfer_deposit_accounting_code_id
  end
end
