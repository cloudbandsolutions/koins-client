class ChangeIntegerToString < ActiveRecord::Migration[4.2]
  def change
    remove_column :savings_accounts, :account_number
    remove_column :savings_accounts, :balance
    
    add_column :savings_accounts, :account_number, :string
    add_column :savings_accounts, :balance, :decimal 
  end
end
