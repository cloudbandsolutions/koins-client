class CreateLoanPaymentDetails < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_payment_details do |t|
      t.integer :loan_payment_id
      t.date :paid_at
      t.decimal :total_amount
      t.decimal :paid_principal
      t.decimal :paid_interest
      t.string :payment_status

      t.timestamps null: false
    end
  end
end
