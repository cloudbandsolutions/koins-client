class RemoveSurveyOptionIdFromSurveyRespondentAnswers < ActiveRecord::Migration[4.2]
  def change
    remove_column :survey_respondent_answers, :survey_option_id
    add_column :survey_respondent_answers, :survey_question_option_id, :integer
  end
end
