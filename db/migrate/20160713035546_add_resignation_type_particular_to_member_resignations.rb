class AddResignationTypeParticularToMemberResignations < ActiveRecord::Migration[4.2]
  def change
    add_column :member_resignations, :resignation_type_particular, :string
  end
end
