class AddUuidToEquityAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :equity_account_transactions, :uuid, :string
  end
end
