class ModifySavingTypesAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    remove_column :savings_types, :default_withdraw_dr_account_id
    remove_column :savings_types, :default_withdraw_cr_account_id
    remove_column :savings_types, :default_deposit_dr_account_id
    remove_column :savings_types, :default_deposit_cr_account_id

    add_column :savings_types, :withdraw_accounting_code_id, :integer
    add_column :savings_types, :deposit_accounting_code_id, :integer
  end
end
