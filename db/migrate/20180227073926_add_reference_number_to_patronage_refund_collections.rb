class AddReferenceNumberToPatronageRefundCollections < ActiveRecord::Migration[5.2]
  def change
    add_column :patronage_refund_collections, :reference_number, :string
  end
end
