class AddPaidPrincipalAndPaidInterestToAmmortizationScheduleEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :ammortization_schedule_entries, :paid_principal, :decimal
    add_column :ammortization_schedule_entries, :paid_interest, :decimal
  end
end
