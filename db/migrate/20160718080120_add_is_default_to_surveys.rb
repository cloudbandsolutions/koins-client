class AddIsDefaultToSurveys < ActiveRecord::Migration[4.2]
  def change
    add_column :surveys, :is_default, :boolean
  end
end
