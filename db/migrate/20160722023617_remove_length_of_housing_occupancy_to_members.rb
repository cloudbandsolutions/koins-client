class RemoveLengthOfHousingOccupancyToMembers < ActiveRecord::Migration[4.2]
  def change
  	remove_column :members, :length_of_housing_occupancy
  end
end
