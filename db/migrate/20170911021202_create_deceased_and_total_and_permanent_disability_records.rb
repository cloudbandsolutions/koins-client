class CreateDeceasedAndTotalAndPermanentDisabilityRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :deceased_and_total_and_permanent_disability_records do |t|
		t.date :deceased_tpd_date
		t.string :status
		t.integer :legal_dependent_id
		t.integer :deceased_and_total_and_permanent_disability_id
		t.string :classification

      t.timestamps null: false
    end
  end
end
