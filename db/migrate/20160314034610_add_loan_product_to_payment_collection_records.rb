class AddLoanProductToPaymentCollectionRecords < ActiveRecord::Migration[4.2]
  def change
    add_reference :payment_collection_records, :loan_product, index: true, foreign_key: true
  end
end
