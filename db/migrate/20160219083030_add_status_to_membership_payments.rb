class AddStatusToMembershipPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :membership_payments, :status, :string
  end
end
