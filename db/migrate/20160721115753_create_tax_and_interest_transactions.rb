class CreateTaxAndInterestTransactions < ActiveRecord::Migration[4.2]
  def change
    create_table :tax_and_interest_transactions do |t|
      t.decimal :tax_amount
      t.decimal :interest_amount
      t.references :savings_account, index: { name: 'sa_index' }, foreign_key: true
      t.string :uuid
      t.references :monthly_interest_and_tax_closing_record, index: { name: 'mit_cr_index' }, foreign_key: true
      t.decimal :monthly_tax_rate
      t.decimal :monthly_interest_rate

      t.timestamps null: false
    end
  end
end
