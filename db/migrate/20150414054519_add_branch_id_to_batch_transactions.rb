class AddBranchIdToBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transactions, :branch_id, :integer
  end
end
