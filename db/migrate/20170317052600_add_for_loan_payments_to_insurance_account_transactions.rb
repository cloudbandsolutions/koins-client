class AddForLoanPaymentsToInsuranceAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_account_transactions, :for_loan_payments, :boolean
  end
end
