class RemoveProcessingFeeInvalidTerms < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_products, :processing_fee_5
    remove_column :loan_products, :processing_fee_10
    remove_column :loan_products, :processing_fee_30
    add_column :loan_products, :processing_fee_35, :decimal
  end
end
