class AddTransactionTypeToBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transactions, :transaction_type, :string
  end
end
