class AddMemberTypeToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :member_type, :string
  end
end
