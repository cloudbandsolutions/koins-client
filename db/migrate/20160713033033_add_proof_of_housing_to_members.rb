class AddProofOfHousingToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :proof_of_housing, :string
  end
end
