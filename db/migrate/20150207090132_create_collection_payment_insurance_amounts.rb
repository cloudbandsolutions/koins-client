class CreateCollectionPaymentInsuranceAmounts < ActiveRecord::Migration[4.2]
  def change
    create_table :collection_payment_insurance_amounts do |t|
      t.integer :collection_payment_id
      t.integer :insurance_type_id
      t.decimal :total_amount

      t.timestamps
    end
  end
end
