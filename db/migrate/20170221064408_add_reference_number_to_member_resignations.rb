class AddReferenceNumberToMemberResignations < ActiveRecord::Migration[4.2]
  def change
    add_column :member_resignations, :reference_number, :string
  end
end
