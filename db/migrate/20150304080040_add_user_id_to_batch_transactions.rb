class AddUserIdToBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transactions, :user_id, :integer
  end
end
