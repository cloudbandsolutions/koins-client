class AddCenterToBatchMoratoria < ActiveRecord::Migration[4.2]
  def change
    add_reference :batch_moratoria, :center, index: true, foreign_key: true
  end
end
