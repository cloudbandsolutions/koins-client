class AddCollectionPaymentIdToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :collection_payment_id, :integer
  end
end
