class CreateWithdrawPaymentTransactions < ActiveRecord::Migration[4.2]
  def change
    create_table :withdraw_payment_transactions do |t|
      t.decimal :amount
      t.integer :savings_account_id
      t.integer :member_id
      t.integer :co_maker_id

      t.timestamps
    end
  end
end
