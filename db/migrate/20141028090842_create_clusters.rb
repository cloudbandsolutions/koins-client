class CreateClusters < ActiveRecord::Migration[4.2]
  def change
    create_table :clusters do |t|
      t.string :name
      t.string :abbreviation
      t.string :code
      t.text :address

      t.timestamps
    end
  end
end
