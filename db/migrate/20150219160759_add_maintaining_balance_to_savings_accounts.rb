class AddMaintainingBalanceToSavingsAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_accounts, :maintaining_balance, :decimal
  end
end
