class AddDescriptionToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :description, :text
  end
end
