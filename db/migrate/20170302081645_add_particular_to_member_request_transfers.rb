class AddParticularToMemberRequestTransfers < ActiveRecord::Migration[4.2]
  def change
    add_column :member_transfer_requests, :particular, :string
  end
end
