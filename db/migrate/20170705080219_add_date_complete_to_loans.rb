class AddDateCompleteToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :date_completed, :date
  end
end
