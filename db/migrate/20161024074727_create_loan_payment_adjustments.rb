class CreateLoanPaymentAdjustments < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_payment_adjustments do |t|
      t.string :uuid
      t.string :status
      t.string :prepared_by
      t.string :approved_by
      t.date :date_approved
      t.string :voucher_reference_number
      t.string :particular
      t.references :branch, index: { name: "index_clpa_branch" }, foreign_key: true

      t.timestamps null: false
    end
  end
end
