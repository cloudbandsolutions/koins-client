class CreateLoanPayments < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_payments do |t|
      t.integer :ammortization_schedule_entry_id
      t.decimal :amount
      t.date :paid_at
      t.string :payment_type
      t.string :uuid

      t.timestamps
    end
  end
end
