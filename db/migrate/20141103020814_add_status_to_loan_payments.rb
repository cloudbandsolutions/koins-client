class AddStatusToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :status, :string
  end
end
