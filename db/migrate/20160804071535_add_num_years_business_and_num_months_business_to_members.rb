class AddNumYearsBusinessAndNumMonthsBusinessToMembers < ActiveRecord::Migration[4.2]
  def change
  	add_column :members, :num_years_business, :integer
  	add_column :members, :num_months_business, :integer
  end
end
