class AddStatusFieldToLoanInsuranceRecord < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurance_records, :status, :string
  end
end
