class AddInterestAccountingCodeIdToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :interest_accounting_code_id, :integer
  end
end
