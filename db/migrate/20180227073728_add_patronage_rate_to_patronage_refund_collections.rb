class AddPatronageRateToPatronageRefundCollections < ActiveRecord::Migration[5.2]
  def change
    add_column :patronage_refund_collections, :patronage_rate, :decimal
  end
end
