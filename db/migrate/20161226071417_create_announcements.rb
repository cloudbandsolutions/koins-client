class CreateAnnouncements < ActiveRecord::Migration[4.2]
  def change
    create_table :announcements do |t|
      t.string :title
      t.text :content
      t.date :announced_on
      t.string :announced_by

      t.timestamps null: false
    end
  end
end
