class RemoveLoanProductIdToLoanInsuranceRecord < ActiveRecord::Migration[4.2]
  def change
  	remove_column :loan_insurance_records, :loan_product_id
  end
end
