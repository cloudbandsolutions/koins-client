class CreateSurveyQuestions < ActiveRecord::Migration[4.2]
  def change
    create_table :survey_questions do |t|
      t.references :survey, index: true, foreign_key: true
      t.string :uuid
      t.integer :priority
      t.string :content

      t.timestamps null: false
    end
  end
end
