class RemoveTotalLoanPayablesFromSnapshots < ActiveRecord::Migration[4.2]
  def change
    remove_column :snapshots, :total_loan_payables
  end
end
