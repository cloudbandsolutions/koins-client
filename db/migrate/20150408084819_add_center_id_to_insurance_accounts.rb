class AddCenterIdToInsuranceAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_accounts, :center_id, :integer
  end
end
