class RefactorTVals < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_deduction_entries, :t_val_5
    remove_column :loan_deduction_entries, :t_val_10
    remove_column :loan_deduction_entries, :t_val_30
    add_column :loan_deduction_entries, :t_val_35, :decimal
  end
end
