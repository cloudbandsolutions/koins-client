class CreateBuildVersions < ActiveRecord::Migration[4.2]
  def change
    create_table :build_versions do |t|
      t.string :build_version
      t.date :build_date
      t.string :category
      t.string :content

      t.timestamps null: false
    end
  end
end
