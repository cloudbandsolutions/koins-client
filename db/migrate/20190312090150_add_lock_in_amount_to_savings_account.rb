class AddLockInAmountToSavingsAccount < ActiveRecord::Migration[5.2]
  def change
    add_column :savings_accounts, :lock_in_amount, :decimal
  end
end
