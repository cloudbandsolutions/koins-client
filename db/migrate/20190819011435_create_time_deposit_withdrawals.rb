class CreateTimeDepositWithdrawals < ActiveRecord::Migration[5.2]
  def change
    create_table :time_deposit_withdrawals do |t|
      t.string :refference_number
      t.string :status
      t.string :total_interest
      t.date :posting_date
      t.string :uuid

      t.timestamps
    end
  end
end
