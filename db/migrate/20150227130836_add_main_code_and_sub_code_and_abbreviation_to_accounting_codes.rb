class AddMainCodeAndSubCodeAndAbbreviationToAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :accounting_codes, :main_code, :string
    add_column :accounting_codes, :sub_code, :string
    add_column :accounting_codes, :abbreviation, :string
  end
end
