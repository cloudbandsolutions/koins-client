class AddPostingDateToInterestOnShareCapitalCollection < ActiveRecord::Migration[5.2]
  def change
    add_column :interest_on_share_capital_collections, :posting_date, :date
  end
end
