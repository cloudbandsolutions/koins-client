class AddParticularAndVoucherReferenceNumberAndWpVoucherReferenceNumberToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :particular, :text
    add_column :loan_payments, :voucher_reference_number, :string
    add_column :loan_payments, :wp_voucher_reference_number, :string
  end
end
