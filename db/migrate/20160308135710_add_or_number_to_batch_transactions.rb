class AddOrNumberToBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transactions, :or_number, :string
  end
end
