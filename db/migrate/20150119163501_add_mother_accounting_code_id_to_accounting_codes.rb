class AddMotherAccountingCodeIdToAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :accounting_codes, :mother_accounting_code_id, :integer
  end
end
