class AddLoanIdToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :loan_id, :integer
  end
end
