class CreateSavingsTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :savings_types do |t|
      t.string :name
      t.string :code

      t.timestamps
    end
  end
end
