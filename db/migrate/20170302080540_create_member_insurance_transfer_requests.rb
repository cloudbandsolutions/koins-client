class CreateMemberInsuranceTransferRequests < ActiveRecord::Migration[4.2]
  def change
    create_table :member_insurance_transfer_requests do |t|
      t.references :insurance_account, index: { name: 'mitr_ia_index' }, foreign_key: true
      t.references :member_transfer_request, index: { name: 'mitr_mtr_index' }, foreign_key: true
      t.decimal :amount
      t.integer :dr_accounting_code_id
      t.integer :cr_accounting_code_id

      t.timestamps null: false
    end
  end
end
