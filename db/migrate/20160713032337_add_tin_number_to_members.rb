class AddTinNumberToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :tin_number, :string
  end
end
