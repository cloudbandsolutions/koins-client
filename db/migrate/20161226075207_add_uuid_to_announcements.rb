class AddUuidToAnnouncements < ActiveRecord::Migration[4.2]
  def change
    add_column :announcements, :uuid, :string
  end
end
