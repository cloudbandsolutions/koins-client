class CreateMemberAttachmentFiles < ActiveRecord::Migration[4.2]
  def change
    create_table :member_attachment_files do |t|
    	t.references :member, index: true, foreign_key: true
      t.string :title

      t.timestamps null: false
    end
  end
end
