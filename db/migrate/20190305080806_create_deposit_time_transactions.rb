class CreateDepositTimeTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :deposit_time_transactions do |t|
      t.decimal :amount
      t.string :lock_in_period
      t.string :status
      t.string :uuid

      t.timestamps
    end
  end
end
