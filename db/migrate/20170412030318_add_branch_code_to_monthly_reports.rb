class AddBranchCodeToMonthlyReports < ActiveRecord::Migration[4.2]
  def change
    add_column :monthly_reports, :branch_code, :string
  end
end
