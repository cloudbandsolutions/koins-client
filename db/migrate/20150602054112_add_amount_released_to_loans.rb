class AddAmountReleasedToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :amount_released, :decimal
  end
end
