class AddUserToCenters < ActiveRecord::Migration[4.2]
  def change
    add_reference :centers, :user, index: true, foreign_key: true
  end
end
