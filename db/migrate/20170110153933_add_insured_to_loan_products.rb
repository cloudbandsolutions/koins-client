class AddInsuredToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :insured, :boolean
  end
end
