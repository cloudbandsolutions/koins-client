class AddOverrideFixedTermToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :override_fixed_term, :boolean
  end
end
