class RemoveMaturityDateFromLoans < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :maturity_date
  end
end
