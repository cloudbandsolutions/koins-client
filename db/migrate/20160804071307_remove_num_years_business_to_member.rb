class RemoveNumYearsBusinessToMember < ActiveRecord::Migration[4.2]
  def change
  	remove_column :members, :num_years_business
  end
end
