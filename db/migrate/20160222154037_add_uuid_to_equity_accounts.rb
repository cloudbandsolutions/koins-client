class AddUuidToEquityAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :equity_accounts, :uuid, :string
  end
end
