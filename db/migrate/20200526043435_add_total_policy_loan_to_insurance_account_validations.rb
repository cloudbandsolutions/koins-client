class AddTotalPolicyLoanToInsuranceAccountValidations < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_validations, :total_policy_loan, :decimal
  end
end
