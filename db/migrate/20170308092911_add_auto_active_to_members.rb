class AddAutoActiveToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :auto_active, :boolean
  end
end
