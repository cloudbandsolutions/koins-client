class CreateNegativeLoanPayments < ActiveRecord::Migration[4.2]
  def change
    create_table :negative_loan_payments do |t|
      t.references :loan, index: true, foreign_key: true
      t.decimal :amount
      t.references :loan_payment, index: true, foreign_key: true
      t.string :status
      t.string :generated_by
      t.string :approved_by
      t.date :transacted_at

      t.timestamps null: false
    end
  end
end
