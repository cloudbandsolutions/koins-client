class AddAccountingFundIdToSubsidiaryAdjusments < ActiveRecord::Migration[4.2]
  def change
    add_column :subsidiary_adjustments, :accounting_fund_id, :integer
  end
end
