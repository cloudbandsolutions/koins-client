class AddDateApprovedToSubsidiaryAdjustmentRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :subsidiary_adjustment_records, :date_approved, :date
  end
end
