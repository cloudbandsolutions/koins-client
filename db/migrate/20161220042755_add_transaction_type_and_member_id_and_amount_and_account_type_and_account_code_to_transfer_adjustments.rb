class AddTransactionTypeAndMemberIdAndAmountAndAccountTypeAndAccountCodeToTransferAdjustments < ActiveRecord::Migration[4.2]
  def change
    add_column :transfer_adjustments, :transaction_type, :string
    add_column :transfer_adjustments, :member_id, :integer
    add_column :transfer_adjustments, :amount, :decimal
    add_column :transfer_adjustments, :account_type, :string
    add_column :transfer_adjustments, :account_code, :string
  end
end
