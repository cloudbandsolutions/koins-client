class CreateReinstatementRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :reinstatement_records do |t|
      t.date :reinstatement_date
      t.string :status
      t.integer :member_id
      t.integer :reinstatement_id
      t.date :old_recognition_date

      t.timestamps null: false
    end
  end
end
