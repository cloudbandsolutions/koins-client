class AddMajorGroupIdToMajorAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :major_accounts, :major_group_id, :integer
  end
end
