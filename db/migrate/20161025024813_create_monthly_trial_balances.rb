class CreateMonthlyTrialBalances < ActiveRecord::Migration[4.2]
  def change
    create_table :monthly_trial_balances do |t|
      t.date :start_date
      t.date :end_date
      t.json :data

      t.timestamps null: false
    end
  end
end
