class AddAccountingCodeTransactionIdToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :accounting_code_transaction_id, :integer
  end
end
