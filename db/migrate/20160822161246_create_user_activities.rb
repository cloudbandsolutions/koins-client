class CreateUserActivities < ActiveRecord::Migration[4.2]
  def change
    create_table :user_activities do |t|
      t.integer :user_id
      t.string :first_name
      t.string :last_name
      t.string :uuid
      t.text :content
      t.integer :record_reference_id
      t.string :username
      t.string :email

      t.timestamps null: false
    end
  end
end
