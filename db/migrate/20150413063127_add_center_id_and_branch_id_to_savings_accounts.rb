class AddCenterIdAndBranchIdToSavingsAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_accounts, :center_id, :integer
    add_column :savings_accounts, :branch_id, :integer
  end
end
