class AddWithdrawAccountingCodeIdAndDepositAccountingCodeIdAndDueAccountingCodeIdToSavingsTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_types, :withdraw_accounting_code_id, :integer
    add_column :savings_types, :deposit_accounting_code_id, :integer
    add_column :savings_types, :due_accounting_code_id, :integer
  end
end
