class AddIsYearEndClosingToJournalEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :journal_entries, :is_year_end_closing, :boolean
    add_column :vouchers, :is_year_end_closing, :boolean
  end
end
