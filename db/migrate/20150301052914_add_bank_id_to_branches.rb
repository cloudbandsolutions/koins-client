class AddBankIdToBranches < ActiveRecord::Migration[4.2]
  def change
    add_column :branches, :bank_id, :integer
  end
end
