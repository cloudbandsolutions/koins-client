class AddAsOfToPatronageRefundCollections < ActiveRecord::Migration[5.2]
  def change
    add_column :patronage_refund_collections, :as_of, :date
  end
end
