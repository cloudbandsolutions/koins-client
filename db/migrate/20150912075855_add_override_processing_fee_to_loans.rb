class AddOverrideProcessingFeeToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :override_processing_fee, :boolean
  end
end
