class AddParticularToCenterTransferRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :center_transfer_records, :particular, :text
  end
end
