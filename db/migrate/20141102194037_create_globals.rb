class CreateGlobals < ActiveRecord::Migration[4.2]
  def change
    create_table :globals do |t|
      t.integer :loan_processing_fee_account_code_id

      t.timestamps
    end
  end
end
