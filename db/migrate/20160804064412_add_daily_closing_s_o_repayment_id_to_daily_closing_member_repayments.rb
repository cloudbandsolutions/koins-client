class AddDailyClosingSORepaymentIdToDailyClosingMemberRepayments < ActiveRecord::Migration[4.2]
  def change
    add_column :daily_closing_member_repayments, :daily_closing_s_o_repayment_id, :integer
  end
end
