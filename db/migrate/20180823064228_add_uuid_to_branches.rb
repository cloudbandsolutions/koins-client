class AddUuidToBranches < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :uuid, :string
  end
end
