class CreateMembershipAccountPayments < ActiveRecord::Migration[4.2]
  def change
    create_table :membership_account_payments do |t|
      t.references :membership_payment, index: true, foreign_key: true
      t.string :account_type
      t.decimal :amount
      t.string :deposit_type

      t.timestamps null: false
    end
  end
end
