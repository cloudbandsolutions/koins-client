class CreateBackgroundOperations < ActiveRecord::Migration[5.2]
  def change
    create_table :background_operations do |t|
      t.string :status
      t.datetime :started_at
      t.datetime :ended_at
      t.string :operation_type
      t.json :data

      t.timestamps
    end
  end
end
