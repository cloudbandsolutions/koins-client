class AddDebitAccountingCodeIdToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :debit_accounting_code_id, :integer
  end
end
