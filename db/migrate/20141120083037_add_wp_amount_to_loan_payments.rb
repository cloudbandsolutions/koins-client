class AddWpAmountToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :wp_amount, :decimal
  end
end
