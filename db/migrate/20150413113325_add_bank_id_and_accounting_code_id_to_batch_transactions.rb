class AddBankIdAndAccountingCodeIdToBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transactions, :bank_id, :integer
    add_column :batch_transactions, :accounting_code_id, :integer
  end
end
