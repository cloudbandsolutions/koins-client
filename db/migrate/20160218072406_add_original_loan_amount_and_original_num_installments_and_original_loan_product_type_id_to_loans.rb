class AddOriginalLoanAmountAndOriginalNumInstallmentsAndOriginalLoanProductTypeIdToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :original_loan_amount, :decimal
    add_column :loans, :original_num_installments, :integer
    add_column :loans, :original_loan_product_type_id, :integer
  end
end
