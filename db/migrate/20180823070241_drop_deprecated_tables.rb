class DropDeprecatedTables < ActiveRecord::Migration[5.2]
  def change
    drop_table :withdraw_payment_transactions
    drop_table :wp_records
  end
end
