class AddProcessingFeeToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :processing_fee, :decimal
  end
end
