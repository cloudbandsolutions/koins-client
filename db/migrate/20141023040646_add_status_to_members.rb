class AddStatusToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :status, :string
  end
end
