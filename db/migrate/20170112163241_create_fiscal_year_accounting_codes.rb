class CreateFiscalYearAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    create_table :fiscal_year_accounting_codes do |t|
      t.integer :year
      t.references :accounting_code, index: true, foreign_key: true
      t.decimal :beginning_balance
      t.decimal :ending_balance
      t.decimal :beginning_credit
      t.decimal :ending_credit

      t.timestamps null: false
    end
  end
end
