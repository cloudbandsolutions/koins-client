class CreateBeneficiaries < ActiveRecord::Migration[4.2]
  def change
    create_table :beneficiaries do |t|
      t.string :first_name
      t.string :last_name
      t.string :middle_name
      t.string :relationship
      t.date :date_of_birth
      t.boolean :is_primary
      t.integer :member_id

      t.timestamps
    end
  end
end
