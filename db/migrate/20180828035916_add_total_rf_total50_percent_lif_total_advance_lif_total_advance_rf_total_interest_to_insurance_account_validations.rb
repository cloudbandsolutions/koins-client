class AddTotalRfTotal50PercentLifTotalAdvanceLifTotalAdvanceRfTotalInterestToInsuranceAccountValidations < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_validations, :total_rf, :decimal
  	add_column :insurance_account_validations, :total_50_percent_lif, :decimal
  	add_column :insurance_account_validations, :total_advance_lif, :decimal
  	add_column :insurance_account_validations, :total_advance_rf, :decimal
  	add_column :insurance_account_validations, :total_interest, :decimal
  end
end
