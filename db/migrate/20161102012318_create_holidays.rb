class CreateHolidays < ActiveRecord::Migration[4.2]
  def change
    create_table :holidays do |t|
      t.string :name
      t.date :holiday_at

      t.timestamps null: false
    end
  end
end
