class AddReverseVoucherReferenceNumberToMonthlyInterestAndTaxClosingRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :monthly_interest_and_tax_closing_records, :reverse_voucher_reference_number, :string
  end
end
