class AddBankIdToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :bank_id, :integer
  end
end
