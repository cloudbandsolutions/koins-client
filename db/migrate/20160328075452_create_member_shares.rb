class CreateMemberShares < ActiveRecord::Migration[4.2]
  def change
    create_table :member_shares do |t|
      t.string :uuid
      t.string :certificate_number
      t.date :date_of_issue
      t.integer :number_of_shares
      t.references :member, index: true, foreign_key: true
      t.integer :no_original_shares
      t.string :no_of_shares_transferred
      t.integer :no_original_certificate

      t.timestamps null: false
    end
  end
end
