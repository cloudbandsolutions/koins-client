class AddAmountPayableToCreditorToClipClaims < ActiveRecord::Migration[5.2]
  def change
  	add_column :clip_claims, :amount_payable_to_creditor, :decimal
  end
end
