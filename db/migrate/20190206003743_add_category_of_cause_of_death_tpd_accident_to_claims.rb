class AddCategoryOfCauseOfDeathTpdAccidentToClaims < ActiveRecord::Migration[5.2]
  def change
  	add_column :claims, :category_of_cause_of_death_tpd_accident, :string
  end
end
