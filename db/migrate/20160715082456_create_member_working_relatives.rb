class CreateMemberWorkingRelatives < ActiveRecord::Migration[4.2]
  def change
    create_table :member_working_relatives do |t|
      t.references :member, index: true, foreign_key: true
      t.string :name
      t.string :relationship

      t.timestamps null: false
    end
  end
end
