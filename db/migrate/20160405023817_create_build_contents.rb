class CreateBuildContents < ActiveRecord::Migration[4.2]
  def change
    create_table :build_contents do |t|
      t.text :content

      t.timestamps null: false
    end
  end
end
