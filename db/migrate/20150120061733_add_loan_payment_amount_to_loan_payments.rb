class AddLoanPaymentAmountToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :loan_payment, :decimal
  end
end
