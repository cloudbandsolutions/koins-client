class AddMaxLoanCycleToLoanInsuranceDeductionEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_insurance_deduction_entries, :max_loan_cycle, :integer
  end
end
