class CreateLoanInsuranceTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_insurance_types do |t|
    	t.string :name

      t.timestamps null: false
    end
  end
end
