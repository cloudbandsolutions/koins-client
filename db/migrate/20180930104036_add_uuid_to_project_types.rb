class AddUuidToProjectTypes < ActiveRecord::Migration[5.2]
  def change
    add_column :project_types, :uuid, :string
  end
end
