class CreatePaymentMembershipCollections < ActiveRecord::Migration[4.2]
  def change
    create_table :payment_membership_collections do |t|
      t.string :reference_number
      t.text :particular
      t.string :uuid
      t.string :or_number
      t.decimal :total_amount
      t.decimal :total_membership_amount
      t.string :total_cp_amount
      t.decimal :total_deposit_amount
      t.references :branch, index: true, foreign_key: true
      t.references :center, index: true, foreign_key: true
      t.string :prepared_by
      t.string :approved_by
      t.string :status
      t.date :paid_at
      t.string :reversed_reference_number

      t.timestamps null: false
    end
  end
end
