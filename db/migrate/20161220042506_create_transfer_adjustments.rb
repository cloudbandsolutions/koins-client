class CreateTransferAdjustments < ActiveRecord::Migration[4.2]
  def change
    create_table :transfer_adjustments do |t|
      t.string :uuid
      t.string :status
      t.string :prepared_by
      t.string :approved_by
      t.date :date_approved
      t.string :accounting_entry_reference_number
      t.string :particular
      t.references :branch, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
