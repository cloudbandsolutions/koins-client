class CreateUserBranches < ActiveRecord::Migration[4.2]
  def change
    create_table :user_branches, id: false do |t|
      t.references :user
      t.references :branch
    end

    add_index :user_branches, [:user_id, :branch_id], unique: true, name: 'user_branch_index'
  end
end
