class AddPayorToVouchers < ActiveRecord::Migration[4.2]
  def change
    add_column :vouchers, :payor, :string
  end
end
