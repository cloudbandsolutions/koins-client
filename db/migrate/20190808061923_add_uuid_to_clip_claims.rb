class AddUuidToClipClaims < ActiveRecord::Migration[5.2]
  def change
    add_column :clip_claims, :uuid, :string
  end
end
