class CreateMonthlyInterestAndTaxClosingRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :monthly_interest_and_tax_closing_records do |t|
      t.integer :month
      t.datetime :closed_at
      t.date :closing_date
      t.decimal :total_interest
      t.decimal :total_tax

      t.timestamps null: false
    end
  end
end
