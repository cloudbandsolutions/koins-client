class AddResignationTypeAndResignationCodeAndResignationReasonToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :resignation_type, :string
    add_column :members, :resignation_code, :string
    add_column :members, :resignation_reason, :string
  end
end
