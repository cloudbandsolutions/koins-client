class AddSoMemberCountsToDailyReports < ActiveRecord::Migration[4.2]
  def change
    add_column :daily_reports, :so_member_counts, :json
  end
end
