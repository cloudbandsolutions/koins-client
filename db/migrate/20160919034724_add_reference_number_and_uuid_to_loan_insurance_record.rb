class AddReferenceNumberAndUuidToLoanInsuranceRecord < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurance_records, :uuid, :string
  	add_column :loan_insurance_records, :reference_number, :string
  end
end
