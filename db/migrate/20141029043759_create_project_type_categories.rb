class CreateProjectTypeCategories < ActiveRecord::Migration[4.2]
  def change
    create_table :project_type_categories do |t|
      t.string :name
      t.string :code

      t.timestamps
    end
  end
end
