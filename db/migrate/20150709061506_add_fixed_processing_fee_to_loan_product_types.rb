class AddFixedProcessingFeeToLoanProductTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_product_types, :fixed_processing_fee, :decimal
  end
end
