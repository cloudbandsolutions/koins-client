class AddUuidToRequestRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :request_records, :uuid, :string
  end
end
