class AddDateRequestedToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :date_requested, :date
  end
end
