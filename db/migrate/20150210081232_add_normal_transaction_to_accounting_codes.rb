class AddNormalTransactionToAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :accounting_codes, :normal_transaction, :string
  end
end
