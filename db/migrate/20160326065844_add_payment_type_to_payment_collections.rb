class AddPaymentTypeToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :payment_type, :string
  end
end
