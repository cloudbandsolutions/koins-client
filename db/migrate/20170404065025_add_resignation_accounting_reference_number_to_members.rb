class AddResignationAccountingReferenceNumberToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :accounting_reference_number, :string
  end
end
