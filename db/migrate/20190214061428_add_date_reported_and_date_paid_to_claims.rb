class AddDateReportedAndDatePaidToClaims < ActiveRecord::Migration[5.2]
  def change
    add_column :claims, :date_reported, :date
    add_column :claims, :date_paid, :date
  end
end
