class AddClusterIdToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :cluster_id, :integer
  end
end
