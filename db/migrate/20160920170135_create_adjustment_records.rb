class CreateAdjustmentRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :subsidiary_adjustment_records do |t|
      t.references :member, index: true, foreign_key: true
      t.references :subsidiary_adjustment, index: true, foreign_key: true
      t.string :adjustment_type
      t.string :account_code
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
