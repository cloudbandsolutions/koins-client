class AddForAdjustmentToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :for_adjustment, :boolean
  end
end
