class AddRoleToUserActivities < ActiveRecord::Migration[4.2]
  def change
    add_column :user_activities, :role, :string
  end
end
