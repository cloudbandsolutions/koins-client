class RemoveBatchTransactionRecords < ActiveRecord::Migration[4.2]
  def change
    drop_table :batch_transaction_records
  end
end
