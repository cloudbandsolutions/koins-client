class CreateInsuranceAccountValidationCancellations < ActiveRecord::Migration[5.2]
  def change
    create_table :insurance_account_validation_cancellations do |t|
      t.integer :member_id
      t.date :date_cancelled
      t.text :reason

      t.timestamps null: false
    end
  end
end
