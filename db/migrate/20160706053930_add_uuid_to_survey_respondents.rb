class AddUuidToSurveyRespondents < ActiveRecord::Migration[4.2]
  def change
    add_column :survey_respondents, :uuid, :string
  end
end
