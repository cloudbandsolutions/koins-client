class AddTypeOfHousingToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :type_of_housing, :string
  end
end
