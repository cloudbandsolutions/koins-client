class RemoveRelationshipToLegalDependents < ActiveRecord::Migration[4.2]
  def change
  	remove_column :legal_dependents, :relationship
  end
end
