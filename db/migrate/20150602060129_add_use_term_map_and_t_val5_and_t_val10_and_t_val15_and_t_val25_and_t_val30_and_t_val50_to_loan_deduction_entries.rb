class AddUseTermMapAndTVal5AndTVal10AndTVal15AndTVal25AndTVal30AndTVal50ToLoanDeductionEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_deduction_entries, :use_term_map, :boolean
    add_column :loan_deduction_entries, :t_val_5, :decimal
    add_column :loan_deduction_entries, :t_val_10, :decimal
    add_column :loan_deduction_entries, :t_val_15, :decimal
    add_column :loan_deduction_entries, :t_val_25, :decimal
    add_column :loan_deduction_entries, :t_val_30, :decimal
    add_column :loan_deduction_entries, :t_val_50, :decimal
  end
end
