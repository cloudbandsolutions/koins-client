class AddWatchlistToDailyReports < ActiveRecord::Migration[4.2]
  def change
    add_column :daily_reports, :watchlist, :json
  end
end
