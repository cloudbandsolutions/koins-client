class AddUuidToInsuranceAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_accounts, :uuid, :string
  end
end
