class AddReasonToLoanMoratoria < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_moratoria, :reason, :text
  end
end
