class AddIsVoidToAmmortizationScheduleEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :ammortization_schedule_entries, :is_void, :boolean
  end
end
