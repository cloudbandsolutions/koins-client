class CreateSurveys < ActiveRecord::Migration[4.2]
  def change
    create_table :surveys do |t|
      t.string :uuid
      t.string :title

      t.timestamps null: false
    end
  end
end
