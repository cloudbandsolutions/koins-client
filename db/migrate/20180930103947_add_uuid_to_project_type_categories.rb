class AddUuidToProjectTypeCategories < ActiveRecord::Migration[5.2]
  def change
    add_column :project_type_categories, :uuid, :string
  end
end
