class AddParticularAndReferenceNumberToInsuranceAccountValidations < ActiveRecord::Migration[4.2]
  def change
  	add_column :insurance_account_validations, :particular, :text
	add_column :insurance_account_validations, :reference_number, :string
  end
end
