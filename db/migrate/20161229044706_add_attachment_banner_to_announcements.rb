class AddAttachmentBannerToAnnouncements < ActiveRecord::Migration[4.2]
  def self.up
    change_table :announcements do |t|
      t.attachment :banner
    end
  end

  def self.down
    remove_attachment :announcements, :banner
  end
end
