class AddForExitAgeToInsuranceAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_account_transactions, :for_exit_age, :boolean
  end
end
