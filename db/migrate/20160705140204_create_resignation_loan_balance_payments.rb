class CreateResignationLoanBalancePayments < ActiveRecord::Migration[4.2]
  def change
    create_table :resignation_loan_balance_payments do |t|
      t.string :account_type
      t.string :account_type_code
      t.references :resignation_loan_balance, index: { name: 'rlb_index' }, foreign_key: true
      t.decimal :amount
      t.string :uuid

      t.timestamps null: false
    end
  end
end
