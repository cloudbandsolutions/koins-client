class AddUuidToClusters < ActiveRecord::Migration[5.2]
  def change
    add_column :clusters, :uuid, :string
  end
end
