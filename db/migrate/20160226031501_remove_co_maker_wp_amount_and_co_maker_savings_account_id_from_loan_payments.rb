class RemoveCoMakerWpAmountAndCoMakerSavingsAccountIdFromLoanPayments < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_payments, :co_maker_wp_amount
    remove_column :loan_payments, :co_maker_savings_account_id
  end
end
