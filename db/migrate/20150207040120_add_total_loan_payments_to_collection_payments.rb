class AddTotalLoanPaymentsToCollectionPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :collection_payments, :total_loan_payments, :decimal
  end
end
