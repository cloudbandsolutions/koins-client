class AddAccountingCodeIdToBanks < ActiveRecord::Migration[4.2]
  def change
    add_column :banks, :accounting_code_id, :integer
  end
end
