class AddCoMakerWpAmountAndCoMakerSavingsAccountIdToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :co_maker_wp_amount, :decimal
    add_column :loan_payments, :co_maker_savings_account_id, :integer
  end
end
