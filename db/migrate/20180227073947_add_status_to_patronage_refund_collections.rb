class AddStatusToPatronageRefundCollections < ActiveRecord::Migration[5.2]
  def change
    add_column :patronage_refund_collections, :status, :string
  end
end
