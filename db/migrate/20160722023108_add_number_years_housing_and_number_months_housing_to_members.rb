class AddNumberYearsHousingAndNumberMonthsHousingToMembers < ActiveRecord::Migration[4.2]
  def change
  	add_column :members, :num_years_housing, :integer
  	add_column :members, :num_months_housing, :integer
  end
end
