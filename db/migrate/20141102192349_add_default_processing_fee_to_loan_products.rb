class AddDefaultProcessingFeeToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :default_processing_fee, :decimal
  end
end
