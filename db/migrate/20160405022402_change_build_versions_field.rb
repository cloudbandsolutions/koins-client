class ChangeBuildVersionsField < ActiveRecord::Migration[4.2]
  def change
  	remove_column :build_versions, :category
  	remove_column :build_versions, :content
  	rename_column :build_versions, :build_version, :build_number
  	add_column :build_versions, :category_id, :integer
  end
end
