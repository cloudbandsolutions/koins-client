class AddIsRemoteToInsuranceAccountValidations < ActiveRecord::Migration[5.2]
  def change
    add_column :insurance_account_validations, :is_remote, :boolean
  end
end
