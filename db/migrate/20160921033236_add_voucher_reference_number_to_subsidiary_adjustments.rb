class AddVoucherReferenceNumberToSubsidiaryAdjustments < ActiveRecord::Migration[4.2]
  def change
    add_column :subsidiary_adjustments, :voucher_reference_number, :string
  end
end
