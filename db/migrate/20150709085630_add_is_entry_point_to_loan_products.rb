class AddIsEntryPointToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :is_entry_point, :boolean
  end
end
