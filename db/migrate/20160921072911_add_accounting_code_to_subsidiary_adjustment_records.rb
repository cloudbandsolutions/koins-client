class AddAccountingCodeToSubsidiaryAdjustmentRecords < ActiveRecord::Migration[4.2]
  def change
    add_reference :subsidiary_adjustment_records, :accounting_code, index: true, foreign_key: true
  end
end
