class RemoveWithdrawComplimentaryAccountingCodeId < ActiveRecord::Migration[4.2]
  def change
    remove_column :savings_types, :withdraw_complimentary_accounting_code_id
  end
end
