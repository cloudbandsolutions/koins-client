class CreateMemberSavingsTransferRequests < ActiveRecord::Migration[4.2]
  def change
    create_table :member_savings_transfer_requests do |t|
      t.references :savings_account, index: { name: 'mstr_sa_index' }, foreign_key: true
      t.references :member_transfer_request, index: { name: 'mstr_mtr_index' }, foreign_key: true
      t.decimal :amount
      t.integer :dr_accounting_code_id
      t.integer :cr_accounting_code_id

      t.timestamps null: false
    end
  end
end
