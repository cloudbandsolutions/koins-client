class RemoveActiveLoansByLoanProductToDailyReports < ActiveRecord::Migration[4.2]
  def change
    remove_column :daily_reports, :active_loans_by_loan_product_to_daily_reports
  end
end
