class AddIsVoidToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :is_void, :boolean
  end
end
