class AddOverrideInterestRateToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :override_interest_rate, :decimal
  end
end
