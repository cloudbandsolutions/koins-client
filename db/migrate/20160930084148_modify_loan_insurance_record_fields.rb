class ModifyLoanInsuranceRecordFields < ActiveRecord::Migration[4.2]
  def change
  	remove_column :loan_insurance_records, :status
  	add_column :loan_insurance_records, :loan_insurance_id, :integer
  end
end
