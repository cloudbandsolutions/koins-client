class AddAmountReleasedAccountingCodeIdAndAmountReceivedAccountingCodeIdToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :amount_released_accounting_code_id, :integer
    add_column :loan_products, :amount_received_accounting_code_id, :integer
  end
end
