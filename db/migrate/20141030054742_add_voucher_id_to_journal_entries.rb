class AddVoucherIdToJournalEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :journal_entries, :voucher_id, :integer
  end
end
