class AddAccountingCodeTransactionComplementIdToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :accounting_code_transaction_complement_id, :integer
  end
end
