class ModifyMembershipTypes < ActiveRecord::Migration[4.2]
  def change
    remove_column :membership_types, :require_savings_deposit
    remove_column :membership_types, :require_insurance_deposit
    remove_column :membership_types, :require_equity_deposit
  end
end
