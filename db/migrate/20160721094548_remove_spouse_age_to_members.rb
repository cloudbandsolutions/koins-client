class RemoveSpouseAgeToMembers < ActiveRecord::Migration[4.2]
  def change
  	remove_column :members, :spouse_age
  end
end
