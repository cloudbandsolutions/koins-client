class AddBeginningBalanceAndEndingBalanceToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :beginning_balance, :decimal
    add_column :savings_account_transactions, :ending_balance, :decimal
  end
end
