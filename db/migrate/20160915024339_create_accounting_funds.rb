class CreateAccountingFunds < ActiveRecord::Migration[4.2]
  def change
    create_table :accounting_funds do |t|
    	t.string :name

      t.timestamps null: false
    end
  end
end
