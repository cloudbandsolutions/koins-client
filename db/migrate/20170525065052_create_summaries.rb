class CreateSummaries < ActiveRecord::Migration[4.2]
  def change
    create_table :summaries do |t|
      t.date :as_of
      t.date :start_date
      t.date :end_date
      t.string :summary_type
      t.json :meta

      t.timestamps null: false
    end
  end
end
