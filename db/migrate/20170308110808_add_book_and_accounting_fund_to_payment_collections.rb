class AddBookAndAccountingFundToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :book, :string
    add_reference :payment_collections, :accounting_fund, index: true, foreign_key: true
  end
end
