class AddAccountingCodeTransactionIdToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :accounting_code_transaction_id
    add_column :loan_products, :accounting_code_transaction_id, :integer
  end
end
