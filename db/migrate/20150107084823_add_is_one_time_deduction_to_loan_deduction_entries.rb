class AddIsOneTimeDeductionToLoanDeductionEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_deduction_entries, :is_one_time_deduction, :boolean
  end
end
