class AddBeneficiaryForToBeneficiary < ActiveRecord::Migration[4.2]
  def change
  	add_column :beneficiaries, :beneficiary_for, :string
  end
end
