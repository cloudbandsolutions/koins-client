class CreateDailyClosingBranchRepayments < ActiveRecord::Migration[4.2]
  def change
    create_table :daily_closing_branch_repayments do |t|
      t.string :uuid
      t.references :daily_closing, index: { name: "index_dcbr_dc" }, foreign_key: true
      t.string :name
      t.integer :num_members
      t.string :loan_product_code
      t.decimal :loan_amount
      t.decimal :principal_paid
      t.decimal :loan_balance
      t.decimal :interest_amount
      t.decimal :interest_paid
      t.decimal :interest_balance
      t.decimal :total_paid
      t.decimal :cummulative_due
      t.decimal :amount_past_due
      t.decimal :repayment_rate

      t.timestamps null: false
    end
  end
end
