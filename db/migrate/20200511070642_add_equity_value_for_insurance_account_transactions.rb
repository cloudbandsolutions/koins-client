class AddEquityValueForInsuranceAccountTransactions < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_transactions, :equity_value, :decimal
  end
end
