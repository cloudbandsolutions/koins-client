class AddMembershipCategoryToMembershipTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :membership_types, :membership_category, :string
  end
end
