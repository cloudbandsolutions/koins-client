class CreateMemberWeeklyExpenses < ActiveRecord::Migration[4.2]
  def change
    create_table :member_weekly_expenses do |t|
      t.references :member, index: true, foreign_key: true
      t.decimal :amount
      t.string :source

      t.timestamps null: false
    end
  end
end
