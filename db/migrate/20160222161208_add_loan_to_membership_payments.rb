class AddLoanToMembershipPayments < ActiveRecord::Migration[4.2]
  def change
    remove_column :member_membership_payments, :loan_id
    add_reference :membership_payments, :loan, index: true, foreign_key: true
  end
end
