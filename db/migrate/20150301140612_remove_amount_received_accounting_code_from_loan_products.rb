class RemoveAmountReceivedAccountingCodeFromLoanProducts < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_products, :amount_received_accounting_code_id
  end
end
