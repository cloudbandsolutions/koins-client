class AddIsDeceasedToBeneficiaries < ActiveRecord::Migration[4.2]
  def change
    add_column :beneficiaries, :is_deceased, :boolean
  end
end
