class CreateBatchTransactionRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :batch_transaction_records do |t|
      t.string :uuid
      t.datetime :transacted_at
      t.date :transaction_date
      t.decimal :total_amount
      t.string :transaction_type
      t.integer :branch_id

      t.timestamps null: false
    end
  end
end
