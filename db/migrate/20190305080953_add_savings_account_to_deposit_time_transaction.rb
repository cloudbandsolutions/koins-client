class AddSavingsAccountToDepositTimeTransaction < ActiveRecord::Migration[5.2]
  def change
    add_reference :deposit_time_transactions, :savings_account, foreign_key: true
  end
end
