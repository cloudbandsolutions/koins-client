class CreateLegalDependents < ActiveRecord::Migration[4.2]
  def change
    create_table :legal_dependents do |t|
      t.string :first_name
      t.string :middle_name
      t.date :date_of_birth
      t.string :relationship
      t.integer :member_id

      t.timestamps
    end
  end
end
