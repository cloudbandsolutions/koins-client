class AddCpAmountToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :cp_amount, :decimal
  end
end
