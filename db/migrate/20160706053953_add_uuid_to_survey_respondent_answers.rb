class AddUuidToSurveyRespondentAnswers < ActiveRecord::Migration[4.2]
  def change
    add_column :survey_respondent_answers, :uuid, :string
  end
end
