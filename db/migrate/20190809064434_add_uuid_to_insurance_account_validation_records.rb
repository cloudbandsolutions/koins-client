class AddUuidToInsuranceAccountValidationRecords < ActiveRecord::Migration[5.2]
  def change
    add_column :insurance_account_validation_records, :uuid, :string
  end
end
