class AddDataForInsuranceAccountTransactions < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_transactions, :data, :json
  end
end
