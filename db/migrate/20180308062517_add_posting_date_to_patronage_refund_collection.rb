class AddPostingDateToPatronageRefundCollection < ActiveRecord::Migration[5.2]
  def change
    add_column :patronage_refund_collections, :posting_date, :date
  end
end
