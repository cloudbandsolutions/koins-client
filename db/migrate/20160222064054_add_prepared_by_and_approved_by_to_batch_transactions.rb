class AddPreparedByAndApprovedByToBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transactions, :prepared_by, :string
    add_column :batch_transactions, :approved_by, :string
  end
end
