class CreateSubsidiaryAdjustmentRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :subsidiary_adjustment_records do |t|
      t.string :uuid
      t.string :status
      t.string :prepared_by
      t.string :approved_by

      t.timestamps null: false
    end
  end
end
