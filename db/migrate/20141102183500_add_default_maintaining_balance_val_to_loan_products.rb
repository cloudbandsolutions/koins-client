class AddDefaultMaintainingBalanceValToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :default_maintaining_balance_val, :decimal
  end
end
