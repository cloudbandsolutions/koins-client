class AddActivatedForInsuranceAndActivatedForLoansToMembershipTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :membership_types, :activated_for_insurance, :boolean
    add_column :membership_types, :activated_for_loans, :boolean
  end
end
