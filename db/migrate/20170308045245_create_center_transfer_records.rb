class CreateCenterTransferRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :center_transfer_records do |t|
      t.string :uuid
      t.references :branch, index: true, foreign_key: true
      t.references :center, index: true, foreign_key: true
      t.string :reference_number
      t.string :status
      t.json :data
      t.string :prepared_by
      t.string :approved_by

      t.timestamps null: false
    end
  end
end
