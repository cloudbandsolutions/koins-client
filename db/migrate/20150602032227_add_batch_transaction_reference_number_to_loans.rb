class AddBatchTransactionReferenceNumberToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :batch_transaction_reference_number, :string
  end
end
