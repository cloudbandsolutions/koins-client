class CreateBuildCategories < ActiveRecord::Migration[4.2]
  def change
    create_table :build_categories do |t|
      t.string :category
      t.integer :build_content_id

      t.timestamps null: false
    end
  end
end
