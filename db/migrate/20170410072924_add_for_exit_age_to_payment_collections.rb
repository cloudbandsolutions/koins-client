class AddForExitAgeToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :for_exit_age, :boolean
  end
end
