class AddCollectionTypeToLoanInsurance < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurances, :collection_type, :string
  end
end
