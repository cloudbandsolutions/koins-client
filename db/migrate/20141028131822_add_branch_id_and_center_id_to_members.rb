class AddBranchIdAndCenterIdToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :branch_id, :integer
    add_column :members, :center_id, :integer
  end
end
