class AddRelationshipToLegalDependents < ActiveRecord::Migration[4.2]
  def change
  	add_column :legal_dependents, :relationship, :string
  end
end
