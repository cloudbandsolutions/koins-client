class AddUuidToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :uuid, :string
  end
end
