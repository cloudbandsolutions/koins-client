class AddLoanCategoryToLoanInsuranceRecord < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurance_records, :loan_category, :string
  end
end
