class AddTotalSavingsAmountToPatronageRefundCollection < ActiveRecord::Migration[5.2]
  def change
    add_column :patronage_refund_collections, :total_savings_amount, :string
  end
end
