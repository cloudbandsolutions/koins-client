class CreateBranchBanks < ActiveRecord::Migration[4.2]
  def change
    create_table :branch_banks, id: false do |t|
      t.references :branch
      t.references :bank
    end

    add_index :branch_banks, [:branch_id, :bank_id], unique: true, name: 'branch_bank_index'
  end
end
