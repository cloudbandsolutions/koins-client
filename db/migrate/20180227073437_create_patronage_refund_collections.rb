class CreatePatronageRefundCollections < ActiveRecord::Migration[5.2]
  def change
    create_table :patronage_refund_collections do |t|
      t.decimal :total_loan_interest_amount
      t.decimal :total_interest_amount

      t.timestamps null: false
    end
  end
end
