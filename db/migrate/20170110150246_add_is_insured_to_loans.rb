class AddIsInsuredToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :is_insured, :boolean
  end
end
