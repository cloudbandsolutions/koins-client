class CreateMemberEquityTransferRequests < ActiveRecord::Migration[4.2]
  def change
    create_table :member_equity_transfer_requests do |t|
      t.references :equity_account, index: { name: 'metr_ea_index' }, foreign_key: true
      t.references :member_transfer_request, index: { name: 'metr_mtr_index' }, foreign_key: true
      t.decimal :amount
      t.integer :dr_accounting_code_id
      t.integer :cr_accounting_code_id

      t.timestamps null: false
    end
  end
end
