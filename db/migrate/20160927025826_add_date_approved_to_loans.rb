class AddDateApprovedToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :date_approved, :date
  end
end
