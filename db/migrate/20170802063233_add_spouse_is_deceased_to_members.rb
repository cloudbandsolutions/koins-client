class AddSpouseIsDeceasedToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :spouse_is_deceased, :boolean
  end
end
