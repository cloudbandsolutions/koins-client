class AddDebitExpenseAccountingCodeIdAndTaxAccountingCodeIdAndInterestAccountingCodeIdToSavingsTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_types, :debit_expense_accounting_code_id, :integer
    add_column :savings_types, :tax_accounting_code_id, :integer
    add_column :savings_types, :interest_accounting_code_id, :integer
  end
end
