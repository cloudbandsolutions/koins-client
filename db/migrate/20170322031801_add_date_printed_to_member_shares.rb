class AddDatePrintedToMemberShares < ActiveRecord::Migration[4.2]
  def change
    add_column :member_shares, :date_printed, :date
  end
end
