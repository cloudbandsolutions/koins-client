class CreateDailyReports < ActiveRecord::Migration[4.2]
  def change
    create_table :daily_reports do |t|
      t.date :as_of
      t.json :loan_product_stats
      t.json :repayments
      t.json :par

      t.timestamps null: false
    end
  end
end
