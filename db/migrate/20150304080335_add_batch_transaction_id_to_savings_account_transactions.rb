class AddBatchTransactionIdToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :batch_transaction_id, :integer
  end
end
