class AddMaintainingBalanceExceptionAndMaintainingBalanceExceptionMinimumAmountToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :maintaining_balance_exception, :boolean
    add_column :loan_products, :maintaining_balance_exception_minimum_amount, :decimal
  end
end
