class AddBeginningBalanaceAndEndingBalanceAndTransactionDateToEquityAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :equity_account_transactions, :beginning_balance, :decimal
    add_column :equity_account_transactions, :ending_balance, :decimal
    add_column :equity_account_transactions, :transaction_date, :date
  end
end
