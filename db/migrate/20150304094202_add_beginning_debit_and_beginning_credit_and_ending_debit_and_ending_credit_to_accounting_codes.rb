class AddBeginningDebitAndBeginningCreditAndEndingDebitAndEndingCreditToAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :accounting_codes, :beginning_debit, :decimal
    add_column :accounting_codes, :beginning_credit, :decimal
    add_column :accounting_codes, :ending_debit, :decimal
    add_column :accounting_codes, :ending_credit, :decimal
  end
end
