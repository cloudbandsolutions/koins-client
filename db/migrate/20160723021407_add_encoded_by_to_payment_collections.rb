class AddEncodedByToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :encoded_by, :string
  end
end
