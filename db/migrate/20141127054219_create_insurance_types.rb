class CreateInsuranceTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :insurance_types do |t|
      t.string :name
      t.string :code
      t.integer :accounting_code_id

      t.timestamps
    end
  end
end
