class AddInsuranceAccountValidationIdToInsuranceAccountValidationCancellations < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_validation_cancellations, :insurance_account_validation_id, :integer
  end
end
