class AddAccountingCodeIdToLoanInsuranceType < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurance_types, :accounting_code_id, :integer
  end
end
