class AddWeeklyInterestRateAndMonthlyInterestRateToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :weekly_interest_rate, :decimal
    add_column :loan_products, :monthly_interest_rate, :decimal
  end
end
