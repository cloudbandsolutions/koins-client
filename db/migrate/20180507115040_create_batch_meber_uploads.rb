class CreateBatchMeberUploads < ActiveRecord::Migration[5.2]
  def change
    create_table :batch_meber_uploads do |t|
      t.string :status

      t.timestamps null: false
    end
  end
end
