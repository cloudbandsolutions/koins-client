class ModifyPreviousMliInMembers < ActiveRecord::Migration[4.2]
  def change
    remove_column :members, :previous_mli_member_since
    add_column :members, :previous_mii_member_since, :date
  end
end
