class AddDefaultAnnualInterestRateAndOverridableInterestRateToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :default_annual_interest_rate, :decimal
    add_column :loan_products, :overridable_interest_rate, :boolean
  end
end
