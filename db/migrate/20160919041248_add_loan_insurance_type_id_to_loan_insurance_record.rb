class AddLoanInsuranceTypeIdToLoanInsuranceRecord < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurance_records, :loan_insurance_type_id, :integer
  end
end
