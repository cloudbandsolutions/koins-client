class CreateLoanInsuranceAccounts < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_insurance_accounts do |t|
    	t.integer :insurance_type_id
    	t.decimal :balance
      t.integer :member_id
      t.string :account_number
      t.string :status
      t.integer :branch_id
      t.integer :center_id
      t.string :uuid

      t.timestamps null: false
    end
  end
end
