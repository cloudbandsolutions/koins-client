class CreateRegularDepositAmounts < ActiveRecord::Migration[4.2]
  def change
    create_table :regular_deposit_amounts do |t|
      t.decimal :min_amount
      t.decimal :max_amount
      t.decimal :deposit_amount

      t.timestamps null: false
    end
  end
end
