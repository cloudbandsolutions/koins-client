class AddEndDateToDepositTimeTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :deposit_time_transactions, :end_date, :date
  end
end
