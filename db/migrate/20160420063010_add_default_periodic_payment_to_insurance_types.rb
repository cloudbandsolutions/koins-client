class AddDefaultPeriodicPaymentToInsuranceTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_types, :default_periodic_payment, :decimal
  end
end
