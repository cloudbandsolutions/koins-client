class RemoveInterestOverrideFromLoans < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :interest_override
  end
end
