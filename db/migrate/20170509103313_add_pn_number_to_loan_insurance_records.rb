class AddPnNumberToLoanInsuranceRecords < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurance_records, :pn_number, :string
  end
end
