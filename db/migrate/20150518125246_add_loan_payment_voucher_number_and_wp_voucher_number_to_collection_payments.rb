class AddLoanPaymentVoucherNumberAndWpVoucherNumberToCollectionPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :collection_payments, :loan_payment_voucher_number, :string
    add_column :collection_payments, :wp_voucher_number, :string
  end
end
