class CreateCollectionPaymentReversedVouchers < ActiveRecord::Migration[4.2]
  def change
    create_table :collection_payment_reversed_vouchers do |t|
      t.integer :collection_payment_id
      t.integer :voucher_id
    end
  end
end
