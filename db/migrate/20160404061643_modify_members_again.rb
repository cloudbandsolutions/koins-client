class ModifyMembersAgain < ActiveRecord::Migration[4.2]
  def change
  	unless column_exists? :members, :previous_mfi_member_since
  		add_column :members, :previous_mfi_member_since, :date
  	end
  end
end
