class AddUuidToFeedbacks < ActiveRecord::Migration[4.2]
  def change
    add_column :feedbacks, :uuid, :string
  end
end
