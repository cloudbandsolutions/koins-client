class CreateTransferAdjustmentRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :transfer_adjustment_records do |t|
      t.references :accounting_code, index: true, foreign_key: true
      t.decimal :amount
      t.references :transfer_adjustment, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
