class RemoveBatchTransactionsAndCollectionPayments < ActiveRecord::Migration[4.2]
  def change
    drop_table :batch_transactions
    drop_table :collection_payments
    remove_column :savings_account_transactions, :batch_transaction_id
    remove_column :savings_account_transactions, :collection_payment_id
    remove_column :insurance_account_transactions, :batch_transaction_id
    remove_column :insurance_account_transactions, :collection_payment_id
    remove_column :equity_account_transactions, :batch_transaction_id
    remove_column :equity_account_transactions, :collection_payment_id
  end
end
