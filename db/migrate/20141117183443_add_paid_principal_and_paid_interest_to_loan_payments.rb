class AddPaidPrincipalAndPaidInterestToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :paid_principal, :decimal
    add_column :loan_payments, :paid_interest, :decimal
  end
end
