class CreateMajorGroups < ActiveRecord::Migration[4.2]
  def change
    create_table :major_groups do |t|
      t.string :name
      t.string :code
      t.string :dc_code

      t.timestamps null: false
    end
  end
end
