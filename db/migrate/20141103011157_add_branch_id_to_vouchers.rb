class AddBranchIdToVouchers < ActiveRecord::Migration[4.2]
  def change
    add_column :vouchers, :branch_id, :integer
  end
end
