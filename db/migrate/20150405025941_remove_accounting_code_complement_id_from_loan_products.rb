class RemoveAccountingCodeComplementIdFromLoanProducts < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_products, :accounting_code_transaction_complement_id
  end
end
