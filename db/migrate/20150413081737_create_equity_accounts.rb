class CreateEquityAccounts < ActiveRecord::Migration[4.2]
  def change
    create_table :equity_accounts do |t|
      t.integer :equity_type_id
      t.decimal :balance
      t.integer :member_id
      t.string :account_number
      t.string :status
      t.integer :branch_id
      t.integer :center_id

      t.timestamps null: false
    end
  end
end
