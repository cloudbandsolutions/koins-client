class CreateCenters < ActiveRecord::Migration[4.2]
  def change
    create_table :centers do |t|
      t.string :name
      t.string :abbreviation
      t.string :code
      t.text :address
      t.integer :branch_id

      t.timestamps
    end
  end
end
