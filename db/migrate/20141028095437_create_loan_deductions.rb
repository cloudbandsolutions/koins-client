class CreateLoanDeductions < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_deductions do |t|
      t.string :name
      t.string :code

      t.timestamps
    end
  end
end
