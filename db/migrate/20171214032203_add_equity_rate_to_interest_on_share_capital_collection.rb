class AddEquityRateToInterestOnShareCapitalCollection < ActiveRecord::Migration[4.2]
  def change
    add_column :interest_on_share_capital_collections, :equity_rate, :string
  end
end
