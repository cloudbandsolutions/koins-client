class RemoveOverrideBatchTransactionNumberFromLoans < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :override_batch_transaction_number
  end
end
