class AddUuidToCenters < ActiveRecord::Migration[4.2]
  def change
    add_column :centers, :uuid, :string
  end
end
