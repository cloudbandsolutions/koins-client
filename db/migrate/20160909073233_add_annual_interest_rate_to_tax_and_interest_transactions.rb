class AddAnnualInterestRateToTaxAndInterestTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :tax_and_interest_transactions, :annual_interest_rate, :decimal
  end
end
