class CreateSnapshots < ActiveRecord::Migration[4.2]
  def change
    create_table :snapshots do |t|
      t.string :area_code
      t.string :cluster_code
      t.string :center_code
      t.string :branch_code
      t.string :uuid
      t.datetime :snapped_at
      t.integer :num_active_loans
      t.integer :num_pending_loans
      t.decimal :total_loan_balance
      t.decimal :total_loan_payments
      t.decimal :total_loan_principal_paid
      t.decimal :total_loan_interest_paid
      t.decimal :total_loan_principal_balance
      t.decimal :total_loan_interest_balance
      t.integer :num_active_male_members
      t.integer :num_active_female_members
      t.integer :num_branches
      t.integer :num_centers

      t.timestamps null: false
    end
  end
end
