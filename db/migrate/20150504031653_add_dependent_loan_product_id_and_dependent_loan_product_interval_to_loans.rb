class AddDependentLoanProductIdAndDependentLoanProductIntervalToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :dependent_loan_product_id, :integer
    add_column :loans, :dependent_loan_product_interval, :integer
  end
end
