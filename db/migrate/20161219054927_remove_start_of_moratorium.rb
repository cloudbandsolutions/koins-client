class RemoveStartOfMoratorium < ActiveRecord::Migration[4.2]
  def change
    remove_column :batch_moratoria, :start_of_moratorium
  end
end
