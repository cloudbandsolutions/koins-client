class AddForResignationToEquityAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :equity_account_transactions, :for_resignation, :boolean
  end
end
