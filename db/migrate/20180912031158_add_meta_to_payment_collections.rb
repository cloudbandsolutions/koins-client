class AddMetaToPaymentCollections < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_collections, :meta, :json
  end
end
