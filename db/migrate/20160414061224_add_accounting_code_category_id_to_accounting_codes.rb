class AddAccountingCodeCategoryIdToAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :accounting_codes, :accounting_code_category_id, :integer
  end
end
