class AddUuidToLegalDependents < ActiveRecord::Migration[4.2]
  def change
    add_column :legal_dependents, :uuid, :string
  end
end
