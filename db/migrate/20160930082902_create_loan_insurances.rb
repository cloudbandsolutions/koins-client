class CreateLoanInsurances < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_insurances do |t|
      t.string :uuid
      t.string :reference_number
      t.date :date_prepared
      t.string :status
      t.integer :branch_id
      t.string :prepared_by
      t.decimal :total_amount
      t.text :particular

      t.timestamps null: false
    end
  end
end
