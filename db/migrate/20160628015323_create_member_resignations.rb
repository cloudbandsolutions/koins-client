class CreateMemberResignations < ActiveRecord::Migration[4.2]
  def change
    create_table :member_resignations do |t|
      t.string :uuid
      t.references :member, index: true, foreign_key: true
      t.date :date_resigned
      t.string :resignation_type
      t.string :status

      t.timestamps null: false
    end
  end
end
