class AddOldCertificateNumberToMembers < ActiveRecord::Migration[4.2]
  def change
  	add_column :members, :old_certificate_number, :string
  end
end
