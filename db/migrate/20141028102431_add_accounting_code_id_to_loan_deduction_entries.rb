class AddAccountingCodeIdToLoanDeductionEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_deduction_entries, :accounting_code_id, :integer
  end
end
