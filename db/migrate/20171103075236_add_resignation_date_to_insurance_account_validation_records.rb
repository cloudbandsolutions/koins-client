class AddResignationDateToInsuranceAccountValidationRecords < ActiveRecord::Migration[4.2]
  def change
  	add_column :insurance_account_validation_records, :resignation_date, :date
  end
end
