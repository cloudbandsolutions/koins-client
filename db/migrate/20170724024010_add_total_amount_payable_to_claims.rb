class AddTotalAmountPayableToClaims < ActiveRecord::Migration[4.2]
  def change
    add_column :claims, :total_amount_payable, :decimal
  end
end
