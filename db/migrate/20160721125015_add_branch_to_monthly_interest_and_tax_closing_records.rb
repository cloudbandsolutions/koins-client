class AddBranchToMonthlyInterestAndTaxClosingRecords < ActiveRecord::Migration[4.2]
  def change
    add_reference :monthly_interest_and_tax_closing_records, :branch, index: true, foreign_key: true
  end
end
