class AddCollectedByAndCheckedByToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :collected_by, :string
    add_column :payment_collections, :checked_by, :string
  end
end
