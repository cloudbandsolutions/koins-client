class AddUuidToAccountingCodes < ActiveRecord::Migration[5.2]
  def change
    add_column :accounting_codes, :uuid, :string
  end
end
