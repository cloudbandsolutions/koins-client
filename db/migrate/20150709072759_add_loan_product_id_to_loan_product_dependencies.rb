class AddLoanProductIdToLoanProductDependencies < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_product_dependencies, :loan_product_id, :integer
  end
end
