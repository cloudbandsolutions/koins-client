class RemoveOrNumberFromLoans < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :or_number
  end
end
