class AddIsEditableToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :is_editable, :boolean
  end
end
