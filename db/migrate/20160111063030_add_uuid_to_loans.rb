class AddUuidToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :uuid, :string
  end
end
