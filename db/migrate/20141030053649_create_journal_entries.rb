class CreateJournalEntries < ActiveRecord::Migration[4.2]
  def change
    create_table :journal_entries do |t|
      t.integer :accounting_code_id
      t.decimal :amount
      t.string :uuid
      t.string :post_type

      t.timestamps
    end
  end
end
