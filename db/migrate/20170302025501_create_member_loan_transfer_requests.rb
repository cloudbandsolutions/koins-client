class CreateMemberLoanTransferRequests < ActiveRecord::Migration[4.2]
  def change
    create_table :member_loan_transfer_requests do |t|
      t.references :member_transfer_request, index: { name: 'mtr_mltr_index' }, foreign_key: true
      t.decimal :amount
      t.integer :dr_accounting_code_id
      t.integer :cr_accounting_code_id
      t.references :loan, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
