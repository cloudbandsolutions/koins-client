class AddStatusToVouchers < ActiveRecord::Migration[4.2]
  def change
    add_column :vouchers, :status, :string
  end
end
