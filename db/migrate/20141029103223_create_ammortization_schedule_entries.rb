class CreateAmmortizationScheduleEntries < ActiveRecord::Migration[4.2]
  def change
    create_table :ammortization_schedule_entries do |t|
      t.integer :loan_id
      t.string :uuid
      t.decimal :amount
      t.decimal :principal
      t.decimal :interest
      t.date :due_at
      t.boolean :paid

      t.timestamps
    end
  end
end
