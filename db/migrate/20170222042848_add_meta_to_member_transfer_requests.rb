class AddMetaToMemberTransferRequests < ActiveRecord::Migration[4.2]
  def change
    add_column :member_transfer_requests, :meta, :json
  end
end
