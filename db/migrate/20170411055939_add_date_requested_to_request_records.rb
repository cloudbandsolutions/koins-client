class AddDateRequestedToRequestRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :request_records, :date_requested, :date
  end
end
