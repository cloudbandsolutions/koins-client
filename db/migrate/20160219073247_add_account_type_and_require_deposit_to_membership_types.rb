class AddAccountTypeAndRequireDepositToMembershipTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :membership_types, :account_type, :string
    add_column :membership_types, :require_deposit, :boolean
  end
end
