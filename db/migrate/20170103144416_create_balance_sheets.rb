class CreateBalanceSheets < ActiveRecord::Migration[4.2]
  def change
    create_table :balance_sheets do |t|
      t.json :data
      t.date :as_of
      t.string :branch_name

      t.timestamps null: false
    end
  end
end
