class AddTotalTimeDepositAmountToPaymentCollection < ActiveRecord::Migration[5.2]
  def change
    add_column :payment_collections, :total_time_deposit_amount, :decimal
  end
end
