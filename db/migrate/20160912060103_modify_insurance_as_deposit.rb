class ModifyInsuranceAsDeposit < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :insurance_as_deposit
    add_column :loans, :insurance_as_disbursement_correction, :boolean
  end
end
