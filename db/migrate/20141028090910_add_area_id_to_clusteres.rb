class AddAreaIdToClusteres < ActiveRecord::Migration[4.2]
  def change
    add_column :clusters, :area_id, :integer
  end
end
