class AddRemainingBalanceToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :remaining_balance, :decimal
  end
end
