class AddIsOneTimeDeductionToLoanInsuranceDeductionEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_insurance_deduction_entries, :is_one_time_deduction, :boolean
  end
end
