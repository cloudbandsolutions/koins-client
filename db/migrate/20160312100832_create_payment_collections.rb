class CreatePaymentCollections < ActiveRecord::Migration[4.2]
  def change
    create_table :payment_collections do |t|
      t.string :reference_number
      t.text :particular
      t.string :uuid
      t.string :or_number
      t.decimal :total_amount
      t.decimal :total_wp_amount
      t.decimal :total_cp_amount
      t.decimal :total_principal_amount
      t.decimal :total_interest_amount
      t.references :branch, index: true, foreign_key: true
      t.references :center, index: true, foreign_key: true
      t.string :prepared_by
      t.string :approved_by
      t.string :status

      t.timestamps null: false
    end
  end
end
