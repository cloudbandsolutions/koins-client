class AddNormalTransactionAndMajorAccountAndMajorGroupToMotherAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :mother_accounting_codes, :normal_transaction, :string
    add_column :mother_accounting_codes, :major_account, :string
    add_column :mother_accounting_codes, :major_group, :string
  end
end
