class AddForLockInToSavingsAccountTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :savings_account_transactions, :for_lock_in, :string
  end
end
