class AddCheckNumberAndBankCheckNumberToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :check_number, :string
    add_column :loans, :bank_check_number, :string
  end
end
