class AddIsSpecialReportToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :is_special_report, :boolean
  end
end
