class AddCancelledByAndDateCancelledToInsuranceAccountValidations < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_validations, :date_cancelled, :date
    add_column :insurance_account_validations, :cancelled_by, :string
  end
end
