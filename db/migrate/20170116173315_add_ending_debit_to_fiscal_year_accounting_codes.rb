class AddEndingDebitToFiscalYearAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :fiscal_year_accounting_codes, :ending_debit, :decimal
  end
end
