class AddUseResignRulesAndResignRuleNumYearsAndResignRuleWithdrawalPercentageToInsuranceTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_types, :use_resign_rules, :boolean
    add_column :insurance_types, :resign_rule_num_years, :integer
    add_column :insurance_types, :resign_rule_withdrawal_percentage, :decimal
  end
end
