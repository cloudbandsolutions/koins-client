class RemovePaymentMembershipCollection < ActiveRecord::Migration[4.2]
  def change
    remove_column :payment_collection_records, :payment_membership_collection_id, :integer
    drop_table :payment_membership_collections
  end
end
