class AddIsAdjustmentToInsuranceAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_account_transactions, :is_adjustment, :boolean
  end
end
