class CreatePatronageRefundCollectionRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :patronage_refund_collection_records do |t|
      t.references :patronage_refund_collection, index: {:name => "patronage_refund_collection"}, foreign_key: true
      t.json :data

      t.timestamps null: false
    end
  end
end
