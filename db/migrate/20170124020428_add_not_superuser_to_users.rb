class AddNotSuperuserToUsers < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :not_superuser, :boolean
  end
end
