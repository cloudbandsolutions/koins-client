class AddLoanInsuranceTypeIdToLoanInsurance < ActiveRecord::Migration[4.2]
  def change
  	remove_column :loan_insurance_records, :loan_insurance_type_id
  	add_column :loan_insurances, :loan_insurance_type_id, :integer
  end
end
