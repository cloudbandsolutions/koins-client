class AddForResignationToInsuranceAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_account_transactions, :for_resignation, :boolean
  end
end
