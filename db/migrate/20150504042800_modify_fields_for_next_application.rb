class ModifyFieldsForNextApplication < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_products, :next_application_loan_cycle_count
    add_column :loan_products, :next_application_interval, :integer
  end
end
