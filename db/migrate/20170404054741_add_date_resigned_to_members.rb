class AddDateResignedToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :date_resigned, :date
  end
end
