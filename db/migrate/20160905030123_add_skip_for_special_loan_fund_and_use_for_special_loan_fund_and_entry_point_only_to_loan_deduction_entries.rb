class AddSkipForSpecialLoanFundAndUseForSpecialLoanFundAndEntryPointOnlyToLoanDeductionEntries < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_deduction_entries, :skip_for_special_loan_fund, :boolean
    add_column :loan_deduction_entries, :use_for_special_loan_fund, :boolean
    add_column :loan_deduction_entries, :entry_point_only, :boolean
  end
end
