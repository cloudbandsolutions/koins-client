class AddUuidToAreas < ActiveRecord::Migration[5.2]
  def change
    add_column :areas, :uuid, :string
  end
end
