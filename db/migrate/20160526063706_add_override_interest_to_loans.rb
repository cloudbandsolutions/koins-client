class AddOverrideInterestToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :override_interest, :boolean
  end
end
