class CreateReinstatements < ActiveRecord::Migration[4.2]
  def change
    create_table :reinstatements do |t|
      t.date :date_prepared
      t.string :status
      t.integer :branch_id
      t.string :prepared_by
      t.string :approved_by

      t.timestamps null: false
    end
  end
end
