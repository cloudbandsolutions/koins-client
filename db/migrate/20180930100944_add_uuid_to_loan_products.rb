class AddUuidToLoanProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :loan_products, :uuid, :string
  end
end
