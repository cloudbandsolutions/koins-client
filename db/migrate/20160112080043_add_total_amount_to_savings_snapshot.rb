class AddTotalAmountToSavingsSnapshot < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_snapshots, :total_amount, :decimal
  end
end
