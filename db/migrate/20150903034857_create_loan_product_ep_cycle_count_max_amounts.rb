class CreateLoanProductEpCycleCountMaxAmounts < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_product_ep_cycle_count_max_amounts do |t|
      t.integer :loan_product_id
      t.integer :cycle_count
      t.decimal :max_amount

      t.timestamps null: false
    end
  end
end
