class AddDepositVoucherReferenceNumberToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :deposit_voucher_reference_number, :string
  end
end
