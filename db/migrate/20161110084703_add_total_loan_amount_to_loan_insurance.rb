class AddTotalLoanAmountToLoanInsurance < ActiveRecord::Migration[4.2]
  def change
  	add_column :loan_insurances, :total_loan_amount, :decimal
  end
end
