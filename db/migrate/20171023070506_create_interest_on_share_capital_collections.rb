class CreateInterestOnShareCapitalCollections < ActiveRecord::Migration[4.2]
  def change
    create_table :interest_on_share_capital_collections do |t|
      t.decimal :total_equity_amount
      t.decimal :total_interest_amount
      t.date :collection_date
      t.string :refference_number
      t.string :status

      t.timestamps null: false
    end
  end
end
