class AddSavingsAccountsToTimeDepositWithdrawal < ActiveRecord::Migration[5.2]
  def change
    add_reference :time_deposit_withdrawals, :savings_accounts, foreign_key: true
  end
end
