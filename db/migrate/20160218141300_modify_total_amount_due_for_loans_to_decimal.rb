class ModifyTotalAmountDueForLoansToDecimal < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :total_amount_due
    add_column :loans, :total_amount_due, :decimal
  end
end
