class CreateFeedbacks < ActiveRecord::Migration[4.2]
  def change
    create_table :feedbacks do |t|
      t.integer :member_id
      t.string :content

      t.timestamps null: false
    end
  end
end
