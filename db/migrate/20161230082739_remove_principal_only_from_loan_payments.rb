class RemovePrincipalOnlyFromLoanPayments < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_payments, :principal_only
  end
end
