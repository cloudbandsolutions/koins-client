class AddAdditionalMembershipFields < ActiveRecord::Migration[4.2]
  def change
  	add_column :members, :previous_mfi_member_since, :date
  	add_column :members, :previous_mli_member_since, :date
  	add_column :members, :pag_ibig_number,    :string
  	add_column :members, :tin_numbe,          :string
  	add_column :members, :phil_health_number, :string
  end
end
