class AddTransactionNumberToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :transaction_number, :string
  end
end
