class AddEducationalAttainmentAndCourseToLegalDependents < ActiveRecord::Migration[4.2]
  def change
    add_column :legal_dependents, :educational_attainment, :string
    add_column :legal_dependents, :course, :string
  end
end
