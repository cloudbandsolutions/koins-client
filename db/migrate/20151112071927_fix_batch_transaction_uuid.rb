class FixBatchTransactionUuid < ActiveRecord::Migration[4.2]
  def change
    remove_column(:batch_transactions, :uuid)
    add_column(:batch_transactions, :uuid, :string)
  end
end
