class AddLoanCycleCountToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :loan_cycle_count, :integer
  end
end
