class AddMemberStatsToDailyReports < ActiveRecord::Migration[4.2]
  def change
    add_column :daily_reports, :member_stats, :json
  end
end
