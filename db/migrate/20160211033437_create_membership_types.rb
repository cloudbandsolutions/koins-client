class CreateMembershipTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :membership_types do |t|
      t.string :name
      t.decimal :fee

      t.timestamps null: false
    end
  end
end
