class ModifyLoanPaymentField < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_payments, :loan_payment
    add_column :loan_payments, :loan_payment_amount, :decimal
  end
end
