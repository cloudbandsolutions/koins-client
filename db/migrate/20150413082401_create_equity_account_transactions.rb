class CreateEquityAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    create_table :equity_account_transactions do |t|
      t.integer :equity_account_id
      t.decimal :amount
      t.string :transaction_type
      t.datetime :transacted_at
      t.text :particular
      t.string :status
      t.string :transacted_by
      t.string :approved_by
      t.string :voucher_reference_number
      t.string :transaction_number
      t.integer :batch_transaction_id
      t.integer :bank_id
      t.integer :accounting_code_id

      t.timestamps null: false
    end
  end
end
