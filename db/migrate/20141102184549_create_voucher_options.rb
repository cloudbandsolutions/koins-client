class CreateVoucherOptions < ActiveRecord::Migration[4.2]
  def change
    create_table :voucher_options do |t|
      t.string :name
      t.string :val
      t.integer :voucher_id

      t.timestamps
    end
  end
end
