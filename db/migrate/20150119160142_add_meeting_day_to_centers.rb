class AddMeetingDayToCenters < ActiveRecord::Migration[4.2]
  def change
    add_column :centers, :meeting_day, :string
  end
end
