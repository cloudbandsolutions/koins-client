class AddExperienceWithMicrofinanceToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :experience_with_microfinance, :string
  end
end
