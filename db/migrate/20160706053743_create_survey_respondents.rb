class CreateSurveyRespondents < ActiveRecord::Migration[4.2]
  def change
    create_table :survey_respondents do |t|
      t.references :member, index: true, foreign_key: true
      t.date :date_answered
      t.references :survey, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
