class AddMonthlyInterestRateAndMonthlyTaxRateToSavingsTypes < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_types, :monthly_interest_rate, :decimal
    add_column :savings_types, :monthly_tax_rate, :decimal
  end
end
