class AddStatusToMemberShares < ActiveRecord::Migration[4.2]
  def change
    add_column :member_shares, :status, :string
  end
end
