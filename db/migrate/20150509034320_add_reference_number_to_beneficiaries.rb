class AddReferenceNumberToBeneficiaries < ActiveRecord::Migration[4.2]
  def change
    add_column :beneficiaries, :reference_number, :string
  end
end
