class RemoveLoanPaymentFields < ActiveRecord::Migration[4.2]
  def change
    remove_column :loan_payments, :deposit_particular
    remove_column :loan_payments, :deposit_voucher_reference_number
    remove_column :loan_payments, :deposit_savings_account_id
    remove_column :loan_payments, :insurance_particular
    remove_column :loan_payments, :insurance_deposit_voucher_reference_number

    add_column :loan_payments, :insurance_deposit_amount, :decimal
  end
end
