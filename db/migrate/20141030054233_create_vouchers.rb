class CreateVouchers < ActiveRecord::Migration[4.2]
  def change
    create_table :vouchers do |t|
      t.string :book
      t.string :uuid
      t.string :reference_number
      t.date :date_prepared
      t.text :particular
      t.string :approved_by
      t.string :prepared_by

      t.timestamps
    end
  end
end
