class AddCollectionPaymentIdToInsuranceAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :insurance_account_transactions, :collection_payment_id, :integer
  end
end
