class AddReferenceNumberToLegalDependents < ActiveRecord::Migration[4.2]
  def change
    add_column :legal_dependents, :reference_number, :string
  end
end
