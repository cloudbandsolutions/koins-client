class CreateSavingsSnapshots < ActiveRecord::Migration[4.2]
  def change
    create_table :savings_snapshots do |t|
      t.string :uuid
      t.string :cluster_code
      t.datetime :snapped_at
      t.integer :num_active_accounts
      t.integer :num_inactive_accounts
      t.decimal :total_withdraw
      t.decimal :total_wp
      t.decimal :total_deposit
      t.decimal :total_reversed
      t.decimal :total_interest
      t.decimal :total_tax
      t.decimal :total_fund_transfer_withdraw
      t.decimal :total_fund_transfer_deposit
      t.string :savings_type_code

      t.timestamps null: false
    end
  end
end
