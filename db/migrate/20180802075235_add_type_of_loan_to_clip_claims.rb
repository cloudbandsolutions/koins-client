class AddTypeOfLoanToClipClaims < ActiveRecord::Migration[5.2]
  def change
  	add_column :clip_claims, :type_of_loan, :string
  end
end
