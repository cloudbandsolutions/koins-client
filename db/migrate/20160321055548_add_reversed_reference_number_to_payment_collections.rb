class AddReversedReferenceNumberToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :reversed_reference_number, :string
  end
end
