class CreateLoanProducts < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_products do |t|
      t.string :name
      t.string :code
      t.integer :bank_id

      t.timestamps
    end
  end
end
