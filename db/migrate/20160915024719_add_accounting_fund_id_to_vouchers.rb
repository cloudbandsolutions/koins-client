class AddAccountingFundIdToVouchers < ActiveRecord::Migration[4.2]
  def change
  	add_column :vouchers, :accounting_fund_id, :integer
  end
end
