class CreateInsuranceRemittanceRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :insurance_remittance_records do |t|
      t.references :member, index: true, foreign_key: true
      t.references :insurance_type, index: true, foreign_key: true
      t.references :insurance_remittance, index: true, foreign_key: true
      t.decimal :amount
      t.string :uuid

      t.timestamps null: false
    end
  end
end
