class AddPaymentMembershipCollectionIdToPaymentCollectionRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collection_records, :payment_membership_collection_id, :integer
  end
end
