class AddPreparedByToBackgroundOperations < ActiveRecord::Migration[5.2]
  def change
  	add_column :background_operations, :prepared_by, :string
  end
end
