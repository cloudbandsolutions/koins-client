class AddMetaToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :meta, :json
  end
end
