class AddCheckNameToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :check_name, :string
  end
end
