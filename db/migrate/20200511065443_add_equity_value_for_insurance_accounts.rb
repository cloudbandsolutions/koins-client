class AddEquityValueForInsuranceAccounts < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_accounts, :equity_value, :decimal
  end
end
