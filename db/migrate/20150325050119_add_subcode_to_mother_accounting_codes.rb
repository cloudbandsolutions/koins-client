class AddSubcodeToMotherAccountingCodes < ActiveRecord::Migration[4.2]
  def change
    add_column :mother_accounting_codes, :subcode, :string
  end
end
