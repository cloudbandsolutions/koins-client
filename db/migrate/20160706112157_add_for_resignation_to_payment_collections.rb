class AddForResignationToPaymentCollections < ActiveRecord::Migration[4.2]
  def change
    add_column :payment_collections, :for_resignation, :boolean
  end
end
