class ChangeSubsidiaryAdjustmentTableName < ActiveRecord::Migration[4.2]
  def change
    rename_table :subsidiary_adjustment_records, :subsidiary_adjustments
  end
end
