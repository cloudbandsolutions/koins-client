class AddAlwaysHaveProcessingFeeToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :always_have_processing_fee, :boolean
  end
end
