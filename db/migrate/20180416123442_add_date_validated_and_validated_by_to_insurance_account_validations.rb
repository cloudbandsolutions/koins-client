class AddDateValidatedAndValidatedByToInsuranceAccountValidations < ActiveRecord::Migration[5.2]
  def change
    add_column :insurance_account_validations, :date_validated, :date
    add_column :insurance_account_validations, :validated_by, :string
  end
end
