class RemoveCheckNumberFromLoans < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :check_number
  end
end
