class AddIsDeceasedToLegalDependents < ActiveRecord::Migration[4.2]
  def change
    add_column :legal_dependents, :is_deceased, :boolean
  end
end
