class AddExtraGeneralSnapshotParameters < ActiveRecord::Migration[4.2]
  def change
    add_column :snapshots, :num_loans_released, :integer
    add_column :snapshots, :num_loans_overdue, :integer
    add_column :snapshots, :num_loans_par, :integer
    add_column :snapshots, :num_loans_good_standing, :integer
    add_column :snapshots, :total_loan_portfolio, :decimal
    add_column :snapshots, :total_loan_payables, :decimal
  end
end
