class AddBankIdAndBankAccountTypeToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :bank_id, :integer
    add_column :members, :bank_account_type, :string
  end
end
