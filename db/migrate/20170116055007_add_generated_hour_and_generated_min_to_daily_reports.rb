class AddGeneratedHourAndGeneratedMinToDailyReports < ActiveRecord::Migration[4.2]
  def change
    add_column :daily_reports, :generated_hour, :integer
    add_column :daily_reports, :generated_min, :integer
  end
end
