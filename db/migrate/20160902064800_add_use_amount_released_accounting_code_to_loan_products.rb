class AddUseAmountReleasedAccountingCodeToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :use_amount_released_accounting_code, :boolean
  end
end
