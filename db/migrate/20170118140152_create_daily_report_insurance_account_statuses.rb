class CreateDailyReportInsuranceAccountStatuses < ActiveRecord::Migration[4.2]
  def change
    create_table :daily_report_insurance_account_statuses do |t|
      t.date :as_of
      t.string :uuid
      t.integer :generated_hour
      t.integer :generated_min
      t.json :data

      t.timestamps null: false
    end
  end
end
