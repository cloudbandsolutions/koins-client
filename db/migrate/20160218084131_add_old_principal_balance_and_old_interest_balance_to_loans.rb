class AddOldPrincipalBalanceAndOldInterestBalanceToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :old_principal_balance, :decimal
    add_column :loans, :old_interest_balance, :decimal
  end
end
