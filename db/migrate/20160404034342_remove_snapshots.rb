class RemoveSnapshots < ActiveRecord::Migration[4.2]
  def change
    drop_table :snapshots
    drop_table :savings_snapshots
  end
end
