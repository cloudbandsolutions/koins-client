class AddLoanPaymentParticularAndWpParticularAndDepositParticularAndInsuranceDepositParticularToCollectionPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :collection_payments, :loan_payment_particular, :text
    add_column :collection_payments, :wp_particular, :text
    add_column :collection_payments, :deposit_particular, :text
    add_column :collection_payments, :insurance_deposit_particular, :text
  end
end
