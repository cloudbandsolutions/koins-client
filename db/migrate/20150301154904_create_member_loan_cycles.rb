class CreateMemberLoanCycles < ActiveRecord::Migration[4.2]
  def change
    create_table :member_loan_cycles do |t|
      t.integer :member_id
      t.integer :loan_product_id
      t.integer :cycle

      t.timestamps
    end
  end
end
