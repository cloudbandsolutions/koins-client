class AddWpAccountingCodeIdToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :wp_accounting_code_id, :integer
  end
end
