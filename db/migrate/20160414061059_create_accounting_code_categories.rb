class CreateAccountingCodeCategories < ActiveRecord::Migration[4.2]
  def change
    create_table :accounting_code_categories do |t|
      t.string :name
      t.string :sub_code
      t.references :mother_accounting_code, index: true, foreign_key: true
      t.string :code

      t.timestamps null: false
    end
  end
end
