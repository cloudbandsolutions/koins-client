class AddDateCheckedAndCheckedByToInsuranceAccountValidations < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_validations, :date_checked, :date
    add_column :insurance_account_validations, :checked_by, :string
  end
end
