class AddBeginningBalanceAndEndingBalanceToTaxAndInterestTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :tax_and_interest_transactions, :beginning_balance, :decimal
    add_column :tax_and_interest_transactions, :ending_balance, :decimal
  end
end
