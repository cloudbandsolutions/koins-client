class AddInstallmentOverrideAndInterestOverrideToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :installment_override, :integer
    add_column :loans, :interest_override, :decimal
  end
end
