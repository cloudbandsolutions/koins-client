class AddDateApprovedAndApprovedByToRequestRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :request_records, :date_approved, :date
    add_column :request_records, :approved_by, :string
  end
end
