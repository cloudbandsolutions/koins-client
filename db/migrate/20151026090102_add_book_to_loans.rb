class AddBookToLoans < ActiveRecord::Migration[4.2]
  def change
    add_column :loans, :book, :string
  end
end
