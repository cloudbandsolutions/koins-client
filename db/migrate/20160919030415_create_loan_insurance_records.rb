class CreateLoanInsuranceRecords < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_insurance_records do |t|
    	t.date :date_released
    	t.date :maturity_date
    	t.integer :loan_term
      t.decimal :loan_amount
      t.decimal :loan_insurance_amount
      t.integer :member_id

      t.timestamps null: false
    end
  end
end
