class AddIsVoidToInsuranceAccountValidationRecords < ActiveRecord::Migration[5.2]
  def change
  	add_column :insurance_account_validation_records, :is_void, :boolean
  end
end
