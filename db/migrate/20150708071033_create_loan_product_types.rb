class CreateLoanProductTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_product_types do |t|
      t.string :name
      t.integer :loan_product_id

      t.timestamps null: false
    end
  end
end
