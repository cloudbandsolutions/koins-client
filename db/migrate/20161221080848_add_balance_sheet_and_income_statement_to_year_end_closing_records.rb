class AddBalanceSheetAndIncomeStatementToYearEndClosingRecords < ActiveRecord::Migration[4.2]
  def change
    add_column :year_end_closing_records, :balance_sheet, :json
    add_column :year_end_closing_records, :income_statement, :json
  end
end
