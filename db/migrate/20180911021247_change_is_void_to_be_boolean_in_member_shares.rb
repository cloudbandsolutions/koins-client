class ChangeIsVoidToBeBooleanInMemberShares < ActiveRecord::Migration[5.2]
  def change
    change_column :member_shares, :is_void, 'boolean USING CAST(is_void AS boolean)'
  end
end
