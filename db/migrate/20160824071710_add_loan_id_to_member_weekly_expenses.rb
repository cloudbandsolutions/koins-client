class AddLoanIdToMemberWeeklyExpenses < ActiveRecord::Migration[4.2]
  def change
    add_column :member_weekly_expenses, :loan_id, :integer
  end
end
