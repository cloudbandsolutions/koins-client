class RemoveMajorGroupsFromMajorAccounts < ActiveRecord::Migration[4.2]
  def change
    remove_column :major_accounts, :major_group
  end
end
