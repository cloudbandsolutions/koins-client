class AddMemberToMemberWithdrawals < ActiveRecord::Migration[4.2]
  def change
    add_reference :member_withdrawals, :member, index: true, foreign_key: true
  end
end
