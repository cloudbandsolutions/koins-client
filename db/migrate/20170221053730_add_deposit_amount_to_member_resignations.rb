class AddDepositAmountToMemberResignations < ActiveRecord::Migration[4.2]
  def change
    add_column :member_resignations, :deposit_amount, :decimal
  end
end
