class AddWaiveProcessingFeeForExistingEntryPointLoansToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :waive_processing_fee_for_existing_entry_point_loans, :boolean
  end
end
