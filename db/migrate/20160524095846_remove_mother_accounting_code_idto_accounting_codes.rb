class RemoveMotherAccountingCodeIdtoAccountingCodes < ActiveRecord::Migration[4.2]
  def change
  	remove_column :accounting_codes, :mother_accounting_code_id
  end
end
