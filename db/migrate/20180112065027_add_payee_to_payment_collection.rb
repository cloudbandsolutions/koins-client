class AddPayeeToPaymentCollection < ActiveRecord::Migration[5.2]
  def change
  	add_column :payment_collections, :payee, :text
  end
end
