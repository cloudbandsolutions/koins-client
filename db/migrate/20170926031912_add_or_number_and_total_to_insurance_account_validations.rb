class AddOrNumberAndTotalToInsuranceAccountValidations < ActiveRecord::Migration[4.2]
  def change
  	add_column :insurance_account_validations, :total, :decimal
	add_column :insurance_account_validations, :or_number, :string
  end
end
