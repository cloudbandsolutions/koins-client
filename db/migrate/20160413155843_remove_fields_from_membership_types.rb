class RemoveFieldsFromMembershipTypes < ActiveRecord::Migration[4.2]
  def change
    remove_column :membership_types, :membership_category
  end
end
