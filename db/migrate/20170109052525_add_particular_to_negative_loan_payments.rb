class AddParticularToNegativeLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :negative_loan_payments, :particular, :string
  end
end
