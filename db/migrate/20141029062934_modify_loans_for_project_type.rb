class ModifyLoansForProjectType < ActiveRecord::Migration[4.2]
  def change
    remove_column :loans, :field_office
    remove_column :loans, :project_category_id
    add_column :loans, :project_type_category_id, :integer
  end
end
