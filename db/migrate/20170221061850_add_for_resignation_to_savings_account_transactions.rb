class AddForResignationToSavingsAccountTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_account_transactions, :for_resignation, :boolean
  end
end
