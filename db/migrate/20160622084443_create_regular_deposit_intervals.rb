class CreateRegularDepositIntervals < ActiveRecord::Migration[4.2]
  def change
    create_table :regular_deposit_intervals do |t|
      t.string :name
      t.integer :interval

      t.timestamps null: false
    end
  end
end
