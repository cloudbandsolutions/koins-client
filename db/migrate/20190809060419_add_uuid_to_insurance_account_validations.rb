class AddUuidToInsuranceAccountValidations < ActiveRecord::Migration[5.2]
  def change
    add_column :insurance_account_validations, :uuid, :string
  end
end
