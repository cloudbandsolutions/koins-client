class CreateLoanPaymentInsuranceAccountDeposits < ActiveRecord::Migration[4.2]
  def change
    create_table :loan_payment_insurance_account_deposits do |t|
      t.integer :loan_payment_id
      t.decimal :amount
      t.integer :insurance_account_id

      t.timestamps
    end
  end
end
