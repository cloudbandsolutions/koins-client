class RemoveCenterAndBranchFromSnapshots < ActiveRecord::Migration[4.2]
  def change
    remove_column :snapshots, :center_code
    remove_column :snapshots, :branch_code
  end
end
