class AddDefaultDrAccountIdAndDefaultCrAccountIdToSavingsTypes < ActiveRecord::Migration[4.2]
  def change
    remove_column :savings_types, :withdraw_accounting_code_id 
    remove_column :savings_types, :deposit_accounting_code_id 
    remove_column :savings_types, :withdraw_complimenetary_accounting_code_id 
    remove_column :savings_types, :deposit_complimentary_accounting_code_id 

    add_column :savings_types, :default_dr_account_id, :integer
    add_column :savings_types, :default_cr_account_id, :integer
  end
end
