class AddCollectionPaymentIdToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :collection_payment_id, :integer
  end
end
