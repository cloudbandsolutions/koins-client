class AddAnnualInterestRateToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :annual_interest_rate, :decimal
  end
end
