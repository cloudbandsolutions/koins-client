class AddTotalWithdrawalAmountToTimeDepositWithdrawal < ActiveRecord::Migration[5.2]
  def change
    add_column :time_deposit_withdrawals, :total_withdrawal_amount, :string
  end
end
