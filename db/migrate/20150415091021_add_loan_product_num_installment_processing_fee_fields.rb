class AddLoanProductNumInstallmentProcessingFeeFields < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :processing_fee_5, :decimal
    add_column :loan_products, :processing_fee_10, :decimal
    add_column :loan_products, :processing_fee_15, :decimal
    add_column :loan_products, :processing_fee_25, :decimal
    add_column :loan_products, :processing_fee_30, :decimal
    add_column :loan_products, :processing_fee_50, :decimal
  end
end
