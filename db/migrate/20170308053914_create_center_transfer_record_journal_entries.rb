class CreateCenterTransferRecordJournalEntries < ActiveRecord::Migration[4.2]
  def change
    create_table :center_transfer_record_journal_entries do |t|
      t.references :center_transfer_record, index: { name: 'ctrj_ctr_index' }, foreign_key: true
      t.references :accounting_code, index: { name: 'ctrj_ac_index' }, foreign_key: true
      t.string :post_type
      t.decimal :amount

      t.timestamps null: false
    end
  end
end
