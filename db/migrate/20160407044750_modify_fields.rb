class ModifyFields < ActiveRecord::Migration[4.2]
  def change
  	remove_column :build_versions, :category_id
  	add_column :build_categories, :build_version_id, :integer
  	remove_column :build_categories, :build_content_id
  	add_column :build_contents, :build_category_id, :integer
  end
end
