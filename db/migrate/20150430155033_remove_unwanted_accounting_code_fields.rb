class RemoveUnwantedAccountingCodeFields < ActiveRecord::Migration[4.2]
  def change
    remove_column :accounting_codes, :major_group
    remove_column :accounting_codes, :major_account
    remove_column :accounting_codes, :abbreviation
  end
end
