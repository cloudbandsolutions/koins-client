class AddBeginningBalanceAndEndingBalanceToSubsidiaryAdjustmentRecord < ActiveRecord::Migration[4.2]
  def change
    add_column :subsidiary_adjustment_records, :beginning_balance, :decimal
    add_column :subsidiary_adjustment_records, :ending_balance, :decimal
  end
end
