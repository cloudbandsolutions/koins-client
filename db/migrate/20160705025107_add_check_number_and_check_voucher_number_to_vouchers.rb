class AddCheckNumberAndCheckVoucherNumberToVouchers < ActiveRecord::Migration[4.2]
  def change
    add_column :vouchers, :check_number, :string
    add_column :vouchers, :check_voucher_number, :string
  end
end
