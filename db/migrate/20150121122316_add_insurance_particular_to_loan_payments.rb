class AddInsuranceParticularToLoanPayments < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_payments, :insurance_particular, :text
  end
end
