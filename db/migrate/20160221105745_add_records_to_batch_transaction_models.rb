class AddRecordsToBatchTransactionModels < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transaction_models, :records, :text
  end
end
