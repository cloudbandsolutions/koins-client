class AddReversedVoucherReferenceNumberToBatchTransactions < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_transactions, :reversed_voucher_reference_number, :string
  end
end
