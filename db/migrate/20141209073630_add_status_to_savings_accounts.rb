class AddStatusToSavingsAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_accounts, :status, :string
  end
end
