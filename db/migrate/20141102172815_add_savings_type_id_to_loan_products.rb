class AddSavingsTypeIdToLoanProducts < ActiveRecord::Migration[4.2]
  def change
    add_column :loan_products, :savings_type_id, :integer
  end
end
