class AddLengthOfHousingOccupancyToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :length_of_housing_occupancy, :integer
  end
end
