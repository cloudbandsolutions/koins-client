class AddActiveLoansToDailyReports < ActiveRecord::Migration[4.2]
  def change
    add_column :daily_reports, :active_loans_by_loan_product_to_daily_reports, :string
    add_column :daily_reports, :active_loans_by_loan_product, :json
  end
end
