class CreateEquityTypes < ActiveRecord::Migration[4.2]
  def change
    create_table :equity_types do |t|
      t.string :name
      t.string :code
      t.boolean :is_default
      t.integer :withdraw_accounting_code_id
      t.integer :deposit_accounting_code_id

      t.timestamps null: false
    end
  end
end
