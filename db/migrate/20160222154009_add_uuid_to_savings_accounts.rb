class AddUuidToSavingsAccounts < ActiveRecord::Migration[4.2]
  def change
    add_column :savings_accounts, :uuid, :string
  end
end
