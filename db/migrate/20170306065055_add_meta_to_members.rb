class AddMetaToMembers < ActiveRecord::Migration[4.2]
  def change
    add_column :members, :meta, :json
  end
end
