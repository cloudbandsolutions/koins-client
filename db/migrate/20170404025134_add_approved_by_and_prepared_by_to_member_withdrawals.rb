class AddApprovedByAndPreparedByToMemberWithdrawals < ActiveRecord::Migration[4.2]
  def change
    add_column :member_withdrawals, :approved_by, :string
    add_column :member_withdrawals, :prepared_by, :string
  end
end
