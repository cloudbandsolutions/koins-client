# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_26_043435) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounting_code_categories", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "sub_code"
    t.integer "mother_accounting_code_id"
    t.text "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mother_accounting_code_id"], name: "index_accounting_code_categories_on_mother_accounting_code_id"
  end

  create_table "accounting_codes", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal "beginning_balance"
    t.decimal "ending_balance"
    t.text "normal_transaction"
    t.text "main_code"
    t.text "sub_code"
    t.decimal "beginning_debit"
    t.decimal "beginning_credit"
    t.decimal "ending_debit"
    t.decimal "ending_credit"
    t.integer "accounting_code_category_id"
    t.string "uuid"
  end

  create_table "accounting_funds", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uuid"
  end

  create_table "active_admin_comments", id: :serial, force: :cascade do |t|
    t.text "namespace"
    t.text "body"
    t.text "resource_id", null: false
    t.text "resource_type", null: false
    t.integer "author_id"
    t.text "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "admin_users", id: :serial, force: :cascade do |t|
    t.text "email", default: "", null: false
    t.text "encrypted_password", default: "", null: false
    t.text "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.text "current_sign_in_ip"
    t.text "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "username"
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_admin_users_on_username", unique: true
  end

  create_table "aggregated_reports", id: :serial, force: :cascade do |t|
    t.string "uuid"
    t.json "data"
    t.date "report_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ammortization_schedule_entries", id: :serial, force: :cascade do |t|
    t.integer "loan_id"
    t.text "uuid"
    t.decimal "amount"
    t.decimal "principal"
    t.decimal "interest"
    t.date "due_at"
    t.boolean "paid"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal "paid_principal"
    t.decimal "paid_interest"
    t.boolean "is_void"
  end

  create_table "announcements", id: :serial, force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.date "announced_on"
    t.string "announced_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uuid"
    t.string "banner_file_name"
    t.string "banner_content_type"
    t.integer "banner_file_size"
    t.datetime "banner_updated_at"
  end

  create_table "app_keys", id: :serial, force: :cascade do |t|
    t.text "app_name"
    t.text "api_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "areas", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "abbreviation"
    t.text "code"
    t.text "address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "uuid"
  end

  create_table "background_operations", force: :cascade do |t|
    t.string "status"
    t.datetime "started_at"
    t.datetime "ended_at"
    t.string "operation_type"
    t.json "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "prepared_by"
  end

  create_table "balance_sheets", id: :serial, force: :cascade do |t|
    t.json "data"
    t.date "as_of"
    t.string "branch_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "prepared_by"
  end

  create_table "banks", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "accounting_code_id"
  end

  create_table "basic_reports", id: :serial, force: :cascade do |t|
    t.string "uuid"
    t.date "reporting_date"
    t.json "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "batch_meber_uploads", id: :serial, force: :cascade do |t|
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "batch_moratoria", id: :serial, force: :cascade do |t|
    t.date "date_initialized"
    t.integer "number_of_days"
    t.text "initialized_by"
    t.text "status"
    t.text "uuid"
    t.text "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "approved_by"
    t.integer "center_id"
    t.index ["center_id"], name: "index_batch_moratoria_on_center_id"
  end

  create_table "beneficiaries", id: :serial, force: :cascade do |t|
    t.text "first_name"
    t.text "last_name"
    t.text "middle_name"
    t.text "relationship"
    t.date "date_of_birth"
    t.boolean "is_primary"
    t.integer "member_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "reference_number"
    t.text "uuid"
    t.string "beneficiary_for"
    t.boolean "is_deceased"
  end

  create_table "branch_banks", id: false, force: :cascade do |t|
    t.integer "branch_id"
    t.integer "bank_id"
    t.index ["branch_id", "bank_id"], name: "branch_bank_index", unique: true
  end

  create_table "branches", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "abbreviation"
    t.text "code"
    t.text "address"
    t.integer "cluster_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "member_counter"
    t.integer "bank_id"
    t.string "uuid"
  end

  create_table "build_categories", id: :serial, force: :cascade do |t|
    t.text "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "build_version_id"
  end

  create_table "build_contents", id: :serial, force: :cascade do |t|
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "build_category_id"
  end

  create_table "build_versions", id: :serial, force: :cascade do |t|
    t.text "build_number"
    t.date "build_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "center_transfer_record_journal_entries", id: :serial, force: :cascade do |t|
    t.integer "center_transfer_record_id"
    t.integer "accounting_code_id"
    t.string "post_type"
    t.decimal "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["accounting_code_id"], name: "ctrj_ac_index"
    t.index ["center_transfer_record_id"], name: "ctrj_ctr_index"
  end

  create_table "center_transfer_records", id: :serial, force: :cascade do |t|
    t.string "uuid"
    t.integer "branch_id"
    t.integer "center_id"
    t.string "reference_number"
    t.string "status"
    t.json "data"
    t.string "prepared_by"
    t.string "approved_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "book"
    t.string "file_file_name"
    t.string "file_content_type"
    t.integer "file_file_size"
    t.datetime "file_updated_at"
    t.text "particular"
    t.index ["branch_id"], name: "index_center_transfer_records_on_branch_id"
    t.index ["center_id"], name: "index_center_transfer_records_on_center_id"
  end

  create_table "centers", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "abbreviation"
    t.text "code"
    t.text "address"
    t.integer "branch_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "meeting_day"
    t.text "officer_in_charge"
    t.text "uuid"
    t.integer "user_id"
    t.index ["user_id"], name: "index_centers_on_user_id"
  end

  create_table "claims", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.integer "center_id"
    t.integer "branch_id"
    t.date "date_prepared"
    t.string "policy_number"
    t.string "type_of_insurance_policy"
    t.string "name_of_insured"
    t.string "beneficiary"
    t.string "classification_of_insured"
    t.date "date_of_birth"
    t.string "gender"
    t.date "date_of_policy_issue"
    t.decimal "face_amount"
    t.date "date_of_death_tpd_accident"
    t.decimal "arrears"
    t.text "cause_of_death_tpd_accident"
    t.decimal "amount_benefit_payable"
    t.decimal "equity_value"
    t.decimal "retirement_fund"
    t.string "prepared_by"
    t.string "length_of_stay"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "returned_contribution"
    t.decimal "total_amount_payable"
    t.string "order_of_child"
    t.string "category_of_cause_of_death_tpd_accident"
    t.date "date_reported"
    t.date "date_paid"
    t.string "uuid"
  end

  create_table "clip_claims", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.integer "center_id"
    t.integer "branch_id"
    t.date "date_prepared"
    t.string "creditors_name"
    t.string "policy_number"
    t.date "date_of_birth"
    t.string "member_name"
    t.string "beneficiary"
    t.string "gender"
    t.string "age"
    t.date "date_of_death"
    t.text "cause_of_death"
    t.date "effective_date_of_coverage"
    t.date "expiration_date_of_coverage"
    t.decimal "amount_of_loan"
    t.string "terms"
    t.decimal "amount_payable_to_beneficiary"
    t.string "prepared_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "amount_payable_to_creditor"
    t.string "type_of_loan"
    t.string "uuid"
  end

  create_table "clusters", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "abbreviation"
    t.text "code"
    t.text "address"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "area_id"
    t.string "uuid"
  end

  create_table "collection_transactions", id: :serial, force: :cascade do |t|
    t.integer "payment_collection_record_id"
    t.integer "loan_payment_id"
    t.text "account_type_code"
    t.text "account_type"
    t.decimal "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "uuid"
    t.index ["loan_payment_id"], name: "index_collection_transactions_on_loan_payment_id"
    t.index ["payment_collection_record_id"], name: "index_collection_transactions_on_payment_collection_record_id"
  end

  create_table "coop_closing_funds", id: :serial, force: :cascade do |t|
    t.string "name"
    t.decimal "ratio"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "daily_report_insurance_account_statuses", id: :serial, force: :cascade do |t|
    t.date "as_of"
    t.string "uuid"
    t.integer "generated_hour"
    t.integer "generated_min"
    t.json "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "branch_id"
    t.index ["branch_id"], name: "index_daily_report_insurance_account_statuses_on_branch_id"
  end

  create_table "daily_reports", id: :serial, force: :cascade do |t|
    t.date "as_of"
    t.json "loan_product_stats"
    t.json "repayments"
    t.json "par"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "member_stats"
    t.json "watchlist"
    t.json "active_loans_by_loan_product"
    t.integer "generated_hour"
    t.integer "generated_min"
    t.json "loan_product_cycle_count_stats"
    t.json "so_member_counts"
  end

  create_table "deceased_and_total_and_permanent_disabilities", id: :serial, force: :cascade do |t|
    t.date "date_prepared"
    t.string "status"
    t.integer "branch_id"
    t.string "prepared_by"
    t.string "approved_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deceased_and_total_and_permanent_disability_records", id: :serial, force: :cascade do |t|
    t.date "deceased_tpd_date"
    t.string "status"
    t.integer "legal_dependent_id"
    t.integer "deceased_and_total_and_permanent_disability_id"
    t.string "classification"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "deposit_time_transactions", force: :cascade do |t|
    t.decimal "amount"
    t.string "lock_in_period"
    t.string "status"
    t.string "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "savings_account_id"
    t.bigint "payment_collection_record_id"
    t.bigint "payment_collection_id"
    t.date "start_date"
    t.date "end_date"
    t.decimal "time_deposit_interest_amount"
    t.index ["payment_collection_id"], name: "index_deposit_time_transactions_on_payment_collection_id"
    t.index ["payment_collection_record_id"], name: "index_deposit_time_transactions_on_payment_collection_record_id"
    t.index ["savings_account_id"], name: "index_deposit_time_transactions_on_savings_account_id"
  end

  create_table "equity_account_transactions", id: :serial, force: :cascade do |t|
    t.integer "equity_account_id"
    t.decimal "amount"
    t.text "transaction_type"
    t.datetime "transacted_at"
    t.text "particular"
    t.text "status"
    t.text "transacted_by"
    t.text "approved_by"
    t.text "voucher_reference_number"
    t.text "transaction_number"
    t.integer "bank_id"
    t.integer "accounting_code_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "uuid"
    t.decimal "beginning_balance"
    t.decimal "ending_balance"
    t.date "transaction_date"
    t.boolean "is_adjustment"
    t.boolean "for_resignation"
  end

  create_table "equity_accounts", id: :serial, force: :cascade do |t|
    t.integer "equity_type_id"
    t.decimal "balance"
    t.integer "member_id"
    t.text "account_number"
    t.text "status"
    t.integer "branch_id"
    t.integer "center_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "uuid"
  end

  create_table "equity_types", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.boolean "is_default"
    t.integer "withdraw_accounting_code_id"
    t.integer "deposit_accounting_code_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "monthly_interest_rate"
    t.decimal "monthly_tax_rate"
  end

  create_table "feedback_options", id: :serial, force: :cascade do |t|
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "feedbacks", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "uuid"
  end

  create_table "file_to_uploads", force: :cascade do |t|
    t.string "status"
    t.string "file_file_name"
    t.string "file_content_type"
    t.bigint "file_file_size"
    t.datetime "file_updated_at"
    t.string "process_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fiscal_year_accounting_codes", id: :serial, force: :cascade do |t|
    t.integer "year"
    t.integer "accounting_code_id"
    t.decimal "beginning_balance"
    t.decimal "ending_balance"
    t.decimal "beginning_credit"
    t.decimal "ending_credit"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "beginning_debit"
    t.decimal "ending_debit"
    t.index ["accounting_code_id"], name: "index_fiscal_year_accounting_codes_on_accounting_code_id"
  end

  create_table "globals", id: :serial, force: :cascade do |t|
    t.integer "loan_processing_fee_account_code_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "holidays", id: :serial, force: :cascade do |t|
    t.string "name"
    t.date "holiday_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "insurance_account_transactions", id: :serial, force: :cascade do |t|
    t.integer "insurance_account_id"
    t.decimal "amount"
    t.text "transaction_type"
    t.datetime "transacted_at"
    t.text "particular"
    t.text "status"
    t.text "transacted_by"
    t.text "approved_by"
    t.text "voucher_reference_number"
    t.text "transaction_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "bank_id"
    t.integer "accounting_code_id"
    t.text "uuid"
    t.decimal "beginning_balance"
    t.decimal "ending_balance"
    t.date "transaction_date"
    t.boolean "is_adjustment"
    t.boolean "for_resignation"
    t.boolean "for_loan_payments"
    t.boolean "for_exit_age"
    t.decimal "equity_value"
    t.json "data"
  end

  create_table "insurance_account_validation_cancellations", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.date "date_cancelled"
    t.text "reason"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "insurance_account_validation_id"
  end

  create_table "insurance_account_validation_records", id: :serial, force: :cascade do |t|
    t.integer "insurance_account_validation_id"
    t.integer "member_id"
    t.integer "center_id"
    t.string "status"
    t.string "transaction_number"
    t.decimal "rf"
    t.decimal "lif_50_percent"
    t.decimal "advance_lif"
    t.decimal "advance_rf"
    t.decimal "interest"
    t.decimal "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "resignation_date"
    t.string "member_classification"
    t.decimal "equity_interest"
    t.boolean "is_void"
    t.string "uuid"
    t.decimal "equity_value"
    t.decimal "policy_loan"
  end

  create_table "insurance_account_validations", id: :serial, force: :cascade do |t|
    t.date "date_prepared"
    t.string "status"
    t.integer "branch_id"
    t.string "prepared_by"
    t.string "approved_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "particular"
    t.string "reference_number"
    t.decimal "total"
    t.string "or_number"
    t.date "date_approved"
    t.date "date_validated"
    t.string "validated_by"
    t.date "date_checked"
    t.string "checked_by"
    t.date "date_cancelled"
    t.string "cancelled_by"
    t.boolean "is_remote"
    t.decimal "total_rf"
    t.decimal "total_50_percent_lif"
    t.decimal "total_advance_lif"
    t.decimal "total_advance_rf"
    t.decimal "total_interest"
    t.decimal "total_equity_interest"
    t.string "uuid"
    t.decimal "total_policy_loan"
  end

  create_table "insurance_accounts", id: :serial, force: :cascade do |t|
    t.integer "insurance_type_id"
    t.decimal "balance"
    t.integer "member_id"
    t.text "account_number"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "status"
    t.integer "branch_id"
    t.integer "center_id"
    t.text "uuid"
    t.decimal "equity_value"
  end

  create_table "insurance_remittance_records", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.integer "insurance_type_id"
    t.integer "insurance_remittance_id"
    t.decimal "amount"
    t.text "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["insurance_remittance_id"], name: "index_insurance_remittance_records_on_insurance_remittance_id"
    t.index ["insurance_type_id"], name: "index_insurance_remittance_records_on_insurance_type_id"
    t.index ["member_id"], name: "index_insurance_remittance_records_on_member_id"
  end

  create_table "insurance_remittances", id: :serial, force: :cascade do |t|
    t.text "uuid"
    t.decimal "amount"
    t.date "start_date"
    t.date "end_date"
    t.text "particular"
    t.text "status"
    t.text "voucher_reference_number"
    t.text "or_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "center_id"
    t.integer "branch_id"
  end

  create_table "insurance_types", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.integer "accounting_code_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "is_default"
    t.integer "withdraw_accounting_code_id"
    t.integer "deposit_accounting_code_id"
    t.decimal "monthly_interest_rate"
    t.decimal "monthly_tax_rate"
    t.decimal "default_periodic_payment"
    t.boolean "use_resign_rules"
    t.integer "resign_rule_num_years"
    t.decimal "resign_rule_withdrawal_percentage"
  end

  create_table "interest_on_share_capital_collection_records", id: :serial, force: :cascade do |t|
    t.integer "interest_on_share_capital_collection_id"
    t.json "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "interest_on_share_capital_collections", id: :serial, force: :cascade do |t|
    t.decimal "total_equity_amount"
    t.decimal "total_interest_amount"
    t.date "collection_date"
    t.string "refference_number"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "equity_rate"
    t.date "posting_date"
    t.string "savings_account_amount"
    t.string "cbu_account_amount"
  end

  create_table "journal_entries", id: :serial, force: :cascade do |t|
    t.integer "accounting_code_id"
    t.decimal "amount"
    t.text "uuid"
    t.text "post_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "voucher_id"
    t.boolean "is_year_end_closing"
  end

  create_table "legal_dependents", id: :serial, force: :cascade do |t|
    t.text "first_name"
    t.text "middle_name"
    t.date "date_of_birth"
    t.integer "member_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "last_name"
    t.text "reference_number"
    t.text "uuid"
    t.text "educational_attainment"
    t.text "course"
    t.boolean "is_deceased"
    t.string "relationship"
    t.boolean "is_tpd"
  end

  create_table "loan_deduction_entries", id: :serial, force: :cascade do |t|
    t.integer "loan_product_id"
    t.integer "loan_deduction_id"
    t.decimal "val"
    t.boolean "is_percent"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "accounting_code_id"
    t.boolean "is_one_time_deduction"
    t.boolean "use_term_map"
    t.decimal "t_val_15"
    t.decimal "t_val_25"
    t.decimal "t_val_50"
    t.decimal "t_val_35"
    t.boolean "skip_for_special_loan_fund"
    t.boolean "use_for_special_loan_fund"
    t.boolean "entry_point_only"
  end

  create_table "loan_deductions", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "is_one_time_fee"
  end

  create_table "loan_insurance_accounts", id: :serial, force: :cascade do |t|
    t.integer "insurance_type_id"
    t.decimal "balance"
    t.integer "member_id"
    t.string "account_number"
    t.string "status"
    t.integer "branch_id"
    t.integer "center_id"
    t.string "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "loan_insurance_deduction_entries", id: :serial, force: :cascade do |t|
    t.integer "loan_product_id"
    t.integer "insurance_type_id"
    t.decimal "val"
    t.boolean "is_percent"
    t.integer "accounting_code_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "is_one_time_deduction"
  end

  create_table "loan_insurance_records", id: :serial, force: :cascade do |t|
    t.date "date_released"
    t.date "maturity_date"
    t.integer "loan_term"
    t.decimal "loan_amount"
    t.decimal "loan_insurance_amount"
    t.integer "member_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "uuid"
    t.string "reference_number"
    t.string "loan_category"
    t.integer "loan_insurance_id"
    t.string "status"
    t.string "pn_number"
  end

  create_table "loan_insurance_types", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "accounting_code_id"
    t.integer "cash_debit_accounting_code_id"
    t.integer "cash_credit_accounting_code_id"
    t.integer "unremitted_debit_accounting_code_id"
    t.integer "unremitted_credit_accounting_code_id"
    t.integer "accounting_fund_id"
  end

  create_table "loan_insurances", id: :serial, force: :cascade do |t|
    t.string "uuid"
    t.string "reference_number"
    t.date "date_prepared"
    t.string "status"
    t.integer "branch_id"
    t.string "prepared_by"
    t.decimal "total_amount"
    t.text "particular"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "loan_insurance_type_id"
    t.string "collection_type"
    t.string "or_number"
    t.string "reversed_reference_number"
    t.decimal "total_loan_amount"
  end

  create_table "loan_membership_payments", id: :serial, force: :cascade do |t|
    t.integer "loan_id"
    t.decimal "amount_paid"
    t.date "paid_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["loan_id"], name: "index_loan_membership_payments_on_loan_id"
  end

  create_table "loan_moratoria", id: :serial, force: :cascade do |t|
    t.integer "ammortization_schedule_entry_id"
    t.date "previous_due_at"
    t.date "new_due_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "reason"
    t.decimal "amount_delayed"
  end

  create_table "loan_payment_adjustment_records", id: :serial, force: :cascade do |t|
    t.integer "loan_payment_adjustment_id"
    t.integer "loan_payment_id"
    t.decimal "new_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["loan_payment_adjustment_id"], name: "index_clpar_lpa"
    t.index ["loan_payment_id"], name: "index_clpa_lp"
  end

  create_table "loan_payment_adjustments", id: :serial, force: :cascade do |t|
    t.string "uuid"
    t.string "status"
    t.string "prepared_by"
    t.string "approved_by"
    t.date "date_approved"
    t.string "voucher_reference_number"
    t.string "particular"
    t.integer "branch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["branch_id"], name: "index_clpa_branch"
  end

  create_table "loan_payment_amortization_schedule_entries", id: :serial, force: :cascade do |t|
    t.integer "loan_payment_id"
    t.integer "ammortization_schedule_entry_id"
    t.decimal "principal_paid"
    t.decimal "interest_paid"
  end

  create_table "loan_payment_details", id: :serial, force: :cascade do |t|
    t.integer "loan_payment_id"
    t.date "paid_at"
    t.decimal "total_amount"
    t.decimal "paid_principal"
    t.decimal "paid_interest"
    t.text "payment_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "loan_payment_insurance_account_deposits", id: :serial, force: :cascade do |t|
    t.integer "loan_payment_id"
    t.decimal "amount"
    t.integer "insurance_account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "loan_payments", id: :serial, force: :cascade do |t|
    t.integer "ammortization_schedule_entry_id"
    t.decimal "amount"
    t.date "paid_at"
    t.text "payment_type"
    t.text "uuid"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "status"
    t.text "approved_by"
    t.text "prepared_by"
    t.integer "loan_id"
    t.decimal "paid_principal"
    t.decimal "paid_interest"
    t.decimal "wp_amount"
    t.integer "co_maker_id"
    t.integer "savings_account_id"
    t.text "particular"
    t.text "voucher_reference_number"
    t.text "wp_voucher_reference_number"
    t.text "wp_particular"
    t.integer "collection_payment_id"
    t.decimal "cp_amount"
    t.decimal "loan_payment_amount"
    t.decimal "deposit_amount"
    t.decimal "insurance_deposit_amount"
    t.text "payment_status"
    t.boolean "is_void"
    t.json "meta"
    t.decimal "principal_due"
    t.decimal "interest_due"
  end

  create_table "loan_product_dependencies", id: :serial, force: :cascade do |t|
    t.integer "cycle_count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "dependent_loan_product_id"
    t.integer "loan_product_id"
  end

  create_table "loan_product_ep_cycle_count_max_amounts", id: :serial, force: :cascade do |t|
    t.integer "loan_product_id"
    t.integer "cycle_count"
    t.decimal "max_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "loan_product_membership_payments", id: :serial, force: :cascade do |t|
    t.integer "loan_product_id"
    t.integer "membership_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["loan_product_id"], name: "index_loan_product_membership_payments_on_loan_product_id"
    t.index ["membership_type_id"], name: "index_loan_product_membership_payments_on_membership_type_id"
  end

  create_table "loan_product_types", id: :serial, force: :cascade do |t|
    t.text "name"
    t.integer "loan_product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "processing_fee_15"
    t.decimal "processing_fee_25"
    t.decimal "processing_fee_35"
    t.decimal "processing_fee_50"
    t.decimal "fixed_processing_fee"
    t.decimal "interest_rate"
    t.boolean "insured"
  end

  create_table "loan_products", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "description"
    t.integer "amount_released_accounting_code_id"
    t.integer "accounting_code_transaction_id"
    t.integer "savings_type_id"
    t.decimal "default_maintaining_balance_val"
    t.decimal "default_processing_fee"
    t.integer "interest_accounting_code_id"
    t.integer "accounting_code_processing_fee_id"
    t.integer "next_application_interval"
    t.integer "priority"
    t.boolean "is_entry_point"
    t.boolean "maintaining_balance_exception"
    t.decimal "maintaining_balance_exception_minimum_amount"
    t.boolean "waive_processing_fee_for_existing_entry_point_loans"
    t.integer "maintaining_balance_exception_cycle_count"
    t.decimal "max_loan_amount"
    t.integer "writeoff_dr_accounting_code_id"
    t.integer "writeoff_cr_accounting_code_id"
    t.boolean "always_have_processing_fee"
    t.boolean "use_amount_released_accounting_code"
    t.decimal "default_annual_interest_rate"
    t.boolean "overridable_interest_rate"
    t.boolean "insured"
    t.string "uuid"
  end

  create_table "loans", id: :serial, force: :cascade do |t|
    t.integer "center_id"
    t.integer "branch_id"
    t.date "date_prepared"
    t.integer "member_id"
    t.decimal "amount"
    t.integer "loan_product_id"
    t.text "term"
    t.text "pn_number"
    t.text "payment_type"
    t.integer "num_installments"
    t.text "voucher_check_number"
    t.integer "bank_id"
    t.text "co_maker_two"
    t.text "voucher_payee"
    t.text "voucher_particular"
    t.text "voucher_reference_number"
    t.date "voucher_date_requested"
    t.integer "project_type_id"
    t.text "status"
    t.decimal "interest_rate"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "project_type_category_id"
    t.text "bank_check_number"
    t.date "date_requested"
    t.date "first_date_of_payment"
    t.decimal "maintaining_balance_val"
    t.decimal "processing_fee"
    t.integer "co_maker_one_id"
    t.decimal "remaining_balance"
    t.text "clip_number"
    t.text "batch_transaction_reference_number"
    t.decimal "amount_released"
    t.integer "loan_cycle_count"
    t.integer "loan_product_type_id"
    t.boolean "business_permit_available"
    t.text "member_full_name"
    t.text "prepared_by"
    t.boolean "override_maintaining_balance"
    t.boolean "override_processing_fee"
    t.text "book"
    t.date "maturity_date"
    t.text "uuid"
    t.boolean "override_installment_interval"
    t.integer "installment_override"
    t.decimal "original_loan_amount"
    t.integer "original_num_installments"
    t.decimal "old_principal_balance"
    t.decimal "old_interest_balance"
    t.decimal "total_amount_due"
    t.boolean "override_interest"
    t.decimal "override_interest_rate"
    t.integer "num_years_business"
    t.integer "num_months_business"
    t.integer "num_workers_business"
    t.boolean "override_insurance_deductions"
    t.boolean "insurance_as_disbursement_correction"
    t.date "date_approved"
    t.decimal "insured_amount"
    t.boolean "is_insured"
    t.string "beneficiary_first_name"
    t.string "beneficiary_middle_name"
    t.string "beneficiary_last_name"
    t.date "beneficiary_date_of_birth"
    t.string "beneficiary_relationship"
    t.date "date_of_release"
    t.json "meta"
    t.date "date_completed"
    t.decimal "interest_amount"
  end

  create_table "major_accounts", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "major_group_id"
  end

  create_table "major_groups", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.text "dc_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "member_attachment_files", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "file_photo_file_name"
    t.string "file_photo_content_type"
    t.integer "file_photo_file_size"
    t.datetime "file_photo_updated_at"
    t.index ["member_id"], name: "index_member_attachment_files_on_member_id"
  end

  create_table "member_certificates", force: :cascade do |t|
    t.integer "member_id"
    t.string "certificate_number"
    t.string "uuid"
    t.date "date_issue"
    t.date "date_printed"
    t.boolean "is_void"
    t.boolean "is_printed"
    t.integer "number_of_certificates"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "member_equity_transfer_requests", id: :serial, force: :cascade do |t|
    t.integer "equity_account_id"
    t.integer "member_transfer_request_id"
    t.decimal "amount"
    t.integer "dr_accounting_code_id"
    t.integer "cr_accounting_code_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["equity_account_id"], name: "metr_ea_index"
    t.index ["member_transfer_request_id"], name: "metr_mtr_index"
  end

  create_table "member_insurance_transfer_requests", id: :serial, force: :cascade do |t|
    t.integer "insurance_account_id"
    t.integer "member_transfer_request_id"
    t.decimal "amount"
    t.integer "dr_accounting_code_id"
    t.integer "cr_accounting_code_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["insurance_account_id"], name: "mitr_ia_index"
    t.index ["member_transfer_request_id"], name: "mitr_mtr_index"
  end

  create_table "member_loan_cycles", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.integer "loan_product_id"
    t.integer "cycle"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "member_loan_transfer_requests", id: :serial, force: :cascade do |t|
    t.integer "member_transfer_request_id"
    t.decimal "amount"
    t.integer "dr_accounting_code_id"
    t.integer "cr_accounting_code_id"
    t.integer "loan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "interest_balance"
    t.index ["loan_id"], name: "index_member_loan_transfer_requests_on_loan_id"
    t.index ["member_transfer_request_id"], name: "mtr_mltr_index"
  end

  create_table "member_membership_payments", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.integer "membership_type_id"
    t.decimal "amount_paid"
    t.date "paid_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_id"], name: "index_member_membership_payments_on_member_id"
    t.index ["membership_type_id"], name: "index_member_membership_payments_on_membership_type_id"
  end

  create_table "member_resignations", id: :serial, force: :cascade do |t|
    t.text "uuid"
    t.integer "member_id"
    t.date "date_resigned"
    t.text "resignation_type"
    t.text "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "or_number"
    t.integer "payment_collection_id"
    t.text "resignation_type_particular"
    t.text "resignation_type_code"
    t.decimal "deposit_amount"
    t.string "reference_number"
    t.index ["member_id"], name: "index_member_resignations_on_member_id"
  end

  create_table "member_savings_transfer_requests", id: :serial, force: :cascade do |t|
    t.integer "savings_account_id"
    t.integer "member_transfer_request_id"
    t.decimal "amount"
    t.integer "dr_accounting_code_id"
    t.integer "cr_accounting_code_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_transfer_request_id"], name: "mstr_mtr_index"
    t.index ["savings_account_id"], name: "mstr_sa_index"
  end

  create_table "member_shares", id: :serial, force: :cascade do |t|
    t.text "uuid"
    t.text "certificate_number"
    t.date "date_of_issue"
    t.integer "number_of_shares"
    t.integer "member_id"
    t.integer "no_original_shares"
    t.text "no_of_shares_transferred"
    t.integer "no_original_certificate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "status"
    t.boolean "printed"
    t.date "date_printed"
    t.boolean "is_void"
    t.index ["member_id"], name: "index_member_shares_on_member_id"
  end

  create_table "member_transfer_requests", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.string "uuid"
    t.string "status"
    t.date "date_of_request"
    t.date "date_transfered"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "meta"
    t.string "particular"
    t.index ["member_id"], name: "index_member_transfer_requests_on_member_id"
  end

  create_table "member_weekly_expenses", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.decimal "amount"
    t.text "source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "loan_id"
    t.index ["member_id"], name: "index_member_weekly_expenses_on_member_id"
  end

  create_table "member_weekly_incomes", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.decimal "amount"
    t.text "source"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "loan_id"
    t.index ["member_id"], name: "index_member_weekly_incomes_on_member_id"
  end

  create_table "member_withdrawal_fund_balances", id: :serial, force: :cascade do |t|
    t.integer "member_withdrawal_id"
    t.string "account_type"
    t.decimal "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "withdrawal_amount"
    t.index ["member_withdrawal_id"], name: "index_member_withdrawal_fund_balances_on_member_withdrawal_id"
  end

  create_table "member_withdrawal_loans", id: :serial, force: :cascade do |t|
    t.integer "member_withdrawal_id"
    t.integer "loan_id"
    t.decimal "total_balance"
    t.decimal "principal_balance"
    t.decimal "interest_balance"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "payment_amount"
    t.index ["loan_id"], name: "index_member_withdrawal_loans_on_loan_id"
    t.index ["member_withdrawal_id"], name: "index_member_withdrawal_loans_on_member_withdrawal_id"
  end

  create_table "member_withdrawals", id: :serial, force: :cascade do |t|
    t.string "uuid"
    t.string "reference_number"
    t.string "book"
    t.date "paid_at"
    t.string "status"
    t.decimal "total_withdrawals"
    t.decimal "total_payments"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "member_id"
    t.string "approved_by"
    t.string "prepared_by"
    t.index ["member_id"], name: "index_member_withdrawals_on_member_id"
  end

  create_table "member_working_relatives", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.text "name"
    t.text "relationship"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_id"], name: "index_member_working_relatives_on_member_id"
  end

  create_table "members", id: :serial, force: :cascade do |t|
    t.text "first_name"
    t.text "middle_name"
    t.text "last_name"
    t.text "gender"
    t.date "date_of_birth"
    t.text "civil_status"
    t.date "spouse_date_of_birth"
    t.text "home_number"
    t.text "processed_by"
    t.text "approved_by"
    t.text "identification_number"
    t.text "address_street"
    t.text "address_barangay"
    t.text "address_city"
    t.text "mobile_number"
    t.text "place_of_birth"
    t.integer "number_of_children"
    t.text "sss_number"
    t.text "spouse_first_name"
    t.text "spouse_middle_name"
    t.text "spouse_last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "status"
    t.text "member_type"
    t.text "profile_picture_file_name"
    t.text "profile_picture_content_type"
    t.integer "profile_picture_file_size"
    t.datetime "profile_picture_updated_at"
    t.integer "religion_id"
    t.text "spouse_occupation"
    t.integer "branch_id"
    t.integer "center_id"
    t.text "signature_file_name"
    t.text "signature_content_type"
    t.integer "signature_file_size"
    t.datetime "signature_updated_at"
    t.text "uuid"
    t.text "insurance_status"
    t.date "previous_mfi_member_since"
    t.text "pag_ibig_number"
    t.text "phil_health_number"
    t.date "previous_mii_member_since"
    t.text "type_of_housing"
    t.integer "num_children"
    t.integer "num_children_elementary"
    t.integer "num_children_high_school"
    t.integer "num_children_college"
    t.integer "bank_id"
    t.text "bank_account_type"
    t.text "bank"
    t.text "tin_number"
    t.text "proof_of_housing"
    t.integer "num_workers_business"
    t.text "reason_for_joining"
    t.boolean "is_experienced_with_microfinance"
    t.text "experience_with_microfinance"
    t.integer "num_years_housing"
    t.integer "num_months_housing"
    t.boolean "is_editable"
    t.integer "num_years_business"
    t.integer "num_months_business"
    t.string "old_certificate_number"
    t.json "meta"
    t.boolean "auto_active"
    t.date "date_resigned"
    t.string "resignation_type"
    t.string "resignation_code"
    t.string "resignation_reason"
    t.string "resignation_accounting_reference_number"
    t.date "insurance_date_resigned"
    t.boolean "spouse_is_deceased"
    t.boolean "is_reinstate"
    t.date "old_previous_mii_member_since"
    t.boolean "is_balik_kasapi"
    t.boolean "is_deceased"
  end

  create_table "membership_account_payments", id: :serial, force: :cascade do |t|
    t.integer "membership_payment_id"
    t.text "account_type"
    t.decimal "amount"
    t.text "deposit_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["membership_payment_id"], name: "index_membership_account_payments_on_membership_payment_id"
  end

  create_table "membership_payments", id: :serial, force: :cascade do |t|
    t.integer "membership_type_id"
    t.integer "member_id"
    t.date "paid_at"
    t.decimal "amount_paid"
    t.text "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "status"
    t.integer "loan_id"
    t.index ["loan_id"], name: "index_membership_payments_on_loan_id"
    t.index ["member_id"], name: "index_membership_payments_on_member_id"
    t.index ["membership_type_id"], name: "index_membership_payments_on_membership_type_id"
  end

  create_table "membership_types", id: :serial, force: :cascade do |t|
    t.text "name"
    t.decimal "fee"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "dr_accounting_entry_id"
    t.integer "cr_accounting_entry_id"
    t.boolean "activated_for_insurance"
    t.boolean "activated_for_loans"
  end

  create_table "monthly_interest_and_tax_closing_records", id: :serial, force: :cascade do |t|
    t.integer "month"
    t.datetime "closed_at"
    t.date "closing_date"
    t.decimal "total_interest"
    t.decimal "total_tax"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "uuid"
    t.text "status"
    t.text "voucher_reference_number"
    t.integer "year"
    t.integer "branch_id"
    t.string "book"
    t.boolean "special"
    t.string "voided_by"
    t.string "reverse_voucher_reference_number"
    t.index ["branch_id"], name: "index_monthly_interest_and_tax_closing_records_on_branch_id"
  end

  create_table "monthly_reports", id: :serial, force: :cascade do |t|
    t.string "report_type"
    t.string "generated_by"
    t.date "date_generated"
    t.json "data"
    t.integer "month"
    t.integer "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "branch_code"
  end

  create_table "monthly_trial_balances", id: :serial, force: :cascade do |t|
    t.date "start_date"
    t.date "end_date"
    t.json "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mother_accounting_codes", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "major_account_id"
    t.text "subcode"
  end

  create_table "negative_loan_payments", id: :serial, force: :cascade do |t|
    t.integer "loan_id"
    t.decimal "amount"
    t.integer "loan_payment_id"
    t.string "status"
    t.string "generated_by"
    t.string "approved_by"
    t.date "transacted_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "particular"
    t.index ["loan_id"], name: "index_negative_loan_payments_on_loan_id"
    t.index ["loan_payment_id"], name: "index_negative_loan_payments_on_loan_payment_id"
  end

  create_table "patronage_refund_collection_records", id: :serial, force: :cascade do |t|
    t.integer "patronage_refund_collection_id"
    t.json "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["patronage_refund_collection_id"], name: "patronage_refund_collection"
  end

  create_table "patronage_refund_collections", id: :serial, force: :cascade do |t|
    t.decimal "total_loan_interest_amount"
    t.decimal "total_interest_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "patronage_rate"
    t.date "as_of"
    t.string "reference_number"
    t.string "status"
    t.date "posting_date"
    t.string "total_savings_amount"
    t.string "total_cbu_amount"
  end

  create_table "payee_accounts", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "contact_number"
    t.string "address"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "payment_collection_records", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.text "loan_product_code"
    t.integer "payment_collection_id"
    t.text "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "total_cp_amount"
    t.decimal "total_wp_amount"
    t.integer "loan_product_id"
    t.decimal "total_principal_amount"
    t.decimal "total_interest_amount"
    t.index ["loan_product_id"], name: "index_payment_collection_records_on_loan_product_id"
    t.index ["member_id"], name: "index_payment_collection_records_on_member_id"
    t.index ["payment_collection_id"], name: "index_payment_collection_records_on_payment_collection_id"
  end

  create_table "payment_collections", id: :serial, force: :cascade do |t|
    t.text "reference_number"
    t.text "particular"
    t.text "uuid"
    t.text "or_number"
    t.decimal "total_amount"
    t.decimal "total_wp_amount"
    t.decimal "total_cp_amount"
    t.decimal "total_principal_amount"
    t.decimal "total_interest_amount"
    t.integer "branch_id"
    t.integer "center_id"
    t.text "prepared_by"
    t.text "approved_by"
    t.text "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "paid_at"
    t.text "reversed_reference_number"
    t.text "payment_type"
    t.text "check_number"
    t.text "check_voucher_number"
    t.date "date_of_check"
    t.text "check_name"
    t.boolean "for_resignation"
    t.text "collected_by"
    t.text "checked_by"
    t.text "encoded_by"
    t.boolean "is_special_report"
    t.integer "use_withdrawal_accounting_code_id"
    t.integer "debit_accounting_code_id"
    t.boolean "for_adjustment"
    t.string "book"
    t.integer "accounting_fund_id"
    t.integer "credit_accounting_code_id"
    t.boolean "for_exit_age"
    t.integer "override_default_savings_dr_accounting_code_id"
    t.text "payee"
    t.json "meta"
    t.decimal "total_time_deposit_amount"
    t.index ["accounting_fund_id"], name: "index_payment_collections_on_accounting_fund_id"
    t.index ["branch_id"], name: "index_payment_collections_on_branch_id"
    t.index ["center_id"], name: "index_payment_collections_on_center_id"
  end

  create_table "project_type_categories", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "uuid"
  end

  create_table "project_types", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.integer "project_type_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string "uuid"
  end

  create_table "regular_deposit_amounts", id: :serial, force: :cascade do |t|
    t.decimal "min_amount"
    t.decimal "max_amount"
    t.decimal "deposit_amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "regular_deposit_interval_id"
  end

  create_table "regular_deposit_intervals", id: :serial, force: :cascade do |t|
    t.text "name"
    t.integer "interval"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reinstatement_records", id: :serial, force: :cascade do |t|
    t.date "reinstatement_date"
    t.string "status"
    t.integer "member_id"
    t.integer "reinstatement_id"
    t.date "old_recognition_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reinstatements", id: :serial, force: :cascade do |t|
    t.date "date_prepared"
    t.string "status"
    t.integer "branch_id"
    t.string "prepared_by"
    t.string "approved_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "religions", id: :serial, force: :cascade do |t|
    t.text "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "request_records", id: :serial, force: :cascade do |t|
    t.json "info"
    t.string "status"
    t.string "prepared_by"
    t.string "request_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "date_requested"
    t.string "uuid"
    t.date "date_approved"
    t.string "approved_by"
  end

  create_table "resignation_loan_balance_payments", id: :serial, force: :cascade do |t|
    t.text "account_type"
    t.text "account_type_code"
    t.integer "resignation_loan_balance_id"
    t.decimal "amount"
    t.text "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["resignation_loan_balance_id"], name: "rlb_index"
  end

  create_table "resignation_loan_balances", id: :serial, force: :cascade do |t|
    t.text "uuid"
    t.decimal "balance"
    t.integer "loan_id"
    t.integer "member_resignation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "interest_balance"
    t.decimal "principal_balance"
    t.index ["loan_id"], name: "index_resignation_loan_balances_on_loan_id"
    t.index ["member_resignation_id"], name: "index_resignation_loan_balances_on_member_resignation_id"
  end

  create_table "resignation_withdrawals", id: :serial, force: :cascade do |t|
    t.text "uuid"
    t.text "account_type"
    t.text "account_type_code"
    t.decimal "amount"
    t.integer "member_resignation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["member_resignation_id"], name: "mem_res_w_index"
  end

  create_table "savings_account_transactions", id: :serial, force: :cascade do |t|
    t.integer "savings_account_id"
    t.decimal "amount"
    t.text "transaction_type"
    t.datetime "transacted_at"
    t.text "particular"
    t.text "status"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "transacted_by"
    t.text "voucher_reference_number"
    t.text "transaction_number"
    t.boolean "is_withdraw_payment"
    t.integer "loan_payment_id"
    t.integer "accounting_code_id"
    t.integer "bank_id"
    t.decimal "beginning_balance"
    t.decimal "ending_balance"
    t.text "uuid"
    t.date "transaction_date"
    t.integer "member_id"
    t.integer "savings_type_id"
    t.boolean "is_adjustment"
    t.boolean "for_resignation"
    t.boolean "for_loan_payments"
    t.string "for_lock_in"
  end

  create_table "savings_accounts", id: :serial, force: :cascade do |t|
    t.integer "savings_type_id"
    t.integer "member_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "account_number"
    t.decimal "balance"
    t.text "status"
    t.decimal "maintaining_balance"
    t.integer "center_id"
    t.integer "branch_id"
    t.text "uuid"
    t.decimal "lock_in_amount"
  end

  create_table "savings_types", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean "is_default"
    t.integer "withdraw_accounting_code_id"
    t.integer "deposit_accounting_code_id"
    t.decimal "monthly_interest_rate"
    t.decimal "monthly_tax_rate"
    t.integer "expense_accounting_code_id"
    t.integer "tax_accounting_code_id"
    t.integer "interest_accounting_code_id"
    t.decimal "annual_interest_rate"
  end

  create_table "settings", id: :serial, force: :cascade do |t|
    t.text "key"
    t.text "val"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "statement_of_account_fund_entries", id: :serial, force: :cascade do |t|
    t.string "account_type"
    t.integer "account_type_reference_id"
    t.integer "account_reference_id"
    t.date "date_of_transaction"
    t.decimal "debit_amount"
    t.decimal "credit_amount"
    t.integer "member_id"
    t.integer "branch_id"
    t.integer "center_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["branch_id"], name: "index_statement_of_account_fund_entries_on_branch_id"
    t.index ["center_id"], name: "index_statement_of_account_fund_entries_on_center_id"
    t.index ["member_id"], name: "index_statement_of_account_fund_entries_on_member_id"
  end

  create_table "subsidiary_adjustment_entries", id: :serial, force: :cascade do |t|
    t.integer "subsidiary_adjustment_id"
    t.integer "accounting_code_id"
    t.string "post_type"
    t.decimal "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["accounting_code_id"], name: "index_subsidiary_adjustment_entries_on_accounting_code_id"
    t.index ["subsidiary_adjustment_id"], name: "index_subsidiary_adjustment_entries_on_subsidiary_adjustment_id"
  end

  create_table "subsidiary_adjustment_records", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.integer "subsidiary_adjustment_id"
    t.string "adjustment_type"
    t.string "account_code"
    t.decimal "amount"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "accounting_code_id"
    t.decimal "beginning_balance"
    t.decimal "ending_balance"
    t.index ["accounting_code_id"], name: "index_subsidiary_adjustment_records_on_accounting_code_id"
    t.index ["member_id"], name: "index_subsidiary_adjustment_records_on_member_id"
    t.index ["subsidiary_adjustment_id"], name: "index_subsidiary_adjustment_records_on_subsidiary_adjustment_id"
  end

  create_table "subsidiary_adjustments", id: :serial, force: :cascade do |t|
    t.string "uuid"
    t.string "status"
    t.string "prepared_by"
    t.string "approved_by"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "date_approved"
    t.string "voucher_reference_number"
    t.text "particular"
    t.integer "branch_id"
    t.integer "accounting_fund_id"
    t.index ["branch_id"], name: "index_subsidiary_adjustments_on_branch_id"
  end

  create_table "summaries", id: :serial, force: :cascade do |t|
    t.date "as_of"
    t.date "start_date"
    t.date "end_date"
    t.string "summary_type"
    t.json "meta"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "survey_question_options", id: :serial, force: :cascade do |t|
    t.integer "survey_question_id"
    t.text "option"
    t.text "uuid"
    t.integer "priority"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "score"
    t.index ["survey_question_id"], name: "index_survey_question_options_on_survey_question_id"
  end

  create_table "survey_questions", id: :serial, force: :cascade do |t|
    t.integer "survey_id"
    t.text "uuid"
    t.integer "priority"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["survey_id"], name: "index_survey_questions_on_survey_id"
  end

  create_table "survey_respondent_answers", id: :serial, force: :cascade do |t|
    t.integer "survey_respondent_id"
    t.integer "survey_question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "uuid"
    t.integer "survey_question_option_id"
    t.index ["survey_question_id"], name: "index_survey_respondent_answers_on_survey_question_id"
    t.index ["survey_respondent_id"], name: "index_survey_respondent_answers_on_survey_respondent_id"
  end

  create_table "survey_respondents", id: :serial, force: :cascade do |t|
    t.integer "member_id"
    t.date "date_answered"
    t.integer "survey_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "uuid"
    t.index ["member_id"], name: "index_survey_respondents_on_member_id"
    t.index ["survey_id"], name: "index_survey_respondents_on_survey_id"
  end

  create_table "surveys", id: :serial, force: :cascade do |t|
    t.text "uuid"
    t.text "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_default"
  end

  create_table "tax_and_interest_transactions", id: :serial, force: :cascade do |t|
    t.decimal "tax_amount"
    t.decimal "interest_amount"
    t.integer "savings_account_id"
    t.text "uuid"
    t.integer "monthly_interest_and_tax_closing_record_id"
    t.decimal "monthly_tax_rate"
    t.decimal "monthly_interest_rate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "beginning_balance"
    t.decimal "ending_balance"
    t.decimal "annual_interest_rate"
    t.index ["monthly_interest_and_tax_closing_record_id"], name: "mit_cr_index"
    t.index ["savings_account_id"], name: "sa_index"
  end

  create_table "time_deposit_withdrawals", force: :cascade do |t|
    t.string "refference_number"
    t.string "status"
    t.string "total_interest"
    t.date "posting_date"
    t.string "uuid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "savings_transactions"
    t.bigint "savings_accounts_id"
    t.string "total_withdrawal_amount"
    t.bigint "deposit_time_transaction_id"
    t.string "after_interest_trans"
    t.index ["deposit_time_transaction_id"], name: "index_time_deposit_withdrawals_on_deposit_time_transaction_id"
    t.index ["savings_accounts_id"], name: "index_time_deposit_withdrawals_on_savings_accounts_id"
  end

  create_table "transfer_adjustment_records", id: :serial, force: :cascade do |t|
    t.integer "accounting_code_id"
    t.decimal "amount"
    t.integer "transfer_adjustment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["accounting_code_id"], name: "index_transfer_adjustment_records_on_accounting_code_id"
    t.index ["transfer_adjustment_id"], name: "index_transfer_adjustment_records_on_transfer_adjustment_id"
  end

  create_table "transfer_adjustments", id: :serial, force: :cascade do |t|
    t.string "uuid"
    t.string "status"
    t.string "prepared_by"
    t.string "approved_by"
    t.date "date_approved"
    t.string "accounting_entry_reference_number"
    t.string "particular"
    t.integer "branch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "transaction_type"
    t.integer "member_id"
    t.decimal "amount"
    t.string "account_type"
    t.string "account_code"
    t.index ["branch_id"], name: "index_transfer_adjustments_on_branch_id"
  end

  create_table "user_activities", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "first_name"
    t.string "last_name"
    t.string "uuid"
    t.text "content"
    t.integer "record_reference_id"
    t.string "username"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role"
  end

  create_table "user_branches", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "branch_id"
    t.index ["user_id", "branch_id"], name: "user_branch_index", unique: true
  end

  create_table "user_centers", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "center_id"
    t.index ["user_id", "center_id"], name: "user_center_index", unique: true
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.text "email", default: "", null: false
    t.text "encrypted_password", default: "", null: false
    t.text "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.text "current_sign_in_ip"
    t.text "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text "identification_number"
    t.text "first_name"
    t.text "last_name"
    t.text "avatar_file_name"
    t.text "avatar_content_type"
    t.integer "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.text "role"
    t.integer "cluster_id"
    t.text "username"
    t.boolean "is_active"
    t.boolean "not_superuser"
    t.boolean "is_regular"
    t.date "incentive_eligble_date"
    t.string "uuid"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "voucher_options", id: :serial, force: :cascade do |t|
    t.text "name"
    t.text "val"
    t.integer "voucher_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vouchers", id: :serial, force: :cascade do |t|
    t.text "book"
    t.text "uuid"
    t.text "reference_number"
    t.date "date_prepared"
    t.text "particular"
    t.text "approved_by"
    t.text "prepared_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "branch_id"
    t.text "status"
    t.text "master_reference_number"
    t.date "date_posted"
    t.text "name_of_receiver"
    t.date "date_of_check"
    t.text "or_number"
    t.text "check_number"
    t.text "check_voucher_number"
    t.text "payee"
    t.integer "accounting_fund_id"
    t.boolean "is_year_end_closing"
    t.string "sub_reference_number"
  end

  create_table "withdraw_payment_transactions", force: :cascade do |t|
    t.decimal "amount"
    t.integer "savings_account_id"
    t.integer "member_id"
    t.integer "co_maker_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wp_records", force: :cascade do |t|
    t.bigint "member_id"
    t.decimal "amount"
    t.bigint "loan_payment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["loan_payment_id"], name: "index_wp_records_on_loan_payment_id"
    t.index ["member_id"], name: "index_wp_records_on_member_id"
  end

  create_table "year_end_closing_records", id: :serial, force: :cascade do |t|
    t.json "data"
    t.integer "voucher_id"
    t.json "trial_balance"
    t.date "date_closing"
    t.integer "year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json "balance_sheet"
    t.json "income_statement"
    t.boolean "for_insurance"
    t.index ["voucher_id"], name: "index_year_end_closing_records_on_voucher_id"
  end

  add_foreign_key "batch_moratoria", "centers"
  add_foreign_key "center_transfer_record_journal_entries", "accounting_codes"
  add_foreign_key "center_transfer_record_journal_entries", "center_transfer_records"
  add_foreign_key "center_transfer_records", "branches"
  add_foreign_key "center_transfer_records", "centers"
  add_foreign_key "daily_report_insurance_account_statuses", "branches"
  add_foreign_key "deposit_time_transactions", "payment_collection_records"
  add_foreign_key "deposit_time_transactions", "payment_collections"
  add_foreign_key "deposit_time_transactions", "savings_accounts"
  add_foreign_key "fiscal_year_accounting_codes", "accounting_codes"
  add_foreign_key "loan_payment_adjustment_records", "loan_payment_adjustments"
  add_foreign_key "loan_payment_adjustment_records", "loan_payments"
  add_foreign_key "loan_payment_adjustments", "branches"
  add_foreign_key "member_attachment_files", "members"
  add_foreign_key "member_equity_transfer_requests", "equity_accounts"
  add_foreign_key "member_equity_transfer_requests", "member_transfer_requests"
  add_foreign_key "member_insurance_transfer_requests", "insurance_accounts"
  add_foreign_key "member_insurance_transfer_requests", "member_transfer_requests"
  add_foreign_key "member_loan_transfer_requests", "loans"
  add_foreign_key "member_loan_transfer_requests", "member_transfer_requests"
  add_foreign_key "member_savings_transfer_requests", "member_transfer_requests"
  add_foreign_key "member_savings_transfer_requests", "savings_accounts"
  add_foreign_key "member_transfer_requests", "members"
  add_foreign_key "member_withdrawal_fund_balances", "member_withdrawals"
  add_foreign_key "member_withdrawal_loans", "loans"
  add_foreign_key "member_withdrawal_loans", "member_withdrawals"
  add_foreign_key "member_withdrawals", "members"
  add_foreign_key "negative_loan_payments", "loan_payments"
  add_foreign_key "negative_loan_payments", "loans"
  add_foreign_key "patronage_refund_collection_records", "patronage_refund_collections"
  add_foreign_key "payment_collections", "accounting_funds"
  add_foreign_key "statement_of_account_fund_entries", "branches"
  add_foreign_key "statement_of_account_fund_entries", "centers"
  add_foreign_key "statement_of_account_fund_entries", "members"
  add_foreign_key "subsidiary_adjustment_entries", "accounting_codes"
  add_foreign_key "subsidiary_adjustment_entries", "subsidiary_adjustments"
  add_foreign_key "subsidiary_adjustment_records", "accounting_codes"
  add_foreign_key "subsidiary_adjustment_records", "members"
  add_foreign_key "subsidiary_adjustment_records", "subsidiary_adjustments"
  add_foreign_key "subsidiary_adjustments", "branches"
  add_foreign_key "time_deposit_withdrawals", "deposit_time_transactions"
  add_foreign_key "time_deposit_withdrawals", "savings_accounts", column: "savings_accounts_id"
  add_foreign_key "transfer_adjustment_records", "accounting_codes"
  add_foreign_key "transfer_adjustment_records", "transfer_adjustments"
  add_foreign_key "transfer_adjustments", "branches"
  add_foreign_key "wp_records", "loan_payments"
  add_foreign_key "wp_records", "members"
  add_foreign_key "year_end_closing_records", "vouchers"
end
