# DEFAULT SAVINGS TYPES
k_impok_accounting_code = AccountingCode.where(subcode: "0002", method_accounting_code: 16).first
golden_k_accounting_code = AccountingCode.where(subcode: "0003", method_accounting_code: 16).first

SavingsType.create!([
  {name: "K-IMPOK", code: "K-IMPOK", withdraw_accounting_code_id: k_impok_acocunting_code.id, deposit_accounting_code_id: k_impok_accounting_code.id, is_default: true},
  {name: "Golden K", code: "GK", withdraw_accounting_code_id: golden_k_accounting_code.id, deposit_accounting_code_id: golden_k_accounting_code.id, is_default: true}
])
