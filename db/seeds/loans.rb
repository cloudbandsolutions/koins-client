# DEFAULT LOAN DEDUCTIONS
puts "Seeding default loan deductions..."
LoanDeduction.create!([
  {name: "CLIP", code: "CLIP"},
  {name: "MBA Membership Fee", code: "MBA Membership Fee", is_one_time_fee: true}
])

puts "Seeding default loan product..."

accounting_code_receivables = AccountingCode.where(name: "Loans Receivables - K-NEGOSYO").first
accounting_code_processing_fee = AccountingCode.where(name: "Processing Fee").first
accounting_code_interest = AccountingCode.where(name: "Interest Revenue - K-NEGOSYO").first

savings_type = SavingsType.where(name: "K-IMPOK").first

loan_product = LoanProduct.create!(
  name: "K-NEGOSYO", code: "K-NEGO", interest_rate: 60, accounting_code_transaction: accounting_code_receivables, accounting_code_processing_fee: accounting_code_processing_fee, interest_accounting_code: accounting_code_interest, savings_type: savings_type, default_maintaining_balance_val: 20, default_processing_fee: 3
)

mba_membership_loan_deduction = LoanDeduction.create!(name: "MBA Membership Fee", code: "MBA Membership Fee")

LoanDeductionEntry.create!(loan_product: loan_product, loan_deduction: mba_membership_loan_deduction,  
puts "Done"
