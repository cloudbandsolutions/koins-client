# DEFAULT ADMINS
AdminUser.create(email: "admin@example.com", password: "password", password_confirmation: "password")

# DEFAULT USERS
User.create!(email: "raphael.alampay@gmail.com", password: "magisamdg", password_confirmation: "magisamdg", first_name: "Raphael", last_name: "Alampay", identification_number: "060081")
