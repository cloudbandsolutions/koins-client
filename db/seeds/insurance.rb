# DEFAULT INSURANCE TYPES
InsuranceType.create!([
  {name: "Life Insurance Fund", code: "LIF", withdraw_accounting_code_id: 27, withdraw_complimentary_accounting_code_id: 2, deposit_accounting_code_id: 27, deposit_complimentary_accounting_code_id: 2, is_default: true},
  {name: "Retirement Fund", code: "RF", withdraw_accounting_code_id: 28, withdraw_complimentary_accounting_code_id: 2, deposit_accounting_code_id: 28, deposit_complimentary_accounting_code_id: 2, is_default: true}
])
