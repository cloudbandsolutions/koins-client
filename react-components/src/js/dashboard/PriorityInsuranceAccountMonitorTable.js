import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";

export default class PriorityInsuranceAccountMonitorTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      source: "/api/v1/insurance/priority_insurance_account_monitor",
      members: []
    };

    this.fetchData();
  }

  componentWillMount() {
    this.setState({ isLoading: true });
  }

  numberWithCommas(x) {
    x = (Math.round(x * 100) / 100).toFixed(2);
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  fetchData() {
    $.ajax({
      url: this.state.source,
      method: 'GET',
      dataType: 'json',
      success: function(response) {
        console.log("PriorityInsuranceAccountMonitorTable");
        console.log(response);

        this.setState({
          isLoading:  false,
          members:  response.members
        });
      }.bind(this),
      error: function(response) {
      }.bind(this)
    });
  }

  handleAccountClick(accountId) {
  }

  handleMemberClick(memberId) {
  }

  renderEntries() {
    var priorityAccountType   = this.props.priorityAccountType;
    var secondaryAccountType  = this.props.secondaryAccountType;
    var numberWithCommas      = this.numberWithCommas;

    var entries           = this.state.members.map(function(el, i) {
      return  <tr key={i}>
                <td>
                  <a href={"/members/" + el.member.id}>{el.member.name}</a>
                </td>
                <td>{priorityAccountType}</td>
                <td class='curr'>{numberWithCommas(el.priority_account.balance)}</td>
                <td>{secondaryAccountType}</td>
                <td class='curr'>{numberWithCommas(el.secondary_account.balance)}</td>
              </tr>;
    });

    return entries;
  }

  render() {
    return (
      <div>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>Member</th>
              <th>Account Type</th>
              <th class='accounting-curr'>Balance</th>
              <th>Account Type</th>
              <th class='accounting-curr'>Balance</th>
            </tr>
          </thead>
          <tbody>
            {this.renderEntries()}
          </tbody>
        </table>
      </div>
    );
  }
}
