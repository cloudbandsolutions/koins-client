import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";
import PriorityInsuranceAccountMonitorTable from './PriorityInsuranceAccountMonitorTable';

ReactDOM.render(
  <div>
    <PriorityInsuranceAccountMonitorTable priorityAccountType="LIF" secondaryAccountType="RF" />
  </div>,
  document.getElementById('priority-insurance-account-monitor')
);
