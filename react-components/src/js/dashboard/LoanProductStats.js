import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";
import LoanProductStatsTable from './LoanProductStatsTable';

ReactDOM.render(
  <div>
    <LoanProductStatsTable />
  </div>,
  document.getElementById('loan-product-stats-app')
);
