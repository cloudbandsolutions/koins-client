import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";

export default class WatchlistTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      source: "/api/v1/watchlist",
      entries: [],
    };

    this.fetchData();
  }

  componentWillMount() {
    this.setState({ isLoading: true });
  }

  numberWithCommas(x) {
    x = (Math.round(x * 100) / 100).toFixed(2);
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  fetchData() {
    $.ajax({
      url: this.state.source,
      method: 'GET',
      dataType: 'json',
      success: function(response) {
        var data = response.data;

        if(typeof response.data == "string") {
          data = JSON.parse(response.data);
        }

        this.setState({
          entries:            data,
          isLoading:          false,
        });
      }.bind(this),
      error: function(response) {
      }.bind(this)
    });
  }

  renderEntries() {
    var numberWithCommas = this.numberWithCommas;
    var entries = this.state.entries.map(function(el, i) {
      var link = "/loans/" + el.loan_id;
      return  <tr key={i}>
                <td><a href={link} target="_blank">{el.name}</a></td>
                <td>{el.center}</td>
                <td>{el.loan_product}</td>
                <td class='curr'>{numberWithCommas(el.loan_amount)}</td>
                <td class='curr'>{numberWithCommas(el.amt_past_due)}</td>
                <td>{el.principal_rr}%</td>
              </tr>;
    });

    return entries;
  }

  render() {
    var numberWithCommas = this.numberWithCommas;

    return (
      <div>
        <div style={{display: this.state.isLoading ? 'block' : 'none'}}>
          <h4>
            Loading watchlist...
          </h4>
        </div>
        <div style={{ display: this.stateisLoading ? 'none' : 'block' }}>
          <table class="table table-bordered table-responsive table-condensed">
            <thead>
              <tr>
                <th>Name</th>
                <th>Center</th>
                <th>Loan Product</th>
                <th class='accounting-curr'>Amount</th>
                <th class='accounting-curr'>Amt Past Due</th>
                <th>RR</th>
              </tr>
            </thead>
            <tbody>
              {this.renderEntries()}
            </tbody>
          </table>
        </div>
        <hr/>
      </div>
    );
  }
}
