import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";
import MuiThemeProvider     from 'material-ui/styles/MuiThemeProvider';
import WatchlistTable from './WatchlistTable';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

ReactDOM.render(
  <MuiThemeProvider>
    <div>
      <WatchlistTable />
    </div>
  </MuiThemeProvider>,
  document.getElementById('watchlist-app')
);
