import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";

export default class LoanProductStatsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      source: "/api/v1/loan_product_stats",
      entries: [],
      total_active_loans: 0,
      total_num_members: 0,
      total_portfolio: 0,
      total_past_due_amt: 0,
      total_rr: 0,
    };

    this.fetchData();
  }

  componentWillMount() {
    this.setState({ isLoading: true });
  }

  numberWithCommas(x) {
    x = (Math.round(x * 100) / 100).toFixed(2);
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  fetchData() {
    $.ajax({
      url: this.state.source,
      method: 'GET',
      dataType: 'json',
      success: function(response) {
        console.log(response);

        var data = response.data;
        if(typeof response.data == "string") {
          data  = JSON.parse(response.data);
        }

        this.setState({
          isLoading:          false,
          entries:            data.entries,
          total_active_loans: data.total_num_loans,
          total_num_members:  data.total_num_members,
          total_portfolio:    data.total_portfolio,
          total_past_due_amt: data.total_amt_past_due,
          total_par_amount:   data.total_par_amount,
          total_par_rate:     data.total_par_rate,
          total_rr:           data.total_principal_rr,
        });
      }.bind(this),
      error: function(response) {
      }.bind(this)
    });
  }

  renderEntries() {
    var numberWithCommas = this.numberWithCommas;
    var entries = this.state.entries.map(function(el, i) {
      if(el.total_num_loans > 0) {
        return  <tr key={i}>
                  <td>{el.loan_product}</td>
                  <td style={{textAlign: "center"}}>{el.total_num_members}</td>
                  <td style={{textAlign: "center"}}>{el.total_num_loans}</td>
                  <td class='curr'>{numberWithCommas(el.total_portfolio)}</td>
                  <td class='curr'>{numberWithCommas(el.total_amt_past_due)}</td>
                  <td class='curr'>{numberWithCommas(el.total_par_amount)}</td>
                  <td>{el.total_par_rate}%</td>
                  <td>{el.total_principal_rr}%</td>
                </tr>;
      }
    });

    return entries;
  }

  render() {
    var numberWithCommas = this.numberWithCommas;

    return (
      <div>
        <div style={{display: this.state.isLoading ? 'block' : 'none'}}>
          <h4>
            Loading loan product statistics...
          </h4>
        </div>
        <div style={{ display: this.stateisLoading ? 'none' : 'block' }}>
          <table class="table table-bordered table-responsive table-condensed">
            <thead>
              <tr>
                <th>Name</th>
                <th style={{textAlign: "center"}}>Active Loaners</th>
                <th style={{textAlign: "center"}}>Active Loans</th>
                <th class='accounting-curr'>Portfolio</th>
                <th class='accounting-curr'>Past Due Amount</th>
                <th class='accounting-curr'>Par Amt</th>
                <th>Par Rate</th>
                <th>RR</th>
              </tr>
            </thead>
            <tbody>
              {this.renderEntries()}
              <tr>
                <th>Total</th>
                <th style={{textAlign: "center"}}>{this.state.total_num_members}</th>
                <th style={{textAlign: "center"}}>{this.state.total_active_loans}</th>
                <th class='curr'>{numberWithCommas(this.state.total_portfolio)}</th>
                <th class='curr'>{numberWithCommas(this.state.total_past_due_amt)}</th>
                <th class='curr'>{numberWithCommas(this.state.total_par_amount)}</th>
                <th>{this.state.total_par_rate}%</th>
                <th>{this.state.total_rr}%</th>
              </tr>
            </tbody>
          </table>
        </div>
        <hr/>
      </div>
    );
  }
}
