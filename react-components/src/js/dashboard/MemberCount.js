import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";
import MemberCountTable from './MemberCountTable';

ReactDOM.render(
  <div>
    <MemberCountTable />
  </div>,
  document.getElementById('member-count-app')
);
