import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";

export default class MemberCountTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      asOf: "",
      source: "/api/v1/member_counts",
      activeSaversMale: 0,
      activeSaversFemale: 0,
      activeSaversOthers: 0,
      activeSaversTotal: 0,
      activeLoanersMale: 0,
      activeLoanersFemale: 0,
      activeLoanersOthers: 0,
      activeLoanersTotal: 0,
      activeMembersMale: 0,
      activeMembersFemale: 0,
      activeMembersOthers: 0,
      activeMembersTotal: 0,
      pendingMembersMale: 0,
      pendingMembersFemale: 0,
      pendingMembersOthers: 0,
      pendingMembersTotal: 0,
      resignedMembersMale: 0,
      resignedMembersFemale: 0,
      resignedMembersOthers: 0,
      resignedMembersTotal: 0,
      invalidCoopMembershipMale: 0,
      invalidCoopMembershipFemale: 0,
      invalidCoopMembershipOthers: 0,
      invalidCoopMembershipTotal: 0
    };

    this.fetchData();
  }

  componentWillMount() {
    this.setState({ isLoading: true });
  }

  fetchData() {
    $.ajax({
      url: this.state.source,
      method: 'GET',
      dataType: 'json',
      success: function(response) {
        var data = response.data;
        var asOf =  response.as_of;

        this.setState({
          isLoading:            false,
          asOf:                 asOf,
          activeSaversMale:     data.pure_savers_male,
          activeSaversFemale:   data.pure_savers_female,
          activeSaversOthers:   data.pure_savers_others,
          activeSaversTotal:    data.pure_savers_total,
          activeLoanersMale:    data.active_loaners_male,
          activeLoanersFemale:  data.active_loaners_female,
          activeLoanersOthers:  data.active_loaners_others,
          activeLoanersTotal:   data.active_loaners_total,
          activeMembersMale:    data.active_members_male,
          activeMembersFemale:  data.active_members_female,
          activeMembersOthers:  data.active_members_others,
          activeMembersTotal:   data.active_members_total,
          pendingMembersMale:   data.pending_members_male,
          pendingMembersFemale: data.pending_members_female,
          pendingMembersOthers: data.pending_members_others,
          pendingMembersTotal:  data.pending_members_total,
          resignedMembersMale:   data.resigned_members_male,
          resignedMembersFemale: data.resigned_members_female,
          resignedMembersOthers: data.resigned_members_others,
          resignedMembersTotal:  data.resigned_members_total,
          invalidCoopMembershipMale:  data.invalid_coop_membership_male,
          invalidCoopMembershipFemale:  data.invalid_coop_membership_female,
          invalidCoopMembershipOthers:  data.invalid_coop_membership_others,
          invalidCoopMembershipTotal:  data.invalid_coop_membership_total
        });
      }.bind(this),
      error: function(response) {
      }.bind(this)
    });
  }

  handleClick(memberType) {
    console.log(memberType);
    if(memberType == "pure_savers") {
      window.location.href = "/pure_savers";
    } else if(memberType == "active_loaners") {
      window.location.href = "/active_loaners";
    } else if(memberType == "active") {
      window.location.href = "/active_members";
    } else if(memberType == "pending") {
      window.location.href = "/pending_members";
    } else if(memberType == "resigned") {
      window.location.href = "/resigned_members";
    } else if(memberType == "invalid_memberships") {
      window.location.href = "/invalid_memberships";
    }
  }

  handleRefreshClick() {
    console.log("Refreshing member count");
    this.setState({ isLoading: true });
    this.fetchData();
    console.log("Done refreshing member count");
  }

  render() {
    return (
      <div>
        <div style={{display: this.state.isLoading ? 'block' : 'none'}}>
          <h4>
            Loading member count...
          </h4>
        </div>
        <div style={{ display: this.stateisLoading ? 'none' : 'block' }}>
          <div class="row">
            <div class="col-md-6">
              <h4>As Of: {this.state.asOf}</h4>
            </div>
            <div class="col-md-6">
              <div class="pull-right">
                <button onClick={this.handleRefreshClick.bind(this)} class='btn btn-primary' id='btn-refresh-member-count'>
                  <span class='fa fa-refresh'></span>
                   Refresh Count
                </button>
              </div>
            </div>
          </div>
          <table class="table table-bordered table-responsive table-condensed member-count">
            <thead>
              <tr>
                <th></th>
                <th>Male</th>
                <th>Female</th>
                <th>Others</th>
                <th>TOTAL</th>
              </tr>
            </thead>
            <tbody>
              <tr class="clickable" onClick={this.handleClick.bind(this, 'pure_savers')}>
                <th>PURE SAVERS</th>
                <td>{this.state.activeSaversMale}</td>
                <td>{this.state.activeSaversFemale}</td>
                <td>{this.state.activeSaversOthers}</td>
                <td>{this.state.activeSaversTotal}</td>
              </tr>
              <tr class="clickable" onClick={this.handleClick.bind(this, 'active_loaners')}>
                <th>ACTIVE LOANERS</th>
                <td>{this.state.activeLoanersMale}</td>
                <td>{this.state.activeLoanersFemale}</td>
                <td>{this.state.activeLoanersOthers}</td>
                <td>{this.state.activeLoanersTotal}</td>
              </tr>
              <tr class="clickable" onClick={this.handleClick.bind(this, 'active')}>
                <th>ACTIVE MEMBERS</th>
                <td>{this.state.activeMembersMale}</td>
                <td>{this.state.activeMembersFemale}</td>
                <td>{this.state.activeMembersOthers}</td>
                <td>{this.state.activeMembersTotal}</td>
              </tr>
              <tr class="clickable" onClick={this.handleClick.bind(this, 'pending')}>
                <th>PENDING MEMBERS</th>
                <td>{this.state.pendingMembersMale}</td>
                <td>{this.state.pendingMembersFemale}</td>
                <td>{this.state.pendingMembersOthers}</td>
                <td>{this.state.pendingMembersTotal}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <hr/>
      </div>
    );
  }
}
