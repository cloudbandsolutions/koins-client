var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  devtool: debug ? "inline-sourcemap" : null,
  entry: {
    MemberCount: "./src/js/dashboard/MemberCount.js",
    LoanProductStats: "./src/js/dashboard/LoanProductStats.js",
    Watchlist: "./src/js/dashboard/Watchlist.js",
    PriorityInsuranceAccountMonitor: "./src/js/dashboard/PriorityInsuranceAccountMonitor"
  },
  //entry: "./src/js/app.js", 
  module: {
    loaders: [
      {
        test: /\.js?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
        query: {
          presets: ['react', 'es2015', 'stage-0'],
          plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy']
        }
      },
      {
        test: /\.css/,
        loaders: ['style', 'css'],
      }
    ]
  },
  output: {
    path: __dirname + "/../app/assets/javascripts/",
    filename: "[name].min.js", 
  },
  plugins: debug ? [] : [
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
  ],
};
