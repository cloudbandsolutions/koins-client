class ActionDispatch::Routing::Mapper
  def draw(routes_name)
    instance_eval(File.read(Rails.root.join("config/routes/#{routes_name}.rb")))
  end
end

Rails.application.routes.draw do
  devise_for :users
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  root to: "pages#index"

  require 'sidekiq/web'
  mount Sidekiq::Web => "/sidekiq"

  resources :savings_account_transactions, only: [:destroy]
  resources :insurance_account_transactions, only: [:destroy]

  resources :request_records, only: [:index, :show]

  # ADJUSTMENTS
  namespace :adjustments do
    resources :subsidiary_adjustments
    resources :transfer_adjustments
    resources :loan_payment_adjustments
    resources :batch_moratoria

    namespace :deposits do
      resources :payment_collections, except: [:new, :create]
    end

    namespace :withdrawals do
      resources :payment_collections, except: [:new, :create]
    end
    

    namespace :time_deposits do
      resources :operation_details do
        collection { get :list_of_time_deposit}
      end
    end


  end

  namespace :transfer do
    resources :center_transfer_records
  end

  ##################
  # MOUNTED ROUTES #
  ##################

  # Static pages
  draw :static
  draw :static_reports

  # API RELATED ROUTES
  draw :api
  draw :api_v2

  ###################

  resources :members do
    collection { post :import }
    collection { post :import_dependents }
    collection { post :import_beneficiaries }
    collection { post :import_correct_mii_recognition_date}
    collection { post :import_members_spouse_as_dependents}
    get "/resignation", to: "members#resignation"
    get "/transfer", to: "members#transfer"
    get "/resignation/edit", to: "members#edit_resignation"
    post "/resignation/:member_resignation_id/update", to: "members#update_resignation", as: :resignation_update
  end

  get "/download_transferred", to: "member_transfer_requests#download_transferred", as: :download_transferred

  # TRANSFER REQUESTS
  resources :member_transfer_requests, only: [:index, :edit, :show, :update]

  namespace :insurance  do
    resources :payment_collections do
      collection { post :upload }
    end
  end

  namespace :income_insurance  do
    resources :payment_collections do
      collection { post :upload }
    end
  end

  resources :feedbacks
  resources :surveys, only: [:index, :show] do
    resources :survey_respondents, except: [:index]
  end

  post "/new_loan_application", to: "loans#new_loan_application", as: :new_loan_application
  post "/new_claim_application", to: "claims#new_claim_application", as: :new_claim_application
  post "/new_clip_claim_application", to: "clip_claims#new_clip_claim_application", as: :new_clip_claim_application

  resources :branches
  resources :centers

  namespace :system_management do
    resources :users
    resources :centers
  end

  namespace :account do
    resources :users
  end

  namespace :accounting do
    get "/dashboard", to: "pages#dashboard"
    resources :loan_payments, only: [:index]
    resources :balance_sheets, only: [:index, :show, :destroy]
    resources :interest_on_share_capital_collections
    resources :patronage_refunds
    get "/patronage_refund/patronage_refund_excel", to: "patronage_refunds#patronage_refund_excel"
    get "/interest_on_share_capital_collection/interest_on_share_capital_excel", to: "interest_on_share_capital_collections#interest_on_share_capital_excel"
    get "/interest_on_share_capital_collection/interest_on_share_capital_pdf", to: "interest_on_share_capital_collections#interest_on_share_capital_pdf", as: :interest_on_share_capital_pdf

    get "/patronage_refund/patronage_refund_collection_pdf", to: "patronage_refunds#patronage_refund_collection_pdf", as: :patronage_refund_collection_pdf
    #get "/interest_on_share_capital_collections/index", to: "interest_on_share_capital_collections#index", as: :interest_on_share_capital_collections
    #get "/patronage_refunds", to: "patronage_refunds#patronage_refund_excel"

    get "/chart_of_accounts", to: "pages#chart_of_accounts", as: :chart_of_accounts
    get "/chart_of_accounts/download", to: "chart_of_accounts#download"
    get "/year_end_closing", to: "pages#year_end_closing"
    get "/income_statement", to: "pages#income_statement"
    get "/balance_sheet", to: "pages#balance_sheet"
    get "/reports/trial_balance", to: "reports#trial_balance", as: :reports_trial_balance
    get "/reports/financial_position", to: "reports#financial_position", as: :reports_financial_position
    get "/reports/statement_of_comprehensive_inc", to: "reports#statement_of_comprehensive_inc", as: :reports_statement_of_comprehensive_inc
  end

  get '/savings_accounts/monthly_interest_tax_generation', to: 'savings_accounts#manual_interest_tax_generation', as: :savings_accounts_manual_interest_tax_generation

  resources :claims do
    get "/claim_validation_pdf", to: "claims#claim_validation_pdf"
    get "/claim_loa_pdf", to: "claims#claim_loa_pdf"
  end

  resources :clip_claims do
    get "/clip_claim_validation_pdf", to: "clip_claims#clip_claim_validation_pdf"
    get "/clip_claim_loa_pdf", to: "clip_claims#clip_claim_loa_pdf"
  end

  resources :loans do
    get "/loan_ledger_pdf", to: "loans#loan_ledger_pdf"
    resources :loan_payments do
      get "approve_payment", to: "loan_payments#approve_payment", as: :approve
      get "reverse_payment", to: "loan_payments#reverse_payment", as: :reverse
    end
    resources :negative_loan_payments, except: [:index, :show]
    get "approve", to: "loans#approve", as: :approve
    get "reverse", to: "loans#reverse", as: :reverse
    get "writeoff", to: "loans#writeoff", as: :writeoff
    get "print_amortization", to: "loans#print_amortization", as: :print_amortization
  end

  resources :loan_insurances do
    collection { post :upload }
    get "approve", to: "loan_insurances#approve", as: :approve
    get "reverse", to: "loan_insurances#reverse", as: :reverse
  end

  resources :reinstatements do
    get "approve", to: "reinstatements#approve", as: :approve
    get "reverse", to: "reinstatements#reverse", as: :reverse
  end

  resources :insurance_account_validations do
    get "approve", to: "insurance_account_validations#approve", as: :approve
    get "reverse", to: "insurance_account_validations#reverse", as: :reverse

    get "/:insurance_account_validation_record_id/withdrawal_pdf", to: "insurance_account_validations#withdrawal_pdf", as: :withdrawal_pdf
    get "/pdf", to: "insurance_account_validations#pdf", as: :pdf
  end

  resources :deceased_and_total_and_permanent_disabilities do
    get "approve", to: "deceased_and_total_and_permanent_disabilities#approve", as: :approve
    get "reverse", to: "deceased_and_total_and_permanent_disabilities#reverse", as: :reverse
  end

  resources :payment_collections do
    get "/download_excel", to: "payment_collections#download_excel", as: :download_excel
    get "/download_wp_list", to: "payment_collections#download_wp_list", as: :download_wp_list
  end

  namespace :membership_payments do
    resources :payment_collections, only: [:index, :new, :show, :destroy, :edit, :update] do
      get "/pdf", to: "payment_collections#pdf"
      resources :payment_collection_records
    end
  end


  namespace :insurance do
    resources :payment_collections do
      get "/pdf", to: "payment_collections#pdf"
    end
  end

  # Member Accounts
  resources :savings_accounts
  resources :insurance_accounts do
    get "/claims_copy_pdf", to: "insurance_accounts#claims_copy_pdf"
    collection { post :import_insurance_accounts }
    collection { post :import_insurance_account_transactions }
    collection { post :import_insured_loans }
  end

  resources :equity_accounts

  # ACCOUNTING ROUTES
  resources :vouchers do
    get "approve", to: "vouchers#approve", as: :approve
    get "/pdf", to: "vouchers#pdf"
    get "/check_pdf", to: "vouchers#check_pdf"
  end

  resources :members do
    get "/change_beneficiaries_pdf", to: "members#change_beneficiaries_pdf"
    get "/blip_form_pdf", to: "members#blip_form_pdf"
    resources :loans, controller: 'members/loans', except: [:destroy] do
      get "/print_check", to: "members/loans#print_check"
      get "/print_voucher", to: "members/loans#print_voucher"
    end
    get "/transactions", to: "members#transactions", as: :transactions
    get "/toggle_active", to: "members#toggle_active", as: :toggle_active

    resources :member_shares, except: [:index] do
      get "/excel", to: "member_shares#excel"
      get "/pdf", to: "member_shares#pdf"
    end

    resources :member_certificates, except: [:index] do
      get "/excel", to: "member_certificates#excel"
      get "/pdf", to: "member_certificates#pdf"
      get "/certificates_for_printing", to: "member_certificates#certificates_for_printing"
    end

    resources :claims, controller: 'members/claims'
    resources :clip_claims, controller: 'members/clip_claims'
  end

  namespace :monitoring do
    resources :daily_reports, only: [:index, :show]
    resources :daily_report_insurance_account_statuses do
      get "/download_excel", to: "daily_report_insurance_account_statuses#download_excel"
    end
  end
  
  namespace :cash_management do
    namespace :time_deposits do
      resources :operation_details do
        collection { get :list_of_time_deposit}
      end
    end
    namespace :operations do
      resources :monthly_interest_and_tax_closing_records, only: [:index, :show, :destroy]
    end

    namespace :insurance_withdrawals do
      resources :payment_collections do
        get "/pdf", to: "payment_collections#pdf"
        collection { post :upload }
      end
    end

    namespace :equity_withdrawals do
      resources :payment_collections do
        get "/pdf", to: "payment_collections#pdf"
        collection { post :upload }
      end
    end

    namespace :deposits do
      resources :interest_and_tax_closing_records, only: [:index, :show]
      resources :payment_collections do
        get "/pdf", to: "payment_collections#pdf"
        collection { post :upload }
      end
    end

    namespace :withdrawals do
      resources :payment_collections do
        get "/pdf", to: "payment_collections#pdf"
      end
    end

    namespace :fund_transfer do
      namespace :insurance do
        resources :payment_collections do
          get "/pdf", to: "payment_collections#pdf"
          collection { post :upload }
        end
      end
    end
  end
end
