# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

set :output, "/tmp/cron_log.log"

 #Backup every 12:00 noon
 every :day, at: '12pm' do
  rake "db:backup"
  rake "dev:update_insured_loans"
  # rake "utils:update_member_insurance_status"
  rake "utils:set_is_reinstate_to_false"
  rake "utils:set_legal_dependents_is_deceased_and_is_tpd_to_false"
  rake "utils:generate_insurance_account_status_report"
end

 #Backup every 5:00 pm
 every :day, at: '5pm' do
   rake "db:backup"
 end

every :day, at: '6pm' do
  rake "dev:update_insured_loans"
  rake "utils:set_loan_insurance_record_status_to_inactive"
  rake "utils:update_member_insurance_status"
  rake "utils:generate_insurance_account_status_report"
  rake "dev:update_member_status_to_archived"
end

# Change status of loan insurance records to inactive
# Check if member is dormant
# every 1.hour do
#   rake "dev:update_mfi_date_from_memberships"
# end

every 3.hours do
  rake "dev:update_mii_date_from_memberships"
  rake "dev:update_mfi_date_from_memberships"
end
