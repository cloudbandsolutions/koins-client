namespace :legacy do
  get "/accounting_entries", to: "finance#accounting_entries"
end
