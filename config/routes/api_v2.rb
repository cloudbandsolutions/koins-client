namespace :api do
  namespace :v2 do
    namespace :sync do
      # USERS
      get "/users", to: "users#index"

      # MEMBERS
      get "/members", to: "members#index"

      # ACCOUNTING ENTRIES
      get "/accounting_entries", to: "accounting_entries#index"

      # CENTERS
      get "/centers", to: "centers#index"
    end
  end
end
