get "/get_flashes", to: "pages#get_flashes"
get "/developer", to: "pages#developer"
get "/export_tools", to: "pages#export_tools"
get "/import_tools", to: "pages#import_tools"
get "/import_beneficiary", to: "pages#import_beneficiary"
get "/import_dependents", to: "pages#import_dependents"
get "/import_insurance_accounts", to: "pages#import_insurance_accounts"
get "/import_insurance_account_transactions", to: "pages#import_insurance_account_transactions"
get "/import_correct_mii_recognition_date", to: "pages#import_correct_mii_recognition_date"
get "/import_members_spouse_as_dependents", to: "pages#import_members_spouse_as_dependents"
get "/upload_insurance_remittances", to: "pages#upload_insurance_remittances"
get "/upload_insurance_fund_transfer", to: "pages#upload_insurance_fund_transfer"
get "/upload_deposit", to: "pages#upload_deposit"
get "/upload_insurance_withdrawal", to: "pages#upload_insurance_withdrawal"
get "/upload_equity_withdrawal", to: "pages#upload_equity_withdrawal"
get "/upload_loan_insurance", to: "pages#upload_loan_insurance"
get "/missing_accounts", to: "pages#missing_accounts"
get "/year_end_records", to: "pages#year_end_records", as: :year_end_records
get "/year_end_records/:id", to: "pages#year_end_record", as: :year_end_record
get "/import_insured_loans", to: "pages#import_insured_loans"

get "/debug/invalid_loans", to: "pages#debug_invalid_loans"
get "/debug/invalid_loan_maintaining_balance", to: "pages#debug_invalid_loan_maintaining_balance"
get "/debug/recognition_dates", to: "debug#recognition_dates", as: :debug_recognition_dates
get "/debug/similar_member_names", to: "debug#similar_member_names", as: :debug_similar_member_names
get "/debug/missing_insurance_transactions", to: "debug#missing_insurance_transactions"
get "/debug/invalid_memberhips", to: "debug#invalid_memberships", as: :debug_invalid_memberships

# STATISTICS
get "/statistics/repayments", to: "statistics#repayments", as: :statistics_repayments

# EXPORTS
get "/exports/members", to: "exports#members", as: :export_members
get "/exports/beneficiaries", to: "exports#beneficiaries", as: :export_beneficiaries
get "/exports/beneficiaries_excel", to: "exports#beneficiaries_excel", as: :export_beneficiaries_excel
get "/exports/members_per_branch_excel", to: "exports#members_per_branch_excel", as: :export_members_per_branch_excel
get "/exports/legal_dependents", to: "exports#legal_dependents", as: :export_legal_dependents
get "/exports/insurance_accounts", to: "exports#insurance_accounts", as: :export_insurance_accounts
get "/exports/insurance_account_transactions", to: "exports#insurance_account_transactions", as: :export_insurance_account_transactions
get "/exports/withdrawal_for_hiip", to: "exports#withdrawal_for_hiip", as: :export_withdawal_for_hiip
get "/exports/dependents_report", to: "exports#dependents_report", as: :export_dependents_report
get "/exports/members_spouse_export_csv", to: "exports#members_spouse_export_csv", as: :export_members_spouse_export_csv
get "/exports/insured_loans", to: "exports#insured_loans", as: :export_insured_loans

# USER ACTIVITIES
resources :user_activities, only: [:index]

# BACKGROUND OPERATIONS
resources :background_operations, only: [:index]

# ANNOUNCEMENTS
resources :announcements

# IMPORTS
#get "/imports/members", to: "imports#members", as: :import_members

# print check voucher
get '/loans/:loan_id/print_check_voucher', to: 'loans#print_check_voucher', as: :print_check_voucher
get '/loans/:loan_id/print_check', to: 'loans#print_check', as: :print_check
get '/loans/:loan_id/loan_ledger', to: 'loans#loan_ledger', as: :loan_ledger

# Books
get "/books/print/:book_type", to: "books#print", as: :print_book
get "/books/:book_type", to: "books#show", as: :book 
get "/books/:book_type/excel", to: "books#excel", as: :book_excel
get "/books/:book_type/csv", to: "books#csv", as: :book_csv
get "/books/:book_type/pdf", to: "books#book_pdf", as: :book_pdf

# Trial Balance
get "/trial_balance", to: "trial_balance#index", as: :trial_balance
get "/trial_balance/entries", to: "trial_balance#entries", as: :trial_balance_entries
get "/trial_balance/generate_download_pdf_url", to: "trial_balance#generate_download_pdf_url"
get "/trial_balance/download_pdf", to: "trial_balance#download_pdf", as: :trial_balance_download_pdf
get "/trial_balance/download_excel", to: "trial_balance#download_excel", as: :trial_balance_download_excel
get "/trial_balance/generate_download_excel_url", to: "trial_balance#generate_download_excel_url"

# General Ledger
# get "/general_ledger", to: "general_ledger#index", as: :general_ledger
get "/general_ledger/entries", to: "general_ledger#entries", as: :general_ledger_entries
get "/general_ledger/generate_download_pdf_url", to: "general_ledger#generate_download_pdf_url"
get "/general_ledger/download_pdf", to: "general_ledger#download_pdf", as: :general_ledger_download_pdf
get "/general_ledger/download_excel", to: "general_ledger#download_excel", as: :general_ledger_download_excel
get "/general_ledger/generate_download_excel_url", to: "general_ledger#generate_download_excel_url"

# Voucher Summary
get "/voucher_summary", to: "voucher_summary#index", as: :voucher_summary
get "/voucher_summary/entries", to: "voucher_summary#entries", as: :voucher_summary_entries
get "/voucher_summary/generate_download_pdf_url", to: "voucher_summary#generate_download_pdf_url"
get "/voucher_summary/download_pdf", to: "voucher_summary#download_pdf", as: :voucher_summary_download_pdf
get "/voucher_summary/download_excel", to: "voucher_summary#download_excel", as: :voucher_summary_download_excel
get "/voucher_summary/generate_download_excel_url", to: "voucher_summary#generate_download_excel_url"

# Reports
get "/reports", to: "reports#index", as: :reports_dashboard

get "/reports/transferred_members", to: "reports#transferred_members", as: :transferred_members

resources :ammortization_schedule_entries do
  resources :loan_moratoria
end

get "/internal/centers", to: "internal#centers"
get "/internal/project_types", to: "internal#project_types"
get "/internal/default_processing_fee", to: "internal#default_processing_fee"

# GET NEXT CLOSING TIME
get "/next_closing_time", to: "pages#next_closing_time"

resources :build_versions do 
  resources :build_categories, module: 'build_versions'
  resources :build_contents, module: 'build_versions'
end

# STATIC PAGES
get "/actions", to: "pages#actions"
get "/active_loaners", to: "pages#active_loaners"
get "/active_members", to: "pages#active_members"
get "/double_fund_transfer", to: "pages#double_fund_transfer"
get "/download_backup", to: "pages#download_backup"
get "/pending_members", to: "pages#pending_members"
get "/pure_savers", to: "pages#pure_savers"
get "/resigned_members", to: "pages#resigned_members"
get "/invalid_memberships", to: "pages#invalid_memberships"
get "/members_for_reinsurance", to: "pages#members_for_reinsurance", as: :members_for_reinsurance
get "/insurance_exit_age_members", to: "pages#insurance_exit_age_members", as: :insurance_exit_age_members
get "/insurance_exit_age_members_pdf", to: "pages#insurance_exit_age_members_pdf", as: :insurance_exit_age_members_pdf
get "/invalid_members", to: "pages#invalid_members", as: :invalid_members
get "/inforce_members", to: "pages#inforce_members"
get "/lapsed_members", to: "pages#lapsed_members"
get "/pages/transferred_icpr_member_data", to: "pages#transferred_icpr_member_data", as: :transferred_icpr_member_data
get "/pages/transferred_interest_on_sharecapital_excel", to: "pages#transferred_interest_on_sharecapital_excel", as: :transferred_interest_on_sharecapital_excel



get "/transferred_icpr_member", to: "pages#transferred_icpr_member", as: :transferred_icpr_member
get "/transferred_patronage_refund", to: "pages#transferred_patronage_refund", as: :transferred_patronage_refund
get "/pages/transferred_patronage_refund_data", to: "pages#transferred_patronage_refund_data", as: :transferred_patronage_refund_data


get "/missing_insurance_transactions", to: "reports#missing_insurance_transactions", as: :missing_insurance_transactions


get "/vouchers/print/:id", to: "vouchers#print", as: :print_voucher
get "/member_shares", to: "member_shares#index"
get '/member_shares/shares_for_printing', to: 'member_shares#shares_for_printing', as: :shares_for_printing

get "/member_certificates", to: "member_certificates#index"
get '/member_certificates/certificates_for_printing', to: 'member_certificates#certificates_for_printing', as: :certificates_for_printing
