# REPORTS: Pages
get '/reports/migs', to: 'reports#migs', as: :migs
get '/reports/migs_pdf', to: 'reports#migs_pdf', as: :migs_pdf
get '/reports/drop_out_rate_by_month', to: 'reports#drop_out_rate_by_month', as: :reports_drop_out_rate_by_month
get '/reports/x_weeks_to_pay', to: 'reports#x_weeks_to_pay', as: :x_weeks_to_pay
get '/reports/insured_loans', to: 'reports#insured_loans', as: :insured_loans
get '/reports/withdrawals', to: 'reports#withdrawals', as: :withdrawals
get "/reports/print_insured_loans", to: "reports#print_insured_loans", as: :reports_print_insured_loans
get "/reports/download_csv_insured_loans", to: "reports#download_csv_insured_loans", as: :reports_download_csv_insured_loans
get '/reports/soa_funds', to: 'reports#soa_funds'
get '/reports/loan_insurance_list', to: 'reports#loan_insurance_list'
get '/reports/or', to: 'reports#or', as: :reports_or
get "/reports/active_loaners", to: "reports#active_loaners", as: :reports_active_loaners
get "/reports/checks", to: "reports#checks", as: :reports_checks
get "/reports/print_checks", to: "reports#print_checks", as: :reports_print_checks
get "/reports/portfolio", to: "reports#portfolio", as: :reports_portfolio
get "/reports/personal_funds", to: "reports#personal_funds", as: :reports_personal_funds
get "/reports/repayments", to: "reports#repayments", as: :reports_repayments
get "/reports/par", to: "reports#par", as: :reports_par
get "/reports/aging_of_receivables", to: "reports#aging_of_receivables", as: :reports_aging_of_receivables
get "/reports/cluster_snapshots", to: "reports#cluster_snapshots", as: :reports_cluster_snapshots
get "/reports/loans_statement_of_accounts", to: "reports#loans_statement_of_accounts", as: :reports_loans_statement_of_accounts
get "/reports/expenses_statement_of_assets", to: "reports#expenses_statement_of_assets", as: :reports_expenses_statement_of_assets
get "/reports/funds_statement_of_accounts", to: "reports#funds_statement_of_accounts", as: :reports_funds_statement_of_accounts
get "/reports/balance_sheet", to: "reports#balance_sheet", as: :reports_balance_sheet
get "/reports/income_statement", to: "reports#income_statement", as: :reports_income_statement 
get "/reports/summary_of_loan_release", to: "reports#summary_of_loan_release", as: :reports_summary_of_loan_release
get "/reports/loan_master_list", to: "reports#loan_master_list", as: :reports_loan_master_list
get "/reports/master_list_of_client_loans", to: "reports#master_list_of_client_loans", as: :reports_master_list_of_client_loans
get "/reports/monthly_rr", to: "reports#monthly_rr", as: :reports_monthly_rr
get "/reports/download_member_registry_excel", to: "reports#download_member_registry_excel", as: :reports_download_member_registry_excel
get "/reports/seriatim_report", to: "reports#seriatim_report", as: :reports_seriatim_report
get "/reports/seriatim", to: "reports#seriatim", as: :reports_seriatim
get "/reports/collections_clip_report", to: "reports#collections_clip_report", as: :reports_collections_clip_report
get "/reports/collections_clip", to: "reports#collections_clip", as: :reports_collections_clip
get "/reports/collections_blip_report", to: "reports#collections_blip_report", as: :reports_collections_blip_report
get "/reports/collections_blip", to: "reports#collections_blip", as: :reports_collections_blip
get "/reports/validations_report", to: "reports#validations_report", as: :reports_validations_report
get "/reports/validations", to: "reports#validations", as: :reports_validations
get "/reports/member_dependent_report", to: "reports#member_dependent_report", as: :reports_member_dependent_report
get "/reports/member_dependent", to: "reports#member_dependent", as: :reports_member_dependent
get "/reports/personal_documents_report", to: "reports#personal_documents_report", as: :reports_personal_documents_report
get "/reports/personal_documents", to: "reports#personal_documents", as: :reports_personal_documents
get "/reports/cic_report", to: "reports#cic_report", as: :reports_cic_report
get "/reports/cic", to: "reports#cic", as: :reports_cic
get "/reports/member_reports", to: "reports#member_reports", as: :member_reports
get "/reports/collection_reports", to: "reports#collection_reports", as: :collection_reports
get 'reports/summary_of_certificates_and_policies', to: 'reports#summary_of_certificates_and_policies', as: :summary_of_certificates_and_policies
get '/reports/resigned_from_microinsurance', to: 'reports#resigned_from_microinsurance'
get '/reports/mfi_new_and_resigned_member_counts', to: 'reports#mfi_new_and_resigned_member_counts', as: :reports_mfi_new_and_resigned_member_counts
get '/reports/mfi_new_and_resigned_detailed', to: 'reports#mfi_new_and_resigned_detailed', as: :reports_mfi_new_and_resigned_detailed
get '/reports/ppi', to: 'reports#ppi', as: :ppi
get '/reports/list_of_resign', to: 'reports#list_of_resign', as: :list_of_resign
get '/reports/list_of_resign_details', to: 'reports#list_of_resign_details', as: :list_of_resign_details
get '/pages/list_of_lp_cycle_details', to: 'pages#list_of_lp_cycle_details', as: :list_of_lp_cycle_details
get '/pages/list_so_member_stats_details', to: 'pages#list_so_member_stats_details', as: :list_so_member_stats_details
get '/loans/loan_payment_breakdowns', to: 'loans#loan_payment_breakdowns', as: :loan_payment_breakdowns
get '/accounting/pages/program_status_reports', to: 'accounting/pages#program_status_reports', as: :program_status_reports
get '/accounting/pages/program_status_data', to: 'accounting/pages#program_status_data', as: :program_status_data
get "/reports/member_quarterly_reports", to: "reports#member_quarterly_reports", as: :member_quarterly_reports
get "/reports/list_of_member_for_cic", to: "reports#list_of_member_for_cic", as: :list_of_member_for_cic
get "/reports/list_of_member_for_cic_excel", to: "reports#list_of_member_for_cic_excel", as: :list_of_member_for_cic_excel
get "/reports/claims_report", to: "reports#claims_report", as: :reports_claims_report
get "/reports/claims", to: "reports#claims", as: :reports_claims
get "/reports/clip_claims_report", to: "reports#clip_claims_report", as: :reports_clip_claims_report
get "/reports/clip_claims", to: "reports#clip_claims", as: :reports_clip_claims

get "/accounting/pages/interest_on_share_capital", to: "accounting/pages#interest_on_share_capital", as: :interest_on_share_capital

get '/reports/x_weeks_to_pay_pdf', to: 'reports#x_weeks_to_pay_pdf', as: :x_weeks_to_pay_pdf

get "/accounting/pages/interest_on_share_capital_data", to: "accounting/pages#interest_on_share_capital_data", as: :interest_on_share_capital_data
get "/cash_management/withdrawals/payment_collections/print_pdf", to: "cash_management/withdrawals/payment_collections#print_pdf", as: :print_pdf
get "/voucher_summary/voucher_summary_pdf", to: "voucher_summary#voucher_summary_pdf", as: :voucher_summary_pdf
get "/member_shares/shares_printed_pdf", to: "member_shares#shares_printed_pdf", as: :shares_printed_pdf
get "/member_certificates/certificates_printed_pdf", to: "member_certificates#certificates_printed_pdf", as: :certificates_printed_pdf
get '/reports/monthly_incentives', to: 'reports#monthly_incentives', as: :monthly_incentives
#je
get '/reports/additional_shares', to: 'reports#additional_shares', as: :additional_shares
get '/reports/additional_shares_excel', to: 'reports#additional_shares_excel', as: :additional_shares_excel
#
get '/reports/personal_funds_pdf', to: 'reports#personal_funds_pdf', as: :personal_funds_pdf
get '/reports/par_pdf', to: 'reports#par_pdf', as: :par_pdf
get '/reports/master_list_of_client_loans_pdf', to: 'reports#master_list_of_client_loans_pdf', as: :master_list_of_client_loans_pdf
get '/reports/repayments_pdf', to: 'reports#repayments_pdf', as: :repayments_pdf
get '/reports/summary_of_loan_release_pdf', to: 'reports#summary_of_loan_release_pdf', as: :summary_of_loan_release_pdf
#get "/accounting/patronage_refunds/patroange_refund_excel", to: "/accounting/patronage_refunds#patroange_refund_excel", as: :patroange_refund_excel


#get '/accounting/pages/program_status_data', to: 'accounting/pages#program_status_data', as: :program_status_data
#get '/accounting/interest_on_share_capital_collections/interest_on_share_capital_pdf', to: 'interest_on_share_capital_collections#interest_on_share_capital_pdf', as: :interest_on_share_capital_pdf


get '/reports/project_type_category', to: 'reports#project_type_category', as: :project_type_category
get "/reports/project_type_category_excel", to: "reports#project_type_category_excel", as: :project_type_category_excel


# PRINT
get "/print/accounting_entry/:id", to: "print#accounting_entry", as: :print_accounting_entry
get "/print/loan_accounting_entry/:loan_id", to: "print#loan_accounting_entry", as: :print_loan_accounting_entry
get "/print/pdf_loan_accounting_entry/:loan_id", to: "print#pdf_loan_accounting_entry", as: :print_pdf_loan_accounting_entry
get "/print/cash_management_deposit/:id", to: "print#cash_management_deposit", as: :print_cash_management_deposit
get "/print/cash_management_withdrawal/:id", to: "print#cash_management_withdrawal", as: :print_cash_management_withdrawal
get "/print/cash_management_fund_transfer/:id", to: "print#cash_management_fund_transfer", as: :print_cash_management_fund_transfer
get "/print/cash_management_membership_payment/:id", to: "print#cash_management_membership_payment", as: :print_cash_management_membership_payment
get "/print/survey/:id", to: "print#survey", as: :print_survey
get "/print/loan_master_list", to: "print#loan_master_list", as: :print_loan_master_list
get "/print/summary_of_loan_release", to: "print#summary_of_loan_release", as: :print_summary_of_loan_release
get "/print/chart_of_accounts", to: "print#chart_of_accounts", as: :print_chart_of_accounts
get "/print/ppi", to: "reports#ppi_pdf", as: :ppi_pdf
get "/accounting/reports/trial_balance_pdf", to: "accounting/reports#trial_balance_pdf", as: :trial_balance_pdf

get "/general_ledger", to: "general_ledger#index", as: :general_ledger
get "/general_ledger/general_ledger_pdf", to: "general_ledger#general_ledger_pdf", as: :general_ledger_pdf
get "/print/income_statements_print", to: "income_statements#income_statements_print", as: :income_statements_print
#get "/accounting/pages/chart_of_accounts_pdf", to: "accounting/pages#chart_of_accounts_pdf", as: :chart_of_accounts_pdf
get "/accounting/pages/chart_of_accounts_pdf", to: "accounting/pages#chart_of_accounts_pdf", as: :chart_of_accounts_pdf
get "/cash_management/operations/monthly_interest_and_tax_closing_records/monthly_interest_pdf", to: "reports#monthly_interest_pdf", as: :monthly_interest_pdf
get "/accounting/pages/income_statement_pdf", to: "accounting/pages#income_statement_pdf", as: :income_statement_pdf
get "/general_ledger/general_pdf", to: "general_ledger#general_pdf", as: :general_pdf
get '/reports/monthly_incentives_pdf', to: 'reports#monthly_incentives_pdf', as: :monthly_incentives_pdf
get '/savings_accounts/:id/savings_account_pdf', to: 'savings_accounts#savings_account_pdf', as: :savings_account_pdf

get '/payment_collections/:id/payment_collection_pdf', to: 'payment_collections#payment_collection_pdf', as: :payment_collection_pdf

get '/insurance_accounts/:id/insurance_account_pdf', to: 'insurance_accounts#insurance_account_pdf', as: :insurance_account_pdf
get '/reports/list_of_resign_pdf', to: 'reports#list_of_resign_pdf', as: :list_of_resign_pdf
get '/reports/list_of_resign_details_pdf', to: 'reports#list_of_resign_details_pdf', as: :list_of_resign_details_pdf
get '/equity_accounts/:id/equity_accounts_pdf', to: 'equity_accounts#equity_accounts_pdf', as: :equity_accounts_pdf
