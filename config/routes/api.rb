namespace :api do
  namespace :v1 do
    # VALUES
    get "/religions", to: "values#religions"

    #HEAD OFFICE
    post "/head_office/send_daily_report", to: "head_office#send_daily_report"

    post "/savings_accounts/rehash", to: "savings_accounts#rehash"
    post "/insurance_accounts/rehash", to: "insurance_accounts#rehash"
    post "/member_shares/flag_for_printing", to: "member_shares#flag_for_printing"
    post "/member_certificates/flag_for_printing", to: "member_certificates#flag_for_printing"

    get "/reports/transferred_members", to: "reports#transferred_members"
    post "/reports/generate_mfi_monthly_member_counts", to: "reports#generate_mfi_monthly_member_counts"

    namespace :transfer do
      post 'center_transfer_records/approve', to: 'center_transfer_records#approve'
    end

    namespace :accounting do
      get "/reports/trial_balance", to: "reports#trial_balance"
      get "/reports/monthly_incentives", to: "reports#monthly_incentives"
      post "/reports/monthly_incentives_pdf", to: "reports#monthly_incentives_pdf"
     
 
      post "/interest_on_share_capital_collections/generate", to: "interest_on_share_capital_collections#generate"

      get "/interest_on_share_capital_collections/approved", to: "interest_on_share_capital_collections#approved"
      get "/interest_on_share_capital_collections/equity_rate", to: "interest_on_share_capital_collections#equity_rate"

      post "/interest_on_share_capital_collections/regenerate", to: "interest_on_share_capital_collections#regenerate"
      get "/reports/financial_position", to: "reports#financial_position"
      get "/reports/statement_of_comprehensive_inc", to: "reports#statement_of_comprehensive_inc"
      post "/patronage_refunds/generate", to: "patronage_refunds#generate"
      get "/patronage_refunds/approve", to: "patronage_refunds#approve"

      get "/interest_on_share_capital_collections/approved_cleared", to: "interest_on_share_capital_collections#approved_cleared"
    end

    # Check portfolio of individual loan
    get "/loans/portfolio", to: "loans#portfolio"

    post "/loans/reamortize", to: "loans#reamortize"
    post "/loans/make_individual_payment", to: "loans#make_individual_payment"
    get "/accounting/income_statement/generate", to: "accounting/income_statement#generate_income_statement"
    post "/accounting/balance_sheet/generate", to: "accounting/balance_sheet#generate_balance_sheet"
    get "/accounting/closing/year_end", to: "accounting/closing#generate_year_end_closing"

    get "/accounting/closing/year_end_insurance", to: "accounting/closing#generate_year_end_closing_insurance"
    
    post "/accounting/closing/approve_year_end", to: "accounting/closing#approve_year_end"
    post "/accounting/closing/approve_year_end_insurance", to: "accounting/closing#approve_year_end_insurance"
    get "/get_members",         to: "members#get_members"
    get "/member_counts",       to: "dashboard#member_counts"
    get "/loan_product_stats",  to: "dashboard#loan_product_stats"
    get "/watchlist",           to: "dashboard#watchlist"
    get "/active_loans_count",  to: "dashboard#active_loans_count"
    post "/generate_daily_report", to: "dashboard#generate_daily_report"
    post "/generate_monthly_daily_report", to: "dashboard#generate_monthly_daily_report"
    post "/send_daily_report", to: "dashboard#send_daily_report"

    namespace :adjustments do
      post 'subsidiary_adjustments/approve', to: 'subsidiary_adjustments#approve'
      post 'subsidiary_adjustments/create', to: 'subsidiary_adjustments#create'
      post 'subsidiary_adjustment_records/create', to: 'subsidiary_adjustment_records#create'
      post 'subsidiary_adjustment_records/delete', to: 'subsidiary_adjustment_records#delete'
      post 'transfer_adjustments/approve', to: 'transfer_adjustments#approve'
      post 'batch_moratoria/approve', to: 'batch_moratoria#approve'
      post 'time_deposit/withdraw', to: 'time_deposit#withdraw'
      post 'time_deposit/approved', to: 'time_deposit#approved'
    end

    namespace :members do
      post 'state/update_editable', to: 'state#update_editable'
      post "resignation/resign", to: "resignation#resign"
      post 'resignation/flag_for_resignation', to: 'resignation#flag_for_resignation'
      post 'resignation/revert_resignation', to: 'resignation#revert_resignation'
      post 'resignation/approve', to: 'resignation#approve'
      post 'request_transfer', to: 'member_transfer_requests#request_transfer'
      post 'cancel_request_transfer', to: 'member_transfer_requests#cancel_request_transfer'
      post 'approve_request_transfer', to: 'member_transfer_requests#approve_request_transfer'
      post 'generate_transfer_records', to: 'member_transfer_requests#generate_transfer_records'
      post 'cancel_transfer_records', to: 'member_transfer_requests#cancel_transfer_records'
      post 'approve_pending_records', to: 'member_transfer_requests#approve_pending_records'
      post 'clean_transfer_records', to: 'member_transfer_requests#clean_transfer_records'
      post "resignation/reinstate_member", to: "resignation#reinstate_member"
    end

    post 'payment_collections/generate_new_billing', to: 'payment_collections#generate_new_billing'

    namespace :membership_payments do
      post 'payment_collections/generate_transaction', to: 'payment_collections#generate_transaction'
      post 'payment_collections/remove_payment_collection_record', to: 'payment_collections#remove_payment_collection_record'
    end

    namespace :insurance do
      post 'payment_collections/generate_transaction', to: 'payment_collections#generate_transaction'
      post 'generate_daily_report_insurance_account_status', to: 'daily_reports#generate_daily_report_insurance_account_status'
      get 'priority_insurance_account_monitor', to: 'monitor#priority_insurance_accounts'
      post 'members/resign', to: 'members#resign'
      post 'members/balik_kasapi_mii', to: 'members#balik_kasapi_mii'
      post 'members/change_recognition_date', to: 'members#change_recognition_date'
    end

    namespace :cash_management do
      namespace :time_deposit do
        post 'payment_collections/generate_transaction', to: 'payment_collections#generate_transaction'
        post 'payment_collections/approved_time_deposit_transaction', to: 'payment_collections#approved_time_deposit_transaction'
        post 'payment_collections/save_member', to: 'payment_collections#save_member'
        post 'payment_collections/save_payment_collection', to: 'payment_collections#save_payment_collection'
      end


      namespace :closing do
        post "/monthly_interest_and_tax_closing_records/create", to: "monthly_interest_and_tax_closing_records#create"
        post "/monthly_interest_and_tax_closing_records/destroy", to: "monthly_interest_and_tax_closing_records#destroy"
        post "/monthly_interest_and_tax_closing_records/approve", to: "monthly_interest_and_tax_closing_records#approve"
        post "/monthly_interest_and_tax_closing_records/void", to: "monthly_interest_and_tax_closing_records#void"
      end

      namespace :deposits do
        post 'payment_collections/generate_transaction', to: 'payment_collections#generate_transaction'
      end

      namespace :withdrawals do
        post 'payment_collections/generate_transaction', to: 'payment_collections#generate_transaction'
      end

      namespace :insurance_withdrawals do
        post 'payment_collections/generate_transaction', to: 'payment_collections#generate_transaction'
      end

      namespace :equity_withdrawals do
        post 'payment_collections/generate_transaction', to: 'payment_collections#generate_transaction'
      end

      namespace :fund_transfer do
        namespace :insurance do
          post 'payment_collections/generate_transaction', to: 'payment_collections#generate_transaction'
        end
      end
    end

    # Batch Moratorium
    post "moratorium/init", to: "moratorium#init"
    post "moratorium/approve", to: "moratorium#approve"

    get 'members/get_accounts', to: 'members#get_accounts'

    # Reports
    get 'reports/soa_funds', to: 'reports#soa_funds'
    get 'reports/active_loaners_by_branch', to: 'reports#active_loaners_by_branch'
    get 'reports/loan_product_portfolio_by_branch', to: 'reports#loan_product_portfolio_by_branch'
    get 'reports/personal_funds', to: 'reports#personal_funds'
    get 'reports/member_reports', to: 'reports#member_reports'
    get 'reports/collection_reports', to: 'reports#collection_reports'
    get 'reports/summary_of_certificates_and_policies', to: 'reports#summary_of_certificates_and_policies'      
    get 'reports/seriatim_report', to: 'reports#seriatim_report'
    get 'reports/collections_clip_report', to: 'reports#collections_clip_report'
    get 'reports/collections_blip_report', to: 'reports#collections_blip_report'
    get 'reports/repayments', to: 'reports#repayments'
    get 'reports/par', to: 'reports#par'
    get 'reports/aging_of_receivables', to: 'reports#aging_of_receivables'
    get 'reports/loans_statement_of_accounts', to: 'reports#loans_statement_of_accounts'
    get 'reports/expenses_statement_of_assets', to: 'reports#expenses_statement_of_assets'
    get 'reports/funds_statement_of_accounts', to: 'reports#funds_statement_of_accounts'
    get 'reports/balance_sheet', to: 'reports#balance_sheet'
    get 'reports/income_statement', to: 'reports#income_statement'
    get 'reports/master_list_of_client_loans', to: 'reports#master_list_of_client_loans'
    get 'reports/monthly_rr', to: 'reports#monthly_rr'
    get 'reports/member_registry', to: 'reports#member_registry'
    get 'reports/x_weeks_to_pay', to: 'reports#x_weeks_to_pay'
    get 'reports/member_quarterly_reports', to: 'reports#member_quarterly_reports'
    get 'reports/personal_funds_pdf', to: 'reports#personal_funds_pdf'
    post 'members/create_missing_accounts', to: 'members#create_missing_accounts'

    # Loan Payments
    post '/loan_payments/add_wp_record', to: 'loan_payments#add_wp_record'
    get '/loan_payments/wp_records', to: 'loan_payments#wp_records'

    # Savings Account Transactions
    post '/savings_account_transactions/add_wp_record', to: 'savings_account_transactions#add_wp_record'
    get '/savings_account_transactions/wp_records', to: 'savings_account_transactions#wp_records'

    # Approve Voucher
    post '/vouchers/approve', to: 'vouchers#approve'

    # Reject Voucher
    post '/vouchers/reject', to: 'vouchers#reject'

    # Approve Loan
    post '/loans/approve', to: 'loans#approve'

    # Writeoff Loan
    post '/loans/writeoff', to: 'loans#writeoff'

    # Reverse loan
    post '/loans/reverse', to: 'loans#reverse'

    # Search loans by batch transaction reference number
    post '/loans/by_batch_transaction_reference_number'

    get 'branches/so_list', to: 'branches#so_list'
    get 'branches/centers_by_so', to: 'branches#centers_by_so'
    get 'branches/centers_by_branch', to: 'branches#centers_by_branch'

    get 'utils/branches', to: 'utils#branches'
    get 'utils/centers', to: 'utils#centers'
    get 'utils/loan_insurance_types', to: 'utils#loan_insurance_types'
    get 'utils/loan_products', to: 'utils#loan_products'
    get 'utils/loan_product_types', to: 'utils#loan_product_types'

    get "utils/member_resignation_type_particulars", to: 'utils#member_resignation_type_particulars'
    get "utils/member_resignation_particular_by_code", to: 'utils#member_resignation_particular_by_code'

    get 'project_types', to: 'project_types#by_project_type_category_id'
    get 'loans/loan_application_voucher_particular', to: 'loans#loan_application_voucher_particular'

    get 'transactions/show', to: 'transactions#show'
    get 'feedbacks/create', to: 'feedbacks#create'
    post 'centers/create', to: 'centers#create'
    post 'members/create', to: 'members#create'
    post 'members/update', to: 'members#update'
    post 'beneficiaries/create', to: 'beneficiaries#create'
    post 'beneficiaries/update', to: 'beneficiaries#update'
    post 'legal_dependents/create', to: 'legal_dependents#create'
    post 'legal_dependents/update', to: 'legal_dependents#update'

    # Sync
    get 'sync_accounting/sync_all', to: 'sync_accounting#sync_all'
    get 'sync_banks/sync_all', to: 'sync_banks#sync_all'
    get 'sync_project_types/sync_all', to: 'sync_project_types#sync_all'
    get 'sync_financial_types/sync_all', to: 'sync_financial_types#sync_all'
    get 'sync_religions/sync_all', to: 'sync_religions#sync_all'
    get 'sync_areas/sync_all', to: 'sync_areas#sync_all'
    get 'sync_clusters/sync_all', to: 'sync_clusters#sync_all'
    get 'sync_clusters/:id/single', to: 'sync_clusters#single'
    get 'sync_branches/sync_all', to: 'sync_branches#sync_all'
    get 'sync_branches/:cluster_id/cluster', to: 'sync_branches#by_cluster'
    get 'sync_centers/:branch_id/branch', to: 'sync_centers#by_branch'
    get 'sync_loan_products/sync_all', to: 'sync_loan_products#sync_all'
    get 'sync_loan_product_types/sync_all', to: 'sync_loan_product_types#sync_all'
    get 'sync_loan_product_dependencies/sync_all', to: 'sync_loan_product_dependencies#sync_all'
    get 'sync_loan_deduction_entries/sync_all', to: 'sync_loan_deduction_entries#sync_all'
    get 'sync_loan_insurance_deduction_entries/sync_all', to: 'sync_loan_insurance_deduction_entries#sync_all'
    get 'sync_members/sync_all', to: 'sync_members#sync_all'
    get 'sync_beneficiaries/sync_all', to: 'sync_beneficiaries#sync_all'
    get 'sync_legal_dependents/sync_all', to: 'sync_legal_dependents#sync_all'
    get 'sync_savings_accounts/sync_all', to: 'sync_savings_accounts#sync_all'

    post 'sync', to: 'sync#index'

    # Misc
    get 'misc/loan_product_ep_cycle_count_max_amounts', to: 'misc#download_loan_product_ep_cycle_count_max_amounts'
    get 'misc/loan_product_dependencies', to: 'misc#download_loan_product_dependencies'
    get 'misc/loan_product_types', to: 'misc#download_loan_product_types'
    get 'misc/loan_deductions', to: 'misc#download_loan_deductions'
    get 'misc/loan_deduction_entries', to: 'misc#download_loan_deduction_entries'
    get 'misc/loan_insurance_deduction_entries', to: 'misc#download_loan_insurance_deduction_entries'


    # Payment Collections
    post 'payment_collections/approve', to: 'payment_collections#approve'
    post 'payment_collections/reverse', to: 'payment_collections#reverse'
    post 'payment_collections/add_member', to: 'payment_collections#add_member'
    post 'payment_collections/delete_payment_collection_record', to: 'payment_collections#delete_payment_collection_record'

    #Loan Insurances
    post 'loan_insurances/generate_transaction', to: 'loan_insurances#generate_transaction'
    post 'loan_insurances/add_member', to: 'loan_insurances#add_member'
    post 'loan_insurances/delete_loan_insurance_record', to: 'loan_insurances#delete_loan_insurance_record'
    post 'loan_insurances/approve', to: 'loan_insurances#approve'
    post 'loan_insurances/reverse', to: 'loan_insurances#reverse'

    #Insurance Account Validations
    post 'insurance_account_validations/generate_transaction', to: 'insurance_account_validations#generate_transaction'
    post 'insurance_account_validations/add_member', to: 'insurance_account_validations#add_member'
    post 'insurance_account_validations/delete_insurance_account_validation_record', to: 'insurance_account_validations#delete_insurance_account_validation_record'
    post 'insurance_account_validations/approve', to: 'insurance_account_validations#approve'
    post 'insurance_account_validations/validate', to: 'insurance_account_validations#validate'
    post 'insurance_account_validations/check', to: 'insurance_account_validations#check'
    post 'insurance_account_validations/cancel', to: 'insurance_account_validations#cancel'
    post 'insurance_account_validations/reverse', to: 'insurance_account_validations#reverse'
    post 'insurance_account_validations/cancel', to: 'insurance_account_validations#cancel'
    post 'insurance_account_validations/cancel_member', to: 'insurance_account_validations#cancel_member'

    #Reinstatements
    post 'reinstatements/generate_transaction', to: 'reinstatements#generate_transaction'
    post 'reinstatements/add_member', to: 'reinstatements#add_member'
    post 'reinstatements/delete_reinstatement_record', to: 'reinstatements#delete_reinstatement_record'
    post 'reinstatements/approve', to: 'reinstatements#approve'
    post 'reinstatements/reverse', to: 'reinstatements#reverse'
  
    #Deceased and Total and Permanent Disabilities
    post 'deceased_and_total_and_permanent_disabilities/generate_transaction', to: 'deceased_and_total_and_permanent_disabilities#generate_transaction'
    post 'deceased_and_total_and_permanent_disabilities/add_legal_dependent', to: 'deceased_and_total_and_permanent_disabilities#add_legal_dependent'
    post 'deceased_and_total_and_permanent_disabilities/delete_deceased_and_total_and_permanent_disability_record', to: 'deceased_and_total_and_permanent_disabilities#delete_deceased_and_total_and_permanent_disability_record'
    post 'deceased_and_total_and_permanent_disabilities/approve', to: 'deceased_and_total_and_permanent_disabilities#approve'
    post 'deceased_and_total_and_permanent_disabilities/reverse', to: 'deceased_and_total_and_permanent_disabilities#reverse'
  end

  namespace :v2 do
    get 'members', to: 'members#index'
  end
end
