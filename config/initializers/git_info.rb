GIT_BRANCH = `git status | sed -n 1p`.split(" ").last
GIT_COMMIT = `git log | sed -n 1p`.split(" ").last

SOFTWARE_NAME = "Kooperative Online Information System"
BUILD_VERSION = "1.0"
