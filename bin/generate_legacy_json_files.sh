RAILS_ENV=production bundle exec rake legacy:save_loan_payments
RAILS_ENV=production bundle exec rake legacy:save_loans
RAILS_ENV=production bundle exec rake legacy:save_membership_payments
RAILS_ENV=production bundle exec rake legacy:save_account_transactions
RAILS_ENV=production bundle exec rake legacy:save_member_accounts
RAILS_ENV=production bundle exec rake legacy:save_accounting_entries
RAILS_ENV=production bundle exec rake legacy:save_members
