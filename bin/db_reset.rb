seed_users = "bundle exec rake db:seed:users"
seed_accounting = "bundle exec rake db:seed:accounting"
seed_members = "bundle exec rake db:seed:members"
seed_project_types = "bundle exec rake db:seed:project_types"

cmd = "bundle exec rake db:drop && bundle exec rake db:migrate && #{seed_users} && #{seed_accounting} && #{seed_members} && #{seed_project_types}"
puts "Running command: #{cmd}"

# Execute command
system cmd
