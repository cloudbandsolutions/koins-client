require 'rails_helper'

RSpec.describe "holidays/index", type: :view do
  before(:each) do
    assign(:holidays, [
      Holiday.create!(
        :name => "Name"
      ),
      Holiday.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of holidays" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
