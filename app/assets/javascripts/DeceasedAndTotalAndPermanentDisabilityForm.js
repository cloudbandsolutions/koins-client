var DeceasedAndTotalAndPermanentDisabilityForm = (function() {
  var $parameters                              = $("#parameters");
  var deceasedAndTotalAndPermanentDisabilityId = $parameters.data('deceased-and-total-and-permanent-disability-id');
  var $section                                 = $(".transaction-table");
  var $btnDelete                               = $(".btn-delete");
  var $modalLoading                            = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $legalDependentSelect                    = $("#legal-dependent-select");
  var $btnAddLegalDependent                    = $("#btn-add-legal-dependent");

 
  // Hide controls
  $(".modal-loading").find('.controls').hide();

  var urlDeleteDeceasedAndTotalAndPermanentDisabilityRecord = "/api/v1/deceased_and_total_and_permanent_disabilities/delete_deceased_and_total_and_permanent_disability_record";
  var urlAddLegalDependent                                  = "/api/v1/deceased_and_total_and_permanent_disabilities/add_legal_dependent";

  var _bindEvents = function() {
    $btnAddLegalDependent.on('click', function() {
      $modalLoading.open();
      if(!$btnAddLegalDependent.hasClass('loading')) {
        $(this).addClass('loading');
        $(this).addClass('disabled');
        var legalDependentId = $legalDependentSelect.val();

        $.ajax({
          url: urlAddLegalDependent,
          method: 'POST',
          dataType: 'json',
          data: { id: deceasedAndTotalAndPermanentDisabilityId, legal_dependent_id: legalDependentId },
          success: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            toastr.success("Successfully added legal dependent");
            window.location.href = "/deceased_and_total_and_permanent_disabilities/" + deceasedAndTotalAndPermanentDisabilityId + "/edit";
          },
          error: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            toastr.error(errorMessages);
            $modalLoading.close();
          }
        });
      } else {
        toastr.info("Still loading member");
      }
    });

    $btnDelete.on('click', function() {
      var $btn = $(this);
      $btn.addClass('loading');
      $btn.addClass('disabled');
      var deceasedAndTotalAndPermanentDisabilityRecordId = $btn.data('deceased-and-total-and-permanent-disability-record-id');

      $modalLoading.open();

      $.ajax({
        url: urlDeleteDeceasedAndTotalAndPermanentDisabilityRecord,
        method: 'POST',
        dataType: 'json',
        data: { deceased_and_total_and_permanent_disability_record_id: deceasedAndTotalAndPermanentDisabilityRecordId },
        success: function(responseContent) {
          toastr.success("Successfully deleted record");
          window.location.href = "/deceased_and_total_and_permanent_disabilities/" + deceasedAndTotalAndPermanentDisabilityId + "/edit";
        },
        error: function(responseContent) {
          toastr.error("Cannot delete this record");
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
          $modalLoading.close();
        }
      });
    });
  }


  var init = function() {
    _bindEvents();
  }

  return {
    init: init
  };
})();
