var reportsDashboard = (function() {
  var data = null;
  var $activeLoanersSegments = $(".active-loaners-graph");
  var url = "/api/v1/reports/active_loaners_by_branch";

  function reloadData() {
    // activate loaders
    $activeLoanersSegments.find(".ui.dimmer.inverted").addClass("active");

    $activeLoanersSegments.each(function(index) {
      var branchId = $(this).data("branch-id");
      var self = $(this);
      var params = {
        branch_id: branchId
      };

      $.ajax({
        url: url,
        dataType: 'json',
        data: params,
        success: function(data) {
          var ctx = self.find(".active-loaners-canvas").get(0).getContext("2d");
          var options = {};
          var myNewChart = new Chart(ctx).Line(data, options);
          self.find(".ui.dimmer.inverted").removeClass("active");
        },
        error: function() {
          toastr.error("Error in getting active loaners for branch " + branchId);
        }
      });
    });
  }

  return {
    reloadData: reloadData
  };
})()

reportsDashboard.reloadData();
