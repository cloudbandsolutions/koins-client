var voucher = (function() {
  var urlApproveTransaction = "/api/v1/vouchers/approve";
  var urlRejectTransaction = "/api/v1/vouchers/reject";
  
  var $btnApprove;
  var $btnApproveConfirmation;
  var $btnCancelApproval;
  var $modalApprove;

  var $btnReject;
  var $btnCancelReject;
  var $btnRejectConfirmation;
  var $modalReject;
  
  var voucherId;
  var $parameters;

  var $modalApprove;
  var $modalReject;
  var $errors;
  var $errorsTemplate;
  var $modalErrorsApproval;
  var $modalErrorsReject;
  var $modalSuccessApproval;
  var $modalSuccessReject;
  var $modalControls;
  var $successTemplate;

  var _displayErrors = function(errors) {
    var errorsDisplay = Mustache.render($errorsTemplate.html(), { errors: errors });
    $errors.html(errorsDisplay);
  }

  var _hideErrors = function() {
    $errors.html("");
  }

  var _addLoadingToConfirmationBtns = function() {
    $btnApproveConfirmation.addClass('loading');
    $btnApproveConfirmation.addClass('disabled');
    $btnRejectConfirmation.addClass('loading');
    $btnRejectConfirmation.addClass('disabled');

    $btnCancelApproval.addClass('loading');
    $btnCancelApproval.addClass('disabled');
    $btnCancelReject.addClass('loading');
    $btnCancelReject.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnApproveConfirmation.removeClass('loading');
    $btnApproveConfirmation.removeClass('disabled');
    $btnRejectConfirmation.removeClass('loading');
    $btnRejectConfirmation.removeClass('disabled');

    $btnCancelApproval.removeClass('loading');
    $btnCancelApproval.removeClass('disabled');
    $btnCancelReject.removeClass('loading');
    $btnCancelReject.removeClass('disabled');
  }
  
  var _bindEvents = function() {

    $btnApproveConfirmation.on('click', function() {
      if(!$btnApproveConfirmation.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $.ajax({
          url: urlApproveTransaction,
          method: 'POST',
          dataType: 'json',
          data: { voucher_id: voucherId },
          success: function(responseContent) {
            $modalSuccessApproval.html(Mustache.render($successTemplate.html(), { messages: ["Successfully approved transaction"] }));
            $modalControls.hide();
            window.location.href = "/vouchers/" + voucherId;
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            $modalErrorsApproval.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to approve voucher");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $btnApprove.on('click', function() {
      $modalSuccessApproval.html("");
      $modalErrorsApproval.html("");
      $modalApprove.open();
    });

    $btnCancelApproval.on('click', function() {
      if(!$btnCancelApproval.hasClass('loading')) {
        $modalApprove.close();
      }
    });

    $btnRejectConfirmation.on('click', function() {
      if(!$btnRejectConfirmation.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $.ajax({
          url:  urlRejectTransaction,
          method: 'POST',
          dataType: 'json',
          data: { voucher_id: voucherId },
          success: function(responseContent) {
            $modalSuccessReject.html(Mustache.render($successTemplate.html(), { messages: ["Successfully reject transaction"] }));
            $modalControls.hide();
            window.location.href = "/vouchers/" + voucherId;
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            console.log(errorMessages);
            $modalErrorsReject.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to reject transaction");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.error("Still loading");
      }
    });

    $btnCancelReject.on('click', function() {
      if($btnCancelReject.hasClass('loading')) {
        toastr.info("Still loading");
      } else {
        $modalReject.close();
      }
    });

    $btnReject.on('click', function() {
      $modalSuccessApproval.html("");
      $modalErrorsReject.html("");
      $modalReject.open();
    });
  }


  var _cacheDom = function() {

    $confirmationModal        = $("#confirmation-modal"); 
    $btnApprove               = $("#btn-approve");
    $btnReject               = $("#btn-reject");
    $btnCancelApproval        = $("#btn-cancel-approval");
    $btnRejectConfirmation   = $("#btn-reject-confirmation");
    $btnCancelReject         = $("#btn-cancel-reject");
    $btnApproveConfirmation   = $("#btn-approve-confirmation");

    $btnRejectConfirm        = $("#btn-reject-confirm");
    $parameters               = $("#parameters");
     voucherId                = $parameters.data('voucher-id');
    $errors                   = $("#errors");
    $errorsTemplate           = $("#errors-template");

    $modalApprove             = $(".modal-approve").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalReject             = $(".modal-reject").remodal({ hashTracking: true, closeOnOutsideClick: false });

    $modalErrorsApproval      = $(".modal-approve").find(".errors");
    $modalErrorsReject       = $(".modal-reject").find(".errors");
    $modalSuccessApproval     = $(".modal-approve").find(".success");
    $modalSuccessReject      = $(".modal-reject").find(".success");
    $modalControls            = $(".modal-controls");
    $successTemplate          = $("#success-template");
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }  

  return {
    init: init
  }
})();

$(document).ready(function() {
  voucher.init();
});
