var generalLedger = (function() {
  var $generateGenPdf;
  var $generateBtn;
  var startDate;
  var $startDateInput;
  var endDate;
  var $endDateInput;
  var $generalLedgerTemplate;
  var $branchSelect;
  var $accountingCodesSelect;
  var accountingCodeIds;
  var branchId;
  var $generalLedgerSection;
  var $loader;
  var $downloadPdfBtn;
  var $accountsReportSection;
  var $downloadExcelBtn;

  var urlEntries = "/general_ledger/entries";
  var urlGenerateDownloadPdf = "/general_ledger/general_ledger_pdf";
  var urlDownloadExcel = "/general_ledger/generate_download_excel_url";
  var urlGenPdf = "/general_ledger/general_pdf";

  var _cacheDom = function() {
    $generateBtn = $("#generate-btn");
    $downloadExcelBtn = $("#download-excel-btn");
    $startDateInput = $("#start-date");
    $endDateInput = $("#end-date");
    $branchSelect = $("#branch-select");
    $generalLedgerSection = $("#general-ledger-section");
    $generalLedgerTemplate = $("#general-ledger-template").html();
    $loader = $("#loader");
    $downloadPdfBtn = $("#download-pdf-btn");
    $accountingCodesSelect = $("#accounting-codes-select");
    $generateGenPdf = $("#download-gen-pdf-btn");
  }

  var _renderGeneralLedger = function(data) {
    $generalLedgerSection.html(Mustache.render($generalLedgerTemplate, data));
  }

  var _validateFilterInput = function() {
    startDate = $startDateInput.val();
    endDate = $endDateInput.val();

    if(!startDate || !endDate) {
      return false;
    } else {
      return true;
    }
  }
  
  var _bindEvents = function() {
    $generateGenPdf.on('click', function() {
      //alert ("my first javascript");
     startDate = $startDateInput.val();
     endDate = $endDateInput.val();
     //alert(endDate);

      var params = {
        start_date: startDate,
        end_date: endDate
      };

      var url = urlGenPdf + "?" + encodeQueryData(params);
      //alert(url);
    
      window.open(url, '_blank');

  });
    $downloadPdfBtn.on('click', function() {
      $loader.addClass("active");
      if(_validateFilterInput()) {
        branchId = $branchSelect.val();
        startDate = $startDateInput.val();
        endDate = $endDateInput.val();
        accountingCodeIds = $accountingCodesSelect.val();

        var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate,
          accounting_code_ids: accountingCodeIds
        };

        console.log(params);

        window.open(urlGenerateDownloadPdf + "?" + encodeQueryData(params), '_blank');
      } else {
        toastr.error("Start date and end date required");
        $loader.removeClass('active');
      }
    });

    $generateBtn.on('click', function() {
      $loader.addClass("active");

      if(_validateFilterInput()) {
        branchId = $branchSelect.val();
        startDate = $startDateInput.val();
        endDate = $endDateInput.val();
        accountingCodeIds = $accountingCodesSelect.val();

        var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate,
          accounting_code_ids: accountingCodeIds
        };

        $.ajax({
          url: urlEntries,
          data: params,
          dataType: 'json',
          method: 'GET',
          success: function(responseContent) {
            _renderGeneralLedger(responseContent.data);
            toastr.success("Generated trial balance");

            $(".curr").each(function() {
              var v = numberWithCommas(parseFloat($(this).html()));

              if(v.toString() != "NaN") {
                $(this).html(v);
              }
            });
            $loader.removeClass('active');
          },
          error: function(responseContent) {
            toastr.error("Something went wrong.");
            $loader.removeClass('active');
          }
        });
      } else {
        toastr.error("Start date and end date required");
        $loader.removeClass('active');
      }
    });

    $downloadExcelBtn.on('click', function() {
      $loader.addClass("active");
      if(_validateFilterInput()) {
        branchId = $branchSelect.val();
        startDate = $startDateInput.val();
        endDate = $endDateInput.val();
        accountingCodeIds = $accountingCodesSelect.val();

        var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate,
          accounting_code_ids: accountingCodeIds
        };

        console.log(params);
        $.ajax({
          url: urlDownloadExcel,
          data: params,
          dataType: 'json',
          method: 'GET',
          success: function(responseContent) {
            //_renderTrialBalance(responseContent.data);
            toastr.success("Downloading Excel. Please wait.");
            $loader.removeClass('active');
            window.open(responseContent.download_url, '_blank');
            //window.location.href=responseContent.download_url;
          },
          error: function(responseContent) {
            toastr.error("Something went wwrong.");
            $loader.removeClass('active');
          }
        });
      } else {
        toastr.error("Start date and end date required");
        $loader.removeClass('active');
      }
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  generalLedger.init();
});
