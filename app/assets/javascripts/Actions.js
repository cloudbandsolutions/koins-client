var Actions = (function() {
  var $btnGenerateDailyReport = $("#btn-generate-daily-report");
  var $btnGenerateReport      = $("#btn-generate-monthly-daily-report");
  var $btnSendDailyReport     = $("#btn-send-daily-report");
  var $loadingModal           = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $loadingModalControls   = $(".modal-loading").find(".controls");
  var urlGenerateDailyClosing = "/api/v1/generate_daily_report";
  var urlGenerateMonthlyDaily = "/api/v1/generate_monthly_daily_report";
  var urlSendDailyReport      = "/api/v1/send_daily_report";

  var init = function() {
    $loadingModalControls.hide();

    $btnGenerateDailyReport.on('click', function() {
      $loadingModal.open();
      $.ajax({
        url: urlGenerateDailyClosing,
        method: 'POST',
        success: function(response) {
          $loadingModal.close();
          toastr.success('Successfully generated daily report');
          // Redirect back to dashboard
          window.location.href = "/";
        },
        error: function(response) {
          $loadingModal.close();
          toastr.error('Error in generating daily report');
        }
      });
    });

    $btnGenerateReport.on('click', function() {
      $loadingModal.open();
      $.ajax({
        url: urlGenerateMonthlyDaily,
        method: 'POST',
        success: function(response) {
          $loadingModal.close();
          toastr.success('Successfully generated monthly report');
        },
        error: function(response) {
          $loadingModal.close();
          toastr.error('Error in generating monthly report');
        }
      });
    });

    $btnSendDailyReport.on('click', function() {
      $loadingModal.open();
      $.ajax({
        url: urlSendDailyReport,
        method: 'POST',
        success: function(response) {
          $loadingModal.close();
          toastr.success('Successfully sent daily report to master server.');
        },
        error: function(response) {
          $loadingModal.close();
          toastr.error('Error in sending daily report to master server');
        }
      });
    });
  }

  return {
    init: init
  };
})();
