var MissingAccounts = (function() {

  var urlCreateMissingAccounts = "/api/v1/members/create_missing_accounts";
  var $btnGenerateMissingAccounts;
  var $modalLoading;
  var $modalLoadingBtnClose;
  var $modalLoadingControls;

  var _cacheDom = function() {
    $btnGenerateMissingAccounts = $("#btn-generate-missing-accounts");
    $modalLoading               = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingControls       = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
  }

  var _bindEvents = function() {
    $btnGenerateMissingAccounts.on('click', function() {
      var $btn = $(this);
      $btn.addClass('loading');
      $btn.addClass('disabled');
      $modalLoading.open();
      $.ajax({
        url: urlCreateMissingAccounts,
        method: 'POST',
        data: {},
        dataType: 'json',
        success: function(responseContent) {
          toastr.success("Successfully created missing accounts");
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
          window.location = "/missing_accounts";
        },
        error: function(responseContent) {
          toastr.error("Error in creating missing accounts");
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
          $modalLoading.close();
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  }
})();
