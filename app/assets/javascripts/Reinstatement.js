var Reinstatement = (function() {
  var $modalBranchSelect;
  var $datePrepared;
  var $btnGenerateReinstatement;
  var $btnCancelGenerateReinstatement;
  var $btnNewReinstatement;
  var $generateReinstatementModal;

  var urlUtilsBranches             = "/api/v1/utils/branches";
  
  var $errors;  
  var $errorsTemplate;
  var $modalErrors;
  var $modalSuccess;
  var $modalControls;
  var $successTemplate;
  var $clickableRow; 

  var $modalLoading;
  var $modalLoadingErrors;
  var $modalLoadingSuccess; 
  var $modalLoadingControls;
  var $modalLoadingBtnClose;

  var urlGenerateReinstatementTransaction = "/api/v1/reinstatements/generate_transaction";

  var _cacheDom = function() {
    $modalBranchSelect               = $("#modal-branch-select");
    $btnGenerateReinstatement        = $("#btn-generate-reinstatement");
    $btnCancelGenerateReinstatement  = $("#btn-cancel-generate-reinstatement");
    $datePrepared                   = $("#date-prepared");
    $generateReinstatementModal      = $(".modal-generate-reinstatement").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $btnNewReinstatement             = $("#btn-new-reinstatement");
    $modalErrors                     = $(".modal-generate-reinstatement").find(".errors");
    $errors                          = $(".errors");
    $modalSuccess                    = $(".modal-generate-reinstatement").find(".success");
    $modalControls                   = $(".modal-generate-reinstatement").find(".controls");
    $errorsTemplate                  = $("#errors-template");
    $successTemplate                 = $("#success-template");
    $clickableRow                    = $(".clickable");
   

    $modalLoading             = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingErrors       = $(".modal-loading").find(".errors");
    $modalLoadingSuccess      = $(".modal-loading").find(".success");
    $modalLoadingControls     = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
    $modalLoadingBtnClose     = $(".modal-loading").find(".btn-close");
  }

  var _addLoadingToConfirmationBtns = function() {
    $btnGenerateReinstatement.addClass('loading');
    $btnGenerateReinstatement.addClass('disabled');

    $btnCancelGenerateReinstatement.addClass('loading');
    $btnCancelGenerateReinstatement.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnGenerateReinstatement.removeClass('loading');
    $btnGenerateReinstatement.removeClass('disabled');

    $btnCancelGenerateReinstatement.removeClass('loading');
    $btnCancelGenerateReinstatement.removeClass('disabled');
  }

  var _bindEvents = function() {
    $modalLoadingBtnClose.on('click', function() {
      $modalLoading.close();
    });


    $clickableRow.on('click', function() {
      transactionId = $(this).data('transaction-id');
      window.location = "/reinstatements/" + transactionId;
    });

    $btnNewReinstatement.on('click', function() {
      $generateReinstatementModal.open();
    });

    $btnCancelGenerateReinstatement.on('click', function() {
      if(!$btnGenerateReinstatement.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $generateReinstatementModal.close();
        _removeLoadingToConfirmationBtns();
      } else {
        toastr.info("Still loading");
      }
    });

    $btnGenerateReinstatement.on('click', function() {
      var branchId       = $modalBranchSelect.val();
      var datePrepared  = $datePrepared.val();
      var data = {
        branch_id: branchId,
        date_prepared: datePrepared
      };

      if(!$btnGenerateReinstatement.hasClass('loading')) {
        $modalErrors.html("");
        $modalSuccess.html("");
        _addLoadingToConfirmationBtns();
        $.ajax({
          url: urlGenerateReinstatementTransaction,
          data: data,
          dataType: 'json',
          method: 'POST',
          success: function(responseContent) {
            console.log(responseContent);
            _removeLoadingToConfirmationBtns();
            $modalSuccess.html(Mustache.render($successTemplate.html(), { messages: responseContent.messages }));
            $modalControls.hide();
            r_id = responseContent.reinstatement_id;
            window.location = "/reinstatements/" + r_id + "/edit";
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            console.log(errorMessages);
            $modalErrors.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to generate deposit transaction");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $modalBranchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($modalBranchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
