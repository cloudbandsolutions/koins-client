var reportsPARs = (function() {
  var $searchBtn            = $("#search-btn");
  var $downloadBtn          = $("#download-btn");
  var $branchSelect         = $("#branch-select");
  var $soSelect             = $("#so-select");
  var $centerSelect         = $("#center-select");
  var $detailedCheckBox     = $("#detailed");
  var $parSection           = $("#reports-par-section");
  var $parTemplate          = $("#reports-par-template").html();
  var $parDetailedTemplate  = $("#reports-par-detailed-template").html();
  var $loanProductSelect    = $("#loan-product-select");

  var urlGeneratePar        = "/api/v1/reports/par";

  var _bindEvents = function() {
    $branchSelect.on('change', function() {
      // empty so select
      populateSelect($centerSelect, [], "id", "name");

      var url = "/api/v1/branches/so_list";
      branchId = $branchSelect.val();

      if(branchId) {
        $.ajax({
          url: url,
          data: { branch_id: branchId },
          dataType: 'json',
          success: function(data) {
            toastr.info("Loading SOs...");
            populateSelect($soSelect, data.so_list, "name", "name");
          },
          error: function(data) {
            toastr.error("Error in loading SOs.");
          }
        });
      } else {
        populateSelect($soSelect, [], "name", "name");
      }
    });

    $soSelect.on('change', function() {
      var url = "/api/v1/branches/centers_by_so";
      so = $soSelect.val();

      $.ajax({
        url: url,
        data: { so: so },
        dataType: 'json',
        success: function(data) {
          toastr.info("Loading Centers...");
          populateSelect($centerSelect, data.centers, "id", "name");
        },
        error: function(data) {
          toastr.error("Error in loading centers.");
        }
      });
    });

    $downloadBtn.on('click', function() {
      $downloadBtn.addClass('loading');

      branchId = $branchSelect.val();
      centerId = $centerSelect.val();
      so = $soSelect.val();
      detailed = $detailedCheckBox.bootstrapSwitch('state');
      loanProductId = $loanProductSelect.val();

      var params = {
        branch_id: branchId,
        center_id: centerId,
        so: so,
        loan_product_id: loanProductId,
        detailed: detailed
      };

      $.ajax({
        url: urlGeneratePar,
        method: 'GET',
        dataType: 'json',
        data: params,
        success: function(data) {
          console.log(data);
          if(detailed) {
            $parSection.html(Mustache.render($parDetailedTemplate, data));
          } else {
            $parSection.html(Mustache.render($parTemplate, data));
          }
          $downloadBtn.removeClass('loading');

          tempUrl = data.download_url;
          window.open(tempUrl, '_blank');
        },
        error: function(data) {
          toastr.error("Error in generating par report");
          $downloadBtn.removeClass('loading');
        }
      });
    });

    $searchBtn.on('click', function() {
      $searchBtn.addClass('loading');

      branchId = $branchSelect.val();
      centerId = $centerSelect.val();
      so = $soSelect.val();
      detailed = $detailedCheckBox.bootstrapSwitch('state');
      loanProductId = $loanProductSelect.val();

      var params = {
        branch_id: branchId,
        center_id: centerId,
        so: so,
        loan_product_id: loanProductId,
        detailed: detailed
      };

      $.ajax({
        url: urlGeneratePar,
        method: 'GET',
        dataType: 'json',
        data: params,
        success: function(data) {
          console.log(data);
          if(detailed) {
            $parSection.html(Mustache.render($parDetailedTemplate, data));
          } else {
            $parSection.html(Mustache.render($parTemplate, data));
          }

          $parSection.find(".curr").each(function() {
            $(this).html(numberWithCommas($(this).html()));
          });

          // Make sticky
          $(".sticky").stickyTableHeaders();

          $searchBtn.removeClass('loading');
        },
        error: function(data) {
          toastr.error("Error in generating par report");
          $searchBtn.removeClass('loading');
        }
      });
    });
  }

  var _loadDefaults = function() {
  }

  var init = function() {
    _loadDefaults();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  reportsPARs.init();
});

