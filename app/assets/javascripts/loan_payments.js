/*
 * Generate loan payment voucher particular
 */
var generateLoanPaymentVoucherParticular = function() {
  var memberId = $("#memberId").val();
  console.log("Member ID: " + memberId);

  var memberName = $("#memberName").val();
  console.log("Member Name: " + memberName);

  var cpAmountText = $("#cpAmountText");
  var cpAmount = cpAmountText.val();
  console.log("CP Amount: " + cpAmount);

  var wpAmountText = $("#wpAmountText");
  var wpAmount = wpAmountText.val();
  console.log("WP Amount: " + wpAmount);

  var totalAmount = Number(cpAmount) + Number(wpAmount);

  var paidAtDateMonthSelect = $("select[name='loan_payment[paid_at(2i)]']");
  var paidAtDateMonth = paidAtDateMonthSelect.val();
  console.log("Paid at Month: " + paidAtDateMonth);

  var paidAtDateDaySelect = $("select[name='loan_payment[paid_at(3i)]']");
  var paidAtDateDay = paidAtDateDaySelect.val();
  console.log("Paid at Day: " + paidAtDateDay);

  var particular = "Payment of Loan/Deposit of Funds - " + memberName + " d/s " + paidAtDateMonth + "/" + paidAtDateDay + " " + cpAmount + " + " + wpAmount + " Dep. = " + totalAmount;
  $("#loan_payment_particular").val(particular);
  console.log("Particular: " + particular);
}

$(function() {
  $("#cpAmountText").on('change', function(evt) {
    console.log($(this).val()); 
    generateLoanPaymentVoucherParticular();
  });

  $("#wpAmountText").on('change', function(evt) {
    console.log($(this).val()); 
    generateLoanPaymentVoucherParticular();
  });

  $("select[name='loan_payment[paid_at(1i)]']").on('change', function(evt) {
    console.log($(this).val()); 
    generateLoanPaymentVoucherParticular();
  });

  $("select[name='loan_payment[paid_at(2i)]']").on('change', function(evt) {
    console.log($(this).val()); 
    generateLoanPaymentVoucherParticular();
  });

  $("select[name='loan_payment[paid_at(3i)]']").on('change', function(evt) {
    console.log($(this).val()); 
    generateLoanPaymentVoucherParticular();
  });
});
