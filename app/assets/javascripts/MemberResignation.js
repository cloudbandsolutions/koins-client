var MemberResignation = (function() {
  var $parameters               = $("#parameters");
  var id                        = $parameters.data('id');
  var memberId                  = $parameters.data('member-id');
  var $modalApproveResignation  = $(".modal-approve-resignation").remodal({
                                    hashTracking: true, 
                                    closeOnOutsideClick: false
                                  });

  var $modalErrorsApproval      = $(".modal-approve-resignation").find(".errors");
  var $modalSuccessApproval     = $(".modal-approve-resignation").find(".success");
  var $modalControlsApproval    = $(".modal-approve-resignation").find(".controls");
  var $modalMessageApproval     = $(".modal-approve-resignation").find(".message");
  var $btnApprove               = $("#btn-approve");
  var $btnConfirmApproval       = $("#btn-confirm-approval");
  var $btnCancelApproval        = $("#btn-cancel-approval");
  var $errorsTemplate           = $("#errors-template");

  var $modalRevertResignation       = $("#modal-revert-resignation");
  var $btnRevertResignation         = $("#btn-revert-resignation");
  var $btnConfirmRevertResignation  = $("#btn-confirm-revert-resignation");

  var urlApprove                = "/api/v1/members/resignation/approve";
  var urlRevertResignation  = "/api/v1/members/resignation/revert_resignation";

  var init  = function() {
    $btnRevertResignation.on('click', function() {
      $modalRevertResignation.modal('show');
    });

    $btnConfirmRevertResignation.on('click', function() {
      $btnConfirmRevertResignation.prop('disabled', true);
      $.ajax({
        url: urlRevertResignation,
        method: 'POST',
        dataType: 'json',
        data: {
          member_id: memberId,
        },
        success: function(response) {
          toastr.success('Successfully reverted member status. Redirecting...')
          window.location = "/members/" + memberId;
        },
        error: function(response) {
          toastr.error('Error in reverting resignation');
          $btnConfirmRevertResignation.prop('disabled', false);
        }
      });
    });

    $btnApprove.on('click', function() {
      $modalApproveResignation.open();
    });

    $btnCancelApproval.on('click', function() {
      $modalApproveResignation.close();
    });

    $btnConfirmApproval.on('click', function() {
      $modalControlsApproval.hide();
      $modalMessageApproval.html("Loading...");
      $modalErrorsApproval.html("");

      $.ajax({
        url: urlApprove,
        method: 'POST',
        dataType: 'json',
        data: {
          id: id
        },
        success: function(response) {
          $modalMessageApproval.html("Success! Redirecting...");
          toastr.success("Success!");
          window.location.href = "/members/" + memberId;
        },
        error: function(response) {
          $modalControlsApproval.show();
          $modalMessageApproval.html("Error!");
          $modalErrorsApproval.html(
            Mustache.render(
              $errorsTemplate.html(),
              JSON.parse(response.responseText)
            )
          );
        }
      });
    });
  }

  return {
    init: init
  };
})();
