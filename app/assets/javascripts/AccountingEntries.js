var AccountingEntries = (function() {
  //this
  var $clickableRow;

  var _cacheDom = function() {
    //for clickabe row
    $clickableRow        = $(".clickable");    }

  var _bindEvents = function() {
    //for clickable row
    $clickableRow.on('click', function() {
      transactionId = $(this).data('transaction-id');
      window.location = "/vouchers/" + transactionId;
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  AccountingEntries.init();
});
