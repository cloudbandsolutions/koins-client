var switchOnOff = (function() {

  var $switchOnOff;
  var $switchForm
 
  var _cacheDom = function() {
    $switchOnOff   = $("#switch-on-off");
    $switchForm    = $(".switch-form");
  }

  var _bindEvents = function() {
    var state = $switchOnOff.bootstrapSwitch('state');
    var checked = state;
    if (checked) {
      $switchForm.show();
    }
    else {
      $switchForm.hide();
    }  

    $switchOnOff.on('switchChange.bootstrapSwitch', function (event, state) {
      var checked = state;
      if (checked) {
          $switchForm.show();
      }
      else {
          $switchForm.hide();
      }
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  switchOnOff.init();
});
