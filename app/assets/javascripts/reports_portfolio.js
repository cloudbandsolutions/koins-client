var reportsPortfolio = (function() {
  var data = null;
  var url = "/api/v1/reports/loan_product_portfolio_by_branch";
  var $filterBtn = $("#filterBtn");
  var $branchSelect = $("#branch-select");
  var $portfolioGraph = $(".portfolio-graph");

  // Events
  $filterBtn.on('click', reloadData);

  function reloadData() {
    var self = $(this);
    var branchId = $branchSelect.val();

    $portfolioGraph.each(function(e, i) {
      var self = this;
      var loanProductId = $(this).data('loan-product-id');
      var params = {
        branch_id: branchId,
        loan_product_id: loanProductId
      };

      var portfolioCanvas = $(self).find(".portfolio-canvas");
      var ctx = portfolioCanvas.get(0).getContext("2d");

      $(self).find(".ui.dimmer.inverted").addClass("active");

      $.ajax({
        url: url,
        dataType: 'json',
        data: params,
        success: function(data) {
          var options = {};
          var lineChart = new Chart(ctx).Line(data.data, options);

          var distributionTable = $(self).find('.distribution-table');
          var template = $(self).find('.distribution-table-template').html();
          distributionTable.html(Mustache.render(template, data));

          $(self).find(".ui.dimmer.inverted").removeClass("active");
        },
        error: function() {
          toastr.error("Error in getting data for branch " + branchId);
          $(self).find(".ui.dimmer.inverted").removeClass("active");
        }
      });
    });
  }
})()
