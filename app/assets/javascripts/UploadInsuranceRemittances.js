var UploadInsuranceRemittances = (function() {
  var $branchSelect;
  var $centerSelect;
  var $dateOfPayment;

  var urlUtilsBranches  = "/api/v1/utils/branches";
  var urlUtilsCenters   = "/api/v1/utils/centers";


  var _cacheDom = function() {
    $branchSelect        = $("#branch-select");
    $centerSelect        = $("#center-select");
    $dateOfPayment       = $("#date-of-payment");
      }


  var _bindEvents = function() {
    
    $branchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($branchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });

    $branchSelect.bind('change', function() {
      var modalBranchId = ($(this).val());
      params = { branch_id: modalBranchId };

      $centerSelect.find('option').remove();

      $.ajax({
        url: urlUtilsCenters,
        type: 'get',
        dataType: 'json',
        data: params,
        success: function(data) {
          var centers = data.data.centers;
          populateSelect($centerSelect, centers, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading centers on branch select");
        }
      });
    });

    // Preload centers
    var modalBranchId = ($branchSelect.val());
    params = { branch_id: modalBranchId };
    $.ajax({
      url: urlUtilsCenters,
      type: 'get',
      dataType: 'json',
      data: params,
      success: function(data) {
        var centers = data.data.centers;
        populateSelect($centerSelect, centers, 'id', 'name');
      },
      error: function() {
        toastr.error("ERROR: loading centers on branch select");
      }
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  UploadInsuranceRemittances.init();
});
