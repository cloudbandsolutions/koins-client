var CashManagementTimeDeposit = (function(){
  var $btnGenerate = $("#btn-generate");
  var $btnAction = $("#btn-action");
  var $startDate = $("#start-date");
  var $id = 1;
  var $modalTransaction             = $("#modal-transaction");
  var urlGenerateTimeDepositTransaction = "/api/v1/cash_management/time_deposit/payment_collections/generate_transaction";
  var urlApprovedTimeDepositTransaction = "/api/v1/cash_management/time_deposit/payment_collections/approved_time_deposit_transaction";


  var $btnEdit = $("#btn-edit");
  var $parameters_id = $("#parameters");
  var $btnApproved = $("#btn-approved")
  
  //withdrawal_interest
  var urlWIthdrawTimeDepositTransaction = "/api/v1/adjustments/time_deposit/withdraw";
  var urlWIthdrawTimeDepositTransactionApproved = "/api/v1/adjustments/time_deposit/approved";
  var $btnInterest = $("#btn-interest");
  var $btnApprovedWithdrwal = $("#btn-approved");
  var $SavingsTransId = $("#savings_trans_id");
  var $ModalTransactionWithdraw = $("#modal-transaction-withdraw");
  var $txtStartDateWithdraw = $("#start-date-withdraw");
  var $btnGenerateInterest = $("#btn-generate-interest");

  var init = function(){


    $btnGenerateInterest.on("click", function(){
      

      $ModalTransactionWithdraw.modal("show");
    
    });

    $btnApprovedWithdrwal.on("click", function(){
      
      var params = {
        savings_transaction: $SavingsTransId.val()
      }

      $.ajax({
        url: urlWIthdrawTimeDepositTransactionApproved,
        method: "POST",
        dataType: 'json',
        data: params,
        success: function(response){
          //alert ("jef")
          pc_id = response.id
          //alert(pc_id)
          alert("Successfully Apporved")
          window.location= "/adjustments/time_deposits/operation_details/" + pc_id
          
          
        }
      });
    
    });
    
    $btnInterest.on("click", function(){

      var params = {
        savings_transaction: $SavingsTransId.val(),
        withdraw_date: $txtStartDateWithdraw.val()
      }
      
      $.ajax({
        url: urlWIthdrawTimeDepositTransaction,
        method: "POST",
        dataType: 'json',
        data: params,
        success: function(response){
          
          pc_id = response.id
          //alert(pc_id)
          //alert("Successfully Apporved")
          window.location= "/adjustments/time_deposits/operation_details/" + pc_id
          
          
        }
      });
    });
    
  


    $btnApproved.on("click", function(){

      
      var params= {
        pc_id: $parameters_id.val()
      }
      $.ajax({
        url: urlApprovedTimeDepositTransaction,
        method: "POST",
        dataType: 'json',
        data: params,
        success: function(response){
          
          alert("Successfully Apporved")
          location.reload();
          
          
        }
      });



    
    });




    $btnEdit.on("click", function(){
      pc_id = $parameters_id.val() 
          
      window.location = "/cash_management/time_deposits/operation_details/" + pc_id +  "/edit"
    });




    $btnAction.on("click", function(){
      //alert("jef")
      $modalTransaction.modal("show");

    });

    $btnGenerate.on("click", function(){
      //alert ("jef")
      var params = {
        start_date: $startDate.val()
      };
      $.ajax({
        url: urlGenerateTimeDepositTransaction,
        method: "POST",
        dateType: 'json',
        data: params,
        success: function(response){
          pc_id = response.payment_collection_id
          window.location = "/cash_management/time_deposits/operation_details/" + pc_id +  "/edit"
        },
        error: function(response){
          toastr.error("Error in generatin Share Capital")
        }
      });
    
    });
  
  };
  return {
    init: init
  }




})();
