var CashManagementEquityWithdrawalsCollectionForm = (function() {
  var $parameters           = $("#parameters");
  var $insuranceTypes       = $parameters.data("insurance-types");
  var paymentCollectionId   = $parameters.data("payment-collection-id");
  var $equityWithdrawals = $(".equity-withdrawal");
  var $grandTotal           = $(".grand-total");
  var $section              = $(".transaction-table");
  var $totalMemberAmounts   = $(".total-member-amount");

  var $modalLoading         = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $memberSelect         = $("#member-select");
  var $btnDelete            = $(".btn-delete");
  var $btnAddMember         = $("#btn-add-member");

  // Hide controls
  $(".modal-loading").find('.controls').hide();

  var urlDeletePaymentCollectionRecord  = "/api/v1/payment_collections/delete_payment_collection_record";
  var urlAddMember                      = "/api/v1/payment_collections/add_member";

  var _bindEvents = function() {
    $btnAddMember.on('click', function() {
      $modalLoading.open();
      if(!$btnAddMember.hasClass('loading')) {
        $(this).addClass('loading');
        $(this).addClass('disabled');
        var memberId = $memberSelect.val();

        $.ajax({
          url: urlAddMember,
          method: 'POST',
          dataType: 'json',
          data: { id: paymentCollectionId, member_id: memberId },
          success: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            toastr.success("Successfully added member");
            window.location.href = "/cash_management/equity_withdrawals/payment_collections/" + paymentCollectionId + "/edit";
          },
          error: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            toastr.error("Something went wrong when trying to add member");
            $modalLoading.close();
          }
        });
      } else {
        toastr.info("Still loading member");
      }
    });

    $btnDelete.on('click', function() {
      var $btn = $(this);
      $btn.addClass('loading');
      $btn.addClass('disabled');
      var paymentCollectionRecordId = $btn.data('payment-collection-record-id');

      $modalLoading.open();

      $.ajax({
        url: urlDeletePaymentCollectionRecord,
        method: 'POST',
        dataType: 'json',
        data: { payment_collection_record_id: paymentCollectionRecordId },
        success: function(responseContent) {
          toastr.success("Successfully deleted record");
          window.location.href = "/cash_management/equity_withdrawals/payment_collections/" + paymentCollectionId + "/edit";
        },
        error: function(responseContent) {
          toastr.error("Cannot delete this record");
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
          $modalLoading.close();
        }
      });
    });

    $.each($insuranceTypes, function() {
      var insuranceType = this;
      var $totalEquityWithdrawals = $section.find(".total-insurance[data-insurance-type-code='" + insuranceType + "']");
      _computeMemberTotalAmount();
      _computeTotalEquityWithdrawals($section.find(".insurance-withdrawal[data-account-code='" + insuranceType + "']"), $totalEquityWithdrawals);
    });

    $equityWithdrawals.on('change', function() {
      var insuranceType = $(this).data("account-code");
      var $totalEquityWithdrawals = $section.find(".total-insurance[data-insurance-type-code='" + insuranceType + "']");
      _computeTotalEquityWithdrawals($section.find(".insurance-withdrawal[data-account-code='" + insuranceType + "']"), $totalEquityWithdrawals);
      _computeMemberTotalAmount();
      _computeTotalAmt($section);
    });

    _computeMemberTotalAmount();
    _computeTotalAmt($section);
  }

  var _computeMemberTotalAmount = function() {
    $.each($totalMemberAmounts, function() {
      var memberId = $(this).data("member-id");
      var t = 0.00;
      $.each($section.find(".cp-amount[data-member-id='" + memberId + "']"), function() {
        t += parseFloat($(this).val());
      });

      $(this).val(t);
    });
  }

  var _computeTotalAmt = function($section) {
    var grandTotal = 0.00;

    $.each($section.find(".total-insurance"), function() {
      grandTotal += parseFloat($(this).val());
    });

    $section.find(".grand-total").val(grandTotal);
  }

  var _computeTotalEquityWithdrawals = function($equityWithdrawals, $totalEquityWithdrawals) {
    var t = 0.00;
    $equityWithdrawals.each(function() {
      t += parseFloat($(this).val());
    });

    $totalEquityWithdrawals.val(t);
  }

  var init = function() {
    _bindEvents();
  }

  return {
    init: init
  };
})();
