var PaymentCollection = (function() {
  var $parameters               = $("#parameters");
  var $errors                   = $("#errors");
  var $errorsTemplate           = $("#errors-template");
  var $successTemplate          = $("#success-template");

  var $btnApprove               = $("#btn-approve");
  var $modalApproveConfirmation = $(".modal-approve-confirmation").remodal({hashTracking: true, closeOnOutsideClick: false});
  var $modalErrorsApproval      = $(".modal-approve-confirmation").find(".errors");
  var $modalSuccessApproval     = $(".modal-approve-confirmation").find(".success");
  var $modalControlsApproval    = $(".modal-approve-confirmation").find(".controls");
  var $modalMessageApproval     = $(".modal-approve-confirmation").find(".message");
  var $btnConfirmApproval       = $("#btn-confirm-approval");
  var $btnCancelApproval        = $("#btn-cancel-approval");

  var $reverseActions           = $("#reverse-actions");
  var $btnReverse               = $("#btn-reverse");
  var $modalReverseConfirmation = $(".modal-reverse-confirmation").remodal({hashTracking: true, closeOnOutsideClick: false});
  var $modalErrorsReverse       = $(".modal-reverse-confirmation").find(".errors");
  var $modalSuccessReverse      = $(".modal-reverse-confirmation").find(".success");
  var $modalControlsReverse     = $(".modal-reverse-confirmation").find(".controls");
  var $modalMessageReverse      = $(".modal-reverse-confirmation").find(".message");
  var $btnConfirmReversal       = $("#btn-confirm-reversal");
  var $btnCancelReversal        = $("#btn-cancel-reversal");

  var paymentCollectionId       = $parameters.data("payment-collection-id");
  var urlApproveTransaction     = "/api/v1/payment_collections/approve";
  var urlReverseTransaction     = "/api/v1/payment_collections/reverse";

  var _handleReversalEvents = function() {
    $btnReverse.on('click', function() {
      $modalReverseConfirmation.open();
    });

    $btnCancelReversal.on('click', function() {
      $modalReverseConfirmation.close();
    });

    $btnConfirmReversal.on('click', function() {
      $modalControlsReverse.hide();
      $modalMessageReverse.html("Loading...");
      $modalErrorsReverse.html("");

      var ajaxConfig = {
        data: { id: paymentCollectionId },
        dataType: 'JSON',
        method: 'POST',
        url: urlReverseTransaction
      };

      $.ajax(ajaxConfig)
        .done(function() {
          toastr.success("Successfully reversed transaction");
          $modalMessageReverse.html("Success! Redirecting...");
          window.location = "/payment_collections/" + paymentCollectionId;
        })
        .fail(function(response) {
          _renderTemplate($modalErrorsReverse, $errorsTemplate.html(), JSON.parse(response.responseText));
          $modalMessageReverse.html("");
          $modalControlsReverse.show();
        })
    });
  }

  var _handleApprovalEvents = function() {
    $btnApprove.on('click', function() {
      $modalApproveConfirmation.open();
    });

    $btnCancelApproval.on('click', function() {
      $modalApproveConfirmation.close();
    });

    $btnConfirmApproval.on('click', function() {
      $modalControlsApproval.hide();
      $modalMessageApproval.html("Loading...");
      $modalErrorsApproval.html("");

      var ajaxConfig = {
        data: { id: paymentCollectionId },
        dataType: 'JSON',
        method: 'POST',
        url: urlApproveTransaction
      };

      $.ajax(ajaxConfig)
        .done(function() {
          toastr.success("Successfully approve transaction");
          $modalMessageApproval.html("Success! Redirecting...");
          window.location = "/payment_collections/" + paymentCollectionId;
        })
        .fail(function(response) {
          _renderTemplate($modalErrorsApproval, $errorsTemplate.html(), JSON.parse(response.responseText));
          $modalMessageApproval.html("");
          $modalControlsApproval.show();
        })
    });
  }

  var _displayErrors = function(errors) {
    var errorsDisplay = Mustache.render($errorsTemplate.html(), { errors: errors });
    $errors.html(errorsDisplay);
  }

  var _hideErrors = function() {
    $errors.html("");
  }

  var _addLoadingToReverseConfirmationBtns = function() {
    $btnConfirmReversal.addClass('loading');
    $btnConfirmReversal.addClass('disabled');

    $btnCancelReversal.addClass('loading');
    $btnCancelReversal.addClass('disabled');
  }
  
  var _removeLoadingFromReversalConfirmationBtns = function() {
    $btnConfirmReversal.removeClass('loading');
    $btnConfirmReversal.removeClass('disabled');

    $btnCancelReversal.removeClass('loading');
    $btnCancelReversal.removeClass('disabled');
  }

  var _bindEvents = function() {
    _handleApprovalEvents();
    _handleReversalEvents();
  }

  var init = function() {
    _bindEvents();
    // Make sticky
    $(".sticky").stickyTableHeaders();
  }

  return {
    init: init
  };
})();
