var MembershipPayment = (function() {
  var $approveBtn;
  var $editBtn;
  var $deleteBtn;
  var $approveMembershipPaymentModal;
  var $confirmApprovalBtn;
  var $rejectApprovalBtn;
  var $parameters;
  var membershipPaymentId;
  var memberId;
  var approvePaymentUrl = "/api/v1/membership_payments/approve";

  var _enableControls = function() {
    $approveBtn.prop('disabled', false);
    $editBtn.prop('disabled', false);
    $deleteBtn.prop('disabled', false);
  }

  var _disableControls = function() {
    $approveBtn.prop('disabled', true);
    $editBtn.prop('disabled', true);
    $deleteBtn.prop('disabled', true);
  }

  var _bindEvents = function() {
    $editBtn.on('click', function() {
    });

    $deleteBtn.on('click', function() {
    });

    $approveBtn.on('click', function() {
      $approveMembershipPaymentModal.modal('show');
    });

    $confirmApprovalBtn.on('click', function() {
      _disableControls();
      $.ajax({
        url: approvePaymentUrl,
        method: 'POST',
        data: { member_id: memberId, membership_payment_id: membershipPaymentId },
        dataType: 'json',
        success: function(responeContent) {
          $approveMembershipPaymentModal.modal('hide');
          toastr.success("Successfully approved payment");
          window.location.replace("/members/" + memberId);
        },
        error: function(responseContent) {
          _enableControls();
          $approveMembershipPaymentModal.modal('hide');
          toastr.error("Error in approving payment");
        }
      });
    });

    $rejectApprovalBtn.on('click', function() {
      $approveMembershipPaymentModal.modal('hide');
    });
  }

  var _cacheDom = function() {
    $parameters = $("#parameters");
    membershipPaymentId = $parameters.data('membership-payment-id');
    memberId = $parameters.data('member-id');
    $approveBtn = $("#approve-btn");
    $editBtn = $("#edit-btn");
    $deleteBtn = $("#delete-btn");
    $approveMembershipPaymentModal = $("#approve-membership-payment-modal");
    $confirmApprovalBtn = $approveMembershipPaymentModal.find(".approve-btn");
    $rejectApprovalBtn = $approveMembershipPaymentModal.find(".reject-btn");
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  MembershipPayment.init();
});
