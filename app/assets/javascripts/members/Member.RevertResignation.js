//= require_directory ./lib

Member.RevertResignation = (function() {
  var $btnRevertResignation                 = $("#btn-revert-resignation");
  var $btnConfirmRevertResignation          = $("#btn-confirm-revert-resignation");
  var $btnCancelRevertResignation           = $("#btn-cancel-revert-resignation");
  var $modalConfirmRevertResignation        = $(".modal-confirm-revert-resignation").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $modalConfirmRevertResignationSection = $(".modal-confirm-revert-resignation");
  var $modalConfirmRevertResignationMessage = $modalConfirmRevertResignationSection.find(".message");
  var $modalConfirmRevertResignationUi      = $modalConfirmRevertResignationSection.find(".ui");

  var $errorsTemplate           = $("#errors-template");
  var $parameters               = $("#parameters");
  var memberId                  = $parameters.data('member-id');

  var urlRevertResignation  = "/api/v1/members/resignation/revert_resignation";

  var _bindEvents = function() {
    $btnRevertResignation.on('click', function() {
      $modalConfirmRevertResignation.open();
    });

    $btnCancelRevertResignation.on('click', function() {
      $modalConfirmRevertResignation.close();
    });

    $btnConfirmRevertResignation.on('click', function() {
      $modalConfirmRevertResignationUi.hide();
      $modalConfirmRevertResignationMessage.html("Reverting...");

      $.ajax({
        url: urlRevertResignation,
        method: 'POST',
        dataType: 'json',
        data: {
          member_id: memberId,
        },
        success: function(response) {
          toastr.success('Successfully reverted member status')
          $modalConfirmRevertResignationMessage.html("Success! Redirecting...");
          window.location = "/members/" + memberId;
        },
        error: function(response) {
          toastr.error('Error in reverting resignation');
          $modalConfirmRevertResignationMessage.html(Mustache.render($errorsTemplate.html(), JSON.parse(response.responseText)));
          $modalConfirmRevertResignationUi.show();
        }
      });
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
