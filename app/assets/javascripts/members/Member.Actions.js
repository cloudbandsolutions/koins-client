//= require_directory ./lib

Member.Actions = (function() {
  var $toggleIsEditable           = $("#toggle-is-editable");
  var $modalRequestTransfer       = $("#modal-request-transfer");
  var $parameters                 = $("#parameters");
  var $btnConfirmRequestTransfer  = $("#btn-confirm-request-transfer");
  var $btnRequestTransfer         = $("#btn-request-transfer");

  var init = function() {
    if($parameters.data("is-editable")) {
      $toggleIsEditable.bootstrapSwitch('setState', true);
    } else {
      $toggleIsEditable.bootstrapSwitch('setState', false);
    }

    $toggleIsEditable.on('change', function() {
      alert('switching states');
    });

    $btnRequestTransfer.on("click", function() {
      $modalRequestTransfer.modal("show");
    });
  };

  return {
    init: init
  };
})();
