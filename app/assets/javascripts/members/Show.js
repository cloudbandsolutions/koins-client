var Show  = (function() {
  var $modalChangeOfRecognitionDate;
  var $modalResignFromInsurance;
  var $btnConfirmInsuranceResign;
  var $btnResignFromInsurance;
  var $btnChangeOfRecognitionDate;
  var $inputDateResigned;
  var $inputReason;
  var $newMiiRecognitionDate;
  var $message;
  var $errorsTemplate;
  var $parameters               = $("#parameters");
  var memberId                  = $parameters.data('member-id');
  var urlResignFromInsurance    = "/api/v1/insurance/members/resign";
  var urlBalikKasapiMii         = "/api/v1/insurance/members/balik_kasapi_mii";
  var urlChangeRecognitionDate  = "/api/v1/insurance/members/change_recognition_date";


  var _cacheDom = function() {
    $modalResignFromInsurance   = $("#modal-resign-from-insurance");
    $btnConfirmInsuranceResign  = $("#btn-confirm-insurance-resign");
    $btnResignFromInsurance     = $("#btn-resign-from-insurance");
    $message                    = $(".message");
    $errorsTemplate             = $("#errors-template");
    $inputDateResigned          = $("#input-date-resigned");
    $inputReason                = $("#input-reason");

    $newRecognitionDate         = $("#new-recognition-date");
    $modalBalikKasapiMii        = $("#modal-balik-kasapi-mii");
    $btnConfirmBalikKasapiMii   = $("#btn-confirm-balik-kasapi-mii");
    $btnBalikKasapiMii          = $("#btn-balik-kasapi-mii");

    $newMiiRecognitionDate              = $("#input-new-mii-recognition-date");
    $modalChangeOfRecognitionDate       = $("#modal-change-of-recognition-date");
    $btnConfirmChangeOfRecognitionDate  = $("#btn-confirm-change-of-recognition-date");
    $btnChangeOfRecognitionDate         = $("#btn-change-of-recognition-date");
  };

  var _bindEvents = function() {
    $btnChangeOfRecognitionDate.on("click", function() {
      $modalChangeOfRecognitionDate.modal("show");

      $btnConfirmChangeOfRecognitionDate.on("click", function() {
        $btnConfirmChangeOfRecognitionDate.prop("disabled", true);

          $.ajax({
          url: urlChangeRecognitionDate,
          method: 'POST',
          dataType: 'json',
          data: { 
            member_id: memberId,
            new_mii_recognition_date: $newMiiRecognitionDate.val(),
          },
          success: function(response) {
            toastr.success("Successfully changed recognition date of member");
            $message.html("Successfully changed recognition date of member");
            window.location = "/members/" + memberId;
          },
          error: function(response) {
            console.log(response.responseText);
            toastr.error('Error changing recognition date of member');
            $message.html(
              Mustache.render(
                $errorsTemplate.html(), 
                JSON.parse(response.responseText)
              )
            );
            $btnConfirmInsuranceResign.prop("disabled", false);
          }
        });

      });
    });

    $btnResignFromInsurance.on("click", function() {
      $modalResignFromInsurance.modal("show");

      $btnConfirmInsuranceResign.on("click", function() {
        $btnConfirmInsuranceResign.prop("disabled", true);

          $.ajax({
          url: urlResignFromInsurance,
          method: 'POST',
          dataType: 'json',
          data: { 
            member_id: memberId,
            date_resigned: $inputDateResigned.val(),
            reason: $inputReason.val()
          },
          success: function(response) {
            toastr.success("resigned member");
            $message.html("Successfully resigned member");
            window.location = "/members/" + memberId;
          },
          error: function(response) {
            console.log(response.responseText);
            toastr.error('Error in resigning member');
            $message.html(
              Mustache.render(
                $errorsTemplate.html(), 
                JSON.parse(response.responseText)
              )
            );
            $btnConfirmInsuranceResign.prop("disabled", false);
          }
        });

      });
    });


    $btnBalikKasapiMii.on("click", function() {
      $modalBalikKasapiMii.modal("show");

      $btnConfirmBalikKasapiMii.on("click", function() {
        $btnConfirmBalikKasapiMii.prop("disabled", true);

        // var data = {
        //   date_resigned: $inputDateResigned.val(),
        //   reason: $inputReason.val()
        // };

          $.ajax({
          url: urlBalikKasapiMii,
          method: 'POST',
          dataType: 'json',
          data: { 
            member_id: memberId,
            new_recognition_date: $newRecognitionDate.val(),
          },
          success: function(response) {
            toastr.success("Member balik kasapi");
            $message.html("Member successfully balik kasapi");
            window.location = "/members/" + memberId;
          },
          error: function(response) {
            console.log(response.responseText);
            toastr.error('Error in balik kasapi');
            $message.html(
              Mustache.render(
                $errorsTemplate.html(), 
                JSON.parse(response.responseText)
              )
            );
            $btnConfirmBalikKasapiMii.prop("disabled", false);
          }
        });

      });  
    });
  };

  var init  = function() {
    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
