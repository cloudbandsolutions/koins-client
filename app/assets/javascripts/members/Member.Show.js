//= require_directory ./lib

Member.RevertResignation = (function() {
  var $btnRevertResignation                 = $("#btn-revert-resignation");
  var $btnConfirmRevertResignation          = $("#btn-confirm-revert-resignation");
  var $modalConfirmRevertResignation        = $("#modal-revert-resignation");

  var $errorsTemplate           = $("#errors-template");
  var $parameters               = $("#parameters");
  var memberId                  = $parameters.data('member-id');

  var urlRevertResignation  = "/api/v1/members/resignation/revert_resignation";

  var _bindEvents = function() {
    $btnRevertResignation.on('click', function() {
      $modalConfirmRevertResignation.modal("show");
    });

    $btnConfirmRevertResignation.on('click', function() {
      $btnConfirmRevertResignation.prop('disabled', true);

      $.ajax({
        url: urlRevertResignation,
        method: 'POST',
        dataType: 'json',
        data: {
          member_id: memberId,
        },
        success: function(response) {
          toastr.success('Successfully reverted member status')
          window.location = "/members/" + memberId;
        },
        error: function(response) {
          $btnConfirmRevertResignation.prop('disabled', false);
          toastr.error('Error in reverting resignation');
        }
      });
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();

Member.Actions = (function() {
  var $toggleIsEditable   = $("#toggle-is-editable");
  var $parameters         = $("#parameters");
  var memberId            = $parameters.data("member-id");
  var currentState        = $parameters.data("is-editable");

  var $modalRequestTransfer       = $("#modal-request-transfer");
  var $modalCancelTransfer        = $("#modal-cancel-transfer");
  var $btnConfirmRequestTransfer  = $("#btn-confirm-request-transfer");
  var $btnRequestTransfer         = $("#btn-request-transfer");
  var $btnCancelRequestTransfer   = $("#btn-cancel-request-transfer");
  var $btnCancelTransfer          = $("#btn-cancel-transfer");
  var $btnConfirmCancelTransfer   = $("#btn-confirm-cancel-transfer");

  var urlUpdateEditable         = "/api/v1/members/state/update_editable";
  var urlRequestTransfer        = "/api/v1/members/request_transfer";
  var urlCancelRequestTransfer  = "/api/v1/members/cancel_request_transfer";

  var init = function() {
    $toggleIsEditable.bootstrapSwitch('state', currentState);

    $toggleIsEditable.on('switchChange.bootstrapSwitch', function(event, state) {
      $.ajax({
        url: urlUpdateEditable,
        method: 'POST',
        data: {
          member_id: memberId,
          state: $.parseJSON(state)
        },
        dataType: 'json',
        success: function(response) {
          toastr.success(response.message);
        },
        error: function(response) {
          $toggleIsEditable.bootstrapSwitch('state', currentState);
          toastr.error(JSON.parse(response.responseText).message);
        }
      });
    });

    $btnRequestTransfer.on("click", function() {
      $modalRequestTransfer.modal("show");
    });

    $btnCancelTransfer.on("click", function() {
      $modalCancelTransfer.modal("show");
    });

    $btnConfirmCancelTransfer.on("click", function() {
      $btnConfirmCancelTransfer.prop("disabled", true);

      $.ajax({
        url: urlCancelRequestTransfer,
        method: 'POST',
        data: {
          member_id: memberId
        },
        dataType: 'json',
        success: function(response) {
          toastr.success('Successfully cancelled transfer');
          window.location.href = "/members/" +  memberId;
        },
        error: function(response) {
          $.each(JSON.parse(response.responseText).errors, function() {
            toastr.error(this);
            $btnConfirmCancelTransfer.prop("disabled", false);
          });
        }
      });
    });

    $btnConfirmRequestTransfer.on("click", function() {
      $btnConfirmRequestTransfer.prop("disabled", true);
      $btnCancelRequestTransfer.prop("disabled", true);

      $.ajax({
        url: urlRequestTransfer,
        method: 'POST',
        data: {
          member_id: memberId
        },
        dataType: 'json',
        success: function(response) {
          toastr.success('Successfully requested transfer');
          window.location.href = "/member_transfer_requests/" +  response.id;
        },
        error: function(response) {
          $.each(JSON.parse(response.responseText).errors, function() {
            toastr.error(this);
            $btnConfirmRequestTransfer.prop("disabled", false);
            $btnCancelRequestTransfer.prop("disabled", false);
          });
        }
      });
    });
  };

  return {
    init: init
  };
})();

Member.Resign = (function() {
  var $btnResign                = $("#btn-resign");
  var $btnConfirmResignation    = $("#btn-confirm-resignation");
  var $btnCancelResignation     = $("#btn-cancel-resignation");
  var $modalResign              = $("#modal-resign");
  var $modalResignSelectStatus  = $("#modal-resign-select-status");
  var $modalResignSelectParticular = $("#modal-resign-select-particular");
  var $modalResignParticularSection = $("#modal-resign-particular-section");
  var $modalResignInputParticular = $("#modal-resign-input-particular");
  var $modalResignDateResigned  = $("#modal-resign-date-resigned");
  var $modalResignMessage       = $(".modal-resign").find(".message");
  var $errorsTemplate           = $("#errors-template");
  var $parameters               = $("#parameters");
  var memberId                  = $parameters.data('member-id');

  //var urlResign                       = "/api/v1/members/resignation/flag_for_resignation";
  var urlResign                       = "/api/v1/members/resignation/resign";
  var urlResignationParticulars       = "/api/v1/utils/member_resignation_type_particulars";
  var urlResignationParticularByCode  = "/api/v1/utils/member_resignation_particular_by_code";

  var _loadParticularByCode = function(resignationCode, resignationType) {
    $.ajax({
      url: urlResignationParticularByCode,
      data: { 
        resignation_type: resignationType,
        resignation_code: resignationCode,
      },
      dataType: 'json',
      method: 'GET',
      success: function(response) {
        $modalResignParticularSection.html(response.particular); 
      },
      error: function(response) {
        toastr.error('Error loading particular by code');
      }
    });
  };

  var _loadDefaultResignationParticulars = function() {
    var resignationType = $modalResignSelectStatus.val();

    $.ajax({
      url: urlResignationParticulars,
      data: { resignation_type: resignationType },
      dataType: 'json',
      method: 'GET',
      success: function(response) {
        populateSelectNormalWithHash($modalResignSelectParticular, response.particulars, 'code', 'code'); 
      },
      error: function(response) {
        toastr.error('Error in loading particulars');
      }
    });
  };

  var _bindEvents = function() {
    _loadDefaultResignationParticulars();
    _loadParticularByCode('A', $modalResignSelectStatus.val());

    $modalResignSelectParticular.on('change', function() {
      _loadParticularByCode($modalResignSelectParticular.val(), $modalResignSelectStatus.val());
    });

    $modalResignSelectStatus.on('change', function() {
      _loadDefaultResignationParticulars();
      _loadParticularByCode('A', $modalResignSelectStatus.val());
    });

    $btnResign.on('click', function() {
      $modalResign.modal("show");
    });

    $btnConfirmResignation.on('click', function() {
      $btnConfirmResignation.prop("disabled", true);

      $.ajax({
        url: urlResign,
        method: 'POST',
        dataType: 'json',
        data: { 
          member_id: memberId,
          date_resigned: $modalResignDateResigned.val(),
          resignation_type: $modalResignSelectStatus.val(),
          resignation_code: $modalResignSelectParticular.val(),
          reason: $modalResignParticularSection.html(),
          particular: $modalResignInputParticular.val()
        },
        success: function(response) {
          toastr.success("resigned member");
          $modalResignMessage.html("Successfully resigned member");
          window.location = "/members/" + memberId;
        },
        error: function(response) {
          console.log(response.responseText);
          toastr.error('Error in resigning member');
          $modalResignMessage.html(
            Mustache.render(
              $errorsTemplate.html(), 
              JSON.parse(response.responseText)
            )
          );
          $btnConfirmResignation.prop("disabled", false);
        }
      });
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
