//= require_directory ./lib

Member.Resign = (function() {
  var $btnResign                = $("#btn-resign");
  var $btnConfirmResignation    = $("#btn-confirm-resignation");
  var $btnCancelResignation     = $("#btn-cancel-resignation");
  var $modalResign              = $(".modal-resign").remodal({hashTracking: true, closeOnOutsideClick: false});
  var $modalResignSelectStatus  = $("#modal-resign-select-status");
  var $modalResignSelectParticular = $("#modal-resign-select-particular");
  var $modalResignParticularSection = $("#modal-resign-particular-section");
  var $modalResignInputParticular = $("#modal-resign-input-particular");
  var $modalResignDateResigned  = $("#modal-resign-date-resigned");
  var $modalResignMessage       = $("#modal-resign").find(".message");
  var $errorsTemplate           = $("#errors-template");
  var $parameters               = $("#parameters");
  var memberId                  = $parameters.data('member-id');

  //var urlResign                       = "/api/v1/members/resignation/flag_for_resignation";
  var urlResign                       = "/api/v1/members/resignation/resign";
  var urlResignationParticulars       = "/api/v1/utils/member_resignation_type_particulars";
  var urlResignationParticularByCode  = "/api/v1/utils/member_resignation_particular_by_code";

  var _loadParticularByCode = function(resignationCode, resignationType) {
    $.ajax({
      url: urlResignationParticularByCode,
      data: { 
        resignation_type: resignationType,
        resignation_code: resignationCode,
      },
      dataType: 'json',
      method: 'GET',
      success: function(response) {
        $modalResignParticularSection.html(response.particular); 
      },
      error: function(response) {
        //toastr.error('Error loading particular by code');
        console.log('Error loading particular by code');
      }
    });
  };

  var _loadDefaultResignationParticulars = function() {
    var resignationType = $modalResignSelectStatus.val();

    $.ajax({
      url: urlResignationParticulars,
      data: { resignation_type: resignationType },
      dataType: 'json',
      method: 'GET',
      success: function(response) {
        populateSelectNormalWithHash($modalResignSelectParticular, response.particulars, 'code', 'code'); 
      },
      error: function(response) {
        console.log("error in loading particulars");
        //toastr.error('Error in loading particulars');
      }
    });
  };

  var _bindEvents = function() {
    _loadDefaultResignationParticulars();
    _loadParticularByCode('A', $modalResignSelectStatus.val());

    $modalResignSelectParticular.on('change', function() {
      _loadParticularByCode($modalResignSelectParticular.val(), $modalResignSelectStatus.val());
    });

    $modalResignSelectStatus.on('change', function() {
      _loadDefaultResignationParticulars();
      _loadParticularByCode('A', $modalResignSelectStatus.val());
    });

    $btnResign.on('click', function() {
      $modalResign.open();
    });

    $btnCancelResignation.on('click', function() {
      $modalResign.close();
    });

    $btnConfirmResignation.on('click', function() {
      $.ajax({
        url: urlResign,
        method: 'POST',
        dataType: 'json',
        data: { 
          member_id: memberId,
          date_resigned: $modalResignDateResigned.val(),
          resignation_type: $modalResignSelectStatus.val(),
          resignation_code: $modalResignSelectParticular.val(),
          reason: $modalResignParticularSection.html(),
          particular: $modalResignInputParticular.val()
        },
        success: function(response) {
          toastr.success('Successfully resgned member')
          $modalResignMessage.html("Success! Redirecting...");
          window.location = "/members/" + memberId;
        },
        error: function(response) {
          toastr.error('Error in resigning member');
          $modalResignMessage.html(Mustache.render($errorsTemplate.html(), JSON.parse(response.responseText)));
        }
      });
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
