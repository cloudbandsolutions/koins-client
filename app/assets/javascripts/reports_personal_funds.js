var personalFunds = (function() {
  var $searchBtn;
  var $printBtn;
  var $downloadBtn;
  var $branchSelect;
  var $soSelect;
  var $centerSelect;
  var $personalFundsSection;
  var $personalFundsTemplate;
  var $personalFundsDetailedTemplate;
  var $accountTypeSelect;
  var $detailedCheckBox;
  var accountClass;
  var $data;
  var savingsTypes;
  var insuranceTypes;
  var equityTypes;
  var branchId;
  var centerId;
  var accountType;
  var detailed;
  var so;
  var $asOf             = $("#as-of");
  var personalFundsUrl  = "/api/v1/reports/personal_funds";
  var personalFundsPdfUrl  = "/reports/personal_funds_pdf";

  var _cacheDom = function() {
    $searchBtn = $("#search-btn");
    $printBtn = $("#print-btn");
    $downloadBtn = $("#download-btn");
    $branchSelect = $("#branch-select");
    $soSelect = $("#so-select");
    $centerSelect = $("#center-select");
    $detailedCheckBox = $("#detailed");
    $personalFundsSection = $("#reports-accounts-section");
    $personalFundsTemplate = $("#reports-accounts-template").html();
    $personalFundsDetailedTemplate = $("#reports-accounts-detailed-template").html();
    $accountTypeSelect = $("#account-type-select");
  }

  var _loadDefaults = function() {
  }

  var _bindEvents = function() {
    $branchSelect.on('change', function() {
      // empty so select
      populateSelect($centerSelect, [], "id", "name");

      var url = "/api/v1/branches/so_list";
      branchId = $branchSelect.val();

      if(branchId) {
        $.ajax({
          url: url,
          data: { branch_id: branchId },
          dataType: 'json',
          success: function(data) {
            toastr.info("Loading SOs...");
            populateSelect($soSelect, data.so_list, "name", "name");
          },
          error: function(data) {
            toastr.error("Error in loading SOs.");
          }
        });
      } else {
        populateSelect($soSelect, [], "name", "name");
      }
    });

    $soSelect.on('change', function() {
      var url = "/api/v1/branches/centers_by_so";
      so = $soSelect.val();

      $.ajax({
        url: url,
        data: { so: so },
        dataType: 'json',
        success: function(data) {
          toastr.info("Loading Centers...");
          populateSelect($centerSelect, data.centers, "id", "name");
        },
        error: function(data) {
          toastr.error("Error in loading centers.");
        }
      });
    });

    $downloadBtn.on('click', function() {
      $downloadBtn.addClass('loading');

      branchId = $branchSelect.val();
      centerId = $centerSelect.val();
      so = $soSelect.val();
      detailed = $detailedCheckBox.bootstrapSwitch('state');
      var accountTypes = $accountTypeSelect.val();
      var asOf  = $asOf.val();

      var params = {
        branch_id: branchId,
        center_id: centerId,
        so: so,
        account_types: accountTypes,
        detailed: detailed,
        as_of: asOf
      };

      $.ajax({
        url: personalFundsUrl,
        method: 'GET',
        dataType: 'json',
        data: params,
        success: function(data) {
          console.log(data);
          if(detailed) {
            $personalFundsSection.html(Mustache.render($personalFundsDetailedTemplate, data));
          } else {
            $personalFundsSection.html(Mustache.render($personalFundsTemplate, data));
          }
          $downloadBtn.removeClass('loading');

          tempUrl = data.download_url;
          window.open(tempUrl, '_blank');

          // Make sticky
          $(".sticky").stickyTableHeaders();
        },
        error: function(data) {
          toastr.error("Error in generating report for accounts");
          $downloadBtn.removeClass('loading');
        }
      });
    });
  //ariel
    $printBtn.on('click', function() {
      $searchBtn.addClass('loading');

      branchId = $branchSelect.val();
      centerId = $centerSelect.val();
      so = $soSelect.val();
      detailed = $detailedCheckBox.bootstrapSwitch('state');
      var accountTypes = $accountTypeSelect.val();
      var asOf  = $asOf.val();

      var params = {
        branch_id: branchId,
        center_id: centerId,
        so: so,
        account_types: accountTypes,
        detailed: detailed,
        as_of: asOf,
      };
      window.open("/reports/personal_funds_pdf?" + encodeQueryData(params), "_blank");
      /*
      $.ajax({
        url: personalFundsPdfUrl,
        method: 'GET',
        dataType: 'json',
        data: params,
        success: function(data) {
          console.log(data);
          window.location.href = "/reports/personal_funds_pdf?" + encodeQueryData(params);

          $personalFundsSection.find(".curr").each(function() {
            $(this).html(numberWithCommas($(this).html()));
          });

          toastr.info("Generating report for accounts");
          $searchBtn.removeClass('loading');

          // Make sticky
          $(".sticky").stickyTableHeaders();
        },
        error: function(data) {
          toastr.error("Error in generating report for accounts");
          $searchBtn.removeClass('loading');
        }
      });
      */
    });
  //ariel
    $searchBtn.on('click', function() {
      $searchBtn.addClass('loading');

      branchId = $branchSelect.val();
      centerId = $centerSelect.val();
      so = $soSelect.val();
      detailed = $detailedCheckBox.bootstrapSwitch('state');
      var accountTypes = $accountTypeSelect.val();
      var asOf  = $asOf.val();

      var params = {
        branch_id: branchId,
        center_id: centerId,
        so: so,
        account_types: accountTypes,
        detailed: detailed,
        as_of: asOf,
      };

      $.ajax({
        url: personalFundsUrl,
        method: 'GET',
        dataType: 'json',
        data: params,
        success: function(data) {
          console.log(data);
          if(detailed) {
            $personalFundsSection.html(Mustache.render($personalFundsDetailedTemplate, data));
          } else {
            $personalFundsSection.html(Mustache.render($personalFundsTemplate, data));
          }

          $personalFundsSection.find(".curr").each(function() {
            $(this).html(numberWithCommas($(this).html()));
          });

          toastr.info("Generating report for accounts");
          $searchBtn.removeClass('loading');

          // Make sticky
          $(".sticky").stickyTableHeaders();
        },
        error: function(data) {
          toastr.error("Error in generating report for accounts");
          $searchBtn.removeClass('loading');
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _loadDefaults();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  personalFunds.init();
});
