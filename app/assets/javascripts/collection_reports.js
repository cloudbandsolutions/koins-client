var collectionReports = (function() {
  var $searchBtn;
  var $downloadBtn;
  var $collectionReportsSection;
  var $collectionReportsTemplate;
  var branchId;
  var $branchSelect;
  var accountClass;
  var $data;
  var $startDate             = $("#start-date");
  var $endDate             = $("#end-date");
  var collectionReportsUrl  = "/api/v1/reports/collection_reports";

  var _cacheDom = function() {
    $searchBtn = $("#search-btn");
    $downloadBtn = $("#download-btn");
    $collectionReportsSection = $("#reports-collections-section");
    $collectionReportsTemplate = $("#reports-collections-template").html();
    $branchSelect = $("#branch-select");
  }

  var _loadDefaults = function() {
  }

  var _bindEvents = function() {

    $downloadBtn.on('click', function() {
      $downloadBtn.addClass('loading');

      branchId = $branchSelect.val();
      var startDate  = $startDate.val();
      var endDate = $endDate.val();

      var params = {
        branch_id: branchId,
        start_date: startDate,
        end_date: endDate
      };

      $.ajax({
        url: collectionReportsUrl,
        method: 'GET',
        dataType: 'json',
        data: params,
        success: function(data) {
          console.log(data);
          $collectionReportsSection.html(Mustache.render($collectionReportsTemplate, data));
          
          $downloadBtn.removeClass('loading');

          tempUrl = data.download_url;
          window.open(tempUrl, '_blank');

          // Make sticky
          $(".sticky").stickyTableHeaders();
        },
        error: function(data) {
          toastr.error("Error in generating report for collection");
          $downloadBtn.removeClass('loading');
        }
      });
    });

    $searchBtn.on('click', function() {
      $searchBtn.addClass('loading');  
      
      branchId = $branchSelect.val();
      var startDate  = $startDate.val();
      var endDate = $endDate.val();

      var params = {
        branch_id: branchId,
        start_date: startDate,
        end_date: endDate
      };

      $.ajax({
        url: collectionReportsUrl,
        method: 'GET',
        dataType: 'json',
        data: params,
        success: function(data) {
          console.log(data);
          $collectionReportsSection.html(Mustache.render($collectionReportsTemplate, data));

          $collectionReportsSection.find(".curr").each(function() {
            $(this).html(numberWithCommas($(this).html()));
          });

          toastr.info("Generating report for collection");
          $searchBtn.removeClass('loading');

          // Make sticky
          $(".sticky").stickyTableHeaders();
        },
        error: function(data) {
          toastr.error("Error in generating report for collection");
          $searchBtn.removeClass('loading');
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _loadDefaults();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  collectionReports.init();
});
