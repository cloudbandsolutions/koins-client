var loansIndex = (function() {
  var searchUrl = "/api/v1/loans/by_batch_transaction_reference_number"
  var $batchSearchBtn;
  var $batchPrintBtn;
  var branchId;
  var batchTransactionReferenceNumber;
  var $branchSelect;
  var batchTransactionLoansTemplate;
  var $batchTransactionLoansTable;
  var $parameters;
  var refNums; 

  var $clickableRow; 

  var _cacheDom = function() {
    $clickableRow  = $(".clickable");
  }

  var _bindEvents = function() {
    $clickableRow.on('click', function() {
      var loanId = $(this).data('loan-id');
      window.location = "/loans/" + loanId;
    });

  }


  var init = function() {
    $parameters = $("#parameters");
    refNums = $parameters.data('ref-nums');

    $batchTransactionLoansTable = $("#batch-table");

    batchTransactionLoansTemplate = $("#batch-table-tbody-template").html();

    $branchSelect = $(".batch-branch-select");

    $batchSearchBtn = $("#batch-search-btn");
    $batchSearchBtn.on('click', function() {
      branchId = $branchSelect.val();
      batchTransactionReferenceNumber = $("#batch-transaction-reference-number").val();

      var params = {
        branch_id: branchId,
        batch_transaction_reference_number: batchTransactionReferenceNumber
      };

      $.ajax({
        url: searchUrl,
        dataType: 'JSON',
        data: params,
        method: 'POST',
        success: function(data) {
          var rendered = Mustache.render(batchTransactionLoansTemplate, { loans: data.loans });
          $batchTransactionLoansTable.find("tbody").html(rendered);
          toastr.success("Displaying loans by batch number: " + batchTransactionReferenceNumber);
        },
        error: function(data) {
          toastr.error("Something went wrong while getting loans by batch number: " + batchTransactionReferenceNumber);
        }
      });
    });

    $batchPrintBtn = $("#batch-print-btn");
    $batchPrintBtn.on('click', function() {
      toastr.info("Feature not yet implemented");
    });

    $("#batch-transaction-reference-number").autocomplete({
      source: refNums
    });
    $("#batch-transaction-reference-number").autocomplete( "option", "appendTo", ".form" );

    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  console.log($('#center-select').val());

  /*
   * Get members based on selected center
   */
  $('#modal-branch-select').on('change', function() {
    var branchSelect = $(this);
    var centerSelect = $("#modal-center-select");
    $.ajax({
      url: '/api/v1/utils/centers' ,
      type: 'GET',
      dataType: 'json',
      data: { branch_id: branchSelect.val() },
      success: function(response) {
        var centers = response.data.centers;
        populateSelect(centerSelect, centers, 'id', 'name');
      },
      error: function() {
        toastr.error('Error in loading centers');
      }
    });
  });

  $('#modal-center-select').on('change', function() {
    var centerSelect = $(this);
    var memberSelect = $('#modal-member-select');
    var centerId = $(this).val();  
    var url = "/api/v1/get_members?center_id=" + centerId;

    $.ajax({
      url: url,
      type: 'GET',
      success: function(response) {
        console.log(response);
        populateSelect(memberSelect, response.data.members, 'id', 'name');
      },
      error: function() {
        toastr.error('Error in loading members');
      }
    });
  });

  loansIndex.init();
});
