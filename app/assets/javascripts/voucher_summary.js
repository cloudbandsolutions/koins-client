var voucherSummary = (function() {
  var $generateBtn;
  var startDate;
  var $startDateInput;
  var endDate;
  var $endDateInput;
  var $voucherSummaryTemplate;
  var $branchSelect;
  var branchId;
  var $voucherSummarySection;
  var $loader;
  var $downloadPdfBtn;
  var $book;
  var $downloadExcelBtn;
  var $printBtn;

  var urlEntries = "/voucher_summary/entries";
  var urlGenerateDownloadPdf = "/voucher_summary/generate_download_pdf_url";
  var urlDownloadExcel = "/voucher_summary/generate_download_excel_url";

  var _cacheDom = function() {
    $downloadPdfBtn         = $("#download-pdf-btn");
    $generateBtn            = $("#generate-btn");
    $startDateInput         = $("#start-date");
    $endDateInput           = $("#end-date");
    $branchSelect           = $("#branch-select");
    $book                   = $("#book");
    $voucherSummarySection  = $("#voucher-summary-section");
    $voucherSummaryTemplate = $("#voucher-summary-template").html();
    $loader = $("#loader");
    $downloadExcelBtn = $("#download-excel-btn");
    $printBtn               = $("#print-btn");
  }

  var _validateFilterInput = function() {
    startDate = $startDateInput.val();
    endDate = $endDateInput.val();

    if(!startDate || !endDate) {
      return false;
    } else {
      return true;
    }
  }

  var _renderTrialBalance = function(data) {
    $voucherSummarySection.html(Mustache.render($voucherSummaryTemplate, data));
  }

  var init = function() {
    _cacheDom();

    //ariel
    $printBtn.on('click', function() {
      branchId  = $branchSelect.val();
      startDate = $startDateInput.val();
      endDate   = $endDateInput.val();
      book      = $book.val();
      var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate,
          book: book
        };

      window.open("/voucher_summary/voucher_summary_pdf?" + encodeQueryData(params), "_blank");
    });
  //ariel


    $generateBtn.on('click', function() {
      $loader.addClass("active");
      if(_validateFilterInput()) {
        branchId  = $branchSelect.val();
        startDate = $startDateInput.val();
        endDate   = $endDateInput.val();
        book      = $book.val();

        var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate,
          book: book
        };

        console.log(params);
        $.ajax({
          url: urlEntries,
          data: params,
          dataType: 'json',
          method: 'GET',
          success: function(responseContent) {
            _renderTrialBalance(responseContent.data);
            toastr.success("Generated voucher summary");
            $loader.removeClass('active');
          },
          error: function(responseContent) {
            toastr.error("Something went wrong.");
            $loader.removeClass('active');
          }
        });
      } else {
        toastr.error("Start date and end date required");
        $loader.removeClass('active');
      }
    });

    $downloadPdfBtn.on('click', function() {
      $loader.addClass("active");
      if(_validateFilterInput()) {
        branchId = $branchSelect.val();
        startDate = $startDateInput.val();
        endDate = $endDateInput.val();

        var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate
        };

        console.log(params);
        $.ajax({
          url: urlGenerateDownloadPdf,
          data: params,
          dataType: 'json',
          method: 'GET',
          success: function(responseContent) {
            _renderTrialBalance(responseContent.data);
            toastr.success("Download voucher summary");
            $loader.removeClass('active');
            window.open(responseContent.download_url, '_blank');
          },
          error: function(responseContent) {
            toastr.error("Something went wrong.");
            $loader.removeClass('active');
          }
        });
      } else {
        toastr.error("Start date and end date required");
        $loader.removeClass('active');
      }
    });

    $downloadExcelBtn.on('click', function() {
      $loader.addClass("active");
      if(_validateFilterInput()) {
        branchId = $branchSelect.val();
        startDate = $startDateInput.val();
        endDate = $endDateInput.val();
        book      = $book.val();

        var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate,
          book: book
        };

        console.log(params);
        $.ajax({
          url: urlDownloadExcel,
          data: params,
          dataType: 'json',
          method: 'GET',
          success: function(responseContent) {
            //_renderTrialBalance(responseContent.data);
            toastr.success("Downloading Excel. Please wait.");
            $loader.removeClass('active');
            //window.open(responseContent.download_url, '_blank');
            window.location.href=responseContent.download_url;
          },
          error: function(responseContent) {
            toastr.error("Something went wwrong.");
            $loader.removeClass('active');
          }
        });
      } else {
        toastr.error("Start date and end date required");
        $loader.removeClass('active');
      }
    });
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  voucherSummary.init();
});

