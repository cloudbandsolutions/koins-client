var PaymentCollections = (function() {
  //this
  var $branchSelect;
  var $centerSelect;

  var $modalBranchSelect;
  var $modalCenterSelect;
  var $dateOfPayment;

  var $btnNewBilling;
  var $btnGenerateBilling;
  var $btnCancelBilling;

  var $modalErrorsGenerateBilling;
  var $modalSuccessGenerateBilling;
  var $modalControlsGenerateBilling;
  var $errorsTemplate;
  var $successTemplate;

  var $modalLoading;
  var $modalLoadingErrors;
  var $modalLoadingSuccess; 
  var $modalLoadingControls;
  var $modalLoadingBtnClose;

  var $clickableRow; 

  var urlUtilsBranches  = "/api/v1/utils/branches";
  var urlUtilsCenters   = "/api/v1/utils/centers";
  var urlNewBilling     = "/api/v1/payment_collections/generate_new_billing";

  var $filterOrNumber;
  var $filterStartDate;
  var $filterEndDate;
  var $filterBranchSelect;
  var $filterCenterSelect;
  var $filterStatus;
  var $btnFilter; 

  var _cacheDom = function() {
    //this
    $branchSelect        = $(".branch-select");
    $centerSelect        = $(".center-select");

    $modalBranchSelect            = $("#modal-branch-select");
    $modalCenterSelect            = $("#modal-center-select");
    $dateOfPayment                = $("#date-of-payment");
    $generateBillingModal         = $(".modal-generate-billing").remodal({ hashTracking: true, closeOnOutsideClick: false });

    $modalErrorsGenerateBilling   = $(".modal-generate-billing").find(".errors");
    $modalSuccessGenerateBilling  = $(".modal-generate-billing").find(".success");
    $modalControlsGenerateBilling = $(".modal-generate-billing").find(".controls");

    $btnNewBilling                = $("#btn-new-billing");
    $btnGenerateBilling           = $("#btn-generate-billing");
    $btnCancelBilling             = $("#btn-cancel-billing");

    $errorsTemplate               = $("#errors-template");
    $successTemplate              = $("#success-template");

    $clickableRow                 = $(".clickable");

    $filterOrNumber               = $('#filter-or-number');
    $filterStartDate              = $('#filter-start-date');
    $filterEndDate                = $('#filter-end-date');
    $filterBranchSelect           = $('#filter-branch-select');
    $filterCenterSelect           = $('#filter-center-select');
    $filterStatus                 = $('#filter-status');
    $btnFilter                    = $('#btn-filter');


    $modalLoading             = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingErrors       = $(".modal-loading").find(".errors");
    $modalLoadingSuccess      = $(".modal-loading").find(".success");
    $modalLoadingControls     = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
    $modalLoadingBtnClose     = $(".modal-loading").find(".btn-close");

  }

  var _addLoadingToConfirmationBtns = function() {
    $btnGenerateBilling.addClass('loading');
    $btnGenerateBilling.addClass('disabled');

    $btnCancelBilling.addClass('loading');
    $btnCancelBilling.addClass('disabled');
  }

  var _removeLoadingFromConfirmationBtns = function() {
    $btnGenerateBilling.removeClass('loading');
    $btnGenerateBilling.removeClass('disabled');

    $btnCancelBilling.removeClass('loading');
    $btnCancelBilling.removeClass('disabled');
  }

  var _bindEvents = function() {

    $branchSelect.bind('afterShow', function() {
      $.ajax({
        url: '/api/v1/utils/branches',
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect(this, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });

    // On change of branch select
    $branchSelect.bind('change', function() {
      var branchId = ($(this).val());  
      params = { branch_id: branchId };

      // Clear centers
      $centerSelect.find('option').remove();

      if(branchId) {
        $.ajax({
          url: '/api/v1/utils/centers' ,
          type: 'get',
          dataType: 'json',
          data: params,
          success: function(data) {
            var centers = data.data.centers;
            populateSelect($centerSelect, centers, 'id', 'name');

            var centerId = $("#parameters").data("center-id");
            if(centerId) {
              console.log("Setting center to " + centerId);
              $centerSelect.val(centerId).trigger("change");
            }
          },
          error: function() {
            toastr.error("ERROR: loading centers on branch select");
          }
        });
      }
    });

    // Preload centers
    var branchId = ($branchSelect.val());  
    params = { branch_id: branchId };

    if(branchId) {
      $.ajax({
        url: '/api/v1/utils/centers' ,
        type: 'get',
        dataType: 'json',
        data: params,
        success: function(data) {
          var centers = data.data.centers;
          populateSelect($centerSelect, centers, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading centers on branch select");
        }
      });
    }

    $modalLoadingBtnClose.on('click', function() {
      $modalLoading.close();
    });
    
    $btnFilter.on('click', function(){
      $modalLoadingControls.hide();

      var orNumber  = $filterOrNumber.val();
      var startDate = $filterStartDate.val();
      var endDate   = $filterEndDate.val();
      var branchId  = $filterBranchSelect.val();
      var centerId  = $filterCenterSelect.val();
      var tStatus   = $filterStatus.val();

      var data = {
        or_number: orNumber,
        start_date: startDate,
        end_date: endDate,
        branch_id: branchId,
        center_id: centerId,
        status: tStatus
      };

      $modalLoading.open();

      window.location = "/payment_collections?" + encodeQueryData(data);

    });

    $btnNewBilling.on('click', function() {
    $modalSuccessGenerateBilling.html("");
    $modalErrorsGenerateBilling.html("");
      $generateBillingModal.open();
    });

    $btnCancelBilling.on('click', function() {
      if(!$btnCancelBilling.hasClass('loading')) {
        $generateBillingModal.close();
      }
    });

    $btnGenerateBilling.on('click', function() {
      if(!$btnGenerateBilling.hasClass('loading')) {
        var branchId      = $modalBranchSelect.val();
        var centerId      = $modalCenterSelect.val();
        var dateOfPayment = $dateOfPayment.val();
        var data = {
          branch_id: branchId,
          center_id: centerId,
          date_of_payment: dateOfPayment
        };

        _addLoadingToConfirmationBtns();

        $.ajax({
          url: urlNewBilling,
          data: data,
          dataType: 'json',
          method: 'POST',
          success: function(responseContent) {
            $modalSuccessGenerateBilling.html(Mustache.render($successTemplate.html(), { messages: ["Successfully created new billing. Redirecting..."] }));
            _removeLoadingFromConfirmationBtns();
            $modalControlsGenerateBilling.hide();
            var pc_id = responseContent.payment_collection_id;
            window.location = "/payment_collections/" + pc_id + "/edit";
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            console.log(errorMessages);
            $modalErrorsGenerateBilling.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Error in creating billing");
            _removeLoadingFromConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $clickableRow.on('click', function() {
      paymentCollectionId = $(this).data('transaction-id');
      window.location = "/payment_collections/" + paymentCollectionId;
    });

    $modalBranchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($modalBranchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });

    $modalBranchSelect.bind('change', function() {
      var modalBranchId = ($(this).val());
      params = { branch_id: modalBranchId };

      $modalCenterSelect.find('option').remove();

      $.ajax({
        url: urlUtilsCenters,
        type: 'get',
        dataType: 'json',
        data: params,
        success: function(data) {
          var centers = data.data.centers;
          populateSelect($modalCenterSelect, centers, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading centers on branch select");
        }
      });
    });

    // Preload centers
    var modalBranchId = ($modalBranchSelect.val());
    params = { branch_id: modalBranchId };
    $.ajax({
      url: urlUtilsCenters,
      type: 'get',
      dataType: 'json',
      data: params,
      success: function(data) {
        var centers = data.data.centers;
        populateSelect($modalCenterSelect, centers, 'id', 'name');
      },
      error: function() {
        toastr.error("ERROR: loading centers on branch select");
      }
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();

    var branchId = $("#parameters").data('branch-id');

    if(branchId) {
      console.log("Setting branch to " + branchId);
      $branchSelect.val(branchId).trigger("change");
    }
  }

  return {
    init: init
  };
})();
