var PaymentCollectionForm = (function() {
  var $loanProductSections;
  var $parameters;
  var $savingsTypes;
  var $insuranceTypes;
  var $equityTypes;

  var $grandTotalLp;
  var $grandTotalSavings;
  var $grandTotalInsurance;
  var $grandTotalEquity;
  var $grandTotalWp;
  var $grandTotalCp;
  var $grandTotalGrandTotal;

  var _cacheDom = function() {
    $loanProductSections  = $(".loan-product-section");
    $parameters           = $("#parameters");
    $savingsTypes         = $parameters.data("savings-types");
    $insuranceTypes       = $parameters.data("insurance-types");
    $equityTypes          = $parameters.data("equity-types");
    $grandTotalLp         = $(".grand-total-loan-payment");
    $grandTotalSavings    = $(".grand-total-savings");
    $grandTotalInsurance  = $(".grand-total-insurance");
    $grandTotalEquity     = $(".grand-total-equity");
    $grandTotalWp         = $(".grand-total-wp");
    $grandTotalCp         = $(".grand-total-cp");
    $grandTotalGrandTotal = $(".grand-total-grand-total");
  }

  var _bindEvents = function() {
    _computeGrandTotalLp();

    $loanProductSections.each(function(e) {
      var $section            = $(this);
      var $grandTotal         = $(this).find(".grand-total");
      var $loanPayment        = $(this).find(".loan-payment");
      var $totalLoanPayments  = $(this).find(".total-loan-payments");
      var $savingsDeposits    = $(this).find(".savings-deposit");
      var $insuranceDeposits  = $(this).find(".insurance-deposit");
      var $equityDeposits     = $(this).find(".equity-deposit");
      var $wps                = $(this).find(".wp");
      var $totalWp            = $(this).find(".total-wp");

      _computeTotalLoanPayments($loanPayment, $totalLoanPayments);
      $loanPayment.on('change', function() {
        _computeTotalLoanPayments($loanPayment, $totalLoanPayments);
        _computeTotalCp($section);
        _computeGrandTotalLp();
      });

      $.each($savingsTypes, function() {
        var savingsType = this;
        var $totalSavings = $section.find(".total-savings[data-savings-type-code='" + savingsType + "']");
        _computeTotalSavings($section.find(".savings-deposit[data-account-code='" + savingsType + "']"), $totalSavings);
        _computeGrandTotalSavings();
      });

      $savingsDeposits.on('change', function() {
        var savingsType = $(this).data("account-code");
        var $totalSavings = $section.find(".total-savings[data-savings-type-code='" + savingsType + "']");
        _computeTotalSavings($section.find(".savings-deposit[data-account-code='" + savingsType + "']"), $totalSavings);
        _computeTotalCp($section);
        _computeGrandTotalSavings();
      });

      $.each($insuranceTypes, function() {
        var insuranceType = this;
        var $totalInsuranceDeposits = $section.find(".total-insurance[data-insurance-type-code='" + insuranceType + "']");
        _computeTotalInsuranceDeposits($section.find(".insurance-deposit[data-account-code='" + insuranceType + "']"), $totalInsuranceDeposits);
        _computeGrandTotalInsurance();
      });

      $insuranceDeposits.on('change', function() {
        var insuranceType = $(this).data("account-code");
        var $totalInsuranceDeposits = $section.find(".total-insurance[data-insurance-type-code='" + insuranceType + "']");
        _computeTotalInsuranceDeposits($section.find(".insurance-deposit[data-account-code='" + insuranceType + "']"), $totalInsuranceDeposits);
        _computeTotalCp($section);
        _computeGrandTotalInsurance();
      });

      $.each($equityTypes, function() {
        var equityType = this;
        var $totalEquityDeposits = $section.find(".total-equity[data-equity-type-code='" + equityType + "']");
        _computeTotalEquityDeposits($section.find(".equity-deposit[data-account-code='" + equityType + "']"), $totalEquityDeposits);
        _computeGrandTotalEquity();
      });

      $equityDeposits.on('change', function() {
        var equityType = $(this).data("account-code");
        var $totalEquityDeposits = $section.find(".total-equity[data-equity-type-code='" + equityType + "']");
        _computeTotalEquityDeposits($section.find(".equity-deposit[data-account-code='" + equityType + "']"), $totalEquityDeposits);
        _computeTotalCp($section);
        _computeGrandTotalEquity();
      });

      _computeTotalWps($wps, $totalWp);
      $wps.on('change', function() {
        _computeTotalWps($wps, $totalWp);
        _computeTotalCp($section);
        _computeGrandTotalWps();
      });

      _computeTotalCp($section);
    });
  }

  var _computeGrandTotalLp = function() {
    var t = 0.00;
    $.each($(".loan-payment"), function() {
      t += parseFloat($(this).val());
    });

    $grandTotalLp.val(t);
  }

  var _computeGrandTotalSavings = function() {
    $.each($savingsTypes, function() {
      var savingsType = this;
      var $totalSavings = $(".total-savings[data-savings-type-code='" + savingsType + "']");
      
      var t = 0.00;
      $.each($(".savings-deposit[data-account-code='" + savingsType + "']"), function() {
        t += parseFloat($(this).val());
      });

      $(".grand-total-savings[data-savings-type-code='" + savingsType + "']").val(t);
    });
  }

  var _computeGrandTotalInsurance = function() {
    $.each($insuranceTypes, function() {
      var insuranceType = this;
      var $totalInsuranceDeposits = $(".total-insurance[data-insurance-type-code='" + insuranceType + "']");

      var t = 0.00;
      $.each($(".insurance-deposit[data-account-code='" + insuranceType +"']"), function() {
        t += parseFloat($(this).val());
      });

      $(".grand-total-insurance[data-insurance-type-code='" + insuranceType + "']").val(t);
    });
  }

  var _computeGrandTotalEquity = function() {
    $.each($equityTypes, function() {
      var equityType = this;
      var $totalEquityDeposits = $(".total-equity[data-equity-type-code='" + equityType + "']");

      var t = 0.00;
      $.each($(".equity-deposit[data-account-code='" + equityType +"']"), function() {
        t += parseFloat($(this).val());
      });

      $(".grand-total-equity[data-equity-type-code='" + equityType + "']").val(t);
    });
  }

  var _computeGrandTotalWps = function() {
    var t = 0.00;

    $.each($(".wp"), function() {
      t += parseFloat($(this).val());
    });

    $(".grand-total-wp").val(t);
  }

  var _computeTotalCp = function($section) {
    var grandTotal = 0.00;
    $.each($section.find(".total-member-cp"), function() {
      var memberId = $(this).data("member-id");
      var t = 0.00;
      $.each($section.find(".cp-amount[data-member-id='" + memberId + "']"), function() {
        t += parseFloat($(this).val());
      });

      t -= parseFloat($section.find(".wp[data-member-id='" + memberId + "']").val());

      grandTotal += t;
      $(this).val(t);

      $section.find(".total-member-amount[data-member-id='" + memberId + "']").val(parseFloat($(this).val()) + parseFloat($section.find(".wp[data-member-id='" + memberId + "']").val()));
    });

    $section.find(".total-cp").val(grandTotal);

    $section.find(".grand-total").val(parseFloat($section.find(".total-wp").val()) + parseFloat($section.find(".total-cp").val()));

    _computeGrandTotalCp();
    _computeGrandTotalAmt();
  }

  var _computeTotalLoanPayments = function($loanPayment, $totalLoanPayments) {
    var t = 0.00;
    $loanPayment.each(function() {
      t += parseFloat($(this).val());
    });

    $totalLoanPayments.val(t);
  }

  var _computeTotalSavings = function($savingsDeposits, $totalSavings) {
    var t = 0.00;
    $savingsDeposits.each(function() {
      t += parseFloat($(this).val());
    });

    $totalSavings.val(t);
  }

  var _computeTotalInsuranceDeposits = function($insuranceDeposits, $totalInsuranceDeposits) {
    var t = 0.00;
    $insuranceDeposits.each(function() {
      t += parseFloat($(this).val());
    });

    $totalInsuranceDeposits.val(t);
  }

  var _computeTotalEquityDeposits = function($equityDeposits, $totalEquityDeposits) {
    var t = 0.00;
    $equityDeposits.each(function() {
      t += parseFloat($(this).val());
    });

    $totalEquityDeposits.val(t);
  }

  var _computeTotalWps = function($wps, $totalWp) {
    var t = 0.00;
    $wps.each(function() {
      t += parseFloat($(this).val());
    });

    $totalWp.val(t);
  }

  var _computeGrandTotalCp = function() {
    var t = 0.00;

    $.each($(".total-cp"), function() {
      t += parseFloat($(this).val());
    });

    $grandTotalCp.val(t);
  }

  var _computeGrandTotalAmt = function() {
    var t = 0.00;

    $.each($(".grand-total"), function() {
      t += parseFloat($(this).val());
    });

    $grandTotalGrandTotal.val(t);
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
    // Make sticky
    $(".sticky").stickyTableHeaders();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  PaymentCollectionForm.init();
});
