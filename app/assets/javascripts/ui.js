var UI = (function() {
  var $branchSelect;
  var $centerSelect;
  var $switchIsReinstate;
  var $oldRecognitionDate;

  var _cacheDom = function() {
    $branchSelect = $("#branch-select");
    $centerSelect = $("#center-select");
    $switchIsReinstate   = $("#switch-is-reinstate");
    $oldRecognitionDate    = $(".old-recognition-date");
  }

  var _bindEvents = function() {
    // for reinstate field
    var state = $switchIsReinstate.bootstrapSwitch('state');
    var checked = state;
    if (checked) {
      $oldRecognitionDate.show();
    }
    else {
      $oldRecognitionDate.hide();
    }  

    $switchIsReinstate.on('switchChange.bootstrapSwitch', function (event, state) {
      var checked = state;
      if (checked) {
          $oldRecognitionDate.show();
      }
      else {
          $oldRecognitionDate.hide();
      }
    }); 


    $branchSelect.bind('afterShow', function() {
      $.ajax({
        url: '/api/v1/utils/branches',
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect(this, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });

    // On change of branch select
    $branchSelect.bind('change', function() {
      var branchId = ($(this).val());  
      params = { branch_id: branchId };

      // Clear centers
      $centerSelect.find('option').remove();

      if(branchId) {
        $.ajax({
          url: '/api/v1/utils/centers' ,
          type: 'get',
          dataType: 'json',
          data: params,
          success: function(data) {
            var centers = data.data.centers;
            populateSelect($centerSelect, centers, 'id', 'name');

            var centerId = $("#parameters").data("center-id");
            if(centerId) {
              console.log("Setting center to " + centerId);
              $centerSelect.val(centerId).trigger("change");
            }
          },
          error: function() {
            toastr.error("ERROR: loading centers on branch select");
          }
        });
      }
    });

    // Preload centers
    var branchId = ($branchSelect.val());  
    params = { branch_id: branchId };

    if(branchId) {
      $.ajax({
        url: '/api/v1/utils/centers' ,
        type: 'get',
        dataType: 'json',
        data: params,
        success: function(data) {
          var centers = data.data.centers;
          populateSelect($centerSelect, centers, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading centers on branch select");
        }
      });
    }
  }
  
  var init = function() {
    _cacheDom();
    _bindEvents();
    var branchId = $("#parameters").data('branch-id');

    if(branchId) {
      console.log("Setting branch to " + branchId);
      $branchSelect.val(branchId).trigger("change");
    }
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  UI.init();

});
