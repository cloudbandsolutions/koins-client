var LoanInsuranceForm = (function() {
  var $parameters              = $("#parameters");
  var loanInsuranceId          = $parameters.data('loan-insurance-id');
  var $section                 = $(".transaction-table");
  var $btnDelete               = $(".btn-delete");
  var $modalLoading            = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $memberSelect            = $("#member-select");
  var $btnAddMember            = $("#btn-add-member");

  var $loanInsuranceTotal      = $(".loan-insurance-total");
  var $loanInsuranceAmount     = $(".loan-insurance-amount");
  var $loanAmount              = $(".loan-amount");
  var $totalAmount             = $(".total-amount");
  var $totalLoanAmount         = $(".total-loan-amount")
 
  // Hide controls
  $(".modal-loading").find('.controls').hide();

  var urlDeleteLoanInsuranceRecord      = "/api/v1/loan_insurances/delete_loan_insurance_record";
  var urlAddMember                      = "/api/v1/loan_insurances/add_member";

  var _bindEvents = function() {
    $btnAddMember.on('click', function() {
      $modalLoading.open();
      if(!$btnAddMember.hasClass('loading')) {
        $(this).addClass('loading');
        $(this).addClass('disabled');
        var memberId = $memberSelect.val();

        $.ajax({
          url: urlAddMember,
          method: 'POST',
          dataType: 'json',
          data: { id: loanInsuranceId, member_id: memberId },
          success: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            toastr.success("Successfully added member");
            window.location.href = "/loan_insurances/" + loanInsuranceId + "/edit";
          },
          error: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            toastr.error("Something went wrong when trying to add member");
            $modalLoading.close();
          }
        });
      } else {
        toastr.info("Still loading member");
      }
    });

    $btnDelete.on('click', function() {
      var $btn = $(this);
      $btn.addClass('loading');
      $btn.addClass('disabled');
      var loanInsuranceRecordId = $btn.data('loan-insurance-record-id');

      $modalLoading.open();

      $.ajax({
        url: urlDeleteLoanInsuranceRecord,
        method: 'POST',
        dataType: 'json',
        data: { loan_insurance_record_id: loanInsuranceRecordId },
        success: function(responseContent) {
          toastr.success("Successfully deleted record");
          window.location.href = "/loan_insurances/" + loanInsuranceId + "/edit";
        },
        error: function(responseContent) {
          toastr.error("Cannot delete this record");
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
          $modalLoading.close();
        }
      });
    });

    $loanInsuranceAmount.on('change', function() {
      _computeLoanInsuranceTotalAmt($section);
    });

    $loanAmount.on('change', function() {
      _computeLoanTotalAmt($section);
    });

    _computeLoanTotalAmt($section);
    _computeLoanInsuranceTotalAmt($section);
  }

  var _computeLoanInsuranceTotalAmt = function($section) {
    var loanInsuranceTotal = 0.00;

    $.each($section.find(".loan-insurance-amount"), function() {
      loanInsuranceTotal += parseFloat($(this).val());
    });

    $section.find(".loan-insurance-total").val(loanInsuranceTotal);
    $totalAmount.val(loanInsuranceTotal);
  }

  var _computeLoanTotalAmt = function($section) {
    var loanTotal = 0.00;

    $.each($section.find(".loan-amount"), function() {
      loanTotal += parseFloat($(this).val());
    });

    $section.find(".loan-total").val(loanTotal);
    $totalLoanAmount.val(loanTotal);
  }


  var init = function() {
    _bindEvents();
  }

  return {
    init: init
  };
})();
