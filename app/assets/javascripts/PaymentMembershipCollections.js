var PaymentMembershipCollections = (function() {
  var $modalBranchSelect;
  var $modalCenterSelect;
  var $dateOfPayment;
  var $btnGeneratePayment;

  var urlUtilsBranches  = "/api/v1/utils/branches";
  var urlUtilsCenters   = "/api/v1/utils/centers";
  var urlNewPayment     = "/payment_membership_collections/new";

  var _cacheDom = function() {
    $modalBranchSelect    = $("#modal-branch-select");
    $modalCenterSelect    = $("#modal-center-select");
    $btnGeneratePayment   = $("#btn-generate-payment");
    $dateOfPayment        = $("#date-of-payment");
  }

  var _bindEvents = function() {
    $btnGeneratePayment.on('click', function() {
      $(this).prop('disabled', true);
      $(this).addClass('loading');

      var branchId      = $modalBranchSelect.val();
      var centerId      = $modalCenterSelect.val();
      var dateOfPayment = $dateOfPayment.val();
      var data = {
        branch_id: branchId,
        center_id: centerId,
        date_of_payment: dateOfPayment
      };

      var urlRedirect = urlNewBilling + "?" + $.param(data);
      window.location.href = urlRedirect;
    });

    $modalBranchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($modalBranchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });

    $modalBranchSelect.bind('change', function() {
      var modalBranchId = ($(this).val());
      params = { branch_id: modalBranchId };

      $modalCenterSelect.find('option').remove();

      $.ajax({
        url: urlUtilsCenters,
        type: 'get',
        dataType: 'json',
        data: params,
        success: function(data) {
          var centers = data.data.centers;
          populateSelect($modalCenterSelect, centers, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading centers on branch select");
        }
      });
    });

    // Preload centers
    var modalBranchId = ($modalBranchSelect.val());
    params = { branch_id: modalBranchId };
    $.ajax({
      url: urlUtilsCenters,
      type: 'get',
      dataType: 'json',
      data: params,
      success: function(data) {
        var centers = data.data.centers;
        populateSelect($modalCenterSelect, centers, 'id', 'name');
      },
      error: function() {
        toastr.error("ERROR: loading centers on branch select");
      }
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  PaymentMembershipCollections.init();
});
