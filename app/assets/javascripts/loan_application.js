var LoanApplication = (function() {
  var $parameters                   = $("#parameters");
  var $overrideMaintainingBalance   = $("#override-maintaining-balance");
  var $overrideProcessingFee        = $("#override-processing-fee");
  var $overrideInstallmentInterval  = $("#override-installment-interval");
  var $installmentOverride          = $("#installment-override");
  var $numInstallments              = $("#num-installments");
  var $originalNumInstallments      = $("#original-num-installments");
  var $originalLoanAmount           = $("#original-loan-amount");
  var $oldPrincipalBalance          = $("#old-principal-balance");
  var $oldInterestBalance           = $("#old-interest-balance");
  var $orNumberInput                = $("#or-number-input");
  var $amount                       = $("#amount");
  var $modeOfPayment                = $("#mode-of-payment");
  var $selectBookType               = $("#select-book-type");
  var $standard                     = $(".standard");
  var $override                     = $(".override");
  var $parameters                   = $("#parameters");
  var $loanProductTypeSelect        = $("#loan-product-type-select");
  var $interestRateInput            = $("#interest-rate-input");
  var urlLoanProductType            = "/api/v1/utils/loan_product_types";

  var memberId;

  var _showStandard = function() {
    $standard.show();
    $override.hide();

    $installmentOverride.prop('disabled', true);
    $originalNumInstallments.prop('disabled', true);
    $originalLoanAmount.prop('disabled', true);
    $oldPrincipalBalance.prop('disabled', true);
    $oldInterestBalance.prop('disabled', true);

    $numInstallments.prop('disabled', false);
    $amount.prop('disabled', false);

    $selectBookType.val('CDB').trigger('change');
  };

  var _hideStandard = function() {
    $standard.hide();
    $override.show();

    $installmentOverride.prop('disabled', false);
    $originalNumInstallments.prop('disabled', false);
    $originalLoanAmount.prop('disabled', false);
    $oldPrincipalBalance.prop('disabled', false);
    $oldInterestBalance.prop('disabled', false);

    $numInstallments.prop('disabled', true);
    $amount.prop('disabled', true);

    $selectBookType.val('JVB').trigger('change');
  };

  var _toggleStandard = function(flag) {
    if(flag) {
      _showStandard();
    } else {
      _hideStandard();
    }
  };

  var _loadState = function() {
    var overrideState = $overrideInstallmentInterval.bootstrapSwitch('state');
    _toggleStandard(!overrideState);
  }

  var init = function() {
    _loadState();

    // Toggle for overrideInstallmentInterval
    $overrideInstallmentInterval.on('switchChange.bootstrapSwitch', function() {
      var overrideState = $overrideInstallmentInterval.bootstrapSwitch('state');
      _toggleStandard(!overrideState);
    });

    memberId = $parameters.data('member-id');
    _bindEvents();
  }

  var _bindEvents = function() {
    $loanProductTypeSelect.on('change', function() {
      $.ajax({
        method: 'GET',
        dataType: 'json',
        url: urlLoanProductType,
        data: {
          id: $loanProductTypeSelect.val()
        },
        success: function(response) {
          console.log(response);
          $interestRateInput.val(response.loan_product_type.interest_rate);
        },
        error: function(response) {
          console.log(response);
          toastr.error("Error in product type change");
        }
      });
    });

    $modeOfPayment.on('change', function() {
      var modeOfPayment = $modeOfPayment.val();
      if(modeOfPayment == "weekly") {
        populateSelectNormal($numInstallments, [15,25,35,50]);
        populateSelectNormal($originalNumInstallments, [15,25,35,50]);
      } else if(modeOfPayment == "monthly") {
        populateSelectNormal($numInstallments, [1,2,3,4,5,6,7,8,9,10,11,12]);
        populateSelectNormal($originalNumInstallments, [1,2,3,4,5,6,7,8,9,10,11,12]);
      } else if(modeOfPayment == "semi-monthly") {
        populateSelectNormal($numInstallments, [1,2,3,4,5,6,7,8,9,10,11,12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]);
        populateSelectNormal($originalNumInstallments, [1,2,3,4,5,6,7,8,9,10,11,12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]);
      }
    });
    //$modeOfPayment.val($parameters.data('num-installments')).trigger('change');
    var modeOfPayment = $modeOfPayment.val();
    if(modeOfPayment == "weekly") {
      populateSelectNormal($numInstallments, [15,25,35,50]);
      populateSelectNormal($originalNumInstallments, [15,25,35,50]);
    } else if(modeOfPayment == "monthly") {
      populateSelectNormal($numInstallments, [1,2,3,4,5,6,7,8,9,10,11,12]);
      populateSelectNormal($originalNumInstallments, [1,2,3,4,5,6,7,8,9,10,11,12]);
    } else if(modeOfPayment == "semi-monthly") {
      populateSelectNormal($numInstallments, [1,2,3,4,5,6,7,8,9,10,11,12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]);
      populateSelectNormal($originalNumInstallments, [1,2,3,4,5,6,7,8,9,10,11,12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]);
    }

    $originalNumInstallments.val($parameters.data('original-num-installments'));
    $numInstallments.val($parameters.data('num-installments'));
  }

  return {
    init: init
  };
})();

/*
 * Generate loan application voucher particular
 */
var generateLoanApplicationVoucherParticular = function() {
  var memberId = $("#memberId").val();
  var amountText = $("#amount");
  var amount = amountText.val();
  var loanProductId = $("#loanProductId").val();
  var clipNumberText = $("#clipNumberText");
  var clipNumber = clipNumberText.val();
  var checkNumber = $("#checkNumberText").val();
  var voucherCheckNumber = $("#voucherCheckNumberText").val();
  var voucherParticularText = $("#voucherParticularText");
  var orNumber = $("#loan_or_number").val();
  var url = '/api/v1/loans/loan_application_voucher_particular';

  data = {
    member_id: memberId,
    amount: amount,
    loan_product_id: loanProductId,
    check_number: checkNumber,
    voucher_check_number: voucherCheckNumber,
    clip_number: clipNumber,
    or_number: orNumber
    //or_number: orNumber
  }

  $.ajax({
    url: url,
    type: 'GET',
    data: data,
    success: function(response) {
      text = response.data.text;
      voucherParticularText.val(text);
    },
    failure: function() {
      alert('fail');
    }
  });
}

$(document).ready(function() {
  // Get project types if project type category is not blank
  var projectTypeSelect = $("#projectTypeSelect");
  var projectTypeCategoryId = $("#projectTypeCategorySelect").data("project-type-category-id");
  var projectTypeId = projectTypeSelect.data("project-type-id");

  if(projectTypeCategoryId) {
    var url = "/api/v1/project_types?project_type_category_id=" + projectTypeCategoryId;

    $.ajax({
      url: url,
      type: 'GET',
      success: function(response) {
        populateSelect(projectTypeSelect, response.data.project_types, 'id', 'name');
        projectTypeSelect.val(projectTypeId);
      },
      failure: function() {
        alert('fail');
      }
    });
  }


  /*
   * Change project type based on selected project type category
   */
  $("#projectTypeCategorySelect").on('change', function(evt) {
    console.log($(this).val());
    var projectTypeCategorySelect = $(this);
    var projectTypeSelect = $("#projectTypeSelect");
    var projectTypeCategoryId = $(this).val();
    var url = "/api/v1/project_types?project_type_category_id=" + projectTypeCategoryId;

    $.ajax({
      url: url,
      type: 'GET',
      success: function(response) {
        populateSelect(projectTypeSelect, response.data.project_types, 'id', 'name');
      },
      failure: function() {
        alert('fail');
      }
    });
  });

  $("#checkNumberText, #voucherCheckNumberText, #clipNumberText").on('focusout', function(evt) {
    generateLoanApplicationVoucherParticular();
  });

  /*
   * Get members based on selected center
   */
  $('#center-select').on('change', function() {
    var centerSelect = $(this);
    var memberSelect = $('#member-select');
    var centerId = $(this).val();  
    var url = "/api/v1/members?center_id=" + centerId;

    $.ajax({
      url: url,
      type: 'GET',
      success: function(response) {
        console.log(response);
        populateSelect(memberSelect, response.data.members, 'id', 'name');
      },
      failure: function() {
        alert('fail');
      }
    });
  });

  /*
   * Get members based on selected center
   */
  $('#loan-branch-select').on('change', function() {
    var branchSelect = $(this);
    var centerSelect = $('#loan-center-select');
    var branchId = $(this).val();  
    var url = "/api/v1/utils/centers";

    var params = { branch_id: branchId }

    $.ajax({
      url: url,
      type: 'GET',
      dataType: 'json',
      data: params,
      success: function(response) {
        console.log(response);
        populateSelect(centerSelect, response.data.centers, 'id', 'name');
      },
      failure: function() {
        toastr.error("ERROR: loading centers on branch select");
      }
    });
  });

  LoanApplication.init();
});
