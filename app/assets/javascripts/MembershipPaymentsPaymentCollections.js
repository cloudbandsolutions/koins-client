var MembershipPaymentsPaymentCollections = (function() {
  var $modalBranchSelect;
  var $dateOfPayment;
  var $btnGenerateBilling;
  var $btnCancelGenerateBilling;
  var $btnFilter;
  var $btnNewTransaction;
  var $generateBillingModal;

  var $filterOrNumber;
  var $filterStartDate;
  var $filterEndDate;
  var $filterBranchSelect;
  var $filterStatus;

  var $errors;
  var $errorsTemplate;
  var $modalErrors;
  var $modalSuccess;
  var $modalControls;
  var $successTemplate;
  var $clickableRow;

  var $modalLoading;
  var $modalLoadingErrors;
  var $modalLoadingSuccess;
  var $modalLoadingControls;
  var $modalLoadingBtnClose;

  var urlUtilsBranches  = "/api/v1/utils/branches";
  var urlUtilsCenters   = "/api/v1/utils/centers";
  var urlGenerateMembershipPaymentTransaction = "/api/v1/membership_payments/payment_collections/generate_transaction";
  var urlNewBilling     = "/membership_payments/payment_collections/new";

  var _cacheDom = function() {
    $modalBranchSelect    = $("#modal-branch-select");
    $btnGenerateBilling   = $("#btn-generate-billing");
    $dateOfPayment        = $("#date-of-payment");
    $btnFilter            = $("#btn-filter");
    $btnNewTransaction    = $("#btn-new-transaction");
    $btnCancelGenerateBilling = $("#btn-cancel-generate-billing");
    $modalErrors              = $(".modal-generate-membership-payment-collection").find(".errors");
    $errors                   = $(".errors");
    $modalSuccess             = $(".modal-generate-membership-payment-collection").find(".success");
    $modalControls            = $(".modal-generate-membership-payment-collection").find(".controls");
    $errorsTemplate           = $("#errors-template");
    $successTemplate          = $("#success-template");
    $generateBillingModal     = $(".modal-generate-membership-payment-collection").remodal({ hashTracking: true, closeOnOutsideClick: false });

    $clickableRow             = $(".clickable");

    $filterOrNumber           = $("#filter-or-number");
    $filterStartDate          = $("#filter-start-date");
    $filterEndDate            = $("#filter-end-date");
    $filterBranchSelect       = $("#filter-branch-select");
    $filterStatus             = $("#filter-status");

    $modalLoading             = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingErrors       = $(".modal-loading").find(".errors");
    $modalLoadingSuccess      = $(".modal-loading").find(".success");
    $modalLoadingControls     = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
    $modalLoadingBtnClose     = $(".modal-loading").find(".btn-close");
  }

  var _addLoadingToConfirmationBtns = function() {
    $btnGenerateBilling.addClass('loading');
    $btnGenerateBilling.addClass('disabled');

    $btnCancelGenerateBilling.addClass('loading');
    $btnCancelGenerateBilling.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnGenerateBilling.removeClass('loading');
    $btnGenerateBilling.removeClass('disabled');

    $btnCancelGenerateBilling.removeClass('loading');
    $btnCancelGenerateBilling.removeClass('disabled');
  }

  var _bindEvents = function() {
    $btnFilter.on('click', function() {
      $modalLoadingControls.hide();

      var orNumber  = $filterOrNumber.val();
      var startDate = $filterStartDate.val();
      var endDate   = $filterEndDate.val();
      var branchId  = $filterBranchSelect.val();
      var tStatus   = $filterStatus.val();

      var data = {
        or_number: orNumber,
        start_date: startDate,
        end_date: endDate,
        t_status: tStatus
      };

      $modalLoading.open();

      window.location = "/membership_payments/payment_collections?" + encodeQueryData(data);
    });

    $clickableRow.on('click', function() {
      transactionId = $(this).data('transaction-id');
      window.location = "/membership_payments/payment_collections/" + transactionId;
    });

    $btnNewTransaction.on('click', function() {
      $generateBillingModal.open();
    });

    $btnCancelGenerateBilling.on('click', function() {
      if(!$btnGenerateBilling.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $generateBillingModal.close();
        _removeLoadingToConfirmationBtns();
      } else {
        toastr.info("Still loading");
      }
    });

    $btnGenerateBilling.on('click', function() {
      var branchId      = $modalBranchSelect.val();
      var dateOfPayment = $dateOfPayment.val();
      var data = {
        branch_id: branchId,
        date_of_payment: dateOfPayment
      };

      if(!$btnGenerateBilling.hasClass('loading')) {
        $modalErrors.html("");
        $modalSuccess.html("");
        _addLoadingToConfirmationBtns();
        $.ajax({
          url: urlGenerateMembershipPaymentTransaction,
          data: data,
          dataType: 'json',
          method: 'POST',
          success: function(responseContent) {
            console.log(responseContent);
            _removeLoadingToConfirmationBtns();
            $modalSuccess.html(Mustache.render($successTemplate.html(), { messages: responseContent.messages }));
            $modalControls.hide();
            pc_id = responseContent.payment_collection_id;
            window.location = "/membership_payments/payment_collections/" + pc_id + "/edit";
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            console.log(errorMessages);
            $modalErrors.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to generate membership-payment transaction");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $modalBranchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($modalBranchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
