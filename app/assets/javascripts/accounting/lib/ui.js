AccountingEntry.Ui = (function() {

  var $selectBook;
  var $parameters;
  var $journalEntries;
  var $postTypeSelection;
  var $journalEntrySection;
  var $amount;
  var $totalDebit;
  var $totalCredit;
  var $totalsSection;
  var $checkInformation;
  var $orSection;

  var totalDebit  = 0.00;
  var totalCredit = 0.00;

  var _toggleSections = function() {
    if($selectBook.val() == 'CRB') {
      $orSection.show();
    } else {
      $orSection.hide();
    }

    if($selectBook.val() == 'CDB') {
      $checkInformation.show();
    } else {
      $checkInformation.hide();
    }
  };

  var _displayTotals = function() {
    $totalDebit.html(totalDebit);
    $totalCredit.html(totalCredit);

    if(totalDebit == totalCredit) {
      $totalsSection.removeClass('bs-callout-danger');
      $totalsSection.addClass('bs-callout-success');
    } else {
      $totalsSection.removeClass('bs-callout-success');
      $totalsSection.addClass('bs-callout-danger');
    }
  };

  var _computeTotals = function() {
    totalDebit    = 0.00;
    totalCredit   = 0.00;

    $journalEntrySection.each(function() {
      var amount    = $(this).find(".amount").val();
      var postType  = $(this).find(".post-type-selection").val();

      if(amount == "") {
        amount = 0.00;
      } else {
        amount = parseFloat(amount);
      }

      if(postType == 'CR') {
        totalCredit += amount;
      } else if(postType == 'DR') {
        totalDebit += amount;
      }
    });

    _displayTotals();
  };

  var _bindEvents = function() {
    $selectBook.on('change', function() { _toggleSections(); });
    $selectBook.val($parameters.data('book')).trigger('change');

    $journalEntries.on('cocoon:after-insert', function(e, addedEntry) {
      $journalEntries = $("#journal_entries");
      $journalEntrySection = $(".journal-entry-section");

      $journalEntries.find('.amount').on('change', function() {
        _computeTotals();
      });

      $journalEntries.find('.post-type-selection').on('change', function() {
        _computeTotals();
      });

      _computeTotals();
    });

    $journalEntries.on('cocoon:after-remove', function(e, removedEntry) {
      $journalEntries = $("#journal_entries");
      $journalEntrySection = $(".journal-entry-section");

      $journalEntries.find('.amount').on('change', function() {
        _computeTotals();
      });

      $journalEntries.find('.post-type-selection').on('change', function() {
        _computeTotals();
      });

      _computeTotals();
    });

    _computeTotals();
    _toggleSections();
  };

  var init = function(config) {
    $selectBook           = config.selectBook;
    $parameters           = config.parameters;
    $journalEntries       = config.journalEntries;
    $postTypeSelection    = config.postTypeSelection;
    $amount               = config.amount;
    $totalDebit           = config.totalDebit;
    $totalCredit          = config.totalCredit;
    $journalEntrySection  = config.journalEntrySection;
    $totalsSection        = config.totalsSection;
    $checkInformation     = config.checkInformation;
    $orSection            = config.orSection;

    _bindEvents();
  };

  return {
    init: init
  };
})();
