//= require_directory ./lib

AccountingEntry.Form = (function() {
  var $parameters           = $("#parameters");
  var $tAccountSection      = $("#tAccountSection");
  var $journalEntries       = $("#journal_entries");
  var $selectBook           = $("#select-book");
  var $totalDebit           = $("#total-debit");
  var $totalCredit          = $("#total-credit");
  var $journalEntrySection  = $(".journal-entry-section");
  var $totalsSection        = $("#totals-section");
  var $checkInformation     = $("#check-information");
  var $orSection            = $("#or-section");

  var init = function() {
    AccountingEntry.Ui.init({
      selectBook: $selectBook,
      parameters: $parameters,
      journalEntries: $journalEntries,
      totalDebit: $totalDebit,
      totalCredit: $totalCredit,
      postTypeSelection: $(".post-type-selection"),
      amount: $(".amount"),
      journalEntrySection: $journalEntrySection,
      totalsSection: $totalsSection,
      checkInformation: $checkInformation,
      orSection: $orSection
    });
  };

  return {
    init: init
  };
})();
