var PatronageRefundShow = (function() {
  var $btnApproved              = $("#btn-approved");
  var $parameters               = $("#parameters");//$parameters.data('id')
  
  var urlApproved               = "/api/v1/accounting/patronage_refunds/approve";
  
  var $modalConfirmApprove      = $("#modal-confirm-approve").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $btnConfirmApprove        = $("#modal-confirm-approve").find(".btn-confirm");
  var $btnCancelApprove         = $("#modal-confirm-approve").find(".btn-cancel");
  var $confirmApprovedMessage   = $("#modal-confirm-approve").find(".message");

  var $control                  = $(".control");
  var $message                  = $(".message");

  var $btnDownloadExcel         = $("#btn-download-excel");
  var $btnDownloadPDF         = $("#btn-download-pdf");

  var id                         = $parameters.data('id');


  $btnDownloadPDF.on("click", function(){
    data ={
            id: id
          }

    window.location.href = "/accounting/patronage_refund/patronage_refund_collection_pdf?" + encodeQueryData(data);
  
  });



  $btnDownloadExcel.on("click", function(){
    //alert("jef");
    data ={ 
            id: id
          }
    window.location.href = "/accounting/patronage_refund/patronage_refund_excel?" + encodeQueryData(data);

  });

  $btnApproved.on("click", function(){
    $modalConfirmApprove.open();    
  });

  $btnCancelApprove.on("click", function(){
    $modalConfirmApprove.close();
  });

  $btnConfirmApprove.on("click", function(){
    //alert("jef")

    $message.html(
      "Loading..."
    );

    $control.hide();
    
    $.ajax({
      url: urlApproved,
      data: {
        id: id
      },
      dataType: 'json',
      method: 'GET',
      success: function(response){
        //window.location.href = "/accounting/patronage_refunds"
        alert("Successfully approved record.")
        window.location.href = "/accounting/patronage_refunds"
        //$confirmApprovedMessage.html("Successfully approved record. Redirecting...");
        //alert("jef");
      }

    });
    
  });



})();
