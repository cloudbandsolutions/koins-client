var StatementOfComprehensiveInc = (function() {
  var $statementOfComprehensiveIncSection  = $("#statement-of-comprehensive-inc-section");
  var $templateStatementOfComprehensiveInc = $("#template-statement-of-comprehensive-inc");
  var $startDate            = $("#start-date");
  var $endDate              = $("#end-date");
  var $branchSelect         = $("#branch-select");
  var $btnGenerate          = $("#btn-generate");
  var urlAccountingSoci       = "/api/v1/accounting/reports/statement_of_comprehensive_inc";
  var $btnDownload          = $("#download-excel-btn");
  var $detailedCheckBox     = $("#detailed");

  var init = function() {
    $btnDownload.on("click", function() {
      $statementOfComprehensiveIncSection.html("Loading...");

      var params = {
        start_date: $startDate.val(),
        end_date: $endDate.val(),
        branch_id: $branchSelect.val(),
        detailed: $detailedCheckBox.bootstrapSwitch('state'),
      };

      $.ajax({
        url: urlAccountingSoci,
        method: "GET",
        dataType: 'json',
        data: params,
        success: function(response) {

        tempUrl = response.data.download_url;
        window.open(tempUrl, '_blank');

        $statementOfComprehensiveIncSection.html("");

        // Make sticky
          $(".sticky").stickyTableHeaders();

        },
        error: function(response) {
          toastr.error("Error in generating statement of comprehensive inc");
          $statementOfComprehensiveIncSection.html("");
        }
      });
    });
  };

  return {
    init: init
  };
})();
