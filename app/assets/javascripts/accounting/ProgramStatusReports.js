var ProgramStatusReports = (function() {
  var $ProgramStatusSection    = $("#program-status-section");
  var $templatePSSection = $("#template-program-status-reports");
  var $btnGenerate = $("#btn-generate");
  //var $urlPSReports = "/accounting/pages/program_status_reports";
  var $urlPSReports = "/accounting/pages/program_status_data";
  var $firstMonth = $("#first-month")

  var init = function() {
    $btnGenerate.on("click",function(){
      var params = {
        first_month: $firstMonth.val()
      };
      $.ajax({
        url: $urlPSReports,
        method: "GET",
        dataType: 'json',
        data: params,
        success: function(response) {
          console.log(response);
          $ProgramStatusSection.html(
            Mustache.render(
              $templatePSSection.html(),
              response.data
            )//end ng Mustache
          ); //end ng program status
        }, //end ng success
        error: function(response) {
          toastr.error("Error in generating program status");
          $ProgramStatusSection.html("");

        }
      
      //$btnGenerate.prop("disabled", false);
    
      });//end ng ajax
    
    }); //end of btngenerate
  };

  return {
    init: init
  };




})();
