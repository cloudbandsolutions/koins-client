var TrialBalance = (function() {
  var $trialBalanceSection  = $("#trial-balance-section");
  var $templateTrialBalance = $("#template-trial-balance");
  var $startDate            = $("#start-date");
  var $endDate              = $("#end-date");
  var $accountingFundSelect = $("#accounting-fund-select");
  var $branchSelect         = $("#branch-select");
  var $btnGenerate          = $("#btn-generate");
  var urlAccountingTb       = "/api/v1/accounting/reports/trial_balance";
  var urlPrintPdf           = "/accounting/reports/trial_balance_pdf";
  var $btnDownload          = $("#download-excel-btn");
  var $btnPrintPdf          = $("#btn-print-pdf");

  var init = function() {
    $btnPrintPdf.on("click", function() {
      accounting_fund_id = $accountingFundSelect.val();

      if(typeof accounting_fund_id === "undefined") {
        accounting_fund_id = "";
      }

      var params = {
        start_date: $startDate.val(),
        end_date: $endDate.val(),
        branch_id: $branchSelect.val(),
        accounting_fund_id: accounting_fund_id
      };

      var url = urlPrintPdf + "?" + encodeQueryData(params);

      window.open(url, '_blank');
    });

    $btnGenerate.on("click", function() {
      $btnGenerate.prop("disabled", true);
      $trialBalanceSection.html("Loading...");

      var params = {
        start_date: $startDate.val(),
        end_date: $endDate.val(),
        branch_id: $branchSelect.val(),
        accounting_fund_id: $accountingFundSelect.val()
      };

      $.ajax({
        url: urlAccountingTb,
        method: "GET",
        dataType: 'json',
        data: params,
        success: function(response) {
          $trialBalanceSection.html(
            Mustache.render(
              $templateTrialBalance.html(),
              response.data
            )
          );
          
          toastr.success("Successfully generated trial balance");
          $btnGenerate.prop("disabled", false);

          $(".curr").each(function() {
            var x = parseFloat($(this).html());
            $(this).html(numberWithCommas(x));
          });
        },
        error: function(response) {
          toastr.error("Error in generating trial balance");
          $trialBalanceSection.html("");
          $btnGenerate.prop("disabled", false);
        }
      });
    });
  
    $btnDownload.on("click", function() {
      $btnGenerate.prop("disabled", true);
      $trialBalanceSection.html("Loading...");

      var params = {
        start_date: $startDate.val(),
        end_date: $endDate.val(),
        branch_id: $branchSelect.val(),
        accounting_fund_id: $accountingFundSelect.val()
      };

      $.ajax({
        url: urlAccountingTb,
        method: "GET",
        dataType: 'json',
        data: params,
        success: function(response) {
          $trialBalanceSection.html(
            Mustache.render(
              $templateTrialBalance.html(),
              response.data
            )
          );
         
        $btnGenerate.prop("disabled", false);

        tempUrl = response.data.download_url;
        window.open(tempUrl, '_blank');

        // Make sticky
          $(".sticky").stickyTableHeaders();

        },
        error: function(response) {
          toastr.error("Error in generating trial balance");
          $trialBalanceSection.html("");
          $btnGenerate.prop("disabled", false);
        }
      });
    });
  };

  return {
    init: init
  };
})();
