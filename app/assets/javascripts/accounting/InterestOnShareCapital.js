var InterestOnShareCapital = (function(){
  var $ShareCapitalInterestSection  = $("#share-capital-interest-section")
  var $templateShareCapitalSection  = $("#template-share-capital-reports");
  var $startDate                    = $("#start-date");
  var $endDate                      = $("#end-date");
  var $btnGenerate                  = $("#btn-generate");
  var $urlInterestSC                = "/api/v1/accounting/interest_on_share_capital_collections/generate";
  var $btnAction                    = $("#btn-action");
  var $modalTransaction             = $("#modal-transaction");
  var $interestEquityRate           = $("#equity-interest");
  var $btnApproved                  = $("#approved-record");
  var $urlApproveTransaction        = "/api/v1/accounting/interest_on_share_capital_collections/approved";
  var urlEquityRate                 = "/api/v1/accounting/interest_on_share_capital_collections/equity_rate";
  var urlRegenerate                 = "/api/v1/accounting/interest_on_share_capital_collections/regenerate";
  //var $urlApproveTransaction        = "/accounting/interest_on_share_capital_collections/save_voucher"
  var $year = $("#year");
  var $btnEditTransaction           = $("#edit-record");
  var $modalEditTransaction         = $("#modal-edit-transaction");
  var $btnEditModalSaveTransaction           = $("#btn-edit");
  var $parameters                   = $("#parameters");
  var $userInput      = $("#user-input");
  var $equityInterest = $("#equity-interest");
  var $loader         = $("#loader");
  
  var $btnDownloadExcel             = $("#btn-download-excel");
  var $btnDownloadPDF               = $("#btn-download-pdf");

  var $insurancedetails              = $parameters.data("id");

  var init = function() {

    $btnDownloadPDF.on("click", function(){

      data = {
        id: $insurancedetails 
       }
      window.location.href = "/accounting/interest_on_share_capital_collection/interest_on_share_capital_pdf?" + encodeQueryData(data);

    });
  
    $btnDownloadExcel.on("click", function(){
      data = {
        id: $insurancedetails 
       }

      window.location.href = "/accounting/interest_on_share_capital_collection/interest_on_share_capital_excel?" + encodeQueryData(data);

    });
    

    $btnEditModalSaveTransaction.on("click", function(){
      
    });
        
    $btnEditTransaction.on("click",function(){
        $modalEditTransaction.modal("show");

        $userInput.hide();
        $loader.show();

        $.ajax({
          url: urlEquityRate,
          method: 'GET',
          data: {
            id: $insurancedetails
          },
          dataType: 'json',
          success: function(response) {
            $equityInterest.val(response.equity_rate);

            $userInput.show();
            $loader.hide();
          },
          error: function(response) {
            alert("error in fetching equity interest");
          }
        });
    });   
    $btnApproved.on("click", function(){
      //alert($insurancedetails);
      var params ={
        equity_id: $insurancedetails
      };
      $.ajax({
        url: $urlApproveTransaction,
        method: "GET",
        dataType: 'json',
        data: params,
        success: function(response){
          console.log(response);
          alert("jef");
        },
        error: function(response){
          toastr.error("Error in generatin Share Capital")          
        }
      
      });
    });


    $btnAction.on("click", function(){
      $modalTransaction.modal("show");
      //alert("jef")
    });


    $btnGenerate.on("click",function(){
      //alert($interestEquityRate.val());
      var params = {
                        
        //start_date: $startDate.val(),
        end_date: $year.val(),
        equity_interest: $interestEquityRate.val()
      
      };
    
      $.ajax({
        url: $urlInterestSC,
        method: "POST",
        dataType: 'json',
        data: params,
        success: function(response){
          //alert ("jef");
          window.location.href = "/accounting/interest_on_share_capital_collections"

        },
        error: function(response) {
          toastr.error("Error in generatin Share Capital")

        }
      
      
      });
      //alert("jef");

    }); //end of btngenerate
  };


  return {
    init: init
  }
})();
