var BalanceSheets = (function() {
  var $btnNewBalanceSheet     = $("#btn-new-balance-sheet");
  var urlGenerateBalanceSheet = "/api/v1/accounting/balance_sheet/generate";

  var init  = function() {
    $btnNewBalanceSheet.on("click", function() {
      var originalContent = $btnNewBalanceSheet.html();
      $btnNewBalanceSheet.addClass('loading');
      $btnNewBalanceSheet.addClass('disabled');
      $btnNewBalanceSheet.html("Loading...");

      $.ajax({
        url: urlGenerateBalanceSheet,
        method: "POST",
        success: function(response) {
          $btnNewBalanceSheet.html("Redirecting...");
          window.location.href = "/accounting/balance_sheets/" + response.balance_sheet.id;
        },
        error: function(response) {
          $btnNewBalanceSheet.removeClass('loading');
          $btnNewBalanceSheet.removeClass('disabled');
          $btnNewBalanceSheet.html(originalContent);
          toastr.error("Error in generating balance sheet");
        }
      });
    });
  };

  return {
    init: init
  };
})();
