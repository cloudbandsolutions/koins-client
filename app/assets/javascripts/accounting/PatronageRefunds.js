var PatronageRefund = (function() {
  var $btnNew                       = $("#btn-new");
  var $modalTransaction             = $("#modal-transaction");
  var $btnGenerate                  = $("#btn-generate");
  var $txtPatronageInterest         = $("#patronage-interest");
  var $as_of                        = $("#start-date");
  var urlCreatePatronageRefunds     = "/api/v1/accounting/patronage_refunds/generate";


  var $urlApproveTransaction        = "/api/v1/accounting/interest_on_share_capital_collections/approved";

  
//  var $btnApproved                  = $("#approved-record");

  var $templatePatroangeRefund      = $("#template-patronage-refund");
  var $PatroangeRefundSection       = $("#patronage-refund-section")
  var $year                         = $("#year");

  var $actions  = $("#actions");
  var $message  = $(".message");

  // Get content of errors template
  var templateErrors  = $("#errors-template").html();

  var init = function() {

 //   $btnApproved.on("click", function(){
  //    alert ("jef");
    
  //  });




    $btnNew.on("click", function(){
      $modalTransaction.modal("show");
    });

    $btnGenerate.on("click",function(){
      //alert($txtPatronageInterest.val());

      $actions.hide();
      $message.html("Loading...");

      $.ajax({
        url: urlCreatePatronageRefunds,
        method: 'POST',
        data: {
          patronage_rate: $txtPatronageInterest.val(),
          //as_of: $as_of.val()
          as_of: $year.val()
        }, //end of data
        dataType: 'json',
        success: function(response){
          window.location.href = "/accounting/patronage_refunds"
          //alert("jef");
        }, //end of success
        error: function(response) {
          var errors = ["Something went wrong"];

          try {
            console.log(response);
            errors = JSON.parse(response.responseText);
            $message.html(
              Mustache.render(
                templateErrors,
                errors
              )
            );

            $actions.show();
          } catch(e) {
            console.log(e);
            $message.html(
              Mustache.render(
                templateErrors,
                errors
              )
            );

            $actions.show();
          }
        }

      });//end of ajax
    });//end of generate


  };



  return {
    init: init
  }
  



})(); 
