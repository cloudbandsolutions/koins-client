var BalanceSheet = (function() {
  var urlGenerateBalanceSheet  = "/api/v1/accounting/balance_sheet/generate";
  var $balanceSheetTemplate    = $("#balance-sheet-template");
  var $balanceSheetSection     = $("#balance-sheet-section");
  var init = function() {
    var data  = $("#parameters").data('data');
    $balanceSheetSection.html(
      Mustache.render(
        $balanceSheetTemplate.html(),
        data
      )
    );

    // format numbers
    $(".curr").each(function() {
      var val = parseFloat($(this).html());
      var newVal = numberWithCommas(val);

      if(val < 0) {
        newVal = val * -1;
        newVal = "(" + numberWithCommas(newVal) + ")";
      }
      $(this).html(newVal);
    });
  };

  return {
    init: init
  };
})();
