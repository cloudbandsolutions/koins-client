var FinancialPosition = (function() {
  var $financialPositionSection  = $("#financial-position-section");
  var $templateFinancialPosition = $("#template-financial-position");
  var $startDate            = $("#start-date");
  var $endDate              = $("#end-date");
  var $branchSelect         = $("#branch-select");
  var $btnGenerate          = $("#btn-generate");
  var urlAccountingFp       = "/api/v1/accounting/reports/financial_position";
  var $btnDownload          = $("#download-excel-btn");
  var $detailedCheckBox     = $("#detailed");

  var init = function() {
    $btnDownload.on("click", function() {
      $financialPositionSection.html("Loading...");

      var params = {
        start_date: $startDate.val(),
        end_date: $endDate.val(),
        branch_id: $branchSelect.val(),
        detailed: $detailedCheckBox.bootstrapSwitch('state'),
      };

      $.ajax({
        url: urlAccountingFp,
        method: "GET",
        dataType: 'json',
        data: params,
        success: function(response) {

        tempUrl = response.data.download_url;
        window.open(tempUrl, '_blank');

        $financialPositionSection.html("");

        // Make sticky
          $(".sticky").stickyTableHeaders();

        },
        error: function(response) {
          toastr.error("Error in generating financial position");
          $financialPositionSection.html("");
        }
      });
    });
  };

  return {
    init: init
  };
})();
