var IncomeStatement = (function() {
  var urlGenerateIncomeStatement  = "/api/v1/accounting/income_statement/generate";
  var $incomeStatementTemplate    = $("#income-statement-template");
  var $incomeStatementSection     = $("#income-statement-section");
  var init = function() {
    $.ajax({
      url: urlGenerateIncomeStatement,
      method: 'GET',
      success: function(response) {
        $incomeStatementSection.html(
          Mustache.render(
            $incomeStatementTemplate.html(),
            response.data
          )
        );

        // format numbers
        $(".curr").each(function() {
          var val = parseFloat($(this).html());
          var newVal = numberWithCommas(val);

          if(val < 0) {
            newVal = val * -1;
            newVal = "(" + numberWithCommas(newVal) + ")";
          }
          $(this).html(newVal);
        });
      },
      error: function(response) {
        toastr.error("Error in generating income statement")
      }
    });
  };

  return {
    init: init
  };
})();
