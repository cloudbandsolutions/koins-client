var trialBalance = (function() {
  var $generateBtn;
  var startDate;
  var $startDateInput;
  var endDate;
  var $endDateInput;
  var $trialBalanceTemplate;
  var $branchSelect;
  var branchId;
  var $trialBalanceSection;
  var $loader;
  var $downloadPdfBtn;
  var $downloadExcelBtn;

  var urlEntries = "/trial_balance/entries";
  var urlGenerateDownloadPdf = "/trial_balance/generate_download_pdf_url";
  var urlDownloadExcel = "/trial_balance/generate_download_excel_url";

  var _cacheDom = function() {
    $downloadPdfBtn = $("#download-pdf-btn");
    $downloadExcelBtn = $("#download-excel-btn");
    $generateBtn = $("#generate-btn");
    $startDateInput = $("#start-date");
    $endDateInput = $("#end-date");
    $branchSelect = $("#branch-select");
    $trialBalanceSection = $("#trial-balance-section");
    $trialBalanceTemplate = $("#trial-balance-template").html();
    $loader = $("#loader");
  }

  var _validateFilterInput = function() {
    startDate = $startDateInput.val();
    endDate = $endDateInput.val();

    if(!startDate || !endDate) {
      return false;
    } else {
      return true;
    }
  }

  var _renderTrialBalance = function(data) {
    $trialBalanceSection.html(Mustache.render($trialBalanceTemplate, data));
  }

  var init = function() {
    _cacheDom();

    $generateBtn.on('click', function() {
      $loader.addClass("active");
      if(_validateFilterInput()) {
        branchId = $branchSelect.val();
        startDate = $startDateInput.val();
        endDate = $endDateInput.val();

        var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate
        };

        // window.location.href = "/print?start_date=" + startDate + "&end_date=" + endDate;

        console.log(params);
        $.ajax({
          url: urlEntries,
          data: params,
          dataType: 'json',
          method: 'GET',
          success: function(responseContent) {
            _renderTrialBalance(responseContent.data);
            toastr.success("Generated trial balance");
            $loader.removeClass('active');
          },
          error: function(responseContent) {
            toastr.error("Something went wrong.");
            $loader.removeClass('active');
          }
        });
      } else {
        toastr.error("Start date and end date required");
        $loader.removeClass('active');
      }
    });

    $downloadPdfBtn.on('click', function() {
      $loader.addClass("active");
      if(_validateFilterInput()) {
        branchId = $branchSelect.val();
        startDate = $startDateInput.val();
        endDate = $endDateInput.val();

        var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate
        };

        console.log(params);
        $.ajax({
          url: urlGenerateDownloadPdf,
          data: params,
          dataType: 'json',
          method: 'GET',
          success: function(responseContent) {
            _renderTrialBalance(responseContent.data);
            toastr.success("Download trial balance");
            $loader.removeClass('active');
            window.open(responseContent.download_url, '_blank');
          },
          error: function(responseContent) {
            toastr.error("Something went wrong.");
            $loader.removeClass('active');
          }
        });
      } else {
        toastr.error("Start date and end date required");
        $loader.removeClass('active');
      }
    });

    $downloadExcelBtn.on('click', function() {
      $loader.addClass("active");
      if(_validateFilterInput()) {
        branchId = $branchSelect.val();
        startDate = $startDateInput.val();
        endDate = $endDateInput.val();

        var params = {
          branch_id: branchId,
          start_date: startDate,
          end_date: endDate
        };

        console.log(params);
        $.ajax({
          url: urlDownloadExcel,
          data: params,
          dataType: 'json',
          method: 'GET',
          success: function(responseContent) {
            //_renderTrialBalance(responseContent.data);
            toastr.success("Downloading Excel. Please wait.");
            $loader.removeClass('active');
            //window.open(responseContent.download_url, '_blank');
            window.location.href=responseContent.download_url;
          },
          error: function(responseContent) {
            toastr.error("Something went wwrong.");
            $loader.removeClass('active');
          }
        });
      } else {
        toastr.error("Start date and end date required");
        $loader.removeClass('active');
      }
    });
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  trialBalance.init();
});
