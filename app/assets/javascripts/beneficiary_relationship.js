var beneficiaryRelationship = (function() {

  var $selectForRelationship;
  var $inputForRelationship;
  var $isPrimary;
  var $isDeceased

 
  var _cacheDom = function() {
    $selectForRelationship   = $('#select-for-relationship');
    $inputForRelationship    = $("#input-for-relationship");
    $isDeceased              = $(".is-deceased");
    $isPrimary               = $(".is-primary");
  }

  var _bindEvents = function() {
    var is_deceased_state = $isDeceased.bootstrapSwitch('state');
    var is_primary_state  = $isPrimary.bootstrapSwitch('state');
    var is_deceaced_checked = is_deceased_state;  

    $isDeceased.on('switchChange.bootstrapSwitch', function (event, is_deceased_state) {
      var checked = is_deceased_state;
      if (checked) {
        $isPrimary.bootstrapSwitch('state', false);
      }
    });

    $selectForRelationship.on('change', function () {
    if (this.value === 'Iba pa') {
      $inputForRelationship.show();
    } else {
      $inputForRelationship.hide();
      var select_value = $selectForRelationship.val();
      $inputForRelationship.val(select_value);
    }
  });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  beneficiaryRelationship.init();
});