//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require jquery_nested_form
//= require Chart
//= require select2
//= require toastr
//= require serializeJson
//= require jquery-ui
//= require handlebars
//= require handlebars-helpers
//= require utils
//= require account_transactions
//= require accounts
//= require cocoon
//= require navigation
//= require countdown
//= require mustache
//= require bootstrap-switch
//= require jquery.serializejson
//= require adminlte-app
//= require accounting
//= require moment
//= require bootstrap-datetimepicker
//= require remodal
//= require jquery.stickytableheaders.min
//= require jquery.floatThead
//= require datatables

var initDatePicker = function() {
  $(".datepick").datepicker({ dateFormat: "yy-mm-dd" });
};

var initDatetimePicker = function() {
  $(".datetimepicker").datetimepicker({
    format: 'MM/DD/YYYY hh:mm A'
  });
}

var numberWithCommas = function(x) {
  x = (Math.round(x * 100) / 100).toFixed(2);
  if(x < 0) {
    x = x * -1;
    x = "(" + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ")";
  } else {
    x = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return x;
}


$(document).ready(function() {
  // datatables
  $('.datatable').DataTable();

  $('.switch').bootstrapSwitch();

  $("#cashManagementBtn").on("click", function() {
    $("#navigation_cash_management").sidebar('toggle');
  });

  $("#accountingBtn").on("click", function() {
    $("#navigation_accounting").sidebar('toggle');
  });

  $("#reportsBtn").on("click", function() {
    $("#navigation_reports").sidebar('toggle');
  });

  // Setup timer for closing
  $.ajax({
    url: '/next_closing_time',
    dataType: 'json',
    success: function(data) {
      $("#timer").countdown(data.closingTime)
        .on('update.countdown', function(event) {
          var countdownTime = event.strftime('%-H hours %M min %S sec');

          $("#timerValue").html(countdownTime);
        });
    }
  });

  initDatePicker();
  initDatetimePicker();

  $("select.select-select2").select2({
    theme: "bootstrap",
    width: "100%"
  });

  var $stickyTable  = $(".sticky");
  $stickyTable.floatThead({
    position: "absolute"
  });
  $('.ui.accordion').accordion();
});

$(document).on('nested:fieldAdded', function() {
  initDatePicker();
  initDatetimePicker();
});

$(document).on('cocoon:after-insert', function(e, insertedItem) {
  initDatePicker();
  initDatetimePicker();
  $("select.select-select2").select2({theme: "bootstrap"});
});

$(document).bind('ajaxStart', function() {
  $(".loadingBox").fadeToggle();
});

$(document).bind('ajaxStop', function() {
  $(".loadingBox").fadeToggle();
});

$.fn.serializeFormObject = function() {
  var formObject = {};
  var a = this.serializeArray();
  $.each(a, function(i, field) {
    formObject[field.name] = field.value;
  });

  return formObject;
}
