// 1) empty selectUi
// 2) use valueName as value=valueName and displayName for <option>displayName</option>
var populateSelect = function(selectUi, data, valueName, displayName) {
  selectUi.find('option').remove();

  // Prepend ALL
  selectUi.prepend("<option value='' selected='selected'>-- ALL --</option>");
  
  data.forEach(function(o) {
    var newOption = '<option value="' + o[valueName] + '">' + o[displayName] + '</option>';
    selectUi.append(newOption);
  });

  selectUi.select2({ theme: "bootstrap" });
};

var populateSelectNormal = function(selectUi, data) {
  selectUi.find('option').remove();
  data.forEach(function(o) {
    var newOption = '<option value="' + o + '">' + o + '</option>';
    selectUi.append(newOption);
  });
};

var populateSelectNormalWithHash = function(selectUi, data, valueName, displayName) {
  selectUi.find('option').remove();
  data.forEach(function(o) {
    var newOption = '<option value="' + o[valueName] + '">' + o[displayName] + '</option>';
    selectUi.append(newOption);
  });
};

var encodeQueryData = function(data) {
  var ret = []
  for(var d in data) {
    ret.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));
  }

  return ret.join("&");
};

var _renderTemplate = function($el, template, locals) {
  $el.html(Mustache.render(template, locals));
};
