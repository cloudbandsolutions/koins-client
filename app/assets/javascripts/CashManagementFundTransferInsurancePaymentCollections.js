var CashManagementFundTransferInsurancePaymentCollections = (function() {
  var $modalNewTransaction              = $(".modal-new-transaction").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $loadingModal                     = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $loadingModalControls             = $(".modal-loading").find('.controls');
  $loadingModalControls.hide();
  var $btnConfirmNewTransaction         = $("#btn-confirm-new-transaction");
  var $btnCancelNewTransaction          = $("#btn-cancel-new-transaction");
  var $modalNewTransactionBranchSelect  = $("#modal-new-transaction-branch-select");
  var $modalNewTransactionDateOfPayment = $("#modal-new-transaction-date-of-payment");

  var $btnNewTransaction                = $("#btn-new-transaction");
  
  var $modalNewTransactionMessage       = $(".modal-new-transaction").find(".message");
  var $modalNewTransactionControls      = $(".modal-new-transaction").find(".controls");

  var urlNewTransaction                 = "/api/v1/cash_management/fund_transfer/insurance/payment_collections/generate_transaction";
  var $errorsTemplate                   = $("#errors-template");
  var $successTemplate                  = $("#success-template");

  var $clickableRow                     = $(".clickable");
  var $filterOrNumber                   = $('#filter-or-number');
  var $filterStartDate                  = $('#filter-start-date');
  var $filterEndDate                    = $('#filter-end-date');
  var $filterBranchSelect               = $('#filter-branch-select');
  var $filterStatus                     = $('#filter-status');
  var $btnFilter                        = $('#btn-filter');

  var _bindEvents = function() {
    $btnFilter.on('click', function(){
      var startDate = $filterStartDate.val();
      var endDate   = $filterEndDate.val();
      var branchId  = $filterBranchSelect.val();
      var tStatus   = $filterStatus.val();

      var data = {    
        branch_id: branchId,
        start_date: startDate,
        end_date: endDate,
        status: tStatus
      };

      $loadingModal.open();

      window.location = "/cash_management/fund_transfer/insurance/payment_collections?" + encodeQueryData(data);
    });

    $btnNewTransaction.on('click', function() {
      $modalNewTransaction.open();
    });
    
    $btnCancelNewTransaction.on('click', function() {
      $modalNewTransaction.close();
    });

    $btnConfirmNewTransaction.on('click', function() {
      $modalNewTransactionControls.hide();
      $modalNewTransactionMessage.html("Loading...");

      var data = {
        branch_id: $modalNewTransactionBranchSelect.val(),
        date_of_payment: $modalNewTransactionDateOfPayment.val(),
      };

      $.ajax({
        url: urlNewTransaction,
        data: data,
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          $modalNewTransactionMessage.html("Successfully created new transaction. Redirecting...");
          window.location = "/cash_management/fund_transfer/insurance/payment_collections/" + response.id;
        },
        error: function(response) {
          toastr.error("Error in generating new transaction");
          $modalNewTransactionMessage.html(Mustache.render($errorsTemplate.html(), JSON.parse(response.responseText)));
          $modalNewTransactionControls.show();
        }
      });
    });


    $clickableRow.on('click', function() {
      Id = $(this).data('transaction-id');
      window.location = "/cash_management/fund_transfer/insurance/payment_collections/" + Id;
    });
  }

  var init = function() {
    _bindEvents();
  }

  return {
    init: init
  };
})();
