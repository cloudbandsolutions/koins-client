var InsurancePaymentCollections = (function() {
  //this
  var $branchSelect;
  var $centerSelect;


  var $modalBranchSelect;
  var $modalCenterSelect;
  var $dateOfPayment;
  var $btnCancelGenerateBilling;
  var $btnGenerateBilling;
  var $btnNewInsurance;

  var urlUtilsBranches  = "/api/v1/utils/branches";
  var urlUtilsCenters   = "/api/v1/utils/centers";
  var urlNewBilling     = "/insurance/payment_collections/new";
  
  var $filterOrNumber; 
  var $filterStartDate;
  var $filterEndDate;
  var $filterBranchSelect;
  var $filterCenterSelect;
  var $filterStatus;

  var $errors;
  var $errorsTemplate;
  var $modalErrors;
  var $modalSuccess;
  var $modalControls;
  var $successTemplate;
  var $clickableRow;

  var $modalLoading;
  var $modalLoadingErrors;
  var $modalLoadingSuccess; 
  var $modalLoadingControls;
  var $modalLoadingBtnClose;

  var urlGenerateDepositTransaction = "/api/v1/insurance/payment_collections/generate_transaction";


  var _cacheDom = function() {
    //this
    $branchSelect        = $(".branch-select");
    $centerSelect        = $(".center-select");

    $modalBranchSelect        = $("#modal-branch-select");
    $modalCenterSelect        = $("#modal-center-select");
    $dateOfPayment            = $("#date-of-payment");
    $btnNewInsurance           = $("#btn-new-insurance");
    $generateBillingModal     = $(".modal-generate-insurance-payment-collection").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalErrors              = $(".modal-generate-insurance-payment-collection").find(".errors");
    $errors                   = $(".errors");
    $modalSuccess             = $(".modal-generate-insurance-payment-collection").find(".success");
    $modalControls            = $(".modal-generate-insurance-payment-collection").find(".controls");
    //cancel-confirm
    $btnGenerateBilling       = $("#btn-generate-billing");
    $btnCancelGenerateBilling = $("#btn-cancel-generate-billing");
    //for clickabe row
    $clickableRow        = $(".clickable");

    $btnFilter                = $("#btn-filter");

    $filterOrNumber           = $("#filter-or-number");
    $filterStartDate          = $("#filter-start-date");
    $filterEndDate            = $("#filter-end-date");
    $filterBranchSelect       = $("#filter-branch-select");
    $filterCenterSelect       = $("#filter-center-select");
    $filterStatus             = $("#filter-status");

    $modalLoading             = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingErrors       = $(".modal-loading").find(".errors");
    $modalLoadingSuccess      = $(".modal-loading").find(".success");
    $modalLoadingControls     = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
    $modalLoadingBtnClose     = $(".modal-loading").find(".btn-close");
      }


  var _addLoadingToConfirmationBtns = function() {
    $btnGenerateBilling.addClass('loading');
    $btnGenerateBilling.addClass('disabled');

    $btnCancelGenerateBilling.addClass('loading');
    $btnCancelGenerateBilling.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnGenerateBilling.removeClass('loading');
    $btnGenerateBilling.removeClass('disabled');

    $btnCancelGenerateBilling.removeClass('loading');
    $btnCancelGenerateBilling.removeClass('disabled');
  }

  var _bindEvents = function() {

    $branchSelect.bind('afterShow', function() {
      $.ajax({
        url: '/api/v1/utils/branches',
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect(this, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });

    // On change of branch select
    $branchSelect.bind('change', function() {
      var branchId = ($(this).val());  
      params = { branch_id: branchId };

      // Clear centers
      $centerSelect.find('option').remove();

      if(branchId) {
        $.ajax({
          url: '/api/v1/utils/centers' ,
          type: 'get',
          dataType: 'json',
          data: params,
          success: function(data) {
            var centers = data.data.centers;
            populateSelect($centerSelect, centers, 'id', 'name');

            var centerId = $("#parameters").data("center-id");
            if(centerId) {
              console.log("Setting center to " + centerId);
              $centerSelect.val(centerId).trigger("change");
            }
          },
          error: function() {
            toastr.error("ERROR: loading centers on branch select");
          }
        });
      }
    });

    // Preload centers
    var branchId = ($branchSelect.val());  
    params = { branch_id: branchId };

    if(branchId) {
      $.ajax({
        url: '/api/v1/utils/centers' ,
        type: 'get',
        dataType: 'json',
        data: params,
        success: function(data) {
          var centers = data.data.centers;
          populateSelect($centerSelect, centers, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading centers on branch select");
        }
      });
    }

    $btnGenerateBilling.on('click', function() {
      $(this).prop('disabled', true);
      $(this).addClass('loading');

      var branchId      = $modalBranchSelect.val();
      var centerId      = $modalCenterSelect.val();
      var dateOfPayment = $dateOfPayment.val();
      var data = {
        branch_id: branchId,
        center_id: centerId,
        date_of_payment: dateOfPayment
      };

      var urlRedirect = urlNewBilling + "?" + $.param(data);
      window.location.href = urlRedirect;

      $btnGenerateBilling.prop('disabled', false);
      $btnGenerateBilling.removeClass('loading');
    });

    //filter
    $btnFilter.on('click', function() {
      $modalLoadingControls.hide();

      var orNumber  = $filterOrNumber.val();
      var startDate = $filterStartDate.val();
      var endDate   = $filterEndDate.val();
      var branchId  = $filterBranchSelect.val();
      var centerId  = $filterCenterSelect.val();
      var tStatus   = $filterStatus.val();

      var data = {
        or_number: orNumber,
        start_date: startDate,
        end_date: endDate,
        branch_id: branchId,
        center_id: centerId,
        t_status: tStatus
      };

      $modalLoading.open();

      window.location = "/insurance/payment_collections?" + encodeQueryData(data);
    });

    //open remodal
    $btnNewInsurance.on('click', function() {
      $generateBillingModal.open();
    });

    //for clickable row
    $clickableRow.on('click', function() {
      transactionId = $(this).data('transaction-id');
      window.location = "/insurance/payment_collections/" + transactionId;
    });

    //cancel modal
    $btnCancelGenerateBilling.on('click', function() {
      if(!$btnGenerateBilling.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $generateBillingModal.close();
        _removeLoadingToConfirmationBtns();
      } else {
        toastr.info("Still loading");
      }
    });


    $modalBranchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($modalBranchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });

    $modalBranchSelect.bind('change', function() {
      var modalBranchId = ($(this).val());
      params = { branch_id: modalBranchId };

      $modalCenterSelect.find('option').remove();

      $.ajax({
        url: urlUtilsCenters,
        type: 'get',
        dataType: 'json',
        data: params,
        success: function(data) {
          var centers = data.data.centers;
          populateSelect($modalCenterSelect, centers, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading centers on branch select");
        }
      });
    });

    // Preload centers
    var modalBranchId = ($modalBranchSelect.val());
    params = { branch_id: modalBranchId };
    $.ajax({
      url: urlUtilsCenters,
      type: 'get',
      dataType: 'json',
      data: params,
      success: function(data) {
        var centers = data.data.centers;
        populateSelect($modalCenterSelect, centers, 'id', 'name');
      },
      error: function() {
        toastr.error("ERROR: loading centers on branch select");
      }
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  InsurancePaymentCollections.init();
});
