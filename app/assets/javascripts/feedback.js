$(document).ready(function() {
  $("#feedbackBtn").on('click', function() {
    $("#feedback-section").addClass("active"); 

    var memberId = $("#member_id").val();
    var feedbackOptionId = $("#feedback_option_id").val();
    var others = $("#others").val();

    params = {
      member_id: memberId,
      feedback_option_id: feedbackOptionId,
      others: others
    }

    $.ajax({
      url: '/api/v1/feedbacks/create',
      method: 'GET',
      dataType: 'json',
      data: params,
      success: function(data) {
        $("#feedback-section").removeClass("active"); 
        toastr.success("Successfully created feedback");

        $("#others").val("");
      },
      error: function() {
        $("#feedback-section").removeClass("active"); 
        toastr.error("Error in creating feedback");
      }
    });
  });
});
