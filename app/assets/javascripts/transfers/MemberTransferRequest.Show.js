var MemberTransferRequest = {};

MemberTransferRequest.Show  = (function() {
  var $modalCancelTransfer        = $("#modal-cancel-transfer");
  var $modalApproveTransfer       = $("#modal-approve-transfer");
  var $btnCancelTransfer          = $("#btn-cancel-transfer");
  var $btnConfirmCancelTransfer   = $("#btn-confirm-cancel-transfer");
  var $btnConfirmApprovetransfer  = $("#btn-confirm-approve-transfer");
  var $btnApprove                 = $("#btn-approve");
  var $parameters                 = $("#parameters")
  var memberId                    = $parameters.data("member-id");
  var memberTransferRequestId     = $parameters.data("member-transfer-request-id"); 

  var urlCancelRequestTransfer  = "/api/v1/members/cancel_request_transfer";
  var urlApproveRequestTransfer = "/api/v1/members/approve_request_transfer";

  var init  = function() {
    $btnApprove.on("click", function() {
      $modalApproveTransfer.modal("show");
    });

    $btnCancelTransfer.on("click", function() {
      $modalCancelTransfer.modal("show");
    });

    $btnConfirmCancelTransfer.on("click", function() {
      $btnConfirmCancelTransfer.prop("disabled", true);

      $.ajax({
        url: urlCancelRequestTransfer,
        method: 'POST',
        data: {
          member_id: memberId
        },
        dataType: 'json',
        success: function(response) {
          toastr.success('Successfully cancelled transfer');
          window.location.href = "/members/" +  memberId;
        },
        error: function(response) {
          $.each(JSON.parse(response.responseText).errors, function() {
            toastr.error(this);
            $btnConfirmCancelTransfer.prop("disabled", false);
          });
        }
      });
    });
  };

  return {
    init: init
  };
})();
