var CenterTransferRecord = {};

CenterTransferRecord.Show = (function() {
  var $btnApprove;
  var $btnConfirmApproval;
  var $modalApprove;
  var $parameters;
  var centerTransferRecordId;
  var url = "/api/v1/transfer/center_transfer_records/approve";

  var _cacheDom = function() {
    $parameters         = $("#parameters");
    $btnApprove         = $("#btn-approve");
    $btnConfirmApproval = $("#btn-confirm-approval");
    $btnConfirmClean    = $("#btn-confirm-clean");
    $modalApprove       = $("#modal-approve");
    centerTransferRecordId  = $parameters.data("id");
  };

  var init = function() {
    _cacheDom();

    $btnApprove.on("click", function() {
      $modalApprove.modal("show");
    });

    $btnConfirmApproval.on("click", function() {
      $btnConfirmApproval.prop("disabled", true);

      $.ajax({
        url: url,
        method: 'POST',
        data: {
          id: centerTransferRecordId
        },
        dataType: 'json',
        success: function(response) {
          toastr.success("Successfully approved transfer. Redirecting...");
          window.location.href = "/transfer/center_transfer_records/" + centerTransferRecordId;
        },
        error: function(response) {
          console.log(response);
          toastr.error("Something went wrong");
          $btnConfirmApproval.prop("disabled", false);
        }
      });
    });
  };

  return {
    init: init
  };
})();
