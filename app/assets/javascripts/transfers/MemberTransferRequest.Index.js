var MemberTransferRequest = {};

MemberTransferRequest.Index = (function() {
  var $btnGenerate;
  var $btnCancelPending;
  var $btnApprovePending;
  var $btnClean;
  var $btnConfirmGenerate;
  var $btnConfirmCancelPending;
  var $btnConfirmApprovePending;
  var $btnConfirmClean;
  var $modalApproveGenerate;
  var $modalApprovePending;
  var $modalCancelPending;
  var $modalClean;
  var $centerSelect;
  var centerId;

  var urlGenerateTransferRecords  = "/api/v1/members/generate_transfer_records";
  var urlCancelTransferRecords    = "/api/v1/members/cancel_transfer_records";
  var urlApprovePendingRecords    = "/api/v1/members/approve_pending_records";
  var urlClean                    = "/api/v1/members/clean_transfer_records";
  
  var _cacheDom = function() {
    $btnGenerate              = $("#btn-generate");
    $btnCancelPending         = $("#btn-cancel-pending");
    $btnApprovePending        = $("#btn-approve-pending");
    $btnClean                 = $("#btn-clean");
    $btnConfirmGenerate       = $("#btn-confirm-generate");
    $btnConfirmCancelPending  = $("#btn-confirm-cancel-pending");
    $btnConfirmApprovePending = $("#btn-confirm-approve-pending");
    $btnConfirmClean          = $("#btn-confirm-clean");
    $modalApproveGenerate     = $("#modal-approve-generate");
    $modalApprovePending      = $("#modal-approve-pending");
    $modalCancelPending       = $("#modal-cancel-pending");
    $modalClean               = $("#modal-clean");
    $centerSelect             = $("#center-select");
  };

  var init  = function() {
    _cacheDom();

    $btnClean.on("click", function() {
      $modalClean.modal("show");
    });

    $btnConfirmClean.on("click", function() {
      $btnConfirmClean.prop("disabled", true);

      $.ajax({
        url: urlClean,
        method: 'POST',
        dataType: 'json',
        success: function(response) {
          toastr.success("Successfully cleaned records. Redirecting...");
          window.location.href = "/member_transfer_requests";
        },
        error: function(response) {
          toastr.error("Something went wrong while trying to clean records");
          $btnConfirmClean.prop("disabled", false);
        }
      });
    });

    $btnApprovePending.on("click", function() {
      $modalApprovePending.modal("show");
    });

    $btnConfirmApprovePending.on("click", function() {
      $btnConfirmApprovePending.prop("disabled", true);

      $.ajax({
        url: urlApprovePendingRecords,
        method: 'POST',
        data: {
        },
        dataType: 'json',
        success: function(response) {
          toastr.success("Successfully approved pending records. Redirecting...");
          window.location.href = "/member_transfer_requests";
        },
        error: function(response) {
          toastr.error("Something went wrong when trying to approve pending records");
        }
      });
    });

    $btnCancelPending.on("click", function() {
      $modalCancelPending.modal("show");
    });

    $btnConfirmCancelPending.on("click", function() {
      $btnConfirmCancelPending.prop("disabled", true);

      $.ajax({
        url: urlCancelTransferRecords,
        data: {
        },
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          toastr.success("Successfully cancelled pending. Redirecting...");
          window.location.href = "/member_transfer_requests";
        },
        error: function(response) {
          toastr.error("Something went wrong");
          $btnConfirmCancelPending.prop("disabled", false);
        }
      });
    });

    $btnGenerate.on("click", function() {
      $modalApproveGenerate.modal("show");
    });

    $btnConfirmGenerate.on("click", function() {
      $btnConfirmGenerate.prop("disabled", true);
      $centerSelect.prop("disabled", true);

      centerId  = $centerSelect.val();

      if(centerId == "") {
        toastr.error("Please select center");
        $btnConfirmGenerate.prop("disabled", false);
        $centerSelect.prop("disabled", false);
      } else {
        $.ajax({
          url: urlGenerateTransferRecords,
          method: 'POST',
          data: {
            center_id: centerId
          },
          dataType: 'json',
          success: function(response) {
            toastr.success("Successfully generated records. Redirecting...");
            window.location.href = "/member_transfer_requests";
          },
          error: function(response) {
            console.log(JSON.parse(response.responseText));
            toastr.error("Error. Something went wrong");
            $btnConfirmGenerate.prop("disabled", false);
            $centerSelect.prop("disabled", false);
          }
        });
      }
    });
  };

  return {
    init: init
  };
})();
