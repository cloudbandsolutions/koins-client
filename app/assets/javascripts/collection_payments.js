/**
 * Recomputes all the total values for cash payments by loan product
 */
function recomputeTotalCpAmount() {
  $(".total-cp-amount").html("0");
  $(".cp_amount_input").each(function(index) {
    var cpAmount = parseFloat($(this).val());
    if(isNaN(cpAmount)) {
      cpAmount = 0.00;
    }
    var loanProduct = $(this).data("loan-product");
    var totalCpAmount = parseFloat($("#cp_amount_total_" + loanProduct).html());
    var newTotalCpAmount = totalCpAmount + cpAmount;
    $("#cp_amount_total_" + loanProduct).html(newTotalCpAmount);
  });
}

/**
 * Recomputes all the total values for wp1 by loan product
 */
function recomputeTotalWpAmount() {
  $(".total-wp-amount").html("0");
  $(".wp_amount_input").each(function(index) {
    var wpAmount = parseFloat($(this).val());
    if(isNaN(wpAmount)) {
      wpAmount = 0.00;
    }
    var loanProduct = $(this).data("loan-product");
    var totalWpAmount = parseFloat($("#wp_amount_total_" + loanProduct).html());
    var newTotalWpAmount = totalWpAmount + wpAmount;
    $("#wp_amount_total_" + loanProduct).html(newTotalWpAmount);
  });
}

/**
 * Recomputes all the total values for wp2 by loan product
 */
function recomputeTotalCoMakerWpAmount() {
  $(".total-co-maker-wp-amount").html("0");
  $(".co_maker_wp_amount_input").each(function(index) {
    var wpAmount = parseFloat($(this).val());
    if(isNaN(wpAmount)) {
      wpAmount = 0.00;
    }
    var loanProduct = $(this).data("loan-product");
    var totalWpAmount = parseFloat($("#co_maker_wp_amount_total_" + loanProduct).html());
    var newTotalWpAmount = totalWpAmount + wpAmount;
    $("#co_maker_wp_amount_total_" + loanProduct).html(newTotalWpAmount);
  });
}

/**
 * Recomputes all the total values for loan payments by loan product
 */
function recomputeTotalLpAmount() {
  $(".total-lp-amount").html("0");
  $(".lp_amount_input").each(function(index) {
    var lpAmount = parseFloat($(this).val());
    if(isNaN(lpAmount)) {
      lpAmount = 0.00;
    }
    var loanProduct = $(this).data("loan-product");
    var totalLpAmount = parseFloat($("#lp_amount_total_" + loanProduct).html());
    var newTotalLpAmount = totalLpAmount + lpAmount;
    $("#lp_amount_total_" + loanProduct).html(newTotalLpAmount);
  });
}

/**
 * Recomputes all the total values for savings depoists by loan product and savings type
 */
function recomputeTotalSavingsAmount() {
  $(".total-savings-amount").html("0");
  $(".savings_amount_input").each(function(index) {
    var savingsAmount = parseFloat($(this).val());
    if(isNaN(savingsAmount)) {
      savingsAmount = 0.00;
    }
    var loanProduct = $(this).data("loan-product");
    var savingsType = $(this).data("savings-type");
    var totalSavingsAmount = parseFloat($("#savings_amount_total_" + savingsType + "_" + loanProduct).html());
    var newTotalSavingsAmount = totalSavingsAmount + savingsAmount;
    $("#savings_amount_total_" + savingsType + "_" + loanProduct).html(newTotalSavingsAmount);
  });
}

/**
 * Recomputes all the total values for insurance depoists by loan product and insurance type
 */
function recomputeTotalInsuranceAmount() {
  $(".total-insurance-amount").html("0");
  $(".insurance_amount_input").each(function(index) {
    var insuranceAmount = parseFloat($(this).val());
    if(isNaN(insuranceAmount)) {
      insuranceAmount = 0.00;
    }
    var loanProduct = $(this).data("loan-product");
    var insuranceType = $(this).data("insurance-type");
    var totalInsuranceAmount = parseFloat($("#insurance_amount_total_" + insuranceType + "_" + loanProduct).html());
    var newTotalInsuranceAmount = totalInsuranceAmount + insuranceAmount;
    $("#insurance_amount_total_" + insuranceType + "_" + loanProduct).html(newTotalInsuranceAmount);
  });
}

function recomputeAmtRcvPerRow() {
  $(".transaction-table").each(function() {
    var total = 0.00;
    var $transactionTable = $(this);
    $transactionTable.find(".transaction-row").each(function() {
      var subTotal = 0.00;
      $(this).find(".green").each(function() {
        subTotal += parseFloat($(this).val());
      });

      $(this).find(".amt-recv").html("" + subTotal);

      total += subTotal;
    });

    $transactionTable.find(".total-amt-recv").html("" + total);
  });
}

function init() {
  $('.ui.accordion').accordion();
}

$(document).ready(function() {
  init();

  recomputeAmtRcvPerRow();

  recomputeTotalCpAmount();
  $('.cp_amount_input').each(function(index) {
    $(this).on('input', function() {
      recomputeTotalCpAmount();
      recomputeAmtRcvPerRow();
    });
  });

  recomputeTotalWpAmount();
  $('.wp_amount_input').each(function(index) {
    $(this).on('input', function() {
      recomputeTotalWpAmount();
      recomputeAmtRcvPerRow();
    });
  });

  recomputeTotalCoMakerWpAmount();
  $('.co_maker_wp_amount_input').each(function(index) {
    $(this).on('input', function() {
      recomputeTotalCoMakerWpAmount();
      recomputeAmtRcvPerRow();
    });
  });

  recomputeTotalLpAmount();
  $('.lp_amount_input').each(function(index) {
    $(this).on('input', function() {
      recomputeTotalLpAmount();
      recomputeAmtRcvPerRow();
    });
  });

  recomputeTotalSavingsAmount();
  $('.savings_amount_input').each(function(index) {
    $(this).on('input', function() {
      recomputeTotalSavingsAmount();
      recomputeAmtRcvPerRow();
    });
  });

  recomputeTotalInsuranceAmount();
  $('.insurance_amount_input').each(function(index) {
    $(this).on('input', function() {
      recomputeTotalInsuranceAmount();
      recomputeAmtRcvPerRow();
    });
  });
});
