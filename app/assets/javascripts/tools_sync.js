var toolsSync = (function() {
  var $syncBtn;
  var syncUrl = "/api/v1/sync";

  var _cacheDom = function() {
    $syncBtn = $("#sync-btn");
  }

  var _bindEvents = function() {
    $syncBtn.on('click', function() {
      $syncBtn.addClass('loading');

      $.ajax({
        url: syncUrl,
        method: 'GET',
        success: function(responseContent) {
          toastr.success("sync successful");
        },
        error: function(responseContent) {
          toastr.error("Something went wrong");
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  toolsSync.init();
});
