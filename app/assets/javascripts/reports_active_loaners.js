var reportsActiveLoaners = (function() {
  var data = null;
  var $activeLoanersSegment = $(".active-loaners-graph");
  var url = "/api/v1/reports/active_loaners_by_branch";
  var $filterBtn = $("#filterBtn");
  var $branchSelect = $("#branch-select");
  var $activeLoanersGraph = $(".active-loaners-graph");
  var $activeLoanersCanvas = $(".active-loaners-canvas");
  var ctx = $activeLoanersCanvas.get(0).getContext("2d");
  var lineChart = null;

  // Events
  $filterBtn.on('click', reloadData);

  function reloadData() {
    $activeLoanersSegment.find(".ui.dimmer.inverted").addClass("active");

    var self = $(this);

    var branchId = $branchSelect.val();
    var params = {
      branch_id: branchId
    };

    $.ajax({
      url: url,
      dataType: 'json',
      data: params,
      success: function(data) {
        if(lineChart != null) {
          lineChart.destroy();
        }
        var options = {};
        lineChart = new Chart(ctx).Line(data.data, options);

        var distributionTable = $('.distribution-table');
        var template = $('.distribution-table-template').html();
        distributionTable.html(Mustache.render(template, data));

        $activeLoanersSegment.find(".ui.dimmer.inverted").removeClass("active");
      },
      error: function() {
        toastr.error("Error in getting active loaners for branch " + branchId);
        $activeLoanersSegment.find(".ui.dimmer.inverted").removeClass("active");
      }
    });
  }

  return {
    reloadData: reloadData
  }
})()
