var ReinstatementForm = (function() {
  var $parameters              = $("#parameters");
  var reinstatementId          = $parameters.data('reinstatement-id');
  var $section                 = $(".transaction-table");
  var $btnDelete               = $(".btn-delete");
  var $modalLoading            = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $memberSelect            = $("#member-select");
  var $btnAddMember            = $("#btn-add-member");

 
  // Hide controls
  $(".modal-loading").find('.controls').hide();

  var urlDeleteReinstatementRecord      = "/api/v1/reinstatements/delete_reinstatement_record";
  var urlAddMember                      = "/api/v1/reinstatements/add_member";

  var _bindEvents = function() {
    $btnAddMember.on('click', function() {
      $modalLoading.open();
      if(!$btnAddMember.hasClass('loading')) {
        $(this).addClass('loading');
        $(this).addClass('disabled');
        var memberId = $memberSelect.val();

        $.ajax({
          url: urlAddMember,
          method: 'POST',
          dataType: 'json',
          data: { id: reinstatementId, member_id: memberId },
          success: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            toastr.success("Successfully added member");
            window.location.href = "/reinstatements/" + reinstatementId + "/edit";
          },
          error: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            toastr.error(errorMessages);
            $modalLoading.close();
          }
        });
      } else {
        toastr.info("Still loading member");
      }
    });

    $btnDelete.on('click', function() {
      var $btn = $(this);
      $btn.addClass('loading');
      $btn.addClass('disabled');
      var reinstatementRecordId = $btn.data('reinstatement-record-id');

      $modalLoading.open();

      $.ajax({
        url: urlDeleteReinstatementRecord,
        method: 'POST',
        dataType: 'json',
        data: { reinstatement_record_id: reinstatementRecordId },
        success: function(responseContent) {
          toastr.success("Successfully deleted record");
          window.location.href = "/reinstatements/" + reinstatementId + "/edit";
        },
        error: function(responseContent) {
          toastr.error("Cannot delete this record");
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
          $modalLoading.close();
        }
      });
    });
  }


  var init = function() {
    _bindEvents();
  }

  return {
    init: init
  };
})();
