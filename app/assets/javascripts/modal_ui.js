$(document).ready(function() {
  var modalBranchSelect = $("#modal-branch-select");
  var modalCenterSelect = $("#modal-center-select");

  modalBranchSelect.bind('afterShow', function() {
    // Load default branches
    $.ajax({
      url: '/api/v1/utils/branches',
      type: 'get',
      dataType: 'json',
      success: function(data) {
        var branches = data.data.branches;
        populateSelect(this, branches, 'id', 'name');
      },
      error: function() {
        toastr.error("ERROR: loading branches");
      }
    });
  });

  // On change of branch select
  modalBranchSelect.bind('change', function() {
    var modalBranchId = ($(this).val());  
    params = { branch_id: modalBranchId };

    // Clear centers
    modalCenterSelect.find('option').remove();

    $.ajax({
      url: '/api/v1/utils/centers',
      type: 'get',
      dataType: 'json',
      data: params,
      success: function(data) {
        var centers = data.data.centers;
        populateSelect(modalCenterSelect, centers, 'id', 'name');
      },
      error: function() {
        toastr.error("ERROR: loading centers on branch select");
      }
    });
  });

  // Preload centers
  var modalBranchId = (modalBranchSelect.val());  
  params = { branch_id: modalBranchId };

  $.ajax({
    url: '/api/v1/utils/centers' ,
    type: 'get',
    dataType: 'json',
    data: params,
    success: function(data) {
      var centers = data.data.centers;
      populateSelect(modalCenterSelect, centers, 'id', 'name');
    },
    error: function() {
      toastr.error("ERROR: loading centers on branch select");
    }
  });
});
