var MembershipPaymentsPaymentCollectionForm = (function() {
  var $savingsTypes;
  var $insuranceTypes;
  var $equityTypes;
  var $membershipTypes;

  var $membershipPayments;
  var $savingsDeposits;
  var $insuranceDeposits;
  var $equityDeposits;
  var $grandTotal;
  var $section;
  var $totalMemberAmounts;

  var $parameters   = $("#parameters");
  var $btnAddMember = $("#btn-add-member");
  var $btnDelete    = $(".btn-delete");
  var $memberSelect = $("#member-select");
  var urlAddMember          = "/api/v1/payment_collections/add_member";
  var urlRemovePaymentCollectionRecord = "/api/v1/membership_payments/payment_collections/remove_payment_collection_record";
  var paymentCollectionId = $parameters.data('payment-collection-id');

  var _cacheDom = function() {
    $savingsTypes         = $parameters.data("savings-types");
    $insuranceTypes       = $parameters.data("insurance-types");
    $equityTypes          = $parameters.data("equity-types");
    $membershipTypes      = $parameters.data("membership-types");
    $idPayments           = $(".id-payment");
    $membershipPayments   = $(".membership-payment");
    $savingsDeposits      = $(".savings-deposit");
    $insuranceDeposits    = $(".insurance-deposit");
    $equityDeposits       = $(".equity-deposit");
    $grandTotal           = $(".grand-total");
    $section              = $(".transaction-table");
    $totalMemberAmounts   = $(".total-member-amount");
  }

  var _bindEvents = function() {
    $btnDelete.on('click', function() {
      var paymentCollectionRecordId = $(this).data('payment-collection-record-id');
      var $btn = $(this);

      $btn.addClass('loading');
      $btn.addClass('disabled');

      $.ajax({
        url: urlRemovePaymentCollectionRecord,
        method: 'POST',
        data: { 
          payment_collection_id: paymentCollectionId,
          payment_collection_record_id: paymentCollectionRecordId,
        },
        dataType: 'json',
        success: function(response) {
          toastr.success("Successfully removed payment collection record. Redirecting...");
          window.location = "/membership_payments/payment_collections/" + paymentCollectionId + "/edit";
        },
        error: function(response) {
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
          toastr.error("Error in removing payment collection record");
        }
      });
    });

    $btnAddMember.on('click', function() {
      if(!$(this).hasClass('loading')) {
        $(this).addClass('loading');
        $(this).addClass('disabled');
        var memberId = $memberSelect.val();

        $.ajax({
          url: urlAddMember,
          method: 'POST',
          dataType: 'json',
          data: { id: paymentCollectionId, member_id: memberId },
          success: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            toastr.success("Successfully added member");
            window.location.href = "/membership_payments/payment_collections/" + paymentCollectionId + "/edit";
          },
          error: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            toastr.error("Something went wrong when trying to add member");
            _displayErrors(errorMessages);
          }
        });
      }
    });

    var $totalIdPayments = $section.find(".total-id-payments");
    _computeMemberTotalAmount();
    _computeTotalIdPayments($section.find(".id-payment"), $totalIdPayments);

    $.each($membershipTypes, function() {
      var membershipType = this;
      var $totalMembershipPayments = $section.find(".total-membership-payments[data-membership-type-name='" + membershipType + "']");
      _computeMemberTotalAmount();
      _computeTotalMembershipPayments($section.find(".membership-payment[data-membership-type-name='" + membershipType + "']"), $totalMembershipPayments);
    });

    $idPayments.on('change', function() {
      _computeMemberTotalAmount();
      _computeTotalIdPayments($section.find(".id-payment"), $totalIdPayments);
    });

    $.each($savingsTypes, function() {
      var savingsType = this;
      var $totalSavings = $section.find(".total-savings[data-savings-type-code='" + savingsType + "']");
      _computeMemberTotalAmount();
      _computeTotalSavings($section.find(".savings-deposit[data-account-code='" + savingsType + "']"), $totalSavings);
    });

    $savingsDeposits.on('change', function() {
      var savingsType = $(this).data("account-code");
      var $totalSavings = $section.find(".total-savings[data-savings-type-code='" + savingsType + "']");
      _computeTotalSavings($section.find(".savings-deposit[data-account-code='" + savingsType + "']"), $totalSavings);
      _computeMemberTotalAmount();
      _computeTotalAmt($section);
    });

    $.each($insuranceTypes, function() {
      var insuranceType = this;
      var $totalInsuranceDeposits = $section.find(".total-insurance[data-insurance-type-code='" + insuranceType + "']");
      _computeMemberTotalAmount();
      _computeTotalInsuranceDeposits($section.find(".insurance-deposit[data-account-code='" + insuranceType + "']"), $totalInsuranceDeposits);
    });

    $insuranceDeposits.on('change', function() {
      var insuranceType = $(this).data("account-code");
      var $totalInsuranceDeposits = $section.find(".total-insurance[data-insurance-type-code='" + insuranceType + "']");
      _computeTotalInsuranceDeposits($section.find(".insurance-deposit[data-account-code='" + insuranceType + "']"), $totalInsuranceDeposits);
      _computeMemberTotalAmount();
      _computeTotalAmt($section);
    });

    $.each($equityTypes, function() {
      var equityType = this;
      var $totalEquityDeposits = $section.find(".total-equity[data-equity-type-code='" + equityType + "']");
      _computeMemberTotalAmount();
      _computeTotalEquityDeposits($section.find(".equity-deposit[data-account-code='" + equityType + "']"), $totalEquityDeposits);
    });

    $equityDeposits.on('change', function() {
      var equityType = $(this).data("account-code");
      var $totalEquityDeposits = $section.find(".total-equity[data-equity-type-code='" + equityType + "']");
      _computeTotalEquityDeposits($section.find(".equity-deposit[data-account-code='" + equityType + "']"), $totalEquityDeposits);
      _computeMemberTotalAmount();
      _computeTotalAmt($section);
    });

    _computeMemberTotalAmount();
    _computeTotalAmt($section);
  }

  var _computeMemberTotalAmount = function() {
    $.each($totalMemberAmounts, function() {
      var memberId = $(this).data("member-id");
      var t = 0.00;
      $.each($section.find(".cp-amount[data-member-id='" + memberId + "']"), function() {
        t += parseFloat($(this).val());
      });

      $(this).val(t);
    });
  }

  var _computeTotalAmt = function($section) {
    var grandTotal = 0.00;
    $.each($section.find(".total-savings"), function() {
      grandTotal += parseFloat($(this).val());
    });

    $.each($section.find(".total-insurance"), function() {
      grandTotal += parseFloat($(this).val());
    });

    $.each($section.find(".total-equity"), function() {
      grandTotal += parseFloat($(this).val());
    });

    $.each($section.find(".total-membership-payments"), function() {
      grandTotal += parseFloat($(this).val());
    });

    $.each($section.find(".total-id-payments"), function() {
      grandTotal += parseFloat($(this).val());
    });

    $section.find(".grand-total").val(grandTotal);
  }

  var _computeTotalIdPayments = function($idPayments, $totalIdPayments) {
    var t = 0.00;
    $idPayments.each(function() {
      t += parseFloat($(this).val());
    });

    $totalIdPayments.val(t);
  }

  var _computeTotalMembershipPayments = function($membershipPayments, $totalMembershipPayments) {
    var t = 0.00;
    $membershipPayments.each(function() {
      t += parseFloat($(this).val());
    });

    $totalMembershipPayments.val(t);
  }

  var _computeTotalSavings = function($savingsDeposits, $totalSavings) {
    var t = 0.00;
    $savingsDeposits.each(function() {
      t += parseFloat($(this).val());
    });

    $totalSavings.val(t);
  }

  var _computeTotalInsuranceDeposits = function($insuranceDeposits, $totalInsuranceDeposits) {
    var t = 0.00;
    $insuranceDeposits.each(function() {
      t += parseFloat($(this).val());
    });

    $totalInsuranceDeposits.val(t);
  }

  var _computeTotalEquityDeposits = function($equityDeposits, $totalEquityDeposits) {
    var t = 0.00;
    $equityDeposits.each(function() {
      t += parseFloat($(this).val());
    });

    $totalEquityDeposits.val(t);
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  MembershipPaymentsPaymentCollectionForm.init();
});
