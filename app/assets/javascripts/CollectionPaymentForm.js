var CollectionPaymentForm = (function() {
  var $wpRecordModal;
  var $wpRecordButtons;
  var $wpRecordNonPayingMemberButtons;
  var $btnAddWpRecord;
  var $btnAddWpRecordNonPayingMember;
  var $wpRecordMemberSelect;
  var $wpRecordAmountInput;
  var $wpRecordMemberSelectNonPayingMember;
  var $wpRecordAmountInputNonPayingMember;
  var loanPaymentId;
  var savingsAccountTransactionId;
  var memberFullName;
  var $wpRecordsTable;
  var $wpRecordsTbodyTemplate;
  var $wpRecordsTbodyTemplateNonPayingMember;
  var urlAddWpRecord                            = "/api/v1/loan_payments/add_wp_record";
  var urlAddWpRecordNonPayingMember             = "/api/v1/savings_account_transactions/add_wp_record";
  var urlWpRecordsFromLoanPayment               = "/api/v1/loan_payments/wp_records";
  var urlWpRecordsFromSavingsAccountTransaction = "/api/v1/savings_account_transactions/wp_records";
  var $wpRecordNonPayingMemberModal;

  var _cacheDom = function() {
    $wpRecordModal                          = $("#wp-record-modal");
    $wpRecordNonPayingMemberModal           = $("#wp-record-non-paying-member-modal");
    $wpRecordButtons                        = $(".wp-record-btn");
    $wpRecordNonPayingMemberButtons         = $(".wp-record-non-paying-member-btn");
    $wpRecordMemberSelect                   = $("#wp-record-member-select");
    $wpRecordAmountInput                    = $("#wp-record-amount-input");
    $wpRecordMemberSelectNonPayingMember    = $("#wp-record-member-select-non-paying-member");
    $wpRecordAmountInputNonPayingMember     = $("#wp-record-amount-input-non-paying-member");
    $btnAddWpRecord                         = $("#btn-add-wp-record");
    $btnAddWpRecordNonPayingMember          = $("#btn-add-wp-record-non-paying-member");
    $wpRecordsTbodyTemplate                 = $("#wp-records-tbody-template");
    $wpRecordsTbodyTemplateNonPayingMember  = $("#wp-records-tbody-template-non-paying-member");
  }

  var _loadWpRecordsNonPayingMember = function() {
    $.ajax({
      url: urlWpRecordsFromSavingsAccountTransaction,
      data: { savings_account_transaction_id: savingsAccountTransactionId },
      dataType: 'json',
      method: 'GET',
      success: function(responseContent) {
        $wpRecordNonPayingMemberModal.find("tbody").html(Mustache.render($wpRecordsTbodyTemplateNonPayingMember.html(), responseContent));
      },
      error: function(responseContent) {
        toastr.error("Error in getting wp records from savings account transaction with id: " + savingsAccountTransactionId);
      }
    });
  }

  var _loadWpRecords = function() {
    $.ajax({
      url: urlWpRecordsFromLoanPayment,
      data: { loan_payment_id: loanPaymentId },
      dataType: 'json',
      method: 'GET',
      success: function(responseContent) {
        $wpRecordModal.find("tbody").html(Mustache.render($wpRecordsTbodyTemplate.html(), responseContent));
      },
      error: function(responseContent) {
        toastr.error("Error in getting wp records from loan payment with id: " + loanPaymentId);
      }
    });
  }

  var _bindEvents = function() {
    $wpRecordNonPayingMemberButtons.on('click', function() {
      savingsAccountTransactionId = $(this).data('savings-account-transaction-id');
      memberFullName = $(this).data('member-name');

      $wpRecordNonPayingMemberModal.modal('show');
      $wpRecordNonPayingMemberModal.find("h3 small").html(memberFullName);
      $wpRecordNonPayingMemberModal.find("tbody").html("");
      _loadWpRecordsNonPayingMember();
    });

    $wpRecordButtons.on('click', function() {
      loanPaymentId = $(this).data('loan-payment-id');
      memberFullName = $(this).data('member-name');

      $wpRecordModal.modal('show');
      $wpRecordModal.find("h3 small").html(memberFullName);
      $wpRecordModal.find("tbody").html("");
      _loadWpRecords();
    });

    $btnAddWpRecordNonPayingMember.on('click', function() {
      $btnAddWpRecordNonPayingMember.addClass('loading');

      var memberId = $wpRecordMemberSelectNonPayingMember.val();
      var amount = $wpRecordAmountInputNonPayingMember.val();

      var valid = true;
      if(!memberId) {
        toastr.error("No member selected");
        valid = false;
      }

      if(!amount) {
        toastr.error("No amount specified");
        valid = false;
      }

      if(valid) {
        $.ajax({
          url: urlAddWpRecordNonPayingMember,
          data: { savings_account_transaction_id: savingsAccountTransactionId, member_id: memberId, amount: amount },
          dataType: 'json',
          method: 'POST',
          success: function(responseContent) {
            toastr.success("Successfully added record");
            _loadWpRecordsNonPayingMember();
            $("[data-savings-account-transaction-id='" + savingsAccountTransactionId + "']").html(responseContent.amount)
            $wpRecordAmountInputNonPayingMember.val(0);
            $btnAddWpRecordNonPayingMember.removeClass('loading');
          },
          error: function(responseContent) {
            toastr.error("Cannot add this withdraw payment record");
            $btnAddWpRecordNonPayingMember.removeClass('loading');
          }
        });
      }
    });

    $btnAddWpRecord.on('click', function() {
      $btnAddWpRecord.addClass('loading');

      var memberId = $wpRecordMemberSelect.val();
      var amount = $wpRecordAmountInput.val();

      var valid = true;
      if(!memberId) {
        toastr.error("No member selected");
        valid = false;
      }

      if(!amount) {
        toastr.error("No amount specified");
        valid = false;
      }

      if(valid) {
        $.ajax({
          url: urlAddWpRecord,
          data: { loan_payment_id: loanPaymentId, member_id: memberId, amount: amount },
          dataType: 'json',
          method: 'POST',
          success: function(responseContent) {
            toastr.success("Successfully added record");
            _loadWpRecords();
            $("[data-loan-payment-id='" + loanPaymentId + "']").html(responseContent.wp_amount)
            $wpRecordAmountInput.val(0);
            $btnAddWpRecord.removeClass('loading');
          },
          error: function(responseContent) {
            toastr.error("Cannot add this withdraw payment record");
            $btnAddWpRecord.removeClass('loading');
          }
        });
      }
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  } 

  return {
    init: init
  };
})();

$(document).ready(function() {
  CollectionPaymentForm.init();
});
