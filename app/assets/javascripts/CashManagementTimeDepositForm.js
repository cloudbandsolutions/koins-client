var CashManagementTimeDepositForm = (function(){
  var $btnNew                   = $("#btn-new");
  var $cbMemberAccountId        = $("#member_account_id");
  var $txtLockInAmout           = $("#lock-in-amout");
  var $dtStartDate              = $("#start-date");
  var $txtLockInPeriod          = $("#lock-in-period");
  var $txtOrNumber              = $("#or_number");
  var $cbBook                   = $("#book");
  var $templateTimeDepositList  = $("#template-time-deposit-list");
  var $timeDepositSection       = $("#time-deposit-section");
  var $btnSave                  = $("#btn-save");
  var $txtOrNumber              = $("#or_number");
  var $txtParticular            = $("#particular");


  var $payment_ids = $("#parameters"); 

  var urlGenerateTimeDepositTransaction = "/api/v1/cash_management/time_deposit/payment_collections/save_member";
  var urlSaveTimeDepositTransaction = "/api/v1/cash_management/time_deposit/payment_collections/save_payment_collection";


  var init = function(){
    $btnSave.on("click", function(){
      var params = {
       payment_ids: $payment_ids.val(),
       or_number: $txtOrNumber.val(),
       book: $cbBook.val(),
       particular: $txtParticular.val()
      }


      $.ajax({
        url: urlSaveTimeDepositTransaction,
        method: "POST",
        dataType: 'json',
        data: params,
        success: function(response){
          pc_id = response.payment_collection_id 
          
          window.location.href = "/cash_management/time_deposits/operation_details/" + $payment_ids.val()
        }
      })
    
    })


    $btnNew.on("click", function(){
      var params = {
        member_account_id: $cbMemberAccountId.val(),
        lock_in_amout: $txtLockInAmout.val(),
        start_date: $dtStartDate.val(),
        lock_in_period: $txtLockInPeriod.val(),
        book: $cbBook.val(),
        payment_ids: $payment_ids.val()
      } 

      $.ajax({
        url: urlGenerateTimeDepositTransaction,
        method: "POST",
        dataType: 'json',
        data: params,
        success: function(response){
        
          location.reload();
          //console.log(response);
          //$timeDepositSection.html(
            //Mustache.render(
              //$templateTimeDepositList.html(),
              //response.data
            //)//end ng Mustache
          //); //end ng timedepositSection
        }, //end ng success
        error: function(response){
          toastr.error("Error");
        }
      
      });
    
    });
  };
  return {
    init: init
  }

})();
