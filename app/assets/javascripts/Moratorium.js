var Moratorium = (function() {
  var startOfMoratorium;
  var description;
  var centerIds;
  var numberOfDays;
  var $inputStartOfMoratorium;
  var $inputNumberOfDays;
  var $centersSelect;
  var $btnInit;
  var $errors;
  var $errorsTemplate;
  var batchMoratoriumId;
  var $parameters;
  var reason;
  var $reason;
  var $currentMoratoriumTemplate;
  var $btnApprove;
  var urlInitMoratorium = "/api/v1/moratorium/init";
  var urlApproveMoratorium = "/api/v1/moratorium/approve";

  var _validateInput = function() {
    var errors = [];

    if(!startOfMoratorium) {
      errors.push("No start of moratorium specified");
    }

    if(!numberOfDays) {
      errors.push("Number of days not specified");
    }

    return errors;
  }

  var _buildValues = function() {
    startOfMoratorium = $inputStartOfMoratorium.val();
    centerIds         = $centersSelect.val();
    numberOfDays      = $inputNumberOfDays.val();
    reason            = $reason.val();
  }

  var _clearErrors = function() {
    $errors.html("");
  }

  var _cacheDom = function() {
    $btnInit                    = $("#btn-init");
    $errors                     = $("#errors");
    $errorsTemplate             = $("#errors-template");
    $inputStartOfMoratorium     = $("#start-of-moratorium");
    $centersSelect              = $("#centers-select");
    $inputNumberOfDays          = $("#number-of-days");
    $reason                     = $("#reason");
    $parameters                 = $("#parameters");
    $currentMoratorium          = $("#current-moratorium");
    batchMoratoriumId           = $parameters.data('batch-moratorium-id');
    $currentMoratoriumTemplate  = $("#current-moratorium-template");
    $btnApprove                 = $("#btn-approve");
    $btnApprove.hide();
  }

  var _enableButtons = function() {
    $btnApprove.removeClass('loading');
    $btnInit.removeClass('loading');
  }

  var _disableButtons = function() {
    $btnApprove.addClass('loading');
    $btnInit.addClass('loading');
  }

  var _bindEvents = function() {
    $btnApprove.on('click', function() {
      toastr.info("approving batch moratorium " + batchMoratoriumId);
      _disableButtons();
    });

    $btnInit.on('click', function() {
      _disableButtons();
      _clearErrors();
      _buildValues();

      var errors = _validateInput();
      if(errors.length > 0) {
        _clearErrors();
        $errors.html(Mustache.render($errorsTemplate.html(), { errors: errors }));
        _enableButtons();
      } else {
        data = {
          start_of_moratorium: startOfMoratorium,
          center_ids: centerIds,
          number_of_days: numberOfDays,
          batch_moratorium_id: batchMoratoriumId,
          reason: reason
        };
        _clearErrors();
        $.ajax({
          url: urlInitMoratorium,
          dataType: 'json',
          data: data,
          method: 'POST',
          success: function(responseContent) {
            console.log(responseContent);
            toastr.info("Generating moratorium");
            _enableButtons();
            $currentMoratorium.html(Mustache.render($currentMoratoriumTemplate.html(), responseContent));
            $parameters.data('batch-moratorium-id', responseContent.batch_moratorium.id);
            batchMoratoriumId = responseContent.batch_moratorium.id;
            $btnApprove.show();
          },
          error: function(responseContent) {
            console.log(responseContent);
            $errors.html(Mustache.render($errorsTemplate.html(), { errors: JSON.parse(responseContent.responseText).errors }));
            _enableButtons();
            toastr.error("Something went wrong while initializing moratorium");
          }
        });
      }
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }
  
  return {
    init: init
  };
})();

$(document).ready(function() {
  Moratorium.init();
});
