$(document).ready(function() {
  $("#generateInterestTaxBtn").on('click', function() {
    $("#interest-tax-generation-section").addClass("active"); 
    $.ajax({
      url: '/api/v1/savings_accounts/generate_interest_and_tax_transactions',
      dataType: 'json',
      method: 'POST',
      success: function(data) {
        $("#interest-tax-generation-section").removeClass("active"); 
        toastr.success("Successfully generated interest and tax transactions");
      },
      error: function() {
        $("#interest-tax-generation-section").removeClass("active"); 
        toastr.error("Error in generating interest and tax");
      }
    });
  });
});
