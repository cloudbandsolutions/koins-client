//= require_directory ./lib

Closing.TrialBalance = (function() {
  var $trialBalanceSection  = $("#trial-balance-section");
  var $trialBalanceTemplate = $("#trial-balance-template").html();
  var trialBalanceData      = $trialBalanceSection.data('trial-balance');

  var init  = function() {
    $trialBalanceSection.html(
      Mustache.render(
        $trialBalanceTemplate,
        trialBalanceData
      )
    );
  };

  return {
    init: init
  };
})();
