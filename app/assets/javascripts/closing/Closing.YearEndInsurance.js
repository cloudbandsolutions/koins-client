//= require_directory ./lib

Closing.YearEndInsurance = (function() {
  var $startDate    = $("#start-date");
  var $endDate      = $("#end-date");
  var urlGenerate   = "/api/v1/accounting/closing/year_end_insurance";
  var urlApprove    = "/api/v1/accounting/closing/approve_year_end_insurance";
  var $yearEndClosingInsuranceTemplate = $("#year-end-closing-insurance-template");
  var $yearEndClosingInsuranceSection  = $("#year-end-closing-insurance-section");
  var $modalLoading           = $(".modal-loading").remodal({
                                  hashTracking: true,
                                  closeOnOutsideClick: false
                                });

  var init = function() {
    $.ajax({
      url: urlGenerate,
      method: 'GET',
      success: function(response) {
        console.log(response);
        $yearEndClosingInsuranceSection.html(
          Mustache.render(
            $yearEndClosingInsuranceTemplate.html(),
            { data: response.data }
          )
        );

        $(".curr").each(function() {
          $(this).html(numberWithCommas($(this).html()));
        });
          
        initApproveClosing();
      },
      error: function(response) {
        toastr.error("Error in generating year end preview");
      }
    });
    initGenerateClosing();
  };

  var initApproveClosing = function() {
    $("#btn-approve").on('click', function() {
      $(".modal-loading").find(".controls").hide();
      $modalLoading.open();

      $.ajax({
        url: urlApprove,
        method: 'POST',
        success: function(response) {
          toastr.success("Successfully closed the books for year");
          $modalLoading.close();
          window.location.href = "/";
        },
        error: function(response) {
          toastr.error("Error in closing year");
          $modalLoading.close();
        }
      });
    });
  };

  var initGenerateClosing = function() {
    $("#generate-btn").on('click', function() {
      $(".modal-loading").find(".controls").hide();
      $modalLoading.open();

      data = {
        start_date: $startDate.val(),
        end_date: $endDate.val(),
      };
      $modalLoading.close();

      window.location = "/api/v1/accounting/closing/year_end_insurance?" + encodeQueryData(data);
    });
  };

  return {
    init: init
  };
})();
