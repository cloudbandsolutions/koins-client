//= require_directory ./lib

Closing.MonthlyInterestAndTaxShow = (function() {
  var $parameters               = $("#parameters");
  var $btnDelete                = $("#btn-delete");
  var $btnApprove               = $("#btn-approve");
  var $btnVoid                  = $("#btn-void");

  var $modalConfirmDelete       = $("#modal-confirm-delete").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $btnConfirmDelete         = $("#modal-confirm-delete").find(".btn-confirm");
  var $btnCancelDelete          = $("#modal-confirm-delete").find(".btn-cancel");
  var $controlsConfirmDelete    = $("#modal-confirm-delete").find(".controls");
  var $confirmDeleteMessage     = $("#modal-confirm-delete").find(".message");

  var $modalConfirmApprove      = $("#modal-confirm-approve").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $btnConfirmApprove        = $("#modal-confirm-approve").find(".btn-confirm");
  var $btnCancelApprove         = $("#modal-confirm-approve").find(".btn-cancel");
  var $controlsConfirmApprove   = $("#modal-confirm-approve").find(".controls");
  var $confirmApproveMessage    = $("#modal-confirm-approve").find(".message");

  var $modalConfirmVoid         = $("#modal-confirm-void").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $btnConfirmVoid           = $("#modal-confirm-void").find(".btn-confirm");
  var $btnCancelVoid            = $("#modal-confirm-void").find(".btn-cancel");
  var $controlsConfirmVoid      = $("#modal-confirm-void").find(".controls");
  var $confirmVoidMessage       = $("#modal-confirm-void").find(".message");

  var $errorsTemplate           = $("#errors-template");

  var urlDelete                 = "/api/v1/cash_management/closing/monthly_interest_and_tax_closing_records/destroy";
  var urlApprove                = "/api/v1/cash_management/closing/monthly_interest_and_tax_closing_records/approve";
  var urlVoid                   = "/api/v1/cash_management/closing/monthly_interest_and_tax_closing_records/void";

  var id                        = $parameters.data('id');

  var init = function() {
    $btnVoid.on('click', function() {
      $modalConfirmVoid.open();
    });

    $btnConfirmVoid.on('click', function() {
      $controlsConfirmVoid.hide();
      $confirmVoidMessage.html("Loading...");

      $.ajax({
        url: urlVoid,
        data: {
          id: id
        },
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          $confirmApproveMessage.html("Successfully voided record. Redirecting...");
          window.location.href = "/cash_management/operations/monthly_interest_and_tax_closing_records";
        },
        error: function(response) {
          $confirmVoidMessage.html(Mustache.render($errorsTemplate.html(), JSON.parse(response.responseText)));
          $controlsConfirmVoid.show();
        }
      });
    });

    $btnCancelVoid.on('click', function() {
      $modalConfirmVoid.close();
    });

    $btnApprove.on('click', function() {
      $modalConfirmApprove.open();
    });

    $btnConfirmApprove.on('click', function() {
      $controlsConfirmApprove.hide();
      $confirmApproveMessage.html("Loading...");

      $.ajax({
        url: urlApprove,
        data: {
          id: id
        },
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          $confirmApproveMessage.html("Successfully approved record. Redirecting...");
          window.location.href = "/cash_management/operations/monthly_interest_and_tax_closing_records";
        },
        error: function(response) {
          $confirmApproveMessage.html(Mustache.render($errorsTemplate.html(), JSON.parse(response.responseText)));
          $controlsConfirmApprove.show();
        }
      });
    });

    $btnCancelApprove.on('click', function() {
      $modalConfirmApprove.close();
    });

    
    $btnDelete.on('click', function() {
      $modalConfirmDelete.open();
    });

    $btnConfirmDelete.on('click', function() {
      $controlsConfirmDelete.hide();
      $confirmDeleteMessage.html("Loading...");

      $.ajax({
        url: urlDelete,
        data: {
          id: id
        },
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          $confirmDeleteMessage.html("Successfully deleted record. Redirecting...");
          window.location.href = "/cash_management/operations/monthly_interest_and_tax_closing_records";
        },
        error: function(response) {
          $confirmDeleteMessage.html(Mustache.render($errorsTemplate.html(), JSON.parse(response.responseText)));
          $controlsConfirmDelete.show();
        }
      });
    });

    $btnCancelDelete.on('click', function() {
      $modalConfirmDelete.close();
    });
  };

  return {
    init: init
  };
})();
