//= require_directory ./lib

Closing.MonthlyInterestAndTaxIndex = (function() {
  var $btnNew             = $("#btn-new");
  var $modalConfirm       = $("#modal-confirm").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $modalControls      = $("#modal-controls");
  var $modalMessage       = $("#modal-confirm").find(".message");
  var $btnConfirm         = $("#btn-confirm");
  var $btnCancel          = $("#btn-cancel");
  var $closingMonth       = $("#closing-month-select");
  var $errorsTemplate     = $("#errors-template");
  var $savingsTypeSelect  = $("#savings-type-select");
  var $specialFlag        = $(".switch");

  var urlCreateMonthlyInterestAndTaxClosingRecord = "/api/v1/cash_management/closing/monthly_interest_and_tax_closing_records/create";

  var _bindEvents = function() {
    $btnNew.on('click', function() {
      $modalConfirm.open();
    });

    $btnConfirm.on('click', function() {
      $modalControls.hide();
      $modalMessage.html("Loading...");

      var special = "";
      if($specialFlag.bootstrapSwitch('state')) {
        special = true;
      }

      $.ajax({
        url: urlCreateMonthlyInterestAndTaxClosingRecord,
        method: 'POST',
        data: {
          closing_month: $closingMonth.val(),
          special: special,
          savings_type_id: $savingsTypeSelect.val()
        },
        dataType: 'json',
        success: function(response) {
          $modalMessage.html("Success! Redirecting...")
          window.location.href = "/cash_management/operations/monthly_interest_and_tax_closing_records";
        },
        error: function(response) {
          $modalMessage.html(Mustache.render($errorsTemplate.html(), JSON.parse(response.responseText)));
          $modalControls.show();
        },
      });
    });

    $btnCancel.on('click', function() {
      $modalConfirm.close();
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
