//= require_directory ./lib

Closing.YearEnd = (function() {
  var urlGenerate   = "/api/v1/accounting/closing/year_end";
  var urlApprove    = "/api/v1/accounting/closing/approve_year_end";
  var $yearEndClosingTemplate = $("#year-end-closing-template");
  var $yearEndClosingSection  = $("#year-end-closing-section");
  var $modalLoading           = $(".modal-loading").remodal({
                                  hashTracking: true,
                                  closeOnOutsideClick: false
                                });

  var init = function() {
    $.ajax({
      url: urlGenerate,
      method: 'GET',
      success: function(response) {
        $yearEndClosingSection.html(
          Mustache.render(
            $yearEndClosingTemplate.html(),
            { data: response.data }
          )
        );

        $(".curr").each(function() {
          $(this).html(numberWithCommas($(this).html()));
        });
          
        initApproveClosing();
      },
      error: function(response) {
        toastr.error("Error in generating year end preview");
      }
    });
  };

  var initApproveClosing = function() {
    $("#btn-approve").on('click', function() {
      $(".modal-loading").find(".controls").hide();
      $modalLoading.open();

      $.ajax({
        url: urlApprove,
        method: 'POST',
        success: function(response) {
          toastr.success("Successfully closed the books for year");
          $modalLoading.close();
          window.location.href = "/";
        },
        error: function(response) {
          toastr.error("Error in closing year");
          $modalLoading.close();
        }
      });
    });
  };

  return {
    init: init
  };
})();
