var UploadInsuranceFundTransfer = (function() {
  var $branchSelect;
  var $dateOfPayment;

  var urlUtilsBranches  = "/api/v1/utils/branches";


  var _cacheDom = function() {
    $branchSelect        = $("#branch-select");
    $dateOfPayment       = $("#date-of-payment");
      }


  var _bindEvents = function() {
    
    $branchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($branchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  UploadInsuranceFundTransfer.init();
});
