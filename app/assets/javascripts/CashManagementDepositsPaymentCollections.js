var CashManagementDepositsPaymentCollections = (function() {
  var $modalBranchSelect;
  var $dateOfPayment;
  var $btnGenerateBilling;
  var $btnCancelGenerateBilling;
  var $btnFilter;
  var $btnNewDeposit;
  var $generateBillingModal;

  var urlUtilsBranches  = "/api/v1/utils/branches";
  var urlUtilsCenters   = "/api/v1/utils/centers";
  var urlNewBilling     = "/cash_management/deposits/payment_collections/new";

  var $filterOrNumber; 
  var $filterStartDate;
  var $filterEndDate;
  var $filterBranchSelect;
  var $filterStatus;
  var $filterCenterSelect;

  var $errors;  
  var $errorsTemplate;
  var $modalErrors;
  var $modalSuccess;
  var $modalControls;
  var $successTemplate;
  var $clickableRow; 

  var $modalLoading;
  var $modalLoadingErrors;
  var $modalLoadingSuccess; 
  var $modalLoadingControls;
  var $modalLoadingBtnClose;

  var urlGenerateDepositTransaction = "/api/v1/cash_management/deposits/payment_collections/generate_transaction";

  var _cacheDom = function() {
    $modalBranchSelect        = $("#modal-branch-select");
    $btnGenerateBilling       = $("#btn-generate-billing");
    $btnCancelGenerateBilling = $("#btn-cancel-generate-billing");
    $dateOfPayment            = $("#date-of-payment");
    $generateBillingModal     = $(".modal-generate-deposit-payment-collection").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $btnNewDeposit            = $("#btn-new-deposit");
    $modalErrors              = $(".modal-generate-deposit-payment-collection").find(".errors");
    $errors                   = $(".errors");
    $modalSuccess             = $(".modal-generate-deposit-payment-collection").find(".success");
    $modalControls            = $(".modal-generate-deposit-payment-collection").find(".controls");
    $errorsTemplate           = $("#errors-template");
    $successTemplate          = $("#success-template");
    $clickableRow             = $(".clickable");
    $btnFilter                = $("#btn-filter");

    $filterOrNumber           = $("#filter-or-number");
    $filterStartDate          = $("#filter-start-date");
    $filterEndDate            = $("#filter-end-date");
    $filterBranchSelect       = $("#filter-branch-select");
    $filterStatus             = $("#filter-status");
    $filterCenterSelect       = $("#filter-center-select");

    $modalLoading             = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingErrors       = $(".modal-loading").find(".errors");
    $modalLoadingSuccess      = $(".modal-loading").find(".success");
    $modalLoadingControls     = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
    $modalLoadingBtnClose     = $(".modal-loading").find(".btn-close");
  }

  var _addLoadingToConfirmationBtns = function() {
    $btnGenerateBilling.addClass('loading');
    $btnGenerateBilling.addClass('disabled');

    $btnCancelGenerateBilling.addClass('loading');
    $btnCancelGenerateBilling.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnGenerateBilling.removeClass('loading');
    $btnGenerateBilling.removeClass('disabled');

    $btnCancelGenerateBilling.removeClass('loading');
    $btnCancelGenerateBilling.removeClass('disabled');
  }

  var _bindEvents = function() {
    $modalLoadingBtnClose.on('click', function() {
      $modalLoading.close();
    });

    $btnFilter.on('click', function() { 
      $modalLoadingControls.hide();

      var orNumber  = $filterOrNumber.val();
      var startDate = $filterStartDate.val();
      var endDate   = $filterEndDate.val();
      var branchId  = $filterBranchSelect.val();
      var tStatus   = $filterStatus.val();
      var centerId  = $filterCenterSelect.val();

      var data = {
        or_number: orNumber,
        start_date: startDate,
        end_date: endDate,
        t_status: tStatus,
        center_id: centerId,
        branch_id: branchId
      };

      $modalLoading.open();

      window.location = "/cash_management/deposits/payment_collections?" + encodeQueryData(data);
    });

    $clickableRow.on('click', function() {
      transactionId = $(this).data('transaction-id');
      window.location = "/cash_management/deposits/payment_collections/" + transactionId;
    });

    $btnNewDeposit.on('click', function() {
      $generateBillingModal.open();
    });

    $btnCancelGenerateBilling.on('click', function() {
      if(!$btnGenerateBilling.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $generateBillingModal.close();
        _removeLoadingToConfirmationBtns();
      } else {
        toastr.info("Still loading");
      }
    });

    $btnGenerateBilling.on('click', function() {
      var branchId      = $modalBranchSelect.val();
      var dateOfPayment = $dateOfPayment.val();
      var data = {
        branch_id: branchId,
        date_of_payment: dateOfPayment
      };

      if(!$btnGenerateBilling.hasClass('loading')) {
        $modalErrors.html("");
        $modalSuccess.html("");
        _addLoadingToConfirmationBtns();
        $.ajax({
          url: urlGenerateDepositTransaction,
          data: data,
          dataType: 'json',
          method: 'POST',
          success: function(responseContent) {
            console.log(responseContent);
            _removeLoadingToConfirmationBtns();
            $modalSuccess.html(Mustache.render($successTemplate.html(), { messages: responseContent.messages }));
            $modalControls.hide();
            pc_id = responseContent.payment_collection_id;
            window.location = "/cash_management/deposits/payment_collections/" + pc_id + "/edit";
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            console.log(errorMessages);
            $modalErrors.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to generate deposit transaction");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $modalBranchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($modalBranchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
