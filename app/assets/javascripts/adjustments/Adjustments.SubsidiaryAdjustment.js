//= require_directory ./lib

Adjustments.SubsidiaryAdjustment = (function() {
  var $parameters               = $("#parameters");
  var $modalErrors              = $(".modal-approve-confirmation").find(".errors");
  var $modalMessage              = $(".modal-approve-confirmation").find(".message");
  var $errorsTemplate           = $("#errors-template");

  var $btnApprove               = $("#btn-approve");
  var $modalApproveConfirmation = $(".modal-approve-confirmation").remodal({hashTracking: true, closeOnOutsideClick: false});
  var $modalControlsApproval    = $(".modal-approve-confirmation").find(".controls");
  var $btnConfirmApproval       = $("#btn-confirm-approval");
  var $btnCancelApproval        = $("#btn-cancel-approval");

  var id                        = $parameters.data('id');

  var urlApproveTransaction     = "/api/v1/adjustments/subsidiary_adjustments/approve";

  var init = function() {
    $btnApprove.on('click', function() {
      $modalApproveConfirmation.open();
    });

    $btnCancelApproval.on('click', function() {
      $modalApproveConfirmation.close();
    });

    $btnConfirmApproval.on('click', function() {
      $modalErrors.html("");
      $modalMessage.html("Processing...");
      $modalControlsApproval.hide();

      $.ajax({
        url: urlApproveTransaction,
        method: 'POST',
        dataType: 'json',
        data: {
          id: id
        },
        success: function(responseContent) {
          $modalMessage.html("Success! Redirecting...");
          toastr.success("Successfully approved transaction");
          window.location.href = "/adjustments/subsidiary_adjustments/" + id;
        },
        error: function(responseContent) {
          console.log(responseContent);
          $modalControlsApproval.show();
          $modalMessage.html("Error!");
          $modalErrors.html(Mustache.render(
            $errorsTemplate.html(), 
            { errors: JSON.parse(responseContent.responseText).errors }));
        }
      });
    });
  };

  return {
    init: init
  };
})();
