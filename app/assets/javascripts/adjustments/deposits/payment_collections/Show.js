var Show = (function() {
  var $btnApprove;

  var $btnApproveConfirmation;
  var $btnCancelApproval;     

  var $btnReverseConfirmation;
  var $btnCancelReverse;

  var $btnAddMember;
  var $btnDelete;
  var $parameters;
  var $memberSelect;
  var paymentCollectionId;
  var $errors;
  var $errorsTemplate;
  var urlApproveTransaction             = "/api/v1/payment_collections/approve";
  var urlDeletePaymentCollectionRecord  = "/api/v1/payment_collections/delete_payment_collection_record";

  var $modalApprove;
  var $modalReverse;
  var $errors;
  var $errorsTemplate;
  var $modalErrorsApproval;
  var $modalErrorsReverse;
  var $modalSuccessApproval;
  var $modalSuccessReverse;
  var $modalControls;
  var $successTemplate;

  var _displayErrors = function(errors) {
    var errorsDisplay = Mustache.render($errorsTemplate.html(), { errors: errors });
    $errors.html(errorsDisplay);
  }

  var _hideErrors = function() {
    $errors.html("");
  }

  var _addLoadingToConfirmationBtns = function() {
    $btnApproveConfirmation.addClass('loading');
    $btnApproveConfirmation.addClass('disabled');

    $btnCancelApproval.addClass('loading');
    $btnCancelApproval.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnApproveConfirmation.removeClass('loading');
    $btnApproveConfirmation.removeClass('disabled');

    $btnCancelApproval.removeClass('loading');
    $btnCancelApproval.removeClass('disabled');
  }

  var _bindEvents = function() {
    $btnDelete.on('click', function() {
      var $btn = $(this);
      $btn.addClass('loading');
      $btn.addClass('disabled');
      var paymentCollectionRecordId = $btn.data('payment-collection-record-id');

      $.ajax({
        url: urlDeletePaymentCollectionRecord,
        method: 'POST',
        dataType: 'json',
        data: { payment_collection_record_id: paymentCollectionRecordId },
        success: function(responseContent) {
          toastr.success("Successfully deleted record");
          window.location.href = "/adjustments/deposits/payment_collections/" + paymentCollectionId;
        },
        error: function(responseContent) {
          toastr.error("Cannot delete this record");
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
        }
      });
    });

    $btnApproveConfirmation.on('click', function() {
      if(!$btnApproveConfirmation.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $.ajax({
          url: urlApproveTransaction,
          method: 'POST',
          dataType: 'json',
          data: { id: paymentCollectionId },
          success: function(responseContent) {
            $modalSuccessApproval.html(Mustache.render($successTemplate.html(), { messages: ["Successfully approved transaction"] }));
            $modalControls.hide();
            window.location.href = "/adjustments/deposits/payment_collections/" + paymentCollectionId;
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            $modalErrorsApproval.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to approve payment collection");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $btnApprove.on('click', function() {
      $modalSuccessApproval.html("");
      $modalErrorsApproval.html("");
      $modalApprove.open();
    });

    $btnCancelApproval.on('click', function() {
      if(!$btnCancelApproval.hasClass('loading')) {
        $modalApprove.close();
      }
    });
  }

  var _cacheDom = function() {
    $confirmationModal        = $("#confirmation-modal"); 
    $addMemberModal           = $("#add-member-modal");
    $btnApprove               = $("#btn-approve");
    $btnCancelApproval        = $("#btn-cancel-approval");
    $btnAddMember             = $("#btn-add-member");
    $btnApproveConfirmation   = $("#btn-approve-confirmation");
    $btnDelete                = $(".btn-delete");
    $parameters               = $("#parameters");
    paymentCollectionId       = $parameters.data("payment-collection-id");
    $errors                   = $("#errors");
    $errorsTemplate           = $("#errors-template");
    $memberSelect             = $("#member-select");

    $modalApprove             = $(".modal-approve").remodal({ hashTracking: true, closeOnOutsideClick: false });

    $modalErrorsApproval      = $(".modal-approve").find(".errors");
    $modalSuccessApproval     = $(".modal-approve").find(".success");
    $modalControls            = $(".modal-controls");
    $successTemplate          = $("#success-template");
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
