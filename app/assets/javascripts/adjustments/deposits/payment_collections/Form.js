var Form = (function() {
  var $parameters           = $("#parameters");
  var paymentCollectionId   = $parameters.data('payment-collection-id');
  var $savingsTypes         = $parameters.data("savings-types");
  var $insuranceTypes       = $parameters.data("insurance-types");
  var $equityTypes          = $parameters.data("equity-types");
  var $savingsDeposits      = $(".savings-deposit");
  var $insuranceDeposits    = $(".insurance-deposit");
  var $equityDeposits       = $(".equity-deposit");
  var $grandTotal           = $(".grand-total");
  var $section              = $(".transaction-table");
  var $totalMemberAmounts   = $(".total-member-amount");
  var $btnDelete            = $(".btn-delete");
  var $modalLoading         = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $memberSelect         = $("#member-select");
  var $btnAddMember         = $("#btn-add-member");
  
  // Hide controls
  $(".modal-loading").find('.controls').hide();

  var urlDeletePaymentCollectionRecord  = "/api/v1/payment_collections/delete_payment_collection_record";
  var urlAddMember                      = "/api/v1/payment_collections/add_member";

  var _bindEvents = function() {
    $btnAddMember.on('click', function() {
      $modalLoading.open();
      if(!$btnAddMember.hasClass('loading')) {
        $(this).addClass('loading');
        $(this).addClass('disabled');
        var memberId = $memberSelect.val();

        $.ajax({
          url: urlAddMember,
          method: 'POST',
          dataType: 'json',
          data: { 
            id: paymentCollectionId, 
            member_id: memberId 
          },
          success: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            toastr.success("Successfully added member");
            window.location.href = "/adjustments/deposits/payment_collections/" + paymentCollectionId + "/edit";
          },
          error: function(responseContent) {
            $(this).removeClass('loading');
            $(this).removeClass('disabled');
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            toastr.error("Something went wrong when trying to add member");
            $modalLoading.close();
          }
        });
      } else {
        toastr.info("Still loading member");
      }
    });

    $btnDelete.on('click', function() {
      var $btn = $(this);
      $btn.addClass('loading');
      $btn.addClass('disabled');
      var paymentCollectionRecordId = $btn.data('payment-collection-record-id');

      $modalLoading.open();

      $.ajax({
        url: urlDeletePaymentCollectionRecord,
        method: 'POST',
        dataType: 'json',
        data: { payment_collection_record_id: paymentCollectionRecordId },
        success: function(responseContent) {
          toastr.success("Successfully deleted record");
          window.location.href = "/adjustments/deposits/payment_collections/" + paymentCollectionId + "/edit";
        },
        error: function(responseContent) {
          toastr.error("Cannot delete this record");
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
          $modalLoading.close();
        }
      });
    });

    $.each($savingsTypes, function() {
      var savingsType = this;
      var $totalSavingsDeposits = $section.find(".total-savings[data-savings-type-code='" + savingsType + "']");
      _computeMemberTotalAmount();
      _computeTotalSavingsDeposits($section.find(".savings-deposit[data-account-code='" + savingsType + "']"), $totalSavingsDeposits);
    });

    $savingsDeposits.on('change', function() {
      var savingsType = $(this).data("account-code");
      var $totalSavingsDeposits = $section.find(".total-savings[data-savings-type-code='" + savingsType + "']");
      _computeTotalSavingsDeposits($section.find(".savings-deposit[data-account-code='" + savingsType + "']"), $totalSavingsDeposits);
      _computeMemberTotalAmount();
      _computeTotalAmt($section);
    });

    $.each($insuranceTypes, function() {
      var insuranceType = this;
      var $totalInsuranceDeposits = $section.find(".total-insurance[data-insurance-type-code='" + insuranceType + "']");
      _computeMemberTotalAmount();
      _computeTotalInsuranceDeposits($section.find(".insurance-deposit[data-account-code='" + insuranceType + "']"), $totalInsuranceDeposits);
    });

    $insuranceDeposits.on('change', function() {
      var insuranceType = $(this).data("account-code");
      var $totalInsuranceDeposits = $section.find(".total-insurance[data-insurance-type-code='" + insuranceType + "']");
      _computeTotalInsuranceDeposits($section.find(".insurance-deposit[data-account-code='" + insuranceType + "']"), $totalInsuranceDeposits);
      _computeMemberTotalAmount();
      _computeTotalAmt($section);
    });

    $.each($equityTypes, function() {
      var equityType = this;
      var $totalEquityDeposits = $section.find(".total-equity[data-equity-type-code='" + equityType + "']");
      _computeMemberTotalAmount();
      _computeTotalEquityDeposits($section.find(".equity-deposit[data-account-code='" + equityType + "']"), $totalEquityDeposits);
    });

    $equityDeposits.on('change', function() {
      var equityType = $(this).data("account-code");
      var $totalEquityDeposits = $section.find(".total-equity[data-equity-type-code='" + equityType + "']");
      _computeTotalEquityDeposits($section.find(".equity-deposit[data-account-code='" + equityType + "']"), $totalEquityDeposits);
      _computeMemberTotalAmount();
      _computeTotalAmt($section);
    });

    _computeMemberTotalAmount();
    _computeTotalAmt($section);
  }

  var _computeMemberTotalAmount = function() {
    $.each($totalMemberAmounts, function() {
      var memberId = $(this).data("member-id");
      var t = 0.00;
      $.each($section.find(".cp-amount[data-member-id='" + memberId + "']"), function() {
        t += parseFloat($(this).val());
      });

      $(this).val(t);
    });
  }

  var _computeTotalAmt = function($section) {
    var grandTotal = 0.00;

    $.each($section.find(".total-savings"), function() {
      grandTotal += parseFloat($(this).val());
    });

    $.each($section.find(".total-insurance"), function() {
      grandTotal += parseFloat($(this).val());
    });

    $.each($section.find(".total-equity"), function() {
      grandTotal += parseFloat($(this).val());
    });

    $section.find(".grand-total").val(grandTotal);
  }

  var _computeTotalSavingsDeposits = function($savingsDeposits, $totalSavingsDeposits) {
    var t = 0.00;
    $savingsDeposits.each(function() {
      t += parseFloat($(this).val());
    });

    $totalSavingsDeposits.val(t);
  }

  var _computeTotalInsuranceDeposits = function($insuranceDeposits, $totalInsuranceDeposits) {
    var t = 0.00;
    $insuranceDeposits.each(function() {
      t += parseFloat($(this).val());
    });

    $totalInsuranceDeposits.val(t);
  }

  var _computeTotalEquityDeposits = function($equityDeposits, $totalEquityDeposits) {
    var t = 0.00;
    $equityDeposits.each(function() {
      t += parseFloat($(this).val());
    });

    $totalEquityDeposits.val(t);
  }

  var init = function() {
    _bindEvents();
  }

  return {
    init: init
  };
})();
