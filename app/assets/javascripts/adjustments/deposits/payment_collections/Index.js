var Index = (function() {
  var $modalNewTransaction;
  var $btnConfirmGenerate;
  var $btnNewTransaction;
  var $modalBranchSelect;
  var $dateOfPayment;
  var $clickable;
  var url = "/api/v1/cash_management/deposits/payment_collections/generate_transaction";

  var _cacheDom = function() {
    $modalNewTransaction  = $("#modal-new-transaction");
    $btnConfirmGenerate   = $("#btn-confirm-generate");
    $btnNewTransaction    = $("#btn-new-transaction");
    $modalBranchSelect    = $("#modal-branch-select");
    $dateOfPayment        = $("#date-of-payment");
    $clickable            = $(".clickable");
  };

  var _bindEvents = function() {
    $btnNewTransaction.on("click", function() {
      $modalNewTransaction.modal("show");
    });

    $btnConfirmGenerate.on("click", function() {
      $btnConfirmGenerate.prop("disabled", true);
      
      var branchId      = $modalBranchSelect.val();
      var dateOfPayment = $dateOfPayment.val();

      $.ajax({
        url: url,
        method: 'POST',
        data: {
          branch_id: branchId,
          date_of_payment: dateOfPayment,
          for_resignation: true
        },
        dataType: 'json',
        success: function(response) {
          toastr.success("Successfully generated transaction. Redirecting...");
          window.location.href = "/adjustments/deposits/payment_collections/" + response.payment_collection_id;
        },
        error: function(response) {
          toastr.error("Error in generating new transaction");
          $btnConfirmGenerate.prop("disabled", false);
        }
      });
    });

    $clickable.on("click", function() {
      var transactionId = $(this).data("transaction-id"); 
      window.location.href = "/adjustments/deposits/payment_collections/" + transactionId;
    });
  };

  var init  = function() {
    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
