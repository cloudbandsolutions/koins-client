var Index = (function() {
  var $modalBranchSelect;
  var $dateOfPayment;
  var $btnNewTransaction;
  var $btnConfirmGenerate;
  var $modalNewTransaction;
  var $clickable;

  var urlUtilsBranches                  = "/api/v1/utils/branches";
  var urlUtilsCenters                   = "/api/v1/utils/centers";
  //var urlGenerateWithdrawalTransaction  = "/api/v1/adjustments/withdrawals/payment_collections/generate_transaction";
  var urlGenerateWithdrawalTransaction  = "/api/v1/cash_management/withdrawals/payment_collections/generate_transaction";

  var _cacheDom = function() {
    $modalNewTransaction  = $("#modal-new-transaction");
    $dateOfPayment        = $("#date-of-payment");
    $btnNewTransaction    = $("#btn-new-transaction");
    $modalBranchSelect    = $("#modal-branch-select");
    $btnConfirmGenerate   = $("#btn-confirm-generate");
    $clickable            = $(".clickable");
  }

  var _bindEvents = function() {
    $clickable.on('click', function() {
      var pc_id       = $(this).data('transaction-id');
      window.location = "/adjustments/withdrawals/payment_collections/" + pc_id;
    });

    $btnNewTransaction.on('click', function() {
      $modalNewTransaction.modal('show');
    });

    $btnConfirmGenerate.on('click', function() {
      var branchId      = $modalBranchSelect.val();
      var dateOfPayment = $dateOfPayment.val();

      var data = {
        branch_id: branchId,
        date_of_payment: dateOfPayment,
        for_resignation: true
      };

      $btnConfirmGenerate.prop("disabled", true);

      $.ajax({
        url: urlGenerateWithdrawalTransaction,
        data: data,
        dataType: 'json',
        method: 'POST',
        success: function(responseContent) {
          var pc_id = responseContent.payment_collection_id;
          console.log(responseContent);
          window.location = "/adjustments/withdrawals/payment_collections/" + pc_id;
        },
        error: function(responseContent) {
          var errorMessages = JSON.parse(responseContent.responseText).errors;
          console.log(errorMessages);
          $btnConfirmGenerate.prop("disabled", true);
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
