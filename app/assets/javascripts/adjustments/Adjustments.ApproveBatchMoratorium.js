//= require_directory ./lib

Adjustments.ApproveBatchMoratorium  = (function() {
  var $approveBtn         = $("#approve-btn");
  var $btnConfirmApproval = $("#btn-confirm-approval");
  var $btnCancelApproval  = $("#btn-cancel-approval");
  var $modalApprove       = $(".modal-approve").remodal({hashTracking: true, closeOnOutsideClick: false});
  var $controls           = $(".modal-approve").find(".controls");
  var $message            = $(".modal-approve").find(".message");
  var url                 = "/api/v1/adjustments/batch_moratoria/approve";
  var id                  = $("#parameters").data('id');

  var _setMessage = function(message) {
    $message.html(message);
  }

  var init  = function() {
    $approveBtn.on('click', function() {
      $modalApprove.open();
    });

    $btnConfirmApproval.on('click', function() {
      $controls.hide();
      _setMessage("Loading...");

      $.ajax({
        url: url,
        data: {
          id: id
        },
        method: 'POST',
        dataType: 'json',
        success: function(response) {
          toastr.success('Successfully approved batch moratorium');
          _setMessage("Success! Redirecting...");
          window.location.href = "/adjustments/batch_moratoria/" + id;
        },
        error: function(response) {
          toastr.error('Error in approving record');
          console.log(response);
          _setMessage("");
          $controls.show();
        }
      });
    });

    $btnCancelApproval.on('click', function() {
      $modalApprove.close();
    });
  };

  return {
    init: init
  };
})();
