//= require_directory ./lib
Adjustments.ApproveTransferAdjustment  = (function() {
  var $approveBtn         = $("#approve-btn");
  var $btnConfirmApproval = $("#btn-confirm-approval");
  var $btnCancelApproval  = $("#btn-cancel-approval");
  var $modalApprove       = $(".modal-approve").remodal({hashTracking: true, closeOnOutsideClick: false});
  var $controls           = $(".modal-approve").find(".controls");
  var $message            = $(".modal-approve").find(".message");
  var $errors             = $(".modal-approve").find(".errors");
  var url                 = "/api/v1/adjustments/transfer_adjustments/approve";
  var id                  = $("#parameters").data('id');

  var _setMessage = function(message) {
    $message.html(message);
  }

  var _setErrors  = function(errors) {
    if(errors.length == 0) {
      $errors.html("");
    } else {
      $errors.html("");
      $errors.append("<ul>");
      for(var i = 0; i < errors.length; i++) {
        $errors.append("<li>" + errors[i] + "</li>");
      }
      $errors.append("</ul>");
    }
  }

  var init  = function() {
    $approveBtn.on('click', function() {
      $modalApprove.open();
    });

    $btnConfirmApproval.on('click', function() {
      $controls.hide();
      _setMessage("Loading...");
      _setErrors([]);

      $.ajax({
        url: url,
        data: {
          id: id
        },
        method: 'POST',
        dataType: 'json',
        success: function(response) {
          toastr.success('Successfully approved batch moratorium');
          _setMessage("Success! Redirecting...");
          window.location.href = "/adjustments/transfer_adjustments/" + id;
        },
        error: function(response) {
          toastr.error('Error in approving record');
          console.log(response);
          _setErrors(JSON.parse(response.responseText).errors);
          _setMessage("");
          $controls.show();
        }
      });
    });

    $btnCancelApproval.on('click', function() {
      $modalApprove.close();
    });
  };

  return {
    init: init
  };
})();
