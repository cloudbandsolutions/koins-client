var Show  = (function() {
  var $btnAddRecord;
  var $selectMember;
  var $selectAccountCode;
  var $selectAdjustmentType;
  var $selectACcountingCode;
  var $inputAmount;
  var $btnDelete;
  var $parameters;
  var subsidiaryAdjustmentId;
  var urlCreateSubsidiaryAdjustmentRecord = "/api/v1/adjustments/subsidiary_adjustment_records/create";
  var urlDeleteSubsidiaryAdjustmentRecord = "/api/v1/adjustments/subsidiary_adjustment_records/delete";
  var urlMembers  = "/api/v1/get_members";

  var _bindEvents = function() {
    $btnDelete.on("click", function() {
      var $btn                          = $(this);
      var subsidiaryAdjustmentRecordId  = $btn.data("id");

      $btn.prop("disabled", true);

      $.ajax({
        url: urlDeleteSubsidiaryAdjustmentRecord,
        method: 'POST',
        data: {
          id: subsidiaryAdjustmentRecordId
        },
        dataType: 'json',
        success: function(response) {
          window.location.href = "/adjustments/subsidiary_adjustments/" + response.id;
        },
        error: function(response) {
          $btn.prop("disabled", false);
        }
      });
    });

    $btnAddRecord.on("click", function() {
      $btnAddRecord.prop("disabled", true);

      $.ajax({
        url: urlCreateSubsidiaryAdjustmentRecord,
        data: _buildData(),
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          window.location.href = "/adjustments/subsidiary_adjustments/" + response.id;
        },
        error: function(response) {
          var errors = JSON.parse(response.responseText).errors;
          for(var i = 0; i < errors.length; i++) {
            toastr.error(errors[i]);
          }

          $btnAddRecord.prop("disabled", false);
        }
      });
    });
  };

  var _buildData  = function() {
    return {
      subsidiary_adjustment_id: subsidiaryAdjustmentId,
      member_id: $selectMember.val(),
      account_code: $selectAccountCode.val(),
      adjustment_type: $selectAdjustmentType.val(),
      amount: $inputAmount.val(),
      accounting_code_id: $selectAccountingCode.val()
    };
  };

  var _formatData = function(data) {
    //return data.id;
    var markup = data.name;
    return markup;
  };

  var _formatDataSelection  = function(data) {
    console.log(data);
    return data.text;
  };

  var _cacheDom = function() {
    $parameters           = $("#parameters");
    $btnAddRecord         = $("#btn-add-record");
    $btnDelete            = $(".btn-delete");
    $selectMember         = $("#select-member");
    $selectAccountCode    = $("#select-account-code");
    $selectAdjustmentType = $("#select-adjustment-type");
    $inputAmount          = $("#input-amount");
    $selectAccountingCode = $("#select-accounting-code");

    subsidiaryAdjustmentId  = $parameters.data("id");

    // Ajax select2
    $selectMember.select2({
      theme: "bootstrap",
      width: "100%",
      ajax: {
        url: urlMembers,
        dataType: 'json',
        delay: 250,
        data: function(params) {
          return {
            q: params.term
          };
        },
        processResults: function(data, params) {
          var members = data.data.members;
          params.page = params.page || 1;
          return {
            results: members,
            pagination: {
              more: (params.page * 30) < members.length
            }
          };
        },
        cache: true
      },
      escapeMarkup: function(markup) { return markup; },
      minimumInputLength: 1,
      templateResult: _formatData,
      templateSelection: _formatDataSelection
    });
  };

  var init  = function() {
    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
