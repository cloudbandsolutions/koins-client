var Form = (function() {
  var $subsidiaryAdjustmentRecords;
  var urlMembers  = "/api/v1/get_members";

  var init = function() {
    $subsidiaryAdjustmentRecords = $("#subsidiary_adjustment_records");
    $(document).find(".member-select").select2({
      theme: "bootstrap",
      width: "100%",
      ajax: {
        url: urlMembers,
        dataType: 'json',
        delay: 250,
        data: function(params) {
          return {
            q: params.term
          };
        },
        processResults: function(data, params) {
          console.log(data.data.members);
          var members = data.data.members;
          params.page = params.page || 1;
          return {
            results: members,
            pagination: {
              more: (params.page * 30) < members.length
            }
          };
        },
        cache: true
      },
      minimumInputLength: 1
    });

    $subsidiaryAdjustmentRecords.on("cocoon:after-insert", function(e, addedRecord) {
      $(addedRecord).find(".member-select").select2({
        theme: "bootstrap",
        width: "100%",
        ajax: {
          url: urlMembers,
          dataType: 'json',
          delay: 250,
          data: function(params) {
            return {
              q: params.term
            };
          },
          processResults: function(data, params) {
            console.log(data.data.members);
            var members = data.data.members;
            params.page = params.page || 1;
            return {
              results: members,
              pagination: {
                more: (params.page * 30) < members.length
              }
            };
          },
          cache: true
        },
        minimumInputLength: 1
      });
    });
  };
  
  return {
    init: init
  };
})();
