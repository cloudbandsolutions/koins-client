var Index = (function() {
  var $btnNew;
  var $btnConfirm;
  var $modalNewSubsidiaryAdjustment;
  var $selectBranch;
  var $inputParticular;
  var $errorsTemplate;
  var $message;

  var urlCreateSubsidiaryAdjustment = "/api/v1/adjustments/subsidiary_adjustments/create";

  var _cacheDom = function() {
    $btnNew                       = $("#btn-new");
    $btnConfirm                   = $("#btn-confirm");
    $modalNewSubsidiaryAdjustment = $("#modal-new-subsidiary-adjustment");
    $selectBranch                 = $("#modal-select-branch");
    $inputParticular              = $("#input-particular");
    $message                      = $(".message");
    $errorsTemplate               = $("#errors-template");
  };

  var _bindEvents = function() {
    $btnNew.on("click", function() {
      $modalNewSubsidiaryAdjustment.modal("show");
    });

    $btnConfirm.on("click", function() {
      $btnConfirm.prop("disabled", true);
      $message.html("Validating...");

      $.ajax({
        url: urlCreateSubsidiaryAdjustment,
        data: {
          branch_id: $selectBranch.val(),
          particular: $inputParticular.val()
        },
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.href = "/adjustments/subsidiary_adjustments/" + response.id;
        },
        error: function(response) {
          $message.html(
            Mustache.render(
              $errorsTemplate.html(),
              JSON.parse(response.responseText)
            )
          );

          $btnConfirm.prop("disabled", false);
        }
      });
    });
  };

  var init = function() {
    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
