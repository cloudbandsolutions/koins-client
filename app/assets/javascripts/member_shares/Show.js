var Show  = (function() {
  var $parameters;
  var $btnFlagForPrinting;
  var id;
  var memberId;
  var urlFlagForPrinting  = "/api/v1/member_shares/flag_for_printing";

  var _cacheDom = function() {
    $parameters         = $("#parameters");
    $btnFlagForPrinting = $("#btn-flag-for-printing");
    id                  = $parameters.data('id');
    memberId            = $parameters.data('member-id');
  };

  var _bindEvents = function() {
    $btnFlagForPrinting.on("click", function() {
      $btnFlagForPrinting.prop("disabled", true);

      $.ajax({
        url: urlFlagForPrinting,
        data: {
          id: id
        },
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          toastr.success("Successfully flagged record"); 
          window.location.href = "/members/" + memberId + "/member_shares/" + id;
        },
        error: function(response) {
          $btnFlagForPrinting.props("disabled", false);
          toastr.error("Something went wrong");
        }
      });
    });
  };

  var init  = function() {
    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
