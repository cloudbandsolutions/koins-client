//= require_directory ./lib

Reports.ValidationsReport = (function() {
  var $downloadBtn       = $("#download-btn");
  var $status            = $("#status");
  var $startDate         = $("#start-date");
  var $endDate           = $("#end-date");
  var $brachSelect       = $("#branch-select");

  var _bindEvents = function() {
    $downloadBtn.on('click', function() {
      data = {
        status: $status.val(),
        start_date: $startDate.val(),
        end_date: $endDate.val(),
        branch: $brachSelect.val(),
      };

      window.location = "/reports/validations_report?" + encodeQueryData(data);
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
