//= require_directory ./lib

Reports.CicReport = (function() {
  var $providerCode      = $("#provider-code");
  var $downloadBtn       = $("#download-btn");
  var $startDate         = $("#start-date");
  var $endDate           = $("#end-date");

  var _bindEvents = function() {
    $downloadBtn.on('click', function() {

      window.location = "/reports/project_type_category_excel";
      //window.location = "/reports/cic_report?" + encodeQueryData(data);
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
