//= require_directory ./lib

Reports.MasterListOfClientLoans = (function() {
  var $branchSelect         = $("#branch-select");
  var $centerSelect         = $("#center-select");
  var $loanProductSelect    = $("#loan-product-select");
  var $asOf                 = $("#as-of");
  var $btnGenerate          = $("#btn-generate");
  var $printBtn             = $("#print-btn");
  var urlCentersByBranch    = "/api/v1/branches/centers_by_branch";
  var urlGenerateReport     = "/api/v1/reports/master_list_of_client_loans";
  
  var $reportsMasterListOfClientLoansSection = $("#reports-master-list-of-client-loans-section");
  var $reportsMasterListOfClientLoansTemplate = $("#reports-master-list-of-client-loans-template");
  
  var _initUiEvents = function() {
    $branchSelect.on('change', function() {
      populateSelect($centerSelect, [], "id", "name");

      if($branchSelect.val()) {
        $.ajax({
          url: urlCentersByBranch,
          data: { branch_id: $branchSelect.val() },
          dataType: 'json',
          success: function(data) {
            populateSelect($centerSelect, data.centers, "id", "name");
          },
          error: function(data) {
            toastr.error("Error in loading Centers.");
          }
        });
      } else {
        populateSelect($centerSelect, [], "id", "name");
      }
    });
  };

  var _buildFilterParams = function() {
    var params = {
      branch_id: $branchSelect.val(),
      center_id: $centerSelect.val(),
      loan_product_id: $loanProductSelect.val(),
      as_of:  $asOf.val()
    };

    console.log(params);

    return params;
  };

  var init = function() {
    _initUiEvents();
    //ariel
    $printBtn.on('click', function() {
      
      var params = {
        branch_id: $branchSelect.val(),
        center_id: $centerSelect.val(),
        loan_product_id: $loanProductSelect.val(),
        as_of:  $asOf.val()
      };



      window.open("/reports/master_list_of_client_loans_pdf?" + encodeQueryData(params), "_blank");
    });
  //ariel

 
    $btnGenerate.on('click', function() {
      $btnGenerate.addClass('loading');
      
      $.ajax({
        url: urlGenerateReport,
        data: _buildFilterParams(),
        dataType: 'json',
        success: function(response) {
          $reportsMasterListOfClientLoansSection.html(
            Mustache.render(
              $reportsMasterListOfClientLoansTemplate.html(), 
              response
            )
          );

          $btnGenerate.removeClass('loading');

          $(".curr").each(function() {
            var x = parseFloat($(this).html());
            $(this).html(numberWithCommas(x));
          });
        },
        error: function(response) {
          toastr.error("Error in generating report");
          $btnGenerate.removeClass('loading');
        }
      });
    });
  };

  return {
    init: init
  };
})();
