//= require_directory ./lib

Reports.ExpSoa = (function() {
  var $branchSelect         = $("#branch-select");
  var $centerSelect         = $("#center-select");
  var $startDate            = $("#start-date");
  var $endDate              = $("#end-date");
  var $btnGenerate          = $("#btn-generate");
  var $loanProductSelect    = $("#loan-product-select");

  var $reportsExpensesSoaSection   = $("#reports-expenses-soa-section");
  var $reportsExpensesSoaTemplate  = $("#reports-expenses-soa-template");

  var urlCentersByBranch    = "/api/v1/branches/centers_by_branch";
  var urlGenerateLSoa       = "/api/v1/reports/expenses_statement_of_assets";
  
  var _initUiEvents = function() {
    $branchSelect.on('change', function() {
      populateSelect($centerSelect, [], "id", "name");

      if($branchSelect.val()) {
        $.ajax({
          url: urlCentersByBranch,
          data: { branch_id: $branchSelect.val() },
          dataType: 'json',
          success: function(data) {
            populateSelect($centerSelect, data.centers, "id", "name");
          },
          error: function(data) {
            toastr.error("Error in loading Centers.");
          }
        });
      } else {
        populateSelect($centerSelect, [], "id", "name");
      }
    });
  };

  var _buildFilterParams = function() {
    var params = {
      branch_id: $branchSelect.val(),
      center_id: $centerSelect.val(),
      start_date: $startDate.val(),
      end_date: $endDate.val(),
      loan_product_id: $loanProductSelect.val()
    };

    return params;
  };

  var init = function() {
    _initUiEvents();

    $btnGenerate.on('click', function() {
      $btnGenerate.addClass('loading');
      
      $.ajax({
        url: urlGenerateLSoa,
        data: _buildFilterParams(),
        dataType: 'json',
        success: function(response) {
          $reportsExpensesSoaSection.html(Mustache.render($reportsExpensesSoaTemplate.html(), response));
          $btnGenerate.removeClass('loading');
          $(".curr").each(function() {
            var newVal = numberWithCommas($(this).html());
            $(this).html(newVal);
          });
          // Make sticky
          $(".sticky").stickyTableHeaders();
        },
        error: function(response) {
          toastr.error("Error in generating SoA");
          $btnGenerate.removeClass('loading');
        }
      });
    });
  };

  return {
    init: init
  };
})();
