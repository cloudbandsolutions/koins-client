//= require_directory ./lib

Reports.SeriatimReport = (function() {
  var $downloadBtn       = $("#download-btn");
  var $asOf              = $("#as-of");
  var $brachSelect       = $("#branch-select");

  var _bindEvents = function() {
    $downloadBtn.on('click', function() {
      data = {
        as_of: $asOf.val(),
        branch: $brachSelect.val(),
      };

      window.location = "/reports/seriatim_report?" + encodeQueryData(data);
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
