//= require_directory ./lib

Reports.Repayments = (function() {
  var $searchBtn                    = $("#search-btn");
  var $branchSelect                 = $("#branch-select");
  var $soSelect                     = $("#so-select");
  var $centerSelect                 = $("#center-select");
  var $repaymentsSection            = $("#reports-repayments-section");
  var $repaymentsTemplate           = $("#reports-repayments-template").html();
  var $repaymentsDetailedTemplate   = $("#reports-repayments-detailed-template").html();
  var $loanProductSelect            = $("#loan-product-select");
  var $detailedCheckBox             = $("#detailed");
  var $asOf                         = $("#as-of");
  var $printBtn                     = $("#print-btn");

  var urlRepayments                 = "/api/v1/reports/repayments";
  var urlBranchSoList               = "/api/v1/branches/so_list";
  var urlBranchCentersBySo          = "/api/v1/branches/centers_by_so";

  var _bindEvents = function() {
    $branchSelect.on('change', function() {
      populateSelect($centerSelect, [], "id", "name");

      var branchId = $branchSelect.val();

      if(branchId) {
        $.ajax({
          url: urlBranchSoList,
          data: { branch_id: $branchSelect.val() },
          dataType: 'json',
          success: function(data) {
            toastr.info("Loading SOs...");
            populateSelect($soSelect, data.so_list, "name", "name");
          },
          error: function(data) {
            toastr.error("Error in loading SOs.");
          }
        });
      } else {
        populateSelect($soSelect, [], "name", "name");
      }
    });

    $soSelect.on('change', function() {
      $.ajax({
        url: urlBranchCentersBySo,
        data: { so: $soSelect.val() },
        dataType: 'json',
        success: function(data) {
          toastr.info("Loading Centers...");
          populateSelect($centerSelect, data.centers, "id", "name");
        },
        error: function(data) {
          toastr.error("Error in loading centers.");
        }
      });
    });

  //ariel
    $printBtn.on('click', function() {
      $searchBtn.addClass('loading');

      var params = {
        so:               $soSelect.val(),
        branch_id:        $branchSelect.val(),
        center_id:        $centerSelect.val(),
        loan_product_id:  $loanProductSelect.val(),
        detailed:         $detailedCheckBox.bootstrapSwitch('state'),
        as_of:            $asOf.val(),
      };

      window.open("/reports/repayments_pdf?" + encodeQueryData(params), "_blank");
    });
  //ariel


    $searchBtn.on('click', function() {
      $searchBtn.addClass('loading');

      var params = {
        so:               $soSelect.val(),
        branch_id:        $branchSelect.val(),
        center_id:        $centerSelect.val(),
        loan_product_id:  $loanProductSelect.val(),
        detailed:         $detailedCheckBox.bootstrapSwitch('state'),
        as_of:            $asOf.val(),
      };

      $.ajax({
        url: urlRepayments,
        method: 'GET',
        dataType: 'json',
        data: params,
        success: function(data) {
          console.log(data);
          if($detailedCheckBox.bootstrapSwitch('state')) {
            $repaymentsSection.html(Mustache.render($repaymentsDetailedTemplate, data));
          } else {
            $repaymentsSection.html(Mustache.render($repaymentsTemplate, data));
          }

          $repaymentsSection.find(".curr").each(function() {
            $(this).html(numberWithCommas($(this).html()));
          });

          $searchBtn.removeClass('loading');

          // Make sticky
          $(".sticky").stickyTableHeaders();
        },
        error: function(data) {
          toastr.error("Error in generating repayments report");
          $searchBtn.removeClass('loading');
        }
      });
    });
  }

  var _loadDefaults = function() {
  }

  var init = function() {
    _loadDefaults();
    _bindEvents();
  }

  return {
    init: init
  };
})();
