var Show = (function(){
  var fetchElements = function(){
    $btnMigsTransaction = $("#btn-migs-transaction");
    $migsDate = $("#migs-date");
  };

  var initializeEvents = function(){
    $btnMigsTransaction.on("click", function(){
      //alert($migsDate.val());
      var params = {
        migs_date: $migsDate.val()
      };

      window.location.href = "/reports/migs?" + encodeQueryData(params);


    });
  };
  
  var init = function(){
    fetchElements();
    initializeEvents();
  };

  return{
    init: init
  };
})();
