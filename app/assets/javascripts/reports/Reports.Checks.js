//= require_directory ./lib

Reports.Checks = (function() {
  var $btnPrint               = $("#btn-print");
  var $filterCheckQ           = $("#filter-check-q");
  var $filterCheckStartNumber = $("#filter-check-start-number");
  var $filterCheckEndNumber   = $("#filter-check-end-number");
  var $filterCheckStartDate   = $("#filter-check-start-date");
  var $filterCheckEndDate     = $("#filter-check-end-date");

  var _bindEvents = function() {
    $btnPrint.on('click', function() {
      data = {
        q: $filterCheckQ.val(),
        start_number: $filterCheckStartNumber.val(),
        end_number: $filterCheckEndDate.val(),
        start_date: $filterCheckStartDate.val(),
        end_date: $filterCheckEndDate.val(),
      };

      window.location = "/reports/print_checks?" + encodeQueryData(data);
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
