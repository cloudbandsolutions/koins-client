//= require_directory ./lib

Reports.DailyReportInsuranceAccountStatuses = (function() {
  var $btnGenerateRecord    = $("#btn-generate-record");
  var $generateRecordModal  = $("#generate-record-modal");
  var $start                = $("#start");
  var $buttons              = $generateRecordModal.find(".btn");
  var $modalBranchSelect    = $generateRecordModal.find(".branch-select");

  var urlGenerateRecord   = "/api/v1/insurance/generate_daily_report_insurance_account_status";
  
  var _disableButtons = function() {
    $buttons.prop("disabled", true);
  };

  var _enableButtons = function() {
    $buttons.prop("disabled", false);
  };

  var _setModalMessage = function(message) {
    $generateRecordModal.find(".message").html(message);
  }

  var init  = function() {
    $btnGenerateRecord.on("click", function() {
      $generateRecordModal.modal("show");
    });

    $start.on("click", function() {
      _disableButtons();
      _setModalMessage("Loading...");

      $.ajax({
        url: urlGenerateRecord,
        data: {
          branch_id: $modalBranchSelect.val()
        },
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          toastr.success("Successfully generated record");
          toastr.info("Redirecting...");
          _setModalMessage("Redirecting...");
          window.location.href = "/monitoring/daily_report_insurance_account_statuses/" + response.id;
        },
        error: function(response) {
          toastr.error("Error in generating record");
          _setModalMessage("Error in generating record. Please contact administrator");
          _enableButtons();
        }
      });
    });
  };

  return {
    init: init
  };
})();
