//= require_directory ./lib

Reports.BalanceSheet = (function() {
  var $branchSelect         = $("#branch-select");
  var $startDate            = $("#start-date");
  var $endDate              = $("#end-date");
  var $btnGenerate          = $("#btn-generate");

  var $reportsBalanceSheetSection   = $("#reports-balance-sheet-section");
  var $reportsBalanceSheetTemplate  = $("#reports-balance-sheet-template");
  var urlGenerateBalanceSheet       = "/api/v1/reports/balance_sheet";
  
  var _initUiEvents = function() {
  };

  var _buildFilterParams = function() {
    var params = {
      branch_id: $branchSelect.val(),
      end_date: $endDate.val(),
      start_date: $startDate.val(),
    };

    return params;
  };

  var init = function() {
    _initUiEvents();

    $btnGenerate.on('click', function() {
      $btnGenerate.addClass('loading');
      
      $.ajax({
        url: urlGenerateBalanceSheet,
        data: _buildFilterParams(),
        dataType: 'json',
        success: function(response) {
          $reportsBalanceSheetSection.html(Mustache.render($reportsBalanceSheetTemplate.html(), response));
          $btnGenerate.removeClass('loading');
        },
        error: function(response) {
          toastr.error("Error in generating Balance Sheet");
          $btnGenerate.removeClass('loading');
        }
      });
    });
  };

  return {
    init: init
  };
})();
