//= require_directory ./lib

Reports.IncomeStatement = (function() {
  var $branchSelect         = $("#branch-select");
  var $startDate            = $("#start-date");
  var $endDate              = $("#end-date");
  var $btnGenerate          = $("#btn-generate");

  var $reportsIncomeStatementSection   = $("#reports-income-statement-section");
  var $reportsIncomeStatementTemplate  = $("#reports-income-statement-template");
  var urlGenerateIncomeStatement       = "/api/v1/reports/income_statement";
  
  var _initUiEvents = function() {
  };

  var _buildFilterParams = function() {
    var params = {
      branch_id: $branchSelect.val(),
      start_date: $startDate.val(),
      end_date: $endDate.val(),
    };

    return params;
  };

  var init = function() {
    _initUiEvents();

    $btnGenerate.on('click', function() {
      $btnGenerate.addClass('loading');
      
      $.ajax({
        url: urlGenerateIncomeStatement,
        data: _buildFilterParams(),
        dataType: 'json',
        success: function(response) {
          $reportsIncomeStatementSection.html(Mustache.render($reportsIncomeStatementTemplate.html(), response));
          $btnGenerate.removeClass('loading');
        },
        error: function(response) {
          toastr.error("Error in generating Balnace Sheet");
          $btnGenerate.removeClass('loading');
        }
      });
    });
  };

  return {
    init: init
  };
})();
