//= require_directory ./lib

Reports.MonthlyRR = (function() {
  var $sectionMonthlyRr     = $("#section-monthly-rr");
  var $templateMonthlyRr    = $("#template-monthly-rr");
  var $btnGenerateMReport   = $("#btn-generate-monthly-daily-report");
  var $loadingModal           = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $loadingModalControls   = $(".modal-loading").find(".controls");

  var urlFetchMonthlyRr       = "/api/v1/reports/monthly_rr";
  var urlGenerateMonthlyDaily = "/api/v1/generate_monthly_daily_report";

  var _renderMonthlyRr = function() {
    $.ajax({
      url: urlFetchMonthlyRr,
      dataType: 'json',
      method: 'GET',
      success: function(response) {
        $sectionMonthlyRr.html(
          Mustache.render(
            $templateMonthlyRr.html(),
            response.data
          )
        );
      },
      error: function(response) {
      }
    });
  }

  var init = function() {
    $loadingModalControls.hide();
    _renderMonthlyRr();

    $btnGenerateMReport.on('click', function() {
      $loadingModal.open();
      $.ajax({
        url: urlGenerateMonthlyDaily,
        method: 'POST',
        success: function(response) {
          $loadingModal.close();
          toastr.success('Successfully generated monthly report');
          _renderMonthlyRr();
        },
        error: function(response) {
          $loadingModal.close();
          toastr.error('Error in generating monthly report');
        }
      });
    });
  }

  return {
    init: init
  };
})();
