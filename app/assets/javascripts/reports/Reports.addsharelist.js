//= require_directory ./lib

Reports.addsharelist = (function() {
  var $branchSelect         = $("#branch-select");
  var $centerSelect         = $("#center-select");
  var $startDate            = $("#start-date");
  var $endDate              = $("#end-date");
  var $btndownloadexcel     = $("#btn-generate-excel");
  var urlCentersByBranch    = "/api/v1/branches/centers_by_branch";
 
  var _initUiEvents = function() {
    $branchSelect.on('change', function() {
      populateSelect($centerSelect, [], "id", "name");

      if($branchSelect.val()) {
        $.ajax({
          url: urlCentersByBranch,
          data: { branch_id: $branchSelect.val() },
          dataType: 'json',
          success: function(data) {
            populateSelect($centerSelect, data.centers, "id", "name");
          },
          error: function(data) {
            toastr.error("Error in loading Centers.");
          }
          });
      }
      else {
        populateSelect($centerSelect, [], "id", "name");
        }
      }); 
     };

     var _initExcel = function(){
      $btndownloadexcel.on('click', function(){
      //alert("hello IDIOT");
      data = {
        start_date: $startDate.val(),
        end_date  : $endDate.val(),
        branch_id : $branchSelect.val(),
        center_id : $centerSelect.val(),
      }
        window.location.href = "/reports/additional_shares_excel?" + encodeQueryData(data);
      });
     
     };
  
  var init = function() { 
   _initExcel();
   _initUiEvents();
  };

  return {
    init: init
  };
})();
