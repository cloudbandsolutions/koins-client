                               //= require_directory ./lib

Reports.XWeeksToPay = (function() {
  var $btnGenerate    = $("#btn-generate"); 
  var $printBtn       = $("#print-btn");
  var $numWeeks       = $("#num-weeks");
  var $loanProductId  = $("#loan-product-id");
  var $reportTemplate = $("#report-template");
  var $reportSection  = $("#report-section");
  var url             = "/api/v1/reports/x_weeks_to_pay"

  var _disableControls  = function() {
    $btnGenerate.prop("disabled", true);
    $numWeeks.prop("disabled", true);
    $loanProductId.prop("disabled", true);
  };

  var _enableControls = function() {
    $btnGenerate.prop("disabled", false);
    $numWeeks.prop("disabled", false);
    $loanProductId.prop("disabled", false);
  };

  var init  = function() {
    //ariel
    $printBtn.on('click', function() {

      var params = {
        num_weeks:        $numWeeks.val(),
        loan_product_id:  $loanProductId.val()
      };


      window.open("/reports/x_weeks_to_pay_pdf?" + encodeQueryData(params), "_blank");
    });
  //ariel

    $btnGenerate.on("click", function() {
      var params = {
        num_weeks:        $numWeeks.val(),
        loan_product_id:  $loanProductId.val()
      };

      _disableControls();

      $.ajax({
        url: url,
        data: params,
        method: 'GET',
        dataType: 'json',
        success: function(response) {
          console.log(response);
          $reportSection.html(
            Mustache.render(
              $reportTemplate.html(),
              response.data
            )
          );

          $(".curr").each(function() {
            var x  = $(this).html();
            $(this).html(
              numberWithCommas(
                parseFloat(x)
              )
            );
          });
          _enableControls();
        },
        error: function(response) {
          toastr.error("Error in fetching results");
          _enableControls();
        }
      });
    });
  };

  return {
    init: init
  };
})();
