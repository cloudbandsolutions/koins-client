//= require_directory ./lib

Reports.SoaFunds = (function() {
  var $reportContent    = $("#report-content");
  var $parameters       = $("#parameters");
  var urlSoaFunds       = "/api/v1/reports/soa_funds";
  var accountCodes      = $parameters.data("account-codes");
  var $soaFundsTemplate = $("#soa-funds-template");
  var $btnGenerate      = $("#btn-generate");
  var $startDate        = $("#start-date");
  var $endDate          = $("#end-date");
  var $centerSelect     = $("#center-select");
  var $accountType      = $("#account-type-select");

  var _renderReport = function(members, account_codes) {
    $reportContent.html(
      Mustache.render(
        $soaFundsTemplate.html(),
        {
          members: members,
          accountCodes: account_codes
        }
      )
    );
  };

  var _fetchData = function() {
    $.ajax({
      url: urlSoaFunds,
      method: 'GET',
      dataType: 'json',
      data: {
        start_date: $startDate.val(),
        end_date: $endDate.val(),
        center_id: $centerSelect.val(),
        account_types: $accountType.val()
      },
      success: function(response) {
        if(response.data.length > 0) {
          _renderReport(response.data, response.account_codes);
          $(".section").each(function() {
            var $thisSection  = $(this);
            for(var i = 0; i < accountCodes.length; i++) {
              var totalDr = 0.00;
              var totalCr = 0.00;
              $thisSection.find(".dr[data-code='" + accountCodes[i] + "']").each(function() {
                var x = parseFloat($(this).html());
                totalDr += x;
              });

              $thisSection.find(".cr[data-code='" + accountCodes[i] + "']").each(function() {
                var x = parseFloat($(this).html());
                totalCr += x;
              });

              $thisSection.find(".total_dr[data-code='" + accountCodes[i] +"']").html(totalDr);
              $thisSection.find(".total_cr[data-code='" + accountCodes[i] +"']").html(totalCr);

              var grandTotalDr = parseFloat($(".grand-total-dr[data-code='" + accountCodes[i] + "']").html());
              grandTotalDr += totalDr;
              $(".grand-total-dr[data-code='" + accountCodes[i] + "']").html(grandTotalDr);

              var grandTotalCr = parseFloat($(".grand-total-cr[data-code='" + accountCodes[i] + "']").html());
              grandTotalCr += totalCr;
              $(".grand-total-cr[data-code='" + accountCodes[i] + "']").html(grandTotalCr);
            }
          });
          $(".curr").each(function() {
            var num = $(this).html();

            if(parseFloat(num) > 0.00) {
              var newNum  = numberWithCommas(num);
              $(this).html(newNum);
            } else if(parseFloat(num) == 0) {
              $(this).html("");
            }
          });
          $btnGenerate.attr("disabled", false);
        } else {
          $reportContent.find(".message").html("No data found");
          $btnGenerate.attr("disabled", false);
        }
      },
      error: function(response) {
        alert("Error");
        $btnGenerate.attr("disabled", false);
      }
    });
  };

  var init = function() {
    $btnGenerate.on("click", function() {
      $btnGenerate.attr("disabled", true);
      _fetchData();
    });
  };

  return {
    init: init
  };
})();
