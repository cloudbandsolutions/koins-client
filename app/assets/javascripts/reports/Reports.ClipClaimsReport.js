//= require_directory ./lib

Reports.ClipClaimsReport = (function() {
  var $downloadBtn              = $("#download-btn");
  var $causeOfDeath             = $("#cause-of-death");
  var $typeOfLoan               = $("#type-of-loan");
  var $brachSelect              = $("#branch-select");
  var $startDate                = $("#start-date");
  var $endDate                  = $("#end-date");

  var _bindEvents = function() {
    $downloadBtn.on('click', function() {
      data = {
        type_of_loan: $typeOfLoan.val(),
        cause_of_death: $causeOfDeath.val(),
        branch: $brachSelect.val(),
        start_date: $startDate.val(),
        end_date: $endDate.val()
      };

      window.location = "/reports/clip_claims_report?" + encodeQueryData(data);
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
