//= require_directory ./lib

Reports.LSoa = (function() {
  var $branchSelect         = $("#branch-select");
  var $centerSelect         = $("#center-select");
  var $startDate            = $("#start-date");
  var $endDate              = $("#end-date");
  var $btnGenerate          = $("#btn-generate");
  var $loanProductSelect    = $("#loan-product-select");

  var $reportsLoansSoaSection   = $("#reports-loans-soa-section");
  var $reportsLoansSoaTemplate  = $("#reports-loans-soa-template");

  var urlCentersByBranch    = "/api/v1/branches/centers_by_branch";
  var urlGenerateLSoa       = "/api/v1/reports/loans_statement_of_accounts";
  
  var _initUiEvents = function() {
    $branchSelect.on('change', function() {
      populateSelect($centerSelect, [], "id", "name");

      if($branchSelect.val()) {
        $.ajax({
          url: urlCentersByBranch,
          data: { branch_id: $branchSelect.val() },
          dataType: 'json',
          success: function(data) {
            populateSelect($centerSelect, data.centers, "id", "name");
          },
          error: function(data) {
            toastr.error("Error in loading Centers.");
          }
        });
      } else {
        populateSelect($centerSelect, [], "id", "name");
      }
    });
  };

  var _buildFilterParams = function() {
    var params = {
      branch_id: $branchSelect.val(),
      center_id: $centerSelect.val(),
      start_date: $startDate.val(),
      end_date: $endDate.val(),
      loan_product_id: $loanProductSelect.val(),
    };

    return params;
  };

  var init = function() {
    _initUiEvents();

    $btnGenerate.on('click', function() {
      $btnGenerate.addClass('loading');
      
      $.ajax({
        url: urlGenerateLSoa,
        data: _buildFilterParams(),
        dataType: 'json',
        success: function(response) {
          $reportsLoansSoaSection.html(Mustache.render($reportsLoansSoaTemplate.html(), response));
          $btnGenerate.removeClass('loading');
        },
        error: function(response) {
          toastr.error("Error in generating SoA");
          $btnGenerate.removeClass('loading');
        }
      });
    });
  };

  return {
    init: init
  };
})();
