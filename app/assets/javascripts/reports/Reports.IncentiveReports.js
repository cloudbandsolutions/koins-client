var Show = (function(){
  var fetchElements = function(){
    $reportSection  = $("#report-section");
    $urlMonthlyInc = "/api/v1/accounting/reports/monthly_incentives";
    $urlMonthlyInc2 = "/api/v1/accounting/reports/monthly_incentives_pdf";

    //$urlMonthlyInc2 = "/reports/monthly_incentives_pdf";
    $btnIncentiveTransaction = $("#btn-incentive-transaction");
    $btnPrintIncentiveTransaction = $("#btn-print-incentive-transaction");

    $reportTemplate = $("#report-template");
    $reportSection  = $("#report-section");
    $startDate = $("#start-date");
    $endDate = $("#end-date");
};
 
  var initializeEvents = function(){
    $btnIncentiveTransaction.on("click", function(){
      //alert($startDate.val() + $endDate.val());
     
      var params = {
        start_date: $startDate.val(),
        end_date: $endDate.val()
      };

      $.ajax({
        url: $urlMonthlyInc,
        method: "GET",
        dataType: 'json',
        data: params,
        success: function(response){
           console.log(response);
          $reportSection.html(
            Mustache.render(
              $reportTemplate.html(),
              response.data
            )
          );
        $(".curr").each(function() {
          var x = parseFloat($(this).html());
          $(this).html(numberWithCommas(x));
        });
    
        }
      });

        
      //window.location.href = "/reports/monthly_incentives?" + encodeQueryData(params);
    });
  }
  
   var initializeEvents2 = function(){
    $btnPrintIncentiveTransaction.on("click", function(){
      //alert($startDate.val() + $endDate.val());
     
      var params2 = {
        start_date: $startDate.val(),
        end_date: $endDate.val()
      };

      $.ajax({
        url: $urlMonthlyInc2,
        method: "POST",
        dataType: 'json',
        data: params2,
        success: function(response){
        console.log(response);
        
        window.location.href = "/reports/monthly_incentives_pdf?" + encodeQueryData(params2);
        //response.data
        $(".curr").each(function() {
          var x = parseFloat($(this).html());
          $(this).html(numberWithCommas(x));
        });
    
        }
      });

        
      //window.location.href = "/reports/monthly_incentives?" + encodeQueryData(params);
    });
  }
 
  var init = function(){
    fetchElements();
    initializeEvents();
    initializeEvents2();
  };
  return{
    init: init
  };
})();

