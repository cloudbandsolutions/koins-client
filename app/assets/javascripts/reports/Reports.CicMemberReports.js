CicMemberReports = (function(){
  var $btnNewTransaction = $("#btn-transaction");
  var $startDate = $("#start-date");
  
  var $endDate = $("#end-date");
  
  var _bindEvents = function() {
   $btnNewTransaction.on("click", function(){ 
    //alert("hello jef");
    data = {
      start_date: $startDate.val(),
      end_date: $endDate.val(),
    };

    //window.location = "/reports/cic_reports?" + encodeQueryData(data);
    window.location.href = "/reports/list_of_member_for_cic_excel?" + encodeQueryData(data);
   });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };


})();

