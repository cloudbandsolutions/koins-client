//= require_directory ./lib

Reports.Par = (function() {
  var $searchBtn            = $("#search-btn");
  var $branchSelect         = $("#branch-select");
  var $soSelect             = $("#so-select");
  var $centerSelect         = $("#center-select");
  var $detailedCheckBox     = $("#detailed");
  var $parSection           = $("#reports-par-section");
  var $parTemplate          = $("#reports-par-template").html();
  var $parDetailedTemplate  = $("#reports-par-detailed-template").html();
  var $loanProductSelect    = $("#loan-product-select");
  var $asOf                 = $("#as-of");
  var $printBtn             = $("#print-btn");

  var urlGeneratePar        = "/api/v1/reports/par";
  var urlBranchesSoList     = "/api/v1/branches/so_list";
  var urlCentersBySo        = "/api/v1/branches/centers_by_so";

  var _bindEvents = function() {
    $branchSelect.on('change', function() {
      populateSelect($centerSelect, [], "id", "name");

      if($branchSelect.val()) {
        $.ajax({
          url: urlBranchesSoList,
          data: { branch_id: $branchSelect.val() },
          dataType: 'json',
          success: function(data) {
            toastr.info("Loading SOs...");
            populateSelect($soSelect, data.so_list, "name", "name");
          },
          error: function(data) {
            toastr.error("Error in loading SOs.");
          }
        });
      } else {
        populateSelect($soSelect, [], "name", "name");
      }
    });

    $soSelect.on('change', function() {
      $.ajax({
        url: urlCentersBySo,
        data: { so: $soSelect.val() },
        dataType: 'json',
        success: function(data) {
          toastr.info("Loading Centers...");
          populateSelect($centerSelect, data.centers, "id", "name");
        },
        error: function(data) {
          toastr.error("Error in loading centers.");
        }
      });
    });
    
    //ariel
    $printBtn.on('click', function() {
      $searchBtn.addClass('loading');

      var params = {
        branch_id:        $branchSelect.val(),
        center_id:        $centerSelect.val(),
        so:               $soSelect.val(),
        loan_product_id:  $loanProductSelect.val(),
        detailed:         $detailedCheckBox.bootstrapSwitch('state'),
        as_of:            $asOf.val(),
      };

      window.open("/reports/par_pdf?" + encodeQueryData(params), "_blank");
    });
  //ariel


    $searchBtn.on('click', function() {
      $searchBtn.addClass('loading');

      var params = {
        branch_id:        $branchSelect.val(),
        center_id:        $centerSelect.val(),
        so:               $soSelect.val(),
        loan_product_id:  $loanProductSelect.val(),
        detailed:         $detailedCheckBox.bootstrapSwitch('state'),
        as_of:            $asOf.val(),
      };

      $.ajax({
        url: urlGeneratePar,
        method: 'GET',
        dataType: 'json',
        data: params,
        success: function(data) {
          if($detailedCheckBox.bootstrapSwitch('state')) {
            $parSection.html(Mustache.render($parDetailedTemplate, data));
          } else {
            $parSection.html(Mustache.render($parTemplate, data));
          }

          $parSection.find(".curr").each(function() {
            $(this).html(numberWithCommas($(this).html()));
          });
          // Make sticky
          $(".sticky").stickyTableHeaders();

          $searchBtn.removeClass('loading');
        },
        error: function(data) {
          toastr.error("Error in generating par report");
          $searchBtn.removeClass('loading');
        }
      });
    });
  }

  var init = function() {
    _bindEvents();
  }

  return {
    init: init
  };
})();
