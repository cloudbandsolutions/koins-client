var Index = (function() {
  var $clickable;

  var _cacheDom = function() {
    $clickable  = $(".clickable");
  };
  
  var _bindEvents = function() {
    $clickable.on('click', function() {
      var id        = $(this).data('id');
      var memberId  = $(this).data('member-id');
      window.location.href = "/members/" + memberId + "/member_certificates/" + id;
    });
  };

  var init  = function() {
    _cacheDom();
    _bindEvents();
  };

  return {
    init: init
  };
})();
