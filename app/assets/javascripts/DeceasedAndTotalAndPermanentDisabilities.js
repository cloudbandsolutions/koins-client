var DeceasedAndTotalAndPermanentDisabilities = (function() {
  var $btnApprove;
  var $btnReverse;

  var $btnApproveConfirmation;
  var $btnCancelApproval;     

  var $btnReverseConfirmation;
  var $btnCancelReverse;

  var $parameters;
  var deceasedAndTotalAndPermanentDisabilityId;
  var $errors;
  var $errorsTemplate;
  var urlApproveTransaction             = "/api/v1/deceased_and_total_and_permanent_disabilities/approve";
  var urlReverseTransaction             = "/api/v1/deceased_and_total_and_permanent_disabilities/reverse";
  
  var $modalApprove;
  var $modalReverse;
  var $errors;
  var $errorsTemplate;
  var $modalErrorsApproval;
  var $modalErrorsReverse;
  var $modalSuccessApproval;
  var $modalSuccessReverse;
  var $modalControls;
  var $successTemplate;

  var _displayErrors = function(errors) {
    var errorsDisplay = Mustache.render($errorsTemplate.html(), { errors: errors });
    $errors.html(errorsDisplay);
  }

  var _hideErrors = function() {
    $errors.html("");
  }

  var _addLoadingToConfirmationBtns = function() {
    $btnApproveConfirmation.addClass('loading');
    $btnApproveConfirmation.addClass('disabled');
    $btnReverseConfirmation.addClass('loading');
    $btnReverseConfirmation.addClass('disabled');

    $btnCancelApproval.addClass('loading');
    $btnCancelApproval.addClass('disabled');
    $btnCancelReverse.addClass('loading');
    $btnCancelReverse.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnApproveConfirmation.removeClass('loading');
    $btnApproveConfirmation.removeClass('disabled');
    $btnReverseConfirmation.removeClass('loading');
    $btnReverseConfirmation.removeClass('disabled');

    $btnCancelApproval.removeClass('loading');
    $btnCancelApproval.removeClass('disabled');
    $btnCancelReverse.removeClass('loading');
    $btnCancelReverse.removeClass('disabled');
  }

  var _bindEvents = function() {
    $btnApproveConfirmation.on('click', function() {
      if(!$btnApproveConfirmation.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $.ajax({
          url: urlApproveTransaction,
          method: 'POST',
          dataType: 'json',
          data: { id: deceasedAndTotalAndPermanentDisabilityId },
          success: function(responseContent) {
            $modalSuccessApproval.html(Mustache.render($successTemplate.html(), { messages: ["Successfully approved transaction"] }));
            $modalControls.hide();
            window.location.href = "/deceased_and_total_and_permanent_disabilities/" + deceasedAndTotalAndPermanentDisabilityId;
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            $modalErrorsApproval.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to approve Deceased and Total and Permanent Disability");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $btnApprove.on('click', function() {
      $modalSuccessApproval.html("");
      $modalErrorsApproval.html("");
      $modalApprove.open();
    });

    $btnCancelApproval.on('click', function() {
      if(!$btnCancelApproval.hasClass('loading')) {
        $modalApprove.close();
      }
    });

    $btnReverseConfirmation.on('click', function() {
      if(!$btnReverseConfirmation.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $.ajax({
          url:  urlReverseTransaction,
          method: 'POST',
          dataType: 'json',
          data: { id: deceasedAndTotalAndPermanentDisabilityId },
          success: function(responseContent) {
            $modalSuccessReverse.html(Mustache.render($successTemplate.html(), { messages: ["Successfully reversed transaction"] }));
            $modalControls.hide();
            window.location.href = "/deceased_and_total_and_permanent_disabilities/" + deceasedAndTotalAndPermanentDisabilityId;
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            console.log(errorMessages);
            $modalErrorsReverse.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to reverse transaction");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.error("Still loading");
      }
    });

    $btnCancelReverse.on('click', function() {
      if($btnCancelReverse.hasClass('loading')) {
        toastr.info("Still loading");
      } else {
        $modalReverse.close();
      }
    });

    $btnReverse.on('click', function() {
      $modalSuccessApproval.html("");
      $modalErrorsReverse.html("");
      $modalReverse.open();
    });
  }

  var _cacheDom = function() {
    $confirmationModal                        = $("#confirmation-modal"); 
    $btnApprove                               = $("#btn-approve");
    $btnReverse                               = $("#btn-reverse");
    $btnCancelApproval                        = $("#btn-cancel-approval");
    $btnReverseConfirmation                   = $("#btn-confirm-reversal");
    $btnCancelReverse                         = $("#btn-cancel-reversal");
    $btnApproveConfirmation                   = $("#btn-confirm-approval");
    $parameters                               = $("#parameters");
    deceasedAndTotalAndPermanentDisabilityId  = $parameters.data("deceased-and-total-and-permanent-disability-id");
    $errors                                   = $("#errors");
    $errorsTemplate                           = $("#errors-template");
    
    $modalApprove                             = $(".modal-approve-confirmation").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalReverse                             = $(".modal-reverse-confirmation").remodal({ hashTracking: true, closeOnOutsideClick: false });

    $modalErrorsApproval                      = $(".modal-approve").find(".errors");
    $modalErrorsReverse                       = $(".modal-reverse").find(".errors");
    $modalSuccessApproval                     = $(".modal-approve").find(".success");
    $modalSuccessReverse                      = $(".modal-reverse").find(".success");
    $modalControls                            = $(".modal-controls");
    $successTemplate                          = $("#success-template");
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
