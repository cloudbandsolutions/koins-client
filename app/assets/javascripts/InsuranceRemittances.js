var InsuranceRemittances = (function() {
  var $btnGenerateRemittance;
  var $modalBranchSelect;
  var $modalCenterSelect;
  var $inputStartDate;
  var $inputEndDate;

  var _cacheDom = function() {
    $btnGenerateRemittance = $("#btn-generate-remittance");
  }

  var _bindEvents = function() {
    $btnGenerateRemittance.on('click', function() {
      branchId  = $modalBranchSelect.val();
      centerId  = $modalCenterSelect.val();
      startDate = $inputStartDate.val();
      endDate   = $inputEndDate.val();
      window.location.href = "/insurance_remittances/new?branch_id=" + branchId + "&center_id=" + centerId + "&start_date=" + startDate + "&end_date=" + endDate;
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  InsuranceRemittances.init();
});
