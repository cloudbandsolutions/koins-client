var DeceasedAndTotalAndPermanentDisability = (function() {
  var $modalBranchSelect;
  var $datePrepared;
  var $btnGenerateDeceasedAndTotalAndPermanentDisability;
  var $btnCancelGenerateDeceasedAndTotalAndPermanentDisability;
  var $btnNewDeceasedAndTotalAndPermanentDisability;
  var $generateDeceasedAndTotalAndPermanentDisabilityModal;

  var urlUtilsBranches             = "/api/v1/utils/branches";
  
  var $errors;  
  var $errorsTemplate;
  var $modalErrors;
  var $modalSuccess;
  var $modalControls;
  var $successTemplate;
  var $clickableRow; 

  var $modalLoading;
  var $modalLoadingErrors;
  var $modalLoadingSuccess; 
  var $modalLoadingControls;
  var $modalLoadingBtnClose;

  var urlGenerateDeceasedAndTotalAndPermanentDisabilityTransaction = "/api/v1/deceased_and_total_and_permanent_disabilities/generate_transaction";

  var _cacheDom = function() {
    $modalBranchSelect                                        = $("#modal-branch-select");
    $btnGenerateDeceasedAndTotalAndPermanentDisability        = $("#btn-generate-deceased-and-total-and-permanent-disability");
    $btnCancelGenerateDeceasedAndTotalAndPermanentDisability  = $("#btn-cancel-generate-deceased-and-total-and-permanent-disability");
    $datePrepared                                             = $("#date-prepared");
    $generateDeceasedAndTotalAndPermanentDisabilityModal      = $(".modal-generate-deceased-and-total-and-permanent-disability").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $btnNewDeceasedAndTotalAndPermanentDisability             = $("#btn-new-deceased-and-total-and-permanent-disability");
    $modalErrors                                              = $(".modal-generate-deceased-and-total-and-permanent-disability").find(".errors");
    $errors                                                   = $(".errors");
    $modalSuccess                                             = $(".modal-generate-deceased-and-total-and-permanent-disability").find(".success");
    $modalControls                                            = $(".modal-generate-deceased-and-total-and-permanent-disability").find(".controls");
    $errorsTemplate                                           = $("#errors-template");
    $successTemplate                                          = $("#success-template");
    $clickableRow                                             = $(".clickable");
   

    $modalLoading                                             = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingErrors                                       = $(".modal-loading").find(".errors");
    $modalLoadingSuccess                                      = $(".modal-loading").find(".success");
    $modalLoadingControls                                     = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
    $modalLoadingBtnClose                                     = $(".modal-loading").find(".btn-close");
  }

  var _addLoadingToConfirmationBtns = function() {
    $btnGenerateDeceasedAndTotalAndPermanentDisability.addClass('loading');
    $btnGenerateDeceasedAndTotalAndPermanentDisability.addClass('disabled');

    $btnCancelGenerateDeceasedAndTotalAndPermanentDisability.addClass('loading');
    $btnCancelGenerateDeceasedAndTotalAndPermanentDisability.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnGenerateDeceasedAndTotalAndPermanentDisability.removeClass('loading');
    $btnGenerateDeceasedAndTotalAndPermanentDisability.removeClass('disabled');

    $btnCancelGenerateDeceasedAndTotalAndPermanentDisability.removeClass('loading');
    $btnCancelGenerateDeceasedAndTotalAndPermanentDisability.removeClass('disabled');
  }

  var _bindEvents = function() {
    $modalLoadingBtnClose.on('click', function() {
      $modalLoading.close();
    });


    $clickableRow.on('click', function() {
      transactionId = $(this).data('transaction-id');
      window.location = "/deceased_and_total_and_permanent_disabilities/" + transactionId;
    });

    $btnNewDeceasedAndTotalAndPermanentDisability.on('click', function() {
      $generateDeceasedAndTotalAndPermanentDisabilityModal.open();
    });

    $btnCancelGenerateDeceasedAndTotalAndPermanentDisability.on('click', function() {
      if(!$btnGenerateDeceasedAndTotalAndPermanentDisability.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $generateDeceasedAndTotalAndPermanentDisabilityModal.close();
        _removeLoadingToConfirmationBtns();
      } else {
        toastr.info("Still loading");
      }
    });

    $btnGenerateDeceasedAndTotalAndPermanentDisability.on('click', function() {
      var branchId       = $modalBranchSelect.val();
      var datePrepared  = $datePrepared.val();
      var data = {
        branch_id: branchId,
        date_prepared: datePrepared
      };

      if(!$btnGenerateDeceasedAndTotalAndPermanentDisability.hasClass('loading')) {
        $modalErrors.html("");
        $modalSuccess.html("");
        _addLoadingToConfirmationBtns();
        $.ajax({
          url: urlGenerateDeceasedAndTotalAndPermanentDisabilityTransaction,
          data: data,
          dataType: 'json',
          method: 'POST',
          success: function(responseContent) {
            console.log(responseContent);
            _removeLoadingToConfirmationBtns();
            $modalSuccess.html(Mustache.render($successTemplate.html(), { messages: responseContent.messages }));
            $modalControls.hide();
            r_id = responseContent.deceased_and_total_and_permanent_disability_id;
            window.location = "/deceased_and_total_and_permanent_disabilities/" + r_id + "/edit";
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            console.log(errorMessages);
            $modalErrors.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to generate deposit transaction");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $modalBranchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($modalBranchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
