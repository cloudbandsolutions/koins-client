var Show  = (function() {
  var $btnSend;
  var $modalLoading;
  var $message;
  var $errorsTemplate;
  var $parameters;
  var dailyReportId;
  var urlSend = "/api/v1/head_office/send_daily_report";

  var init  = function() {
    _cacheDom();
    _bindEvents();
  };

  var _bindEvents = function() {
    $btnSend.on("click", function() {
      $btnSend.prop("disabled", true);
      $modalLoading.modal("show");

      $.ajax({
        url: urlSend,
        data: {
          id: dailyReportId
        },
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          $message.html("Success! Redirecting...");
          window.location.href = "/monitoring/daily_reports/" + dailyReportId;
        },
        error: function(response) {
          $message.html(
            "Error in sending to server"
          );

          $btnSend.prop("disabled", false);
        }
      });
    });
  };

  var _cacheDom = function() {
    $btnSend        = $("#btn-send");
    $modalLoading   = $("#modal-loading");
    $errorsTemplate = $("#errors-template");
    $parameters     = $("#parameters");
    dailyReportId   = $parameters.data("id");
  };

  return {
    init: init
  };
})();
