var Surveys = (function() {

  var $clickableRow;

 
  var _cacheDom = function() {
    $clickableRow         = $(".clickable");
  }

  var _bindEvents = function() {
    $clickableRow.on('click', function() {
      Id = $(this).data('transaction-id');
      window.location = "/surveys/" + Id;
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();

$(document).ready(function() {
  Surveys.init();
});
