var MemberFilter = (function() {
  //this
  var $branchSelect;
  var $centerSelect;
  var $filterSelect;
  
  var urlUtilsBranches  = "/api/v1/utils/branches";
  var urlUtilsCenters   = "/api/v1/utils/centers";

  var $filterStatus
  var $filterName; 
  var $filterBranchSelect;
  var $filterCenterSelect;
  var $filterMemberTypeSelect;
  var $btnFilter;

  var $modalLoading;
  var $modalLoadingErrors;
  var $modalLoadingSuccess; 
  var $modalLoadingControls;
  var $modalLoadingBtnClose;

  var $clickableRow;


  var _cacheDom = function() {
    //this
    $branchSelect        = $(".branch-select");
    $centerSelect        = $(".center-select");
    $filterSelect        = $(".filter-select");

    $filterStatus = $("#filter-status-select")
    $filterName = $("#filter-name");
    $filterBranchSelect = $("#filter-branch-select");
    $filterCenterSelect = $("#filter-center-select");
    $filterMemberTypeSelect = $("#filter-member-type-select");
    $btnFilter = $("#btn-filter");

    $modalLoading             = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingErrors       = $(".modal-loading").find(".errors");
    $modalLoadingSuccess      = $(".modal-loading").find(".success");
    $modalLoadingControls     = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
    $modalLoadingBtnClose     = $(".modal-loading").find(".btn-close");

    $clickableRow             = $(".clickable");
  }

   var _bindEvents = function() {

    $modalLoadingBtnClose.on('click', function() {
      $modalLoading.close();
    });

    $btnFilter.on('click', function() {
      $modalLoadingControls.hide();
      
      var filterStatus = $filterStatus.val();
      var filterName  = $filterName.val();
      var branchId  = $filterBranchSelect.val();
      var centerId   = $filterCenterSelect.val();
      var memberType = $filterMemberTypeSelect.val();

      var data = {
        name: filterName,
        branch_id: branchId,
        center_id: centerId,
        status: filterStatus,
        member_type: memberType,
      }; 

      $modalLoading.open();

      window.location = "/members?" + encodeQueryData(data);
    });

    $clickableRow.on('click', function() {
      memberId = $(this).data('member-id');
      window.location = "/members/" + memberId;
    });

     $branchSelect.bind('afterShow', function() {
      $.ajax({
        url: '/api/v1/utils/branches',
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect(this, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });

    // On change of branch select
    $branchSelect.bind('change', function() {
      var branchId = ($(this).val());  
      params = { branch_id: branchId };

      // Clear centers
      $centerSelect.find('option').remove();

      if(branchId) {
        $.ajax({
          url: '/api/v1/utils/centers' ,
          type: 'get',
          dataType: 'json',
          data: params,
          success: function(data) {
            var centers = data.data.centers;
            populateSelect($centerSelect, centers, 'id', 'name');

            var centerId = $("#parameters").data("center-id");
            if(centerId) {
              console.log("Setting center to " + centerId);
              $centerSelect.val(centerId).trigger("change");
            }
          },
          error: function() {
            toastr.error("ERROR: loading centers on branch select");
          }
        });
      }
    });

    // Preload centers
    var branchId = ($branchSelect.val());  
    params = { branch_id: branchId };

    if(branchId) {
      $.ajax({
        url: '/api/v1/utils/centers' ,
        type: 'get',
        dataType: 'json',
        data: params,
        success: function(data) {
          var centers = data.data.centers;
          populateSelect($centerSelect, centers, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading centers on branch select");
        }
      });
    }

 
  }



  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
