var LoanInsurance = (function() {
  var $modalBranchSelect;
  var $dateOfPayment;
  var $modalLoanInsuranceTypeSelect;
  var $modalCollectionTypeSelect;
  var $btnGenerateLoanInsurance;
  var $btnCancelGenerateLoanInsurance;
  // var $btnFilter;
  var $btnNewLoanInsurance;
  var $generateLoanInsuranceModal;

  var urlUtilsBranches             = "/api/v1/utils/branches";
  var urlUtilsLoanInsuranceTypes   = "/api/v1/utils/loan_insurance_types";
  // var urlNewLoanInsurance          = "/loan_insurances/new";

  // var $filterOrNumber; 
  // var $filterStartDate;
  // var $filterEndDate;
  // var $filterBranchSelect;
  // var $filterStatus;

  var $errors;  
  var $errorsTemplate;
  var $modalErrors;
  var $modalSuccess;
  var $modalControls;
  var $successTemplate;
  var $clickableRow; 

  var $modalLoading;
  var $modalLoadingErrors;
  var $modalLoadingSuccess; 
  var $modalLoadingControls;
  var $modalLoadingBtnClose;

  var urlGenerateLoanInsuranceTransaction = "/api/v1/loan_insurances/generate_transaction";

  var _cacheDom = function() {
    $modalBranchSelect               = $("#modal-branch-select");
    $modalLoanInsuranceTypeSelect    = $("#modal-loan-insurance-type-select");
    $modalCollectionTypeSelect       = $("#modal-collection-type-select");
    $btnGenerateLoanInsurance        = $("#btn-generate-loan-insurance");
    $btnCancelGenerateLoanInsurance  = $("#btn-cancel-generate-loan-insurance");
    $dateOfPayment                   = $("#date-of-payment");
    $generateLoanInsuranceModal      = $(".modal-generate-loan-insurance").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $btnNewLoanInsurance             = $("#btn-new-loan-insurance");
    $modalErrors                     = $(".modal-generate-loan-insurance").find(".errors");
    $errors                          = $(".errors");
    $modalSuccess                    = $(".modal-generate-loan-insurance").find(".success");
    $modalControls                   = $(".modal-generate-loan-insurance").find(".controls");
    $errorsTemplate                  = $("#errors-template");
    $successTemplate                 = $("#success-template");
    $clickableRow                    = $(".clickable");
    // $btnFilter                    = $("#btn-filter");

    // $filterOrNumber           = $("#filter-or-number");
    // $filterStartDate          = $("#filter-start-date");
    // $filterEndDate            = $("#filter-end-date");
    // $filterBranchSelect       = $("#filter-branch-select");
    // $filterStatus             = $("#filter-status");

    $modalLoading             = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingErrors       = $(".modal-loading").find(".errors");
    $modalLoadingSuccess      = $(".modal-loading").find(".success");
    $modalLoadingControls     = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
    $modalLoadingBtnClose     = $(".modal-loading").find(".btn-close");
  }

  var _addLoadingToConfirmationBtns = function() {
    $btnGenerateLoanInsurance.addClass('loading');
    $btnGenerateLoanInsurance.addClass('disabled');

    $btnCancelGenerateLoanInsurance.addClass('loading');
    $btnCancelGenerateLoanInsurance.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnGenerateLoanInsurance.removeClass('loading');
    $btnGenerateLoanInsurance.removeClass('disabled');

    $btnCancelGenerateLoanInsurance.removeClass('loading');
    $btnCancelGenerateLoanInsurance.removeClass('disabled');
  }

  var _bindEvents = function() {
    $modalLoadingBtnClose.on('click', function() {
      $modalLoading.close();
    });

    // $btnFilter.on('click', function() { 
    //   $modalLoadingControls.hide();

    //   var orNumber  = $filterOrNumber.val();
    //   var startDate = $filterStartDate.val();
    //   var endDate   = $filterEndDate.val();
    //   var branchId  = $filterBranchSelect.val();
    //   var tStatus   = $filterStatus.val();

    //   var data = {
    //     or_number: orNumber,
    //     start_date: startDate,
    //     end_date: endDate,
    //     branch_id: branchId,
    //     t_status: tStatus
    //   };

    //   $modalLoading.open();

    //   window.location = "/cash_management/deposits/payment_collections?" + encodeQueryData(data);
    // });

    $clickableRow.on('click', function() {
      transactionId = $(this).data('transaction-id');
      window.location = "/loan_insurances/" + transactionId;
    });

    $btnNewLoanInsurance.on('click', function() {
      $generateLoanInsuranceModal.open();
    });

    $btnCancelGenerateLoanInsurance.on('click', function() {
      if(!$btnGenerateLoanInsurance.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $generateLoanInsuranceModal.close();
        _removeLoadingToConfirmationBtns();
      } else {
        toastr.info("Still loading");
      }
    });

    $btnGenerateLoanInsurance.on('click', function() {
      var collectionType = $modalCollectionTypeSelect.val();
      var branchId       = $modalBranchSelect.val();
      var dateOfPayment  = $dateOfPayment.val();
      var loanInsuranceTypeId = $modalLoanInsuranceTypeSelect.val();
      var data = {
        collection_type: collectionType,
        branch_id: branchId,
        date_of_payment: dateOfPayment,
        loan_insurance_type_id: loanInsuranceTypeId
      };

      if(!$btnGenerateLoanInsurance.hasClass('loading')) {
        $modalErrors.html("");
        $modalSuccess.html("");
        _addLoadingToConfirmationBtns();
        $.ajax({
          url: urlGenerateLoanInsuranceTransaction,
          data: data,
          dataType: 'json',
          method: 'POST',
          success: function(responseContent) {
            console.log(responseContent);
            _removeLoadingToConfirmationBtns();
            $modalSuccess.html(Mustache.render($successTemplate.html(), { messages: responseContent.messages }));
            $modalControls.hide();
            li_id = responseContent.loan_insurance_id;
            window.location = "/loan_insurances/" + li_id + "/edit";
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            console.log(errorMessages);
            $modalErrors.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to generate deposit transaction");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $modalBranchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($modalBranchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });

    $modalLoanInsuranceTypeSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsLoanInsuranceTypes,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var loan_insurance_types = data.data.loan_insurance_types;
          populateSelect($modalLoanInsuranceTypeSelect, loan_insurance_types, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
