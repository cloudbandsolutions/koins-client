var Loan = (function() {
  var $btnApprove;
  var $approveLoanModal;
  var $approveConfirmBtn;
  var $approveCancelBtn;

  var $reverseBtn;
  var $reverseLoanModal;
  var $reverseConfirmBtn;
  var $reverseCancelBtn;
  var $reverseModalControls;

  var $loanDetails;
  var approveUrl      = "/api/v1/loans/approve";
  var reverseUrl      = "/api/v1/loans/reverse";
  var writeoffUrl     = "/api/v1/loans/writeoff";
  var reamortizeUrl   = "/api/v1/loans/reamortize";
  var paymentUrl      = "/api/v1/loans/make_individual_payment";
  var $modalControls;
  var loanId;

  var $errors;
  var $errorsTemplate;
  var $modalErrorsApproval;
  var $modalErrorsReverse;
  var $modalSuccessApproval;
  var $modalSuccessReverse;
  var $modalErrorsWriteoff;
  var $modalSuccessWriteoff;
  var $modalMessageApproval;
  var $modalControls;
  var $successTemplate;

  var $writeoffBtn;
  var $writeoffLoanModal;
  var $writeoffConfirmBtn;
  var $writeoffCancelBtn;
  var $writeoffModalControls;

  var $reamortizeBtn;
  var $reamortizeModal;
  var $reamortizeControls;
  var $reamortizeInputInterestRate;
  var $reamortizeInputNumInstallments;
  var $reamortizeInputPaymentTerm;

  var $paymentModal;
  var $payBtn;
  var $btnSubmitPayment;
  var $inputPaymentAmount;
  var $inputPaymentParticular;
  var $inputDebitAccountingCodeId;

  var _cacheDom = function() {
    $loanDetails          = $("#loan-details");
    loanId                = $loanDetails.data("loan-id");
    $btnApprove           = $("#approve-btn");
    $writeoffBtn          = $("#btn-writeoff");
    $reverseBtn           = $("#reverse-btn");
    $errors               = $("#errors");
    $errorsTemplate       = $("#errors-template");
    $successTemplate      = $("#success-template");
    $modalControls        = $("#modal-controls");
    $reverseModalControls = $("#reverse-modal-controls");
    $reverseLoanModal     = $(".modal-reverse-loan-modal").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $reverseConfirmBtn    = $(".modal-reverse-loan-modal").find("#btn-reverse-confirmation");
    $reverseCancelBtn     = $(".modal-reverse-loan-modal").find("#btn-cancel-reverse");
    $approveLoanModal     = $(".modal-approve-loan-modal").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $approveConfirmBtn    = $(".modal-approve-loan-modal").find('#btn-approve-confirmation');
    $approveCancelBtn     = $(".modal-approve-loan-modal").find('#btn-cancel-approval');
    $modalErrorsApproval  = $(".modal-approve-loan-modal").find(".errors");
    $modalErrorsReverse   = $(".modal-reverse-loan-modal").find(".errors");
    $modalMessageApproval = $(".modal-approve-loan-modal").find(".message");
    $modalSuccessApproval = $(".modal-approve-loan-modal").find(".success");
    $modalSuccessReverse  = $(".modal-reverse-loan-modal").find(".success");
    $writeoffLoanModal    = $(".modal-writeoff-loan-modal").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $writeoffModalControls = $("#writeoff-modal-controls");
    $modalSuccessWriteoff = $(".modal-writeoff-loan-modal").find(".success");
    $modalErrorsWriteoff  = $(".modal-writeoff-loan-modal").find(".errors");

    $reamortizeBtn        = $("#reamortize-btn");
    $reamortizeModal      = $(".modal-reamortize-loan-modal").remodal({ 
                              hashTracking: true, 
                              closeOnOutsideClick: false 
                            });
    $reamortizeControls   = $(".modal-reamortize-loan-modal").find(".controls");
    $reamortizeMessage    = $(".modal-reamortize-loan-modal").find(".message");
    $reamortizeErrors     = $(".modal-reamortize-loan-modal").find(".errors");
    $btnApproveReamortization = $("#btn-approve-reamortization");
    $btnCancelReamortization  = $("#btn-cancel-reamortization");

    $reamortizeInputInterestRate    = $("#input-interest-rate");
    $reamortizeInputNumInstallments = $("#input-num-installments");
    $reamortizeInputPaymentTerm     = $("#input-payment-term");

    $writeoffConfirmBtn   = $("#btn-writeoff-confirmation");
    $writeoffCancelBtn    = $("#btn-cancel-writeoff");

    $paymentModal               = $("#modal-add-payment");
    $payBtn                     = $("#pay-btn");
    $btnSubmitPayment           = $("#btn-submit-payment");
    $inputPaymentAmount         = $("#input-payment-amount");
    $inputPaymentParticular     = $("#input-payment-particular");
    $inputDebitAccountingCodeId = $("#input-debit-accounting-code-id");
  }

  var _reamortizeLoan = function() {
    $reamortizeControls.hide();
    $reamortizeMessage.html("");
    $reamortizeMessage.html("Loading...");
    data = {
      id: loanId,
      new_interest_rate: $reamortizeInputInterestRate.val(),
      new_num_installments: $reamortizeInputNumInstallments.val(),
      new_term: $reamortizeInputPaymentTerm.val()
    };
    console.log(data);

    $.ajax({
      url: reamortizeUrl,
      method: 'POST',
      data: data,
      dataType: 'json',
      success: function(data) {
        toastr.success("Successfully reamortized loan");
        $modalMessageApproval.html("Successfully reamortized loan! Redirecting...");
        window.location.replace("/loans/" + loanId);
      },
      error: function(response) {
        toastr.error("Something went wrong while reamortizing loan.");
        $reamortizeControls.show();
      }
    });
  }

  var _bindReamortizationEvents = function() {
    $reamortizeModal.open();
  }

  var _bindApproveConfirmEvents = function() {
    $modalControls.hide();
    $modalMessageApproval.html("Processing...");
    var params = { loan_id: loanId };
    $.ajax({
      url: approveUrl,
      method: 'POST',
      data: params,
      dataType: 'json',
      success: function(data) {
        toastr.success(data.message);
        $modalMessageApproval.html("Success! Redirecting...");
        window.location.replace("/loans/" + loanId);
      },
      error: function(data) {
        toastr.error("Something went wrong.");
        var errorMessages = JSON.parse(data.responseText).errors;
        $modalErrorsApproval.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
        $modalMessageApproval.html("");
        $modalControls.show();
      }
    });
  }

  var _bindEvents = function() {
    $reamortizeBtn.on('click', _bindReamortizationEvents);

    $btnApproveReamortization.on('click', _reamortizeLoan);
    $btnCancelReamortization.on('click', function() { $reamortizeModal.close(); });

    $approveConfirmBtn.on('click', _bindApproveConfirmEvents);

    $writeoffBtn.on('click', function() {
      $writeoffLoanModal.open();
    });

    $writeoffCancelBtn.on('click', function() {
      $writeoffLoanModal.close();
    });

    $writeoffConfirmBtn.on('click', function() {
      $writeoffModalControls.hide();
      var params = { loan_id: loanId };
      $.ajax({
        url: writeoffUrl,
        method: 'POST',
        data: params,
        dataType: 'json',
        success: function(data) {
          toastr.success(data.message);
          window.location.replace("/loans/" + loanId);
        },
        error: function(data) {
          toastr.error("Something went wrong.");
          var errorMessages = JSON.parse(data.responseText).errors;
          $modalErrorsWriteoff.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
          $writeoffModalControls.show();
        }
      });
    });

    $approveCancelBtn.on('click', function() {
      $approveLoanModal.close();
    });

    $btnApprove.on('click', function() {
      $approveLoanModal.open();
    });

    $reverseBtn.on('click', function() {
      $reverseLoanModal.open();
    });

    $payBtn.on('click', function() {
      $paymentModal.modal('toggle');
    });

    $btnSubmitPayment.on('click', function() {
      var data = {
        amount: $inputPaymentAmount.val(),
        particular: $inputPaymentParticular.val(),
        loan_id: loanId,
        debit_accounting_code_id: $inputDebitAccountingCodeId.val()
      };

      $paymentModal.find(".errors").html("");

      $paymentModal.find(".message").html("Loading...");
      $paymentModal.find(".form-actions").hide();

      $.ajax({
        url: paymentUrl,
        data: data,
        dataType: 'json',
        method: 'POST',
        success: function(response) {
          $inputPaymentAmount.val("");
          $inputPaymentParticular.val("");
          $paymentModal.find(".errors").html("");
          $paymentModal.find(".message").html("Success! Redirecting...");
          window.location.href = "/payment_collections/" + response.payment_collection_id + "/edit";
        },
        error: function(response) {
          console.log(response);
          toastr.error("Something went wrong.");
          var errorMessages = JSON.parse(response.responseText).errors;
          $paymentModal.find(".errors").html(
            Mustache.render(
              $errorsTemplate.html(), 
              { 
                errors: 
                errorMessages 
              }
            )
          );
          $paymentModal.find(".form-actions").show();
        }
      });
    });

    $reverseConfirmBtn.on('click', function() {
      var params = {
        loan_id: loanId
      }

      $reverseModalControls.hide();
      $.ajax({
        url: reverseUrl,
        method: 'POST',
        data: params,
        dataType: 'json',
        success: function(data) {
          toastr.success(data.message);
          window.location.replace("/loans/" + loanId);
        },
        error: function(data) {
          var errorMessages = JSON.parse(data.responseText).errors;
          $modalErrorsReverse.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
          toastr.error("Something went wrong when trying to reverse loan");
          $reverseModalControls.show();
        }
      });
    });

    $reverseCancelBtn.on('click', function() {
      $reverseLoanModal.close();
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
