var TransferredInterestOnSharedCapital = (function(){
  var $selectedYear = $("#selected_year");
  var $btnAction= $("#btn-action");
  var $btnExcel = $("#btn-download-excel-cleared");
  var $btnApproved = $("#btn-approved-record");
  var $templateInterestOnShareCapital = $("#template_interest_on_share_capital");
  var $ICPRsection = $("#ICPR-section");
  var $url = "pages/transferred_icpr_member_data";
  var $urlApproveTransaction        = "/api/v1/accounting/interest_on_share_capital_collections/approved_cleared";

  var init = function() {

    $btnApproved.on("click", function(){
      var params = {
        selected_years: $selectedYear.val() 
      };  
     
      $.ajax({
        url: $urlApproveTransaction,
        method: "GET",
        dataType: "json",
        data: params,
        success: function(response){
          alert("jef");
        }
      });


    
    });

    
    $btnExcel.on("click", function(){
      data  ={
        id: $selectedYear.val()
      }
      
      window.location.href = "/pages/transferred_interest_on_sharecapital_excel?" + encodeQueryData(data);

    });



    $btnAction.on("click", function(){
      var params ={
        share_capital_interest_id: $selectedYear.val()
      };


      $.ajax({
        url: $url,
        method: "GET",
        dataType: 'json',
        data: params,
        success: function(response) {
          console.log(response);
          $ICPRsection.html(
            Mustache.render(
              $templateInterestOnShareCapital.html(),
              response.data
            )
          );
        }
      
      });


    });
  
  };

  return {
    init: init
  };


})();
