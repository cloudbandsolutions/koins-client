//= require_directory ./lib

Exports.Beneficiaries = (function() {
  var $centerSelect = $("#center-select");
  var $btnDownload  = $("#export-beneficiaries-segment").find(".btn-download");
  var urlExport     = "/exports/beneficiaries_excel";
  
  var $branchSelect = $("#branch-select");
  var $branchSelectMembersSpouse = $("#branch-select-members-spouse");
  var $btnDownloadMemberPerBranch = $("#export-member-per-branch-segment").find(".btn-download-member-per-branch");
  var $btnDownloadMembersSpouse  = $("#members-spouse-csv-export-segment").find(".btn-members-spouse-export-csv");
  var urlExportMemberPerBranch = "/exports/members_per_branch_excel";
  var urlExportMembersSpouseExportCsv = "/exports/members_spouse_export_csv";

  var $startDate = $("#filter").find("#filter-start-date");
  var $endDate = $("#filter").find("#filter-end-date");
  var $btnDownloadMember  = $("#export-member-segment-csv").find(".btn-download-members-csv");
  var $btnDownloadBeneficiary  = $("#export-member-segment-csv").find(".btn-download-beneficiaries-csv");
  var $btnDownloadDependent  = $("#export-member-segment-csv").find(".btn-download-dependents-csv");
  var $btnDownloadInsuranceAccount  = $("#export-transaction-segment-csv").find(".btn-download-insurance-accounts-csv");
  var $btnDownloadInsuranceAccountTransaction  = $("#export-transaction-segment-csv").find(".btn-download-insurance-account-transactions-csv");
  var $btnDownloadInsuredLoan  = $("#export-insured-loan-segment-csv").find(".btn-download-insured-loans-csv");
  var $btnDownloadWithdrawalForHiip  = $("#export-withdrawal-for-hiip-segment-csv").find(".btn-download-withdrawal-for-hiip-csv");

  var urlExportMember     = "/exports/members";
  var urlExportBeneficiary     = "/exports/beneficiaries";
  var urlExportLegalDependent     = "/exports/legal_dependents";
  var urlExportInsuranceAccount     = "/exports/insurance_accounts";
  var urlExportInsuranceAccountTransaction     = "/exports/insurance_account_transactions";
  var urlExportInsuredLoan     = "/exports/insured_loans";
  var urlExportWithdrawalForHiip     = "/exports/withdrawal_for_hiip";

  var init  = function() {
    $btnDownloadMemberPerBranch.on('click', function() {
      var params    = {
        branch_id:  $branchSelect.val(),
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportMemberPerBranch + "?" + encodeQueryData(params);
    });

    $btnDownloadMembersSpouse.on('click', function() {
      var params    = {
        branch_id:  $branchSelectMembersSpouse.val(),
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportMembersSpouseExportCsv + "?" + encodeQueryData(params);
    });


    $btnDownload.on('click', function() {
      var params    = {
        center_id:  $centerSelect.val(),
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExport + "?" + encodeQueryData(params);
    });

    $btnDownloadMember.on('click', function() {
      var params    = {
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportMember + "?" + encodeQueryData(params);
    });

    $btnDownloadBeneficiary.on('click', function() {
      var params    = {
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportBeneficiary + "?" + encodeQueryData(params);
    });

    $btnDownloadDependent.on('click', function() {
      var params    = {
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportLegalDependent + "?" + encodeQueryData(params);
    });


    $btnDownloadInsuranceAccount.on('click', function() {
      var params    = {
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportInsuranceAccount + "?" + encodeQueryData(params);
    });


    $btnDownloadInsuranceAccountTransaction.on('click', function() {
      var params    = {
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportInsuranceAccountTransaction + "?" + encodeQueryData(params);
    });

    $btnDownloadWithdrawalForHiip.on('click', function() {
      var params    = {
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportWithdrawalForHiip + "?" + encodeQueryData(params);
    });

    $btnDownloadInsuredLoan.on('click', function() {
      var params    = {
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportInsuredLoan + "?" + encodeQueryData(params);
    });
  };

  return {
    init: init
  };
})();
