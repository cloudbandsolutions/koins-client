//= require_directory ./lib

Exports.Tools = (function() {
  var $startDate = $("#filter").find("#filter-start-date");
  var $endDate = $("#filter").find("#filter-end-date");
  var $btnDownloadMember  = $("#export-member-segment").find(".btn-download-members");
  var urlExportMember     = "/exports/members";

  var init  = function() {
    $btnDownloadMember.on('click', function() {
      var params    = {
        start_date:  $startDate.val(),
        end_date:  $endDate.val()
      }

      window.location = urlExportMember + "?" + encodeQueryData(params);
    });
  };

  return {
    init: init
  };
})();
