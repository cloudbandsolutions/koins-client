var InsuranceAccountValidation = (function() {
  var $modalBranchSelect;
  var $datePrepared;
  var $btnGenerateInsuranceAccountValidation;
  var $btnCancelGenerateInsuranceAccountValidation;
  var $btnNewInsuranceAccountValidation;
  var $generateInsuranceAccountValidationModal;

  var urlUtilsBranches             = "/api/v1/utils/branches";
  
  var $errors;  
  var $errorsTemplate;
  var $modalErrors;
  var $modalSuccess;
  var $modalControls;
  var $successTemplate;
  var $clickableRow; 

  var $modalLoading;
  var $modalLoadingErrors;
  var $modalLoadingSuccess; 
  var $modalLoadingControls;
  var $modalLoadingBtnClose;

  var urlGenerateInsuranceAccountValidationTransaction = "/api/v1/insurance_account_validations/generate_transaction";

  var _cacheDom = function() {
    $modalBranchSelect               = $("#modal-branch-select");
    $btnGenerateInsuranceAccountValidation = $("#btn-generate-insurance-account-validation");
    $btnCancelGenerateInsuranceAccountValidation  = $("#btn-cancel-generate-insurance-account-validation");
    $datePrepared                   = $("#date-prepared");
    $generateInsuranceAccountValidationModal      = $(".modal-generate-insurance-account-validation").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $btnNewInsuranceAccountValidation             = $("#btn-new-insurance-account-validation");
    $modalErrors                     = $(".modal-generate-insurance-account-validation").find(".errors");
    $errors                          = $(".errors");
    $modalSuccess                    = $(".modal-generate-insurance-account-validation").find(".success");
    $modalControls                   = $(".modal-generate-insurance-account-validation").find(".controls");
    $errorsTemplate                  = $("#errors-template");
    $successTemplate                 = $("#success-template");
    $clickableRow                    = $(".clickable");
   

    $modalLoading             = $(".modal-loading").remodal({ hashTracking: true, closeOnOutsideClick: false });
    $modalLoadingErrors       = $(".modal-loading").find(".errors");
    $modalLoadingSuccess      = $(".modal-loading").find(".success");
    $modalLoadingControls     = $(".modal-loading").find(".controls");
    $modalLoadingControls.hide();
    $modalLoadingBtnClose     = $(".modal-loading").find(".btn-close");
  }

  var _addLoadingToConfirmationBtns = function() {
    $btnGenerateInsuranceAccountValidation.addClass('loading');
    $btnGenerateInsuranceAccountValidation.addClass('disabled');

    $btnCancelGenerateInsuranceAccountValidation.addClass('loading');
    $btnCancelGenerateInsuranceAccountValidation.addClass('disabled');
  }

  var _removeLoadingToConfirmationBtns = function() {
    $btnGenerateInsuranceAccountValidation.removeClass('loading');
    $btnGenerateInsuranceAccountValidation.removeClass('disabled');

    $btnCancelGenerateInsuranceAccountValidation.removeClass('loading');
    $btnCancelGenerateInsuranceAccountValidation.removeClass('disabled');
  }

  var _bindEvents = function() {
    $modalLoadingBtnClose.on('click', function() {
      $modalLoading.close();
    });


    $clickableRow.on('click', function() {
      transactionId = $(this).data('transaction-id');
      window.location = "/insurance_account_validations/" + transactionId;
    });

    $btnNewInsuranceAccountValidation.on('click', function() {
      $generateInsuranceAccountValidationModal.open();
    });

    $btnCancelGenerateInsuranceAccountValidation.on('click', function() {
      if(!$btnGenerateInsuranceAccountValidation.hasClass('loading')) {
        _addLoadingToConfirmationBtns();
        $generateInsuranceAccountValidationModal.close();
        _removeLoadingToConfirmationBtns();
      } else {
        toastr.info("Still loading");
      }
    });

    $btnGenerateInsuranceAccountValidation.on('click', function() {
      var branchId       = $modalBranchSelect.val();
      var datePrepared  = $datePrepared.val();
      var data = {
        branch_id: branchId,
        date_prepared: datePrepared
      };

      if(!$btnGenerateInsuranceAccountValidation.hasClass('loading')) {
        $modalErrors.html("");
        $modalSuccess.html("");
        _addLoadingToConfirmationBtns();
        $.ajax({
          url: urlGenerateInsuranceAccountValidationTransaction,
          data: data,
          dataType: 'json',
          method: 'POST',
          success: function(responseContent) {
            console.log(responseContent);
            _removeLoadingToConfirmationBtns();
            $modalSuccess.html(Mustache.render($successTemplate.html(), { messages: responseContent.messages }));
            $modalControls.hide();
            i_id = responseContent.insurance_account_validation_id;
            window.location = "/insurance_account_validations/" + i_id + "/edit";
          },
          error: function(responseContent) {
            var errorMessages = JSON.parse(responseContent.responseText).errors;
            console.log(errorMessages);
            $modalErrors.html(Mustache.render($errorsTemplate.html(), { errors: errorMessages }));
            toastr.error("Something went wrong when trying to generate insurance account validation transaction");
            _removeLoadingToConfirmationBtns();
          }
        });
      } else {
        toastr.info("Still loading");
      }
    });

    $modalBranchSelect.bind('afterShow', function() {
      $.ajax({
        url: urlUtilsBranches,
        type: 'get',
        dataType: 'json',
        success: function(data) {
          var branches = data.data.branches;
          populateSelect($modalBranchSelect, branches, 'id', 'name');
        },
        error: function() {
          toastr.error("ERROR: loading branches");
        }
      });
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
