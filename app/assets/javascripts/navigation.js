$(document).ready(function() {
  // Display modal when transaction details button is selected
  $('#bankBtn').on('click', function(evt) {
    evt.preventDefault();
  
    var bankNavigationModal = $('.bankNavigationModal');
    bankNavigationModal.modal();
  });
});
