var CashManagementFundTransferInsurancePaymentCollection = (function() {
  var $confirmationModal;
  var $reverseConfirmationModal;
  var $btnConfirmApproval;
  var $btnReverseConfirm;
  var $btnAddMember;
  var $btnDelete;
  var $parameters;
  var $memberSelect;
  var paymentCollectionId;
  var $errorsTemplate;

  var $btnApprove                       = $("#btn-approve");
  var $btnReverse                       = $("#btn-reverse");
  var $parameters                       = $("#parameters");
  var paymentCollectionId               = $parameters.data("payment-collection-id");

  var $successTemplate                  = $("#success-template");
  var $errorsTemplate                   = $("#errors-template");

  var $modalApproveTransaction          = $(".modal-approve-transaction").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $modalApproveTransactionMessage   = $(".modal-approve-transaction").find(".message");
  var $modalApproveTransactionControls  = $(".modal-approve-transaction").find(".controls");

  var $btnConfirmApproveTransaction     = $("#btn-confirm-approve-transaction");
  var $btnCancelApproveTransaction      = $("#btn-cancel-approve-transaction");

  var $modalReverseTransaction          = $(".modal-reverse-transaction").remodal({ hashTracking: true, closeOnOutsideClick: false });
  var $modalReverseTransactionMessage   = $(".modal-reverse-transaction").find(".message");
  var $modalReverseTransactionControls  = $(".modal-reverse-transaction").find(".controls");

  var $btnConfirmReverseTransaction     = $("#btn-confirm-reverse-transaction");
  var $btnCancelReverseTransaction      = $("#btn-cancel-reverse-transaction");

  var urlApproveTransaction             = "/api/v1/payment_collections/approve";
  var urlReverseTransaction             = "/api/v1/payment_collections/reverse";
  var urlAddMember                      = "/api/v1/payment_collections/add_member";
  var urlDeletePaymentCollectionRecord  = "/api/v1/payment_collections/delete_payment_collection_record";

  var _cacheDom = function() {
    $reverseConfirmationModal = $("#reverse-confirmation-modal");
    $addMemberModal           = $("#add-member-modal");
    $btnAddMember             = $("#btn-add-member");
    $btnDelete                = $(".btn-delete");
    $memberSelect             = $("#member-select");
  }

  var _bindEvents = function() {
    $btnReverse.on('click', function() {
      $modalReverseTransaction.open();
    });

    $btnConfirmReverseTransaction.on('click', function() {
      $modalReverseTransactionMessage.html("Reversing transaction...");
      $modalReverseTransactionControls.hide();

      $.ajax({
        url: urlReverseTransaction,
        method: 'POST',
        data: { id: paymentCollectionId },
        dataType: 'json',
        success: function(response) {
          toastr.success("Successfully reversed transaction");
          $modalReverseTransactionMessage.html("Successfully reversed transaction. Redirecting...");
          window.location = "/cash_management/fund_transfer/insurance/payment_collections/" + paymentCollectionId;
        },
        error: function(response) {
          toastr.error("Error in reversing transaction");
          $modalReverseTransactionMessage.html(Mustache.render($errorsTemplate.html(), JSON.parse(response.responseText)));
          $modalReverseTransactionControls.show();
        }
      });
    });

    $btnCancelReverseTransaction.on('click', function() {
      $modalReverseTransaction.close();
    });

    $btnConfirmApproveTransaction.on('click', function() {
      $modalApproveTransactionMessage.html("Approving transaction...");
      $modalApproveTransactionControls.hide();

      $.ajax({
        url: urlApproveTransaction,
        method: 'POST',
        data: { id: paymentCollectionId },
        dataType: 'json',
        success: function(response) {
          toastr.success("Successfully approved transaction");
          $modalApproveTransactionMessage.html("Successfully approved transaction. Redirecting...");
          window.location = "/cash_management/fund_transfer/insurance/payment_collections/" + paymentCollectionId;
        },
        error: function(response) {
          toastr.error("Error in approving transaction");
          $modalApproveTransactionMessage.html(Mustache.render($errorsTemplate.html(), JSON.parse(response.responseText)));
          $modalApproveTransactionControls.show();
        }
      });
    });

    $btnDelete.on('click', function() {
      var $btn = $btnDelete;
      $btn.addClass('loading');
      $btn.addClass('disabled');
      var paymentCollectionRecordId = $btn.data('payment-collection-record-id');

      $.ajax({
        url: urlDeletePaymentCollectionRecord,
        method: 'POST',
        dataType: 'json',
        data: { payment_collection_record_id: paymentCollectionRecordId },
        success: function(responseContent) {
          toastr.success("Successfully deleted record");
          window.location.href = "/cash_management/fund_transfer/insurance/payment_collections/" + paymentCollectionId;
        },
        error: function(responseContent) {
          toastr.error("Cannot delete this record");
          $btn.removeClass('loading');
          $btn.removeClass('disabled');
        }
      });
    });

    $btnAddMember.on('click', function() {
      $(this).addClass('loading');
      $(this).addClass('disabled');
      var memberId = $memberSelect.val();

      $.ajax({
        url: urlAddMember,
        method: 'POST',
        dataType: 'json',
        data: { id: paymentCollectionId, member_id: memberId },
        success: function(responseContent) {
          $(this).removeClass('loading');
          $(this).removeClass('disabled');
          toastr.success("Successfully added member");
          window.location.href = "/cash_management/fund_transfer/insurance/payment_collections/" + paymentCollectionId;
        },
        error: function(responseContent) {
          $(this).removeClass('loading');
          $(this).removeClass('disabled');
          var errorMessages = JSON.parse(responseContent.responseText).errors;
          toastr.error("Something went wrong when trying to add member");
          _displayErrors(errorMessages);
        }
      });
    });

    $btnApprove.on('click', function() {
      $modalApproveTransaction.open();
    });

    $btnCancelApproveTransaction.on('click', function() {
      $modalApproveTransaction.close();
    });
  }

  var init = function() {
    _cacheDom();
    _bindEvents();
  }

  return {
    init: init
  };
})();
