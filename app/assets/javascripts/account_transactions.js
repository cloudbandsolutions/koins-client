$(document).ready(function() {
  // Display modal when transaction details button is selected
  $('.accountTransactionDetailsBtn').on('click', function(evt) {
    evt.preventDefault();

    var transactionNumber = $(this).data('transaction-number');
    var transactionCategory = $(this).data('transaction-category');
    var source = $('#transactionTemplate').html();
    var template = Handlebars.compile(source);
    var transactionModal = $('.transactionModal');

    var params = {
      transaction_number: transactionNumber,
      transaction_category: transactionCategory
    }

    var url = '/api/v1/transactions/show';

    $.ajax({
      url: url,
      data: params,
      success: function(data) {
        transactionModal.modal();
        console.log(data.data.transaction);
        var context = { 
          transaction: data.data.transaction,
          transaction_number: data.data.transaction_number,
          transaction_category: data.data.transaction_category
        };
        var html = template(context);
        transactionModal.find('.modal-body-content').html(html);
      },
      error: function() {
        toastr.error("ERROR: cannot get transaction");
      }
    });
  });

  // Approve this transaction
  $('.approve-transaction-btn').on('click', function(evt) {
    console.log('click');
    evt.preventDefault();

    var transactionNumber = $(this).data('transaction-number');
    var transactionCategory = $(this).data('transaction-category');
    var params = {
      transaction_number: transactionNumber,
      transaction_category: transactionCategory
    }

    $(this).attr('disabled', 'disabled');
    console.log('here');
  });
});

