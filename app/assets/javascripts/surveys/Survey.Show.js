//= require_directory ./lib

Survey.Show = (function() {
  var $btnAdd     = $(".btn-add");
  var $parameters = $("#parameters");
  var $surveyId   = $parameters.data('survey-id');

  var $modalAdd         = $(".modal-add").remodal();
  var $modalAddSection  = $(".modal-add");
  var $modalAddMessage  = $modalAddSection.find(".message");
  var $ModalAddUi       = $modalAddSection.find(".ui");
  var $btnConfirmAdd    = $modalAddSection.find("#btn-confirm-add");
  var $btnCancelAdd     = $modalAddSection.find("#btn-cancel-add");

  var $dateAnswered     = $modalAddSection.find(".modal-add-date-answered");
  var $memberSelect     = $modalAddSection.find("#member-select");

  var _bindEvents = function() {
    $btnAdd.on('click', function() {
      $modalAdd.open();
    });

    $btnCancelAdd.on('click', function() {
      $modalAdd.close();
    });

    $btnConfirmAdd.on('click', function() {
      alert('add');
    });
  };

  var init = function() {
    _bindEvents();
  };

  return {
    init: init
  };
})();
