module DailyClosings
  class GenerateDailyClosing
    def initialize(closing_date:)
      @daily_closing  = DailyClosing.new(closing_date: @closing_date)
      @closing_date   = closing_date
      @data           = Reports::GenerateRepayments.new(branch_id: nil, so: nil, center_id: nil, loan_product_id: nil, as_of: @closing_date).execute!
    end

    def execute!
      build_daily_closing_repayments
    end

    private

    def build_daily_closing_repayments
      puts "Start building daily closing repayments"
    end

    def build_daily_closing_branch_repayments
      @data[:loan_products].each do |data_loan_product|

        data_loan_product[:branches].each do |data_branch|
          daily_closing_branch_repayment = DailyClosingBranchRepayment.new(
                                              loan_product_code: data_loan_product[:loan_product],
                                              name: data_branch[:name]
                                            )

          @daily_closing.daily_closing_branch_repayments << daily_closing_branch_repayment
        end
      end
    end
  end
end
