module Vouchers
	class VoucherApproval
		def initialize(voucher:, approved_by:)
			@voucher = voucher
			@approved_by = approved_by
		end

		def execute!
			# Approve connected loan application
	    loan = Loan.where(voucher_reference_number: @voucher.reference_number, status: 'for_approval').first

	    if !loan.nil?
	      loan.update!(status: 'active')
	    end

	    voucher.update!(status: 'approved', approved_by: @approved_by)
		end
	end
end
