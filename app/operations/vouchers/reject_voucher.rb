module Vouchers
	class RejectVoucher
		def initialize(voucher:, current_user:)
			@voucher = voucher
			@current_user = current_user.full_name
		end

		def execute!
			if @voucher.status != "pending"
        raise "Cannot reject non-pending accounting entry"
      end

      @voucher.update!(status: "rejected", reference_number: Vouchers::VoucherNumber.new(book: @voucher.book, branch: @voucher.branch).execute!, approved_by: @current_user)
		end
	end
end