module Vouchers
	class VoucherNumber
		def initialize(book:, branch:)
			@book = book
			@branch = branch
		end

		def execute!
			voucher_counter = Setting.where(key: "VOUCHER_COUNTER_#{@book}_#{@branch.code.upcase}").first

      # Default
      voucher_number = 1

      if voucher_counter.nil?
        Setting.create!(key: "VOUCHER_COUNTER_#{@book}_#{@branch.code.upcase}", val: "1")
        voucher_number = 1
      else
        voucher_number = voucher_counter.val.to_i
        new_voucher_counter = voucher_number + 1

        #update the counter
        Setting.where(key: "VOUCHER_COUNTER_#{@book}_#{@branch.code.upcase}").first.update!(val: new_voucher_counter.to_s)

        voucher_number = new_voucher_counter
      end

      voucher_number = voucher_number.to_s.rjust(10, "0")

      voucher_number
		end
	end
end