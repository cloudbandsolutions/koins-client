module InsuranceTransactions
  class ValidateInsuredLoansImportCsvFile
    attr_accessor :insured_loan, :errors

    def initialize(insured_loan:)
      @insured_loan = insured_loan
      @errors = []
    end

    def execute!
      check_if_parameters_present!
      @errors
    end

    private

    def check_if_parameters_present!
      if @insured_loan['identification_number'].nil?
        @errors << "Member ID can't be blank."
      end        
    end
  end
end
