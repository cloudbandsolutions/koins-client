module InsuranceTransactions
  class LoadInsuranceAccountTransactionsFromCsvFile
    attr_accessor :file

    def initialize(file:)
      @file = file
    end

    def execute!
      load_csv_file!
    end

    private

    def load_csv_file!
      insurance_account_ids = []

      CSV.foreach(@file, headers: true) do |row|
        uuid = row['uuid']
        insurance_account_transaction_record = InsuranceAccountTransaction.where(uuid: uuid).first

        if insurance_account_transaction_record.nil?
          insurance_account_transaction = InsuranceAccountTransaction.new
            insurance_account_transaction.transacted_at = row['transacted_at']
            insurance_account_transaction.particular = row['particular']
            insurance_account_transaction.status = row['status']
            insurance_account_transaction.transacted_by = row['transacted_by']
            insurance_account_transaction.approved_by = row['approved_by']
            insurance_account_transaction.voucher_reference_number = row['voucher_reference_number']
            insurance_account_transaction.transaction_number = row['transaction_number']
            insurance_account_transaction.bank_id = row['bank_id']
            insurance_account_transaction.accounting_code_id = row['accounting_code_id']
            insurance_account_transaction.uuid = row['uuid']
        
          insurance_account_transaction.transaction_date = row['transaction_date']
          insurance_account_transaction.is_adjustment = row['is_adjustment']

          insurance_account_uuid = row['insurance_account_uuid']
          statuses = ["active", "inactive"]
          insurance_account = InsuranceAccount.where("uuid = ? AND status IN (?)", insurance_account_uuid, statuses).first
          # insurance_account = InsuranceAccount.where("uuid = ? AND status = ?", insurance_account_uuid, "active").first

          insurance_account_transaction.insurance_account_id = insurance_account.id
          amount = row['amount']
          transaction_type = row['transaction_type']
          insurance_account_transaction.amount = amount
          insurance_account_transaction.transaction_type = transaction_type
          
          insurance_account_transaction.save!

          # if transaction_type == "withdraw" or transaction_type == 'reversed' or transaction_type == 'fund_transfer_withdraw' or transaction_type == 'reverse_deposit'
          #   updated_balance = insurance_account.balance.try(:to_f) - amount.try(:to_f)
          #   insurance_account.update!(balance: updated_balance)
          # elsif transaction_type == "deposit" or transaction_type == 'fund_transfer_deposit' or transaction_type == 'reverse_withdraw'
          #   updated_balance = insurance_account.balance.try(:to_f) + amount.try(:to_f)
          #   insurance_account.update!(balance: updated_balance)
          # end

          insurance_account_ids << insurance_account_transaction.insurance_account.id
        else
          insurance_account_transaction_record.update!(
            amount: row['amount'],
            transaction_type: row['transaction_type'],
            transacted_at: row['transacted_at'],
            particular: row['particular'],
            status: row['status'],
            transacted_by: row['transacted_by'],
            approved_by: row['approved_by'],
            voucher_reference_number: row['voucher_reference_number'],
            transaction_number: row['transaction_number'],
            uuid: row['uuid'],
            transaction_date: row['transaction_date'],
            is_adjustment: row['is_adjustment']
          )

          insurance_account_ids << insurance_account_transaction_record.insurance_account.id
        end
      end

      insurance_account_ids = insurance_account_ids.uniq

      InsuranceAccount.where(id: insurance_account_ids).each do |acc|
        ::Insurance::RehashAccount.new(insurance_account: acc).execute!
      end
    end
  end
end
