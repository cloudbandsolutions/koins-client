module InsuranceTransactions
  class LoadInsuranceAccountsFromCsvFile
    attr_accessor :file

    def initialize(file:)
      @file = file
    end

    def execute!
      load_csv_file!
    end

    private

    def load_csv_file!
      CSV.foreach(@file.path, headers: true) do |row|
        member_id = row['member_id']
        member = Member.where(identification_number: member_id).first

        if !member.nil?
          insurance_type_code = row['insurance_type']
          insurance_account = member.insurance_accounts.joins(:insurance_type).where("insurance_types.code = ?", insurance_type_code).first
          if insurance_account.present? 
            insurance_account.update!(
              # balance: row['balance'],
              # account_number: row['account_number'],
              status: row['status'],
              uuid: row['uuid']
            )
          else
            new_insurance_account = InsuranceAccount.new
            new_insurance_account.member = member
            new_insurance_account.balance = 0.00
            
            insurance_type = InsuranceType.where(code: row['insurance_type']).first

            new_insurance_account.insurance_type = insurance_type
            new_insurance_account.status = row['status']
            new_insurance_account.account_number = row['account_number']
            new_insurance_account.branch = member.branch
            new_insurance_account.center = member.center
            new_insurance_account.uuid = row['uuid']

            new_insurance_account.save!
          end
        end       
      end
    end
  end
end
