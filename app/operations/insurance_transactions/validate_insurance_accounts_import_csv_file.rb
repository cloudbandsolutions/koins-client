module InsuranceTransactions
  class ValidateInsuranceAccountsImportCsvFile
    attr_accessor :insurance_account, :errors

    def initialize(insurance_account:)
      @insurance_account = insurance_account
      @errors = []
    end

    def execute!
      check_if_parameters_present!
      @errors
    end

    private

    def check_if_parameters_present!
      if @insurance_account['insurance_type'].nil?
        @errors << "Insurance Type can't be blank."
      end      

      if @insurance_account['balance'].nil?
        @errors << "Balance can't be blank."
      end

      if @insurance_account['member_id'].nil?
        @errors << "Member ID can't be blank."
      end

      # if @insurance_account['account_number'].nil?
      #   @errors << "Account Number can't be blank."
      # end

      if @insurance_account['status'].nil?
        @errors << "Status can't be blank."
      end

      if @insurance_account['uuid'].nil?
        @errors << "UUID can't be blank."
      end
    end
  end
end
