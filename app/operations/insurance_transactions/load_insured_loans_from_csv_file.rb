module InsuranceTransactions
  class LoadInsuredLoansFromCsvFile
    attr_accessor :file

    def initialize(file:)
      @file = file
    end

    def execute!
      load_csv_file!
    end

    private

    def load_csv_file!
      CSV.foreach(@file.path, headers: true) do |row|
        pn_number = row['pn_number']
        loan_category = row['loan_category']
        member_id = row['identification_number']
        member = Member.where(identification_number: member_id).first

        loan_insurance_record = LoanInsuranceRecord.where(pn_number: pn_number, loan_category: loan_category, member_id: member.id).first

        date_released = row['date_released']
        maturity_date = row['maturity_date']
        loan_term = row['loan_term']
        loan_amount = row['loan_amount']
        loan_insurance_amount = row['loan_insurance_amount']
        status = row['status']

        if !loan_insurance_record.nil?
          loan_insurance_record.update!(
              date_released: date_released,
              maturity_date: maturity_date,
              loan_term: loan_term,
              loan_amount: loan_amount,
              loan_insurance_amount: loan_insurance_amount,
              status: status
            )
        else
          new_loan_insurance_record = LoanInsuranceRecord.new
          new_loan_insurance_record.member = member
          new_loan_insurance_record.date_released = row['date_released']
          new_loan_insurance_record.maturity_date = row['maturity_date']
          new_loan_insurance_record.loan_term = row['loan_term']
          new_loan_insurance_record.loan_amount = row['loan_amount']
          new_loan_insurance_record.loan_insurance_amount = row['loan_insurance_amount']
          new_loan_insurance_record.pn_number = row['pn_number']
          new_loan_insurance_record.status = row['status']
          new_loan_insurance_record.loan_category = row['loan_category']
          
          new_loan_insurance_record.save!
        end       
      end
    end
  end
end
