module InsuranceTransactions
  class ValidateInsuranceAccountTransactionsImportCsvFile
    attr_accessor :insurance_account_transaction, :errors

    def initialize(insurance_account_transaction:)
      @insurance_account_transaction = insurance_account_transaction
      @errors = []
    end

    def execute!
      check_if_parameters_present!
      @errors
    end

    private

    def check_if_parameters_present!
      if @insurance_account_transaction['insurance_account_uuid'].nil?
        @errors << "Insurance Account UUID can't be blank."
      end 

      if @insurance_account_transaction['uuid'].nil?
        @errors << "UUID can't be blank."
      end           
    end
  end
end
