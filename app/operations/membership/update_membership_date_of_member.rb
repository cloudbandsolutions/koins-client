module Membership
  class UpdateMembershipDateOfMember
    def initialize(member:, previous_mii_member_since:)
      @previous_mii_member_since        = previous_mii_member_since
      @member                           = member
      @insurance_membership_type        = MembershipType.where(activated_for_insurance: true)
    end

    def execute!
      @insurance_membership_type.each do |insurance_membership_type|
        insurance_membership_payment = MembershipPayment.where(membership_type_id: insurance_membership_type.id, member_id: @member.id, status: "paid").first
          if !insurance_membership_payment.nil?
            insurance_membership_payment.update(
              paid_at: @previous_mii_member_since,
            )
        end
      end
    end
  end
end
