module Membership
  class UpdateMemberMaintainingBalance
    def initialize(member:, savings_account:)
      @member = member
      @savings_account = savings_account
      @active_loans = @member.loans.active.joins(:loan_product => :savings_type).where("savings_types.id = ?", @savings_account.savings_type.id)
    end

    def execute!
      maintaining_balance = 0.00
      @active_loans.each do |loan|
        maintaining_balance += loan.maintaining_balance_val
      end

      @savings_account.update!(maintaining_balance: maintaining_balance)
    end
  end
end
