module Membership
  class LoadDependentsFromCsvFile
    attr_accessor :file

    def initialize(file:)
      @file = file
    end

    def execute!
      load_csv_file!
    end

    private

    def load_csv_file!
      CSV.foreach(@file.path, headers: true) do |row|
        reference_number = row['reference_number']
        uuid = row['uuid']

        if !reference_number.nil?
          dependent_record = LegalDependent.where(reference_number: reference_number).first

          if dependent_record.nil?
            legal_dependent = LegalDependent.new
            legal_dependent.reference_number = row['reference_number']
            legal_dependent.first_name = row['first_name']
            legal_dependent.middle_name = row['middle_name']
            legal_dependent.last_name = row['last_name']
            legal_dependent.date_of_birth = row['date_of_birth']
            legal_dependent.educational_attainment = row['educational_attainment']
            legal_dependent.course = row['course']
            legal_dependent.is_deceased = row['is_deceased']
            legal_dependent.is_tpd = row['is_tpd']
            legal_dependent.relationship = row['relationship']

            member_identification_number = row['member_identification_number']
            member = Member.where(identification_number: member_identification_number).first
            member_id = member.id
            legal_dependent.member_id = member_id

            legal_dependent.save!
          else
            dependent_record.update!(first_name: row['first_name'], middle_name: row['middle_name'], last_name: row['last_name'], date_of_birth: row['date_of_birth'], educational_attainment: row['educational_attainment'], course: row['course'], is_deceased: row['is_deceased'], is_tpd: row['is_tpd'], relationship: row['relationship'])
          end
        elsif !uuid.nil?
          dependent_record = LegalDependent.where(uuid: uuid).first

          if dependent_record.nil?
            legal_dependent = LegalDependent.new
            legal_dependent.reference_number = row['reference_number']
            legal_dependent.first_name = row['first_name']
            legal_dependent.middle_name = row['middle_name']
            legal_dependent.last_name = row['last_name']
            legal_dependent.date_of_birth = row['date_of_birth']
            legal_dependent.educational_attainment = row['educational_attainment']
            legal_dependent.course = row['course']
            legal_dependent.is_deceased = row['is_deceased']
            legal_dependent.is_tpd = row['is_tpd']
            legal_dependent.relationship = row['relationship']
            legal_dependent.uuid = uuid

            member_identification_number = row['member_identification_number']
            member = Member.where(identification_number: member_identification_number).first
            member_id = member.id
            legal_dependent.member_id = member_id

            legal_dependent.save!
          else
            dependent_record.update!(first_name: row['first_name'], middle_name: row['middle_name'], last_name: row['last_name'], date_of_birth: row['date_of_birth'], educational_attainment: row['educational_attainment'], course: row['course'], is_deceased: row['is_deceased'], is_tpd: row['is_tpd'], relationship: row['relationship'], uuid: uuid)
          end
        end  
      end
    end
  end
end
