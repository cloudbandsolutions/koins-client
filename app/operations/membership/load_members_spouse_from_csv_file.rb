module Membership
  class LoadMembersSpouseFromCsvFile
    attr_accessor :file

    def initialize(file:)
      @file = file
    end

    def execute!
      load_csv_file!
    end

    private

    def load_csv_file!
      CSV.foreach(@file.path, headers: true) do |row|
        member_identification_number = row['member_identification_number']   
        member = Member.where(identification_number: member_identification_number).first

        if !member.nil?
          spouse_first_name = row['first_name']
          spouse_last_name = row['last_name']
          spouse_middle_name = row['middle_name']
          spouse_dob = row['date_of_birth']
          relationship = row['relationship']
          dependent_record = LegalDependent.where(first_name: spouse_first_name, last_name: spouse_last_name, date_of_birth: spouse_dob).first_name

          if dependent_record.nil?
            legal_dependent = LegalDependent.new
            legal_dependent.first_name = spouse_first_name
            legal_dependent.last_name = spouse_last_name
            legal_dependent.middle_name = spouse_middle_name
            legal_dependent.date_of_birth = spouse_dob
            legal_dependent.educational_attainment = row['educational_attainment']
            legal_dependent.course = row['course']
            legal_dependent.is_deceased = row['is_deceased']
            legal_dependent.is_tpd = row['is_tpd']
            legal_dependent.relationship = relationship
            legal_dependent.member_id = member.id
            
            legal_dependent.save!
          else
            dependent_record.update!(first_name: row['first_name'], middle_name: row['middle_name'], last_name: row['last_name'], date_of_birth: row['date_of_birth'], educational_attainment: row['educational_attainment'], course: row['course'], is_deceased: row['is_deceased'], is_tpd: row['is_tpd'], relationship: row['relationship'])
          end
        end       
      end
    end
  end
end
