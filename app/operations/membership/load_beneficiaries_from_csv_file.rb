module Membership
  class LoadBeneficiariesFromCsvFile
    attr_accessor :file

    def initialize(file:)
      @file = file
    end

    def execute!
      load_csv_file!
    end

    private

    def load_csv_file!
      CSV.foreach(@file.path, headers: true) do |row|
        reference_number = row['reference_number']
        uuid = row['uuid']

        if !reference_number.nil? 
          beneficiary_record = Beneficiary.where(reference_number: reference_number).first

          if beneficiary_record.nil?
            beneficiary = Beneficiary.new
            beneficiary.reference_number = row['reference_number']
            beneficiary.first_name = row['first_name']
            beneficiary.middle_name = row['middle_name']
            beneficiary.last_name = row['last_name']
            beneficiary.is_primary = row['is_primary']
            beneficiary.date_of_birth = row['date_of_birth']
            beneficiary.relationship = row['relationship']

            member_identification_number = row['member_identification_number']
            member = Member.where(identification_number: member_identification_number).first
            member_id = member.id
            beneficiary.member_id = member_id
          
            beneficiary.save!
          else
            beneficiary_record.update!(first_name: row['first_name'], middle_name: row['middle_name'], last_name: row['last_name'], is_primary: row['is_primary'], date_of_birth: row['date_of_birth'], relationship: row['relationship'])
          end
        elsif !uuid.nil? 
          beneficiary_record = Beneficiary.where(uuid: uuid).first

          if beneficiary_record.nil?
            beneficiary = Beneficiary.new
            beneficiary.reference_number = row['reference_number']
            beneficiary.first_name = row['first_name']
            beneficiary.middle_name = row['middle_name']
            beneficiary.last_name = row['last_name']
            beneficiary.is_primary = row['is_primary']
            beneficiary.date_of_birth = row['date_of_birth']
            beneficiary.relationship = row['relationship']
            beneficiary.uuid = uuid

            member_identification_number = row['member_identification_number']
            member = Member.where(identification_number: member_identification_number).first
            member_id = member.id
            beneficiary.member_id = member_id
          
            beneficiary.save!
          else
            beneficiary_record.update!(first_name: row['first_name'], middle_name: row['middle_name'], last_name: row['last_name'], is_primary: row['is_primary'], date_of_birth: row['date_of_birth'], relationship: row['relationship'], uuid: uuid)
          end
        end
      end
    end
  end
end
