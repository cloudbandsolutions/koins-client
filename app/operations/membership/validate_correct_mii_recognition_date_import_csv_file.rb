module Membership
  class ValidateCorrectMiiRecognitionDateImportCsvFile
    attr_accessor :member_date, :errors

    def initialize(member_date:)
      @member_date = member_date
      @errors = []
    end

    def execute!
      check_parameters!
      @errors
    end

    private

    def check_parameters!
      if @member_date['correct_mii_recognition_date'].nil?
        @errors << "MII Date cant be blank."
      end

      if @member_date['member_id'].nil?
        @errors << "Member ID cant be blank."
      end
    end
  end
end
