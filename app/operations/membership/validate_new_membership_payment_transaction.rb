module Membership
  class ValidateNewMembershipPaymentTransaction
    def initialize(branch_id:, date_of_payment:)
      @branch           = Branch.where(id: branch_id).first
      @date_of_payment  = date_of_payment
      @errors           = []
    end

    def execute!
      validate_required_parameters!
      #validate_has_pending_transactions!

      @errors
    end

    private

    def validate_required_parameters!
      if !@branch.present?
        @errors << "Branch required"
      end

      if !@date_of_payment.present?
        @errors << "Date of payment required"
      end
    end

    def validate_has_pending_transactions!
      if @branch.present?
        if PaymentCollection.membership.where(branch_id: @branch.id, status: "pending").count > 0
          @errors << "This branch still has pending deposit transactions"
        end
      end
    end
  end
end
