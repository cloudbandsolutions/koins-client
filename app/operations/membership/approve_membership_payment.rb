module Membership
  class ApproveMembershipPayment
    def initialize(membership_payment:, current_user:)
      @membership_payment = membership_payment
      @current_user       = current_user
      @c_working_date     = ApplicationHelper.current_working_date
    end 
    
    def execute! 
      membership_type = @membership_payment.membership_type
      member = @membership_payment.member
      membership_payment_dr_accounting_entry = membership_type.dr_accounting_entry
      membership_payment_cr_accounting_entry = membership_type.cr_accounting_entry
      voucher = Voucher.new(book: "CRB", date_prepared: Date.today, branch: member.branch, particular: "#{membership_type} Membership payment for #{@membership_payment.member.full_name}")

      if @membership_payment.amount_paid > 0
        dr_journal_entry_for_payment = JournalEntry.new(post_type: "DR", amount: @membership_payment.amount_paid, accounting_code: membership_payment_dr_accounting_entry)
        cr_journal_entry_for_payment = JournalEntry.new(post_type: "CR", amount: @membership_payment.amount_paid, accounting_code: membership_payment_cr_accounting_entry)

        voucher.journal_entries << dr_journal_entry_for_payment
        voucher.journal_entries << cr_journal_entry_for_payment
      end

      account_payments = []
      @membership_payment.membership_account_payments.each do |membership_account_payment|
        if membership_account_payment.amount > 0
          deposit_type  = membership_account_payment.deposit_type
          account_type  = membership_account_payment.account_type
          amount        = membership_account_payment.amount

          if deposit_type == "savings"
            savings_type = SavingsType.where(code: account_type).first
            savings_account = SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id).first

            dr_journal_entry_for_deposit = JournalEntry.new(post_type: "DR", amount: amount, accounting_code: membership_payment_dr_accounting_entry)
            voucher.journal_entries << dr_journal_entry_for_deposit

            cr_journal_entry_for_deposit = JournalEntry.new(post_type: "CR", amount: amount, accounting_code: savings_type.deposit_accounting_code)
            voucher.journal_entries << cr_journal_entry_for_deposit

            savings_account_transaction = SavingsAccountTransaction.new(savings_type: savings_type, savings_account: savings_account, amount: amount, transaction_type: "deposit")
            account_payments << savings_account_transaction
          elsif deposit_type == "insurance"
            insurance_type = InsuranceType.where(code: account_type).first
            insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).first

            dr_journal_entry_for_deposit = JournalEntry.new(post_type: "DR", amount: amount, accounting_code: membership_payment_dr_accounting_entry)
            voucher.journal_entries << dr_journal_entry_for_deposit

            cr_journal_entry_for_deposit = JournalEntry.new(post_type: "CR", amount: amount, accounting_code: insurance_type.deposit_accounting_code)
            voucher.journal_entries << cr_journal_entry_for_deposit

            insurance_account_transaction = InsuranceAccountTransaction.new(insurance_account: insurance_account, amount: amount, transaction_type: "deposit")
            account_payments << insurance_account_transaction
          elsif deposit_type == "equity"
            equity_type = EquityType.where(code: account_type).first
            equity_account = EquityAccount.where(member_id: member.id, equity_type_id: equity_type.id).first

            dr_journal_entry_for_deposit = JournalEntry.new(post_type: "DR", amount: amount, accounting_code: membership_payment_dr_accounting_entry)
            voucher.journal_entries << dr_journal_entry_for_deposit

            cr_journal_entry_for_deposit = JournalEntry.new(post_type: "CR", amount: amount, accounting_code: equity_type.deposit_accounting_code)
            voucher.journal_entries << cr_journal_entry_for_deposit

            equity_account_transaction = EquityAccountTransaction.new(equity_account: equity_account, amount: amount, transaction_type: "deposit")
            account_payments << equity_account_transaction
          end
        end
      end

      valid_accounts = true
      account_payments.each do |account_payment|
        if !account_payment.valid?
          valid_accounts = false
        end
      end

      # Validate account payments
      if voucher.valid? and valid_accounts
        voucher.save!
        ::Accounting::ApproveVoucher.new(voucher: voucher, user: @current_user).execute!
        reference_number = voucher.reference_number
        account_payments.each do |account_payment|
          account_payment.voucher_reference_number = reference_number
          account_payment.save!
          account_payment.approve!(@current_user.full_name)
        end
      else
        raise "invalid: #{voucher.errors.messages}"
      end

      @membership_payment.update!(status: "paid")

      # Update previous_mii_since
      if member.previous_mii_since.nil?
        member.update!(previous_mii_since: @c_working_date)
      end
    end
  end
end
