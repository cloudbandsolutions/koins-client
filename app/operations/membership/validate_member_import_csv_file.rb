module Membership
  class ValidateMemberImportCsvFile
    attr_accessor :member, :errors

    def initialize(member:)
      @member = member
      @members = Member.all
      @errors = []
    end

    def execute!
      check_if_center_present!
      check_if_branch_present!
      @errors
    end

    private

    def check_if_branch_present!
      if @member['branch'].nil?
        @errors << "Branch can't be blank."
      end
    end

    def check_if_center_present!
      if @member['center'].nil?
        @errors << "Center can't be blank."
      end
    end 
  end
end
