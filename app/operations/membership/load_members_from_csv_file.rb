module Membership
  class LoadMembersFromCsvFile
    attr_accessor :file

    def initialize(file:)
      @file = file
    end

    def execute!
      load_csv_file!
    end

    private

    def load_csv_file!
      CSV.foreach(@file.path, headers: true) do |row|
        identification_number = row['identification_number']
        member_record = Member.where(identification_number: identification_number).first

        # comment out pagupdated na ung mga sato        
        # meta = row['meta_id']

        # if meta
        #   meta_data = JSON.parse(meta)
        # else
        #   meta_data = ""
        # end

        if member_record.nil?
          member = Member.new
          insurance_status = row['insurance_status']

          member.first_name = row['first_name']
          member.middle_name = row['middle_name']
          member.last_name = row['last_name']
          member.uuid = row['uuid']

          recognition_date = row['recognition_date']
          status = row['status']

          if recognition_date.present? && insurance_status.present?
            member.previous_mii_member_since = recognition_date
            if insurance_status == "inforce" || insurance_status == "lapsed" 
              if status == "cleared"
                member.status = "cleared"
                member.insurance_status = "cleared"
              elsif status == "resigned"
                member.status = "resigned"
                member.insurance_status = "resigned"
              else  
                member.status = "active"
                member.insurance_status = insurance_status
              end
            elsif insurance_status == "dormant"
              if status == "archived"
                member.status = "archived"
                member.insurance_status = insurance_status
              elsif status == "resigned"
                member.status = "resigned"
                member.insurance_status = "resigned"
              else
                member.status = status
                member.insurance_status = insurance_status
              end
            elsif insurance_status == "resigned"
              member.status = "resigned"
              member.insurance_status = insurance_status
            elsif insurance_status == "cleared"
              member.status = "cleared"
              member.insurance_status = insurance_status
            elsif insurance_status == "pending"
              member.status = "pending"
              member.insurance_status = insurance_status  
            elsif row['member_type'] == "GK"
              member.status = "resigned"
              member.insurance_status = insurance_status
            end
          elsif recognition_date.nil?
            member.previous_mii_member_since = recognition_date
            member.insurance_status = insurance_status
            
            if status == "archived"
              member.status = "archived"
            elsif status == "resigned"
              member.status = "resigned"
            else
              member.status = "pending"
            end
          end

          birthday = row['date_of_birth']
          if birthday.nil?
            member.date_of_birth = Date.today.to_s
          else
            member.date_of_birth = row['date_of_birth']
          end

          sss_num = row['sss_number']
          if sss_num.nil?
            member.sss_number = ''
          else
            member.sss_number = sss_num
          end

          phil_health_num = row['phil_health_number']
          if phil_health_num.nil?
            member.phil_health_number = ''
          else
            member.phil_health_number = phil_health_num
          end

          pag_ibig_num = row['pag_ibig_number']
          if pag_ibig_num.nil?
            member.pag_ibig_number = ''
          else
            member.pag_ibig_number = pag_ibig_num
          end

          tin_num = row['tin_number']
          if tin_num.nil?
            member.tin_number = ''
          else
            member.tin_number = tin_num
          end
  
          member.place_of_birth = row['place_of_birth']
          member.num_children = row['number_of_children']
          member.spouse_first_name = row['spouse_first_name']
          member.spouse_last_name = row['spouse_last_name']
          member.spouse_middle_name = row['spouse_middle_name']
          member.spouse_date_of_birth = row['spouse_date_of_birth']
          member.mobile_number = row['cellphone_number']
          member.address_street = row['address_street']
          member.address_barangay = row['address_barangay']
          member.address_city = row['address_city']
          member.gender = row['gender']
          member.civil_status = row['civil_status']
          member.date_resigned = row['date_resigned']
          member.resignation_type = row['resignation_type']
          member.resignation_code = row['resignation_code']
          member.resignation_reason = row['resignation_reason']
          member.insurance_date_resigned = row['insurance_date_resigned']
          member.is_reinstate = row['is_reinstate']
          member.old_previous_mii_member_since = row['old_previous_mii_member_since']
          member.is_balik_kasapi = row['is_balik_kasapi']
          # comment out pagupdated na ung mga sato
          # member.meta = meta_data

          member_type = row['member_type']
          if member_type.nil?
            member.member_type = "Regular"
          else
            member.member_type = member_type
          end

          branch_name = row['branch']
          branch = Branch.where(name: branch_name).first
          bank = Bank.first  
          cluster = Cluster.first
          if branch.nil?
            branch =  Branch.new
            branch.name = row['branch']
            name_branch = row['branch']
            branch_code_abbreviation = name_branch[0..1] + name_branch[-1]
            branch.code = branch_code_abbreviation.upcase
            branch.abbreviation = branch_code_abbreviation.upcase
            branch.address = row['branch']
            branch.cluster = cluster
            branch.bank = bank
            branch.save!
            member.branch = branch
          else
            member.branch = branch
          end 

          center_name = row['center'].upcase
          center = Center.where(name: center_name, branch_id: branch.id).first
          if center.nil?
            center = Center.new
            center.name = row['center']
            name_center = row['center']
            center_abbreviation = name_center
            center.abbreviation = center_abbreviation.upcase
            center.address = row['center']
            center.meeting_day = "Monday"
            center.branch = branch
            center.save!
            member.center = center
          else
            member.center = center
          end

          member.identification_number = row['identification_number']
          
          member.save!
          member.branch.update(member_counter: member.branch.member_counter + 1)
          Members::GenerateMissingAccounts.new(member: member).execute!
        else
          sss_num = row['sss_number']
          if sss_num.nil?
            sss_number = ''
          else
            sss_number = sss_num
          end

          phil_health_num = row['phil_health_number']
          if phil_health_num.nil?
            phil_health_number = ''
          else
            phil_health_number = phil_health_num
          end

          pag_ibig_num = row['pag_ibig_number']
          if pag_ibig_num.nil?
            pag_ibig_number = ''
          else
            pag_ibig_number = pag_ibig_num
          end

          tin_num = row['tin_number']
          if tin_num.nil?
            tin_number = ''
          else
            tin_number = tin_num
          end

          insurance_status = row['insurance_status']

          if row['recognition_date'].present? && insurance_status.present?  
            if insurance_status == "inforce" || insurance_status == "lapsed"
              if row['status'] == "cleared"
                status = "cleared"
                insurance_status = "cleared"
              else
                status = "active"
              end
            elsif insurance_status == "dormant"
              if row['status'] == "archived"
                status = "archived"
              elsif row['status'] == "resigned"
                status = "resigned"
                insurance_status = "resigned"
              else
                status = row['status']
                insurance_status = "dormant"
              end
            elsif insurance_status == "pending"
              status = "pending"
            elsif insurance_status == "resigned"
              status = "resigned"
            elsif row['member_type'] == "GK"
              status = "resigned"
            elsif row['status'] == "archived"
              status = "archived"
            elsif row['status'] == "cleared"
              status = "cleared"
              insurance_status = "cleared"
            else
              status = row['status']
              insurance_status = insurance_status
            end
          else
            if row['status'] == "archived"
              status = "archived"
            elsif row['status'] == "resigned"
              status = "resigned"
            else
              status = "pending"
            end
          end

          branch_name = row['branch']
          branch = Branch.where(name: branch_name).first

          center_name = row['center'].upcase
          center = Center.where(name: center_name, branch_id: branch.id).first
          if center.nil?
            center = Center.new
            center.name = row['center']
            name_center = row['center']
            center_abbreviation = name_center
            center.abbreviation = center_abbreviation.upcase
            center.address = row['center']
            center.meeting_day = "Monday"
            center.branch = branch
            center.save!
          end
            
          member_record.update!(
            uuid: row['uuid'],
            branch_id: branch.id,
            center_id: center.id,
            member_type: row['member_type'],
            status: status,
            insurance_status: insurance_status,
            first_name: row['first_name'],
            middle_name: row['middle_name'],
            last_name: row['last_name'],
            previous_mii_member_since: row['recognition_date'],
            date_of_birth: row['date_of_birth'],
            address_street: row['address_street'],
            address_barangay: row['address_barangay'],
            address_city: row['address_city'],
            gender: row['gender'],
            civil_status: row['civil_status'],
            place_of_birth: row['place_of_birth'],
            num_children: row['number_of_children'],
            spouse_first_name: row['spouse_first_name'],
            spouse_last_name: row['spouse_last_name'],
            spouse_middle_name: row['spouse_middle_name'],
            spouse_date_of_birth: row['spouse_date_of_birth'],
            sss_number: sss_number,
            tin_number: tin_number,
            pag_ibig_number: pag_ibig_number,
            phil_health_number: phil_health_number,
            mobile_number: row['cellphone_number'],
            date_resigned: row['date_resigned'],
            resignation_type: row['resignation_type'],
            resignation_code: row['resignation_code'],
            resignation_reason: row['resignation_reason'],
            insurance_date_resigned: row['insurance_date_resigned'],
            is_reinstate: row['is_reinstate'],
            old_previous_mii_member_since: row['old_previous_mii_member_since'],
            is_balik_kasapi: row["is_balik_kasapi"],
            # comment out pagupdated na ung mga sato
            # meta: meta_data
            )
        end       
      end
    end
  end
end
