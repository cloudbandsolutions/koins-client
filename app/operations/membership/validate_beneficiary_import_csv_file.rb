module Membership
  class ValidateBeneficiaryImportCsvFile
    attr_accessor :beneficiary, :errors

    def initialize(beneficiary:)
      @beneficiary = beneficiary
      @errors = []
    end

    def execute!
      validate_if_member_present!
      validate_if_identification_number_present!
      #validate_if_reference_number_present!
      @errors
    end

    private

    def validate_if_member_present!
      member = Member.where(identification_number: @beneficiary['member_identification_number']).first
      if member.nil?
        @errors << "No member with identification_number #{@beneficiary['member_identification_number']}"
      end
    end

    def validate_if_identification_number_present!
      if @beneficiary['member_identification_number'].nil?
        @errors << "Member Identification Number can't be blank."
      end
    end

    def validate_if_reference_number_present!
      if @beneficiary['reference_number'].nil?
        @errors << "Reference Number can't be blank."
      end
    end
  end
end
