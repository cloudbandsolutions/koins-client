module Membership
  class ValidateDependentImportCsvFile
    attr_accessor :dependent, :errors

    def initialize(dependent:)
      @dependent = dependent
      @errors = []
    end

    def execute!
      validate_if_member_present!
      validate_if_identification_number_present!
      #validate_if_reference_number_present!
      @errors
    end

    private

    def validate_if_member_present!
      member = Member.where(identification_number: @dependent['member_identification_number']).first
      if member.nil?
        @errors << "No member with identification_number #{@dependent['member_identification_number']}"
      end
    end

    def validate_if_identification_number_present!
      if @dependent['member_identification_number'].nil?
        @errors << "Member Identification Number can't be blank."
      end
    end

    def validate_if_reference_number_present!
      if @dependent['reference_number'].nil?
        @errors << "Reference Number can't be blank."
      end
    end
  end
end
