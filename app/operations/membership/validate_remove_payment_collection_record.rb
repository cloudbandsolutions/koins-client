module Membership
  class ValidateRemovePaymentCollectionRecord
    def initialize(payment_collection:, payment_collection_record:)
      @payment_collection_record  = payment_collection_record
      @payment_collection         = payment_collection
      @errors                     = []
    end

    def execute!
      check_parameters!
      check_state!
      @errors
    end

    private

    def check_state!
      if !@payment_collection.try(:membership?)
        @errors << "Not membership payment"
      end

      if !@payment_collection.try(:pending?)
        @errors << "Payment not pending"
      end
    end

    def check_parameters!
      if !@payment_collection_record.present?
        @errors << "Payment collection record not found"
      end

      if !@payment_collection.present?
        @errors << "Payment collection not found"
      end
    end
  end
end
