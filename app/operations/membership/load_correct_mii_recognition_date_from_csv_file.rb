module Membership
  class LoadCorrectMiiRecognitionDateFromCsvFile
    attr_accessor :file

    def initialize(file:)
      @file = file
    end

    def execute!
      load_csv_file!
    end

    private

    def load_csv_file!
      CSV.foreach(@file.path, headers: true) do |row|
        identification_number = row['member_id']
        member = Member.where(identification_number: identification_number).first

        if !member.nil?
          previous_mii_member_since = row['correct_mii_recognition_date']
          member.update!(
            previous_mii_member_since: previous_mii_member_since,
            )

          Membership::UpdateMembershipDateOfMember.new(member: member, previous_mii_member_since: previous_mii_member_since ).execute!
        end       
      end
    end
  end
end
