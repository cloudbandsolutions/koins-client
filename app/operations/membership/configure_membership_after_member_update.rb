module Membership
  class ConfigureMembershipAfterMemberUpdate
    def initialize(member:)
      @member                           = member
      @insurance_membership_types       = MembershipType.where(activated_for_insurance: true)
      @insurance_membership_payments    = MembershipPayment.where(membership_type_id: @insurance_membership_types.pluck(:id), member_id: @member.id, status: "paid")
      @microfinance_membership_types    = MembershipType.where(activated_for_loans: true)
      @microfinance_membership_payments = MembershipPayment.where(membership_type_id: @microfinance_membership_types.pluck(:id), member_id: @member.id, status: "paid")
    end

    def execute!
      if @member.previous_mii_member_since.nil?
        @insurance_membership_payments.destroy_all
      end

      if @member.previous_mfi_member_since.nil?
        @microfinance_membership_payments.destroy_all
      end

      if !@member.previous_mii_member_since.nil?
        if @insurance_membership_payments.count == 0
          @insurance_membership_types.each do |insurance_membership_type|
            MembershipPayment.create!(
              membership_type: insurance_membership_type, 
              member: @member, 
              status: "paid", 
              amount_paid: insurance_membership_type.fee, 
              paid_at: @member.previous_mii_member_since
            )
          end
        end
      end
    end
  end
end
