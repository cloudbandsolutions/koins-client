module Membership
  class ValidateMembersSpouseImportCsvFile
    attr_accessor :spouse, :errors

    def initialize(spouse:)
      @spouse = spouse
      @errors = []
    end

    def execute!
      check_parameters!
      @errors
    end

    private

    def check_parameters!
      if @spouse['member_identification_number'].nil?
        @errors << "Member identification number can't be blank."
      end

      if @spouse['first_name'].nil?
        @errors << "Spouse first name can't be blank."
      end

       if @spouse['last_name'].nil?
        @errors << "Spouse last name can't be blank."
      end

       if @spouse['date_of_birth'].nil?
        @errors << "Spouse date of birth can't be blank."
      end
    end
  end
end
