module Membership
  class RemovePaymentCollectionRecord
    def initialize(payment_collection:, payment_collection_record:)
      @payment_collection_record  = payment_collection_record
      @payment_collection         = payment_collection
    end

    def execute!
      @payment_collection_record.destroy!
      @payment_collection.update(updated_at: Time.now)
    end
  end
end
