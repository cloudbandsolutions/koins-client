module RequestRecords
  class CreateResignationRequestRecord
    def initialize(member:, user:, date_resigned:, resignation_type:, resignation_code:, reason:, particular:)
      @member           = member
      @equity_accounts  = EquityAccount.where(member_id: @member.id)
      @equity_balance   = @equity_accounts.sum(:balance)
      @user             = user
      @date_resigned    = date_resigned
      @resignation_type = resignation_type
      @resignation_code = resignation_code
      @reason           = reason
      @particular       = particular
      @info             = {}

      @debit_accounting_code  = AccountingCode.find(Settings.equity_debit_accounting_code_id)
      @credit_accounting_code = AccountingCode.find(Settings.equity_credit_accounting_code_id)

      @c_working_date   = ApplicationHelper.current_working_date
      @request_record   = RequestRecord.new(
                            prepared_by: @user.full_name,
                            request_type: "resignation",
                            date_requested: @c_working_date
                          )
    end

    def execute!
      @info[:member_id]           = @member.id
      @info[:member_last_name]    = @member.last_name
      @info[:member_first_name]   = @member.first_name
      @info[:member_middle_name]  = @member.middle_name
      @info[:center]              = @member.center.to_s
      @info[:branch]              = @member.branch.to_s
      @info[:resignation_type]    = @resignation_type
      @info[:resignation_code]    = @resignation_code
      @info[:resignation_reason]  = @reason

      # Accounting entry
      accounting_entry  = {}
      accounting_entry[:book]             = 'JVB'
      accounting_entry[:branch_id]        = @member.branch.id
      accounting_entry[:particular]       = @particular
      accounting_entry[:date_prepared]    = @c_working_date
      accounting_entry[:journal_entries]  = []

      accounting_entry[:journal_entries] << {
        amount: @equity_balance,
        post_type: "DR",
        accounting_code: @debit_accounting_code
      }

      accounting_entry[:journal_entries] << {
        amount: @equity_balance,
        post_type: "CR",
        accounting_code: @credit_accounting_code
      }

      @info[:accounting_entry]  = accounting_entry

      @request_record.info  = @info
      @request_record.save!

      @request_record
    end
  end
end
