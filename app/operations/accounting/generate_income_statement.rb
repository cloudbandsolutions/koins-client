module Accounting
  class GenerateIncomeStatement
    def initialize(as_of:, prepared_by:)
      @as_of              = as_of
     #raise @as_of.inspect
      @start_date         = Date.new(@as_of.to_date.year, 01, 01)
      @end_date           = Date.new(@as_of.to_date.year, 12, 31)
      #@end_date           = as_of
      @prepared_by        = prepared_by
      @expenses_group_id  = Settings.expenses_group_id
     # raise @expenses_group_id.inspect
      @expense_group      = MajorGroup.find(@expenses_group_id)
      @revenue_group_id   = Settings.revenue_group_id
      @revenue_group      = MajorGroup.find(@revenue_group_id)
      @date_prepared      = ApplicationHelper.current_working_date.strftime("%b %d, %Y")

      @revenue_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @revenue_group.id)
      @expense_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @expense_group.id)

      @data                       = {}
      @data[:date_prepared]       = @date_prepared
      @data[:revenue_entries]     = {}
      @data[:expense_entries]     = {}
    end

    def execute!
      build_revenue!
      build_expense!

      @data[:total_net_income]  = @data[:revenue_entries][:total_net] - @data[:expense_entries][:total_net]
      #raise @data[:total_net_income].to_f.inspect
      @data
    end

    private

    def build_expense!
      total_debit   = JournalEntry.joins(:voucher).where(
                        "vouchers.status = 'approved' 
                        AND post_type = 'DR' 
                        AND accounting_code_id IN (?) 
                        AND vouchers.is_year_end_closing IS NULL 
                        AND vouchers.date_prepared between ? 
                        and ?", 
                        @expense_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                      ).sum(:amount)

                    #raise total_debit.to_f.inspect

#      raise @expense_accounting_codes.pluck("accounting_codes.id").uniq.inspect

      total_credit  = JournalEntry.joins(:voucher).where(
                        "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                        @expense_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                      ).sum(:amount)

      #total_net     = total_debit - total_credit
      total_net  = total_debit - total_credit
      #raise total_net.to_f.inspect

      @data[:expense_entries][:total_debit]   = total_debit
      @data[:expense_entries][:total_credit]  = total_credit
      @data[:expense_entries][:total_net]     = total_net
      #raise @data[:expense_entries][:total_net].to_f.inspect
      ### BUILD MAJOR ACCOUNTS
      @data[:expense_entries][:major_account_entries]  = []

      @expense_group.major_accounts.each do |major_account|
        major_account_accounting_codes  = @expense_accounting_codes.where("major_accounts.id = ?", major_account.id)
        major_account_total_debit       = JournalEntry.joins(:voucher).where(
                                            "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",

                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                          ).sum(:amount)
        major_account_total_credit      = JournalEntry.joins(:voucher).where(
                                            "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                          ).sum(:amount)
        major_account_total_net         = major_account_total_debit - major_account_total_credit

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s,
          total_debit: major_account_total_debit,
          total_credit: major_account_total_credit,
          total_net:  major_account_total_net
        }

        major_account_data[:mother_accounting_code_entries] = []
        major_account.mother_accounting_codes.each do |mother_accounting_code|
          ### BUILD MOTHER ACCOUNTING CODES
          mother_accounting_code_accounting_codes = @expense_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)
          mother_accounting_code_total_debit      = JournalEntry.joins(:voucher).where(
                                                      "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,@start_date, @end_date
                                                    ).sum(:amount)
          mother_accounting_code_total_credit     = JournalEntry.joins(:voucher).where(
                                                      "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                                    ).sum(:amount)

          mother_accounting_code_total_net        = mother_accounting_code_total_debit  - mother_accounting_code_total_credit

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s,
            total_debit:  mother_accounting_code_total_debit,
            total_credit: mother_accounting_code_total_credit,
            total_net: mother_accounting_code_total_net
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            ### BUILD ACCOUNTING CODE CATEGORY
            accounting_code_category_accounting_codes = @expense_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id) 
            accounting_code_category_total_debit      = JournalEntry.joins(:voucher).where(
                                                          "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                                        ).sum(:amount)
            accounting_code_category_total_credit     = JournalEntry.joins(:voucher).where(
                                                          "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq, @start_date,@end_date
                                                        ).sum(:amount)

            accounting_code_category_total_net        = accounting_code_category_total_debit  - accounting_code_category_total_credit

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
              total_debit: accounting_code_category_total_debit,
              total_credit: accounting_code_category_total_credit,
              total_net: accounting_code_category_total_net
            }

            accounting_code_category_data[:accounting_code_entries] = []
            accounting_code_category_accounting_codes.each do |accounting_code|
            
              ### BUILD ACCOUNTING CODE
              accounting_code_total_debit     = JournalEntry.joins(:voucher).where(
                                                  "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id = ? AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ?",
                                                  accounting_code.id,@start_date,@end_date
                                                  
                                                ).sum(:amount)
                                               # raise @start_date.inspect
              #if accounting_code_total_debit > 0 
              #  raise accounting_code.id.inspect
              #end
                                                
              accounting_code_total_credit    = JournalEntry.joins(:voucher).where(
                                                  "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id = ? AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ?  ",
                                                  accounting_code.id, @start_date,@end_date
                                                ).sum(:amount)
              accounting_code_total_net       = accounting_code_total_debit - accounting_code_total_credit
              accounting_code_data = {
                id: accounting_code.id,
                name: accounting_code.to_s,
                total_debit: accounting_code_total_debit,
                total_credit: accounting_code_total_credit,
                total_net: accounting_code_total_net
              }

              if accounting_code_data[:total_net] != 0
                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            end

            if accounting_code_category_data[:total_net] != 0
              mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
            end
          end

          if mother_accounting_code_data[:total_net] != 0
            major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
          end
        end

        if major_account_data[:total_net] != 0
          @data[:expense_entries][:major_account_entries] << major_account_data
        end
      end
    end

    def build_revenue!
      total_debit   = JournalEntry.joins(:voucher).where(
                        "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ?",
                        @revenue_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                      ).sum(:amount)

      total_credit  = JournalEntry.joins(:voucher).where(
                        "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ?",
                        @revenue_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                      ).sum(:amount)

      total_net     = total_credit - total_debit

      @data[:revenue_entries][:total_debit]   = total_debit
      @data[:revenue_entries][:total_credit]  = total_credit
      @data[:revenue_entries][:total_net]     = total_net

      ### BUILD MAJOR ACCOUNTS
      @data[:revenue_entries][:major_account_entries]  = []

      @revenue_group.major_accounts.each do |major_account|
        major_account_accounting_codes  = @revenue_accounting_codes.where("major_accounts.id = ?", major_account.id)
        major_account_total_debit       = JournalEntry.joins(:voucher).where(
                                            "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                          ).sum(:amount)
        major_account_total_credit      = JournalEntry.joins(:voucher).where(
                                            "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                          ).sum(:amount)
        major_account_total_net         = major_account_total_credit - major_account_total_debit

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s,
          total_debit: major_account_total_debit,
          total_credit: major_account_total_credit,
          total_net:  major_account_total_net
        }

        major_account_data[:mother_accounting_code_entries] = []
        major_account.mother_accounting_codes.each do |mother_accounting_code|
          ### BUILD MOTHER ACCOUNTING CODES
          mother_accounting_code_accounting_codes = @revenue_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)
          mother_accounting_code_total_debit      = JournalEntry.joins(:voucher).where(
                                                      "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                                    ).sum(:amount)
          mother_accounting_code_total_credit     = JournalEntry.joins(:voucher).where(
                                                      "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                                    ).sum(:amount)

          mother_accounting_code_total_net        = mother_accounting_code_total_credit - mother_accounting_code_total_debit

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s,
            total_debit:  mother_accounting_code_total_debit,
            total_credit: mother_accounting_code_total_credit,
            total_net: mother_accounting_code_total_net
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            ### BUILD ACCOUNTING CODE CATEGORY
            accounting_code_category_accounting_codes = @revenue_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id) 
            accounting_code_category_total_debit      = JournalEntry.joins(:voucher).where(
                                                          "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                                        ).sum(:amount)
            accounting_code_category_total_credit     = JournalEntry.joins(:voucher).where(
                                                          "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,@start_date,@end_date
                                                        ).sum(:amount)

            accounting_code_category_total_net        = accounting_code_category_total_credit - accounting_code_category_total_debit

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
              total_debit: accounting_code_category_total_debit,
              total_credit: accounting_code_category_total_credit,
              total_net: accounting_code_category_total_net
            }

            accounting_code_category_data[:accounting_code_entries] = []
            accounting_code_category_accounting_codes.each do |accounting_code|
              ### BUILD ACCOUNTING CODE
              accounting_code_total_debit     = JournalEntry.joins(:voucher).where(
                                                  "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id = ? AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                  accounting_code.id,@start_date,@end_date
                                                ).sum(:amount)
              accounting_code_total_credit    = JournalEntry.joins(:voucher).where(
                                                  "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id = ? AND vouchers.is_year_end_closing IS NULL AND vouchers.date_prepared between ? and ? ",
                                                  accounting_code.id,@start_date,@end_date
                                                ).sum(:amount)
              accounting_code_total_net       = accounting_code_total_credit - accounting_code_total_debit
              accounting_code_data = {
                id: accounting_code.id,
                name: accounting_code.to_s,
                total_debit: accounting_code_total_debit,
                total_credit: accounting_code_total_credit,
                total_net: accounting_code_total_net
              }

              if accounting_code_data[:total_net] != 0
                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            end

            if accounting_code_category_data[:total_net] != 0
              mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
            end
          end

          if mother_accounting_code_data[:total_net] != 0
            major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
          end
        end

        if major_account_data[:total_net] != 0
          @data[:revenue_entries][:major_account_entries] << major_account_data
        end
      end
    end
  end
end
