module Accounting
	class AccountingCodeTrialBalance
		def initialize(accounting_code:)
			@accounting_code = accounting_code
		end

		def execute!
			result = {}

      result[:accounting_code] = @accounting_code
      result[:debit] = JournalEntry.joins(:voucher).where("vouchers.status = ? AND journal_entries.accounting_code_id = ? AND journal_entries.post_type = ?", "approved", @accounting_code.id, "DR").sum(:amount).to_f
      result[:credit] = JournalEntry.joins(:voucher).where("vouchers.status = ? AND journal_entries.accounting_code_id = ? AND journal_entries.post_type = ?", "approved", @accounting_code.id, "CR").sum(:amount).to_f

      result
		end
	end
end