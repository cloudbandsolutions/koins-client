module Accounting
  class SaveInterestOnShareCapitalCollections
    def initialize(data:)
      @data = data
      @interest_on_share_capital_collection = InterestOnShareCapitalCollection.new
      @interest_on_share_capital_collection.total_equity_amount = @data[:total_share]
      #@interest_on_share_capital_collection.total_interest_amount = @data[:gtotal_share]
      @interest_on_share_capital_collection.total_interest_amount = @data[:total_savings_account_amount] + @data[:total_cbu_account_amount] 
      @interest_on_share_capital_collection.collection_date = @data[:end_date]
      @interest_on_share_capital_collection.equity_rate = @data[:equity_interest_rate]
      @interest_on_share_capital_collection.savings_account_amount = @data[:total_savings_account_amount]
      @interest_on_share_capital_collection.cbu_account_amount = @data[:total_cbu_account_amount]
      
      #raise  @branch.inspect


    
    end

    def execute!
      
      @data[:test].each do |d|
        iosccr = InterestOnShareCapitalCollectionRecord.new(
          data: d
        )
        @interest_on_share_capital_collection.interest_on_share_capital_collection_records << iosccr

      end


      @interest_on_share_capital_collection.save
      #@voucher.save


    end
  end
end

