module Accounting
	class TrialBalanceExcel
		def initialize(data:, end_date:, start_date:, branch:, accounting_fund:)
			@data     = data
      @start_date = start_date
      @end_date = end_date
      @branch  = branch
      @accounting_fund = accounting_fund
    end	

		def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
          label_cell = wb.styles.add_style b: true, font_name: "Calibri"
          currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right_bold = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
          percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
          left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
          underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
          header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
          date_format_cell = wb.styles.add_style format_code: "mm-dd-yyyy", font_name: "Calibri", alignment: { horizontal: :right }
          default_cell = wb.styles.add_style font_name: "Calibri"

          sheet.add_row ["", "", "Trial Balance"], style: title_cell
          sheet.merge_cells("C1:D1")

          sheet.add_row []
          t_head = []
          t_head << "Accounting Code"
          t_head << "Beginning"
          t_head << ""
          t_head << "Current"
          t_head << ""
          t_head << "Ending"
          t_head << ""          
          sheet.merge_cells("B3:C3")
          sheet.merge_cells("D3:E3")
          sheet.merge_cells("F3:G3")
          sheet.add_row t_head, style: title_cell

          t_cr_dr = []
          t_cr_dr << ""
          t_cr_dr << "DR"
          t_cr_dr << "CR"
          t_cr_dr << "DR"
          t_cr_dr << "CR"
          t_cr_dr << "DR"
          t_cr_dr << "CR"   
          sheet.add_row t_cr_dr, style: title_cell

          @data[:entries].each do |entry|
            t_entry = []
            t_entry << entry[:accounting_code]
            t_entry << entry[:beginning_debit]
            t_entry << entry[:beginning_credit]
            t_entry << entry[:current_debit]
            t_entry << entry[:current_credit]
            t_entry << entry[:ending_debit]
            t_entry << entry[:ending_credit]
            sheet.add_row t_entry, style: [nil, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right] 
          end

            t_total = []
            t_total << "Total"
            t_total << @data[:total_beginning_debit]
            t_total << @data[:total_beginning_credit]
            t_total << @data[:total_current_debit]
            t_total << @data[:total_current_debit]
            t_total << @data[:total_ending_debit]
            t_total << @data[:total_ending_debit]
            sheet.add_row t_total, style: [nil, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold] 
        end
      end

      p
		end
	end
end
