module Accounting
  class GenerateAccountingEntryForInterestOnShareCapitalCollection
    def initialize(interest_on_share_capital_collection:, user:)
      @interest_on_share_capital_collection = interest_on_share_capital_collection
     @user = user
    
      @book = "JVB"
      @particular = "to record interest on share capital"
      @c_working_date     = ApplicationHelper.current_working_date
      #raise Settings.branch_ids.inspect
      @branch = Branch.where(id: Settings.branch_ids).first
      @voucher = Voucher.new(
                    status: "pending",
                    branch: @branch,
                    particular: "To record adjustment for IC 2018 declaration due to wrong rate.",
                    book: @book,
                    date_prepared: @c_working_date,
                    prepared_by: @user.to_s,

                  ) 
      
    end
    def execute!
    build_debit_entries!
    #build_credit_entries
    build_credit_for_savings_entries!
    build_credit_for_cbu_entries!
    @voucher

    end
    def build_debit_entries!
      accounting_code_debit = AccountingCode.find(430)
      debit_amount = @interest_on_share_capital_collection.total_interest_amount
      
      journal_entry = JournalEntry.new(
                          amount: debit_amount,
                          post_type: "CR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry


    end
    
    def build_credit_for_savings_entries!
      accounting_code_debit = AccountingCode.find(1890)
      credit_amount = @interest_on_share_capital_collection.cbu_account_amount
      journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "DR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry
   end



    def build_credit_for_cbu_entries!
      accounting_code_debit = AccountingCode.find(102)
      credit_amount = @interest_on_share_capital_collection.savings_account_amount
      journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "DR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry
   end

  end


end
