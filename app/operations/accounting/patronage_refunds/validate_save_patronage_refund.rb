module Accounting
  module PatronageRefunds
    class ValidateSavePatronageRefund
      def initialize(params:)
        @patronage_rate = params[:patronage_rate]
       
          @errors = []
      end

      def execute!
        if @patronage_rate.blank?
          @errors << "Not yet implemented"
        end
        @errors
      end
    end
  end
end
