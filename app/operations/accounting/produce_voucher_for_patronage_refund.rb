module Accounting
  class ProduceVoucherForPatronageRefund
    def initialize(patronage_refund_collection:, user:)
     @user = user
     @patronage_refund_collection = patronage_refund_collection
     @closing_date = ApplicationHelper.current_working_date
     @branch = Branch.where(id: Settings.branch_ids).first
#    raise @patronage_refund_collection.total_interest_amount.inspect
    end
    def execute!

        @voucher = Voucher.new(
                      book: 'JVB',
                      particular: "To Record declaration of Interest for Patronage Refund 2016",
                      branch: @branch,
                      date_prepared: @closing_date
                    )
        build_debit_entries!
#        #build_credit_entries!
        build_credit_for_cbu_entries!
        build_credit_for_savings_entries!
    
      @voucher
    end

    private

    def build_debit_entries!
      accounting_code_debit = AccountingCode.find(429)
      
      debit_amount = @patronage_refund_collection.total_interest_amount
      
      journal_entry = JournalEntry.new(
                          amount: debit_amount,
                          post_type: "DR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry


    end
    
    def build_credit_for_cbu_entries!
      accounting_code_debit = AccountingCode.find(1890)
      credit_amount = @patronage_refund_collection.total_cbu_amount
      journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "CR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry
   end

    def build_credit_for_savings_entries!
      accounting_code_debit = AccountingCode.find(102)
      credit_amount = @patronage_refund_collection.total_savings_amount
      journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "CR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry
   end

  end
end
