module Accounting
	class GenerateReverseEntryForLoanInsurance
		def initialize(voucher:)
			@voucher =  voucher
			@particular = "To reverse entry for #{@voucher.book} ##{@voucher.reference_number} dated #{@voucher.date_prepared.strftime("%b %d, %Y")}"
		end

		def execute!
			reverse_voucher = Voucher.new(
                  reference_number: Accounting::GenerateVoucherNumber.new(book: "JVB", branch: @voucher.branch).execute!,
                  date_prepared: Time.now,
                  particular: @particular,
                  book: 'JVB',
                  branch: @voucher.branch,
                  prepared_by: @voucher.prepared_by,
                  approved_by: @voucher.approved_by,
                  status: @voucher.status,
                  accounting_fund: @voucher.accounting_fund
                )

      @voucher.journal_entries.each do |journal_entry|
        reversed_journal_entry = JournalEntry.new(
                                  amount: journal_entry.amount,
                                  post_type: "#{journal_entry.post_type == 'DR' ? 'CR' : 'DR'}",
                                  accounting_code: journal_entry.accounting_code
                                )

        reverse_voucher.journal_entries << reversed_journal_entry
      end

      reverse_voucher.save!

      reverse_voucher
		end
	end
end
