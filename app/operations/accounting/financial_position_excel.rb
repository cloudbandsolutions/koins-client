module Accounting
	class FinancialPositionExcel
		def initialize(data:, end_date:, start_date:, branch:, detailed:)
			@data     = data
      @start_date = start_date.to_date
      @end_date = end_date.to_date
      @branch  = branch
      @detailed = detailed
    end	

		def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
          label_cell = wb.styles.add_style b: true, font_name: "Calibri"
          currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right_bold = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
          percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
          left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
          underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
          header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
          date_format_cell = wb.styles.add_style format_code: "mm-dd-yyyy", font_name: "Calibri", alignment: { horizontal: :right }
          default_cell = wb.styles.add_style font_name: "Calibri"
          label_cell_underline = wb.styles.add_style b: true, font_name: "Calibri", u: true
          currency_cell_right_bold_underline = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true, u: true

          sheet.add_row ["", "", "#{Settings.company}"], style: title_cell
          sheet.add_row ["", "", "Statement of Financial Position"], style: title_cell
          sheet.add_row ["", "", "As of #{@end_date.strftime("%b %d, %Y")}"], style: title_cell
          sheet.add_row []
          sheet.column_widths 1, 50, 5, 5, 5, 5
          # sheet.add_row ["Normal\nBalance", "", "As of #{@end_date.strftime("%b %d, %Y")}", "", "", "", "As of #{(@end_date - 1.month).strftime("%b %d, %Y")}", "Variance"], style: title_cell
          sheet.add_row ["Normal\nBalance", "", "As of #{@end_date.strftime("%b %d, %Y")}", "", "", ""], style: title_cell
          sheet.add_row ["", "", "General Fund", "Mutual Benefit", "Optional Fund", "TOTAL", "", ""], style: title_cell            
          sheet.merge_cells("C5:F5")
          sheet.merge_cells("A5:A6")
          # sheet.merge_cells("G5:G6")
          # sheet.merge_cells("H5:H6")
          

          sheet.add_row ["", "1 - ASSETS"], style: [nil, title_cell]
          @data[:assets_entries].each do |asset|
            @total_assets_gen_fund = 0.00
            @total_assets_mut_fund = 0.00
            @total_assets_opt_fund = 0.00
            @total_assets_fund = 0.00

            @data[:assets_entries][:major_account_entries].each do |asset_major_acc|
              @assets_major_gen_amt = 0.00
              @assets_major_mut_amt = 0.00
              @assets_major_opt_amt = 0.00
              
              sheet.add_row ["DR", "#{asset_major_acc[:name]}"], style: [nil, label_cell]

              asset_major_acc[:mother_accounting_code_entries].each do |asset_mother_acc|
                @assets_mother_gen_amt = 0.00
                @assets_mother_mut_amt = 0.00
                @assets_mother_opt_amt = 0.00
                
                # sheet.add_row ["DR", "#{asset_mother_acc[:name]}"], style: [nil, label_cell]
                
                asset_mother_acc[:accounting_code_category_entries].each do |asset_acc_code_cat|
                  # sheet.add_row ["DR", "#{asset_acc_code_cat[:name]}"], style: [nil, label_cell]


                  asset_acc_code_cat[:accounting_code_entries].each do |asset_acc_code|

                    if asset_acc_code[:m_ending_debit] > 0
                      @assets_mutual_amount = asset_acc_code[:m_ending_debit]
                    elsif asset_acc_code[:m_ending_credit] > 0
                      @assets_mutual_amount = asset_acc_code[:m_ending_credit] * -1
                    elsif asset_acc_code[:m_ending_credit] = 0
                      @assets_mutual_amount = 0.00 
                    end 

                    if asset_acc_code[:g_ending_debit] > 0
                      @assets_gen_amount = asset_acc_code[:g_ending_debit]
                    elsif asset_acc_code[:g_ending_credit] > 0
                      @assets_gen_amount = asset_acc_code[:g_ending_credit] * -1
                    elsif asset_acc_code[:g_ending_credit] = 0
                      @assets_gen_amount = 0.00  
                    end 

                    if asset_acc_code[:o_ending_debit] > 0
                      @assets_opt_amount = asset_acc_code[:o_ending_debit]
                    elsif asset_acc_code[:o_ending_credit] > 0
                      @assets_opt_amount = asset_acc_code[:o_ending_credit] * -1
                    elsif asset_acc_code[:o_ending_credit] = 0
                      @assets_opt_amount = 0.00  
                    end

                    @assets_total = @assets_mutual_amount.to_f + @assets_gen_amount.to_f + @assets_opt_amount.to_f
                    
                    # FOR TOTAL ASSET
                    @total_assets_gen_fund = @total_assets_gen_fund + @assets_gen_amount.to_f
                    @total_assets_opt_fund = @total_assets_opt_fund + @assets_opt_amount.to_f
                    @total_assets_mut_fund = @total_assets_mut_fund + @assets_mutual_amount.to_f
                    @total_assets_fund = @total_assets_fund + @assets_total.to_f

                    @assets_mother_gen_amt += @assets_gen_amount.to_f
                    @assets_mother_mut_amt += @assets_mutual_amount.to_f
                    @assets_mother_opt_amt += @assets_opt_amount.to_f

                    if @detailed == "true"
                      t_entry = []
                      t_entry << "DR"
                      t_entry << asset_acc_code[:name]
                      t_entry << @assets_gen_amount
                      t_entry << @assets_mutual_amount
                      t_entry << @assets_opt_amount
                      t_entry << @assets_total
                      sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                    end 
                  end
                  
                  @assets_mother_gen_total_amt = @assets_mother_gen_amt
                  @assets_mother_mut_total_amt = @assets_mother_mut_amt
                  @assets_mother_opt_total_amt = @assets_mother_opt_amt
                  @assets_mother_total_amt = @assets_mother_gen_amt + @assets_mother_mut_amt + @assets_mother_opt_amt

                  @assets_major_gen_amt += @assets_mother_gen_total_amt
                  @assets_major_mut_amt += @assets_mother_mut_total_amt
                  @assets_major_opt_amt += @assets_mother_opt_total_amt
                end
                sheet.add_row ["", "#{asset_mother_acc[:name]}", @assets_mother_gen_total_amt, @assets_mother_mut_total_amt, @assets_mother_opt_total_amt, @assets_mother_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
                
                @assets_major_gen_total_amt = @assets_major_gen_amt
                @assets_major_mut_total_amt = @assets_major_mut_amt
                @assets_major_opt_total_amt = @assets_major_opt_amt
                @assets_major_total_amt = @assets_major_gen_amt + @assets_major_mut_amt + @assets_major_opt_amt
                end
              sheet.add_row ["", "TOTAL #{asset_major_acc[:name].upcase}", @assets_major_gen_total_amt, @assets_major_mut_total_amt, @assets_major_opt_total_amt, @assets_major_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold] 
            end
          end
          sheet.add_row ["", "TOTAL ASSETS", @total_assets_gen_fund, @total_assets_mut_fund, @total_assets_opt_fund, @total_assets_fund], style: [nil, label_cell_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline]

          

          sheet.add_row ["", "2 - LIABILITIES"], style: [nil, title_cell]
          @data[:liabilities_entries].each do |liability|
            @total_liabilities_gen_fund = 0.00
            @total_liabilities_mut_fund = 0.00
            @total_liabilities_opt_fund = 0.00
            @total_liabilities_fund = 0.00

            @data[:liabilities_entries][:major_account_entries].each do |liability_major_acc|
              @liabilities_major_gen_amt = 0.00
              @liabilities_major_mut_amt = 0.00
              @liabilities_major_opt_amt = 0.00
              
              sheet.add_row ["CR", "#{liability_major_acc[:name]}"], style: [nil, label_cell]
              
              liability_major_acc[:mother_accounting_code_entries].each do |liability_mother_acc|
                @liabilities_mother_gen_amt = 0.00
                @liabilities_mother_mut_amt = 0.00
                @liabilities_mother_opt_amt = 0.00

                # sheet.add_row ["CR", "#{liability_mother_acc[:name]}"], style: [nil, label_cell]
                
                liability_mother_acc[:accounting_code_category_entries].each do |liability_acc_code_cat|
                  # sheet.add_row ["CR", "#{liability_acc_code_cat[:name]}"], style: [nil, label_cell]

                  liability_acc_code_cat[:accounting_code_entries].each do |liability_acc_code|

                    if liability_acc_code[:m_ending_debit] > 0
                      @liabilities_mutual_amount = liability_acc_code[:m_ending_debit]
                    elsif liability_acc_code[:m_ending_credit] > 0
                      @liabilities_mutual_amount = liability_acc_code[:m_ending_credit] * -1
                    elsif liability_acc_code[:m_ending_credit] = 0
                      @liabilities_mutual_amount = 0.00  
                    end 

                    if liability_acc_code[:g_ending_debit] > 0
                      @liabilities_gen_amount = liability_acc_code[:g_ending_debit]
                    elsif liability_acc_code[:g_ending_credit] > 0
                      @liabilities_gen_amount = liability_acc_code[:g_ending_credit] * -1
                    elsif liability_acc_code[:g_ending_credit] = 0
                      @liabilities_gen_amount = 0.00 
                    end 

                    if liability_acc_code[:o_ending_debit] > 0
                      @liabilities_opt_amount = liability_acc_code[:o_ending_debit]
                    elsif liability_acc_code[:o_ending_credit] > 0
                      @liabilities_opt_amount = liability_acc_code[:o_ending_credit] * -1
                    elsif liability_acc_code[:o_ending_credit] = 0
                      @liabilities_opt_amount = 0.00  
                    end

                    @liabilities_total = @liabilities_mutual_amount.to_f + @liabilities_gen_amount.to_f + @liabilities_opt_amount.to_f

                    @total_liabilities_gen_fund = @total_liabilities_gen_fund + @liabilities_gen_amount.to_f
                    @total_liabilities_opt_fund = @total_liabilities_opt_fund + @liabilities_opt_amount.to_f
                    @total_liabilities_mut_fund = @total_liabilities_mut_fund + @liabilities_mutual_amount.to_f
                    @total_liabilities_fund = @total_liabilities_fund + @liabilities_total.to_f

                    @liabilities_mother_gen_amt += @liabilities_gen_amount.to_f
                    @liabilities_mother_mut_amt += @liabilities_mutual_amount.to_f
                    @liabilities_mother_opt_amt += @liabilities_opt_amount.to_f 

                    if @detailed == "true"
                      t_entry = []
                      t_entry << "CR"
                      t_entry << liability_acc_code[:name]
                      t_entry << @liabilities_gen_amount
                      t_entry << @liabilities_mutual_amount
                      t_entry << @liabilities_opt_amount
                      t_entry << @liabilities_total
                      sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                    end 
                  end

                  @liabilities_mother_gen_total_amt = @liabilities_mother_gen_amt
                  @liabilities_mother_mut_total_amt = @liabilities_mother_mut_amt
                  @liabilities_mother_opt_total_amt = @liabilities_mother_opt_amt
                  @liabilities_mother_total_amt = @liabilities_mother_gen_amt + @liabilities_mother_mut_amt + @liabilities_mother_opt_amt

                  @liabilities_major_gen_amt += @liabilities_mother_gen_total_amt
                  @liabilities_major_mut_amt += @liabilities_mother_mut_total_amt
                  @liabilities_major_opt_amt += @liabilities_mother_opt_total_amt
                end 
                sheet.add_row ["", "#{liability_mother_acc[:name]}", @liabilities_mother_gen_total_amt, @liabilities_mother_mut_total_amt, @liabilities_mother_opt_total_amt, @liabilities_mother_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                @liabilities_major_gen_total_amt = @liabilities_major_gen_amt
                @liabilities_major_mut_total_amt = @liabilities_major_mut_amt
                @liabilities_major_opt_total_amt = @liabilities_major_opt_amt
                @liabilities_major_total_amt = @liabilities_major_gen_amt + @liabilities_major_mut_amt + @liabilities_major_opt_amt
              end
              sheet.add_row ["", "TOTAL #{liability_major_acc[:name].upcase}", @liabilities_major_gen_total_amt, @liabilities_major_mut_total_amt, @liabilities_major_opt_total_amt, @liabilities_major_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]  
            end
          end
          sheet.add_row ["", "TOTAL LIABILITIES", @total_liabilities_gen_fund, @total_liabilities_mut_fund, @total_liabilities_opt_fund, @total_liabilities_fund], style: [nil, label_cell_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline]



          sheet.add_row ["", "3 - FUND BALANCE"], style: [nil, title_cell]
          @data[:fund_balance_entries].each do |fund_balance|
            @total_fund_balance_gen_fund = 0.00
            @total_fund_balance_mut_fund = 0.00
            @total_fund_balance_opt_fund = 0.00
            @total_fund_balance_fund = 0.00

            @data[:fund_balance_entries][:major_account_entries].each do |fund_balance_major_acc|
              @fund_balance_major_gen_amt = 0.00
              @fund_balance_major_mut_amt = 0.00
              @fund_balance_major_opt_amt = 0.00

              sheet.add_row ["CR", "#{fund_balance_major_acc[:name]}"], style: [nil, label_cell]
              
              fund_balance_major_acc[:mother_accounting_code_entries].each do |fund_balance_mother_acc|
                @fund_balance_mother_gen_amt = 0.00
                @fund_balance_mother_mut_amt = 0.00
                @fund_balance_mother_opt_amt = 0.00

                # sheet.add_row ["CR", "#{fund_balance_mother_acc[:name]}"], style: [nil, label_cell]
                
                fund_balance_mother_acc[:accounting_code_category_entries].each do |fund_balance_acc_code_cat|
                  # sheet.add_row ["CR", "#{fund_balance_acc_code_cat[:name]}"], style: [nil, label_cell]

                  fund_balance_acc_code_cat[:accounting_code_entries].each do |fund_balance_acc_code|

                    if fund_balance_acc_code[:m_ending_debit] > 0
                      @fund_balance_mutual_amount = fund_balance_acc_code[:m_ending_debit]
                    elsif fund_balance_acc_code[:m_ending_credit] > 0
                      @fund_balance_mutual_amount = fund_balance_acc_code[:m_ending_credit] * -1
                    elsif fund_balance_acc_code[:m_ending_credit] = 0
                      @fund_balance_mutual_amount = 0.00  
                    end 

                    if fund_balance_acc_code[:g_ending_debit] > 0
                      @fund_balance_gen_amount = fund_balance_acc_code[:g_ending_debit]
                    elsif fund_balance_acc_code[:g_ending_credit] > 0
                      @fund_balance_gen_amount = fund_balance_acc_code[:g_ending_credit] * -1
                    elsif fund_balance_acc_code[:g_ending_credit] = 0
                      @fund_balance_gen_amount = 0.00 
                    end 

                    if fund_balance_acc_code[:o_ending_debit] > 0
                      @fund_balance_opt_amount = fund_balance_acc_code[:o_ending_debit]
                    elsif fund_balance_acc_code[:o_ending_credit] > 0
                      @fund_balance_opt_amount = fund_balance_acc_code[:o_ending_credit] * -1
                    elsif fund_balance_acc_code[:o_ending_credit] = 0
                      @fund_balance_opt_amount = 0.00
                    end

                    @fund_balance_total = @fund_balance_mutual_amount.to_f + @fund_balance_gen_amount.to_f + @fund_balance_opt_amount.to_f

                    @total_fund_balance_gen_fund = @total_fund_balance_gen_fund + @fund_balance_gen_amount.to_f
                    @total_fund_balance_opt_fund = @total_fund_balance_opt_fund + @fund_balance_opt_amount.to_f
                    @total_fund_balance_mut_fund = @total_fund_balance_mut_fund + @fund_balance_mutual_amount.to_f
                    @total_fund_balance_fund = @total_fund_balance_fund + @fund_balance_total.to_f

                    @fund_balance_mother_gen_amt += @fund_balance_gen_amount.to_f
                    @fund_balance_mother_mut_amt += @fund_balance_mutual_amount.to_f
                    @fund_balance_mother_opt_amt += @fund_balance_opt_amount.to_f    

                    if @detailed == "true"
                      t_entry = []
                      t_entry << "CR"
                      t_entry << fund_balance_acc_code[:name]
                      t_entry << @fund_balance_gen_amount
                      t_entry << @fund_balance_mutual_amount
                      t_entry << @fund_balance_opt_amount
                      t_entry << @fund_balance_total
                      sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                    end 
                  end

                  @fund_balance_mother_gen_total_amt = @fund_balance_mother_gen_amt
                  @fund_balance_mother_mut_total_amt = @fund_balance_mother_mut_amt
                  @fund_balance_mother_opt_total_amt = @fund_balance_mother_opt_amt
                  @fund_balance_mother_total_amt = @fund_balance_mother_gen_amt + @fund_balance_mother_mut_amt + @fund_balance_mother_opt_amt

                  @fund_balance_major_gen_amt += @fund_balance_mother_gen_total_amt
                  @fund_balance_major_mut_amt += @fund_balance_mother_mut_total_amt
                  @fund_balance_major_opt_amt += @fund_balance_mother_opt_total_amt
                end
                sheet.add_row ["", "#{fund_balance_mother_acc[:name]}", @fund_balance_mother_gen_total_amt, @fund_balance_mother_mut_total_amt, @fund_balance_mother_opt_total_amt, @fund_balance_mother_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                @fund_balance_major_gen_total_amt = @fund_balance_major_gen_amt
                @fund_balance_major_mut_total_amt = @fund_balance_major_mut_amt
                @fund_balance_major_opt_total_amt = @fund_balance_major_opt_amt
                @fund_balance_major_total_amt = @fund_balance_major_gen_amt + @fund_balance_major_mut_amt + @fund_balance_major_opt_amt 
              end
              #sheet.add_row ["", "TOTAL #{fund_balance_major_acc[:name].upcase}", @fund_balance_major_gen_total_amt, @fund_balance_major_mut_total_amt, @fund_balance_major_opt_total_amt, @fund_balance_major_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
            end
          end
          
          @data[:fund_balance_entries_b].each do |fund_balance_b|
            @total_fund_balance_gen_fundd = 0.00
            @total_fund_balance_mut_fundd = 0.00
            @total_fund_balance_opt_fundd = 0.00
            @total_fund_balance_fundd = 0.00

            @data[:fund_balance_entries_b][:major_account_entries_b].each do |fund_balance_major_acc_b|
              @fund_balance_major_gen_amtt = 0.00
              @fund_balance_major_mut_amtt = 0.00
              @fund_balance_major_opt_amtt = 0.00
 
              #sheet.add_row ["CR", "#{fund_balance_major_acc_b[:name]}"], style: [nil, label_cell]
             
              if fund_balance_major_acc_b[:mother_accounting_code_entries_b].count > 0

                fund_balance_major_acc_b[:mother_accounting_code_entries_b].each do |fund_balance_mother_acc_b|
                  @fund_balance_mother_gen_amtt = 0.00
                  @fund_balance_mother_mut_amtt = 0.00
                  @fund_balance_mother_opt_amtt = 0.00

                  #sheet.add_row ["CR", "#{fund_balance_mother_acc_b[:name]}"], style: [nil, label_cell]
                  
                  fund_balance_mother_acc_b[:accounting_code_category_entries_b].each do |fund_balance_acc_code_cat_b|
                    #sheet.add_row ["CR", "#{fund_balance_acc_code_cat_b[:name]}"], style: [nil, label_cell]

                    fund_balance_acc_code_cat_b[:accounting_code_entries_b].each do |fund_balance_acc_code_b|

                      if fund_balance_acc_code_b[:m_ending_debit] > 0
                        @fund_balance_mutual_amountt = fund_balance_acc_code_b[:m_ending_debit]
                      elsif fund_balance_acc_code_b[:m_ending_credit] > 0
                        @fund_balance_mutual_amountt = fund_balance_acc_code_b[:m_ending_credit] * -1
                      elsif fund_balance_acc_code_b[:m_ending_credit] = 0
                        @fund_balance_mutual_amountt = 0.00  
                      end 

                      if fund_balance_acc_code_b[:g_ending_debit] > 0
                        @fund_balance_gen_amountt = fund_balance_acc_code_b[:g_ending_debit]
                      elsif fund_balance_acc_code_b[:g_ending_credit] > 0
                        @fund_balance_gen_amountt = fund_balance_acc_code_b[:g_ending_credit] * -1
                      elsif fund_balance_acc_code_b[:g_ending_credit] = 0
                        @fund_balance_gen_amountt = 0.00 
                      end 

                      if fund_balance_acc_code_b[:o_ending_debit] > 0
                        @fund_balance_opt_amountt = fund_balance_acc_code_b[:o_ending_debit]
                      elsif fund_balance_acc_code_b[:o_ending_credit] > 0
                        @fund_balance_opt_amountt = fund_balance_acc_code_b[:o_ending_credit] * -1
                      elsif fund_balance_acc_code_b[:o_ending_credit] = 0
                        @fund_balance_opt_amountt = 0.00
                      end

                      @fund_balance_totall = @fund_balance_mutual_amountt.to_f + @fund_balance_gen_amountt.to_f + @fund_balance_opt_amountt.to_f

                      @total_fund_balance_gen_fundd = @total_fund_balance_gen_fundd + @fund_balance_gen_amountt.to_f
                      @total_fund_balance_opt_fundd = @total_fund_balance_opt_fundd + @fund_balance_opt_amountt.to_f
                      @total_fund_balance_mut_fundd = @total_fund_balance_mut_fundd + @fund_balance_mutual_amountt.to_f
                      @total_fund_balance_fundd = @total_fund_balance_fundd + @fund_balance_totall.to_f

                      @fund_balance_mother_gen_amtt += @fund_balance_gen_amountt.to_f
                      @fund_balance_mother_mut_amtt += @fund_balance_mutual_amountt.to_f
                      @fund_balance_mother_opt_amtt += @fund_balance_opt_amountt.to_f    

                      if @detailed == "true"
                        t_entry = []
                        t_entry << "CR"
                        t_entry << fund_balance_acc_code_b[:name]
                        t_entry << @fund_balance_gen_amountt
                        t_entry << @fund_balance_mutual_amountt
                        t_entry << @fund_balance_opt_amountt
                        t_entry << @fund_balance_totall
                        sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                      end 
                    end

                    @fund_balance_mother_gen_total_amtt = @fund_balance_mother_gen_amtt
                    @fund_balance_mother_mut_total_amtt = @fund_balance_mother_mut_amtt
                    @fund_balance_mother_opt_total_amtt = @fund_balance_mother_opt_amtt
                    @fund_balance_mother_total_amtt = @fund_balance_mother_gen_amtt + @fund_balance_mother_mut_amtt + @fund_balance_mother_opt_amtt

                    @fund_balance_major_gen_amtt += @fund_balance_mother_gen_total_amtt
                    @fund_balance_major_mut_amtt += @fund_balance_mother_mut_total_amtt
                    @fund_balance_major_opt_amtt += @fund_balance_mother_opt_total_amtt
                    
                  end
                  #sheet.add_row ["", "#{fund_balance_mother_acc_b[:name]}", @fund_balance_mother_gen_total_amtt, @fund_balance_mother_mut_total_amtt, @fund_balance_mother_opt_total_amtt, @fund_balance_mother_total_amtt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                  @fund_balance_major_gen_total_amtt = @fund_balance_major_gen_amtt
                  @fund_balance_major_mut_total_amtt = @fund_balance_major_mut_amtt
                  @fund_balance_major_opt_total_amtt = @fund_balance_major_opt_amtt
                  @fund_balance_major_total_amtt = @fund_balance_major_gen_amtt + @fund_balance_major_mut_amtt + @fund_balance_major_opt_amtt 
                end
              else
                @fund_balance_major_gen_total_amtt = 0.00
                @fund_balance_major_mut_total_amtt = 0.00
                @fund_balance_major_opt_total_amtt = 0.00
                @fund_balance_major_total_amtt = 0.00  
              end
              #sheet.add_row ["", "TOTAL #{fund_balance_major_acc_b[:name].upcase}", @fund_balance_major_gen_total_amtt, @fund_balance_major_mut_total_amtt, @fund_balance_major_opt_total_amtt, @fund_balance_major_total_amtt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
              
              if fund_balance_major_acc_b[:name] == "Revenue"
                @revenue_gen_total = @fund_balance_major_gen_total_amtt
                @revenue_mut_total = @fund_balance_major_mut_total_amtt
                @revenue_opt_total = @fund_balance_major_opt_total_amtt
                @revenue_total = @fund_balance_major_total_amtt

              elsif fund_balance_major_acc_b[:name] == "Benefit Expenses" 
                @net_surplus_before_operating_expense_major_gen_total = @revenue_gen_total + @fund_balance_major_gen_total_amtt
                @net_surplus_before_operating_expense_major_mut_total = @revenue_mut_total + @fund_balance_major_mut_total_amtt
                @net_surplus_before_operating_expense_major_opt_total = @revenue_opt_total + @fund_balance_major_opt_total_amtt
                @net_surplus_before_operating_expense_major_total = @revenue_total + @fund_balance_major_total_amt
                
                #sheet.add_row ["", "NET SURPLUS BEFORE OPERATING EXPENSE", @net_surplus_before_operating_expense_major_gen_total, @net_surplus_before_operating_expense_major_mut_total, @net_surplus_before_operating_expense_major_opt_total, @net_surplus_before_operating_expense_major_total], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

              elsif fund_balance_major_acc_b[:name] == "Operating Expenses"
                @net_surplus_before_non_operating_expense_major_gen_total = @net_surplus_before_operating_expense_major_gen_total + @fund_balance_major_gen_total_amtt
                @net_surplus_before_non_operating_expense_major_mut_total = @net_surplus_before_operating_expense_major_mut_total + @fund_balance_major_mut_total_amtt
                @net_surplus_before_non_operating_expense_major_opt_total = @net_surplus_before_operating_expense_major_opt_total + @fund_balance_major_opt_total_amtt
                @net_surplus_before_non_operating_expense_major_total = @net_surplus_before_operating_expense_major_total + @fund_balance_major_total_amtt
                
                #sheet.add_row ["", "NET SURPLUS BEFORE NON OPERATING INCOME(EXPENSE)", @net_surplus_before_non_operating_expense_major_gen_total, @net_surplus_before_non_operating_expense_major_mut_total, @net_surplus_before_non_operating_expense_major_opt_total, @net_surplus_before_non_operating_expense_major_total], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

              elsif fund_balance_major_acc_b[:name] == "Other Non-Opearting Expenses"
                #sheet.add_row ["", "NET SURPLUS BEFORE INVESTMENT RETURN", @net_surplus_before_non_operating_expense_major_gen_total, @net_surplus_before_non_operating_expense_major_mut_total, @net_surplus_before_non_operating_expense_major_opt_total, @net_surplus_before_non_operating_expense_major_total], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
              end
            end
          end

            # para sa revenue na hiwalay (investment income at investment expenses)
            @data[:fund_balance_entries_a][:major_account_entries_a].each do |fund_balance_major_acc_a|
              @fund_balance_major_gen_amtt = 0.00
              @fund_balance_major_mut_amtt = 0.00
              @fund_balance_major_opt_amtt = 0.00

              #sheet.add_row ["CR", "#{fund_balance_major_acc_a[:name]} a"], style: [nil, label_cell]
              
              fund_balance_major_acc_a[:mother_accounting_code_entries_a].each do |fund_balance_mother_acc_a|
                @fund_balance_mother_gen_amtt = 0.00
                @fund_balance_mother_mut_amtt = 0.00
                @fund_balance_mother_opt_amtt = 0.00

                #sheet.add_row ["CR", "#{fund_balance_mother_acc_a[:name]} b"], style: [nil, label_cell]
                
                fund_balance_mother_acc_a[:accounting_code_category_entries_a].each do |fund_balance_acc_code_cat_a|
                  #sheet.add_row ["CR", "#{fund_balance_acc_code_cat_a[:name]} c"], style: [nil, label_cell]

                  fund_balance_acc_code_cat_a[:accounting_code_entries_a].each do |fund_balance_acc_code_a|

                    if fund_balance_acc_code_a[:m_ending_debit] > 0
                      @fund_balance_mutual_amountt = fund_balance_acc_code_a[:m_ending_debit]
                    elsif fund_balance_acc_code_a[:m_ending_credit] > 0
                      @fund_balance_mutual_amountt = fund_balance_acc_code_a[:m_ending_credit] * -1
                    elsif fund_balance_acc_code_a[:m_ending_credit] = 0
                      @fund_balance_mutual_amountt = 0.00  
                    end 

                    if fund_balance_acc_code_a[:g_ending_debit] > 0
                      @fund_balance_gen_amountt = fund_balance_acc_code_a[:g_ending_debit]
                    elsif fund_balance_acc_code_a[:g_ending_credit] > 0
                      @fund_balance_gen_amountt = fund_balance_acc_code_a[:g_ending_credit] * -1
                    elsif fund_balance_acc_code_a[:g_ending_credit] = 0
                      @fund_balance_gen_amountt = 0.00 
                    end 

                    if fund_balance_acc_code_a[:o_ending_debit] > 0
                      @fund_balance_opt_amountt = fund_balance_acc_code_a[:o_ending_debit]
                    elsif fund_balance_acc_code_a[:o_ending_credit] > 0
                      @fund_balance_opt_amountt = fund_balance_acc_code_a[:o_ending_credit] * -1
                    elsif fund_balance_acc_code_a[:o_ending_credit] = 0
                      @fund_balance_opt_amountt = 0.00
                    end

                    @fund_balance_totall = @fund_balance_mutual_amountt.to_f + @fund_balance_gen_amountt.to_f + @fund_balance_opt_amountt.to_f

                    @total_fund_balance_gen_fundd = @total_fund_balance_gen_fundd + @fund_balance_gen_amountt.to_f
                    @total_fund_balance_opt_fundd = @total_fund_balance_opt_fundd + @fund_balance_opt_amountt.to_f
                    @total_fund_balance_mut_fundd = @total_fund_balance_mut_fundd + @fund_balance_mutual_amountt.to_f
                    @total_fund_balance_fundd = @total_fund_balance_fundd + @fund_balance_totall.to_f

                    @fund_balance_mother_gen_amtt += @fund_balance_gen_amountt.to_f
                    @fund_balance_mother_mut_amtt += @fund_balance_mutual_amountt.to_f
                    @fund_balance_mother_opt_amtt += @fund_balance_opt_amountt.to_f    

                    if @detailed == "true"
                      t_entry = []
                      t_entry << "CR"
                      t_entry << fund_balance_acc_code_a[:name]
                      t_entry << @fund_balance_gen_amountt
                      t_entry << @fund_balance_mutual_amountt
                      t_entry << @fund_balance_opt_amountt
                      t_entry << @fund_balance_totall
                      sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                    end   
                  end

                  @fund_balance_mother_gen_total_amtt = @fund_balance_mother_gen_amtt
                  @fund_balance_mother_mut_total_amtt = @fund_balance_mother_mut_amtt
                  @fund_balance_mother_opt_total_amtt = @fund_balance_mother_opt_amtt
                  @fund_balance_mother_total_amtt = @fund_balance_mother_gen_amtt + @fund_balance_mother_mut_amtt + @fund_balance_mother_opt_amtt

                  @fund_balance_major_gen_amtt += @fund_balance_mother_gen_total_amtt
                  @fund_balance_major_mut_amtt += @fund_balance_mother_mut_total_amtt
                  @fund_balance_major_opt_amtt += @fund_balance_mother_opt_total_amtt
                end
                #sheet.add_row ["", "#{fund_balance_mother_acc_a[:name]}", @fund_balance_mother_gen_total_amtt, @fund_balance_mother_mut_total_amtt, @fund_balance_mother_opt_total_amtt, @fund_balance_mother_total_amtt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                @fund_balance_major_gen_total_amtt = @fund_balance_major_gen_amtt
                @fund_balance_major_mut_total_amtt = @fund_balance_major_mut_amtt
                @fund_balance_major_opt_total_amtt = @fund_balance_major_opt_amtt
                @fund_balance_major_total_amtt = @fund_balance_major_gen_amtt + @fund_balance_major_mut_amtt + @fund_balance_major_opt_amtt 
              end
              #sheet.add_row ["", "TOTAL #{fund_balance_major_acc_a[:name].upcase}", @fund_balance_major_gen_total_amtt, @fund_balance_major_mut_total_amtt, @fund_balance_major_opt_total_amtt, @fund_balance_major_total_amtt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
    
            end
          sheet.add_row ["", "NET COMPREHENSIVE SURPLUS (LOSS)", @total_fund_balance_gen_fundd, @total_fund_balance_mut_fundd, @total_fund_balance_opt_fundd, @total_fund_balance_fundd], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

          sheet.add_row ["", "TOTAL FUND BALANCE", @total_fund_balance_gen_fund + @total_fund_balance_gen_fundd, @total_fund_balance_mut_fund + @total_fund_balance_mut_fundd, @total_fund_balance_opt_fund + @total_fund_balance_opt_fundd, @total_fund_balance_fund + @total_fund_balance_fundd], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

          sheet.add_row ["", "2 & 3 - TOTAL LIABILITIES & FUND BALANCE", (@total_liabilities_gen_fund + @total_fund_balance_gen_fund) + @total_fund_balance_gen_fundd , (@total_liabilities_mut_fund + @total_fund_balance_mut_fund) + @total_fund_balance_mut_fundd, (@total_liabilities_opt_fund + @total_fund_balance_opt_fund) + @total_fund_balance_opt_fundd, (@total_liabilities_fund + @total_fund_balance_fund) + @total_fund_balance_fundd], style: [nil, label_cell_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline]
        end
      end
      p
		end
	end
end
