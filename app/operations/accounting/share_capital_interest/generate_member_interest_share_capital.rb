module Accounting
  module ShareCapitalInterest
    class GenerateMemberInterestShareCapital
      SAVINGS_INTEREST  = 0.9

      def initialize(member:, equity_account:, year:, equity_interest:)
        @member           = member
        @year             = year
        @start_date       = Date.new(@year, 1, 1)
        @end_date         = Date.new(@year, 12, 31)
        @equity_interest  = (equity_interest.to_f / 100).round(4)
        @previous_year    = @year - 1
        @equity_account   = equity_account

        @previous_balance = @equity_account.equity_account_transactions.where(
                              "extract(year from transacted_at) = ? AND status = ?",
                              @previous_year,
                              "approved"
                            ).sum(:amount)

        @data = {
          member_first_name: @member.first_name,
          member_last_name: @member.last_name,
          member_id: @member.id,
          equity_account_id: @equity_account.id,
          equity_interest: @equity_interest,
          previous_balance: @previous_balance,
          start_date: @start_date,
          end_date: @end_date,
          identification_number: @member.identification_number
        }

        records = []
        (1..12).map{ |o| o }.each do |month|
          date  = Date.new(@year, month, -1)

          records << {
            date: date,
            amount: 0.00
          }
        end

        @data[:records] = records
      end

      def execute!
        if @previous_balance > 0.00
          @data[:records][0][:amount] = @previous_balance
        end

        @data[:records].each_with_index do |record, i|
          if i > 0
            # previous
            @data[:records][i][:amount] = @data[:records][i - 1][:amount]

            current_month_balance = @equity_account.equity_account_transactions.where(
                                      "extract(year from transacted_at) = ? AND extract(month from transacted_at) = ?",
                                      @year,
                                      i + 1
                                    ).last.try(:ending_balance)

            if current_month_balance.blank?
              current_month_balance = 0.00
            end

            @data[:records][i][:amount] += current_month_balance
          end
        end

        # Compute total share
        total_share = 0.00
        @data[:records].each_with_index do |record, i|
          total_share += record[:amount]
        end

        # Compute average share
        average_share = (total_share / 12).round(2)

        # Compute Interest amount
        interest_amount = (average_share * @equity_interest).round(2)

        # Compute Savings
        savings_amount  = (interest_amount * SAVINGS_INTEREST).round(2)

        # Compute CBU
        cbu_amount      = (interest_amount * (1 - SAVINGS_INTEREST)).round(2)

        @data[:total_share]     = total_share
        @data[:average_share]   = average_share
        @data[:interest_amount] = interest_amount
        @data[:savings_amount]  = savings_amount
        @data[:cbu_amount]      = cbu_amount

        @data
      end
    end
  end
end
