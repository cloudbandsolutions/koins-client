module Accounting
  class GenerateBalanceSheet
    def initialize(as_of:, prepared_by:)
      @as_of                = as_of.to_date
      #as_of_d = "2017-12-31"
      #@as_of = as_of_d.to_date

      @start_date           = Date.new(@as_of.to_date.year, 01, 01)
      @end_date             = Date.new(@as_of.to_date.year, 12,31)
      #raise @as_of.inspect
      @prepared_by          = prepared_by
      @assets_group_id      = Settings.assets_group_id
      @liabilities_group_id = Settings.liabilities_group_id
      @equity_group_id      = Settings.equity_group_id
      @revenue_group_id     = Settings.revenue_group_id
      @expenses_group_id    = Settings.expenses_group_id

      @assets_group         = MajorGroup.find(@assets_group_id)
      @liabilities_group    = MajorGroup.find(@liabilities_group_id)
      @equity_group         = MajorGroup.find(@equity_group_id)
      @revenue_group        = MajorGroup.find(@revenue_group_id)
      @expenses_group       = MajorGroup.find(@expenses_group_id)

      @assets_accounting_codes      = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @assets_group.id)
      @liabilities_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @liabilities_group.id)
      @equity_accounting_codes      = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @equity_group.id)
      @revenue_accounting_codes     = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @revenue_group.id)
      @expenses_accounting_codes    = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @expenses_group.id)

      #@journal_entries  = JournalEntry.joins(:voucher).where("vouchers.date_prepared >= ?", @start_date)
      @journal_entries  = JournalEntry.joins(:voucher)
      #@journal_entries  = JournalEntry.joins(:voucher)

      @data                       = {}
      @data[:date_prepared]       = @as_of

      @data[:assets_entries]      = {}
      @data[:liabilities_entries] = {}
      @data[:equity_entries]      = {}
      @data[:revenue_entries]     = {}
      @data[:expenses_entries]    = {}
    end

    def execute!
      build_assets!
      build_liabilities!
      build_equity!
      build_revenue!
      build_expenses!

      @data[:total_liabilities_and_net_worth] = @data[:liabilities_entries][:total_net] + @data[:equity_entries][:total_net] + @data[:revenue_entries][:total_net] - @data[:expenses_entries][:total_net]

      @data[:total_net_income]  = @data[:revenue_entries][:total_net] - @data[:expenses_entries][:total_net]
     # raise @data[:expenses_entries][:total_net].to_f.inspect

      @data
    end

    private

    def build_expenses!
      total_debit   = @journal_entries.where(
                        "vouchers.status = 'approved' 
                        AND post_type = 'DR' 
                        AND accounting_code_id IN (?) AND 
                        vouchers.date_prepared between ? 
                        AND ?",
                        @expenses_accounting_codes.pluck("accounting_codes.id").uniq,
                        @start_date,
                        @end_date
                      ).sum(:amount) 
                      #raise total_debit.to_f.inspect
      total_credit  = @journal_entries.where(
                        "vouchers.status = 'approved' 
                        AND post_type = 'CR' 
                        AND accounting_code_id IN (?) 
                        AND vouchers.date_prepared between ? 
                        AND ?",
                        @expenses_accounting_codes.pluck("accounting_codes.id").uniq,
                        @start_date,
                        @end_date
                      ).sum(:amount)
                      #raise total_credit.to_f.inspect
      total_net     = total_debit - total_credit

      @data[:expenses_entries][:total_debit]   = total_debit
      @data[:expenses_entries][:total_credit]  = total_credit


      @data[:expenses_entries][:total_net]     = total_net


      ### BUILD MAJOR ACCOUNTS
      @data[:expenses_entries][:major_account_entries]  = []
      @expenses_group.major_accounts.each do |major_account|
        major_account_accounting_codes  = @expenses_accounting_codes.where("major_accounts.id = ?", major_account.id)
        major_account_total_debit       = @journal_entries.where(
                                            "vouchers.status = 'approved' 
                                            AND post_type = 'DR' 
                                            AND accounting_code_id IN (?) 
                                            AND vouchers.date_prepared between ? 
                                            AND ?",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,
                                            @start_date,
                                            @end_date
                                          ).sum(:amount)

        major_account_total_credit      = @journal_entries.where(
                                            "vouchers.status = 'approved' 
                                            AND post_type = 'CR' 
                                            AND accounting_code_id IN (?) 
                                            AND vouchers.date_prepared between ? 
                                            AND ?",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,
                                            @start_date,
                                            @end_date
                                          ).sum(:amount)

        major_account_total_net         = major_account_total_debit - major_account_total_credit

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s,
          total_debit: major_account_total_debit,
          total_credit: major_account_total_credit,
          total_net:  major_account_total_net
        }

        major_account_data[:mother_accounting_code_entries] = []
        major_account.mother_accounting_codes.each do |mother_accounting_code|
          ### BUILD MOTHER ACCOUNTING CODES
          mother_accounting_code_accounting_codes = @expenses_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)
          mother_accounting_code_total_debit      = @journal_entries.where(
                                                      "vouchers.status = 'approved' 
                                                      AND post_type = 'DR' 
                                                      AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,
                                                      @as_of
                                                    ).sum(:amount)
          mother_accounting_code_total_credit     = @journal_entries.where(
                                                      "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,
                                                      @as_of
                                                    ).sum(:amount)

          mother_accounting_code_total_net        = mother_accounting_code_total_debit  - mother_accounting_code_total_credit
          #raise mother_accounting_code_total_net.to_f.inspect
          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s,
            total_debit:  mother_accounting_code_total_debit,
            total_credit: mother_accounting_code_total_credit,
            total_net: mother_accounting_code_total_net
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            ### BUILD ACCOUNTING CODE CATEGORY
            accounting_code_category_accounting_codes = @expenses_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id) 
            accounting_code_category_total_debit      = @journal_entries.where(
                                                          "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,
                                                          @as_of
                                                        ).sum(:amount)
            accounting_code_category_total_credit     = @journal_entries.where(
                                                          "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,
                                                          @as_of
                                                        ).sum(:amount)

            accounting_code_category_total_net        = accounting_code_category_total_debit  - accounting_code_category_total_credit

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
              total_debit: accounting_code_category_total_debit,
              total_credit: accounting_code_category_total_credit,
              total_net: accounting_code_category_total_net
            }

            accounting_code_category_data[:accounting_code_entries] = []
            accounting_code_category_accounting_codes.each do |accounting_code|
              ### BUILD ACCOUNTING CODE
              accounting_code_total_debit     = @journal_entries.where(
                                                  "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id = ? AND vouchers.date_prepared <= ?",
                                                  accounting_code.id,
                                                  @as_of
                                                ).sum(:amount)
              accounting_code_total_credit    = @journal_entries.where(
                                                  "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id = ? AND vouchers.date_prepared <= ?",
                                                  accounting_code.id,
                                                  @as_of
                                                ).sum(:amount)
              accounting_code_total_net       = accounting_code_total_debit - accounting_code_total_credit
              accounting_code_data = {
                id: accounting_code.id,
                name: accounting_code.to_s,
                total_debit: accounting_code_total_debit,
                total_credit: accounting_code_total_credit,
                total_net: accounting_code_total_net
              }

              if accounting_code_data[:total_net] != 0
                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            end

            if accounting_code_category_data[:total_net] != 0
              mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
            end
          end

          if mother_accounting_code_data[:total_net] != 0
            major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
          end
        end

        if major_account_data[:total_net] != 0
          @data[:expenses_entries][:major_account_entries] << major_account_data
        end
      end
    end

    def build_revenue!
      total_debit   = @journal_entries.where(
                        "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared between ? and ?",
                        @revenue_accounting_codes.pluck("accounting_codes.id").uniq,
                        @start_date,
                        @end_date
                      ).sum(:amount)

      total_credit  = @journal_entries.where(
                        "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared between ? and ?",
                        @revenue_accounting_codes.pluck("accounting_codes.id").uniq,
                        @start_date,
                        @end_date
                      ).sum(:amount)

      total_net     = total_credit - total_debit

      @data[:revenue_entries][:total_debit]   = total_debit
      @data[:revenue_entries][:total_credit]  = total_credit
      @data[:revenue_entries][:total_net]     = total_net

      ### BUILD MAJOR ACCOUNTS
      @data[:revenue_entries][:major_account_entries]  = []
      @revenue_group.major_accounts.each do |major_account|
        major_account_accounting_codes  = @revenue_accounting_codes.where("major_accounts.id = ?", major_account.id)
        major_account_total_debit       = @journal_entries.where(
                                            "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,
                                            @as_of
                                          ).sum(:amount)
        major_account_total_credit      = @journal_entries.where(
                                            "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,
                                            @as_of
                                          ).sum(:amount)
        major_account_total_net         = major_account_total_credit - major_account_total_debit

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s,
          total_debit: major_account_total_debit,
          total_credit: major_account_total_credit,
          total_net:  major_account_total_net
        }

        major_account_data[:mother_accounting_code_entries] = []
        major_account.mother_accounting_codes.each do |mother_accounting_code|
          ### BUILD MOTHER ACCOUNTING CODES
          mother_accounting_code_accounting_codes = @revenue_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)
          mother_accounting_code_total_debit      = @journal_entries.where(
                                                      "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,
                                                      @as_of
                                                    ).sum(:amount)
          mother_accounting_code_total_credit     = @journal_entries.where(
                                                      "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,
                                                      @as_of
                                                    ).sum(:amount)

          mother_accounting_code_total_net        = mother_accounting_code_total_credit  - mother_accounting_code_total_debit

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s,
            total_debit:  mother_accounting_code_total_debit,
            total_credit: mother_accounting_code_total_credit,
            total_net: mother_accounting_code_total_net
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            ### BUILD ACCOUNTING CODE CATEGORY
            accounting_code_category_accounting_codes = @revenue_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id) 
            accounting_code_category_total_debit      = @journal_entries.where(
                                                          "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) ANd vouchers.date_prepared <= ?",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,
                                                          @as_of
                                                        ).sum(:amount)
            accounting_code_category_total_credit     = @journal_entries.where(
                                                          "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,
                                                          @as_of
                                                        ).sum(:amount)

            accounting_code_category_total_net        = accounting_code_category_total_credit  - accounting_code_category_total_debit

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
              total_debit: accounting_code_category_total_debit,
              total_credit: accounting_code_category_total_credit,
              total_net: accounting_code_category_total_net
            }

            accounting_code_category_data[:accounting_code_entries] = []
            accounting_code_category_accounting_codes.each do |accounting_code|
              ### BUILD ACCOUNTING CODE
              accounting_code_total_debit     = @journal_entries.where(
                                                  "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id = ? AND vouchers.date_prepared <= ?",
                                                  accounting_code.id,
                                                  @as_of
                                                ).sum(:amount)
              accounting_code_total_credit    = @journal_entries.where(
                                                  "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id = ? ANd vouchers.date_prepared <= ?",
                                                  accounting_code.id,
                                                  @as_of
                                                ).sum(:amount)
              accounting_code_total_net       = accounting_code_total_credit - accounting_code_total_debit
              accounting_code_data = {
                id: accounting_code.id,
                name: accounting_code.to_s,
                total_debit: accounting_code_total_debit,
                total_credit: accounting_code_total_credit,
                total_net: accounting_code_total_net
              }

              if accounting_code_data[:total_net] != 0
                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            end

            if accounting_code_category_data[:total_net] != 0
              mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
            end
          end

          if mother_accounting_code_data[:total_net] != 0
            major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
          end
        end

        if major_account_data[:total_net] != 0
          @data[:revenue_entries][:major_account_entries] << major_account_data
        end
      end
    end

    def build_equity!
      total_debit   = @journal_entries.where(
                        "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                        @equity_accounting_codes.pluck("accounting_codes.id").uniq,
                        @as_of
                      ).sum(:amount)

      total_credit  = @journal_entries.where(
                        "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                        @equity_accounting_codes.pluck("accounting_codes.id").uniq,
                        @as_of
                      ).sum(:amount)

      total_net     = total_credit - total_debit

      @data[:equity_entries][:total_debit]   = total_debit
      @data[:equity_entries][:total_credit]  = total_credit
      @data[:equity_entries][:total_net]     = total_net

      ### BUILD MAJOR ACCOUNTS
      @data[:equity_entries][:major_account_entries]  = []
      @equity_group.major_accounts.each do |major_account|
        major_account_accounting_codes  = @equity_accounting_codes.where("major_accounts.id = ?", major_account.id)
        major_account_total_debit       = @journal_entries.where(
                                            "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,
                                            @as_of
                                          ).sum(:amount)
        major_account_total_credit      = @journal_entries.where(
                                            "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,
                                            @as_of
                                          ).sum(:amount)
        major_account_total_net         = major_account_total_credit - major_account_total_debit

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s,
          total_debit: major_account_total_debit,
          total_credit: major_account_total_credit,
          total_net:  major_account_total_net
        }

        major_account_data[:mother_accounting_code_entries] = []
        major_account.mother_accounting_codes.each do |mother_accounting_code|
          ### BUILD MOTHER ACCOUNTING CODES
          mother_accounting_code_accounting_codes = @equity_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)
          mother_accounting_code_total_debit      = @journal_entries.where(
                                                      "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,
                                                      @as_of
                                                    ).sum(:amount)
          mother_accounting_code_total_credit     = @journal_entries.where(
                                                      "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,
                                                      @as_of
                                                    ).sum(:amount)

          mother_accounting_code_total_net        = mother_accounting_code_total_credit  - mother_accounting_code_total_debit

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s,
            total_debit:  mother_accounting_code_total_debit,
            total_credit: mother_accounting_code_total_credit,
            total_net: mother_accounting_code_total_net
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            ### BUILD ACCOUNTING CODE CATEGORY
            accounting_code_category_accounting_codes = @equity_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id) 
            accounting_code_category_total_debit      = @journal_entries.where(
                                                          "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,
                                                          @as_of
                                                        ).sum(:amount)
            accounting_code_category_total_credit     = @journal_entries.where(
                                                          "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,
                                                          @as_of
                                                        ).sum(:amount)

            accounting_code_category_total_net        = accounting_code_category_total_credit  - accounting_code_category_total_debit

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
              total_debit: accounting_code_category_total_debit,
              total_credit: accounting_code_category_total_credit,
              total_net: accounting_code_category_total_net
            }

            accounting_code_category_data[:accounting_code_entries] = []
            accounting_code_category_accounting_codes.each do |accounting_code|
              ### BUILD ACCOUNTING CODE
              accounting_code_total_debit     = @journal_entries.where(
                                                  "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id = ? AND vouchers.date_prepared <= ?",
                                                  accounting_code.id,
                                                  @as_of
                                                ).sum(:amount)
              accounting_code_total_credit    = @journal_entries.where(
                                                  "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id = ? AND vouchers.date_prepared <= ?",
                                                  accounting_code.id,
                                                  @as_of
                                                ).sum(:amount)
              accounting_code_total_net       = accounting_code_total_credit - accounting_code_total_debit
              accounting_code_data = {
                id: accounting_code.id,
                name: accounting_code.to_s,
                total_debit: accounting_code_total_debit,
                total_credit: accounting_code_total_credit,
                total_net: accounting_code_total_net
              }

              if accounting_code_data[:total_net] != 0
                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            end

            if accounting_code_category_data[:total_net] != 0
              mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
            end
          end

          if mother_accounting_code_data[:total_net] != 0
            major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
          end
        end

        if major_account_data[:total_net] != 0
          @data[:equity_entries][:major_account_entries] << major_account_data
        end
      end
    end

    def build_liabilities!
      total_debit   = @journal_entries.where(
                        "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?)",
                        @liabilities_accounting_codes.pluck("accounting_codes.id").uniq
                        #@start_date,
                        #@end_date
                      ).sum(:amount)

      total_credit  = @journal_entries.where(
                        "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?)",
                        @liabilities_accounting_codes.pluck("accounting_codes.id").uniq
                        #@start_date,
                        #@end_date
                      ).sum(:amount)

      total_net     = total_credit - total_debit

      @data[:liabilities_entries][:total_debit]   = total_debit
      @data[:liabilities_entries][:total_credit]  = total_credit
      @data[:liabilities_entries][:total_net]     = total_net

      ### BUILD MAJOR ACCOUNTS
      @data[:liabilities_entries][:major_account_entries]  = []
      @liabilities_group.major_accounts.each do |major_account|
        major_account_accounting_codes  = @liabilities_accounting_codes.where("major_accounts.id = ?", major_account.id)
        major_account_total_debit       = @journal_entries.where(
                                            "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?)",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq
                                            #@start_date,
                                            #@end_date
                                          ).sum(:amount)

        major_account_total_credit      = @journal_entries.where(
                                            "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?)",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq
                                            #@start_date,
                                            #@end_date
                                          ).sum(:amount)

        major_account_total_net         = major_account_total_credit - major_account_total_debit

        major_account_data = {
          id: major_account.id,
          name: "#{major_account.to_s}",
          total_debit: major_account_total_debit,
          total_credit: major_account_total_credit,
          total_net:  major_account_total_net
        }

        major_account_data[:mother_accounting_code_entries] = []
        major_account.mother_accounting_codes.each do |mother_accounting_code|
          ### BUILD MOTHER ACCOUNTING CODES
          mother_accounting_code_accounting_codes = @liabilities_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_total_debit      = @journal_entries.where(
                                                      "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?)",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq
                                                      #@start_date,
                                                      #@end_date
                                                    ).sum(:amount)

          mother_accounting_code_total_credit     = @journal_entries.where(
                                                      "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared >= ? and vouchers.date_prepared <= ?",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,
                                                      @start_date,
                                                      @end_date
                                                    ).sum(:amount)

          mother_accounting_code_total_net        = mother_accounting_code_total_credit  - mother_accounting_code_total_debit

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: "#{mother_accounting_code.to_s}",
            total_debit:  mother_accounting_code_total_debit,
            total_credit: mother_accounting_code_total_credit,
            total_net: mother_accounting_code_total_net
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            ### BUILD ACCOUNTING CODE CATEGORY
            accounting_code_category_accounting_codes = @liabilities_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id) 

            accounting_code_category_total_debit      = JournalEntry.joins(:voucher).where(
                                                          "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?)",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq
                                                          #@start_date,
                                                          #@end_date
                                                        ).sum(:amount)

            

            accounting_code_category_total_credit     = JournalEntry.joins(:voucher).where(
                                                          "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?)",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq
                                                          #@start_date,
                                                          #@end_date
                                                        ).sum(:amount)

            

            accounting_code_category_total_net        = accounting_code_category_total_credit  - accounting_code_category_total_debit

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
              total_debit: accounting_code_category_total_debit,
              total_credit: accounting_code_category_total_credit,
              total_net: accounting_code_category_total_net
            }

            accounting_code_category_data[:accounting_code_entries] = []
            accounting_code_category_accounting_codes.each do |accounting_code|
              ### BUILD ACCOUNTING CODE
              #accounting_code_total_debit     = @journal_entries.where(
              #                                    "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id = ? AND vouchers.date_prepared between ? and ?",
               #                                   accounting_code.id,
                #                                  @start_date,
                #                                  @end_date
                #                                ).sum(:amount)

                accounting_code_total_debit   = JournalEntry.joins(:voucher).where(
                                                  "vouchers.status = ? AND journal_entries.post_type = ? AND journal_entries.accounting_code_id = ?", 
                                                  'approved', 
                                                  'DR', 
                                                  accounting_code.id
                                                ).sum(:amount)
                                              

                                                

              #accounting_code_total_credit    = @journal_entries.where(
               #                                   "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id = ? AND vouchers.date_prepared between ? and ?",
                #                                  accounting_code.id,
                #                                  @start_date,
                #                                  @end_date
                #                                ).sum(:amount)

                accounting_code_total_credit   =  JournalEntry.joins(:voucher).where(
                                                    "vouchers.status = ? AND journal_entries.post_type = ? AND journal_entries.accounting_code_id = ?",
                                                    'approved', 
                                                    'CR',
                                                    accounting_code.id
                                                  ).sum(:amount)
               
               #if accounting_code.id == 102
               # raise accounting_code_total_debit.to_f.inspect
               #end
              
              accounting_code_total_net       = accounting_code_total_credit - accounting_code_total_debit
              
              #if accounting_code.id == 102
              #  raise accounting_code_total_net.to_f.inspect
              #end


              accounting_code_data = {
                id: accounting_code.id,
                name: accounting_code.to_s,
                total_debit: accounting_code_total_debit,
                total_credit: accounting_code_total_credit,
                total_net: accounting_code_total_net
              }

              if accounting_code_data[:total_net] != 0
                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            end

            if accounting_code_category_data[:total_net] != 0
              mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
            end
          end

          if mother_accounting_code_data[:total_net] != 0
            major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
          end
        end

        if major_account_data[:total_net] != 0
          @data[:liabilities_entries][:major_account_entries] << major_account_data
        end
      end
    end

    def build_assets!
      total_debit   = @journal_entries.where(
                        "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                        @assets_accounting_codes.pluck("accounting_codes.id").uniq,
                        @as_of
                      ).sum(:amount)

      total_credit  = @journal_entries.where(
                        "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                        @assets_accounting_codes.pluck("accounting_codes.id").uniq,
                        @as_of
                      ).sum(:amount)

      total_net     = total_debit - total_credit

      @data[:assets_entries][:total_debit]   = total_debit
      @data[:assets_entries][:total_credit]  = total_credit
      @data[:assets_entries][:total_net]     = total_net

      ### BUILD MAJOR ACCOUNTS
      @data[:assets_entries][:major_account_entries]  = []
      @assets_group.major_accounts.each do |major_account|
        major_account_accounting_codes  = @assets_accounting_codes.where("major_accounts.id = ?", major_account.id)
        major_account_total_debit       = @journal_entries.where(
                                            "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,
                                            @as_of
                                          ).sum(:amount)

        major_account_total_credit      = @journal_entries.where(
                                            "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                            major_account_accounting_codes.pluck("accounting_codes.id").uniq,
                                            @as_of
                                          ).sum(:amount)

        major_account_total_net         = major_account_total_debit - major_account_total_credit

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s,
          total_debit: major_account_total_debit,
          total_credit: major_account_total_credit,
          total_net:  major_account_total_net
        }

        major_account_data[:mother_accounting_code_entries] = []
        major_account.mother_accounting_codes.each do |mother_accounting_code|
          ### BUILD MOTHER ACCOUNTING CODES
          mother_accounting_code_accounting_codes = @assets_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_total_debit      = @journal_entries.where(
                                                      "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,
                                                      @as_of
                                                    ).sum(:amount)

          mother_accounting_code_total_credit     = @journal_entries.where(
                                                      "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                      mother_accounting_code_accounting_codes.pluck("accounting_codes.id").uniq,
                                                      @as_of
                                                    ).sum(:amount)

          mother_accounting_code_total_net        = mother_accounting_code_total_debit  - mother_accounting_code_total_credit

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s,
            total_debit:  mother_accounting_code_total_debit,
            total_credit: mother_accounting_code_total_credit,
            total_net: mother_accounting_code_total_net
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            ### BUILD ACCOUNTING CODE CATEGORY
            accounting_code_category_accounting_codes = @assets_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id) 

            accounting_code_category_total_debit      = @journal_entries.where(
                                                          "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,
                                                          @as_of
                                                        ).sum(:amount)

            accounting_code_category_total_credit     = @journal_entries.where(
                                                          "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id IN (?) AND vouchers.date_prepared <= ?",
                                                          accounting_code_category_accounting_codes.pluck("accounting_codes.id").uniq,
                                                          @as_of
                                                        ).sum(:amount)

            accounting_code_category_total_net        = accounting_code_category_total_debit  - accounting_code_category_total_credit

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
              total_debit: accounting_code_category_total_debit,
              total_credit: accounting_code_category_total_credit,
              total_net: accounting_code_category_total_net
            }

            accounting_code_category_data[:accounting_code_entries] = []
            accounting_code_category_accounting_codes.each do |accounting_code|
              ### BUILD ACCOUNTING CODE
              accounting_code_total_debit     = @journal_entries.where(
                                                  "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id = ? AND vouchers.date_prepared <= ?",
                                                  accounting_code.id,
                                                  @as_of
                                                ).sum(:amount)

              accounting_code_total_credit    = @journal_entries.where(
                                                  "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id = ? AND vouchers.date_prepared <= ?",
                                                  accounting_code.id,
                                                  @as_of
                                                ).sum(:amount)
              accounting_code_total_net       = accounting_code_total_debit - accounting_code_total_credit
              accounting_code_data = {
                id: accounting_code.id,
                name: accounting_code.to_s,
                total_debit: accounting_code_total_debit,
                total_credit: accounting_code_total_credit,
                total_net: accounting_code_total_net
              }

              if accounting_code_data[:total_net] != 0
                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            end

            if accounting_code_category_data[:total_net] != 0
              mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
            end
          end

          if mother_accounting_code_data[:total_net] != 0
            major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
          end
        end

        if major_account_data[:total_net] != 0
          @data[:assets_entries][:major_account_entries] << major_account_data
        end
      end
    end
  end
end
