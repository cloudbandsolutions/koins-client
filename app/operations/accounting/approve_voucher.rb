module Accounting
  class ApproveVoucher
    require 'application_helper'

    def initialize(voucher:, user:)
      @voucher = voucher
      @user = user
    end

    def execute!
      if @voucher.status != "pending"
        raise "Cannot approve non-pending accounting entry"
      end

      c_working_date     = ApplicationHelper.current_working_date

      if !@voucher.date_prepared.nil?
        c_working_date = @voucher.date_prepared
      end
      
      reference_number  = Vouchers::VoucherNumber.new(book: @voucher.book, branch: @voucher.branch).execute!

      sub_reference_number = ""
      if @voucher.accounting_fund.present?
        sub_reference_number  = ::Accounting::GenerateSubReferenceNumber.new(
                                  accounting_fund: @voucher.accounting_fund,
                                  book: @voucher.book
                                ).execute!
      end

      @voucher.update(
        status: "approved", 
        reference_number: reference_number, 
        approved_by: @user, 
        sub_reference_number: sub_reference_number,
        date_posted: c_working_date,
        date_prepared: c_working_date
      )

      @voucher
    end
  end
end
