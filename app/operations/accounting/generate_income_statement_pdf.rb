module Accounting
  class GenerateIncomeStatementPdf
    def initialize
      @expenses_group_id          = Settings.expenses_group_id
      @expense_group              = MajorGroup.find(@expenses_group_id)
      @date_prepared              = ApplicationHelper.current_working_date.strftime("%b %d, %Y") 
      @revenue_group_id           = Settings.revenue_group_id
      @revenue_group              = MajorGroup.find(@revenue_group_id)
      @data                       = {}
      @data[:company]             = Settings.company
      @data[:address]             = Settings.company_address
      @data[:revenue_group]       = @revenue_group 
      @data[:date_prepared]       = @date_prepared    
      @data[:expense_entries]     = {}
      @data[:total_acc_revenue]   = 0

    end

    def execute!
      build_revenue!
      build_expense!
      
      @data[:total_net_income] = @data[:total_revenue] - @data[:total_expense]

      @data
    end

    def build_revenue!
      @data[:revenue] = []
      MajorGroup.where("major_groups.id = ?", @revenue_group_id).each do |rev_major_grp|
      rev_major_grp_data                   = {}
      rev_major_grp_data[:name]            = 'REVENUE'
      rev_major_grp_data[:major_accounts]  = []

      rev_major_grp_debit = JournalEntry.joins(:voucher, :accounting_code => [:accounting_code_category => [:mother_accounting_code => :major_account]]).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND major_accounts.major_group_id =?", rev_major_grp.id).sum("journal_entries.amount")

      rev_major_grp_credit = JournalEntry.joins(:voucher, :accounting_code => [:accounting_code_category => [:mother_accounting_code => :major_account]]).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.is_year_end_closing IS NULL AND major_accounts.major_group_id =?", rev_major_grp.id).sum("journal_entries.amount")

      @data[:total_revenue] = rev_major_grp_credit - rev_major_grp_debit
      rev_major_grp_data[:total_data] = 'TOTAL'
      
        rev_major_grp.major_accounts.each do |rev_major_acc|
          rev_major_acc_data                            = {}
          rev_major_acc_data[:id]                       = rev_major_acc.id
          rev_major_acc_data[:mother_accounting_codes]  = []
          
          rev_major_acc_debit = JournalEntry.joins(:voucher, :accounting_code => [:accounting_code_category => :mother_accounting_code]).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND mother_accounting_codes.major_account_id =?", rev_major_acc.id).sum("journal_entries.amount")

          rev_major_acc_credit = JournalEntry.joins(:voucher, :accounting_code => [:accounting_code_category => :mother_accounting_code]).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.is_year_end_closing IS NULL AND mother_accounting_codes.major_account_id =?", rev_major_acc.id).sum("journal_entries.amount")

          rev_major_acc_total = rev_major_acc_credit - rev_major_acc_debit

          if rev_major_acc_total != 0
            rev_major_acc_data[:name]       = rev_major_acc.name
            rev_major_acc_data[:total]      = rev_major_acc_total
            rev_major_acc_data[:total_data] = 'TOTAL'
          else

          end

            rev_major_acc.mother_accounting_codes.each do |rev_mother_acc|
              rev_mother_acc_data = {}
              rev_mother_acc_data[:id] = rev_mother_acc.id
              rev_mother_acc_data[:accounting_code_categories] = []

              rev_mother_acc_debit = JournalEntry.joins(:voucher, :accounting_code =>:accounting_code_category).where("vouchers.status ='approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND accounting_code_categories.mother_accounting_code_id =?", rev_mother_acc.id).sum("journal_entries.amount")

              rev_mother_acc_credit = JournalEntry.joins(:voucher, :accounting_code =>:accounting_code_category).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.is_year_end_closing IS NULL AND accounting_code_categories.mother_accounting_code_id=?", rev_mother_acc.id).sum("journal_entries.amount")

              rev_mother_acc_total = rev_mother_acc_credit - rev_mother_acc_debit
              
              if rev_mother_acc_total != 0
                rev_mother_acc_data[:total]      = rev_mother_acc_total
                rev_mother_acc_data[:name]       = rev_mother_acc.name
                rev_mother_acc_data[:total_data] = 'TOTAL'
              else

              end
                
              rev_mother_acc.accounting_code_categories.each do |rev_acc_code_cat|
                  rev_acc_code_cat_data                    = {}
                  rev_acc_code_cat_data[:id]               = rev_acc_code_cat.id
                  rev_acc_code_cat_data[:accounting_codes] = []

                  rev_acc_code_debit = JournalEntry.joins(:voucher, :accounting_code).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND accounting_codes.accounting_code_category_id = ?", rev_acc_code_cat.id).sum("journal_entries.amount")

                  rev_acc_code_credit = JournalEntry.joins(:voucher, :accounting_code).where("vouchers.status = 'approved' AND post_type= 'CR' AND vouchers.is_year_end_closing IS NULL AND accounting_codes.accounting_code_category_id = ?", rev_acc_code_cat.id).sum("journal_entries.amount")

                  rev_acc_code_total = rev_acc_code_credit - rev_acc_code_debit

                  if rev_acc_code_total != 0
                    rev_acc_code_cat_data[:name]  = rev_acc_code_cat.name
                    rev_acc_code_cat_data[:total] = rev_acc_code_total
                    rev_acc_code_cat_data[:total_data] = 'TOTAL'
                  else
                  
                  end
                  
                  rev_acc_code_cat.accounting_codes.each do |rev_acc_code|
                    rev_acc_code_data = {}

                    rev_debit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND journal_entries.accounting_code_id = ?", rev_acc_code.id).sum("journal_entries.amount")

                    rev_credit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.is_year_end_closing IS NULL AND journal_entries.accounting_code_id = ?", rev_acc_code.id).sum("journal_entries.amount")

                    rev_acc_code_data[:rev_total_data] = rev_credit - rev_debit

                    if rev_acc_code_data[:rev_total_data] !=  0
                      rev_acc_code_data[:name] = rev_acc_code.name
                      rev_acc_code_data[:rev_total] = rev_acc_code_data[:rev_total_data]
                      rev_acc_code_data[:code] = rev_acc_code.code
                    else
                    
                    end
                  rev_acc_code_cat_data[:accounting_codes] << rev_acc_code_data
                  end

              rev_mother_acc_data[:accounting_code_categories] << rev_acc_code_cat_data
              end


            rev_major_acc_data[:mother_accounting_codes] << rev_mother_acc_data
            end
        
        rev_major_grp_data[:major_accounts] << rev_major_acc_data
        end

      @data[:revenue] << rev_major_grp_data
      end
    end

    def build_expense!
      @data[:expense] = []
      MajorGroup.where("major_groups.id = ?", @expenses_group_id).each do |exp_major_grp|
      exp_major_grp_data                     = {}
      exp_major_grp_data[:name]              = exp_major_grp.name
      exp_major_grp_data[:id]                = exp_major_grp.id
      exp_major_grp_data[:major_accounts]    = []
        
        major_grp_debit = JournalEntry.joins(:voucher, :accounting_code => [:accounting_code_category => [:mother_accounting_code =>:major_account]]).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND major_accounts.major_group_id =?", exp_major_grp.id).sum("journal_entries.amount")

        major_grp_credit = JournalEntry.joins(:voucher, :accounting_code => [:accounting_code_category => [:mother_accounting_code =>:major_account]]).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.is_year_end_closing IS NULL AND major_accounts.major_group_id =?", exp_major_grp.id).sum("journal_entries.amount")
        
        @data[:total_expense] = major_grp_debit - major_grp_credit
        exp_major_grp_data[:total_data] = 'TOTAL'

        exp_major_grp.major_accounts.each do |exp_major_acc|
          exp_major_acc_data                            = {}
          exp_major_acc_data[:id]                       = exp_major_acc.id
          exp_major_acc_data[:mother_accounting_codes]  = []
          
          major_acc_debit = JournalEntry.joins(:voucher, :accounting_code => [:accounting_code_category => :mother_accounting_code]).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND mother_accounting_codes.major_account_id =?", exp_major_acc.id).sum("journal_entries.amount")
          
          major_acc_credit = JournalEntry.joins(:voucher, :accounting_code => [:accounting_code_category => :mother_accounting_code]).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.is_year_end_closing IS NULL AND mother_accounting_codes.major_account_id =?", exp_major_acc.id).sum("journal_entries.amount")

          major_acc_total = major_acc_debit - major_acc_credit

          if major_acc_total != 0
            exp_major_acc_data[:name]  = exp_major_acc.name
            exp_major_acc_data[:total] = major_acc_total
            exp_major_acc_data[:total_data] = 'TOTAL'
          else

          end



            exp_major_acc.mother_accounting_codes.each do |exp_mother_acc|
              exp_mother_acc_data = {}
              exp_mother_acc_data[:id] = exp_mother_acc.id

              mother_acc_debit = JournalEntry.joins(:voucher, :accounting_code =>:accounting_code_category).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND accounting_code_categories.mother_accounting_code_id =?", exp_mother_acc.id).sum("journal_entries.amount")

              mother_acc_credit = JournalEntry.joins(:voucher, :accounting_code =>:accounting_code_category).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.is_year_end_closing IS NULL AND accounting_code_categories.mother_accounting_code_id =?", exp_mother_acc.id).sum("journal_entries.amount")
              
              mother_acc_total = mother_acc_debit - mother_acc_credit
              if mother_acc_total != 0
                exp_mother_acc_data[:total] = mother_acc_total
                exp_mother_acc_data[:name] = exp_mother_acc.name
                exp_mother_acc_data[:total_data] = 'TOTAL'
              else
              
              end
               
              exp_mother_acc_data[:accounting_code_categories] = []
              

                exp_mother_acc.accounting_code_categories.each do |acc_code_cat|
                  acc_code_cat_data         = {}
                  acc_code_cat_data[:id]    = acc_code_cat.id
                  acc_code_cat_data[:accounting_codes] = []

                  acc_code_debit = JournalEntry.joins(:voucher, :accounting_code).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND accounting_codes.accounting_code_category_id = ?", acc_code_cat.id).sum("journal_entries.amount")

                  acc_code_credit = JournalEntry.joins(:voucher, :accounting_code).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.is_year_end_closing IS NULL AND accounting_codes.accounting_code_category_id = ?", acc_code_cat.id).sum("journal_entries.amount")


                  acc_code_total = acc_code_debit - acc_code_credit
                  
                  if acc_code_total != 0  
                    acc_code_cat_data[:name]  = acc_code_cat.name
                    acc_code_cat_data[:total] = acc_code_total
                    acc_code_cat_data[:total_data] = 'TOTAL'
                  else
                  
                  end

                  acc_code_cat.accounting_codes.each do |exp_acc_code|
                    exp_acc_code_data = {}
                    
                    exp_debit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.is_year_end_closing IS NULL AND journal_entries.accounting_code_id = ?", exp_acc_code.id).sum("journal_entries.amount")

                    exp_credit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.is_year_end_closing IS NULL AND journal_entries.accounting_code_id = ?", exp_acc_code.id).sum("journal_entries.amount")

                    exp_acc_code_data[:exp_total_data] = exp_debit - exp_credit

                    if exp_acc_code_data[:exp_total_data] !=  0
                      exp_acc_code_data[:name] = exp_acc_code.name
                      exp_acc_code_data[:exp_total] = exp_acc_code_data[:exp_total_data]
                      exp_acc_code_data[:code] = exp_acc_code.code
                    else
                      

                    end

                    acc_code_cat_data[:accounting_codes] << exp_acc_code_data
                  end

                  exp_mother_acc_data[:accounting_code_categories] << acc_code_cat_data
                end
            
              exp_major_acc_data[:mother_accounting_codes] << exp_mother_acc_data
            end

          exp_major_grp_data[:major_accounts] << exp_major_acc_data
        end
      
      @data[:expense] << exp_major_grp_data
      end
    
    end

    
  end

end

