module Accounting
  class GeneratePatronageRefund
    def initialize(patronage_rate:, as_of:)
      year = as_of.to_date.year
      
      start_month = "#{year}-01-01"
      @start_month_details = start_month.to_date
    
     


      @patronage_rate = (patronage_rate.to_f / 100)
      @as_of = as_of.to_date
      @generate_year = @as_of.year
      @member = Member.where("status = ? or status = ?", "active", "resigned")
      #@member = Member.where("status <> ?" ,"archived")
      @data = {} 
      @data[:interest_details] = []
      @data[:total_loan_interest] = 0
      @data[:total_loan_interest_rate] = 0
      
    end
    def execute!
      total_loan_interest_amount = 0
      total_loan_interest_rate = 0

      dtotal_savings = 0
      dtotal_cbu = 0
      @member.each do |m|
        
   #     lpactive =  LoanPayment.joins(:loan).where(is_void:  nil, , loans:{member_id: m.id} )
   #     lpactive.each do |lp|
          #loan_interest_payment = LoanPayment.joins(:loan).where("extract(year from paid_at) = ? and loans.member_id = ? ",@generate_year,lp.id).sum(:paid_interest)
          
         # loan_interest_payment = LoanPayment.joins(:loan).where("extract(year from paid_at) = ? and loan_payments.paid_interest > 0 and loan_payments.is_void IS NULL  and loans.member_id = ? and loan_payments.status =? ",@generate_year,m.id,"approved").sum(:paid_interest)
          
          loan_interest_payment = LoanPayment.joins(:loan).where("extract(year from paid_at) = ? and loan_payments.status =? and loans.member_id = ? ",@generate_year,"approved",m.id).sum(:paid_interest)

          
          member_savings_account_id = SavingsAccount.where(savings_type_id: 1, member_id: m.id).ids
          tmp = {}
          #tmp[:total_per_month] = []
      
          tmp[:member_id] = m.id
          tmp[:member_name] = m.full_name
          tmp[:member_loan_interest_amount] = loan_interest_payment
          tmp[:g_member_loan_interest_rate] = (loan_interest_payment * @patronage_rate)
          #tmp[:member_savings_account_id] = member_savings_account_id
          tmp[:dtotal_savings] = (tmp[:g_member_loan_interest_rate] * 0.9).round(2)
          tmp[:dtotal_cbu] = (tmp[:g_member_loan_interest_rate] * 0.1).round(2)
          total_loan_interest_amount += (loan_interest_payment).round(2)
          sum_savings_and_cbu = (tmp[:dtotal_savings] + tmp[:dtotal_cbu]).round(2)
          if sum_savings_and_cbu > tmp[:g_member_loan_interest_rate]
            tmp[:member_loan_interest_rate] = sum_savings_and_cbu
          else
            tmp[:member_loan_interest_rate] = tmp[:g_member_loan_interest_rate]
          end
          
          total_loan_interest_rate += (tmp[:member_loan_interest_rate]).round(2)
        
          tmp[:reasons]           = []
        
          start_date_details = @start_month_details
          member_total_interest = 0
          while(start_date_details <= @as_of ) do
            dep = {}
            loan_interest_payment = LoanPayment.joins(:loan).where(
                                                                  "extract(month from paid_at) = ? and 
                                                                   extract(year from paid_at) = ? and 
                                                                   loans.member_id = ? and 
                                                                   loan_payments.status = ?",
                                                                   start_date_details.month,
                                                                   @generate_year,
                                                                   m.id,
                                                                   "approved"
                                                                ).sum(:paid_interest)
            member_total_interest += loan_interest_payment
            dep[:month] = Date::MONTHNAMES[start_date_details.month]
            dep[:amount] = loan_interest_payment
            #tmp[:reasons] << dep
            start_date_details = start_date_details + 1.month
            tmp[:reasons] << dep
        
          end #end of while
          dtotal_savings = tmp[:dtotal_savings] #(member_total_interest * 0.9).round(2)
          dtotal_cbu = tmp[:dtotal_cbu] #(member_total_interest * 0.1).round(2)

          @data[:interest_details] << tmp
      
      #  end #lpactive
      end #end of member
      @data[:total_loan_interest] = total_loan_interest_amount
      @data[:total_loan_interest_rate] = total_loan_interest_rate
      @data
    end
  end
end
