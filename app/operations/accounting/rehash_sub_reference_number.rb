module Accounting
  class RehashSubReferenceNumber
    def initialize(accounting_fund:)
      @accounting_fund    = accounting_fund
      @accounting_entries = Voucher.approved.where(
                              accounting_fund_id: accounting_fund.id
                            ).order("reference_number ASC")
    end

    def execute!
      @accounting_entries.each do |voucher|
        sub_reference_number  = ::Accounting::GenerateSubReferenceNumber.new(
                                  accounting_fund: voucher.accounting_fund,
                                  book: voucher.book
                                ).execute!
        voucher.update(
          status: "approved", 
          sub_reference_number: sub_reference_number
        )
      end
    end
  end
end
