module Accounting
  class ProduceVoucherForInterestOnShareCapital
    def initialize(interest_on_share_capital_collection:, user:)
     @user = user
     @interest_on_share_capital_collection = interest_on_share_capital_collection
     @closing_date = ApplicationHelper.current_working_date
     @branch = Branch.where(id: Settings.branch_ids).first

    end
    def execute!
      
      @voucher = Voucher.new(
                            book: "JVB",
                            particular: "To record declaration of interest for Share capital.",
                            branch: @branch,
                            date_prepared: @closing_date
                          )      
      build_debit_entries!
      build_credit_for_savings_entries!
      build_debit_for_savings_entries!
      @voucher
    end


    private

    def build_debit_entries!
      accounting_code_debit = AccountingCode.find(430)
      
      debit_amount = @interest_on_share_capital_collection.total_interest_amount
      
      journal_entry = JournalEntry.new(
                          amount: debit_amount,
                          post_type: "DR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry


    end
    
    def build_credit_for_savings_entries!
      accounting_code_debit = AccountingCode.find(1890)
      credit_amount = @interest_on_share_capital_collection.cbu_account_amount
      journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "CR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry
   end


    def build_debit_for_savings_entries!
      accounting_code_debit = AccountingCode.find(102)
      credit_amount = @interest_on_share_capital_collection.savings_account_amount
      journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "CR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry
   end



  end
end
