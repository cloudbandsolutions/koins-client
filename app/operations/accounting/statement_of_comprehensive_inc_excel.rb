module Accounting
	class StatementOfComprehensiveIncExcel
		def initialize(data:, end_date:, start_date:, branch:, detailed:)
			@data     = data
      @start_date = start_date.to_date
      @end_date = end_date.to_date
      @branch  = branch
      @detailed = detailed
    end	

		def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
          label_cell = wb.styles.add_style b: true, font_name: "Calibri"
          currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right_bold = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
          percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
          left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
          underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
          header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
          date_format_cell = wb.styles.add_style format_code: "mm-dd-yyyy", font_name: "Calibri", alignment: { horizontal: :right }
          default_cell = wb.styles.add_style font_name: "Calibri"
          label_cell_underline = wb.styles.add_style b: true, font_name: "Calibri", u: true
          currency_cell_right_bold_underline = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true, u: true

          sheet.add_row ["", "", "#{Settings.company}"], style: title_cell
          sheet.add_row ["", "", "Statement of Comprehensive Inc"], style: title_cell
          sheet.add_row ["", "", "As of #{@end_date.strftime("%b %d, %Y")}"], style: title_cell
          sheet.add_row []
          sheet.column_widths 1, 50, 5, 5, 5, 5
          # sheet.add_row ["Normal\nBalance", "", "As of #{@end_date.strftime("%b %d, %Y")}", "", "", "", "As of #{(@end_date - 1.month).strftime("%b %d, %Y")}", "Variance"], style: title_cell
          sheet.add_row ["Normal\nBalance", "", "As of #{@end_date.strftime("%b %d, %Y")}", "", "", ""], style: title_cell
          sheet.add_row ["", "", "General Fund", "Mutual Benefit", "Optional Fund", "TOTAL", "", ""], style: title_cell            
          sheet.merge_cells("C5:F5")
          sheet.merge_cells("A5:A6")
          # sheet.merge_cells("G5:G6")
          # sheet.merge_cells("H5:H6")
          

          sheet.add_row ["", "2 - FUND BALANCE"], style: [nil, title_cell]
          @data[:income_entries].each do |income|
            @total_income_gen_fund = 0.00
            @total_income_mut_fund = 0.00
            @total_income_opt_fund = 0.00
            @total_income_fund = 0.00

            @data[:income_entries][:major_account_entries].each do |income_major_acc|
              @income_major_gen_amt = 0.00
              @income_major_mut_amt = 0.00
              @income_major_opt_amt = 0.00

              sheet.add_row ["CR", "#{income_major_acc[:name]}"], style: [nil, label_cell]
             
              if income_major_acc[:mother_accounting_code_entries].count > 0

                income_major_acc[:mother_accounting_code_entries].each do |income_mother_acc|
                  @income_mother_gen_amt = 0.00
                  @income_mother_mut_amt = 0.00
                  @income_mother_opt_amt = 0.00

                  # sheet.add_row ["CR", "#{fund_balance_mother_acc[:name]}"], style: [nil, label_cell]
                  
                  income_mother_acc[:accounting_code_category_entries].each do |income_acc_code_cat|
                    # sheet.add_row ["CR", "#{fund_balance_acc_code_cat[:name]}"], style: [nil, label_cell]

                    income_acc_code_cat[:accounting_code_entries].each do |income_acc_code|

                      if income_acc_code[:m_ending_debit] > 0
                        @income_mutual_amount = income_acc_code[:m_ending_debit]
                      elsif income_acc_code[:m_ending_credit] > 0
                        @income_mutual_amount = income_acc_code[:m_ending_credit] * -1
                      elsif income_acc_code[:m_ending_credit] = 0
                        @income_mutual_amount = 0.00  
                      end 

                      if income_acc_code[:g_ending_debit] > 0
                        @income_gen_amount = income_acc_code[:g_ending_debit]
                      elsif income_acc_code[:g_ending_credit] > 0
                        @income_gen_amount = income_acc_code[:g_ending_credit] * -1
                      elsif income_acc_code[:g_ending_credit] = 0
                        @income_gen_amount = 0.00 
                      end 

                      if income_acc_code[:o_ending_debit] > 0
                        @income_opt_amount = income_acc_code[:o_ending_debit]
                      elsif income_acc_code[:o_ending_credit] > 0
                        @income_opt_amount = income_acc_code[:o_ending_credit] * -1
                      elsif income_acc_code[:o_ending_credit] = 0
                        @income_opt_amount = 0.00
                      end

                      @income_total = @income_mutual_amount.to_f + @income_gen_amount.to_f + @income_opt_amount.to_f

                      @total_income_gen_fund = @total_income_gen_fund + @income_gen_amount.to_f
                      @total_income_opt_fund = @total_income_opt_fund + @income_opt_amount.to_f
                      @total_income_mut_fund = @total_income_mut_fund + @income_mutual_amount.to_f
                      @total_income_fund = @total_income_fund + @income_total.to_f

                      @income_mother_gen_amt += @income_gen_amount.to_f
                      @income_mother_mut_amt += @income_mutual_amount.to_f
                      @income_mother_opt_amt += @income_opt_amount.to_f    

                      if @detailed == "true"
                        t_entry = []
                        t_entry << "CR"
                        t_entry << income_acc_code[:name]
                        t_entry << @income_gen_amount
                        t_entry << @income_mutual_amount
                        t_entry << @income_opt_amount
                        t_entry << @income_total
                        sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                      end 
                    end

                    @income_mother_gen_total_amt = @income_mother_gen_amt
                    @income_mother_mut_total_amt = @income_mother_mut_amt
                    @income_mother_opt_total_amt = @income_mother_opt_amt
                    @income_mother_total_amt = @income_mother_gen_amt + @income_mother_mut_amt + @income_mother_opt_amt

                    @income_major_gen_amt += @income_mother_gen_total_amt
                    @income_major_mut_amt += @income_mother_mut_total_amt
                    @income_major_opt_amt += @income_mother_opt_total_amt
                    
                  end
                  sheet.add_row ["", "#{income_mother_acc[:name]}", @income_mother_gen_total_amt, @income_mother_mut_total_amt, @income_mother_opt_total_amt, @income_mother_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                  @income_major_gen_total_amt = @income_major_gen_amt
                  @income_major_mut_total_amt = @income_major_mut_amt
                  @income_major_opt_total_amt = @income_major_opt_amt
                  @income_major_total_amt = @income_major_gen_amt + @income_major_mut_amt + @income_major_opt_amt 
                end
              else
                @income_major_gen_total_amt = 0.00
                @income_major_mut_total_amt = 0.00
                @income_major_opt_total_amt = 0.00
                @income_major_total_amt = 0.00  
              end
              sheet.add_row ["", "TOTAL #{income_major_acc[:name].upcase}", @income_major_gen_total_amt, @income_major_mut_total_amt, @income_major_opt_total_amt, @income_major_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
              
              if income_major_acc[:name] == "Revenue"
                @revenue_gen_total = @income_major_gen_total_amt
                @revenue_mut_total = @income_major_mut_total_amt
                @revenue_opt_total = @income_major_opt_total_amt
                @revenue_total = @income_major_total_amt
              end
            end
          end

            # para sa expenses

            @total_expenses_gen_fund = 0.00
            @total_expenses_mut_fund = 0.00
            @total_expenses_opt_fund = 0.00
            @total_expenses_fund = 0.00
            
            @data[:expenses_entries][:major_account_entries].each do |expenses_major_acc|
              @expenses_major_gen_amt = 0.00
              @expenses_major_mut_amt = 0.00
              @expenses_major_opt_amt = 0.00

              sheet.add_row ["CR", "#{expenses_major_acc[:name]}"], style: [nil, label_cell]
              
              expenses_major_acc[:mother_accounting_code_entries].each do |expenses_mother_acc|
                @expenses_mother_gen_amt = 0.00
                @expenses_mother_mut_amt = 0.00
                @expenses_mother_opt_amt = 0.00

                # sheet.add_row ["CR", "#{expenses_mother_acc_a[:name]} b"], style: [nil, label_cell]
                
                expenses_mother_acc[:accounting_code_category_entries].each do |expenses_acc_code_cat|
                  # sheet.add_row ["CR", "#{expenses_acc_code_cat_a[:name]} c"], style: [nil, label_cell]

                  expenses_acc_code_cat[:accounting_code_entries].each do |expenses_acc_code|

                    if expenses_acc_code[:m_ending_debit] > 0
                      @expenses_mutual_amount = expenses_acc_code[:m_ending_debit]
                    elsif expenses_acc_code[:m_ending_credit] > 0
                      @expenses_mutual_amount = expenses_acc_code[:m_ending_credit] * -1
                    elsif expenses_acc_code[:m_ending_credit] = 0
                      @expenses_mutual_amount = 0.00  
                    end 

                    if expenses_acc_code[:g_ending_debit] > 0
                      @expenses_gen_amount = expenses_acc_code[:g_ending_debit]
                    elsif expenses_acc_code[:g_ending_credit] > 0
                      @expenses_gen_amount = expenses_acc_code_a[:g_ending_credit] * -1
                    elsif expenses_acc_code[:g_ending_credit] = 0
                      @expenses_gen_amount = 0.00 
                    end 

                    if expenses_acc_code[:o_ending_debit] > 0
                      @expenses_opt_amount = expenses_acc_code[:o_ending_debit]
                    elsif expenses_acc_code[:o_ending_credit] > 0
                      @expenses_opt_amount = expenses_acc_code_a[:o_ending_credit] * -1
                    elsif expenses_acc_code[:o_ending_credit] = 0
                      @expenses_opt_amount = 0.00
                    end

                    @expenses_total = @expenses_mutual_amount.to_f + @expenses_gen_amount.to_f + @expenses_opt_amount.to_f

                    @total_expenses_gen_fund = @total_expenses_gen_fund + @expenses_gen_amount.to_f
                    @total_expenses_opt_fund = @total_expenses_opt_fund + @expenses_opt_amount.to_f
                    @total_expenses_mut_fund = @total_expenses_mut_fund + @expenses_mutual_amount.to_f
                    @total_expenses_fund = @total_expenses_fund + @expenses_total.to_f

                    @expenses_mother_gen_amt += @expenses_gen_amount.to_f
                    @expenses_mother_mut_amt += @expenses_mutual_amount.to_f
                    @expenses_mother_opt_amt += @expenses_opt_amount.to_f    

                    if @detailed == "true"
                      t_entry = []
                      t_entry << "CR"
                      t_entry << expenses_acc_code[:name]
                      t_entry << @expenses_gen_amount
                      t_entry << @expenses_mutual_amount
                      t_entry << @expenses_opt_amount
                      t_entry << @expenses_total
                      sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                    end   
                  end

                  @expenses_mother_gen_total_amt = @expenses_mother_gen_amt
                  @expenses_mother_mut_total_amt = @expenses_mother_mut_amt
                  @expenses_mother_opt_total_amt = @expenses_mother_opt_amt
                  @expenses_mother_total_amt = @expenses_mother_gen_amt + @expenses_mother_mut_amt + @expenses_mother_opt_amt

                  @expenses_major_gen_amt += @expenses_mother_gen_total_amt
                  @expenses_major_mut_amt += @expenses_mother_mut_total_amt
                  @expenses_major_opt_amt += @expenses_mother_opt_total_amt
                end
                sheet.add_row ["", "#{expenses_mother_acc[:name]}", @expenses_mother_gen_total_amt, @expenses_mother_mut_total_amt, @expenses_mother_opt_total_amt, @expenses_mother_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                @expenses_major_gen_total_amt = @expenses_major_gen_amt
                @expenses_major_mut_total_amt = @expenses_major_mut_amt
                @expenses_major_opt_total_amt = @expenses_major_opt_amt
                @expenses_major_total_amt = @expenses_major_gen_amt + @expenses_major_mut_amt + @expenses_major_opt_amt 
              end

              sheet.add_row ["", "TOTAL #{expenses_major_acc[:name].upcase}", @expenses_major_gen_total_amt, @expenses_major_mut_total_amt, @expenses_major_opt_total_amt, @expenses_major_total_amt], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

              if expenses_major_acc[:name] == "Benefit Expenses" 
                @net_surplus_before_operating_expense_major_gen_total = @revenue_gen_total + @expenses_major_gen_total_amt
                @net_surplus_before_operating_expense_major_mut_total = @revenue_mut_total + @expenses_major_mut_total_amt
                @net_surplus_before_operating_expense_major_opt_total = @revenue_opt_total + @expenses_major_opt_total_amt
                @net_surplus_before_operating_expense_major_total = @revenue_total + @expenses_major_total_amt
                
                sheet.add_row ["", "NET SURPLUS BEFORE OPERATING EXPENSE", @net_surplus_before_operating_expense_major_gen_total, @net_surplus_before_operating_expense_major_mut_total, @net_surplus_before_operating_expense_major_opt_total, @net_surplus_before_operating_expense_major_total], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

              elsif expenses_major_acc[:name] == "Operating Expenses"
                @net_surplus_before_non_operating_expense_major_gen_total = @net_surplus_before_operating_expense_major_gen_total + @expenses_major_gen_total_amt
                @net_surplus_before_non_operating_expense_major_mut_total = @net_surplus_before_operating_expense_major_mut_total + @expenses_major_mut_total_amt
                @net_surplus_before_non_operating_expense_major_opt_total = @net_surplus_before_operating_expense_major_opt_total + @expenses_major_opt_total_amt
                @net_surplus_before_non_operating_expense_major_total = @net_surplus_before_operating_expense_major_total + @expenses_major_total_amt
                
                sheet.add_row ["", "NET SURPLUS BEFORE NON OPERATING INCOME(EXPENSE)", @net_surplus_before_non_operating_expense_major_gen_total, @net_surplus_before_non_operating_expense_major_mut_total, @net_surplus_before_non_operating_expense_major_opt_total, @net_surplus_before_non_operating_expense_major_total], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
              end
              
            end

            @total_income_gen_fund_o = 0.00
            @total_income_mut_fund_o = 0.00
            @total_income_opt_fund_o = 0.00
            @total_income_fund_o = 0.00

            @data[:income_entries_o][:major_account_entries_o].each do |income_major_acc|
              @income_major_gen_amt_o = 0.00
              @income_major_mut_amt_o = 0.00
              @income_major_opt_amt_o = 0.00

              sheet.add_row ["CR", "#{income_major_acc[:name]}"], style: [nil, label_cell]
             
              if income_major_acc[:mother_accounting_code_entries_o].count > 0

                income_major_acc[:mother_accounting_code_entries_o].each do |income_mother_acc|
                  @income_mother_gen_amt_o = 0.00
                  @income_mother_mut_amt_o = 0.00
                  @income_mother_opt_amt_o = 0.00

                  # sheet.add_row ["CR", "#{income_mother_acc[:name]}"], style: [nil, label_cell]
                  
                  income_mother_acc[:accounting_code_category_entries_o].each do |income_acc_code_cat|
                    # sheet.add_row ["CR", "#{income_acc_code_cat[:name]}"], style: [nil, label_cell]

                    income_acc_code_cat[:accounting_code_entries_o].each do |income_acc_code|

                      if income_acc_code[:m_ending_debit] > 0
                        @income_mutual_amount_o = income_acc_code[:m_ending_debit]
                      elsif income_acc_code[:m_ending_credit] > 0
                        @income_mutual_amount_o = income_acc_code[:m_ending_credit] * -1
                      elsif income_acc_code[:m_ending_credit] = 0
                        @income_mutual_amount_o = 0.00  
                      end 

                      if income_acc_code[:g_ending_debit] > 0
                        @income_gen_amount_o = income_acc_code[:g_ending_debit]
                      elsif income_acc_code[:g_ending_credit] > 0
                        @income_gen_amount_o = income_acc_code[:g_ending_credit] * -1
                      elsif income_acc_code[:g_ending_credit] = 0
                        @income_gen_amount_o = 0.00 
                      end 

                      if income_acc_code[:o_ending_debit] > 0
                        @income_opt_amount_o = income_acc_code[:o_ending_debit]
                      elsif income_acc_code[:o_ending_credit] > 0
                        @income_opt_amount_o = income_acc_code[:o_ending_credit] * -1
                      elsif income_acc_code[:o_ending_credit] = 0
                        @income_opt_amount_o = 0.00
                      end

                      @income_total_o = @income_mutual_amount_o.to_f + @income_gen_amount_o.to_f + @income_opt_amount_o.to_f

                      @total_income_gen_fund_o = @total_income_gen_fund_o + @income_gen_amount_o.to_f
                      @total_income_opt_fund_o = @total_income_opt_fund_o + @income_opt_amount_o.to_f
                      @total_income_mut_fund_o = @total_income_mut_fund_o + @income_mutual_amount_o.to_f
                      @total_income_fund_o = @total_income_fund_o + @income_total_o.to_f

                      @income_mother_gen_amt_o += @income_gen_amount_o.to_f
                      @income_mother_mut_amt_o += @income_mutual_amount_o.to_f
                      @income_mother_opt_amt_o += @income_opt_amount_o.to_f    

                      if @detailed == "true"
                        t_entry = []
                        t_entry << "CR"
                        t_entry << income_acc_code[:name]
                        t_entry << @income_gen_amount_o
                        t_entry << @income_mutual_amount_o
                        t_entry << @income_opt_amount_o
                        t_entry << @income_total_o
                        sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                      end 
                    end

                    @income_mother_gen_total_amt_o = @income_mother_gen_amt_o
                    @income_mother_mut_total_amt_o = @income_mother_mut_amt_o
                    @income_mother_opt_total_amt_o = @income_mother_opt_amt_o
                    @income_mother_total_amt_o = @income_mother_gen_amt_o + @income_mother_mut_amt_o + @income_mother_opt_amt_o

                    @income_major_gen_amt_o += @income_mother_gen_total_amt_o
                    @income_major_mut_amt_o += @income_mother_mut_total_amt_o
                    @income_major_opt_amt_o += @income_mother_opt_total_amt_o
                    
                  end
                  sheet.add_row ["", "#{income_mother_acc[:name]}", @income_mother_gen_total_amt_o, @income_mother_mut_total_amt_o, @income_mother_opt_total_amt_o, @income_mother_total_amt_o], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                  @income_major_gen_total_amt_o = @income_major_gen_amt_o
                  @income_major_mut_total_amt_o = @income_major_mut_amt_o
                  @income_major_opt_total_amt_o = @income_major_opt_amt_o
                  @income_major_total_amt_o = @income_major_gen_amt_o + @income_major_mut_amt_o + @income_major_opt_amt_o 
                end
              else
                @income_major_gen_total_amt_o = 0.00
                @income_major_mut_total_amt_o = 0.00
                @income_major_opt_total_amt_o = 0.00
                @income_major_total_amt_o = 0.00  
              end
              sheet.add_row ["", "TOTAL #{income_major_acc[:name].upcase}", @income_major_gen_total_amt_o, @income_major_mut_total_amt_o, @income_major_opt_total_amt_o, @income_major_total_amt_o], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
            end


            @total_expenses_gen_fund_o = 0.00
            @total_expenses_mut_fund_o = 0.00
            @total_expenses_opt_fund_o = 0.00
            @total_expenses_fund_o = 0.00

            @data[:expenses_entries_o][:major_account_entries_o].each do |expenses_major_acc|
              @expenses_major_gen_amt_o = 0.00
              @expenses_major_mut_amt_o = 0.00
              @expenses_major_opt_amt_o = 0.00

              sheet.add_row ["CR", "#{expenses_major_acc[:name]}"], style: [nil, label_cell]
             
              if expenses_major_acc[:mother_accounting_code_entries_o].count > 0

                expenses_major_acc[:mother_accounting_code_entries_o].each do |expenses_mother_acc|
                  @expenses_mother_gen_amt_o = 0.00
                  @expenses_mother_mut_amt_o = 0.00
                  @expenses_mother_opt_amt_o = 0.00

                  # sheet.add_row ["CR", "#{expenses_mother_acc[:name]}"], style: [nil, label_cell]
                  
                  expenses_mother_acc[:accounting_code_category_entries_o].each do |expenses_acc_code_cat|
                    # sheet.add_row ["CR", "#{expenses_acc_code_cat[:name]}"], style: [nil, label_cell]

                    expenses_acc_code_cat[:accounting_code_entries_o].each do |expenses_acc_code|

                      if expenses_acc_code[:m_ending_debit] > 0
                        @expenses_mutual_amount_o = expenses_acc_code[:m_ending_debit]
                      elsif expenses_acc_code[:m_ending_credit] > 0
                        @expenses_mutual_amount_o = expenses_acc_code[:m_ending_credit] * -1
                      elsif expenses_acc_code[:m_ending_credit] = 0
                        @expenses_mutual_amount_o = 0.00  
                      end 

                      if expenses_acc_code[:g_ending_debit] > 0
                        @expenses_gen_amount_o = expenses_acc_code[:g_ending_debit]
                      elsif expenses_acc_code[:g_ending_credit] > 0
                        @expenses_gen_amount_o = expenses_acc_code[:g_ending_credit] * -1
                      elsif expenses_acc_code[:g_ending_credit] = 0
                        @expenses_gen_amount_o = 0.00 
                      end 

                      if expenses_acc_code[:o_ending_debit] > 0
                        @expenses_opt_amount_o = expenses_acc_code[:o_ending_debit]
                      elsif expenses_acc_code[:o_ending_credit] > 0
                        @expenses_opt_amount_o = expenses_acc_code[:o_ending_credit] * -1
                      elsif expenses_acc_code[:o_ending_credit] = 0
                        @expenses_opt_amount_o = 0.00
                      end

                      @expenses_total_o = @expenses_mutual_amount_o.to_f + @expenses_gen_amount_o.to_f + @expenses_opt_amount_o.to_f

                      @total_expenses_gen_fund_o = @expenses_income_gen_fund_o + @expenses_gen_amount_o.to_f
                      @total_expenses_opt_fund_o = @total_expenses_opt_fund_o + @expenses_opt_amount_o.to_f
                      @total_expenses_mut_fund_o = @total_expenses_mut_fund_o + @expenses_mutual_amount_o.to_f
                      @total_expenses_fund_o = @total_expenses_fund_o + @expenses_total_o.to_f

                      @expenses_mother_gen_amt_o += @expenses_gen_amount_o.to_f
                      @expenses_mother_mut_amt_o += @expenses_mutual_amount_o.to_f
                      @expenses_mother_opt_amt_o += @expenses_opt_amount_o.to_f    

                      if @detailed == "true"
                        t_entry = []
                        t_entry << "CR"
                        t_entry << expenses_acc_code[:name]
                        t_entry << @expenses_gen_amount_o
                        t_entry << @expenses_mutual_amount_o
                        t_entry << @expenses_opt_amount_o
                        t_entry << @expenses_total_o
                        sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                      end 
                    end

                    @expenses_mother_gen_total_amt_o = @expenses_mother_gen_amt_o
                    @expenses_mother_mut_total_amt_o = @expenses_mother_mut_amt_o
                    @expenses_mother_opt_total_amt_o = @expenses_mother_opt_amt_o
                    @expenses_mother_total_amt_o = @expenses_mother_gen_amt_o + @expenses_mother_mut_amt_o + @expenses_mother_opt_amt_o

                    @expenses_major_gen_amt_o += @expenses_mother_gen_total_amt_o
                    @expenses_major_mut_amt_o += @expenses_mother_mut_total_amt_o
                    @expenses_major_opt_amt_o += @expenses_mother_opt_total_amt_o
                    
                  end
                  sheet.add_row ["", "#{expenses_mother_acc[:name]}", @expenses_mother_gen_total_amt_o, @expenses_mother_mut_total_amt_o, @expenses_mother_opt_total_amt_o, @expenses_mother_total_amt_o], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                  @expenses_major_gen_total_amt_o = @expenses_major_gen_amt_o
                  @expenses_major_mut_total_amt_o = @expenses_major_mut_amt_o
                  @expenses_major_opt_total_amt_o = @expenses_major_opt_amt_o
                  @expenses_major_total_amt_o = @expenses_major_gen_amt_o + @expenses_major_mut_amt_o + @expenses_major_opt_amt_o 
                end
              else
                @expenses_major_gen_total_amt_o = 0.00
                @expenses_major_mut_total_amt_o = 0.00
                @expenses_major_opt_total_amt_o = 0.00
                @expenses_major_total_amt_o = 0.00  
              end
              sheet.add_row ["", "TOTAL #{expenses_major_acc[:name].upcase}", @expenses_major_gen_total_amt_o, @expenses_major_mut_total_amt_o, @expenses_major_opt_total_amt_o, @expenses_major_total_amt_o], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
                
            end
 
            @net_surplus_before_non_operating_expense_major_gen_grand_total = @net_surplus_before_non_operating_expense_major_gen_total + @expenses_major_gen_total_amt_o + @income_major_gen_total_amt_o
            @net_surplus_before_non_operating_expense_major_mut_grand_total = @net_surplus_before_non_operating_expense_major_mut_total + @expenses_major_mut_total_amt_o + @income_major_mut_total_amt_o
            @net_surplus_before_non_operating_expense_major_opt_grand_total = @net_surplus_before_non_operating_expense_major_opt_total + @expenses_major_opt_total_amt_o + @income_major_opt_total_amt_o
            @net_surplus_before_non_operating_expense_major_grand_total = @net_surplus_before_non_operating_expense_major_total + @expenses_major_total_amt_o + @income_major_total_amt_o
            
            sheet.add_row ["", "NET SURPLUS BEFORE INVESTMENT RETURN",  @net_surplus_before_non_operating_expense_major_gen_grand_total,
                                                                        @net_surplus_before_non_operating_expense_major_mut_grand_total, 
                                                                        @net_surplus_before_non_operating_expense_major_opt_grand_total, 
                                                                        @net_surplus_before_non_operating_expense_major_grand_total], 
                                                                        style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

            # para sa revenue na hiwalay (investment income at investment expenses)           
            @total_income_gen_fund_i = 0.00
            @total_income_mut_fund_i = 0.00
            @total_income_opt_fund_i = 0.00
            @total_income_fund_i = 0.00

            @data[:income_entries_i][:major_account_entries_i].each do |income_major_acc|
              @income_major_gen_amt_i = 0.00
              @income_major_mut_amt_i = 0.00
              @income_major_opt_amt_i = 0.00

              # sheet.add_row ["CR", "#{income_major_acc_a[:name]} a"], style: [nil, label_cell]
              
              income_major_acc[:mother_accounting_code_entries_i].each do |income_mother_acc|
                @income_mother_gen_amt_i = 0.00
                @income_mother_mut_amt_i = 0.00
                @income_mother_opt_amt_i = 0.00

                # sheet.add_row ["CR", "#{income_mother_acc_a[:name]} b"], style: [nil, label_cell]
                
                income_mother_acc[:accounting_code_category_entries_i].each do |income_acc_code_cat|
                  # sheet.add_row ["CR", "#{income_acc_code_cat_a[:name]} c"], style: [nil, label_cell]

                  income_acc_code_cat[:accounting_code_entries_i].each do |income_acc_code|

                    if income_acc_code[:m_ending_debit] > 0
                      @income_mutual_amount_i = income_acc_code[:m_ending_debit]
                    elsif income_acc_code[:m_ending_credit] > 0
                      @income_mutual_amount_i = income_acc_code[:m_ending_credit] * -1
                    elsif income_acc_code[:m_ending_credit] = 0
                      @income_mutual_amount_i = 0.00  
                    end 

                    if income_acc_code[:g_ending_debit] > 0
                      @income_gen_amount_i = income_acc_code[:g_ending_debit]
                    elsif income_acc_code[:g_ending_credit] > 0
                      @income_gen_amount_i = income_acc_code[:g_ending_credit] * -1
                    elsif income_acc_code[:g_ending_credit] = 0
                      @income_gen_amount_i = 0.00 
                    end 

                    if income_acc_code[:o_ending_debit] > 0
                      @income_opt_amount_i = income_acc_code[:o_ending_debit]
                    elsif income_acc_code[:o_ending_credit] > 0
                      @income_opt_amount_i = income_acc_code[:o_ending_credit] * -1
                    elsif income_acc_code[:o_ending_credit] = 0
                      @income_opt_amount_i = 0.00
                    end

                    @income_total_i = @income_mutual_amount_i.to_f + @income_gen_amount_i.to_f + @income_opt_amount_i.to_f

                    @total_income_gen_fund_i = @total_income_gen_fund_i + @income_gen_amount_i.to_f
                    @total_income_opt_fund_i = @total_income_opt_fund_i + @income_opt_amount_i.to_f
                    @total_income_mut_fund_i = @total_income_mut_fund_i + @income_mutual_amount_i.to_f
                    @total_income_fund_i = @total_income_fund_i + @income_total_i.to_f

                    @income_mother_gen_amt_i += @income_gen_amount_i.to_f
                    @income_mother_mut_amt_i += @income_mutual_amount_i.to_f
                    @income_mother_opt_amt_i += @income_opt_amount_i.to_f    

                    if @detailed == "true"
                      t_entry = []
                      t_entry << "CR"
                      t_entry << income_acc_code[:name]
                      t_entry << @income_gen_amount_i
                      t_entry << @income_mutual_amount_i
                      t_entry << @income_opt_amount_i
                      t_entry << @income_total_i
                      sheet.add_row t_entry, style: [nil, nil,currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
                    end   
                  end

                  @income_mother_gen_total_amt_i = @income_mother_gen_amt_i
                  @income_mother_mut_total_amt_i = @income_mother_mut_amt_i
                  @income_mother_opt_total_amt_i = @income_mother_opt_amt_i
                  @income_mother_total_amt_i = @income_mother_gen_amt_i + @income_mother_mut_amt_i + @income_mother_opt_amt_i

                  @income_major_gen_amt_i += @income_mother_gen_total_amt_i
                  @income_major_mut_amt_i += @income_mother_mut_total_amt_i
                  @income_major_opt_amt_i += @income_mother_opt_total_amt_i
                end
                sheet.add_row ["", "#{income_mother_acc[:name]}", @income_mother_gen_total_amt_i, @income_mother_mut_total_amt_i, @income_mother_opt_total_amt_i, @income_mother_total_amt_i], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                @income_major_gen_total_amt_i = @income_major_gen_amt_i
                @income_major_mut_total_amt_i = @income_major_mut_amt_i
                @income_major_opt_total_amt_i = @income_major_opt_amt_i
                @income_major_total_amt_i = @income_major_gen_amt_i + @income_major_mut_amt_i + @income_major_opt_amt_i 
              end
              sheet.add_row ["", "TOTAL #{income_major_acc[:name].upcase}", @income_major_gen_total_amt_i, @income_major_mut_total_amt_i, @income_major_opt_total_amt_i, @income_major_total_amt_i], style: [nil, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
            end

          sheet.add_row ["", "NET COMPREHENSIVE SURPLUS (LOSS)", @net_surplus_before_non_operating_expense_major_gen_grand_total + @total_income_gen_fund_i,
                                                                 @net_surplus_before_non_operating_expense_major_mut_grand_total + @total_income_mut_fund_i, 
                                                                 @net_surplus_before_non_operating_expense_major_opt_grand_total + @total_income_opt_fund_i, 
                                                                 @net_surplus_before_non_operating_expense_major_grand_total + @total_income_fund_i],
                                                                  style: [nil, label_cell_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline, currency_cell_right_bold_underline]
        end
      end
      p
		end
	end
end
