module Accounting
  class GenerateProgramStatusReports
    def initialize
     #@as_of  = DateTime.parse("2017-05-31")
     @as_of = Date.new(2017, 5, 31)
     #month = (@as_of.strftime("%m").to_i) - 1
     @present_month = @as_of.month
     #raise @present_month.inspect
     @month = @as_of.month - 1
     
     @current_year = @as_of.year
     
     #raise @month.inspect

      @jef = Date.civil(@current_year, @month, -1)
     # raise @jef.inspect

     @present_month_details = DailyReport.where(as_of: @as_of).last
     @previus_month_details = DailyReport.where("extract(month from as_of) = ? and extract(year from as_of) = ?", @month,@current_year).order(:as_of).last
     
     @active_member = Member.all
     @data = {}
     @data[:present_month_client_count_male] = 0
     @data[:present_month_client_count_female] = 0
     @data[:total_present_month_client_count] = 0

     
  
     @data[:previous_month_client_count_female] = 0
     @data[:previous_month_client_count_male] = 0  
     @data[:total_previous_client_count] = 0

    
     @data[:total_client_count_female] = 0
     @data[:total_client_count_male] = 0
     @data[:total_count_client] = 0
    
    #---------------------------------------------------------

     @data[:present_month_member_count_pure_saver] = 0
     @data[:present_month_member_count_active_loaner] = 0
     @data[:total_present_month_member_count] = 0
    
     @data[:previous_month_member_count_pure_saver] = 0
     @data[:previous_month_member_count_active_loaner] = 0
     @data[:total_previous_month_member_count] = 0 
  
     @data[:total_member_count_pure_saver] = 0
     @data[:total_member_count_active_loaner] = 0
     @data[:total_member_count] = 0
    #----------------------------------------------------------
     @data[:livelihood_details] = []
     @data[:previous_livelihood_details] = []
     @data[:total_number_of_active_member_per_loan] = 0
     
    #--------------------------------------------------------
    @data[:loan_portfolio_details] = []
    @data[:total_number_of_loan_disbursed] = []
    @data[:total_amount_of_loan_disbursed] = []
    @data[:loan_past_due_amount_present] = 0
    @data[:loan_past_due_amount_previous] = 0
  end
    
    def execute!
      build_outstanding_loan_amount
      build_active_client
      build_active_members
      build_number_of_active_loans
      build_total_new_client
      build_loan_portfolio
      build_total_number_loan_disbursed
      build_total_amount_loan_disbursed
      build_past_due_amount
      build_par_amount
      build_amount_due
      #build_outstanding_loan_amount
      @data
    end


    private
    def build_amount_due
      daily_report_month_generate =  DailyReport.where("extract(year from as_of) = ?",@current_year).pluck(:as_of).map{|x| x.month}.uniq
        second_loop_amount_due = 0
      daily_report_month_generate.each do |drmg|
        
        if @present_month <= drmg
        #monthly_date_list = DailyReport.where("extract(year from as_of) = ?",@current_year).order(:as_of).last.repayments ["loan_products"]
        monthly_date_list = DailyReport.where("extract(month from as_of) = ?  and extract(year from as_of) = ?",drmg,@current_year).order(:as_of).last.repayments["loan_products"]
        first_loop_amount_due = 0
          monthly_date_list.each do |mdl|
            first_loop_amount_due += mdl["total_loan_balance"].to_f

          end
        end

        if @month <= drmg
          
        end
        second_loop_amount_due += first_loop_amount_due.to_f
      end
      @data[:total_amount_due] = second_loop_amount_du = second_loop_amount_due

    end





    def build_par_amount
      livelihood_products_id = Settings.active_loans_type
      par_third_loop = 0
      par_privious_third_loop = 0
      par_privious_privious_third_loop = 0
      par_total_par_monthly = 0
      livelihood_products_id.each do |lpi|
        par_scond_loop = 0
        par_privious_scond_loop = 0
        par_privious_privious_scond_loop = 0
        lpi["particular"].each do |p|
          daily_reports_par = DailyReport.where(as_of: @as_of).last.repayments["loan_products"]
          previous_reports_par = @previus_month_details.repayments["loan_products"]          
          loan_particular_par = LoanProduct.find(p["loanid"]).name
          par_first_loop = 0
          daily_reports_par.each do |drp|
            if drp["loan_product"] == loan_particular_par
              par_first_loop += drp["total_par_amount"].to_f
            end #if end
          end #drp end
          par_privious_first_loop = 0
          previous_reports_par.each do |prp|
            if prp["loan_product"] == loan_particular_par
              par_privious_first_loop += prp["total_par_amount"].to_f
            end #if end
          end #prp end

          prev_month = @month - 1

          total_par_previous_previous = DailyReport.where("extract(month from as_of) = ? and extract(year from as_of) = ?", prev_month,@current_year).order(:as_of).last.repayments["loan_products"]

          par_first_previous_previous = 0
          total_par_previous_previous.each do |tppp|
            if tppp["loan_product"] == loan_particular_par
              par_first_previous_previous += tppp["total_par_amount"].to_f
            end
          end #end tppp


          par_privious_privious_scond_loop += par_first_previous_previous
          par_scond_loop += par_first_loop  
          par_privious_scond_loop += par_privious_first_loop
        end #p end
        par_privious_privious_third_loop += par_privious_privious_scond_loop
        par_privious_third_loop += par_privious_scond_loop
        par_third_loop += par_scond_loop
        par_total_par_monthly = par_third_loop - par_privious_third_loop
      end
      @data[:par_previous_month] = par_privious_third_loop - par_privious_privious_third_loop 
      @data[:par_present_month] = par_third_loop - par_privious_third_loop
      @data[:par_total_par_monthly] = @data[:par_present_month]  - @data[:par_previous_month]
  #--------------------------------------------------------monthly---------------------------------------------------------------
      daily_report_month_generate =  DailyReport.where("extract(year from as_of) = ?",@current_year).pluck(:as_of).map{|x| x.month}.uniq
      
      present_second_loop_par = 0
      previous_second_loop_par = 0
      daily_report_month_generate.each do |drmg|
        if drmg < @present_month
          monthly_date_list = DailyReport.where("extract(month from as_of) = ? and extract(year from as_of) = ?", drmg,@current_year).order(:as_of).last.repayments["loan_products"]
          
          present_par = 0
          previous_par = 0
          monthly_date_list.each do |mdl|
             present_par += mdl["total_par_amount"].to_f
          end #end monthly
        end #end of if
        present_second_loop_par += present_par.to_f


        if drmg < @month
          yearly_present_date_list = DailyReport.where("extract(month from as_of) = ? and extract(year from as_of) = ?", drmg,@current_year).order(:as_of).last.repayments["loan_products"]
          yearly_present_date_list.each do |ypdl|
            previous_par += ypdl["total_par_amount"].to_f
          end
        end
      
      previous_second_loop_par += previous_par.to_f

      end #drmg
      @data[:par_previous_yearly] = previous_second_loop_par
      @data[:par_present_yearly] = present_second_loop_par
      @data[:par_total_yearly] = @data[:par_present_yearly] - @data[:par_previous_yearly]


      @data[:par_total_present] = @data[:par_present_month] + @data[:par_present_yearly]
      @data[:par_total_previous] = @data[:par_previous_month] + @data[:par_previous_yearly]

      @data[:par_total_incdec] = @data[:par_total_present] - @data[:par_total_previous]

      @data[:par_rate_present] = (@data[:par_total_present] / @data[:third_loop]) * 100
      @data[:par_rate_previous] = (@data[:par_total_previous] / @data[:previous_third_loop]) / 100

      @data[:par_rate_incdec] = @data[:par_rate_present] - @data[:par_rate_previous]


    end

    def build_outstanding_loan_amount
      livelihood_products_id = Settings.active_loans_type
      third_loop = 0
      previous_third_loop = 0
      livelihood_products_id.each do |lpitotal|
        second_loop = 0
        previous_second_loop = 0
        lpitotal["particular"].each do |p|
          daily_reports_portfolio = DailyReport.where(as_of: @as_of).last.repayments["loan_products"]
          first_loop = 0
          previous_first_loop = 0
          loan_particular_amount_disbursed = LoanProduct.find(p["loanid"]).name
          daily_reports_portfolio.each do |drp|
            if drp["loan_product"] == loan_particular_amount_disbursed
            
              first_loop =+ drp["total_portfolio"].to_f 
              #raise first_loop.inspect
            end
          end
          second_loop += first_loop.to_f
          
          previous_reports_portfolio = @previus_month_details.repayments["loan_products"]

          previous_reports_portfolio.each do |prp|
            if prp["loan_product"] == loan_particular_amount_disbursed
              previous_first_loop =+ prp["total_portfolio"].to_f
            end
          end
          previous_second_loop += previous_first_loop

        end
        third_loop += second_loop.to_f
        previous_third_loop += previous_second_loop
       # raise third_loop.inspect
      end
      @data[:third_loop] = third_loop
      @data[:previous_third_loop] = previous_third_loop
      @data[:total_outstanding_loop] = third_loop - previous_third_loop
    end

    def build_past_due_amount
      present_past_due  = 0
      previous_past_due = 0
      previous_previous_past_due = 0
       total_past_due_present = @present_month_details.repayments["loan_products"]
       total_past_due_present.each do |tpdp|
         present_past_due += tpdp["total_amt_past_due"].to_f
       end

       total_past_due_previous = @previus_month_details.repayments["loan_products"]
       total_past_due_previous.each do |tpdprev|
        previous_past_due += tpdprev["total_amt_past_due"].to_f
       end
      prev_month = @month - 1

      total_past_due_previous_previous = DailyReport.where("extract(month from as_of) = ? and extract(year from as_of) = ?", prev_month,@current_year).order(:as_of).last.repayments["loan_products"]

      total_past_due_previous_previous.each do |tpdpp|
        previous_previous_past_due += tpdpp["total_amt_past_due"].to_f
      end

       total_past_due_in_a_month = present_past_due - previous_past_due
       total_past_due_in_a_month_prev = previous_past_due - previous_previous_past_due
       total_past_due_in_a_month >= 0 ? display_past_due_present = total_past_due_in_a_month : display_past_due_present = 0
       total_past_due_in_a_month_prev >= 0 ? display_past_due_previous = total_past_due_in_a_month_prev : display_past_due_previous = 0
      

      @data[:loan_past_due_amount_present] = display_past_due_present
      @data[:loan_past_due_amount_previous] = display_past_due_previous
      @data[:total_loan_past_due_amount] = @data[:loan_past_due_amount_present] - @data[:loan_past_due_amount_previous]

      #--------------------------------------------------past due 31 - 365 days----------------------------------------------------------------------------
      
      daily_report_month_generate =  DailyReport.where("extract(year from as_of) = ?",@current_year).pluck(:as_of).map{|x| x.month}.uniq
  
      present_details_past_due = 0
      previous_details_past_due = 0
      daily_report_month_generate.each do |drmg|
        previous_past_due = 0
        present_past_due = 0
        if drmg < @present_month

          monthly_date_list = DailyReport.where("extract(month from as_of) = ? and extract(year from as_of) = ?", drmg,@current_year).order(:as_of).last.repayments["loan_products"]


          monthly_date_list.each do |mdl|
            present_past_due += mdl["total_amt_past_due"].to_f
          
          end
        
        end

        if drmg < @month 
           monthly_date_list_prev = DailyReport.where("extract(month from as_of) = ? and extract(year from as_of) = ?", drmg,@current_year).order(:as_of).last.repayments["loan_products"]
            monthly_date_list_prev.each do |mdlp|
              previous_past_due += mdlp["total_amt_past_due"].to_f
            end
        end
        present_details_past_due += present_past_due
        previous_details_past_due += previous_past_due
        @data[:present_past_due_yearly] = present_details_past_due
        @data[:previous_past_due_yearly] = previous_details_past_due
        @data[:total_past_due_yearly] = @data[:present_past_due_yearly] - @data[:previous_past_due_yearly]

      @data[:total_past_due_amount_present] = @data[:loan_past_due_amount_present] + @data[:present_past_due_yearly]
      @data[:total_past_due_amount_previous] = @data[:loan_past_due_amount_previous] + @data[:previous_past_due_yearly]

      @data[:total_past_due_incdec] = @data[:total_past_due_amount_present] - @data[:total_past_due_amount_previous]

      @data[:total_past_due_rate] = (@data[:total_past_due_amount_present] / @data[:third_loop]) * 100
      @data[:total_previous_past_due_rate] = (@data[:total_past_due_amount_previous] / @data[:previous_third_loop]) * 100

      @data[:gtotal_past_due_rate] = @data[:total_past_due_rate] - @data[:total_previous_past_due_rate]

      end


    end

    def build_total_amount_loan_disbursed
      livelihood_products_id = Settings.active_loans_type


      livelihood_products_id.each do |lpi|
        loan_type_amount_of_disbursed = {}
        loan_type_amount_of_disbursed[:loan_product_type_disbursed] = lpi["name"]
        loan_type_amount_of_disbursed[:loan_product_type_disbursed_details] = []
        lpi["particular"].each do |p|
          loan_particular_amount_disbursed = {}
          loan_particular_amount_disbursed["loan_product_name"] = LoanProduct.find(p["loanid"]).name
          
          loan_particular_amount_disbursed["present_loan_product_total_disbursment"] = Loan.where("extract(month from date_approved) >= ? and extract(month from date_approved) <= ? and extract(year from date_of_release)= ? and loan_product_id = ?  ",@present_month,@present_month,@current_year,p["loanid"]).sum("amount")

          loan_particular_amount_disbursed["previous_loan_product_total_disbursment"] = Loan.where("extract(month from date_approved) >= ? and extract(month from date_approved) <= ? and extract(year from date_of_release)= ? and loan_product_id = ?  ",@month,@month,@current_year,p["loanid"]).sum("amount")
          
          loan_particular_amount_disbursed["loan_product_total_disbursment"] = loan_particular_amount_disbursed["present_loan_product_total_disbursment"] - loan_particular_amount_disbursed["previous_loan_product_total_disbursment"]

          loan_type_amount_of_disbursed[:loan_product_type_disbursed_details] << loan_particular_amount_disbursed
          
        end

        @data[:total_amount_of_loan_disbursed] << loan_type_amount_of_disbursed
      end
    end

    def build_total_number_loan_disbursed
      @data[:total_number_of_disbursed_present_as_of] = Loan.where("extract(month from date_of_release) >= ? and extract(month from date_of_release) <= ? and extract(year from date_of_release)= ? ",1,@present_month,@current_year).count

      @data[:total_number_of_disbursed_previous_as_of] = Loan.where("extract(month from date_of_release) >= ? and extract(month from date_of_release) <= ? and extract(year from date_of_release)= ? ",1,@month,@current_year).count
      
      @data[:total_number_as_of] = @data[:total_number_of_disbursed_present_as_of] - @data[:total_number_of_disbursed_previous_as_of]
    
      @data[:number_of_disbursed_present] = Loan.where("extract(month from date_of_release) >= ? and extract(month from date_of_release) <= ? and extract(year from date_of_release)= ? ",@present_month,@present_month,@current_year).count
      @data[:number_of_disbursed_previous] = Loan.where("extract(month from date_of_release) >= ? and extract(month from date_of_release) <= ? and extract(year from date_of_release)= ? ",@month,@month,@current_year).count 
      
      @data[:total_number_of_disbursed] = @data[:number_of_disbursed_present] - @data[:number_of_disbursed_previous]


      livelihood_products_id = Settings.active_loans_type
    
      livelihood_products_id.each do |lpi|
        loan_type_number_of_disbursed = {}
        loan_type_number_of_disbursed[:loan_product_name] = []
        loan_type_number_of_disbursed[:loan_type] = lpi["name"]
        #loan_type_number_of_disbursed = 0
        loan_type_number_of_disbursed["present_total_loan_product_number_of_disbursed"] = 0
        loan_type_number_of_disbursed["previous_total_loan_product_number_of_disbursed"] = 0
        loan_type_number_of_disbursed["total_loan_product_number_of_disbursed"] = 0
        lpi["particular"].each do |p|
          loan_particular_disbursed = {}
          loan_particular_disbursed["loan_product_disbursed"] = LoanProduct.find(p["loanid"]).name

          loan_particular_disbursed["present_loan_product_number_of_disbursed"] = Loan.where("extract(month from date_of_release) >= ? and extract(month from date_of_release) <= ? and extract(year from date_of_release)= ? and loan_product_id = ? ",@present_month,@present_month,@current_year, p["loanid"]).count
            
         # loan_type_number_of_disbursed += loan_particular_disbursed["present_loan_product_number_of_disbursed"]

          loan_type_number_of_disbursed["present_total_loan_product_number_of_disbursed"] += loan_particular_disbursed["present_loan_product_number_of_disbursed"]
          
          loan_particular_disbursed["previous_loan_product_number_of_disbursed"] = Loan.where("extract(month from date_of_release) >= ? and extract(month from date_of_release) <= ? and extract(year from date_of_release)= ? and loan_product_id = ? ",@month,@month,@current_year, p["loanid"]).count

          loan_type_number_of_disbursed["previous_total_loan_product_number_of_disbursed"] += loan_particular_disbursed["previous_loan_product_number_of_disbursed"]

          loan_particular_disbursed["total_loan_product_number_of_disbursed"] =loan_particular_disbursed["present_loan_product_number_of_disbursed"] - loan_particular_disbursed["previous_loan_product_number_of_disbursed"]
          
          loan_type_number_of_disbursed["total_loan_product_number_of_disbursed"] += loan_particular_disbursed["total_loan_product_number_of_disbursed"]

          loan_type_number_of_disbursed[:loan_product_name]  << loan_particular_disbursed
        
        end
      

        @data[:total_number_of_loan_disbursed] << loan_type_number_of_disbursed
      end
      


    end

    def build_loan_portfolio
     livelihood_products_id = Settings.active_loans_type
     livelihood_products_id.each do |lpi|
      loan_type_portfolio_details = {}
      loan_type_portfolio_details[:loan_product_balance_details] = []
      loan_type_portfolio_details[:loan_product_portfolio_details] = []

      loan_type_portfolio_details[:loan_portfolio_type] = lpi["name"]
      total_present_portfolio = 0
      total_previous_portfolio = 0
      lpi["particular"].each do |p|
        loan_particular_portfolio_details = {}
        loan_particular_portfolio_details[:particular_details] = LoanProduct.find(p["loanid"]).name
        
        daily_reports_portfolio = DailyReport.where(as_of: @as_of).last.repayments["loan_products"]
        
        daily_reports_portfolio.each do |drp|
          if drp["loan_product"] == loan_particular_portfolio_details[:particular_details]
            
            loan_particular_portfolio_details[:present_portfolio] = drp["total_portfolio"]  
            total_present_portfolio += loan_particular_portfolio_details[:present_portfolio].to_f
          end
        end
        
        loan_type_portfolio_details[:present_portfolio_total] = total_present_portfolio


        previous_reports_portfolio = @previus_month_details.repayments["loan_products"]
        
        previous_reports_portfolio.each do |prf|
          if prf["loan_product"] == loan_particular_portfolio_details[:particular_details]
            loan_particular_portfolio_details[:previous_portfolio] = prf["total_portfolio"]
            
            total_previous_portfolio += prf["total_portfolio"].to_f
          end
        end

        loan_type_portfolio_details[:previous_portfolio_total] = total_previous_portfolio
      
        
        total_portfolio_present_present = @present_month_details.repayments["loan_products"]
        total_portfolio_present_present.each do |tppp|
           if tppp["loan_product"] == loan_particular_portfolio_details[:particular_details]
             total_previous_reports_portfolio = @previus_month_details.repayments["loan_products"]
             total_previous_reports_portfolio.each do |tprp|
              if tprp["loan_product"] == loan_particular_portfolio_details[:particular_details]
                loan_particular_portfolio_details[:total_loan_portfolio] = tppp["total_portfolio"].to_f - tprp["total_portfolio"].to_f
              end
             end
          
           end
        end
 
        loan_type_portfolio_details[:total_portfolio] = loan_type_portfolio_details[:present_portfolio_total] - loan_type_portfolio_details[:previous_portfolio_total]
        
        loan_type_portfolio_details[:loan_product_portfolio_details] << loan_particular_portfolio_details

      end
      @data[:loan_portfolio_details] << loan_type_portfolio_details
     end
    end


    def build_total_new_client

     @data[:present_total_member_count_as_of] = MembershipPayment.where("extract(month from paid_at) >= ? and extract(month from paid_at) <= ? and extract(year from paid_at)=?  and membership_type_id = ? ", 1,@present_month,@current_year,1).count

     @data[:previous_total_member_count_as_of] = MembershipPayment.where("extract(month from paid_at) >= ? and extract(month from paid_at) <= ? and extract(year from paid_at)=?  and membership_type_id = ? ", 1,@month,@current_year,1).count

    
     @data[:present_total_member_count_for_the_month] = MembershipPayment.where("extract(month from paid_at) >= ? and extract(month from paid_at) <= ? and extract(year from paid_at)=?  and membership_type_id = ? ", @present_month,@present_month,@current_year,1).count

     @data[:previous_total_member_count_for_the_month] = MembershipPayment.where("extract(month from paid_at) >= ? and extract(month from paid_at) <= ? and extract(year from paid_at)=?  and membership_type_id = ? ", @month,@month,@current_year,1).count

     @data[:total_previous_and_present_member_count] = @data[:present_total_member_count_for_the_month] - @data[:previous_total_member_count_for_the_month]

    end



    def build_number_of_active_loans
      livelihood_products_id = Settings.active_loans_type
      gtotal_number_of_active_loans_present = 0    
      livelihood_products_id.each do |lpi|
        loan_type_details = {}
        loan_type_details[:product_name] = []
        loan_type_details[:loantype] = lpi["name"]
        loan_amount_type_total = 0
        previous_total_loan_amount = 0
        lpi["particular"].each do |p|
          loan_particular_name = {}
          loan_particular_name[:loan_name_details] = LoanProduct.find(p["loanid"]).name
          daily_reports_loan_details = @present_month_details.active_loans_by_loan_product['loan_products']
          previous_daily_reports_loan_details = @previus_month_details.active_loans_by_loan_product['loan_products']
          
          daily_reports_loan_details.each do |drld|
            if drld["loan_product"] == loan_particular_name[:loan_name_details]
              loan_particular_name[:total_amout_product] = drld["total"]
              loan_amount_type_total += drld["total"]
            end
          end #end present loop
          
          loan_type_details[:totalloantypeamount] = loan_amount_type_total
          gtotal_number_of_active_loans_present += loan_type_details[:totalloantypeamount]
          #end present active member per loan

    
          previous_daily_reports_loan_details.each do |pdrld|
          
            if pdrld["loan_product"] == loan_particular_name[:loan_name_details]
              loan_particular_name[:previous_total_amout_product] = pdrld["total"]
              previous_total_loan_amount += pdrld["total"]
            end
            
          end #end previous loop

          loan_type_details[:previoustotalloantypeamount] = previous_total_loan_amount
          #end previous active member per loan

          loan_type_details[:grandtotalloantypeamount] = loan_type_details[:totalloantypeamount] - loan_type_details[:previoustotalloantypeamount]
        
  
          daily_reports_loan_details.each do |totaldrld|

            if loan_particular_name[:loan_name_details] == totaldrld["loan_product"]
              present_loan_product = totaldrld["loan_product"]
              present_month_total = totaldrld["total"]
              previous_daily_reports_loan_details.each do |totalpdrld|
                if totalpdrld["loan_product"] == present_loan_product
                  loan_particular_name[:test] = present_month_total  - totalpdrld["total"]
                end
              end
            
            end

          end
          loan_particular_name[:totalamount] = loan_particular_name[:test]
          #end of total active member per loan

          loan_type_details[:product_name] << loan_particular_name
        end
        @data[:total_number_of_active_member_per_loan] = gtotal_number_of_active_loans_present

        @data[:livelihood_details] << loan_type_details
        
      end

    end
    

    def build_active_client
      #month = 3
    
      @data[:present_month_client_count_male]    = @present_month_details.member_stats["active_members_male"]
      @data[:present_month_client_count_female]  = @present_month_details.member_stats["active_members_female"]
      @data[:total_present_month_client_count]   = @data[:present_month_client_count_female] + @data[:present_month_client_count_male]
      @data[:previous_month_client_count_female] = @previus_month_details.member_stats["active_members_female"]
      @data[:previous_month_client_count_male]   = @previus_month_details.member_stats["active_members_male"]
      @data[:total_previous_client_count]        = @data[:previous_month_client_count_female] + @data[:previous_month_client_count_male]
      @data[:total_client_count_female]          = @present_month_details.member_stats["active_members_female"] - @previus_month_details.member_stats["active_members_female"]
      @data[:total_client_count_male]            = @present_month_details.member_stats["active_members_male"] - @previus_month_details.member_stats["active_members_male"]
      @data[:total_count_client]                 = @data[:total_present_month_client_count] - @data[:total_previous_client_count]   

    end

    def build_active_members
      @data[:present_month_member_count_pure_saver]     = @present_month_details.member_stats["pure_savers_total"]
      @data[:present_month_member_count_active_loaner]  = @present_month_details.member_stats["active_loaners_total"]
      @data[:total_present_month_member_count]          = @data[:present_month_member_count_active_loaner] + @data[:present_month_member_count_pure_saver]
      @data[:previous_month_member_count_pure_saver]    = @previus_month_details.member_stats["pure_savers_total"]
      @data[:previous_month_member_count_active_loaner] = @previus_month_details.member_stats["active_loaners_total"]
      @data[:total_previous_month_member_count]         = @data[:previous_month_member_count_pure_saver] + @data[:previous_month_member_count_active_loaner]
      @data[:total_member_count_pure_saver] = @data[:present_month_member_count_pure_saver] - @data[:previous_month_member_count_pure_saver]
      @data[:total_member_count_active_loaner] = @data[:present_month_member_count_active_loaner] - @data[:previous_month_member_count_active_loaner]
      @data[:total_member_count] = @data[:total_present_month_member_count] - @data[:total_previous_month_member_count]

    end

  end
end
