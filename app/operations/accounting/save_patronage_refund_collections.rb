module Accounting
  class SavePatronageRefundCollections
    def initialize(data:, as_of:, patronage_rate:)
      @data = data
      @as_of = as_of
      #raise @as_of.inspect
      @patronage_rate = patronage_rate
      @patronage_refund_collection = PatronageRefundCollection.new
      @patronage_refund_collection.total_loan_interest_amount = @data[:total_loan_interest]
      @patronage_refund_collection.total_interest_amount = @data[:total_loan_interest_rate]
      @patronage_refund_collection.patronage_rate = @patronage_rate
      @patronage_refund_collection.as_of = @as_of
      @patronage_refund_collection.total_savings_amount = (@data[:total_loan_interest_rate] * 0.9)
      @patronage_refund_collection.total_cbu_amount  = (@data[:total_loan_interest_rate] * 0.1)


    end
    def execute!
      @data[:interest_details].each do |d|
        prcr = PatronageRefundCollectionRecord.new(
          data: d
        )
        @patronage_refund_collection.patronage_refund_collection_records << prcr
      end
      @patronage_refund_collection.save
    end
  end
end
