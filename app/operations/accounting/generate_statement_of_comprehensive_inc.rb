module Accounting
  class GenerateStatementOfComprehensiveInc
    def initialize(start_date:, end_date:, branch:)
      @data  = {}
      @data[:income_entries]     = {}
      @data[:income_entries_o]     = {}
      @data[:income_entries_i]     = {}
      @data[:expenses_entries]     = {}
      @data[:expenses_entries_o]     = {}
      @data[:fund_balance_entries_a]     = {}
      @data[:company]    = Settings.company
      @data[:branch]     = branch
      @data[:entries]    = []
      @data[:start_date] = start_date.to_date
      @data[:end_date]   = end_date.to_date

      @data[:m_total_beginning_debit] = 0
      @data[:m_total_beginning_credit] = 0
      @data[:g_total_beginning_debit] = 0
      @data[:g_total_beginning_credit] = 0
      @data[:o_total_beginning_debit] = 0
      @data[:o_total_beginning_credit] = 0
      @data[:m_total_current_debit] = 0
      @data[:m_total_current_credit] = 0
      @data[:g_total_current_debit] = 0
      @data[:g_total_current_credit] = 0
      @data[:o_total_current_debit] = 0
      @data[:o_total_current_credit] = 0
      @data[:m_total_ending_debit] = 0
      @data[:m_total_ending_credit] = 0
      @data[:g_total_ending_debit] = 0
      @data[:g_total_ending_credit] = 0
      @data[:o_total_ending_debit] = 0
      @data[:o_total_ending_credit] = 0

      @year             = @data[:end_date].year
      @previous_year    = @year - 1
       
      @fund_balance_major_group = MajorGroup.where(name: "FUND BALANCE").first
      @income_major_group = MajorGroup.where(name: "INCOME").first
      @expenses_major_group = MajorGroup.where(name: "EXPENSES").first
      
      @fund_balance = @fund_balance_major_group.major_accounts.where("name != ?","Fund Balance")
      @income = @income_major_group.major_accounts
      @expenses = @expenses_major_group.major_accounts
      
      @fund_balance_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ? AND major_accounts.id IN (?)", @fund_balance_major_group.id, @fund_balance.pluck(:id))

      @income_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @income_major_group.id)
      @expenses_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @expenses_major_group.id)

      @journal_entries  = JournalEntry.joins(:voucher)
      @start_date = @data[:start_date]
      @end_date   = @data[:end_date]

      @mutual_benefit = AccountingFund.where(name: "Mutual Benefit Fund").first 
      @general_fund = AccountingFund.where(name: "General Fund").first
      @optional_fund = AccountingFund.where(name: "Optional Fund").first

      @entries  = []
    end

    def execute!
      build_income!
      build_income_o!
      build_income_i!
      build_expenses!
      build_expenses_o!
      # income_a
      build_fund_balance_a!

      @data[:entries] = @entries

      @data
    end

    # income_a:
    def build_fund_balance_a!
      @data[:fund_balance_entries_a][:major_account_entries_a] = []
      ### BUILD MAJOR ACCOUNT

      @fund_balance_major_group.major_accounts.where("major_accounts.id = ?", 6).each do |major_account|
        major_account_accounting_codes  = @fund_balance_accounting_codes.where("major_accounts.id = ?", major_account.id)

        major_account_data_a = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data_a[:mother_accounting_code_entries_a] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.where("name != ? AND name != ? AND name != ? AND name != ?", "Member's Fee / Dues", "Net Member's Contribution", "Net Premiums", "Other Income").order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @fund_balance_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data_a = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data_a[:accounting_code_category_entries_a]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @fund_balance_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data_a = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data_a[:accounting_code_entries_a] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data_a = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data_a[:accounting_code_entries_a] << accounting_code_data_a
              end
            mother_accounting_code_data_a[:accounting_code_category_entries_a] << accounting_code_category_data_a
          end
          major_account_data_a[:mother_accounting_code_entries_a] << mother_accounting_code_data_a
        end
        @data[:fund_balance_entries_a][:major_account_entries_a] << major_account_data_a
      end
    end

    #revenue lang (investment income , investment expenses)
    def build_income_i!
      @data[:income_entries_i][:major_account_entries_i]  = []
      ### BUILD MAJOR ACCOUNT

      @income_major_group.major_accounts.where("major_accounts.id = ?", 6).each do |major_account|
        major_account_accounting_codes  = @income_accounting_codes.where("major_accounts.id = ?", major_account.id)

        major_account_data_i = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data_i[:mother_accounting_code_entries_i] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.where("name != ? AND name != ? AND name != ? AND name != ?", "Member's Fee / Dues", "Net Member's Contribution", "Net Premiums", "Other Income").order("id ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @income_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data_i = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data_i[:accounting_code_category_entries_i]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @income_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data_i = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data_i[:accounting_code_entries_i] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data_i = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data_i[:accounting_code_entries_i] << accounting_code_data_i
              end
            mother_accounting_code_data_i[:accounting_code_category_entries_i] << accounting_code_category_data_i
          end
          major_account_data_i[:mother_accounting_code_entries_i] << mother_accounting_code_data_i
        end
        @data[:income_entries_i][:major_account_entries_i] << major_account_data_i
      end
    end

    #revenue lang
    def build_income!
      @data[:income_entries][:major_account_entries]  = []
      ### BUILD MAJOR ACCOUNT

      @income_major_group.major_accounts.where("major_accounts.id = ?", 6).each do |major_account|
        major_account_accounting_codes  = @income_accounting_codes.where("major_accounts.id = ?", major_account.id)

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data[:mother_accounting_code_entries] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.where("name != ? AND name != ?", "Investment Expenses", "Investment Income").order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @income_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @income_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data[:accounting_code_entries] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
          end
          major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
        end
        @data[:income_entries][:major_account_entries] << major_account_data
      end
    end

    # other non-operating income
    def build_income_o!
      @data[:income_entries_o][:major_account_entries_o]  = []
      ### BUILD MAJOR ACCOUNT

      @income_major_group.major_accounts.where("major_accounts.id = ?", 9).each do |major_account|
        major_account_accounting_codes  = @income_accounting_codes.where("major_accounts.id = ?", major_account.id)

        major_account_data_o = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data_o[:mother_accounting_code_entries_o] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @income_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data_o = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data_o[:accounting_code_category_entries_o]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @income_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data_o = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data_o[:accounting_code_entries_o] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data_o = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data_o[:accounting_code_entries_o] << accounting_code_data_o
              end
            mother_accounting_code_data_o[:accounting_code_category_entries_o] << accounting_code_category_data_o
          end
          major_account_data_o[:mother_accounting_code_entries_o] << mother_accounting_code_data_o
        end
        @data[:income_entries_o][:major_account_entries_o] << major_account_data_o
      end
    end

    def build_expenses_o!
      @data[:expenses_entries_o][:major_account_entries_o]  = []
      ### BUILD MAJOR ACCOUNT

      @expenses_major_group.major_accounts.where("major_accounts.id = ?", 10).each do |major_account|
        major_account_accounting_codes  = @expenses_accounting_codes.where("major_accounts.id = ?", major_account.id)

        major_account_data_o = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data_o[:mother_accounting_code_entries_o] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @expenses_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data_o = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data_o[:accounting_code_category_entries_o]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @expenses_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data_o = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data_o[:accounting_code_entries_o] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data_o = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data_o[:accounting_code_entries_o] << accounting_code_data_o
              end
            mother_accounting_code_data_o[:accounting_code_category_entries_o] << accounting_code_category_data_o
          end
          major_account_data_o[:mother_accounting_code_entries_o] << mother_accounting_code_data_o
        end
        @data[:expenses_entries_o][:major_account_entries_o] << major_account_data_o
      end
    end
  

    def build_expenses!
      @data[:expenses_entries][:major_account_entries]  = []
      ### BUILD MAJOR ACCOUNT

      @expenses_major_group.major_accounts.where("major_accounts.id != ?", 10).each do |major_account|
        major_account_accounting_codes  = @expenses_accounting_codes.where("major_accounts.id = ?", major_account.id)

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data[:mother_accounting_code_entries] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.where("name != ? AND name != ?", "Investment Expenses", "Investment expenses").order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @expenses_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @expenses_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data[:accounting_code_entries] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
          end
          major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
        end
        @data[:expenses_entries][:major_account_entries] << major_account_data
      end
    end
  
  end
end
