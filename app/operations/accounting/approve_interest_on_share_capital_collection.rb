module Accounting
  class ApproveInterestOnShareCapitalCollection
    def initialize(interest_on_share_capital_collection:, user:)
      @interest_on_share_capital_collection = interest_on_share_capital_collection
      @interest_on_share_capital_collection_record = InterestOnShareCapitalCollectionRecord.where(interest_on_share_capital_collection_id: @interest_on_share_capital_collection.id)
      
      @user = user
      @approved_by = @user.full_name

      @voucher = ::Accounting::ProduceVoucehrForInterestOnShareCapital.new(
                                                interest_on_share_capital_collection: @interest_on_share_capital_collection,
                                                user: @user
                                                
                                                ).execute!
      
    end

    def execute!
      @voucher.save
      @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
      @interest_on_share_capital_collection.update!(
                                                      status: "approved",
                                                      refference_number: @voucher.reference_number,
                                                      posting_date: @voucher.date_posted
                                                    )
      @interest_on_share_capital_collection_record.each do |iosccr|
        #id_member= Member.where(uuid: iosccr.data["member_id"]).pluck(:id).shift
        savings_account_id = SavingsAccount.where(member_id: iosccr.data["member_id"] , savings_type_id: 1)
        cbu_account_id = SavingsAccount.where(member_id: iosccr.data["member_id"] , savings_type_id: 3)
        if iosccr.data["first_loop"] > 0 
          savings_account_id.each do |sai|
          if sai.balance.round(2) >= iosccr.data["member_savings_amount_distribute"]
            if sai.balance < sai.maintaining_balance
              Loan.where("member_id = ?" , iosccr.data["member_id"]).update(override_maintaining_balance: 'true')
            end
            savings_account_transaction = SavingsAccountTransaction.create!(
                                            amount: iosccr.data["member_savings_amount_distribute"],
                                            transacted_at: @interest_on_share_capital_collection.posting_date,
                                           created_at: @interest_on_share_capital_collection.posting_date,
                                            transaction_type: "withdraw",
                                            particular: @voucher.particular,
                                            voucher_reference_number: @interest_on_share_capital_collection.refference_number,
                                            savings_account_id: sai.id
                                          )
            savings_account_transaction.approve!(@approved_by)
            end
          end #end ng sai
          cbu_account_id.each do |cai|
          if cai.balance >= iosccr.data["member_savings_amount_distribute"]
            savings_account_transaction = SavingsAccountTransaction.create!(
                                            amount: iosccr.data["member_cbu_amount_distribute"],
                                            transacted_at: @interest_on_share_capital_collection.posting_date,
                                            created_at: @interest_on_share_capital_collection.posting_date,
                                            transaction_type: "withdraw",
                                            particular: @voucher.particular,
                                            voucher_reference_number: @interest_on_share_capital_collection.refference_number,
                                            savings_account_id: cai.id
                                          )
            savings_account_transaction.approve!(@approved_by)
          end
          end #end ng cbu
        end #end ng if
        
      end #end ng iosccr

    end
  end
end

