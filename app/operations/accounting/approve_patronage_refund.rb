module Accounting
  class ApprovePatronageRefund
    def initialize(patronage_refund_collection:, user:)
      @patronage_refund_collection = patronage_refund_collection
      @patronage_refund_collection_record = PatronageRefundCollectionRecord.where(patronage_refund_collection_id: @patronage_refund_collection.id )
      
      @user = user
      @approved_by = @user.full_name
    
      @voucher = ::Accounting::ProduceVoucherForPatronageRefund.new(patronage_refund_collection: @patronage_refund_collection, user: @user).execute!

    end
    def execute!
      
      @voucher.save!
      @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
      
      @patronage_refund_collection.update!(
        status: "approved",
        reference_number:  @voucher.reference_number,
        posting_date: @voucher.date_posted
      )
      
      @patronage_refund_collection_record.each do |prcr|
            id_member= Member.where(uuid: (prcr.data["member_id"])).pluck(:id).shift 
            savings_account_id = SavingsAccount.where(member_id: id_member, savings_type_id: 1)
            cbu_account_id = SavingsAccount.where(member_id: id_member, savings_type_id: 3)

            if prcr.data["member_loan_interest_rate"].to_f > 0
              savings_account_id.each do |sid|
                savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: prcr.data["dtotal_savings"],
                                          transacted_at: @patronage_refund_collection.posting_date,
                                          created_at: @patronage_refund_collection.posting_date,
                                          transaction_type: "interest",
                                          particular: @voucher.particular,
                                          voucher_reference_number: @patronage_refund_collection.reference_number,
                                          savings_account_id: sid.id
                )
                savings_account_transaction.approve!(@approved_by)
              end
              cbu_account_id.each do |cai|
                savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: prcr.data["dtotal_cbu"],
                                          transacted_at: @patronage_refund_collection.posting_date,
                                          created_at: @patronage_refund_collection.posting_date,
                                          transaction_type: "interest",
                                          particular: @voucher.particular,
                                          voucher_reference_number: @patronage_refund_collection.reference_number,
                                          savings_account_id: cai.id
                )
                savings_account_transaction.approve!(@approved_by)
              end
            end
        
      end
       
      

    end
  end
end
