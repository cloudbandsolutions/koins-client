module Accounting
  class GenerateExcelForInterestOnShareCapital
    def initialize(interest_on_share_capital_id:)
      @p = Axlsx::Package.new
      @interest_on_share_capital_id = interest_on_share_capital_id
      @IOSCRecord = InterestOnShareCapitalCollectionRecord.where(interest_on_share_capital_collection_id: @interest_on_share_capital_id).order("data ->> 'member_name' ASC")
    end
    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          #sheet.add_row ["#","Member Id","Name","Jan","Feb","Mar.","Apr","May","June","July","Aug","Sept.","Oct.","Nov.","Dec.","Total Share","Ave. Share","Interest Amount","Savings_amount","CBU"]

          sheet.add_row ["#","member_id","member_id_number","Name","Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec","Total_Share","Ave_Share","Interest_Amount","Savings_amount","CBU"]

          iter = 1
          @IOSCRecord.each_with_index do |interest_on_share, i|
            if interest_on_share.data["first_loop"] > 0
              data_row = []
              data_row << iter
              data_row << interest_on_share.data["member_id"]
              data_row << Member.find(interest_on_share.data["member_id"]).identification_number
              data_row << Member.find(interest_on_share.data["member_id"]).status
              data_row << interest_on_share.data["member_name"]
              interest_on_share.data["equtiytrans"].each do |isd|
                data_row << isd["amount"]
              end
              data_row << interest_on_share.data["first_loop"]
              data_row << interest_on_share.data["aver_first_loop"]
              data_row << interest_on_share.data["total_interest_amount"].round(2)
              data_row << interest_on_share.data["member_savings_amount_distribute"].round(2)
              data_row << interest_on_share.data["member_cbu_amount_distribute"].round(2)
              data_row << Member.find(interest_on_share.data["member_id"]).uuid
              sheet.add_row(data_row)
              iter = iter + 1
            end

          end



        end
      end
      @p

    end
  end
end
