module Accounting
  class GenerateChartOfAccountsPdf
    def initialize
      @data = {}
      @data[:company]            = Settings.company

    end

    def execute!
     @data[:major_groups] = []
     MajorGroup.select("*").order(:code).each do |major_group|
        major_group_data = {}
        major_group_data[:name] = major_group.name
        major_group_data[:code] = major_group.code
        major_group_data[:major_accounts] = []
        
        major_group.major_accounts.each do |major_account|
          major_account_data = {}
          major_account_data[:name] = major_account.name
          major_account_data[:code] = major_account.code
          major_account_data[:mother_accounting_codes] = []

          major_account.mother_accounting_codes.each do |mother_account|
            mother_account_data = {}
            mother_account_data[:name] = mother_account.name
            mother_account_data[:code] = mother_account.code
            mother_account_data[:accounting_code_categories] = []

            mother_account.accounting_code_categories.each do |accounting_category|
              accounting_category_data = {}
              accounting_category_data[:name] = accounting_category.name
              accounting_category_data[:code] = accounting_category.code
              accounting_category_data[:accounting_codes] = []
              
              accounting_category.accounting_codes.each do |accounting_code_final|
                accounting_code_final[:name] = accounting_code_final.name
                accounting_code_final[:code] = accounting_code_final.code

                      
                accounting_category_data[:accounting_codes] << accounting_code_final
              end
               

              mother_account_data[:accounting_code_categories] << accounting_category_data 
            end


            major_account_data[:mother_accounting_codes] << mother_account_data
          end

          major_group_data[:major_accounts] << major_account_data
        end
        
        @data[:major_groups] << major_group_data
      end
      @data
      
    end
  end
end
