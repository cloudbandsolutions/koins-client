module Accounting
  class GenerateAccountingEntryForPartronageRefund
    def initialize(patroange_refund_collection: ,user:)
     @patroange_refund_collection = patroange_refund_collection
    @user = user
    @book = "JVB"
    @branch = Branch.where(id: Settings.branch_id).first
    @c_working_date = ApplicationHelper.current_working_date
    @voucher = Voucher.new(
                  status: "pending",
                  branch: @branch,
                  book: @book,
                  date_prepared: @c_working_date,
                  prepared_by: @user.to_s
                  

                )
    end
    def execute!
      build_debit_entries
      #build_credit_entries
  
    build_credit_savings_entries
      
      build_credit_for_cbu_entries!
      @voucher
    end

    
    def build_debit_entries
      accounting_code_debit = AccountingCode.find(429)
      debit_amount = @patroange_refund_collection.total_interest_amount.round(2)
      
      journal_entry = JournalEntry.new(
                          amount: debit_amount,
                          post_type: "DR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry


    end
    
    def build_credit_for_cbu_entries!
      accounting_code_debit = AccountingCode.find(1890)
      #credit_amount = @interest_on_share_capital_collection.savings_account_amount
      credit_amount = @patroange_refund_collection.total_cbu_amount
      journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "CR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry
    end

    def build_credit_savings_entries

      accounting_code_debit = AccountingCode.find(102)
      credit_amount = @patroange_refund_collection.total_savings_amount
      journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "CR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry
   end

  end
end
