module Accounting
  class GenerateFinancialPosition
    def initialize(start_date:, end_date:, branch:)
      @data  = {}
      @data[:liabilities_entries] = {}
      @data[:assets_entries]      = {}
      @data[:fund_balance_entries]     = {}
      @data[:fund_balance_entries_a]   = {}
      @data[:fund_balance_entries_b]   = {}
      @data[:company]    = Settings.company
      @data[:branch]     = branch
      @data[:entries]    = []
      @data[:start_date] = start_date.to_date
      @data[:end_date]   = end_date.to_date

      @data[:m_total_beginning_debit] = 0
      @data[:m_total_beginning_credit] = 0
      @data[:g_total_beginning_debit] = 0
      @data[:g_total_beginning_credit] = 0
      @data[:o_total_beginning_debit] = 0
      @data[:o_total_beginning_credit] = 0
      @data[:m_total_current_debit] = 0
      @data[:m_total_current_credit] = 0
      @data[:g_total_current_debit] = 0
      @data[:g_total_current_credit] = 0
      @data[:o_total_current_debit] = 0
      @data[:o_total_current_credit] = 0
      @data[:m_total_ending_debit] = 0
      @data[:m_total_ending_credit] = 0
      @data[:g_total_ending_debit] = 0
      @data[:g_total_ending_credit] = 0
      @data[:o_total_ending_debit] = 0
      @data[:o_total_ending_credit] = 0

      @year             = @data[:end_date].year
      @previous_year    = @year - 1
       
      @assets_major_group = MajorGroup.where(name: "ASSETS").first
      @liabilities_major_group = MajorGroup.where(name: "LIABILITIES").first 
      @fund_balance_major_group = MajorGroup.where(name: "FUND BALANCE").first
      @fund_balance = @fund_balance_major_group.major_accounts.where(name: "Fund Balance").first
      @not_fund_balance = @fund_balance_major_group.major_accounts.where("name != ?","Fund Balance")

      @assets_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @assets_major_group.id)
      @liabilities_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @liabilities_major_group.id)
      @fund_balance_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ? AND major_accounts.id = ?", @fund_balance_major_group.id, @fund_balance.id)
      @fund_balance_accounting_codess = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ? AND major_accounts.id IN (?)", @fund_balance_major_group.id, @not_fund_balance.pluck(:id))

      @journal_entries  = JournalEntry.joins(:voucher)
      @start_date = @data[:start_date]
      @end_date   = @data[:end_date]

      @mutual_benefit = AccountingFund.where(name: "Mutual Benefit Fund").first 
      @general_fund = AccountingFund.where(name: "General Fund").first
      @optional_fund = AccountingFund.where(name: "Optional Fund").first

      @entries  = []
    end

    def execute!
      build_assets!
      build_liabilities!
      build_fund_balance!
      build_fund_balance_b!
      build_fund_balance_a!

      @data[:entries] = @entries

      @data
    end

    def build_fund_balance_a!
      @data[:fund_balance_entries_a][:major_account_entries_a] = []
      ### BUILD MAJOR ACCOUNT

      @fund_balance_major_group.major_accounts.where("major_accounts.id = ?", 6).each do |major_account|
        major_account_accounting_codes  = @fund_balance_accounting_codess.where("major_accounts.id = ?", major_account.id)

        major_account_data_a = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data_a[:mother_accounting_code_entries_a] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.where("name != ? AND name != ? AND name != ? AND name != ?", "Member's Fee / Dues", "Net Member's Contribution", "Net Premiums", "Other Income").order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @fund_balance_accounting_codess.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data_a = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data_a[:accounting_code_category_entries_a]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @fund_balance_accounting_codess.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data_a = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data_a[:accounting_code_entries_a] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data_a = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data_a[:accounting_code_entries_a] << accounting_code_data_a
              end
            mother_accounting_code_data_a[:accounting_code_category_entries_a] << accounting_code_category_data_a
          end
          major_account_data_a[:mother_accounting_code_entries_a] << mother_accounting_code_data_a
        end
        @data[:fund_balance_entries_a][:major_account_entries_a] << major_account_data_a
      end
    end


    def build_fund_balance_b!
      @data[:fund_balance_entries_b][:major_account_entries_b]  = []
      ### BUILD MAJOR ACCOUNT

      @fund_balance_major_group.major_accounts.where("major_accounts.id IN (?)", @not_fund_balance.pluck(:id)).each do |major_account|
        major_account_accounting_codes  = @fund_balance_accounting_codess.where("major_accounts.id = ?", major_account.id)

        major_account_data_b = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data_b[:mother_accounting_code_entries_b] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.where("name != ? AND name != ?", "Investment Expenses", "Investment Income").order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @fund_balance_accounting_codess.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data_b = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data_b[:accounting_code_category_entries_b]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @fund_balance_accounting_codess.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data_b = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data_b[:accounting_code_entries_b] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data_b = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data_b[:accounting_code_entries_b] << accounting_code_data_b
              end
            mother_accounting_code_data_b[:accounting_code_category_entries_b] << accounting_code_category_data_b
          end
          major_account_data_b[:mother_accounting_code_entries_b] << mother_accounting_code_data_b
        end
        @data[:fund_balance_entries_b][:major_account_entries_b] << major_account_data_b
      end
    end

    def build_fund_balance!
      @data[:fund_balance_entries][:major_account_entries]  = []
      ### BUILD MAJOR ACCOUNT

      @fund_balance_major_group.major_accounts.where("major_accounts.id = ?", @fund_balance.id).each do |major_account|
        major_account_accounting_codes  = @fund_balance_accounting_codes.where("major_accounts.id = ?", major_account.id)

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data[:mother_accounting_code_entries] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @fund_balance_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @fund_balance_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data[:accounting_code_entries] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
          end
          major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
        end
        @data[:fund_balance_entries][:major_account_entries] << major_account_data
      end
    end


    def build_liabilities!
      @data[:liabilities_entries][:major_account_entries]  = []
      ### BUILD MAJOR ACCOUNT

      @liabilities_major_group.major_accounts.each do |major_account|
        major_account_accounting_codes  = @liabilities_accounting_codes.where("major_accounts.id = ?", major_account.id)

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data[:mother_accounting_code_entries] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @liabilities_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @liabilities_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data[:accounting_code_entries] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
          end
          major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
        end
        @data[:liabilities_entries][:major_account_entries] << major_account_data
      end
    end

    def build_assets!
      @data[:assets_entries][:major_account_entries]  = []
      ### BUILD MAJOR ACCOUNT

      @assets_major_group.major_accounts.each do |major_account|
        major_account_accounting_codes  = @assets_accounting_codes.where("major_accounts.id = ?", major_account.id)

        major_account_data = {
          id: major_account.id,
          name: major_account.to_s
        }

        major_account_data[:mother_accounting_code_entries] = []

        ### BUILD MOTHER ACCOUNTING CODE
        major_account.mother_accounting_codes.order("code ASC").each do |mother_accounting_code|
          mother_accounting_code_accounting_codes = @assets_accounting_codes.where("mother_accounting_codes.id = ?", mother_accounting_code.id)

          mother_accounting_code_data = {
            id: mother_accounting_code.id,
            name: mother_accounting_code.to_s
          }

          mother_accounting_code_data[:accounting_code_category_entries]  = []

          ### BUILD ACCOUNTING CODE CATEGORY
          mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
            accounting_code_category_accounting_codes = @assets_accounting_codes.where("accounting_code_categories.id = ?", accounting_code_category.id)

            accounting_code_category_data = {
              id: accounting_code_category.id,
              name: accounting_code_category.to_s,
             }

            accounting_code_category_data[:accounting_code_entries] = []
            
            ### BUILD ACCOUNTING CODE
            accounting_code_category_accounting_codes.each do |accounting_code|

                # entry = {}
                # entry[:accounting_code] = "#{accounting_code.to_s}"

                m_beginning_debit   = 0.00
                m_beginning_credit  = 0.00
                g_beginning_debit   = 0.00
                g_beginning_credit  = 0.00
                o_beginning_debit   = 0.00
                o_beginning_credit  = 0.00

                
                m_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_credit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_beginning_debit += @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared < ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")                                  
              
                if accounting_code.debit?
                  m_beginning_debit = m_beginning_debit - m_beginning_credit
                  m_beginning_credit = 0
                  g_beginning_debit = g_beginning_debit - g_beginning_credit
                  g_beginning_credit = 0
                  o_beginning_debit = o_beginning_debit - o_beginning_credit
                  o_beginning_credit = 0

                  if m_beginning_debit < 0
                    m_beginning_credit  = m_beginning_debit * -1
                    m_beginning_debit   = 0.00
                  end

                  if g_beginning_debit < 0
                    g_beginning_credit  = g_beginning_debit * -1
                    g_beginning_debit   = 0.00
                  end

                  if o_beginning_debit < 0
                    o_beginning_credit  = o_beginning_debit * -1
                    o_beginning_debit   = 0.00
                  end
                elsif accounting_code.credit?
                  m_beginning_credit  = m_beginning_credit - m_beginning_debit
                  m_beginning_debit   = 0
                  g_beginning_credit  = g_beginning_credit - g_beginning_debit
                  g_beginning_debit   = 0
                  o_beginning_credit  = o_beginning_credit - o_beginning_debit
                  o_beginning_debit   = 0

                  if m_beginning_credit < 0
                    m_beginning_debit   = m_beginning_credit * -1
                    m_beginning_credit  = 0.00
                  end

                  if g_beginning_credit < 0
                    g_beginning_debit   = g_beginning_credit * -1
                    g_beginning_credit  = 0.00
                  end

                  if o_beginning_credit < 0
                    o_beginning_debit   = o_beginning_credit * -1
                    o_beginning_credit  = 0.00
                  end
                end

                
                m_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                m_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @mutual_benefit.id
                                  ).sum("journal_entries.amount")

                g_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                g_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @general_fund.id
                                  ).sum("journal_entries.amount")

                o_current_credit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'CR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                o_current_debit = @journal_entries.where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ?
                                    AND vouchers.date_prepared >= ?
                                    AND vouchers.date_prepared <= ?
                                    AND vouchers.accounting_fund_id = ?
                                    AND journal_entries.post_type = 'DR'",
                                    accounting_code.id,
                                    @start_date,
                                    @end_date,
                                    @optional_fund.id
                                  ).sum("journal_entries.amount")

                m_ending_credit = m_beginning_credit + m_current_credit
                m_ending_debit  = m_beginning_debit + m_current_debit
                g_ending_credit = g_beginning_credit + g_current_credit
                g_ending_debit  = g_beginning_debit + g_current_debit                
                o_ending_credit = o_beginning_credit + o_current_credit
                o_ending_debit  = o_beginning_debit + o_current_debit

                if accounting_code.debit?
                  m_ending_debit = m_ending_debit - m_ending_credit
                  m_ending_credit = 0
                  g_ending_debit = g_ending_debit - g_ending_credit
                  g_ending_credit = 0
                  o_ending_debit = o_ending_debit - o_ending_credit
                  o_ending_credit = 0

                  if m_ending_debit < 0
                    m_ending_credit = m_ending_debit * -1
                    m_ending_debit  = 0.00
                  end

                  if g_ending_debit < 0
                    g_ending_credit = g_ending_debit * -1
                    g_ending_debit  = 0.00
                  end

                  if o_ending_debit < 0
                    o_ending_credit = o_ending_debit * -1
                    o_ending_debit  = 0.00
                  end
                elsif accounting_code.credit?
                  m_ending_credit = m_ending_credit - m_ending_debit
                  m_ending_debit = 0
                  g_ending_credit = g_ending_credit - g_ending_debit
                  g_ending_debit = 0
                  o_ending_credit = o_ending_credit - o_ending_debit
                  o_ending_debit = 0

                  if m_ending_credit < 0
                    m_ending_debit  = m_ending_credit * -1
                    m_ending_credit = 0.00
                  end

                  if g_ending_credit < 0
                    g_ending_debit  = g_ending_credit * -1
                    g_ending_credit = 0.00
                  end

                  if o_ending_credit < 0
                    o_ending_debit  = o_ending_credit * -1
                    o_ending_credit = 0.00
                  end
                end

                 accounting_code_data = {
                  id: accounting_code.id,
                  name: accounting_code.to_s,
                  m_ending_debit: m_ending_debit,
                  m_ending_credit: m_ending_credit,
                  g_ending_debit: g_ending_debit,
                  g_ending_credit: g_ending_credit,
                  o_ending_debit: o_ending_debit,
                  o_ending_credit: o_ending_credit
                }

                accounting_code_category_data[:accounting_code_entries] << accounting_code_data
              end
            mother_accounting_code_data[:accounting_code_category_entries] << accounting_code_category_data
          end
          major_account_data[:mother_accounting_code_entries] << mother_accounting_code_data
        end
        @data[:assets_entries][:major_account_entries] << major_account_data
      end
    end
  
  end
end
