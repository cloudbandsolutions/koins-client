module Accounting
  class GenerateSubReferenceNumber
    def initialize(accounting_fund:, book:)
      @accounting_fund  = accounting_fund
      @book             = book
      @prefix           = @accounting_fund.prefix
      @key              = "#{@book.try(:upcase)}_#{@prefix}"
    end

    def execute!
			sub_reference_number_counter  = Setting.where(
                                        key: @key
                                      ).first

      number  = 1
      if sub_reference_number_counter.nil?
        Setting.create!(key: @key, val: "1")
        number = 1
      else
        number      = sub_reference_number_counter.val.to_i
        new_number  = number + 1
        #update the counter
        Setting.where(key: @key).first.update!(val: new_number.to_s)
        number      = new_number
      end

      sub_reference_number  = "#{@book.to_s}-#{@prefix}-#{number.to_s.rjust(10, "0")}"

      sub_reference_number
    end
  end
end
