module Accounting
  class GenerateExcelForPatronageRefund
    def initialize(patronage_id:)
      @p = Axlsx::Package.new
      @patronage_id = patronage_id 
      @patronage_refund_collection_records = PatronageRefundCollectionRecord.where(patronage_refund_collection_id:  @patronage_id ).order(" data ->> 'member_name' ASC"   )
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet| 
          sheet.add_row ["#","member_id","member_id_number","Name","Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec","total_interest","Patronage_Refund","savings","cbu"]

        iter = 1
        @patronage_refund_collection_records.each do |prcr|
          if prcr.data["member_loan_interest_amount"].to_f > 0
            data_row = []
            data_row << (iter)
            data_row << prcr.data["member_id"]
            data_row << Member.find(prcr.data["member_id"]).identification_number
            data_row << Member.find(prcr.data["member_id"]).status
            data_row << prcr.data["member_name"]
            prcr.data["reasons"].each do |r|
              data_row << r["amount"]
            end
            data_row << prcr.data["member_loan_interest_amount"]
            data_row << (prcr.data["member_loan_interest_rate"].to_f)
            data_row << (prcr.data["dtotal_savings"].to_f)
            data_row << (prcr.data["dtotal_cbu"].to_f)
            data_row << Member.find(prcr.data["member_id"]).uuid

            sheet.add_row(data_row)
            iter = iter + 1
          end
        end

          
        end

      end
      @p 
    end

  end
end
