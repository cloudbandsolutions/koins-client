module Accounting
	class GenerateReverseEntryForLoan
    require 'application_helper'

		def initialize(voucher:, particular:)
      @current_working_date = ApplicationHelper.current_working_date
			@voucher = voucher
			@particular = particular
		end

		def execute!
			reverse_voucher = Voucher.new(
                  reference_number: Vouchers::VoucherNumber.new(book: "JVB", branch: @voucher.branch).execute!,
                  date_prepared: @current_working_date,
                  particular: @particular,
                  book: 'JVB',
                  branch: @voucher.branch,
                  prepared_by: @voucher.prepared_by,
                  approved_by: @voucher.approved_by,
                  status: @voucher.status
                )

      @voucher.voucher_options.each do |voucher_option|
        reverse_voucher.voucher_options << VoucherOption.new(
                                            name: voucher_option.name,
                                            val: voucher_option.val
                                          )
      end

      @voucher.journal_entries.each do |journal_entry|
        reversed_journal_entry = JournalEntry.new(
                                  amount: journal_entry.amount,
                                  post_type: "#{journal_entry.post_type == 'DR' ? 'CR' : 'DR'}",
                                  accounting_code: journal_entry.accounting_code
                                )

        reverse_voucher.journal_entries << reversed_journal_entry
      end

      reverse_voucher.save!

      reverse_voucher
		end
	end
end
