module Accounting
  class GenerateFiscalYearAccountingCodes
    def initialize(year:)
      @year                                 = year
      @accounting_codes                     = AccountingCode.all
      @current_fiscal_year_accounting_codes = FiscalYearAccountingCode.joins(:accounting_code)
                                                .where(year: @year)
      @prev_fiscal_year_accounting_codes    = FiscalYearAccountingCode.joins(:accounting_code)
                                                .where(year: @year - 1)
      @journal_entries                      = JournalEntry.joins(:voucher)
                                                .where(
                                                  "extract(year from vouchers.date_prepared) = ?",
                                                  @year
                                                )
    end

    def execute!
      result = []
      @accounting_codes.each do |accounting_code|
        previous_fiscal_year_accounting_code  = @prev_fiscal_year_accounting_codes.where(
                                                  accounting_code_id: accounting_code.id
                                                ).first

        beginning_balance = 0.00
        ending_balance    = 0.00
        beginning_credit  = 0.00
        ending_credit     = 0.00
        beginning_debit   = 0.00
        ending_debit      = 0.00


        if !previous_fiscal_year_accounting_code
          beginning_credit  = 0.00
          beginning_debit   = 0.00
          beginning_balance = 0.00
        else
          beginning_credit  = previous_fiscal_year_accounting_code.try(:ending_credit)
          beginning_debit   = previous_fiscal_year_accounting_code.try(:ending_debit)
          beginning_balance = previous_fiscal_year_accounting_code.try(:ending_balance)
        end

        ending_credit = beginning_credit + @journal_entries.where("journal_entries.accounting_code_id = ? AND journal_entries.post_type = ?", accounting_code.id, "CR").sum("journal_entries.amount")
        ending_debit  = beginning_debit + @journal_entries.where("journal_entries.accounting_code_id = ? AND journal_entries.post_type = ?", accounting_code.id, "DR").sum("journal_entries.amount")

        fiscal_year_accounting_code = @current_fiscal_year_accounting_codes.where(
                                        year: @year,
                                        accounting_code_id: accounting_code.id
                                      ).last

        if !fiscal_year_accounting_code
          fiscal_year_accounting_code = FiscalYearAccountingCode.new(year: @year, accounting_code: accounting_code)
        end

        net = 0.00
        if accounting_code.debit?
          net = ending_debit - ending_credit
        elsif accounting_code.credit?
          net = ending_credit - ending_debit
        end

        ending_balance  = net

        fiscal_year_accounting_code.beginning_balance = beginning_balance
        fiscal_year_accounting_code.ending_balance    = ending_balance
        fiscal_year_accounting_code.beginning_credit  = beginning_credit
        fiscal_year_accounting_code.ending_credit     = ending_credit
        fiscal_year_accounting_code.beginning_debit   = beginning_debit
        fiscal_year_accounting_code.ending_debit      = ending_debit

        fiscal_year_accounting_code.save!

        result << fiscal_year_accounting_code
      end

      result
    end
  end
end
