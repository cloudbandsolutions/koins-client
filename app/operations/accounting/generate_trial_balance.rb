module Accounting
  class GenerateTrialBalance
    def initialize(start_date:, end_date:, branch:, accounting_fund:)
      @data  = {}
      @data[:company]    = Settings.company
      @data[:branch]     = branch
      @data[:accounting_fund]  = accounting_fund
      @data[:entries]    = []
      @data[:start_date] = start_date.to_date
      @data[:end_date]   = end_date.to_date

      @data[:total_beginning_debit] = 0
      @data[:total_beginning_credit] = 0
      @data[:total_current_debit] = 0
      @data[:total_current_credit] = 0
      @data[:total_ending_debit] = 0
      @data[:total_ending_credit] = 0

      @year             = @data[:end_date].year
      @previous_year    = @year - 1
       
      @accounting_fund = @data[:accounting_fund]

      @accounting_codes = AccountingCode.select("*").order(:code)

      @journal_entries  = JournalEntry.joins(:voucher).where(
                            "vouchers.book != 'MISC'"
                          )

      @exempted_accounting_code_ids  = Settings.exempted_accounting_code_ids || [337]

      if @accounting_fund.present?
        @journal_entries  = @journal_entries.where(
                              "vouchers.accounting_fund_id = ?",
                              accounting_fund
                            )
      end

      @start_date = @data[:start_date]
      @end_date   = @data[:end_date]
     
      @expenses_group_id  = Settings.expenses_group_id
      @expense_group      = MajorGroup.find(@expenses_group_id)

      @revenue_group_id   = Settings.revenue_group_id
      @revenue_group      = MajorGroup.find(@revenue_group_id)

      @revenue_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @revenue_group.id)
      @expense_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @expense_group.id)

      @entries  = []
    end

    def execute!
      @accounting_codes.each do |accounting_code|
        entry = {}
        entry[:accounting_code] = "#{accounting_code.to_s}"

        beginning_debit   = 0.00
        beginning_credit  = 0.00
       
      
        if @expense_accounting_codes.pluck(:id).include?(accounting_code.id) || @revenue_accounting_codes.pluck(:id).include?(accounting_code.id)
          beginning_credit += @journal_entries.where(
                              "vouchers.status = 'approved' 
                              AND journal_entries.accounting_code_id = ?
                              AND vouchers.date_posted < ?
                              AND journal_entries.post_type = 'CR'
                              AND journal_entries.is_year_end_closing IS ?
                              AND extract(year from vouchers.date_posted) = ?",
                              accounting_code.id,
                              @start_date,
                              nil,
                              @year
                            ).sum("journal_entries.amount")

          beginning_debit += @journal_entries.where(
                              "vouchers.status = 'approved' 
                              AND journal_entries.accounting_code_id = ?
                              AND vouchers.date_posted < ?
                              AND journal_entries.post_type = 'DR'
                              AND journal_entries.is_year_end_closing IS ?
                              AND extract(year from vouchers.date_posted) = ?",
                              accounting_code.id,
                              @start_date,
                              nil,
                              @year
                            ).sum("journal_entries.amount")
        else
          if @exempted_accounting_code_ids.include?(accounting_code.id)
            beginning_credit += JournalEntry.joins(:voucher).where(
                                "vouchers.status = 'approved' 
                                AND journal_entries.accounting_code_id = ?
                                AND vouchers.date_posted < ?
                                AND journal_entries.post_type = 'CR'",
                                accounting_code.id,
                                @start_date
                              ).sum("journal_entries.amount")

            beginning_debit += JournalEntry.joins(:voucher).where(
                                "vouchers.status = 'approved' 
                                AND journal_entries.accounting_code_id = ?
                                AND vouchers.date_posted < ?
                                AND journal_entries.post_type = 'DR'",
                                accounting_code.id,
                                @start_date
                              ).sum("journal_entries.amount")
          else
            beginning_credit += @journal_entries.where(
                                "vouchers.status = 'approved' 
                                AND journal_entries.accounting_code_id = ?
                                AND vouchers.date_posted < ?
                                AND journal_entries.post_type = 'CR'
                                AND journal_entries.is_year_end_closing IS ?",
                                accounting_code.id,
                                @start_date,
                                nil
                              ).sum("journal_entries.amount")

            beginning_debit += @journal_entries.where(
                                "vouchers.status = 'approved' 
                                AND journal_entries.accounting_code_id = ?
                                AND vouchers.date_posted < ?
                                AND journal_entries.post_type = 'DR'
                                AND journal_entries.is_year_end_closing IS ?",
                                accounting_code.id,
                                @start_date,
                                nil
                              ).sum("journal_entries.amount")
          end
        end

      
        if accounting_code.debit?
          beginning_debit = beginning_debit - beginning_credit
          beginning_credit = 0

          if beginning_debit < 0
            beginning_credit  = beginning_debit * -1
            beginning_debit   = 0.00
          end
        elsif accounting_code.credit?
          beginning_credit  = beginning_credit - beginning_debit
          beginning_debit   = 0

          if beginning_credit < 0
            beginning_debit   = beginning_credit * -1
            beginning_credit  = 0.00
          end
        end

        
        current_credit = @journal_entries.where(
                            "vouchers.status = 'approved' 
                            AND journal_entries.accounting_code_id = ?
                            AND vouchers.date_posted >= ?
                            AND vouchers.date_posted <= ?
                            AND journal_entries.post_type = 'CR'
                            AND journal_entries.is_year_end_closing IS ?",
                            accounting_code.id,
                            @start_date,
                            @end_date,
                            nil
                          ).sum("journal_entries.amount")

        current_debit = @journal_entries.where(
                            "vouchers.status = 'approved' 
                            AND journal_entries.accounting_code_id = ?
                            AND vouchers.date_posted >= ?
                            AND vouchers.date_posted <= ?
                            AND journal_entries.post_type = 'DR'
                            AND journal_entries.is_year_end_closing IS ?",
                            accounting_code.id,
                            @start_date,
                            @end_date,
                            nil
                          ).sum("journal_entries.amount")

        #if accounting_code.debit?
        #  current_debit = current_debit - current_credit
        #  current_credit = 0
        #elsif accounting_code.credit?
        #  current_credit = current_credit - current_debit
        #  current_debit = 0
        #end

        ending_credit = beginning_credit + current_credit
        ending_debit  = beginning_debit + current_debit

        if accounting_code.debit?
          ending_debit = ending_debit - ending_credit
          ending_credit = 0

          if ending_debit < 0
            ending_credit = ending_debit * -1
            ending_debit  = 0.00
          end
        elsif accounting_code.credit?
          ending_credit = ending_credit - ending_debit
          ending_debit = 0

          if ending_credit < 0
            ending_debit  = ending_credit * -1
            ending_credit = 0.00
          end
        end

        entry[:beginning_debit]   = beginning_debit
        entry[:beginning_credit]  = beginning_credit
        entry[:current_debit]     = current_debit
        entry[:current_credit]    = current_credit
        entry[:ending_debit]      = ending_debit
        entry[:ending_credit]     = ending_credit

        if entry[:beginning_debit] == 0 and entry[:beginning_credit] == 0 and entry[:current_debit] == 0 and entry[:current_credit] == 0 and entry[:ending_debit] == 0 and entry[:ending_credit] == 0
        else
          @entries << entry

          @data[:total_beginning_debit]   += beginning_debit
          @data[:total_beginning_credit]  += beginning_credit
          @data[:total_current_debit]     += current_debit
          @data[:total_current_credit]    += current_credit
          @data[:total_ending_debit]      += ending_debit
          @data[:total_ending_credit]     += ending_credit
        end
      end

      @data[:entries] = @entries

      @data
    end
  end
end
