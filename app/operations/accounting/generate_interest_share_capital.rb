module Accounting
  class GenerateInterestShareCapital
    def initialize(start_date:, end_date:, equity_interest:)
      @start_date = start_date.to_date
      @end_date = end_date
      @previous_year = @start_date.year - 1
      @equity_interest = (equity_interest.to_f / 100)
      #raise @equity_interest.inspect
      @data = {}
      @data[:equity_interest_rate] = equity_interest.to_f
      @data[:total_details] = 0     
      @data[:end_date] = @end_date
     @data[:test] = []
    end
    def execute!
      #members = Member.where("status <> ? and status <> ?","cleared","transferred")
      members = Member.active
      itersample = 0
      aveshare = 0
      last_loop = 0
      dtotal_savings_amount = 0
      dtotal_cbu_amount = 0
      EquityType.all.each do |equity_type|
        first_loop = 0
        gTotal_share_interest = 0
        total_savings_amount = 0
        total_cbu_amount = 0
        members.each_with_index do |m, i|
          temp = {}
          temp[:iiter] = i + 1
          temp[:member_id] = m.id
          temp[:member_name] = m.full_name
          temp[:equtiytrans] = []
            
            equity_account = EquityAccount.where(member_id: m.id, equity_type_id: equity_type.id)
            equity_account_id = equity_account.ids

            last_month_transaction = EquityAccountTransaction.where("extract(year from transacted_at) <= ? and equity_account_id = ?", @previous_year, equity_account_id).last
          
            if !last_month_transaction
              last_month_ending_balance = 0
            else
               last_month_ending_balance = last_month_transaction[:ending_balance]
            end
            
            
            start_date_details = @start_date.to_date
            
            present_year = 0
            eee = 0
            jjj = 0
            ttt = 0
            while(start_date_details <= @end_date.to_date ) do
              dep = {}
              dep[:equity_account_details] = []
              dep[:month] = Date::MONTHNAMES[start_date_details.month]
              start_date_year = start_date_details.year
              start_date_month = start_date_details.month
              
              equity_account_transaction_details = EquityAccountTransaction.where(
                                                                    "extract(year from transacted_at) = ? and 
                                                                     extract(month from transacted_at) = ? and 
                                                                     equity_account_id = ?",
                                                                     start_date_year,
                                                                     start_date_month,
                                                                     equity_account_id
                                                                     ).last
             if start_date_month == 1
               if !equity_account_transaction_details
                dep[:amount] = last_month_ending_balance
               else
                dep[:amount] = equity_account_transaction_details.ending_balance
               end
               jjj = dep[:amount]
             else
              
              if !equity_account_transaction_details
            
                dep[:amount] = eee
                #if eee == 0 
                 # dep[:amount] = last_month_ending_balance
                #else
                #  dep[:amount] = eee
                #end
                #  dep[:transaction_type] = "running balance"
                jjj = eee
              else
                dep[:amount] = equity_account_transaction_details.ending_balance
                dep[:transaction_type] = "Deposit/Withdraw"
                jjj = equity_account_transaction_details.ending_balance
              end
              
             end
              eee = jjj
    


              start_date_details = start_date_details + 1.month
              ttt += dep[:amount].round(2)
              temp[:equtiytrans] << dep
            
            end #end of month loop
            @data[:total_details] += ttt

            temp[:first_loop] =  ttt.to_f
            temp[:aver_first_loop] = (temp[:first_loop] / 12).round(2)
            
            temp[:g_total_interest_amount] = (temp[:aver_first_loop] * @equity_interest).round(2)

            total_savings_amount += (temp[:g_total_interest_amount] * 0.9).round(2)
            total_cbu_amount += (temp[:g_total_interest_amount] * 0.1).round(2)
            temp[:member_savings_amount_distribute] =  (temp[:g_total_interest_amount] * 0.9).round(2)
            
            temp[:member_cbu_amount_distribute] = ( temp[:g_total_interest_amount] * 0.1).round(2)
            gTotal_share_interest += temp[:g_total_interest_amount].round(2)

            sum_total_amount = (temp[:member_savings_amount_distribute] + temp[:member_cbu_amount_distribute]).round(2)

            if sum_total_amount > temp[:g_total_interest_amount]
            
              temp[:total_interest_amount] = sum_total_amount 
              
            else
              temp[:total_interest_amount] = temp[:g_total_interest_amount]
            end
            

            gTotal_share_interest += temp[:total_interest_amount].round(2)
            

            @data[:test] << temp
          
        end #member loop end
        last_loop += gTotal_share_interest
        dtotal_savings_amount = total_savings_amount
        dtotal_cbu_amount = total_cbu_amount
      end #equity loop end
      @data[:total_share]= @data[:total_details]
      @data[:average_share]= aveshare
      @data[:total_savings_account_amount] = (dtotal_savings_amount).round(2)
      @data[:total_cbu_account_amount] = (dtotal_cbu_amount).round(2)
#      raise @data[:average_share].inspect
      @data[:gtotal_share] = (last_loop).round(2)
      #raise @data[:gtotal_share].inspect
      @data
    end
  
  end
end
