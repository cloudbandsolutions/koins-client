module Savings
	class CurrentBranchesSavingsAccounts
		def initialize(branch_ids:)
			@branch_ids = branch_ids
		end

		def execute!
			savings_accounts = []

      SavingsAccount.where("branch_id IN (?)", @branch_ids).each do |savings_account|
        sa = savings_account.to_hash
        savings_accounts << sa
      end

      savings_accounts
		end
	end
end