module Savings
  class GenerateJournalVoucherFromTransaction
    def initialize(savings_account_transaction:, prepared_by:, approved_by:)
    	@savings_account_transaction = savings_account_transaction
      @prepared_by = prepared_by
      @approved_by = approved_by
    end

    def execute!
      if ["deposit"].include? @savings_account_transaction.transaction_type
        book = "CRB"
      else
        book = "JVB"
      end
      voucher = Voucher.new(
                  particular: @savings_account_transaction.particular,
                  reference_number: Vouchers::VoucherNumber.new(book: book, branch: @savings_account_transaction.savings_account.branch).execute!,
                  status: 'approved',
                  book: book,
                  branch_id: @savings_account_transaction.savings_account.member.branch.id,
                  date_prepared: @savings_account_transaction.transacted_at,
                  prepared_by: @prepared_by,
                  approved_by: @approved_by
                )

      if @savings_account_transaction.transaction_type == "withdraw"
        je_debit = JournalEntry.new(
                    amount: @savings_account_transaction.amount,
                    post_type: "DR",
                    accounting_code: @savings_account_transaction.savings_account.savings_type.withdraw_accounting_code
                  )

        voucher.journal_entries << je_debit

        je_credit = JournalEntry.new(
                      amount: @savings_account_transaction.amount,
                      post_type: "CR",
                      accounting_code: @savings_account_transaction.accounting_code
                    )

        voucher.journal_entries << je_credit


      elsif ["deposit", "fund_transfer_deposit"].include? @savings_account_transaction.transaction_type
        je_debit = JournalEntry.new(
                    amount: @savings_account_transaction.amount,
                    post_type: "DR",
                    accounting_code: @savings_account_transaction.accounting_code
                  )

        voucher.journal_entries << je_debit

        je_credit = JournalEntry.new(
                      amount: @savings_account_transaction.amount,
                      post_type: "CR",
                      accounting_code: @savings_account_transaction.savings_account.savings_type.deposit_accounting_code
                    )

        voucher.journal_entries << je_credit
      else
        # TODO: ERROR
        raise "Error in Savings Account Service"
      end

      voucher.save!
    end
  end
end