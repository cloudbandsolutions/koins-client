module Savings
  class CreateInterestAndTaxRecords
    def initialize(closing_date:, savings_type:, monthly_tax_rate:, monthly_interest_rate:, branch:)
      @closing_date           = closing_date
      @savings_type           = savings_type
      @monthly_tax_rate       = monthly_tax_rate
      @monthly_interest_rate  = monthly_interest_rate
      @branch                 = branch
      @savings_accounts       = SavingsAccount.active.where(branch_id: @branch.id, savings_type_id: savings_type.id)
      @records = []

      @total_interest         = 0.00
      @total_tax              = 0.00
    end

    def execute!
      @savings_accounts.each do |savings_account|
        record = Savings::ComputeClosingInterestAndTax.new(
                            savings_account: savings_account,
                            closing_date: closing_date,
                            annual_interest_rate: annual_interest_rate,
                            tax_rate: tax_rate
                          ).execute!

        if !record.nil?
          @records << record
          @total_interest += record[:total_interest]
          @total_tax += record[:total_tax]
        end
      end
    end

    private
  end
end
