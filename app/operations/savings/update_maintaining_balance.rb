module Savings
	class UpdateMaintainingBalance
		def initialize(savings_account:)
			@savings_account = savings_account
		end

		def execute!
			member = @savings_account.member
      #active_loans = member.loans.active.joins(:loan_product => :savings_type).where("savings_types.id = ? and loans.date_approved <= ?", @savings_account.savings_type.id, "2019-07-01")
      

      #Non Entry level Maintaining Balance
      active_loans=member.loans.active.where("loan_product_id NOT IN (?)",["14","5","6","23","30"])
      maintaining_balance = []
        active_loans.each do |b|
          if b[:date_approved] < Date.parse("2019-07-15")
              if b[:amount].to_i >= 10001 
                maintaining_balance << b.maintaining_balance_val.to_i
                if b[:amount].to_i <= 10000
                maintaining_balance << 0
                end
              end
          end
          
          if b[:date_approved] >= Date.parse("2019-07-15")
              maintaining_balance << b.maintaining_balance_val.to_i
          end

        end

      #For Entry Level Maintaining_Balance

      entry_level_loans= LoanProduct.where(is_entry_point: true).ids
        entry_level_loans.each do |el|
          active_lev= member.loans.active.where("loan_product_id = ?",el)
          active_lev.each do |entry_loans|     
            maintaining_balance << entry_loans.maintaining_balance_val.to_i
          end
        end
      
      @bal= maintaining_balance.sum
      @savings_account.update!(maintaining_balance: @bal.to_i)
		  
    end
	end
end
