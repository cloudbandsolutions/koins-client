module Savings
  class RehashAccount
    def initialize(savings_account:)
      @savings_account              = savings_account

      
      @savings_account_transactions = SavingsAccountTransaction.where(
                                        "savings_account_id = ? AND status IN (?)", 
                                        @savings_account.id, ["approved", "reversed"]
                                      ).order("transacted_at ASC, id ASC")

      @original_maintaining_balance = @savings_account.maintaining_balance
    end

    def execute!
      @savings_account.update!(balance: 0.00, maintaining_balance: -9999999)
      @savings_account_transactions.each do |sat|
        sat.update!(
          updated_at: sat.transaction_date,
          beginning_balance: 0.00,
          ending_balance: 0.00,
          status: "pending"
        )
      end

      @savings_account_transactions.each do |sat|
        sat.beginning_balance = SavingsAccount.find(@savings_account.id).balance
        sat.approve!("")
      end

      @savings_account.update!(maintaining_balance: @original_maintaining_balance)
      @savings_account
    end
  end
end
