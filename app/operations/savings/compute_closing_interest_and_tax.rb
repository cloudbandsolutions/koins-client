module Savings
	class ComputeClosingInterestAndTax
		def initialize(savings_account:, closing_date:, annual_interest_rate:, tax_rate:)
			@savings_account      = savings_account
			@closing_date         = closing_date
			@annual_interest_rate = annual_interest_rate
			@tax_rate             = tax_rate
		end

		def execute!
			data = []

      # Vaid payment collections
      #valid_reference_numbers = PaymentCollection.approved.where("extract(month from updated_at) = ? AND extract(year from updated_at) = ?", @closing_date.month, @closing_date.year).pluck(:reference_number)
      # for postgresql
      #savings_account_transactions = SavingsAccountTransaction.approved.where(
      #                                  "voucher_reference_number IN (?) AND savings_account_id = ? AND transaction_type IN (?) AND extract(month from created_at) = ?", 
      #                                  valid_reference_numbers, 
      #                                  @savings_account.id, 
      #                                  ["deposit", "withdraw", "wp"], 
      #                                  @closing_date.month
      #                                )
      j = []
      savings_account_transactions = SavingsAccountTransaction.approved.where(
                                        "savings_account_id = ? AND extract(month from transacted_at) = ? AND extract(year from transacted_at) = ?",
                                        @savings_account.id,
                                        @closing_date.month,
                                        @closing_date.year
                                      ).order("transacted_at ASC")
    
      


      last_working_day    = Utils::GetLastWorkingDay.new(some_date: (@closing_date - 1.month)).execute!
      latest_transaction  = SavingsAccountTransaction.approved_and_reversed.where(
                              "savings_account_id = ? AND extract(month from transacted_at) <= ? AND extract(year from transacted_at) = ?", 
                              @savings_account.id, 
                              last_working_day.month,
                              last_working_day.year
                            ).try(:last)

      # NOTE: If we don't have latest transaction for previous month, check if we have earliest transaction for this month
      if !latest_transaction
        latest_transaction  = SavingsAccountTransaction.approved_and_reversed.where(
                                "savings_account_id = ? AND transacted_at > ? AND transacted_at < ?", 
                                @savings_account.id, 
                                last_working_day, 
                                @closing_date
                              ).try(:first)
        if latest_transaction
          if latest_transaction.transaction_date
            last_working_day = latest_transaction.transaction_date.to_date
          else
            last_working_date = latest_transaction.transacted_at.to_date
          end
        end
      end

      ending_balance = latest_transaction.try(:ending_balance)

      num_days_before_next_transaction = (@closing_date - last_working_day).to_i
      if savings_account_transactions.count > 0
        num_days_before_next_transaction = (savings_account_transactions.order("transacted_at ASC").first.created_at.to_date - last_working_day).to_i
      end

      if ending_balance
        d = {}
        d[:created_at] = last_working_day
        d[:beginning_balance] = nil
        #d[:ending_balance] = savings_account_transactions.order("created_at ASC").first.beginning_balance
        d[:ending_balance] = ending_balance
        d[:deposits] = nil
        d[:withdrawals] = nil
        d[:num_days_before_next_transaction] = num_days_before_next_transaction
        d[:interest_per_month] = ((d[:ending_balance] * (@annual_interest_rate.to_f/100)) / 12).to_f.round(3)
        d[:interest_earned_on_deposits] = ((d[:interest_per_month] * d[:num_days_before_next_transaction]) / 30).to_f.round(3)
        d[:withholding_tax_on_deposits] = (d[:interest_earned_on_deposits] * (@tax_rate.to_f / 100)).to_f.round(3)
        data << d

        created_ats = savings_account_transactions.pluck(:created_at).uniq
        created_ats.each_with_index do |created_at, i|
          d = {}
          sats = savings_account_transactions.where(created_at: created_at).order("created_at ASC")
          d[:created_at] = created_at
          d[:beginning_balance] = sats[0].beginning_balance.to_f.round(3)
          d[:ending_balance] = sats[sats.count - 1].ending_balance.to_f.round(3)
          d[:deposits] = sats.where(transaction_type: ["deposit", "reverse_withdraw"]).sum(:amount).to_f
          d[:withdrawals] = sats.where(transaction_type: ["withdraw", "wp", "reverse_deposit"]).sum(:amount).to_f
          if i == created_ats.count - 1
            d[:num_days_before_next_transaction] = (@closing_date - created_at.to_date).to_i
          else
            d[:num_days_before_next_transaction] = (created_ats[i+1].to_date - created_at.to_date).to_i
          end
          d[:interest_per_month] = ((d[:ending_balance] * (@annual_interest_rate.to_f/100)) / 12).to_f.round(3)
          d[:interest_earned_on_deposits] = ((d[:interest_per_month] * d[:num_days_before_next_transaction]) / 30).to_f.round(3)
          d[:withholding_tax_on_deposits] = (d[:interest_earned_on_deposits] * (@tax_rate.to_f / 100)).to_f.round(3)
          data << d
        end

        total_interest = 0.00
        total_tax = 0.00
        data.each do |d|
          total_interest = total_interest + d[:interest_earned_on_deposits]
          total_tax = total_tax + d[:withholding_tax_on_deposits]
        end

        if @savings_account.balance == 0
          total_interest = 0.00
        end

        ret = { savings_account: @savings_account, total_interest: total_interest.to_f.round(2), total_tax: total_tax.to_f.round(2) }

        ret
      else
        nil
      end
		end
	end
end
