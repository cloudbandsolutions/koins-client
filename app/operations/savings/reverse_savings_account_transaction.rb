module Savings
	class ReverseSavingsAccountTransaction
		def initialize(savings_account_transaction:)
			@savings_account_transaction = savings_account_transaction
		end

	      def execute!
		    transaction_type = ["withdraw", "wp", "fund_transfer_withdraw"].include?(@savings_account_transaction.transaction_type) ? "reverse_withdraw" : "reverse_deposit"

                  beginning_balance = @savings_account_transaction.savings_account.balance
                  ending_balance = transaction_type == "reverse_withdraw" ? beginning_balance + @savings_account_transaction.amount : beginning_balance - @savings_account_transaction.amount

                  sat = SavingsAccountTransaction.new(
                                    amount: @savings_account_transaction.amount,
                                    transaction_type: transaction_type,
                                    voucher_reference_number: @savings_account_transaction.voucher_reference_number,
                                    particular: "REVERSED ENTRY",
                                    savings_account: @savings_account_transaction.savings_account,
                                    status: 'reversed',
                                    beginning_balance: beginning_balance,
                                    ending_balance: ending_balance
                                  )
                  sat.save!
                  sat.generate_updates!
		end
	end
end