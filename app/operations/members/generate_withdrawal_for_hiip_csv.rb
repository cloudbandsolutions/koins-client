module Members
	class GenerateWithdrawalForHiipCsv
		def initialize(data:)
			@data = data
		end

		def execute!
	       CSV.generate do |csv|
                csv << [ 
                    :payment_collection_uuid,
                    :payment_type,
                    :or_number,
                    :payment_collection_record_uuid,
                    :member,
                    :member_uuid,
                    :amount,
                    :accounting_entry_uuid,
                    :voucher_reference_number,
                    :particular,
                    :approved_by,
                    :prepared_by,
                    :book,
                    :date_approved,
                    :date_prepared,
                    :master_reference_number
                ]

                @data.each do |rec|
                    csv << [
                        rec[:payment_collection_uuid],
                        rec[:payment_type],
                        rec[:or_number],
                        rec[:payment_collection_record_uuid],
                        rec[:member],
                        rec[:member_uuid],
                        rec[:amount],
                        rec[:accounting_entry_uuid],
                        rec[:voucher_reference_number],
                        rec[:particular],
                        rec[:approved_by],
                        rec[:prepared_by],
                        rec[:book],
                        rec[:date_approved],
                        rec[:date_prepared],
                        rec[:master_reference_number]
                    ]
                end
            end
		end
	end
end