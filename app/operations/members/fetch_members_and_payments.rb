module Members
  class FetchMembersAndPayments
    def initialize
      @pure_active_members          = Member.pure_active
      @cooperative_membership_type  = Settings.cooperative_membership_type
      @membership_type              = MembershipType.where(name: @cooperative_membership_type).first
      @data                         = {}
      @data[:members]               = []
    end

    def execute!
      @pure_active_members.each do |member|
        membership_payments = MembershipPayment.paid.where(
                                member_id: member.id,
                                status: "paid",
                                membership_type_id: @membership_type.id
                              )
        data  = {
          id: member.id,
          identification_number: member.identification_number,
          membership_payments: membership_payments,
          membership_payments_size: membership_payments.size
        }
        @data[:members] << data
      end

      @data
    end
  end
end
