module Members
  class ValidateCancelMemberTransferRequest
    def initialize(member_id:)
      @member_id  = member_id
      @member     = Member.where(id: @member_id).first
      @errors     = []
    end

    def execute!
      if !@member
        @errors << "Member required"
      end

      if @member
        if !@member.for_transfer?
          @errors << "This member is not for transfer"
        end
      end

      @errors
    end
  end
end
