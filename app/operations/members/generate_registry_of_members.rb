module Members
  class GenerateRegistryOfMembers
    def initialize(center:)
      @center   = center
      #@members  = Member.active
      @members  = Member.where(status: "active")
      if @center.present?
        @members  = @members.where(center_id: @center.id)
      end

      @members  = @members.order("last_name asc")
      @data     = []
    end

    def execute!
      @members.each do |member|
        d = {}
        d[:membership_number]       = member.identification_number
        d[:name_of_member]          = member.full_name_titleize
        d[:center]                  = member.center.name
        d[:tin_number]              = member.tin_number
        d[:date_accepted]           = member.date_accepted_coop
        d[:bod_res_num_acc]         = member.bod_res_num_acc
        d[:membership_kind_regular] = member.member_type  == "Regular" ? "Yes" : "No" 
        d[:membership_kind_assoc]   = member.member_type  ==  "GK" ? "Yes" : "No"
        d[:number_of_shares_subs]   = Settings.default_num_shares_subscribed ||= 4
        d[:amount_subscribed]       = get_amount_subscribed
        d[:initial_payment]         = member.initial_payment_equity # TODO: Operation to extract initial payment
        #d[:address]                 = member.full_address
        d[:address_street]          = member.address_street
        d[:address_barangay]        = member.address_barangay
        d[:address_city]            = member.address_city
        d[:date_of_birth]           = member.date_of_birth.strftime("%B %m, %Y")
        d[:age]                     = member.age
        d[:gender]                  = member.gender
        d[:civil_status]            = member.civil_status
        d[:highest_education]       = member.highest_education
        d[:occupation]              = member.occupation # TODO: Extract operation form member
        d[:num_dependents]          = member.num_dependents
        d[:religion]                = member.try(:religion).try(:to_s)
        d[:annual_income]           = Members::GenerateAnnualIncome.new(member: member).execute!
        d[:date]                    = member.date_resigned
        d[:mobile_number]           = member.mobile_number
        d[:sato]                    = member.branch.name
        @data << d
      end

      @data
    end
    
    private

    def get_amount_subscribed
      max_amount                    = Settings.equity_maximum_amount ||= 100
      default_num_shares_subscribed = Settings.default_num_shares_subscribed ||= 4
      amount_subscribed             = max_amount * default_num_shares_subscribed

      amount_subscribed
    end
  end
end
