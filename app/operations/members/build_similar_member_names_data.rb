module Members
  class BuildSimilarMemberNamesData
    def initialize(members:)
      @members  = members
      @data     = {}
      @sql      = "SELECT last_name, first_name, count(*) FROM members GROUP BY last_name, first_name"
      @result   = ActiveRecord::Base.connection.execute(@sql).values

      @data     = []
    end

    def execute!
      multi_res = []
      @result.each do |r|
        if r[2].to_i > 1
          multi_res << r
        end
      end
      
      multi_res.each do |res|
        d = {}
        d[:last_name]   = res[0]
        d[:first_name]  = res[1]
        d[:count]       = res[2]

        d[:members] = @members.where(last_name: d[:last_name], first_name: d[:first_name])

        @data << d
      end

      @data
    end
  end
end
