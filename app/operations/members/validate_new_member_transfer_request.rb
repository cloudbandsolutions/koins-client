module Members
  class ValidateNewMemberTransferRequest
    def initialize(member_id:)
      @member_id  = member_id
      @member     = Member.where(id: @member_id).first
      @errors     = []
    end

    def execute!
      if !@member
        @errors << "Member required"
      end

      if @member.member_transfer_requests.size > 0
        @merrors << "Member already has a transfer record"
      end

      @errors
    end
  end
end
