module Members
	class NewMemberRecord
		def initialize(member_params:)
			@member_params = member_params
		end

		def execute!
			branch = Branch.where(code: @member_params[:branch_code]).first
      center = Center.where(code: @member_params[:center_code]).first

      @member_params[:branch] = branch
      @member_params[:center] = center

      @member_hash.except!(:branch_code, :center_code)
		end
	end
end