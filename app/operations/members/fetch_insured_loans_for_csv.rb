module Members
  class FetchInsuredLoansForCsv
    def initialize(start_date:, end_date:)
      @records  = []
      @start_date = start_date.try(:to_date)
      @end_date = end_date.try(:to_date)
      @loans = []

      if @end_date.present? && @start_date.present?
        Loan.where("updated_at >= ? AND updated_at <= ?", @start_date, @end_date).each do |loan|
          accounting_entry = loan.accounting_entry
          if !accounting_entry.nil?
            clip = accounting_entry.journal_entries.where(accounting_code_id: 99).first
            if !clip.nil?
              @loans << loan
            end
          end
        end

        @entry_level_loans  = @loans
      else
        Loan.all.each do |loan|
          accounting_entry = loan.accounting_entry
          if !accounting_entry.nil?
            clip = accounting_entry.journal_entries.where(accounting_code_id: 99).first
            if !clip.nil?
              @loans << loan
            end
          end
        end

        @entry_level_loans  = @loans
      end
    end

    def execute!
      @entry_level_loans.each do |loan|
        record = {}
        record[:loan]         = loan
        record[:uuid]         = loan.uuid
        record[:pn_number]    = loan.pn_number
        record[:member]       = loan.member.full_name_titleize
        record[:identification_number] = loan.member.identification_number
        record[:first_name]     = loan.member.first_name
        record[:middle_name]     = loan.member.middle_name
        record[:last_name]     = loan.member.last_name
        record[:loan_product]   = loan.loan_product.to_s
        record[:first_date_of_payment]  = loan.first_date_of_payment.strftime("%m-%d-%Y")
        record[:uuid]  = loan.uuid
        record[:status] = loan.status
        
        if loan.override_installment_interval == true
          record[:amount] = loan.original_loan_amount
          record[:num_installments] = loan.original_num_installments
          record[:maturity_date]  = loan.maturity_date.strftime("%m-%d-%Y")  
          record[:approximated_date_released] = (loan.maturity_date - (loan.original_num_installments).weeks).strftime("%B %d, %Y")
        else
          record[:amount] = loan.amount
          record[:num_installments] = loan.num_installments
          record[:maturity_date]  = loan.maturity_date.strftime("%m-%d-%Y")
          record[:approximated_date_released] = loan.date_approved.strftime("%m-%d-%Y")
        end

        record[:term] = loan.term


        lde = loan.loan_product.loan_deduction_entries.where(accounting_code_id: 99).first
        if lde.present?
          if lde.is_percent and !lde.use_term_map
            p = (lde.val / 100).to_f
            record[:insured_amount] = (p * record[:amount])
          elsif lde.is_percent and lde.use_term_map
            case record[:num_installments]
            when 15
              p = lde.t_val_15.to_f
            when 25
              p = lde.t_val_25.to_f
            when 35
              p = lde.t_val_35.to_f
            when 50
              p = lde.t_val_50.to_f
            else
              if record[:term] == "semi-monthly" || record[:term] == "monthly"
                #p = lde.t_val_15
                p = lde.t_val_50.to_f
              elsif 
                p = lde.t_val_25.to_f
              end
            end

            p = p / 100
            
            if record[:term] == "semi-monthly"
              mos = (record[:num_installments] / 2).to_f
              record[:insured_amount] = (p * record[:amount]) * (mos / 12).to_f
            else  
              record[:insured_amount] = (p * record[:amount])
            end
          elsif lde.is_percent == false and lde.use_term_map == false
            record[:insured_amount] = lde.val.to_f
          elsif lde.is_percent == false and lde.use_term_map == true
            case record[:num_installments]
            when 15
              p = lde.t_val_15.to_f
            when 16..25
              p = lde.t_val_25.to_f
            when 35
              p = lde.t_val_35.to_f
            when 50
              p = lde.t_val_50.to_f
            else
              #p = lde.t_val_25
              if record[:term] == "semi-monthly" || record[:term] == "monthly"
                p = lde.t_val_15
              elsif 
                p = lde.t_val_25.to_f
              end
            end

            record[:insured_amount] = p
          else
            raise "Invalid loan deduction entry"
          end
        end

        @records << record
      end

      @records
    end
  end
end