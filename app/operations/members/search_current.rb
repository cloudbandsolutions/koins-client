module Members
	class SearchCurrent
		def initialize(params:, branches:, i:)
			@params = params
			@branches = branches
			@i = i
		end

		def execute!
			members = Member.current.select("*")
      
      if Settings.is_cluster_office?
        members = members.where("branch_id IN (?)", @branches.pluck(:id))
      end 

      if @params[:status].present?
        status = @params[:status]
        members = members.where("status = ?", status)
      end

      if @params[:name].present?
        name = @params[:name]
        members = members.where("lower(first_name) LIKE :name OR lower(middle_name) LIKE :name OR lower(last_name) LIKE :name OR identification_number LIKE :name", name: "%#{name.downcase}%")
      end

      if @params[:branch_id].present?
        branch = Branch.find(@params[:branch_id])
        members = members.where(branch_id: branch.try(:id))
      end

      if @params[:center_id].present?
        center = Center.find(@params[:center_id])
        members = members.where(center_id: center.id)
      end

      if @params[:member_type].present?
        member_type = @params[:member_type]
        members = members.where(member_type: member_type)
      end

      members
		end
	end
end
