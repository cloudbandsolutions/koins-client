module Members
  class GenerateTransferRecords
    def initialize(center:, date_requested:)
      @center         = center
      @date_requested = date_requested
      @members        = Member.active.where(center_id: @center.id).order("last_name ASC")
    end

    def execute!
      @members.each do |member|
        ::Members::CreateNewMemberTransferRequest.new(
          member_id: member.id,
          date_requested: @date_requested
        ).execute!
      end

      true
    end
  end
end
