module Members
  class ValidateGenerateTransferRecords
    def initialize(center:)
      @center   = center
      @members  = Member.active.where(center_id: @center.id).order("last_name ASC")
      @errors   = []
    end

    def execute!
      if @members.size == 0
        @errors << "No active members for this center"
      else
        @members.each do |member|
          member_transfer_request_errors  = ::Members::ValidateNewMemberTransferRequest.new(
                                              member_id: member.id
                                            ).execute!

          if member_transfer_request_errors.size > 0
            member_transfer_request_errors.each do |e|
              @errors << e
            end
          end
        end
      end

      @errors
    end
  end
end
