module Members
  class GenerateAnnualIncome
    def initialize(member:)
      @num_weeks_in_year  = 48
      @member             = member
      @annual_income      = 0.00
      @loans              = Loan.active_and_paid.where(
                              member_id: member.id
                            )

      @member_weekly_incomes  = MemberWeeklyIncome.where(
                                  loan_id: @loans.pluck(:id)
                                )
    end

    def execute!
      @annual_income  = @member_weekly_incomes.sum(:amount) / @num_weeks_in_year

      @annual_income
    end
  end
end
