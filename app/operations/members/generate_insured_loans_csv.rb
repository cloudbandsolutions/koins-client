module Members
	class GenerateInsuredLoansCsv
		def initialize(data:)
			@data = data
		end

		def execute!
	       CSV.generate do |csv|
                        csv << [ 
                            :identification_number,
                            :date_released,
                            :maturity_date,
                            :loan_term,
                            :loan_amount,
                            :loan_insurance_amount,
                            :pn_number,
                            :status,
                            :loan_category
                        ]

                @data.each do |data|
                    csv << [
                    data[:identification_number],
                    data[:approximated_date_released],
                    data[:maturity_date],
                    data[:num_installments],
                    data[:amount],
                    data[:insured_amount],
                    data[:pn_number],
                    data[:status],
                    data[:loan_product]
                    ]
                end
            end
		end
	end
end