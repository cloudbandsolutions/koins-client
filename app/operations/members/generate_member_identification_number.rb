module Members
	class GenerateMemberIdentificationNumber
		def initialize(cluster_code:, branch_code:, branch:)
			@cluster_code = cluster_code
			@branch_code = branch_code
			@branch = branch
		end

		def execute!
			next_member_counter = @branch.member_counter + 1
      member_identification_number = @cluster_code + @branch_code + next_member_counter.to_s.rjust(5, "0")

      member_identification_number
		end
	end
end
