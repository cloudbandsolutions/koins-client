module Members
  class RestoreResignedMember
    def initialize(member:)
      @member         = member
      @old_previous_mii_member_since = @member.previous_mii_member_since
      @c_working_date = ApplicationHelper.current_working_date

      if !@member.resigned?
        raise "Member is not resigned"
      end
    end

    def execute!
      # Destroy all loan cycles
      MemberLoanCycle.where(member_id: @member.id).each do |mlc|
        mlc.destroy!
      end
      
      #pending insurance status
      Member.find(@member.id).update(insurance_status: "pending")

      # Void all memberhsip payments
      MembershipPayment.where(member_id: @member.id).each do |mp|
        mp.update!(status: "void")
      end

      @member.savings_accounts.each do |savings_account|
        savings_account.update!(status: "active")
      end

      @member.insurance_accounts.each do |insurance_account|
        insurance_account.update!(status: "active")
      end

      @member.equity_accounts.each do |equity_account|
        equity_account.update!(status: "active")
      end

      meta = @member.meta

      if meta
        if !meta['restore_records']
          meta['restore_records'] = []
        end
      else
        meta = {}
        meta['restore_records'] = []
      end

      meta['restore_records'] << {
        date_restord: @c_working_date  
      }

      @member.update!(status: "pending", previous_mfi_member_since: nil, previous_mii_member_since: nil, meta: meta, is_balik_kasapi: true, old_previous_mii_member_since: @old_previous_mii_member_since)

      @member
    end
  end
end
