module Members
  class GenerateBeneficiariesExcel
    def initialize(data:, center:)
      @data           = data
      @center         = center
      @p              = Axlsx::Package.new
      @header_labels  = [
        "ID Number",
        "Last Name",
        "First Name",
        "Middle Name",
        "Insurance Member Since",
        "Status",
        "Beneficiary Last Name",
        "Beneficiary First Name",
        "Beneficiary Middle Name",
        "Relationship",
        "Date of Birth",
        "Age",
        "Is Primary",
        "Reference Number",
      ]
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          if @center.present?
            sheet.add_row ["Beneficiaries list from #{@center.to_s}"]
          end

          # Headers
          sheet.add_row @header_labels

          @data.each do |member_data|
            member_row  = []

            member_row  <<  member_data[:identification_number]
            member_row  <<  member_data[:last_name]
            member_row  <<  member_data[:first_name]
            member_row  <<  member_data[:middle_name]
            member_row  <<  member_data[:mii_since]
            member_row  <<  member_data[:status]

            if member_data[:beneficiaries].first.present?
              b_data  = member_data[:beneficiaries].first
              member_row  <<  b_data[:last_name]
              member_row  <<  b_data[:first_name]
              member_row  <<  b_data[:middle_name]
              member_row  <<  b_data[:relationship]
              member_row  <<  b_data[:date_of_birth]
              member_row  <<  b_data[:age]
              member_row  <<  b_data[:primary]
              member_row  <<  b_data[:reference_number]
            end

            sheet.add_row member_row

            # Second beneficiary
            if member_data[:beneficiaries].size > 1
              secondary_row = []
              secondary_row <<  ""
              secondary_row <<  ""
              secondary_row <<  ""
              secondary_row <<  ""
              secondary_row <<  ""
              secondary_row <<  ""

              secondary_beneficiary = member_data[:beneficiaries].last
              secondary_row <<  secondary_beneficiary[:first_name]
              secondary_row <<  secondary_beneficiary[:last_name]
              secondary_row <<  secondary_beneficiary[:middle_name]
              secondary_row <<  secondary_beneficiary[:relationship]
              secondary_row <<  secondary_beneficiary[:date_of_birth]
              secondary_row <<  secondary_beneficiary[:age]
              secondary_row <<  secondary_beneficiary[:primary]
              secondary_row <<  secondary_beneficiary[:reference_number]

              sheet.add_row secondary_row
            end
          end
        end
      end

      @p
    end
  end
end
