module Members
	class GetAccounts
		def initialize(member:)
			@member = member
		end

		def execute!
			accounts = {}

      accounts['savings_accounts'] = @member.savings_accounts
      accounts['insurance_accounts'] = @member.insurance_accounts
      accounts['equity_accounts'] = @member.equity_accounts

      accounts
		end
	end
end