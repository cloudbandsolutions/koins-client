module Members
	class NewBeneficiaryRecord
		def initialize(beneficiary_params:)
			@beneficiary_params = beneficiary_params
		end

		def execute!
			member = Member.where(identification_number: @beneficiary_params[:member_identification_number]).first

      @beneficiary_params[:member] = member

      @beneficiary_params.except!(:member_identification_number)
		end
	end
end