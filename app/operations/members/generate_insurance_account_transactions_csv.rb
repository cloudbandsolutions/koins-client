module Members
	class GenerateInsuranceAccountTransactionsCsv
		def initialize(insurance_account_transactions:)
			@insurance_account_transactions = insurance_account_transactions
		end

		def execute!
	       CSV.generate do |csv|
                        csv << [ 
                            :insurance_account_uuid,
                            :amount,
                            :transaction_type,
                            :transacted_at,
                            :particular,
                            :status,
                            :transacted_by,
                            :approved_by,
                            :voucher_reference_number,
                            :transaction_number,
                            :bank_id,
                            :accounting_code_id,
                            :uuid,
                            :beginning_balance,
                            :ending_balance,
                            :transaction_date,
                            :is_adjustment,
                            :is_for_loan_payments,
                            :is_for_exit_age,
                            :is_withdraw_payment,
                            :is_fund_transfer,
                            :is_interest,
                            # for clip and hiip data
                            :id_data,
                            :principal_data,
                            :interest_data,
                            :first_date_of_payment_data,
                            :maturity_date_data,
                            :original_maturity_date_data,
                            :accounting_entry_id_data,
                            :journal_entry_id_data,
                            :amount_data,
                            :loan_product_id_data,
                            :loan_product_name_data,
                            :member_id_data,
                            :date_approved_data,
                            :date_released_data,
                            :reference_number_data,
                            :book_data,
                            :member_account_id_data,
                            :term_data,
                            :num_installments_data,
                            :account_transaction_id_data,
                            :status_data,
                            :equity_value
                        ]

                @insurance_account_transactions.each do |iat|
                    insurance_account = iat.insurance_account
                    if insurance_account
                        csv << [
                        iat.insurance_account.uuid,
                        iat.amount,
                        iat.transaction_type,
                        iat.transacted_at.strftime("%Y-%m-%d"),
                        iat.particular,
                        iat.status,
                        iat.transacted_by,
                        iat.approved_by,
                        iat.voucher_reference_number,
                        iat.transaction_number,
                        iat.bank_id,
                        iat.accounting_code_id,
                        iat.uuid,
                        iat.beginning_balance,
                        iat.ending_balance,
                        iat.transaction_date,
                        iat.is_adjustment,
                        iat.for_loan_payments,
                        iat.for_exit_age,
                        nil,
                        nil,
                        nil,
                        # for clip and hiip
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        nil,
                        iat.equity_value
                        ]
                    end
                end
            end
		end
	end
end