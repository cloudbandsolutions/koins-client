module Members
  class CancelPendingTransferRecords
    def initialize
      @members  = Member.for_transfer.order("last_name ASC")
    end

    def execute!
      @members.each do |member|
        ::Members::CancelMemberTransferRequest.new(
          member_id: member.id
        ).execute!
      end

      true
    end
  end
end
