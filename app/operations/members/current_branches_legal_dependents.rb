module Members
	class CurrentBranchesLegalDependents
		def initialize(branch_ids:)
			@branch_ids = branch_ids
		end

		def execute!
			legal_dependents = []
      Member.where("branch_id IN (?)", branch_ids).each do |member|
        member.legal_dependents.each do |b|
          legal_dependents << b.to_hash
        end
      end

      legal_dependents
		end
	end
end