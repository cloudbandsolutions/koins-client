module Members
	class CurrentBranchesMembers
		def initialize(branch_ids:)
			@branch_ids = branch_ids
		end

		def execute!
			members = []
      Member.where("branch_id IN (?)", @branch_ids).each do |member|
        m = member.to_hash
        members << m
      end

      members
		end
	end
end