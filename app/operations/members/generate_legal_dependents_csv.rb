module Members
	class GenerateLegalDependentsCsv
		def initialize(legal_dependents:)
			@legal_dependents = legal_dependents
		end

		def execute!
			CSV.generate do |csv|
                csv << [
                    :first_name, 
                    :middle_name, 
                    :last_name, 
                    :date_of_birth,
                    :educational_attainment,
                    :course,
                    :is_deceased,
                    :is_tpd,
                    :reference_number,
                    :relationship,
                    :member_identification_number,
                    :uuid,
                    :member_uuid
                        ]
                                
                @legal_dependents.each do |ld|
                  csv << [
                    ld.first_name,
                    ld.middle_name,
                    ld.last_name,
                    ld.date_of_birth,
                    ld.educational_attainment,
                    ld.course,
                    ld.is_deceased,
                    ld.is_tpd,
                    ld.reference_number,
                    ld.relationship,
                    ld.member.identification_number,
                    ld.uuid,
                    ld.member.uuid
                  ]
                end
            end
		end
	end
end