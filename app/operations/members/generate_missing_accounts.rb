module Members
	class GenerateMissingAccounts
		def initialize(member:)
			@member = member
		end

		def execute!
			# Generate missing savings accounts
      SavingsType.all.each do |savings_type|
        if SavingsAccount.where(savings_type_id: savings_type.id, member_id: @member.id).first.nil?
          SavingsAccount.create!(savings_type: savings_type, branch: @member.branch, center: @member.center, balance: 0.00, member_id: @member.id, account_number: SecureRandom.hex(4))
        end
      end

      # Generate missing insurance accounts
      InsuranceType.all.each do |insurance_type|
        if InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: @member.id).first.nil?
          InsuranceAccount.create!(insurance_type: insurance_type, branch: @member.branch, center: @member.center, balance: 0.00, member_id: @member.id, account_number: SecureRandom.hex(4))
        end
      end

      # Generate missing equity accounts
      EquityType.all.each do |equity_type|
        if EquityAccount.where(equity_type_id: equity_type.id, member_id: @member.id).first.nil?
          EquityAccount.create!(equity_type: equity_type, branch: @member.branch, center: @member.center, balance: 0.00, member_id: @member.id, account_number: SecureRandom.hex(4))
        end
      end
		end
	end
end