module Members
  class CreateNewMemberTransferRequest
    def initialize(member_id:, date_requested:)
      @member                   = Member.find(member_id)
      @date_requested           = date_requested
      @active_loans             = @member.loans.where(status: 'active')
      @member_transfer_request  = MemberTransferRequest.new(
                                    member: @member,
                                    date_of_request: @date_requested
                                  )
    end

    def execute!
      generate_loan_transfers!
      generate_savings_transfers!
      generate_equity_transfers!
      generate_insurance_transfers!

      @member_transfer_request.save!

      @member.update!(
        status: 'for-transfer'
      )

      @member_transfer_request
    end

    private

    def generate_insurance_transfers!
      @member.insurance_accounts.each do |insurance_account|
        amount  = insurance_account.balance
        member_insurance_transfer_request = MemberInsuranceTransferRequest.new(
                                              insurance_account: insurance_account,
                                              amount: amount
                                            )
        
        @member_transfer_request.member_insurance_transfer_requests << member_insurance_transfer_request

        insurance_account.deactivate!
      end
    end

    def generate_equity_transfers!
      @member.equity_accounts.each do |equity_account|
        amount  = equity_account.balance
        member_equity_transfer_request  = MemberEquityTransferRequest.new(
                                            equity_account: equity_account,
                                            amount: amount
                                          )

        @member_transfer_request.member_equity_transfer_requests << member_equity_transfer_request

        equity_account.deactivate!
      end
    end

    def generate_savings_transfers!
      @member.savings_accounts.each do |savings_account|
        amount  = savings_account.balance
        member_savings_transfer_request = MemberSavingsTransferRequest.new(
                                            savings_account: savings_account,
                                            amount: amount
                                          )

        @member_transfer_request.member_savings_transfer_requests << member_savings_transfer_request

        savings_account.deactivate!
      end
    end

    def generate_loan_transfers!
      @active_loans.each do |active_loan|
        amount            = active_loan.principal_balance
        loan              = active_loan
        interest_balance  = active_loan.interest_balance

        member_loan_transfer_request  = MemberLoanTransferRequest.new(
                                          loan: loan,
                                          amount: amount,
                                          interest_balance: interest_balance
                                        )

        @member_transfer_request.member_loan_transfer_requests << member_loan_transfer_request

        active_loan.update!(status: 'for-transfer')
      end
    end
  end
end
