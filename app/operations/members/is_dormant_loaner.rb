module Members
  class IsDormantLoaner
    def initialize(member:, threshold:, current_date:, savings_type:)
      @savings_type = savings_type
      @member       = member
      
      @threshold    = threshold
      @current_date = current_date

      @member_loans   = Loan.where(member_id: @member.id)
      @member_active_loans   = Loan.where(member_id: @member.id, status: "active").last
      

      if @member_loans.size > 0
        @loan_date_released = @member_loans.ids
        
        @loan_payments  = LoanPayment.approved.where(
                            loan_id: @member_loans.pluck(:id)
                          ).order("paid_at ASC")
        

        if @loan_payments.size > 0
                     
          @latest_payment = @loan_payments.last
          
          @final_date     = @current_date.to_date - @threshold.months
        end
       
      end
    end

    def execute!
      if @savings_type.id == 2
        
        false
      else
      if  @member_active_loans
        false
        
      else
        if @latest_payment
      
          if Loan.find(@latest_payment.loan_id).status  == "paid"
            
            if @latest_payment.paid_at < @final_date
              true
            else
              false
            end
          else
            false
          end
        else
           mPayment = MembershipPayment.where(member_id: @member.id, membership_type_id: 1, status: "paid").last.paid_at
           @final_date     = @current_date.to_date - @threshold.months
           if mPayment < @final_date
             true
           else
              false
           end
        
          
        end
      
      end
      end
    
    
    end
  end
end
