module Members
  class ApproveMemberTransferRequest
    def initialize(member_transfer_request:)
      @member_transfer_request    = member_transfer_request
      @voucher                    = ::Transfer::ProduceAccountingEntry.new(
                                      member_transfer_request: @member_transfer_request
                                    ).execute!
    end

    def execute!
      @member_transfer_request.member_savings_transfer_requests.each do |member_savings_transfer_request|
      end

      @member_transfer_request.member_equity_transfer_requests.each do |member_equity_transfer_request|
      end

      @member_transfer_request.member_insurance_transfer_requests.each do |member_insurance_transfer_request|
      end

      @member_transfer_request.member_loan_transfer_requests.each do |member_loan_transfer_request|
      end
    end
  end
end
