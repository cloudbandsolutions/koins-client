module Members
	class NewLegalDependentRecord
		def initialize(legal_dependent_params:)
			@legal_dependent_params = legal_dependent_params
		end

		def execute!
			member = Member.where(identification_number: @legal_dependent_params[:member_identification_number]).first

      @legal_dependent_params[:member] = member

      @legal_dependent_params.except!(:member_identification_number)
		end
	end
end