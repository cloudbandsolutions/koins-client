module Members
	class GenerateInsuranceAccountsCsv
		def initialize(insurance_accounts:)
			@insurance_accounts = insurance_accounts
		end

		def execute!
	       CSV.generate do |csv|
                        csv << [ 
                            :insurance_type,
                            :balance,
                            :member_id,
                            :account_number,
                            :status,
                            :branch,   
                            :center,
                            :uuid,
                            :member_uuid,
                            :equity_value
                        ]

                @insurance_accounts.each do |ia|
                    csv << [
                    ia.insurance_type.code,
                    ia.balance,
                    ia.member.identification_number,
                    ia.account_number,
                    ia.status,
                    ia.branch,
                    ia.center,
                    ia.uuid,
                    ia.member.uuid,
                    ia.equity_value,
                    ]
                end
            end
		end
	end
end