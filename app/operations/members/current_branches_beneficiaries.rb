module Members
	class CurrentBranchesBeneficiaries
		def initialize(branch_ids:)
			@branch_ids = branch_ids
		end

		def execute!
			beneficiaries = []
      Member.where("branch_id IN (?)", @branch_ids).each do |member|
        member.beneficiaries.each do |b|
          beneficiaries << b.to_hash
        end
      end

      beneficiaries
		end
	end
end