module Members
  class GenerateBeneficiaries
    def initialize(center:)
      @center   = center
      @members  = Member.active
      if @center.present?
        @members  = @members.where(center_id: @center.id)
      end

      @members  = @members.order("last_name asc")
      @data     = []
    end

    def execute!
      @members.each do |member|
        d = {}
        d[:identification_number] = member.identification_number
        d[:first_name]            = member.first_name
        d[:middle_name]           = member.middle_name
        d[:last_name]             = member.last_name
        d[:mii_since]             = member.mii_since
        d[:status]                = member.status
        d[:beneficiaries]         = []
        member.beneficiaries.each do |beneficiary|
          b = {}
          b[:first_name]    = beneficiary.first_name
          b[:middle_name]   = beneficiary.middle_name
          b[:last_name]     = beneficiary.last_name
          b[:relationship]  = beneficiary.relationship
          b[:date_of_birth] = beneficiary.date_of_birth
          b[:age]           = beneficiary.age
          b[:primary]       = beneficiary.is_primary == true ? "Yes" : "No"
          b[:reference_number] = beneficiary.reference_number

          d[:beneficiaries] <<  b
        end

        @data << d
      end

      @data
    end
  end
end
