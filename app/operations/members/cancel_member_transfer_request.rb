module Members
  class CancelMemberTransferRequest
    def initialize(member_id:)
      @member         = Member.find(member_id)
    end

    def execute!
      member_transfer_request = MemberTransferRequest.where(
                                  member_id: @member.id,
                                  status: "pending"
                                ).first

      if member_transfer_request
        # activate savings accounts
        member_transfer_request.member_savings_transfer_requests.each do |member_savings_transfer_request|
          member_savings_transfer_request.savings_account.activate!
        end

        # activate equity accounts
        member_transfer_request.member_equity_transfer_requests.each do |member_equity_transfer_request|
          member_equity_transfer_request.equity_account.activate!
        end

        # activate insurance accounts
        member_transfer_request.member_insurance_transfer_requests.each do |member_insurance_transfer_request|
          member_insurance_transfer_request.insurance_account.activate!
        end

        # activate loans
        member_transfer_request.member_loan_transfer_requests.each do |member_loan_transfer_request|
          member_loan_transfer_request.loan.update!(status: 'active')
        end

        member_transfer_request.destroy!

        @member.update!(
          status: 'active'
        )

        member_transfer_request.destroy!
      end

      @member
    end
  end
end
