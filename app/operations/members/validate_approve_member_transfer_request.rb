module Members
  class ValidateApproveMemberTransferRequest
    def initialize(member_transfer_request:)
      @member_transfer_request    = member_transfer_request
      @errors                     = []
    end

    def execute!
      if !@member_transfer_request
        @errors << "No transfer request found"
      else
        if !@member_transfer_request.particular.present?
          @errors << "Particular required"
        end

        if !@member_transfer_request.pending?
          @errors << "This record is not pending"
        end

        @member_transfer_request.member_savings_transfer_requests.each do |member_savings_transfer_request|
          if !member_savings_transfer_request.complete? and member_savings_transfer_request.amount > 0
            @errors << "No accounting entries for #{member_savings_transfer_request.savings_account.savings_type}"
          end
        end

        @member_transfer_request.member_equity_transfer_requests.each do |member_equity_transfer_request|
          if !member_equity_transfer_request.complete? and member_equity_transfer_request.amount > 0
            @errors << "No accounting entries for #{member_equity_transfer_request.equity_account.equity_type}"
          end
        end

        @member_transfer_request.member_insurance_transfer_requests.each do |member_insurance_transfer_request|
          if !member_insurance_transfer_request.complete? and member_insurance_transfer_request.amount > 0
            @errors << "No accounting entries for #{member_insurance_transfer_request.insurance_account.insurance_type}"
          end
        end

        @member_transfer_request.member_loan_transfer_requests.each do |member_loan_transfer_request|
          if !member_loan_transfer_request.complete? and member_loan_transfer_request.amount > 0
            @errors << "No accounting entries for #{member_loan_transfer_request.loan.loan_product}"
          end
        end
      end

      @errors
    end
  end
end
