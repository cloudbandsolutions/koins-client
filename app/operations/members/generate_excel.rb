module Members
	class GenerateExcel
		def initialize(members:)
			@members = members
		end

		def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          @members.each do |m|
            sheet.add_row([
              m.uuid,
              m.first_name,
              m.middle_name,
              m.last_name
            ])
          end
        end
      end

      p
		end
	end
end