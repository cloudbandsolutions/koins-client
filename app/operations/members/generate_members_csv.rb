module Members
	class GenerateMembersCsv
		def initialize(members:)
			@members = members
		end

		def execute!
	       CSV.generate do |csv|
                        csv << [ 
                            :identification_number,
                            :member_type,
                            :status,
                            :insurance_status,
                            :first_name, 
                            :middle_name, 
                            :last_name,
                            :recognition_date,
                            :center, 
                            :branch, 
                            :gender,
                            :date_of_birth,
                            :place_of_birth,
                            :civil_status,
                            :number_of_children,
                            :spouse_first_name,
                            :spouse_last_name,
                            :spouse_middle_name,
                            :spouse_date_of_birth,
                            :address_street,
                            :address_barangay,
                            :address_city,
                            :sss_number,
                            :tin_number,
                            :pag_ibig_number,
                            :phil_health_number,
                            :cellphone_number,
                            :uuid,
                            :meta_id,
                            :date_resigned,
                            :resignation_type,
                            :resignation_code,
                            :resignation_reason,
                            :insurance_date_resigned,
                            :is_reinstate,
                            :old_previous_mii_member_since,
                            :is_balik_kasapi,
                            :center_id,
                            :branch_id
                            
                        ]
                @members.each do |m|
                    if m.tin_number.present?
                        tin = m.tin_number.split("-").join("")
                    elsif m.sss_number.present?
                        sss = m.sss_number.split("-").join("")
                    elsif m.pag_ibig_number.present?
                        pag_ibig = m.pag_ibig_number.split("-").join("")
                    elsif m.phil_health_number.present?
                        phil_health = m.phil_health_number.split("-").join("")
                    end

                    if m.meta
                        meta_id = m.meta.to_json
                    else
                        meta_id = ""
                    end

                    csv << [
                    m.identification_number,
                    m.member_type,
                    m.status,
                    m.insurance_status,    
                    m.first_name,
                    m.middle_name,
                    m.last_name,
                    m.previous_mii_member_since,
                    m.center,
                    m.branch,
                    m.gender,
                    m.date_of_birth,
                    m.place_of_birth,
                    m.civil_status,
                    m.num_children,
                    m.spouse_first_name,
                    m.spouse_last_name,
                    m.spouse_middle_name,
                    m.spouse_date_of_birth,
                    m.address_street,
                    m.address_barangay,
                    m.address_city,
                    sss,
                    tin,
                    pag_ibig,
                    phil_health,
                    m.mobile_number,
                    m.uuid,
                    meta_id,
                    m.date_resigned,
                    m.resignation_type,
                    m.resignation_code,
                    m.resignation_reason,
                    m.insurance_date_resigned,
                    m.is_reinstate,
                    m.old_previous_mii_member_since,
                    m.is_balik_kasapi,
                    m.center.uuid,
                    m.branch.uuid
                    ]
                end
            end
		end
	end
end