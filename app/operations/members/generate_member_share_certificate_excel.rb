module Members
	class GenerateMemberShareCertificateExcel
		def initialize(member:, member_share:)
			@member       = member
      @member_share = member_share
      @config       = {}
		end

		def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          generate_config!(wb)

          sheet.add_row ["", @member_share.certificate_number.to_s], style: @config[:normal]
          sheet.add_row []
          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["", @member_share.date_of_issue.day.ordinalize, "", month_to_filipino(@member_share.date_of_issue.month).capitalize, "", @member_share.date_of_issue.year]
        end
      end

      p
		end

    private

    def month_to_filipino(m)
      if m == 1
        return "enero"
      elsif m == 2
        return "pebrero"
      elsif m == 3
        return "marso"
      elsif m == 4
        return "abril"
      elsif m == 5
        return "mayo"
      elsif m == 6
        return "hunyo"
      elsif m == 7
        return "hulyo"
      elsif m == 8
        return "agosto"
      elsif m == 9
        return "setyembre"
      elsif m == 10
        return "oktubre"
      elsif m == 11
        return "nobyembre"
      elsif m == 12
        return "disyembre"
      else
        return "invalid"
      end
    end

    def generate_config!(wb)
      @config[:normal]              = wb.styles.add_style font_name: "Calibri", sz: 11, alignment: { horizontal: :left }
      @config[:title_cell]          = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", sz: 11
      @config[:label_cell]          = wb.styles.add_style b: true, font_name: "Calibri", sz: 11
      @config[:currency_cell]       = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri", sz: 11
      @config[:currency_cell_right] = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", sz: 11
      @config[:percent_cell]        = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri", sz: 11
      @config[:left_aligned_cell]   = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", sz: 11
      @config[:underline_cell]      = wb.styles.add_style u: true, font_name: "Calibri", sz: 11
      @config[:header_cells]        = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri", sz: 11
      @config[:default_cell]        = wb.styles.add_style font_name: "Calibri", sz: 11
    end
	end
end
