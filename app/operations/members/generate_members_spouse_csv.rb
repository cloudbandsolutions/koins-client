module Members
    class GenerateMembersSpouseCsv
        def initialize(members:)
            @members = members
            @mem = []
            @members.each do |m|
                if m.spouse_first_name.present?
                    @mem << m
                end
            end
        end

        def execute!
            CSV.generate do |csv|
                csv << [
                    :first_name, 
                    :middle_name, 
                    :last_name, 
                    :date_of_birth,
                    :educational_attainment,
                    :course,
                    :is_deceased,
                    :is_tpd,
                    :reference_number,
                    :relationship,
                    :member_identification_number
                        ]
                                
                @mem.each do |m|
                    if m.spouse_date_of_birth.nil?
                        spouse_date_of_birth = Time.now.strftime("%Y-%m-%d")
                    else
                        spouse_date_of_birth = m.spouse_date_of_birth
                    end

                  csv << [
                    m.spouse_first_name,
                    m.spouse_middle_name,
                    m.spouse_last_name,
                    spouse_date_of_birth,
                    nil,
                    nil,
                    nil,
                    nil,
                    nil,
                    "Spouse",
                    m.identification_number
                  ]
                end
            end
        end
    end
end