module Members
	class UpdateMemberInsuranceStatus
		def initialize(member:)
			@member = member
		end

		def execute!
			current_date = ApplicationHelper.current_working_date.to_date
      insurance_account_code = Settings.default_insurance_account_code ||= "LIF"
      start_date = @member.previous_mii_member_since.try(:to_date)

      insurance_account = @member.insurance_accounts.joins(:insurance_type).where("insurance_types.code = ?", insurance_account_code).first
      insurance_account_transactions  = insurance_account.insurance_account_transactions.where("amount > 0 AND insurance_account_id = ?", insurance_account.id).order("transacted_at ASC")

      default_periodic_payment = InsuranceType.where(code: insurance_account_code).first.default_periodic_payment


      if start_date.blank?
        insurance_membership_type_name = Settings.insurance_membership_type_name
        @member.memberships.each do |membership|
          if membership[:membership_type][:name] == insurance_membership_type_name
            if !membership[:membership_payment][:paid_at].nil?
              start_date = membership[:membership_payment][:paid_at].try(:to_date)
            end
          end
        end
      end

      if start_date.present?
                
        if insurance_account_transactions.size > 0
          latest_payment           = insurance_account_transactions.last
          last_payment_date        = insurance_account_transactions.last[:transacted_at].to_date
          current_balance          = latest_payment ? latest_payment.ending_balance : 0.00
       
          # Code
          num_days                 = (current_date - start_date).to_i
          num_weeks                = (num_days / 7).to_i
          insured_amount           = num_weeks * default_periodic_payment
          latest_transaction_date  = latest_payment ? latest_payment.transacted_at.to_date : start_date

          num_days_insured         = (latest_transaction_date.to_date  - start_date).to_i
          num_weeks_insured        = (num_days_insured / 7).to_i

          insured_amount           = num_weeks  * default_periodic_payment
          coverage_date            = (start_date + ((current_balance / default_periodic_payment).to_i).weeks).strftime("%B %d, %Y")
          amt_past_due             = (current_balance - insured_amount) * -1
          num_weeks_past_due       = (amt_past_due / default_periodic_payment).to_i

          days_lapsed = (current_date - last_payment_date).to_i

          if current_balance == 0.00 && latest_payment.for_resignation == true
            @member.update(insurance_status: "resigned")
          elsif current_balance == 0.00
            @member.update(insurance_status: "dormant")
          elsif days_lapsed <= 45 && current_balance >= insured_amount
            @member.update(insurance_status: "inforce")
          elsif days_lapsed > 45 && current_balance >= insured_amount
            @member.update(insurance_status: "inforce")
          elsif days_lapsed <= 45 && current_balance < insured_amount && amt_past_due < 97
            @member.update(insurance_status: "inforce")
          elsif days_lapsed <= 45 && current_balance < insured_amount && amt_past_due >= 97
            @member.update(insurance_status: "lapsed")  
          elsif days_lapsed > 45 && current_balance < insured_amount
            @member.update(insurance_status: "lapsed")
          end
        elsif insurance_account.insurance_account_transactions.count == 0
          @member.update(insurance_status: "dormant")
        end
      else
        @member.update(insurance_status: "pending") 
      end

      if @member.member_type == "GK"
        @member.update(insurance_status: "resigned")
      elsif @member.status == "resigned"
        if @member.previous_mii_member_since.nil?
          @member.update(insurance_status: "pending")
        else
          @member.update(insurance_status: "resigned")
        end
      elsif @member.status == "pending"
        @member.update(insurance_status: "pending")
      elsif @member.status == "archived"
        @member.update(insurance_status: "dormant")
      elsif @member.status == "cleared"
        @member.update(insurance_status: "cleared")
      end
		end
	end
end