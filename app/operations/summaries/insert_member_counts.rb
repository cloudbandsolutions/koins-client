module Summaries
  class InsertMemberCounts
    def initialize(as_of:, data:)
      @as_of        = as_of
      @data         = data
      @summary_type = "MEMBER_COUNTS"

      @summary      = Summary.new(
                        summary_type: @summary_type,
                        meta: @data,
                        as_of: @as_of
                      )
    end

    def execute!
      @summary.save!
    end
  end
end
