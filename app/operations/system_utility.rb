class SystemUtility

  # TODO: Actual algo to generate reference numbers
  def self.generate_reference_number
    "#{SecureRandom.hex(4)}"
  end

  def self.print_cluster_office_status
    if Settings.is_cluster_office
      puts "CLUSTER CONFIGURATION"
      Cluster.all.each do |cluster|
        puts "Cluster: #{cluster.name} (#{cluster.code})"
        puts "Branches:"
        Branch.where(cluster_id: cluster.id).each do |branch|
          puts "  Branch: #{branch.name}"
          puts "  Centers for branch #{branch.name}:"
          Center.where(branch_id: branch.id).each do |center|
            puts "    - #{center.name}"
          end
        end
      end
    else
      puts "Not a cluster office"
    end
  end
end
