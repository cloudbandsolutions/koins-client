module Surveys
  class GenerateNewSurveyRespondent
    def initialize(survey:)
      @survey = survey
      @survey_respondent = SurveyRespondent.new(survey: survey)
    end

    def execute!
      build_questions!
      @survey_respondent
    end

    private

    def build_questions!
      @survey.survey_questions.each do |question|
        @survey_respondent.survey_respondent_answers.build({ survey_question: question })
      end
    end
  end
end
