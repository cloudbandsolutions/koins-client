module Transfer
  class ApproveCenterTransferRecord
    require 'application_helper'

    def initialize(center_transfer_record:, user:)
      @center_transfer_record = center_transfer_record
      @member_records         = @center_transfer_record.data['members']
      @center                 = @center_transfer_record.center
      @user                   = user
      @c_working_date         = ApplicationHelper.current_working_date
      @voucher                = ::Transfer::ProduceCenterTransferRecordAccountingEntry.new(
                                  center_transfer_record: @center_transfer_record,
                                  user: @user
                                ).execute!
    end

    def execute!
      approve_entry!
      generate_records!
      update_status!
    end

    def approve_entry!
      @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
    end
      
    def generate_records!
      @member_records.each do |member_record|
        member  = ::Transfer::GenerateMemberRecord.new(
                    member_record: member_record,
                    center: @center
                  ).execute!

        ::Transfer::GenerateMemberSavings.new(
          member: member, 
          member_record: member_record, 
          user: @user,
          voucher: @voucher
        ).execute!

        ::Transfer::GenerateMemberInsurance.new(
          member: member, 
          member_record: member_record, 
          user: @user,
          voucher: @voucher
        ).execute!

        ::Transfer::GenerateMemberEquity.new(
          member: member, 
          member_record: member_record, 
          user: @user,
          voucher: @voucher
        ).execute!

        ::Transfer::GenerateMemberLoans.new(
          member: member,
          member_record: member_record,
          user: @user,
          voucher: @voucher
        ).execute!
      end
    end

    def update_status!
      @center_transfer_record.update!(
        status: 'approved',
        approved_by: @user.full_name,
        reference_number: @voucher.reference_number
      )
    end
  end
end
