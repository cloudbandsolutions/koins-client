module Transfer
  class ApprovePendingRecords
    def initialize
      @pending_records  = MemberTransferRequest.pending
    end

    def execute!
      @pending_records.each do |pending_record|
        ::Transfer::ApproveMemberTransferRequest.new(member_transfer_request: pending_record).execute!
      end
    end
  end
end
