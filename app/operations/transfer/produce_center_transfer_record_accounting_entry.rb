module Transfer
  class ProduceCenterTransferRecordAccountingEntry
    require 'application_helper'

    def initialize(center_transfer_record:, user:)
      @center_transfer_record = center_transfer_record
      @reference_number       = @center_transfer_record.reference_number
      @book                   = 'JVB'
      @voucher                = nil
      @user                   = user
      @c_working_date         = ApplicationHelper.current_working_date
    end

    def execute!
      if @center_transfer_record.approved?
        @voucher  = Voucher.where(book: @book, reference_number: @reference_number).first
      else
        @voucher  = Voucher.new(
                      book: @book,
                      date_prepared: @c_working_date,
                      particular: @center_transfer_record.particular,
                      branch: @center_transfer_record.branch,
                      status: 'pending'
                    )

        @center_transfer_record.center_transfer_record_journal_entries.each do |je|
          @voucher.journal_entries  <<  JournalEntry.new(
                                          amount: je.amount,
                                          post_type: je.post_type,
                                          accounting_code: je.accounting_code
                                        )
        end
      end

      @voucher
    end
  end
end
