module Transfer
  class GenerateMemberLoans
    require 'application_helper'

    def initialize(member:, member_record:, user:, voucher:)
      @member         = member
      @member_record  = member_record
      @user           = user
      @voucher        = voucher
      @c_working_date = ApplicationHelper.current_working_date
    end

    def execute!
      @member_record['loans'].each do |loan_record|
        loan_product          = LoanProduct.find(loan_record['loan_product_id'])
        loan_product_type     = LoanProductType.find(loan_record['loan_product_type_id'])

        if loan_record['project_type_id'].present?
          project_type          = ProjectType.find(loan_record['project_type_id'])
        end

        if loan_record['project_type_category_id'].present?
          project_type_category = ProjectTypeCategory.find(loan_record['project_type_category_id'])
        end

        bank                  = @member.branch.bank

        loan  = Loan.new(
                  pn_number:                loan_record['pn_number'],
                  bank:                     bank,
                  member:                   @member,
                  voucher_date_requested:   @c_working_date,
                  num_installments:         loan_record['remaining_num_installments'].to_i,
                  bank_check_number:        loan_record['bank_check_number'],
                  beneficiary_first_name:   loan_record['beneficiary_first_name'],
                  beneficiary_middle_name:  loan_record['beneficiary_middle_name'],
                  beneficiary_last_name:    loan_record['beneficiary_last_name'],
                  beneficiary_relationship: loan_record['beneficiary_relationship'],
                  clip_number:              loan_record['clip_number'],
                  date_approved:            loan_record['date_approved'],
                  date_of_release:          loan_record['date_of_release'],
                  date_prepared:            loan_record['date_prepared'],
                  insured_amount:           loan_record['insured_amount'],
                  interest_rate:            loan_record['interest_rate'].to_f,
                  voucher_check_number:     loan_record['voucher_check_number'],
                  book:                     @voucher.book,
                  voucher_reference_number: @voucher.reference_number,
                  voucher_particular:       @voucher.particular,
                  status:                   'active',
                  loan_product:             loan_product,
                  loan_product_type:        loan_product_type,
                  project_type:             project_type,
                  project_type_category:    project_type_category,
                  first_date_of_payment:    loan_record['first_date_of_payment'],
                  payment_type:             loan_record['payment_type'],
                  original_loan_amount:     loan_record['original_amount'],
                  amount:                   loan_record['principal_balance'].to_f, 
                  original_num_installments:  loan_record['num_installments'],
                  override_installment_interval:  loan_record['remaining_num_installments'].to_i,
                  installment_override:  loan_record['remaining_num_installments'].to_i,
                  #override_maintaining_balance: true,
                  old_principal_balance:    loan_record['principal_balance'].to_f,
                  old_interest_balance:     loan_record['interest_balance'].to_f,
                  term:                     loan_record['term'],
                  voucher_payee:            @member.full_name,
                  processing_fee:           0.00,
                  loan_cycle_count:         loan_record['loan_cycle_count'],
                )

        #loan.remaining_balance  = loan.old_principal_balance + loan.old_interest_balance

        if !loan.save
          ap loan
          ap loan_record
          raise loan.errors.messages.inspect
        end

        loan.update_remaining_balance!

        loan
      end
    end
  end
end
