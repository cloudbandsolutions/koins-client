module Transfer
  class BuildPendingTransferDetails
    def initialize
      @pending_requests = MemberTransferRequest.pending
      @data             = {}
      @loan_products    = LoanProduct.where(id: @pending_requests.joins(member_loan_transfer_requests: [loan: :loan_product]).pluck("loan_products.id").uniq)
      @savings_types    = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types     = EquityType.all
    end

    def execute!
      @data[:loans]     = []
      @data[:savings]   = []
      @data[:insurance] = []
      @data[:equity]    = []

      @loan_products.each do |loan_product|
        loan_product_data                         = {}
        loan_product_data[:loan_product_id]       = loan_product.id
        loan_product_data[:loan_product]          = loan_product.to_s
        loan_product_data[:principal_balance]     = @pending_requests.joins(member_loan_transfer_requests: [loan: :loan_product]).where("loan_products.id = ?", loan_product.id).sum("member_loan_transfer_requests.amount")
        loan_product_data[:interest_balance]      = @pending_requests.joins(member_loan_transfer_requests: [loan: :loan_product]).where("loan_products.id = ?", loan_product.id).sum("member_loan_transfer_requests.interest_balance")

        if loan_product_data[:principal_balance] > 0
          @data[:loans] << loan_product_data
        end
      end

      @savings_types.each do |savings_type|
        savings_data                    = {}
        savings_data[:savings_type_id]  = savings_type.id
        savings_data[:savings_type]     = savings_type.to_s
        savings_data[:balance]          = @pending_requests.joins(member_savings_transfer_requests: [:savings_account]).where("savings_accounts.savings_type_id = ?", savings_type.id).sum("member_savings_transfer_requests.amount");

        if savings_data[:balance] > 0
          @data[:savings] << savings_data
        end
      end

      @insurance_types.each do |insurance_type|
        insurance_data                      = {}
        insurance_data[:insurance_type_id]  = insurance_type.id
        insurance_data[:insurance_type]     = insurance_type.to_s
        insurance_data[:balance]            = @pending_requests.joins(member_insurance_transfer_requests: [:insurance_account]).where("insurance_accounts.insurance_type_id = ?", insurance_type.id).sum("member_insurance_transfer_requests.amount");

        if insurance_data[:balance] > 0
          @data[:insurance] << insurance_data
        end
      end

      @equity_types.each do |equity_type|
        equity_data                   = {}
        equity_data[:equity_type_id]  = equity_type.id
        equity_data[:equity_type]     = equity_type.to_s
        equity_data[:balance]         = @pending_requests.joins(member_equity_transfer_requests: [:equity_account]).where("equity_accounts.equity_type_id = ?", equity_type.id).sum("member_equity_transfer_requests.amount");

        if equity_data[:balance] > 0
          @data[:equity] << equity_data
        end
      end

      @data
    end
  end
end
