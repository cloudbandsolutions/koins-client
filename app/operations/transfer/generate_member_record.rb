module Transfer
  class GenerateMemberRecord
    def initialize(member_record:, center:)
      @member_record  = member_record['member']
      @religion       = Religion.where(id: member_record['member']['religion_id']).first
      @center         = center
      @branch         = @center.branch
    end

    def execute!
      build_member!
    end

    private

    def build_member!
      member  = Member.new(
                  first_name:                       @member_record['first_name'],
                  middle_name:                      @member_record['middle_name'],
                  last_name:                        @member_record['last_name'],
                  gender:                           @member_record['gender'],
                  date_of_birth:                    @member_record['date_of_birth'],
                  civil_status:                     @member_record['civil_status'],
                  spouse_date_of_birth:             @member_record['spouse_date_of_birth'],
                  home_number:                      @member_record['home_number'],
                  processed_by:                     @member_record['processed_by'],
                  approved_by:                      @member_record['approved_by'],
                  mobile_number:                    @member_record['mobile_number'],
                  place_of_birth:                   @member_record['place_of_birth'],
                  number_of_children:               @member_record['number_of_children'],
                  sss_number:                       @member_record['sss_number'],
                  spouse_first_name:                @member_record['spouse_first_name'],
                  spouse_middle_name:               @member_record['spouse_middle_name'],
                  spouse_last_name:                 @member_record['spouse_last_name'],
                  auto_active:                      true,
                  member_type:                      @member_record['member_type'],
                  religion:                         @religion,
                  spouse_occupation:                @member_record['spouse_occupation'],
                  center:                           @center,
                  branch:                           @branch,
                  uuid:                             @member_record['uuid'],
                  insurance_status:                 @member_record['insurance_status'],
                  previous_mfi_member_since:        @member_record['previous_mfi_member_since'],
                  previous_mii_member_since:        @member_record['previous_mii_member_since'],
                  phil_health_number:               @member_record['phil_health_number'],
                  type_of_housing:                  @member_record['type_of_housing'],
                  num_children:                     @member_record['num_children'],
                  num_children_high_school:         @member_record['num_children_high_school'],
                  num_children_college:             @member_record['num_children_college'],
                  bank:                             @member_record['bank'],
                  tin_number:                       @member_record['tin_number'],
                  proof_of_housing:                 @member_record['proof_of_housing'],
                  num_workers_business:             @member_record['num_workers_business'],
                  reason_for_joining:               @member_record['num_workers_business'],
                  is_experienced_with_microfinance: @member_record['is_experienced_with_microfinance'],
                  experience_with_microfinance:     @member_record['experience_with_microfinance'],
                  num_years_housing:                @member_record['num_years_housing'],
                  is_editable:                      false,
                  num_months_housing:               @member_record['num_months_housing'],
                  num_years_business:               @member_record['num_years_business'],
                  num_months_business:              @member_record['num_months_business'],
                  old_certificate_number:           @member_record['old_certificate_number'],
                  address_street:                   @member_record['address_street'],
                  address_barangay:                 @member_record['address_barangay'],
                  address_city:                     @member_record['address_city']
                )

      # Beneficiaries
      @member_record['beneficiaries'].each do |b_data|
        member.beneficiaries  <<  Beneficiary.new(
                                    first_name: b_data['first_name'],
                                    middle_name: b_data['middle_name'],
                                    last_name: b_data['last_name'],
                                    relationship: b_data['relationship'],
                                    date_of_birth: b_data['date_of_birth'],
                                    is_primary: b_data['is_primary'],
                                    reference_number: b_data['reference_number'],
                                    uuid: b_data['uuid']
                                  )
      end

      # Legal Depenents
      @member_record['legal_dependents'].each do |l_data|
        member.legal_dependents <<  LegalDependent.new(
                                      first_name: l_data['first_name'],
                                      middle_name: l_data['middle_name'],
                                      last_name: l_data['last_name'],
                                      date_of_birth: l_data['date_of_birth'],
                                      reference_number: l_data['reference_number'],
                                      educational_attainment: l_data['educational_attainment'],
                                      course: l_data['course'],
                                      uuid: l_data['uuid']
                                    )
      end

      # Store meta
      meta  = @member_record['meta']
      if meta
        identification_numbers  = meta['identification_numbers']

        if identification_numbers
          identification_numbers << @member_record['identification_number']
        else
          identification_numbers  = [@member_record['identification_number']]
        end

        meta['identification_numbers'] = identification_numbers
      else
        meta  = { identification_numbers: [@member_record['identification_number']] }
      end

      member.meta = meta

      if !member.save
        ap member
        raise member.errors.inspect
      end

      member.branch.update(member_counter: member.branch.member_counter + 1)

      member
    end
  end
end
