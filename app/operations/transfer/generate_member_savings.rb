module Transfer
  class GenerateMemberSavings
    require 'application_helper'

    def initialize(member:, member_record:, user:, voucher:)
      @member         = member
      @member_record  = member_record
      @user           = user
      @c_working_date = ApplicationHelper.current_working_date
      @voucher        = voucher
      @approved_by    = user.full_name
    end

    def execute!
      @member.savings_accounts.delete_all

      @member_record['savings'].each do |savings_record|
        balance         = savings_record['balance'].to_f
        amount          = balance
        savings_type    = SavingsType.find(savings_record['savings_type_id'])
        savings_account = SavingsAccount.new(
                            account_number: savings_record['account_number'],
                            savings_type:   savings_type,
                            uuid:           savings_record['uuid'],
                            member:         @member,
                            branch:         @member.branch,
                            center:         @member.center
                          )

        savings_account.save!

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @c_working_date,
                                          created_at: @c_working_date,
                                          transaction_type: "deposit",
                                          particular: @voucher.particular,
                                          voucher_reference_number: @voucher.reference_number,
                                          savings_account: savings_account
                                        )

          savings_account_transaction.approve!(@approved_by)
        end
      end
    end
  end
end
