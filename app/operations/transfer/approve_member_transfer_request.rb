module Transfer
  class ApproveMemberTransferRequest
    def initialize(member_transfer_request:)
      @member_transfer_request  = member_transfer_request
    end

    def execute!
      approve_loan_transfers!

      @member_transfer_request.member.update!(status: "transferred")
      @member_transfer_request.approve!
    end

    private

    def approve_loan_transfers!
      @member_transfer_request.member_loan_transfer_requests.each do |o|
        o.loan.update!(status: "transferred")
      end
    end
  end
end
