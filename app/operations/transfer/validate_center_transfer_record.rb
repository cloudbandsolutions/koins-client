module Transfer
  class ValidateCenterTransferRecord
    def initialize(center_transfer_record:, user:)
      @center_transfer_record = center_transfer_record
      @user                   = user
      @voucher                = ::Transfer::ProduceCenterTransferRecordAccountingEntry.new(
                                  center_transfer_record: @center_transfer_record,
                                  user: @user
                                ).execute!
      @errors                 = []
    end

    def execute!
      if !@voucher.valid?
      end
      @errors
    end
  end
end
