module Transfer
  class GenerateMemberInsurance
    require 'application_helper'

    def initialize(member:, member_record:, user:, voucher:)
      @member         = member
      @member_record  = member_record
      @user           = user
      @c_working_date = ApplicationHelper.current_working_date
      @voucher        = voucher
      @approved_by    = user.full_name
    end

    def execute!
      @member.insurance_accounts.delete_all

      @member_record['insurance'].each do |insurance_record|
        balance           = insurance_record['balance'].to_f
        amount            = balance
        insurance_type    = InsuranceType.find(insurance_record['insurance_type_id'])
        insurance_account = InsuranceAccount.new(
                            account_number: insurance_record['account_number'],
                            insurance_type:   insurance_type,
                            uuid:           insurance_record['uuid'],
                            member:         @member,
                            branch:         @member.branch,
                            center:         @member.center
                          )

        insurance_account.save!

        if amount > 0
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @c_working_date,
                                          created_at: @c_working_date,
                                          transaction_type: "deposit",
                                          particular: @voucher.particular,
                                          voucher_reference_number: @voucher.reference_number,
                                          insurance_account: insurance_account
                                        )

          insurance_account_transaction.approve!(@approved_by)
        end
      end
    end
  end
end
