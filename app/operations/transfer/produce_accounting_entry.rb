module Transfer
  class ProduceAccountingEntry
    require 'application_helper'

    def initialize(member_transfer_request:, user:)
      @member_transfer_request  = member_transfer_request
      @book                     = 'JVB'
      @user                     = user
      @c_working_date           = ApplicationHelper.current_working_date
      @voucher                  = Voucher.new(
                                    book: @book, 
                                    prepared_by: @user.full_name,
                                    particular: @member_transfer_request.particular
                                  )
    end

    def execute!
      # savings
      generate_entries_for_savings!

      # equity
      generate_entries_for_equity!

      # insurance
      generate_entries_for_insurance!

      # loans
      generate_entries_for_loans!

      @voucher
    end

    private

    def generate_entries_for_savings!
      @member_transfer_request.member_savings_transfer_requests.each do |member_savings_transfer_request|
        amount              = member_savings_transfer_request.amount
        dr_accounting_code  = member_savings_transfer_request.dr_accounting_code
        cr_accounting_code  = member_savings_transfer_request.dr_accounting_code


        if amount > 0 and member_savings_transfer_request.complete?
          @voucher.journal_entries  <<  JournalEntry.new(
                                          amount: amount,
                                          post_type: "DR",
                                          accounting_code: dr_accounting_code
                                        )

          @voucher.journal_entries  <<  JournalEntry.new(
                                          amount: amount,
                                          post_type: "CR",
                                          accounting_code: cr_accounting_code
                                        )
        end
      end
    end

    def generate_entries_for_equity!
      @member_transfer_request.member_equity_transfer_requests.each do |member_equity_transfer_request|
        amount              = member_equity_transfer_request.amount
        dr_accounting_code  = member_equity_transfer_request.dr_accounting_code
        cr_accounting_code  = member_equity_transfer_request.dr_accounting_code

        if amount > 0 and member_equity_transfer_request.complete?
          @voucher.journal_entries  <<  JournalEntry.new(
                                          amount: amount,
                                          post_type: "DR",
                                          accounting_code: dr_accounting_code
                                        )

          @voucher.journal_entries  <<  JournalEntry.new(
                                          amount: amount,
                                          post_type: "CR",
                                          accounting_code: cr_accounting_code
                                        )
        end
      end
    end

    def generate_entries_for_loans!
      @member_transfer_request.member_loan_transfer_requests.each do |member_loan_transfer_request|
        amount              = member_loan_transfer_request.amount
        dr_accounting_code  = member_loan_transfer_request.dr_accounting_code
        cr_accounting_code  = member_loan_transfer_request.dr_accounting_code

        if amount > 0 and member_loan_transfer_request.complete?
          @voucher.journal_entries  <<  JournalEntry.new(
                                          amount: amount,
                                          post_type: "DR",
                                          accounting_code: dr_accounting_code
                                        )

          @voucher.journal_entries  <<  JournalEntry.new(
                                          amount: amount,
                                          post_type: "CR",
                                          accounting_code: cr_accounting_code
                                        )
        end
      end
    end

    def generate_entries_for_insurance!
    end
  end
end
