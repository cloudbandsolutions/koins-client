module Transfer
  class GenerateMemberEquity
    require 'application_helper'

    def initialize(member:, member_record:, user:, voucher:)
      @member         = member
      @member_record  = member_record
      @user           = user
      @c_working_date = ApplicationHelper.current_working_date
      @voucher        = voucher
      @approved_by    = user.full_name
    end

    def execute!
      @member.equity_accounts.delete_all

      @member_record['equity'].each do |equity_record|
        balance         = equity_record['balance'].to_f
        amount          = balance
        equity_type     = EquityType.find(equity_record['equity_type_id'])
        equity_account  = EquityAccount.new(
                            account_number: equity_record['account_number'],
                            equity_type:    equity_type,
                            uuid:           equity_record['uuid'],
                            member:         @member,
                            branch:         @member.branch,
                            center:         @member.center
                          )

        equity_account.save!

        if amount > 0
          equity_account_transaction = EquityAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @c_working_date,
                                          created_at: @c_working_date,
                                          transaction_type: "deposit",
                                          particular: @voucher.particular,
                                          voucher_reference_number: @voucher.reference_number,
                                          equity_account: equity_account
                                        )

          equity_account_transaction.approve!(@approved_by)
        end
      end
    end
  end
end
