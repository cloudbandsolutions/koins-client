module Transfer
  class CleanTransferRecords
    def initialize
      @approved_records  = MemberTransferRequest.approved
    end

    def execute!
      @approved_records.each do |approved_record|
        approved_record.update!(status: "processed")
        approved_record.member.update!(status: "cleared")
      end
    end
  end
end
