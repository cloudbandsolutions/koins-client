module Transfer
  class BuildTransferredMembers
    def initialize
      @members          = Member.where(status: "transferred").order("last_name ASC")
      @loan_products    = LoanProduct.all
      @insurance_types  = InsuranceType.all
      @savings_types    = SavingsType.all
      @equity_types     = EquityType.all
      @approved_records = MemberTransferRequest.approved
      @data             = {}
      @data[:branch]    = Branch.first.code
      @data[:branch_id] = Branch.first.id
      @data[:members]   = []
      @data[:totals]    = {}

      @data[:totals][:loan_products]  = []
      @data[:totals][:savings]        = []
      @data[:totals][:insurance]      = []
      @data[:totals][:equity]         = []
    end

    def execute!
      build_totals!
      build_members!

      @data
    end

    private

    def build_totals!
      @savings_types.each do |o|
        savings_type    = o.to_s
        savings_type_id = o.id
        total_amount    = SavingsAccount.where(member_id: @members.pluck(:id), savings_type_id: o.id).sum(:balance)

        if total_amount > 0
          @data[:totals][:savings] << {
            savings_type: savings_type,
            savings_type_id: savings_type_id,
            total_amount: total_amount
          }
        end
      end

      @insurance_types.each do |o|
        insurance_type    = o.to_s
        insurance_type_id = o.id
        total_amount    = InsuranceAccount.where(member_id: @members.pluck(:id), insurance_type_id: o.id).sum(:balance)

        if total_amount > 0
          @data[:totals][:insurance] << {
            insurance_type: insurance_type,
            insurance_type_id: insurance_type_id,
            total_amount: total_amount
          }
        end
      end

      @equity_types.each do |o|
        equity_type     = o.to_s
        equity_type_id  = o.id
        total_amount    = EquityAccount.where(member_id: @members.pluck(:id), equity_type_id: o.id).sum(:balance)

        if total_amount > 0
          @data[:totals][:equity] << {
            equity_type: equity_type,
            equity_type_id: equity_type_id,
            total_amount: total_amount
          }
        end
      end

      @loan_products.each do |o|
        loan_product            = o.to_s
        loan_product_id         = o.id
        total_principal_balance = 0.00
        total_interest_balance  = 0.00

        #Loan.where(member_id: @members.pluck(:id), status: 'transferred', loan_product_id: loan_product_id).each do |loan|
        @approved_records.each do |approved_record|
          approved_record.member_loan_transfer_requests.each do |mltr|
            loan  = mltr.loan
            if loan
              if loan.loan_product.id == loan_product_id
                total_principal_balance += loan.principal_balance
                total_interest_balance  += loan.interest_balance
              end
            end
          end
        end

        if total_principal_balance > 0 or total_interest_balance > 0
          @data[:totals][:loan_products] << {
            loan_product: loan_product,
            loan_product_id: loan_product_id,
            total_principal_balance: total_principal_balance,
            total_interest_balance: total_interest_balance
          }
        end
      end
    end

    def build_members!
      @members.each do |member|
        member_data = {}

        member_data[:member]  = member.attributes

        member_data[:member][:beneficiaries]  = []
        member.beneficiaries.each do |beneficiary|
          member_data[:member][:beneficiaries] << {
            first_name:       beneficiary.first_name,
            last_name:        beneficiary.last_name,
            middle_name:      beneficiary.middle_name,
            relationship:     beneficiary.relationship,
            date_of_birth:    beneficiary.date_of_birth,
            is_primary:       beneficiary.is_primary,
            reference_number: beneficiary.reference_number,
            uuid:             beneficiary.uuid
          }
        end

        member_data[:member][:legal_dependents] = []
        member.legal_dependents.each do |legal_dependent|
          member_data[:member][:legal_dependents] << {
            first_name:             legal_dependent.first_name,
            last_name:              legal_dependent.last_name,
            middle_name:            legal_dependent.middle_name,
            date_of_birth:          legal_dependent.date_of_birth,
            reference_number:       legal_dependent.reference_number,
            educational_attainment: legal_dependent.educational_attainment,
            uuid:                   legal_dependent.uuid,
            course:                 legal_dependent.course
          }
        end

        # loans
        member_data[:loans] = []
        member.loans.where(status: 'transferred').each do |loan|
          member_data[:loans] << {
            bank_check_number: loan.bank_check_number,
            beneficiary_first_name: loan.beneficiary_first_name,
            beneficiary_middle_name: loan.beneficiary_middle_name,
            beneficiary_last_name: loan.beneficiary_last_name,
            beneficiary_relationship: loan.beneficiary_relationship,
            book: loan.book,
            clip_number: loan.clip_number,
            date_approved: loan.date_approved,
            date_of_release: loan.date_of_release,
            date_prepared: loan.date_prepared,
            insured_amount: loan.insured_amount,
            interest_balance: loan.interest_balance,
            interest_rate: loan.interest_rate,
            is_insured: loan.is_insured,
            loan_product_id: loan.loan_product.id,
            loan_product_type_id: loan.loan_product_type.id,
            maturity_date: loan.maturity_date,
            original_amount: loan.amount,
            original_num_installments: loan.original_num_installments,
            num_installments: loan.num_installments,
            remaining_num_installments: loan.remaining_num_installments,
            payment_type: loan.payment_type,
            pn_number: loan.pn_number,
            prepared_by: loan.prepared_by,
            principal_balance: loan.principal_balance,
            project_type_category_id: loan.project_type_category_id,
            project_type_id: loan.project_type_id,
            term: loan.term,
            uuid: loan.uuid,
            voucher_check_number: loan.voucher_check_number,
            first_date_of_payment: loan.ammortization_schedule_entries.unpaid.first.due_at,
            loan_cycle_count: loan.loan_cycle_count
          }
        end
        
        # savings
        member_data[:savings] = []
        member.savings_accounts.each do |savings_account|
          member_data[:savings] <<  savings_account.attributes
        end

        # insurance
        member_data[:insurance] = []
        member.insurance_accounts.each do |insurance_account|
          member_data[:insurance] << insurance_account.attributes
        end

        # equity
        member_data[:equity]  = []
        member.equity_accounts.each do |equity_account|
          member_data[:equity] << equity_account.attributes
        end

        @data[:members] << member_data
      end
    end
  end
end
