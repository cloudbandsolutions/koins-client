module Utils
  class WriteToJsonFile
    def initialize(path:, filename:, data:)
      @filename   = filename
      @path       = path
      @full_path  = "#{@path}/#{@filename}"
      @data       = data
    end

    def execute!
      File.open(@full_path, "w") do |f|
        f.write(JSON.pretty_generate(@data).force_encoding("UTF-8"))
      end

      @full_path
    end
  end
end
