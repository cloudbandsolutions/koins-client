module Debug
  class FetchMissingInsuranceTransactions
    def initialize
      @insurance_account_transactions = InsuranceAccountTransaction.approved.where(
                                            "insurance_account_id IS NULL AND amount > 0 and status = 'approved'"
                                          ).order("created_at ASC")
    end

    def execute!
      @insurance_account_transactions
    end
  end
end
