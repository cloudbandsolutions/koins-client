module Debug
  class FetchInvalidMemberships
    def initialize
      @members  = Member.active.order("last_name ASC")
      @records  = []
    end

    def execute!
      @members.each do |member|
        if member.previous_mii_member_since.nil?
          @records  <<  {
            member: member,
            membership_type: Settings.cooperative_membership_type
          }
        end

        if member.previous_mfi_member_since.nil?
          @records  <<  {
            member: member,
            membership_type: Settings.insurance_membership_type_name
          }
        end
      end

      @records
    end
  end
end
