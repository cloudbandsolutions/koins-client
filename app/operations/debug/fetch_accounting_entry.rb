module Debug
  class FetchAccountingEntry
    def initialize(reference_number:)
      @reference_number = reference_number
    end

    def execute!
      Voucher.where(
        reference_number: @reference_number, 
      )
    end
  end
end
