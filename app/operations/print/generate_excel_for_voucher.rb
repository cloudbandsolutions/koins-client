module Print
  class GenerateExcelForVoucher
    def initialize(voucher:)
      @voucher  = voucher
      @p        = Axlsx::Package.new
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center = wb.styles.add_style(alignment: {horizontal: :center})
          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["", "", "#{Settings.company} - #{@voucher.branch.to_s.upcase}", "", ""], style: @header_cells, widths: [30, 30, 30, 30, 30]
          sheet.add_row ["", "", "#{Settings.company_address}"], style: @header_cells
          if @voucher.crb?
            sheet.add_row ["", "", "CASH VOUCHER"], style: [nil, nil, @header_border]
          elsif @voucher.jvb?
            sheet.add_row ["", "", "JOURNAL RECEIPT"], style: [nil, nil, @header_border]
          elsif @voucher.cdb?
            sheet.add_row ["", "", "CASH DISBURSETMENT"], style: [nil, nil, @header_border]
          end
          sheet.add_row []
          sheet.add_row ["Reference No.:", "#{@voucher.reference_number.rjust(8, '0') if @voucher.reference_number.present?}", "Doc. No.:", ""], style: [@header_cells, @left_aligned_bold_cell, @header_cells, @left_aligned_bold_cell]
          sheet.add_row ["Date Approved:", "#{@voucher.date_prepared.strftime('%m/%d/%Y') if !@voucher.date_prepared.nil?}", "Date Requested:", ""], style: [@header_cells, @left_aligned_bold_cell, @header_cells, @left_aligned_bold_cell]
          sheet.add_row ["", "", "", "", ""], style: @underline_cell
          sheet.add_row []
          sheet.add_row ["Account Code", "Account Name", "", "Debit", "Credit"], style: [@underline_bold_cell, @underline_bold_cell, nil, @underline_bold_cell, @underline_bold_cell]
          @voucher.journal_entries.order_accounting.each do |journal_entry|
            if journal_entry.post_type == "DR" and journal_entry.amount > 0
              sheet.add_row ["#{journal_entry.accounting_code.code}", journal_entry.accounting_code.name, "", journal_entry.amount], style: [@center_cell, nil, @right_aligned_cell, @currency_cell_right, @currency_cell_right_bold, nil]
            end
          end

          @voucher.journal_entries.order_accounting.each do |journal_entry|
            if journal_entry.post_type == "CR" and journal_entry.amount > 0
              sheet.add_row ["#{journal_entry.accounting_code.code}",journal_entry.accounting_code.name, "", "", journal_entry.amount], style: [@center_cell, nil, @right_aligned_cell, @currency_cell_right, @currency_cell_right, nil]
            end
          end
          sheet.add_row ["", "", "Total:", "#{@voucher.total_debit}", "#{@voucher.total_credit}"], style: [nil, nil, @currency_cell_right_bold, @currency_cell_right_bold, @currency_cell_right_bold]
          sheet.add_row ["", "", "*** Nothing Follows ***"], style: @header_cells
          sheet.add_row []
          sheet.add_row ["Particular: #{@voucher.particular}", "", "", "", ""], style: @default_cell, widths: [30, 30, 30, 30, 30] 
          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["#{@voucher.prepared_by}", "", "#{@voucher.approved_by}"], style: @underline_cell
          sheet.add_row ["Prepared by:", "Reviewed by:", "Approved by:"], style: @default_cell
        end
      end

      @p
    end

    private
    def initialize_formats!(wb)
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @right_aligned_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri" 
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
  end
end
