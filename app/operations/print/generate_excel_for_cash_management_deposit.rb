module Print
  class GenerateExcelForCashManagementDeposit
    def initialize(payment_collection:)
      @payment_collection  = payment_collection
      @p                   = Axlsx::Package.new
      @savings_types       = SavingsType.all
      @insurance_types     = InsuranceType.all
      @equity_types        = EquityType.all
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center = wb.styles.add_style(alignment: {horizontal: :center})
          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["", "", "", "#{Settings.company} - #{@payment_collection.branch.to_s.upcase}", "", ""], style: @header_cells
          sheet.add_row ["", "", "", "#{Settings.company_address}"], style: @header_cells
          sheet.add_row ["", "", "", "Cash Management - Deposit"], style: @header_cells
          sheet.add_row []
          sheet.add_row ["", "OR No.: #{@payment_collection.or_number.rjust(8, '0')}"], style: [@header_cells, @left_aligned_bold_cell]
          sheet.add_row []

          header_saving_type_labels = []
          @savings_types.each do |saving_type|
            header_saving_type_labels << saving_type.code
          end

          header_insurance_type_labels = []
          @insurance_types.each do |insurance_type|
            header_insurance_type_labels << insurance_type.code
          end

          header_equity_type_labels = []
          @equity_types.each do |equity_type|
            header_equity_type_labels << equity_type.code
          end

          header_data = []
          header_data << ""
          header_data << "Member"
          header_saving_type_labels.each do |s|
            header_data << s
          end
          header_insurance_type_labels.each do |i|
            header_data << i
          end
          header_equity_type_labels.each do |e|
            header_data << e
          end
          header_data << "Amt Rec"
          sheet.add_row header_data, style: @left_aligned_bold_cell, widths: [5, 40, 15, 15, 15, 15, 15, 20]
          #sheet.add_row ["", "Member", "GK", "K-IMPOK", "LIF", "RF", "SC", "Amt Rec"], style: @left_aligned_bold_cell, widths: [5, 35, 10, 15, 10, 10, 10, 20]
          
          @payment_collection.payment_collection_records.joins(:member).order("members.last_name ASC").each_with_index do |payment_collection_record, i|  
            
            saving_transaction_data = []
            @savings_types.each do |saving_type|
              code = saving_type.code
              payment_collection_record.collection_transactions.where(account_type: "SAVINGS").each do |s|
                if s.account_type_code == code
                  saving_transaction_data << s.amount
                end
              end
            end
            insurance_transaction_data = []
            @insurance_types.each do |insurance_type|
              code = insurance_type.code
              payment_collection_record.collection_transactions.where(account_type: "INSURANCE").each do |i|
                if i.account_type_code == code
                  insurance_transaction_data << i.amount
                end
              end
            end
            equity_transaction_data = []
            @equity_types.each do |equity_type|
              code = equity_type.code
              payment_collection_record.collection_transactions.where(account_type: "EQUITY").each do |e|
                if e.account_type_code == code
                  equity_transaction_data << e.amount
                end
              end
            end
           
            temp_data = []
            temp_data << "#{i + 1}"
            temp_data << "#{payment_collection_record.member.full_name_middle_initial}"
            saving_transaction_data.each do |s|
              temp_data << s
            end
            insurance_transaction_data.each do |i|
              temp_data << i
            end
            equity_transaction_data.each do |e|
              temp_data << e
            end
            temp_data << "#{payment_collection_record.total_cp_amount}"  
            sheet.add_row temp_data, style: [@center_cell, @left_align_cell, @currency_cell, @currency_cell, @currency_cell, @currency_cell, @currency_cell, @currency_cell]
            #sheet.add_row ["#{i + 1}", "#{payment_collection_record.member.full_name}", "", "", "", "", "", "#{payment_collection_record.total_cp_amount}"], style: [@center_cell, @left_align_cell, @currency_cell, @currency_cell, @currency_cell, @currency_cell, @currency_cell, @currency_cell]
          end

          total_saving_transaction_data = []
          @savings_types.each do |saving_type|
            code = saving_type.code
            total_saving_transaction_data << CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "SAVINGS", account_type_code: code).sum(:amount)
          end
          total_insurance_transaction_data = []
          @insurance_types.each do |insurance_type|
            code = insurance_type.code
            total_insurance_transaction_data << CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "INSURANCE", account_type_code: code).sum(:amount)
          end
          total_equity_transaction_data = []
          @equity_types.each do |equity_type|
            code = equity_type.code
            total_equity_transaction_data << CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "EQUITY", account_type_code: code).sum(:amount)
          end

          total_data = []
          total_data << ""
          total_data << "Total"
          total_saving_transaction_data.each do |st|
            total_data << st
          end 
          total_insurance_transaction_data.each do |it|
            total_data << it
          end 
          total_equity_transaction_data.each do |et|
            total_data << et
          end 
          total_data << "#{@payment_collection.total_amount}"

          sheet.add_row total_data, style: [nil, @left_aligned_bold_cell, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold]
          #sheet.add_row ["", "Total:", "", "", "", "", "", "#{@payment_collection.total_amount}"], style: [nil, @left_aligned_bold_cell, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold]
        end
      end

      @p
    end

    private
    def initialize_formats!(wb)
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_left_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @right_aligned_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri" 
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
  end
end
