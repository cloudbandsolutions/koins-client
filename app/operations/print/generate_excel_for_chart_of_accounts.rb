module Print
  class GenerateExcelForChartOfAccounts
    def initialize()
      @p = Axlsx::Package.new
      @major_group = MajorGroup.select("*").order(:code)

    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          sheet.add_row []
          center = wb.styles.add_style(alignment: {horizontal: :center})
          sheet.add_row [ "CHART OF ACCOUNTS" ], style: @left_aligned_bold_cell
          sheet.add_row []
          @major_group.each do |major_group|
            sheet.add_row ["#{major_group}", "#{major_group.code.rjust(8, '0')}"], style: @left_aligned_bold_cell, widths: [70, 30]

            major_group.major_accounts.order(:code).each do |major_account|
              sheet.add_row ["#{major_account}", "#{major_account.code.rjust(8, '0')}"], style: @left_aligned_bold_cell 

              major_account.mother_accounting_codes.each do |mother_accounting_code|
                sheet.add_row ["#{mother_accounting_code}", "#{mother_accounting_code.code.rjust(8, '0')}"], style: @left_aligned_bold_cell
          
                mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
                  sheet.add_row ["#{accounting_code_category.name}", "#{accounting_code_category.code.rjust(8, "0")}"], style: @left_aligned_bold_cell
                  
                  accounting_code_category.accounting_codes.each do |accounting_code|
                    sheet.add_row ["#{accounting_code.name}", "#{accounting_code.code}", "#{accounting_code.accounting_code_category.mother_accounting_code.major_account.major_group.dc_code}"], style: @default_cell 
                  end
                end  
              end
            end    
          end
        end
      end

      @p
    end

    private
    def initialize_formats!(wb)
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_left_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @right_aligned_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri" 
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
  end
end
