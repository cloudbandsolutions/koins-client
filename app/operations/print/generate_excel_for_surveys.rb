module Print
  class GenerateExcelForSurveys
    def initialize(survey:)
      @survey  = survey
      @p       = Axlsx::Package.new
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center = wb.styles.add_style(alignment: {horizontal: :center})
          sheet.add_row ["", "#{@survey.title.upcase}"], style: @header_cells
          sheet.add_row []
          sheet.add_row ["", "Respondent", "Branch", "Date Answered", "Score"], style: @left_aligned_bold_cell, widths: [5, 50, 20, 20, 15]
          @survey.survey_respondents.each_with_index do |survey_respondent, i|  
            sheet.add_row ["#{i + 1}", "#{survey_respondent.member.full_name if !survey_respondent.member.nil?}", "#{survey_respondent.member.branch if !survey_respondent.member.nil?}", "#{survey_respondent.date_answered}", "#{survey_respondent.total_score}"], style: @default_cell
          end
        end
      end

      @p
    end

    private
    def initialize_formats!(wb)
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_left_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @right_aligned_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri" 
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
  end
end
