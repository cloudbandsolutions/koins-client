module Print
  class GenerateExcelForCashManagementFundTransfer
    def initialize(payment_collection:)
      @payment_collection  = payment_collection
      @p                   = Axlsx::Package.new
      @savings_types       = SavingsType.all
      @insurance_types     = InsuranceType.all
      @equity_types        = EquityType.all
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center = wb.styles.add_style(alignment: {horizontal: :center})
          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["", "", "#{Settings.company} - #{@payment_collection.branch.to_s.upcase}", "", ""], style: @header_cells
          sheet.add_row ["", "", "#{Settings.company_address}"], style: @header_cells
          sheet.add_row ["", "", "Cash Management - Fund Transfer"], style: @header_cells
          sheet.add_row []
          sheet.add_row ["", "OR No.: #{@payment_collection.or_number.rjust(8, '0')}"], style: [@header_cells, @left_aligned_bold_cell]
          sheet.add_row []

          header_transaction_labels = []
          @insurance_types.each do |insurance_type|
            header_transaction_labels << insurance_type.code
          end

          header_data = []
          header_data << ""
          header_data << "Member"
          header_transaction_labels.each do |h|
            header_data << h
          end
          header_data << "Amt Rec"
          sheet.add_row header_data, style: @left_aligned_bold_cell, widths: [5, 35, 32, 32, 32]
          #sheet.add_row ["", "Member", "LIF", "RF", "Amt Rec"], style: @left_aligned_bold_cell, widths: [5, 35, 40, 40, 40]
          @payment_collection.payment_collection_records.joins(:member).order("members.last_name ASC").each_with_index do |payment_collection_record, i|  
            
            temp_transaction_data = []
            @insurance_types.each do |insurance_type|
              code = insurance_type.code
              payment_collection_record.collection_transactions.where(account_type: "INSURANCE_FUND_TRANSFER").each do |t|
                if t.account_type_code == code
                  temp_transaction_data << t.amount
                end
              end
            end
            #temp_data = ["#{i + 1}", "#{payment_collection_record.member.full_name}", "", "", "#{payment_collection_record.total_cp_amount}"]
            temp_data = []
            temp_data << "#{i + 1}"
            temp_data << "#{payment_collection_record.member.full_name}"
            temp_transaction_data.each do |t|
              temp_data << t
            end
            temp_data << "#{payment_collection_record.total_cp_amount}"
            sheet.add_row temp_data, style: [@center_cell, @left_align_cell, @currency_cell, @currency_cell, @currency_cell]
          end

          total_transaction_data = []
          @insurance_types.each do |insurance_type|
            code = insurance_type.code
            total_transaction_data << CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "INSURANCE_FUND_TRANSFER", account_type_code: code).sum(:amount)
          end

          total_data = []
          total_data << ""
          total_data << "Total"
          total_transaction_data.each do |tt|
            total_data << tt
          end 
          total_data << "#{@payment_collection.total_amount}"
          sheet.add_row total_data, style: [nil, @left_aligned_bold_cell, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold]
          # sheet.add_row ["", "Total:", "", "", "#{@payment_collection.total_amount}"],style: [nil, @left_aligned_bold_cell, @currency_cell_left_bold, @currency_cell_left_bold, @currency_cell_left_bold]
        end
      end

      @p
    end

    private
    def initialize_formats!(wb)
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_left_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @right_aligned_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri" 
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
  end
end
