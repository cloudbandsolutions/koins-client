module PaymentCollections
  class CreatePaymentCollectionForMembershipPayment
    attr_accessor :branch, :center, :paid_at, :prepared_by, :payment_collection

    def initialize(branch:, paid_at:, prepared_by:, members:)
      @branch           = branch
      @paid_at          = paid_at
      @prepared_by      = prepared_by
      @members          = members
      @savings_types    = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types     = EquityType.all
      @membership_types = MembershipType.all
      @particular       = "Payment of Membership/Deposit of Funds"
      @payment_collection = PaymentCollection.new(
                              branch: @branch,
                              particular: @particular,
                              or_number: "#{Time.now.to_i} CHANGE-ME",
                              prepared_by: @prepared_by,
                              paid_at: @paid_at,
                              payment_type: "membership"
                            )
    end

    def execute!
      @members.each do |member|
        payment_collection_record = PaymentCollectionRecord.new(
                                      member: member
                                    )

        @membership_types.each do |membership_type|
          amount = membership_type.fee

          if MembershipPayment.paid.where(member_id: member.id, membership_type_id: membership_type.id).count > 0
            amount = 0.00
          end

          if amount.nil?
            amount = 0.00
          end

          # Check for waive_member_types_for_insurance_membership
          if Settings.waive_member_types_for_insurance_membership.include?(member.member_type) and membership_type.activated_for_insurance == true

            amount = 0.00
          end

          collection_transaction =  CollectionTransaction.new(
                                      amount: amount,
                                      account_type_code: membership_type.name,
                                      account_type: "MEMBERSHIP_PAYMENT"
                                    )

          payment_collection_record.collection_transactions << collection_transaction
        end

        # ID Payment
        if Settings.id_payment_activated == true
          collection_transaction =  CollectionTransaction.new(
                                      amount: 0.00,
                                      account_type_code: "ID_PAYMENT",
                                      account_type: "ID_PAYMENT",
                                    )

          payment_collection_record.collection_transactions << collection_transaction
        end

        @savings_types.each do |savings_type|
          amount = 0.00
          collection_transaction =  CollectionTransaction.new(
                                      amount: amount,
                                      account_type_code: savings_type.code,
                                      account_type: "SAVINGS"
                                    )

          payment_collection_record.collection_transactions << collection_transaction
        end

        @insurance_types.each do |insurance_type|
          amount = 0.00
          collection_transaction =  CollectionTransaction.new(
                                      amount: amount,
                                      account_type_code: insurance_type.code,
                                      account_type: "INSURANCE"
                                    )

          payment_collection_record.collection_transactions << collection_transaction
        end

        @equity_types.each do |equity_type|
          #amount = Settings.equity_maximum_amount
          #default to 0
          amount  = 0.00

          if amount.nil?
            amount = 0.00
          end

          collection_transaction =  CollectionTransaction.new(
                                      amount: amount,
                                      account_type_code: equity_type.code,
                                      account_type: "EQUITY"
                                    )
          
          payment_collection_record.collection_transactions << collection_transaction
        end

        @payment_collection.payment_collection_records << payment_collection_record
      end

      @payment_collection
    end
  end
end
