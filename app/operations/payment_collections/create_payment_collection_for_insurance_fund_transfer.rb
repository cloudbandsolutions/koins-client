module PaymentCollections
  class CreatePaymentCollectionForInsuranceFundTransfer
    attr_accessor :branch, :date_of_payment, :prepared_by, :payment_collection

    def initialize(branch:, date_of_payment:, prepared_by:, members:)
      @branch           = branch
      @paid_at          = date_of_payment
      @prepared_by      = prepared_by
      @members          = members
      @insurance_types  = InsuranceType.all
      @particular       = "Payment of Insurance accounts"
      @payment_collection = PaymentCollection.new(
                              branch: @branch,
                              particular: @particular,
                              or_number: "CHANGE-ME-#{Time.now.to_i}",
                              prepared_by: @prepared_by,
                              paid_at: @paid_at,
                              payment_type: "insurance_fund_transfer"
                            )
    end

    def execute!
      @members.each do |member|
        payment_collection_record = PaymentCollectionRecord.new(
                                      member: member
                                    )


        @insurance_types.each do |insurance_type|
          amount = 0.00
          collection_transaction =  CollectionTransaction.new(
                                      amount: amount,
                                      account_type_code: insurance_type.code,
                                      account_type: "INSURANCE_FUND_TRANSFER"
                                    )

          payment_collection_record.collection_transactions << collection_transaction
        end

        @payment_collection.payment_collection_records << payment_collection_record
      end

      @payment_collection.save!

      @payment_collection
    end
  end
end
