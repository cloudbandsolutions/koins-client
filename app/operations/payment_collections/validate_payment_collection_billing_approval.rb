module PaymentCollections
  class ValidatePaymentCollectionBillingApproval
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @savings_types = SavingsType.all
      @insurance_types = InsuranceType.all
      @equity_types = EquityType.all
      @collection_transactions = CollectionTransaction.where(
                                  account_type: "WP",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))

      @insurance_collection_transactions = CollectionTransaction.where(
                                  account_type: "INSURANCE",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))
      
      @voucher = PaymentCollections::ProduceVoucherForPaymentCollection.new(payment_collection: @payment_collection).execute!
      @errors = []
    end

    def execute!
      check_or_number!
      check_valid_status!
      check_if_billing!
      check_collection_transactions!
      check_valid_voucher!
      check_for_member_if_has_pending_validation!
      check_member_if_exit_age!

      @errors
    end

    private

    def check_or_number!
      if !@payment_collection.special_report?
        if !@payment_collection.or_number.present?
          @errors << "Or number required"
        else
          or_numbers = PaymentCollection.approved.where.not(or_number: nil).pluck(:or_number)

          if or_numbers.include? @payment_collection.or_number
            @errors << "Or number taken for #{@payment_collection.or_number}. Transaction ID: #{PaymentCollection.approved.where(or_number: @payment_collection.or_number).first.try(:id)}"
          end
        end
      end
    end

    def check_valid_voucher!
      dr_cr_amounts = @voucher.debit_credit_amounts
      if dr_cr_amounts[:debit] != dr_cr_amounts[:credit]
        @errors << "Unbalanced accounting entry! Debit: #{dr_cr_amounts[:debit]} Credit: #{dr_cr_amounts[:credit]}"
      end
    end

    def check_collection_transactions!
      @collection_transactions.each do |ct|
        amount          = ct.amount
        member          = ct.payment_collection_record.member
        savings_account = member.default_savings_account
        if (savings_account.balance - amount) < savings_account.maintaining_balance and amount > 0
          @errors << "Cannot withdraw #{amount} for account #{savings_account.account_number}. Maintaining balance: #{savings_account.maintaining_balance}"
        end
      end
    end

    def check_member_if_exit_age!
      @insurance_collection_transactions.each do |ict|
        amount          = ict.amount
        member          = ict.payment_collection_record.member
        if member.age >= 65 and amount > 0
          @errors << "Cannot deposit to #{ict.account_type_code} account. #{member} is already #{member.age} years old!"
        end
      end
    end

    def check_for_member_if_has_pending_validation!
      @payment_collection.payment_collection_records.each do |payment_collection_record|
        member = payment_collection_record.member
        if member.meta.nil?
          if member.insurance_account_validation_records.count > 0 && payment_collection_record.collection_transactions.where("account_type = ?", "INSURANCE").sum(:amount) > 0
            member.insurance_account_validation_records.each do |insurance_account_validation_record|
              if insurance_account_validation_record.is_void != true
                @errors << "#{member} already have validation!"
              end
            end      
          end    
        end
      end
    end

    def check_if_billing!
      if !@payment_collection.billing?
        @errors << "This is not a billing transaction"
      end
    end

    def check_valid_status!
      if @payment_collection.approved?
        @errors << "This transaction is already approved"
      end

      if @payment_collection.reversed?
        @errors << "This transaction is reversed."
      end
    end
  end
end
