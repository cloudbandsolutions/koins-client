module PaymentCollections
  class ValidatePaymentCollectionForApproval
    def initialize(payment_collection: payment_collection)
      @payment_collection = payment_collection
      @errors             = []
    end

    def execute!
      if payment_collection.billing?
        @errors = PaymentCollections::ValidatePaymentCollectionBillingReversal.new(payment_collection: @payment_collection).execute!
      elsif payment_collection.insurance_fund_transfer?
        @errors = PaymentCollections::ValidatePaymentCollectionInsuranceFundTransferReversal.new(payment_collection: @payment_collection).execute!
      elsif payment_collection.deposit?
        @errors = PaymentCollections::ValidatePaymentCollectionDepositReversal.new(payment_collection: @payment_collection).execute!
      elsif payment_collection.withdraw?
        @errors = PaymentCollections::ValidatePaymentCollectionWithdrawReversal.new(payment_collection: @payment_collection).execute!
      elsif payment_collection.insurance?
        @errors = PaymentCollections::ValidatePaymentCollectionInsurancePaymentReversal.new(payment_collection: @payment_collection).execute!
      elsif payment_collection.membership?
        @errors = PaymentCollections::ValidatePaymentCollectionMembershipPaymentReversal.new(payment_collection: @payment_collection).execute!
      else
        raise "Invalid payment collection type"
      end

      @errors
    end
  end
end
