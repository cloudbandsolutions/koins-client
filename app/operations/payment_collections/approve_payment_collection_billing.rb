module PaymentCollections
  class ApprovePaymentCollectionBilling
    attr_accessor :payment_collection, :errors

    require 'application_helper'

    def initialize(payment_collection:, user:)
      @payment_collection = payment_collection
      @user               = user
      @voucher            = PaymentCollections::ProduceVoucherForPaymentCollection.new(payment_collection: @payment_collection).execute!
      @approved_by        = @user.full_name
      @c_working_date     = ApplicationHelper.current_working_date
    end

    def execute!
      if @payment_collection.status != 'approved'
        @voucher.save!
        @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
        @payment_collection.update!(
          status: "approved", 
          reference_number: @voucher.reference_number,
          paid_at: @c_working_date,
          updated_at: @c_working_date,
        )
      end

      approve_loan_payments!
      perform_savings_deposit!
      perform_withdraw_payments!
      perform_insurance_deposit!
      perform_equity_deposit!

      @payment_collection
    end

    private

    def approve_loan_payments!
      collection_transaction_ids = []
      @payment_collection.payment_collection_records.each do |payment_collection_record|
        payment_collection_record.collection_transactions.each do |ct|
          collection_transaction_ids << ct.id
        end
      end

      loan_payments = LoanPayment.where(id: CollectionTransaction.where(account_type: "LOAN_PAYMENT", id: collection_transaction_ids).pluck(:loan_payment_id))
      loan_payments.each do |loan_payment|
        if loan_payment.status == 'pending'
          loan_payment.approve_payment!
        end
      end
    end

    def perform_savings_deposit!
      savings_transactions = CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      savings_transactions.each do |savings_transaction|
        amount          = savings_transaction.amount
        member          = savings_transaction.payment_collection_record.member
        savings_type    = SavingsType.where(code: savings_transaction.account_type_code).first
        savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @payment_collection.paid_at,
                                          created_at: @payment_collection.paid_at,
                                          transaction_type: "deposit",
                                          particular: @payment_collection.particular,
                                          voucher_reference_number: @payment_collection.reference_number,
                                          savings_account: savings_account
                                        )

          savings_account_transaction.approve!(@approved_by)
        end
      end
    end

    def perform_insurance_deposit!
      insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      insurance_transactions.each do |insurance_transaction|
        amount            = insurance_transaction.amount
        member            = insurance_transaction.payment_collection_record.member
        insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
        insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

        if amount > 0
           if insurance_account.insurance_type_id == 1
              equity_amount = amount.to_f / 2
              insurance_account_transaction = InsuranceAccountTransaction.create!(
                                                amount: amount,
                                                transacted_at: @payment_collection.paid_at,
                                                created_at: @payment_collection.paid_at,
                                                transaction_type: "deposit",
                                                particular: @payment_collection.particular,
                                                voucher_reference_number: @payment_collection.reference_number,
                                                equity_value: equity_amount + insurance_account.equity_value,
                                                insurance_account: insurance_account
                                              )

              insurance_account_transaction.approve!(@approved_by)
              insurance_account.update!(equity_value: insurance_account_transaction.ending_balance.to_f / 2)
            else
              insurance_account_transaction = InsuranceAccountTransaction.create!(
                                                amount: amount,
                                                transacted_at: @payment_collection.paid_at,
                                                created_at: @payment_collection.paid_at,
                                                transaction_type: "deposit",
                                                particular: @payment_collection.particular,
                                                voucher_reference_number: @payment_collection.reference_number,
                                                insurance_account: insurance_account
                                              )
              insurance_account_transaction.approve!(@approved_by)
            end
        end
      end
    end

    def perform_equity_deposit!
      equity_transactions = CollectionTransaction.where(account_type: "EQUITY", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      equity_transactions.each do |equity_transaction|
        amount          = equity_transaction.amount
        member          = equity_transaction.payment_collection_record.member
        equity_type     = EquityType.where(code: equity_transaction.account_type_code).first
        equity_account  = EquityAccount.where(equity_type_id: equity_type.id, member_id: member.id).first
        
        if amount > 0
          equity_account_transaction = EquityAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @payment_collection.paid_at,
                                          created_at: @payment_collection.paid_at,
                                          transaction_type: "deposit",
                                          particular: @payment_collection.particular,
                                          voucher_reference_number: @payment_collection.reference_number,
                                          equity_account: equity_account
                                        )

          equity_account_transaction.approve!(@approved_by)
        end
      end
    end

    def perform_withdraw_payments!
      wp_transactions = CollectionTransaction.where(account_type: "WP", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      wp_transactions.each do |wp_transaction|
        amount          = wp_transaction.amount
        member          = wp_transaction.payment_collection_record.member
        savings_account = member.default_savings_account

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @payment_collection.paid_at,
                                          created_at: @payment_collection.paid_at,
                                          transaction_type: "wp",
                                          is_withdraw_payment: true,
                                          particular: @payment_collection.particular,
                                          voucher_reference_number: @payment_collection.reference_number,
                                          savings_account: savings_account
                                        )

          savings_account_transaction.approve!(@approved_by)
        end
      end
    end
  end
end
