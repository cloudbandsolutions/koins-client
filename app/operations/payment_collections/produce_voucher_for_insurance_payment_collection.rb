module PaymentCollections
  class ProduceVoucherForInsurancePaymentCollection
    require 'application_helper'
    attr_accessor :payment_collection, :voucher

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @particular         = build_particular
      @book               = "JVB"
      @c_working_date     = ApplicationHelper.current_working_date
      @payee = @payment_collection.payee

      if @payment_collection.book.present?
        @book = @payment_collection.book
      end

      if @payment_collection.accounting_fund.present?
        @accounting_fund = @payment_collection.accounting_fund
      else
        @accounting_fund = AccountingFund.first
      end

      if @payment_collection.pending?
        @voucher = Voucher.new(
                      or_number: @payment_collection.or_number, 
                      status: "pending", 
                      branch: @payment_collection.branch, 
                      book: @book,
                      accounting_fund: @accounting_fund,
                      date_prepared: @c_working_date,
                      prepared_by: @payment_collection.prepared_by, 
                      particular: @particular,
                      accounting_fund: @accounting_fund,
                      payee: @payee
                      )

      elsif @payment_collection.approved? or @payment_collection.reversed?
        @voucher = Voucher.where(reference_number: @payment_collection.reference_number, branch_id: @payment_collection.branch.id, book: @book).first
      end

      @bank             = @payment_collection.branch.bank
      @insurance_types  = InsuranceType.all
    end
    
    def execute!
      if @payment_collection.pending?
        build_entries
      end

      @voucher
    end

    private

    def build_particular
      center = @payment_collection.center
      particular = "Deposit of Funds, DS Date #{Date.today}"

      if !@payment_collection.particular.nil?
        particular = @payment_collection.particular
      end

      particular
    end

    def build_entries
      build_debit_entries
      build_credit_entries
    end

    def build_debit_entries
      cash_in_bank    = @payment_collection.total_amount
      if @payment_collection.debit_accounting_code.present?
        accounting_code = @payment_collection.debit_accounting_code
      else  
        accounting_code = AccountingCode.where(id: 22).first
      end

      journal_entry   = JournalEntry.new(
                          amount: cash_in_bank,
                          post_type: 'DR',
                          accounting_code: accounting_code
                        )

      @voucher.journal_entries << journal_entry
    end

    def build_credit_entries
      build_entries_for_insurance_deposits
    end

    def build_entries_for_insurance_deposits
      @insurance_types.each do |insurance_type|
        accounting_code  = insurance_type.deposit_accounting_code
        if @payment_collection.credit_accounting_code
          accounting_code = @payment_collection.credit_accounting_code
        end

        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "INSURANCE",
                                              account_type_code: insurance_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end
  end
end
