module PaymentCollections
  class ValidateNewEquityWithdrawalTransaction
    def initialize(branch_id:, date_of_payment:)
      @branch           = Branch.where(id: branch_id).first
      @date_of_payment  = date_of_payment
      @errors           = []
    end

    def execute!
      validate_required_parameters!
      
      @errors
    end

    private

    def validate_required_parameters!
      if !@branch.present?
        @errors << "Branch required"
      end

      if !@date_of_payment.present?
        @errors << "Date of payment required"
      end
    end
  end
end
