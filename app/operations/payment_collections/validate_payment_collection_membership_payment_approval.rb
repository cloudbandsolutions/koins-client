module PaymentCollections
  class ValidatePaymentCollectionMembershipPaymentApproval
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @membership_types = MembershipType.all
      @errors = []
    end

    def execute!
      check_for_pending!
      check_for_or_number!
      check_for_amount!
      check_for_already_paid!
      check_for_equity!
      @errors
    end

    private

    def check_for_or_number!
      if !@payment_collection.or_number.present?
        @errors << "OR Number required"
      end

      if PaymentCollection.where.not(id: @payment_collection.id)
                          .where(or_number: @payment_collection.or_number)
                          .count > 0
        @errors << "OR Number #{@payment_collection.or_number} already taken"
      end
    end

    def check_for_equity!
      @payment_collection.payment_collection_records.each do |payment_collection_record|
        payment_collection_record.collection_transactions.where(account_type: "EQUITY").each do |collection_transaction|
          member = collection_transaction.payment_collection_record.member
          amount = collection_transaction.amount
          max_amount = Settings.equity_maximum_amount
          member_equity_amount = member.equity_amount
          if member.equity_amount + amount > max_amount
            @errors << "Cannot deposit equity for member #{member.to_s}. Current balance: #{member_equity_amount}. Max amount: #{max_amount}"
          end
        end
      end
    end

    def check_for_already_paid!
      @membership_types.each do |membership_type|
        CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type_code: membership_type.name).each do |collection_transaction|
          amount = collection_transaction.amount
          member = collection_transaction.payment_collection_record.member
          if amount > 0
            if MembershipPayment.paid.where(membership_type_id: membership_type.id, member_id: member.id).count > 0
              @errors << "#{member.full_name} is already a member of #{membership_type.name}"
            end
          end
        end
      end
    end

    def check_for_pending!
      if !@payment_collection.pending? 
        @errors << "This record is not pending"
      end
    end

    def check_for_amount!
      if @payment_collection.total_amount == 0
        @errors << "This record has no amount"
      end
    end
  end
end
