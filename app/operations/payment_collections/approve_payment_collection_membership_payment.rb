module PaymentCollections
  class ApprovePaymentCollectionMembershipPayment
    attr_accessor :payment_collection, :errors

    require 'application_helper'

    def initialize(payment_collection:, user:)
      @payment_collection = payment_collection
      @user               = user
      @c_working_date     = ApplicationHelper.current_working_date
    end

    def execute!
      voucher = PaymentCollections::ProduceVoucherForMembershipPaymentPaymentCollection.new(payment_collection: @payment_collection).execute!
      voucher.save!
      ::Accounting::ApproveVoucher.new(voucher: voucher, user: @user).execute!

      @payment_collection.update!(
        reference_number: voucher.reference_number,
        paid_at: @c_working_date
      )

      create_membership_payments!
      create_savings_deposits!
      create_insurance_deposits!
      create_equity_deposits!
      @payment_collection.update!(status: "approved")
      @payment_collection
    end

    private

    def create_equity_deposits!
      CollectionTransaction.where(account_type: "EQUITY", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)).each do |collection_transaction|
        if collection_transaction.amount > 0
          member          = collection_transaction.payment_collection_record.member
          equity_type    = EquityType.where(code: collection_transaction.account_type_code).first
          equity_account = EquityAccount.where(member_id: member.id, equity_type_id: equity_type.id).first
          equity_account_transaction = EquityAccountTransaction.create!(
                                            amount: collection_transaction.amount,
                                            transacted_at: @payment_collection.paid_at,
                                            created_at: @payment_collection.paid_at,
                                            particular: @payment_collection.particular,
                                            transaction_type: "deposit",
                                            voucher_reference_number: @payment_collection.reference_number,
                                            equity_account: equity_account
                                          )
          equity_account_transaction.approve!(@user.full_name)
        end
      end
    end

    def create_insurance_deposits!
      CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)).each do |collection_transaction|
        if collection_transaction.amount > 0
          member          = collection_transaction.payment_collection_record.member
          insurance_type    = InsuranceType.where(code: collection_transaction.account_type_code).first
          insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).first
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: collection_transaction.amount,
                                            transacted_at: @payment_collection.paid_at,
                                            created_at: @payment_collection.paid_at,
                                            particular: @payment_collection.particular,
                                            transaction_type: "deposit",
                                            voucher_reference_number: @payment_collection.reference_number,
                                            insurance_account: insurance_account
                                          )
          insurance_account_transaction.approve!(@user.full_name)
        end
      end
    end

    def create_savings_deposits!
      CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)).each do |collection_transaction|
        if collection_transaction.amount > 0
          member          = collection_transaction.payment_collection_record.member
          savings_type    = SavingsType.where(code: collection_transaction.account_type_code).first
          savings_account = SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id).first
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: collection_transaction.amount,
                                          transacted_at: @payment_collection.paid_at,
                                          created_at: @payment_collection.paid_at,
                                          transaction_type: "deposit",
                                          particular: @payment_collection.particular,
                                          voucher_reference_number: @payment_collection.reference_number,
                                          savings_account: savings_account
                                        )
          savings_account_transaction.approve!(@user.full_name)
        end
      end
    end

    def create_membership_payments!
      CollectionTransaction.where(account_type: "MEMBERSHIP_PAYMENT", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)).each do |collection_transaction|
        if collection_transaction.amount > 0
          member          = collection_transaction.payment_collection_record.member
          membership_type = MembershipType.where(name: collection_transaction.account_type_code).first
          MembershipPayment.create!(
            member: member,
            membership_type: membership_type,
            paid_at: @payment_collection.paid_at,
            created_at: @payment_collection.paid_at,
            amount_paid: collection_transaction.amount,
            status: "paid"
          )

          if membership_type.activated_for_loans == true
            member.update(previous_mfi_member_since: @payment_collection.paid_at)
          elsif membership_type.activated_for_insurance == true
            member.update(previous_mii_member_since: @payment_collection.paid_at)
          end

          member.update(status: "active")
        end
      end
    end
  end
end
