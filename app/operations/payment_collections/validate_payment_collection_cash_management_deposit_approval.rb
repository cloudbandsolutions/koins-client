module PaymentCollections
  class ValidatePaymentCollectionCashManagementDepositApproval
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @savings_types = SavingsType.all
      @insurance_types = InsuranceType.all
      @equity_types = EquityType.all
      @errors = []
    end

    def execute!
      check_for_pending!
      check_for_or_number!
      check_for_amount!
      check_for_equity!
      if Settings.activate_microinsurance
        check_for_member_if_has_pending_validation!
      end
      @errors
    end

    private

    def check_for_or_number!
      if !@payment_collection.or_number.present?
        if @payment_collection.book == 'CRB'
          @errors << "OR Number required"
        end
      end

      if PaymentCollection.where.not(id: @payment_collection.id)
                          .where(or_number: @payment_collection.or_number)
                          .count > 0
        @errors << "OR Number #{@payment_collection.or_number} already taken"
      end
    end

    def check_for_equity!
      @payment_collection.payment_collection_records.each do |payment_collection_record|
        payment_collection_record.collection_transactions.where(account_type: "EQUITY").each do |collection_transaction|
          member = collection_transaction.payment_collection_record.member
          amount = collection_transaction.amount
          max_amount = Settings.equity_maximum_amount
          member_equity_amount = member.equity_amount
          if member.equity_amount + amount > max_amount
            @errors << "Cannot deposit equity for member #{member.to_s}. Current balance: #{member_equity_amount}. Max amount: #{max_amount}"
          end
        end
      end
    end

    def check_for_member_if_has_pending_validation!
      @payment_collection.payment_collection_records.each do |payment_collection_record|
        member = payment_collection_record.member
        if member.insurance_account_validation_records.count > 0
          @errors << "#{member} has pending validation"
        end
      end
    end

    def check_for_pending!
      if !@payment_collection.pending? 
        @errors << "This record is not pending"
      end
    end

    def check_for_amount!
      if @payment_collection.total_amount == 0
        @errors << "This record has no amount"
      end
    end
  end
end
