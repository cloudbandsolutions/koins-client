module PaymentCollections
  class ValidatePaymentCollectionInsurancePaymentApproval
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @insurance_types = InsuranceType.all
      @errors = []
    end

    def execute!
      check_for_pending!
      check_for_amount!
      check_for_valid_fields!
      @errors
    end

    private

    def check_for_valid_fields!
      if !@payment_collection.valid?
        @payment_collection.errors.messages.each do |m|
          @errors << m
        end
      end
    end

    def check_for_pending!
      if !@payment_collection.pending? 
        @errors << "This record is not pending"
      end
    end

    def check_for_amount!
      if @payment_collection.total_amount == 0
        @errors << "This record has no amount"
      end
    end
  end
end
