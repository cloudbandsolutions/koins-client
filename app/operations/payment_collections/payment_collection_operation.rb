module PaymentCollections  
  class PaymentCollectionOperation
    def self.validate_for_approval(payment_collection)
      # Validate WP
      errors = []
      collection_transactions = CollectionTransaction.where(
                                  account_type: "WP",
                                  id: CollectionTransaction.where(payment_collection_record_id: payment_collection.payment_collection_records.pluck(:id)))

      collection_transactions.each do |ct|
        amount          = ct.amount
        member          = ct.payment_collection_record.member
        savings_account = member.default_savings_account
        if (savings_account.balance - amount) < savings_account.maintaining_balance and amount > 0
          errors << "Cannot withdraw #{amount} for account #{savings_account.account_number}. Maintaining balance: #{savings_account.maintaining_balance}"
        end
      end

      if errors.count > 0
        errors.each do |e|
          Rails.logger.debug e
        end
        false
      else
        true
      end
    end

    def self.reverse!(payment_collection, current_user)
      approved_by = current_user.full_name

      # Generate reverse voucher
      voucher = ProduceVoucherForPaymentCollection.new(payment_collection: payment_collection).execute!
      reversed_voucher = Accounting::GenerateReverseEntry.new(voucher: voucher).execute!

      # Invalidate loan payments
      loan_payments = LoanPayment.approved.where(id: CollectionTransaction.where(account_type: "LOAN_PAYMENT", payment_collection_record_id: payment_collection.payment_collection_records.pluck(:id)).pluck(:loan_payment_id))
      loan_payments.each do |loan_payment|
        # Loans::LoanPaymentOperation.reverse_loan_payment!(loan_payment)
        Loans::ReverseLoanPayment.new(loan_payment: loan_payment).execute!
        if loan_payment.loan.paid?
          loan_payment.loan.update!(status: 'active')
        end
      end

      # Perform Savings reverse withdrawals
      savings_transactions = CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: payment_collection.payment_collection_records.pluck(:id))
      savings_transactions.each do |savings_transaction|
        amount          = savings_transaction.amount
        member          = savings_transaction.payment_collection_record.member
        savings_type    = SavingsType.where(code: savings_transaction.account_type_code).first
        savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.where(transaction_type: "deposit", savings_account_id: savings_account.id, voucher_reference_number: payment_collection.reference_number).first
          Savings::ReverseSavingsAccountTransaction.new(savings_account_transaction: savings_account_transaction).execute!
        end
      end

      # Perform Insurance reverse withdrawals
      insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: payment_collection.payment_collection_records.pluck(:id))
      insurance_transactions.each do |insurance_transaction|
        amount            = insurance_transaction.amount
        member            = insurance_transaction.payment_collection_record.member
        insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
        insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

        if amount > 0
          insurance_account_transaction = InsuranceAccountTransaction.where(transaction_type: "deposit", insurance_account_id: insurance_account.id, voucher_reference_number: payment_collection.reference_number).first
          Insurance::ReverseInsuranceAccountTransaction.new(insurance_account_transaction: insurance_account_transaction).execute!
        end
      end

      # Perform Equity reverse withdrawals
      equity_transactions = CollectionTransaction.where(account_type: "EQUITY", payment_collection_record_id: payment_collection.payment_collection_records.pluck(:id))
      equity_transactions.each do |equity_transaction|
        amount          = equity_transaction.amount
        member          = equity_transaction.payment_collection_record.member
        equity_type     = EquityType.where(code: equity_transaction.account_type_code).first
        equity_account  = EquityAccount.where(equity_type_id: equity_type.id, member_id: member.id).first
        
        if amount > 0
          equity_account_transaction = EquityAccountTransaction.where(transaction_type: "deposit", equity_account_id: equity_account.id, voucher_reference_number: payment_collection.reference_number).first
          Equity::ReverseEquityAccountTransaction.new(equity_account_transaction: equity_account_transaction).execute!
        end
      end

      # Perform WP reverse
      wp_transactions = CollectionTransaction.where(account_type: "WP", payment_collection_record_id: payment_collection.payment_collection_records.pluck(:id))
      wp_transactions.each do |wp_transaction|
        amount          = wp_transaction.amount
        member          = wp_transaction.payment_collection_record.member
        savings_account = member.default_savings_account

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.where(transaction_type: "wp", savings_account_id: savings_account.id, voucher_reference_number: payment_collection.reference_number).first
          Savings::ReverseSavingsAccountTransaction.new(savings_account_transaction: savings_account_transaction).execute!
        end
      end

      # Update OR number for this payment collection
      payment_collection.update!(status: "reversed", or_number: "#{payment_collection.or_number}-#{Time.now.to_i}-REVERSED", reversed_reference_number: reversed_voucher.reference_number)
    end
  end
end