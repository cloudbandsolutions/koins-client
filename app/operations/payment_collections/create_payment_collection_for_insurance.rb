module PaymentCollections
  class CreatePaymentCollectionForInsurance
    attr_accessor :branch, :center, :paid_at, :prepared_by, :payment_collection

    def initialize(branch:, center:, paid_at:, prepared_by:, members:)
      @branch           = branch
      @center           = center
      @paid_at          = paid_at
      @prepared_by      = prepared_by
      @members          = members
      @insurance_types  = InsuranceType.where.not(code: "HIIP")
      if !@center.nil?
        @particular       = "Insurance Remittance of #{@branch} branch - #{@center.name.titleize} center"
      else
        @particular       = "Insurance Remittance of #{@branch} branch"
      end
        
      @payment_collection = PaymentCollection.new(
                              branch: @branch,
                              center: @center,
                              particular: @particular,
                              or_number: "#{Time.now.to_i}-CHANGE-ME",
                              prepared_by: @prepared_by,
                              paid_at: @paid_at,
                              payment_type: "insurance_remittance",
                              payee: @branch
                            )
    end

    def execute!
      @members.each do |member|
        payment_collection_record = PaymentCollectionRecord.new(
                                      member: member
                                    )

        @insurance_types.each do |insurance_type|
          amount = 0.00

          if insurance_type.default_periodic_payment.present?
            amount = insurance_type.default_periodic_payment
          end

          collection_transaction =  CollectionTransaction.new(
                                      amount: amount,
                                      account_type_code: insurance_type.code,
                                      account_type: "INSURANCE"
                                    )

          payment_collection_record.collection_transactions << collection_transaction
        end

        @payment_collection.payment_collection_records << payment_collection_record
      end

      @payment_collection
    end
  end
end
