module PaymentCollections
  class ProduceVoucherForDepositPaymentCollection
    attr_accessor :payment_collection, :voucher

    require 'application_helper'

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @particular         = build_particular
      @c_working_date     = ApplicationHelper.current_working_date

      if Settings.activate_microinsurance
        if @payment_collection.accounting_fund.present?
          @accounting_fund = @payment_collection.accounting_fund
        else
          @accounting_fund = AccountingFund.first
        end
        
        if @payment_collection.payee.present?
          @payee = @payment_collection.payee
        else
          @payee = @payment_collection.branch
        end

        if @payment_collection.book.present?
          @book = @payment_collection.book
        else
          @book = 'JVB'
        end
      else
         if @payment_collection.book.present?
          @book = @payment_collection.book
        else
          @book = 'CRB'
        end

        @accounting_fund = @payment_collection.accounting_fund
        @payee = @payment_collection.payee
      end

      # if @payment_collection.book.present?
      #   @book = @payment_collection.book
      # end

      # if @payment_collection.accounting_fund.present?
      #   @accounting_fund = @payment_collection.accounting_fund
      # end

      # if @payment_collection.payee.present?
      #   @payee = @payment_collection.payee
      # end
      
      if @payment_collection.pending?
        @voucher = Voucher.new(
                    status: "pending", 
                    branch: @payment_collection.branch, 
                    book: @book,
                    date_prepared: @c_working_date,
                    prepared_by: @payment_collection.prepared_by, 
                    particular: @particular,
                    or_number: @payment_collection.or_number,
                    accounting_fund: @accounting_fund,
                    payee: @payee
                  )
      elsif @payment_collection.approved? or @payment_collection.reversed?
        @voucher = Voucher.where(reference_number: @payment_collection.reference_number, branch_id: @payment_collection.branch.id, book: @book).last
      else
        raise "Invalid payment collection"
      end

      @bank             = @payment_collection.branch.bank
      @savings_types    = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types     = EquityType.all
    end

    def execute!
      if @payment_collection.pending?
        build_entries
      end

      @voucher
    end

    private

    def build_particular
      branch = @payment_collection.branch
      particular = "Deposit of Funds - Branch Name #{branch.name}, DS Date #{@payment_collection.paid_at} DS Amount #{@payment_collection.total_cp_amount}"

      if !@payment_collection.particular.nil?
        particular = @payment_collection.particular
      end

      particular
    end

    def build_entries
      build_debit_entries
      build_credit_entries
    end

    def build_debit_entries
      # DR Cash in Bank
      cash_in_bank    = @payment_collection.total_amount
      if Settings.activate_microinsurance
        if @payment_collection.debit_accounting_code.present?
          accounting_code = @payment_collection.debit_accounting_code
        else  
          accounting_code = AccountingCode.where(id: 22).first
        end  
      else
        if @payment_collection.debit_accounting_code.present?
          accounting_code = @payment_collection.debit_accounting_code
        else
          accounting_code = @bank.accounting_code
        end
      end
      
      journal_entry = JournalEntry.new(
                        amount: cash_in_bank,
                        post_type: 'DR',
                        accounting_code: accounting_code
                      )

      @voucher.journal_entries << journal_entry
    end

    def build_credit_entries
      build_savings_credit_entries
      build_insurance_credit_entries
      build_equity_credit_entries
    end

    def build_savings_credit_entries
      SavingsType.all.each do |savings_type|
        accounting_code = savings_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), 
                                              account_type: "SAVINGS", 
                                              account_type_code: savings_type.code)
                                              .sum(:amount)

        if savings_type.is_default == true and @payment_collection.total_wp_amount > 0
          amount -= @payment_collection.total_wp_amount
        end

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end

    def build_insurance_credit_entries
      @insurance_types.each do |insurance_type|
        accounting_code  = insurance_type.deposit_accounting_code
        if @payment_collection.credit_accounting_code.present?
          accounting_code = @payment_collection.credit_accounting_code
        end
        
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "INSURANCE",
                                              account_type_code: insurance_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end

    def build_equity_credit_entries
      @equity_types.each do |equity_type|
        accounting_code = equity_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "EQUITY",
                                              account_type_code: equity_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end
  end
end
