module PaymentCollections
  class CreatePaymentCollectionForIndividualPayment
    def initialize(amount:, loan:, particular:, paid_at:, prepared_by:)
      @amount         = amount
      @loan           = loan
      @branch         = @loan.branch
      @center         = @loan.center
      @prepared_by    = prepared_by
      @paid_at        = paid_at
      @particular     = particular

      @payment_collection = PaymentCollection.new(
                              branch: @branch,
                              center: @center,
                              particular: @particular,
                              or_number: "#{Time.now.to_i}-CHANGE-ME",
                              prepared_by: @prepared_by,
                              paid_at: @paid_at,
                              payment_type: "billing"
                            )

      checked_by        = User.where(is_active: true, role: 'FM').first.try(:full_name)
      collected_by      = @center.officer_in_charge
      encoded_by        = prepared_by
      posted_by         = User.where(is_active: true, role: 'BK').first.try(:full_name)

      @payment_collection.checked_by    = checked_by
      @payment_collection.collected_by  = collected_by
      @payment_collection.encoded_by    = encoded_by
      @payment_collection.approved_by   = posted_by
    end

    def execute!
      build_payment_collection_record

      @payment_collection
    end

    private 
    def build_payment_collection_record
      loan            = @loan
      next_payment    = @amount
      paid_at         = @paid_at

      loan_payment = LoanPayment.new(
                        amount: next_payment,
                        paid_at: paid_at,
                        loan: loan,
                        loan_payment_amount: next_payment,
                        cp_amount: next_payment,
                        wp_amount: 0,
                        deposit_amount: 0,
                        savings_account: loan.member.default_savings_account,
                        particular: @particular,
                        wp_particular: "n/a"
                      )

      Rails.logger.debug "LOAN PAYMENT VALID? #{loan_payment.valid?}"
      Rails.logger.debug loan_payment.errors.messages

      cycle_count = 0
      member_loan_cycle = MemberLoanCycle.where(
                            member_id: loan.member.id,
                            loan_product_id: loan.loan_product.id
                          ).first.try(:cycle)

      if !member_loan_cycle.nil?
        cycle_count = member_loan_cycle
      end

      payment_collection_record = PaymentCollectionRecord.new(
                                    member: loan.member,
                                    loan_product: loan.loan_product,
                                  )

      Rails.logger.debug "PAYMENT COLLECTION RECORD VALID? #{payment_collection_record.valid?}"
      Rails.logger.debug payment_collection_record.errors.messages

      # Transaction for loan payment
      collection_transaction =  CollectionTransaction.new(
                                  amount: next_payment,
                                  loan_payment: loan_payment,
                                  account_type: "LOAN_PAYMENT"
                                )
      payment_collection_record.collection_transactions << collection_transaction
      
      Rails.logger.debug "VALID COLLECTION TRANSACTION? #{collection_transaction.valid?}"
      Rails.logger.debug collection_transaction.errors.messages

      @payment_collection.payment_collection_records << payment_collection_record
    end
  end
end
