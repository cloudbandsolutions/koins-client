module PaymentCollections
  class ReversePaymentCollection
    def initialize(payment_collection:, user:)
      @user               = user
      @approved_by        = @user.full_name
      @payment_collection = payment_collection
      @membership_types   = MembershipType.all
    end

    def execute!
      if @payment_collection.insurance_fund_transfer?
        reverse_insurance_fund_transfer!
        @payment_collection.update!(status: "reversed")
      elsif @payment_collection.billing?
        reverse_billing!
        new_or_number = "#{@payment_collection.or_number} - REVERSED - #{Time.now.to_i}"
        @payment_collection.update!(status: "reversed", or_number: new_or_number)
      elsif @payment_collection.deposit?
        reverse_deposit!
        new_or_number = "#{@payment_collection.or_number} - REVERSED - #{Time.now.to_i}"
        @payment_collection.update!(status: "reversed", or_number: new_or_number)
      elsif @payment_collection.insurance?
        reverse_insurance!
        new_or_number = "#{@payment_collection.or_number} - REVERSED - #{Time.now.to_i}"
        @payment_collection.update!(status: "reversed", or_number: new_or_number)  
      elsif @payment_collection.withdraw?
        reverse_withdraw!
        new_or_number = "#{@payment_collection.or_number} - REVERSED - #{Time.now.to_i}"
        @payment_collection.update!(status: "reversed", or_number: new_or_number)
      elsif @payment_collection.membership?
        reverse_membership_payment!
        new_or_number = "#{@payment_collection.or_number} - REVERSED - #{Time.now.to_i}"
        @payment_collection.update!(status: "reversed", or_number: new_or_number)
      else
        raise "Invalid payment collection type"
      end
    end

    private

    def reverse_billing!
      voucher           = PaymentCollections::ProduceVoucherForPaymentCollection.new(payment_collection: @payment_collection).execute!
      reversed_voucher  = Accounting::GenerateReverseEntry.new(voucher: voucher).execute!
      @payment_collection.reversed_reference_number = reversed_voucher.reference_number

      # Invalidate loan payments
      loan_payments = LoanPayment.approved.where(id: CollectionTransaction.where(account_type: "LOAN_PAYMENT", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)).pluck(:loan_payment_id))
      loan_payments.each do |loan_payment|
        # Loans::LoanPaymentOperation.reverse_loan_payment!(loan_payment)
        Loans::ReverseLoanPayment.new(loan_payment: loan_payment).execute!
        if loan_payment.loan.paid?
          loan_payment.loan.update!(status: 'active')
        end
      end

      # Perform Savings reverse withdrawals
      savings_transactions = CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      savings_transactions.each do |savings_transaction|
        amount          = savings_transaction.amount
        member          = savings_transaction.payment_collection_record.member
        savings_type    = SavingsType.where(code: savings_transaction.account_type_code).first
        savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.where(transaction_type: "deposit", savings_account_id: savings_account.id, voucher_reference_number: @payment_collection.reference_number).first
           Savings::ReverseSavingsAccountTransaction.new(savings_account_transaction: savings_account_transaction).execute!
        end
      end

      # Perform Insurance reverse withdrawals
      insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      insurance_transactions.each do |insurance_transaction|
        amount            = insurance_transaction.amount
        member            = insurance_transaction.payment_collection_record.member
        insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
        insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

        if amount > 0
          insurance_account_transaction = InsuranceAccountTransaction.where(transaction_type: "deposit", insurance_account_id: insurance_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Insurance::ReverseInsuranceAccountTransaction.new(insurance_account_transaction: insurance_account_transaction).execute!
        end
      end

      # Perform Equity reverse withdrawals
      equity_transactions = CollectionTransaction.where(account_type: "EQUITY", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      equity_transactions.each do |equity_transaction|
        amount          = equity_transaction.amount
        member          = equity_transaction.payment_collection_record.member
        equity_type     = EquityType.where(code: equity_transaction.account_type_code).first
        equity_account  = EquityAccount.where(equity_type_id: equity_type.id, member_id: member.id).first
        
        if amount > 0
          equity_account_transaction = EquityAccountTransaction.where(transaction_type: "deposit", equity_account_id: equity_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Equity::ReverseEquityAccountTransaction.new(equity_account_transaction: equity_account_transaction).execute!
        end
      end

      # Perform WP reverse
      wp_transactions = CollectionTransaction.where(account_type: "WP", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      wp_transactions.each do |wp_transaction|
        amount          = wp_transaction.amount
        member          = wp_transaction.payment_collection_record.member
        savings_account = member.default_savings_account

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.where(transaction_type: "wp", savings_account_id: savings_account.id, voucher_reference_number: @payment_collection.reference_number).first
           Savings::ReverseSavingsAccountTransaction.new(savings_account_transaction: savings_account_transaction).execute!
        end
      end
    end

    def reverse_membership_payment!
      approved_by = @user.full_name

      # Generate reverse voucher
      voucher = PaymentCollections::ProduceVoucherForMembershipPaymentPaymentCollection.new(payment_collection: @payment_collection).execute!
      reversed_voucher = Accounting::GenerateReverseEntry.new(voucher: voucher).execute!
      @payment_collection.reversed_reference_number = reversed_voucher.reference_number

      # Perform reverse for membership payments
      membership_payments = CollectionTransaction.where(account_type: "MEMBERSHIP_PAYMENT", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      membership_payments.each do |mp_transaction|
        amount          = mp_transaction.amount
        member          = mp_transaction.payment_collection_record.member

        if amount > 0
          @membership_types.each do |membership_type|
            name = membership_type.name
            if mp_transaction.account_type_code == name
              MembershipPayment.where(member_id: member.id, membership_type_id: membership_type.id, status: 'paid').each do |membership_payment|
                membership_payment.destroy!

                if membership_type.activated_for_loans == true
                  member.update!(previous_mfi_member_since: nil, status: "pending")
                end

                if membership_type.activated_for_insurance == true
                  member.update!(previous_mii_member_since: nil)
                end
              end
            end
          end
        end
      end

      # Perform Savings reverse withdrawals
      savings_transactions = CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      savings_transactions.each do |savings_transaction|
        amount          = savings_transaction.amount
        member          = savings_transaction.payment_collection_record.member
        savings_type    = SavingsType.where(code: savings_transaction.account_type_code).first
        savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.where(transaction_type: "deposit", savings_account_id: savings_account.id, voucher_reference_number: @payment_collection.reference_number).first
           Savings::ReverseSavingsAccountTransaction.new(savings_account_transaction: savings_account_transaction).execute!
        end
      end

      # Perform Insurance reverse withdrawals
      insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      insurance_transactions.each do |insurance_transaction|
        amount          = insurance_transaction.amount
        member          = insurance_transaction.payment_collection_record.member
        insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
        insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

        if amount > 0
          insurance_account_transaction = InsuranceAccountTransaction.where(transaction_type: "deposit", insurance_account_id: insurance_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Insurance::ReverseInsuranceAccountTransaction.new(insurance_account_transaction: insurance_account_transaction).execute!
        end
      end

      # Perform Equity reverse withdrawals
      equity_transactions = CollectionTransaction.where(account_type: "EQUITY", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      equity_transactions.each do |equity_transaction|
        amount          = equity_transaction.amount
        member          = equity_transaction.payment_collection_record.member
        equity_type    = EquityType.where(code: equity_transaction.account_type_code).first
        equity_account = EquityAccount.where(equity_type_id: equity_type.id, member_id: member.id).first

        if amount > 0
          equity_account_transaction = EquityAccountTransaction.where(transaction_type: "deposit", equity_account_id: equity_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Equity::ReverseEquityAccountTransaction.new(equity_account_transaction: equity_account_transaction).execute!
        end
      end
    end

    def reverse_withdraw!
      approved_by = @user.full_name

      # Generate reverse voucher
      voucher = PaymentCollections::ProduceVoucherForWithdrawalPaymentCollection.new(payment_collection: @payment_collection).execute!
      reversed_voucher = Accounting::GenerateReverseEntry.new(voucher: voucher).execute!
      @payment_collection.reversed_reference_number = reversed_voucher.reference_number

      # Perform Savings reverse deposits
      savings_transactions = CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      savings_transactions.each do |savings_transaction|
        amount          = savings_transaction.amount
        member          = savings_transaction.payment_collection_record.member
        savings_type    = SavingsType.where(code: savings_transaction.account_type_code).first
        savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.where(transaction_type: "withdraw", savings_account_id: savings_account.id, voucher_reference_number: @payment_collection.reference_number).first
           Savings::ReverseSavingsAccountTransaction.new(savings_account_transaction: savings_account_transaction).execute!
        end
      end

      # Perform Insurance reverse deposits
      insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      insurance_transactions.each do |insurance_transaction|
        amount          = insurance_transaction.amount
        member          = insurance_transaction.payment_collection_record.member
        insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
        insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

        if amount > 0
          insurance_account_transaction = InsuranceAccountTransaction.where(transaction_type: "withdraw", insurance_account_id: insurance_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Insurance::ReverseInsuranceAccountTransaction.new(insurance_account_transaction: insurance_account_transaction).execute!
        end
      end

      # Perform Equity reverse deposits
      equity_transactions = CollectionTransaction.where(account_type: "EQUITY", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      equity_transactions.each do |equity_transaction|
        amount          = equity_transaction.amount
        member          = equity_transaction.payment_collection_record.member
        equity_type    = EquityType.where(code: equity_transaction.account_type_code).first
        equity_account = EquityAccount.where(equity_type_id: equity_type.id, member_id: member.id).first

        if amount > 0
          equity_account_transaction = EquityAccountTransaction.where(transaction_type: "withdraw", equity_account_id: equity_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Equity::ReverseEquityAccountTransaction.new(equity_account_transaction: equity_account_transaction).execute!
        end
      end
    end

    def reverse_deposit!
      approved_by = @user.full_name

      # Generate reverse voucher
      voucher = PaymentCollections::ProduceVoucherForDepositPaymentCollection.new(payment_collection: @payment_collection).execute!
      reversed_voucher = Accounting::GenerateReverseEntry.new(voucher: voucher).execute!
      @payment_collection.reversed_reference_number = reversed_voucher.reference_number

      # Perform Savings reverse withdrawals
      savings_transactions = CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      savings_transactions.each do |savings_transaction|
        amount          = savings_transaction.amount
        member          = savings_transaction.payment_collection_record.member
        savings_type    = SavingsType.where(code: savings_transaction.account_type_code).first
        savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.where(transaction_type: "deposit", savings_account_id: savings_account.id, voucher_reference_number: @payment_collection.reference_number).first
           Savings::ReverseSavingsAccountTransaction.new(savings_account_transaction: savings_account_transaction).execute!
        end
      end

      # Perform Insurance reverse withdrawals
      insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      insurance_transactions.each do |insurance_transaction|
        amount          = insurance_transaction.amount
        member          = insurance_transaction.payment_collection_record.member
        insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
        insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

        if amount > 0
          insurance_account_transaction = InsuranceAccountTransaction.where(transaction_type: "deposit", insurance_account_id: insurance_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Insurance::ReverseInsuranceAccountTransaction.new(insurance_account_transaction: insurance_account_transaction).execute!
        end
      end

      # Perform Equity reverse withdrawals
      equity_transactions = CollectionTransaction.where(account_type: "EQUITY", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      equity_transactions.each do |equity_transaction|
        amount          = equity_transaction.amount
        member          = equity_transaction.payment_collection_record.member
        equity_type    = EquityType.where(code: equity_transaction.account_type_code).first
        equity_account = EquityAccount.where(equity_type_id: equity_type.id, member_id: member.id).first

        if amount > 0
          equity_account_transaction = EquityAccountTransaction.where(transaction_type: "deposit", equity_account_id: equity_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Equity::ReverseEquityAccountTransaction.new(equity_account_transaction: equity_account_transaction).execute!
        end
      end
    end

    def reverse_insurance!
      approved_by = @user.full_name

      # Generate reverse voucher
      voucher = PaymentCollections::ProduceVoucherForDepositPaymentCollection.new(payment_collection: @payment_collection).execute!
      reversed_voucher = Accounting::GenerateReverseEntry.new(voucher: voucher).execute!
      @payment_collection.reversed_reference_number = reversed_voucher.reference_number

      # Perform Savings reverse withdrawals
      savings_transactions = CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      savings_transactions.each do |savings_transaction|
        amount          = savings_transaction.amount
        member          = savings_transaction.payment_collection_record.member
        savings_type    = SavingsType.where(code: savings_transaction.account_type_code).first
        savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

        if amount > 0
          savings_account_transaction = SavingsAccountTransaction.where(transaction_type: "deposit", savings_account_id: savings_account.id, voucher_reference_number: @payment_collection.reference_number).first
           Savings::ReverseSavingsAccountTransaction.new(savings_account_transaction: savings_account_transaction).execute!
        end
      end

      # Perform Insurance reverse withdrawals
      insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      insurance_transactions.each do |insurance_transaction|
        amount          = insurance_transaction.amount
        member          = insurance_transaction.payment_collection_record.member
        insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
        insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

        if amount > 0
          insurance_account_transaction = InsuranceAccountTransaction.where(transaction_type: "deposit", insurance_account_id: insurance_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Insurance::ReverseInsuranceAccountTransaction.new(insurance_account_transaction: insurance_account_transaction).execute!
        end
      end

      # Perform Equity reverse withdrawals
      equity_transactions = CollectionTransaction.where(account_type: "EQUITY", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      equity_transactions.each do |equity_transaction|
        amount          = equity_transaction.amount
        member          = equity_transaction.payment_collection_record.member
        equity_type    = EquityType.where(code: equity_transaction.account_type_code).first
        equity_account = EquityAccount.where(equity_type_id: equity_type.id, member_id: member.id).first

        if amount > 0
          equity_account_transaction = EquityAccountTransaction.where(transaction_type: "deposit", equity_account_id: equity_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Equity::ReverseEquityAccountTransaction.new(equity_account_transaction: equity_account_transaction).execute!
        end
      end
    end
   
    def reverse_insurance_fund_transfer!
      # Perform Insurance reverse withdrawals
      insurance_transactions = CollectionTransaction.where(account_type: "INSURANCE_FUND_TRANSFER", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id))
      insurance_transactions.each do |insurance_transaction|
        amount            = insurance_transaction.amount
        member            = insurance_transaction.payment_collection_record.member
        insurance_type    = InsuranceType.where(code: insurance_transaction.account_type_code).first
        insurance_account = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: member.id).first

        if amount > 0
          insurance_account_transaction = InsuranceAccountTransaction.where(transaction_type: "fund_transfer_deposit", insurance_account_id: insurance_account.id, voucher_reference_number: @payment_collection.reference_number).first
          Insurance::ReverseInsuranceAccountTransaction.new(insurance_account_transaction: insurance_account_transaction).execute!
        end
      end
    end
  end
end
