module PaymentCollections
  class ValidatePaymentCollectionCashManagementEquityWithdrawalApproval
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @errors = []
    end

    def execute!
      check_for_pending!
      check_for_amount!
      check_insurance_withdrawables!
      @errors
    end

    private

    def check_for_pending!
      if !@payment_collection.pending? 
        @errors << "This record is not pending"
      end
    end

    def check_for_amount!
      if @payment_collection.total_amount == 0
        @errors << "This record has no amount"
      end
    end

    def check_insurance_withdrawables!
      collection_transactions = CollectionTransaction.where(
                                  account_type: "INSURANCE",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))

      collection_transactions.each do |ct|
        amount            = ct.amount
        member            = ct.payment_collection_record.member
        insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: InsuranceType.where(code: ct.account_type_code).first.id).first
        if (insurance_account.equity_value - amount) < 0
          @errors << "Cannot withdraw #{amount} for #{insurance_account.insurance_type.code} insurance account of #{insurance_account.member.full_name}. Current balance: #{insurance_account.balance}"
        end
      end
    end
  end
end
