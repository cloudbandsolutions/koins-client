module PaymentCollections
  class GenerateWpListForBilling
    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @config             = {}
      @wp_transactions    = CollectionTransaction.joins(
                              :payment_collection_record => [:member, :payment_collection]
                            ).where(
                              "payment_collections.id = ? AND collection_transactions.amount > 0 AND collection_transactions.account_type = ?", 
                              @payment_collection.id, 
                              "WP"
                            ).order("members.last_name")
    end

    def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          generate_config!(wb)
          sheet.add_row []
          sheet.add_row ["", "", "#{Settings.company}", "", ""], style: @config[:title_cell], widths: [40, 10, 25, 25]
          sheet.add_row ["", "", "#{@payment_collection.branch.try(:name).upcase}"], style: @config[:title_cell]
          sheet.add_row ["", "", "Withdraw Payments List Report"], style: @config[:title_cell]
          sheet.add_row []
          sheet.add_row ["Reference No.: #{@payment_collection.reference_number.to_s.rjust(10, '0')}", ""], style: [ @config[:label_cell], @config[:default_cell]]
          sheet.add_row ["Particular: #{@payment_collection.particular}"], style: [@config[:label_cell], @config[:default_cell]]
          sheet.add_row ["Center: #{@payment_collection.center}", ""], style: [@config[:label_cell], @config[:default_cell]]
          sheet.add_row ["Date Approved: #{@payment_collection.paid_at}"], style: [@config[:label_cell], @config[:default_cell]]
          sheet.add_row []

          header = []
          header << "Names"
          header << "Count"
          header << "Total"
          header <<  Member.first.savings_accounts.joins(:savings_type).where("savings_types.is_default = ?", true).first.savings_type.name         
          sheet.add_row header, style: @config[:center_cell_bold]
          sheet.add_row ["#{@payment_collection.center}"], style: @config[:label_cell]
          
          @wp_transactions.each_with_index do |t, i|
            sheet.add_row [t.payment_collection_record.member.full_name, "",  t.amount,  t.amount], style: [@config[:indent_cell], @config[:default_cell_center], @config[:currency_cell_right], @config[:currency_cell_right]]
            @count = i + 1
          end

          center_count_total = []
          center_count_total << "#{@payment_collection.center}"
          center_count_total << @count
          center_count_total << CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "WP").sum(:amount)
          center_count_total << CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "WP").sum(:amount)
          sheet.add_row center_count_total, style: [@config[:label_cell], @config[:center_cell_bold], @config[:currency_cell_right_bold], @config[:currency_cell_right_bold]]

          grand_total_data = []
          grand_total_data << "Grand Total"
          grand_total_data << @count
          grand_total_data << CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "WP").sum(:amount)
          grand_total_data << CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "WP").sum(:amount)
          sheet.add_row grand_total_data, style: [@config[:label_cell], @config[:center_cell_bold], @config[:currency_cell_right_bold], @config[:currency_cell_right_bold]]
          sheet.add_row []
          sheet.add_row []
          sheet.add_row [" Encoded By: ", "", " Posted By: "], style: [@config[:label_cell], @config[:default_cell], @config[:label_cell], @config[:default_cell]], widths: [40, 10, 25, 25]
          sheet.add_row []
          sheet.add_row [ @payment_collection.encoded_by, "", @payment_collection.approved_by], style: @config[:default_cell], widths: [40, 10, 25, 25]
        end
      end

      p
    end

    private

    def generate_config!(wb)
        @config[:normal]                      = wb.styles.add_style font_name: "Calibri", sz: 11, format_code: "#,##0.00",:border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :left }
        @config[:title_cell]                  = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", sz: 11
        @config[:label_cell]                  = wb.styles.add_style b: true, font_name: "Calibri", sz: 11
        @config[:currency_cell]               = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri", sz: 11
        @config[:currency_cell_bold]          = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri", sz: 11, b: true
        @config[:currency_cell_right_bold]    = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", sz: 11, b: true
        @config[:currency_cell_right]         = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", sz: 11
        @config[:currency_cell_center]        = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :center }, format_code: "#,##0.00", font_name: "Calibri", sz: 11,:border => { :style => :thin, :color => "FF000000" }
        @config[:percent_cell]                = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri", sz: 11
        @config[:left_aligned_cell]           = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", sz: 11
        @config[:underline_cell]              = wb.styles.add_style u: true, font_name: "Calibri", sz: 11
        @config[:header_cells]                = wb.styles.add_style font_name: "Calibri", sz: 11, format_code: "#,##0.00",:border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :left }
        @config[:default_cell]                = wb.styles.add_style font_name: "Calibri", sz: 11
        @config[:default_cell_center]         = wb.styles.add_style font_name: "Calibri", sz: 11, alignment: { horizontal: :center }
        @config[:space_row]                   = wb.styles.add_style :border => { :style => :thin, :color => "FF000000" }, sz: 11
        @config[:center_cell_bold]            = wb.styles.add_style b: true, font_name: "Calibri", sz: 11, alignment: { horizontal: :center }
        @config[:indent_cell]                 = wb.styles.add_style alignment: { horizontal: :left , :indent => 1},font_name: "Calibri", sz: 11
    end
  end
end
  
