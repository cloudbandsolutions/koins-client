module PaymentCollections
  class ApprovePaymentCollectionCashManagementEquityWithdrawal
    attr_accessor :payment_collection, :errors

    require 'application_helper'

    def initialize(payment_collection:, user:)
      @payment_collection = payment_collection
      @user               = user
      @c_working_date     = ApplicationHelper.current_working_date
    end

    def execute!

      create_equity_withdraws!

      @payment_collection.update!(
        status: "approved", 
        paid_at: @c_working_date,
        reference_number: "#{Time.now.to_i}",
        updated_at: @c_working_date,
        approved_by: @user
      )

      @payment_collection
    end

    private

    def create_equity_withdraws!
      for_adjustment  = @payment_collection.for_adjustment
      CollectionTransaction.where(account_type: "INSURANCE", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)).each do |collection_transaction|
        if collection_transaction.amount > 0
          member            = collection_transaction.payment_collection_record.member
          insurance_type    = InsuranceType.where(code: collection_transaction.account_type_code).first
          insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).first
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: collection_transaction.amount,
                                            transacted_at: @payment_collection.paid_at,
                                            created_at: @payment_collection.paid_at,
                                            particular: @payment_collection.particular,
                                            transaction_type: "withdraw_ev",
                                            voucher_reference_number: @payment_collection.reference_number,
                                            insurance_account: insurance_account,
                                            is_adjustment: for_adjustment
                                          )

          last_ev_amount = insurance_account.equity_value
          latest_ev_amount = last_ev_amount - collection_transaction.amount
          insurance_account.update(equity_value: latest_ev_amount)
          
          insurance_account_transaction.approve!(@user.full_name)
        end
      end
    end
  end
end
