module PaymentCollections  
  class LoadInsuranceFundTransferFromCsvFile
    attr_accessor :file

    def initialize(file:, branch:, paid_at:, prepared_by:)
      @file = file
      @prepared_by = prepared_by
      @branch = branch
      @paid_at = paid_at
    end

    def execute!    
      load_csv_file!
    end

    private

    def load_csv_file!
      payment_collection = PaymentCollection.new(
              branch: @branch,
              payment_type: 'insurance_fund_transfer',
              paid_at: @paid_at,
              particular: 'Payment of Insurance accounts',
              prepared_by: @prepared_by
          )

      CSV.foreach(@file.path, headers: true) do |row|
        member = Member.where(identification_number: row['identification_number']).first
        payment_collection_record = PaymentCollectionRecord.new(
            member: member
                        )

          @insurance_types  = InsuranceType.all
          @insurance_types.each do |insurance_type|
            if insurance_type.code == 'LIF'
              amount = row['LIF']
            elsif insurance_type.code == 'RF'
              amount = row['RF']
            else
              amount = 0.00
            end
            collection_transaction =  CollectionTransaction.new(
                                        amount: amount,
                                        account_type_code: insurance_type.code,
                                        account_type: "INSURANCE_FUND_TRANSFER"
                                      )

            payment_collection_record.collection_transactions << collection_transaction
          end

          payment_collection.payment_collection_records << payment_collection_record
      end
      payment_collection.save!
    end
  end
end  