module PaymentCollections
  class CreatePaymentCollectionForInsuranceWithdrawals
    attr_accessor :branch, :paid_at, :prepared_by, :payment_collection

    def initialize(branch:, paid_at:, prepared_by:, members:)
      @branch           = branch
      @paid_at          = paid_at
      @prepared_by      = prepared_by
      @insurance_types  = InsuranceType.all
      @members          = members
      @particular       = "Insurance Withdrawal of Funds #{@branch}"
      @payment_collection = PaymentCollection.new(
                              branch: @branch,
                              particular: @particular,
                              or_number: "WT-#{Time.now.to_i}",
                              prepared_by: @prepared_by,
                              paid_at: @paid_at,
                              payment_type: "insurance_withdrawal"
                            )
    end

    def execute!
      build_payment_collection_records
      @payment_collection
    end

    private

    def build_payment_collection_records
      @members.each do |member|
        payment_collection_record = PaymentCollectionRecord.new(
                                      member: member,
                                    )

        # Transactions for insurance
        @insurance_types.each do |insurance_type|
          amount = 0.00
          collection_transaction =  CollectionTransaction.new(
                                      amount: amount,
                                      account_type_code: insurance_type.code,
                                      account_type: "INSURANCE"
                                    )

          payment_collection_record.collection_transactions << collection_transaction
        end

        @payment_collection.payment_collection_records << payment_collection_record
      end
    end
  end
end
