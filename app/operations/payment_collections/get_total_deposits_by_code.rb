module PaymentCollections
  class GetTotalDepositsByCode
    def initialize(payment_collection:, account_type_code:)
      @payment_collection         = payment_collection
      @payment_collection_records = payment_collection.payment_collection_records
      @collection_transactions    = CollectionTransaction.where(payment_collection_record_id: @payment_collection_records.pluck(:id))
      @account_type_code          = account_type_code
    end

    def execute!
      total = @collection_transactions.where(account_type: "SAVINGS", account_type_code: @account_type_code).sum(:amount)

      total
    end
  end
end
