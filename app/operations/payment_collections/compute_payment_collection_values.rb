module PaymentCollections
  class ComputePaymentCollectionValues
    def initialize(payment_collection:)
      @payment_collection = payment_collection
    end

    def execute!
      PaymentCollectionRecord.where(payment_collection_id: @payment_collection.id).each do |payment_collection_record|
        CollectionTransaction.where(payment_collection_record_id: payment_collection_record.id).each do |collection_transaction|
          if collection_transaction.account_type == "WP"
            @payment_collection.total_wp_amount += collection_transaction.amount
            @payment_collection.total_cp_amount -= collection_transaction.amount
          else
            @payment_collection.total_cp_amount += collection_transaction.amount
            if collection_transaction.account_type == "LOAN_PAYMENT"
              loan_payment = collection_transaction.loan_payment 
              @payment_collection.total_principal_amount += loan_payment.payment_stats[:principal]
              @payment_collection.total_interest_amount += loan_payment.payment_stats[:interest]
            end
          end
        end
      end

      @payment_collection.save!
    end
  end
end
