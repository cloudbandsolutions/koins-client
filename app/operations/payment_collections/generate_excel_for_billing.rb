module PaymentCollections
  class GenerateExcelForBilling
    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @savings_types      = SavingsType.all
      @insurance_types    = InsuranceType.all.reverse
      @equity_types       = EquityType.all
      @config             = {}
    end

    def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          generate_config!(wb)
          sheet = generate_headers!(sheet)

          sheet.add_row ["#{Settings.company}"], style: @config[:title_cell]
          sheet.add_row ["#{@payment_collection.branch.try(:name).upcase}"], style: @config[:title_cell]
          @payment_collection.loan_products.order(:is_entry_point).each do |loan_product|
            sheet.add_row []
            sheet.add_row ["#{@payment_collection.center.try(:name)} #{loan_product.name}", @payment_collection.paid_at.strftime("%m/%d/%Y"),"","","","","","","","","","",""], style: @config[:title_cell]
            sheet.add_row build_header_labels, style: @config[:header_cells]

            @payment_collection.records_by_loan_product(loan_product).each do |record|
              sheet.add_row build_transaction_row(record, loan_product), style: @config[:normal]
              sheet.column_widths 55, nil, 15, 10, 10, 10, 8, nil, 10, 10, 10, 10, 10
            end

            # total
            sheet.add_row ["","","","","","","","","","","","",""], style: @config[:space_row]
            sheet.add_row build_total_row(@payment_collection, loan_product), style: @config[:normal]
          end

          sheet.add_row []

          sheet.add_row ["COLLECTION", @payment_collection.total_amount, "", "Center Officers:"], style: [@config[:currency_cell], @config[:currency_cell], nil, @config[:default_cell]]
          sheet.add_row ["LESS WP", @payment_collection.total_wp_amount, "", "__________________"], style: [@config[:currency_cell], @config[:currency_cell], nil, nil]
          sheet.add_row ["GRAND TOTAL", @payment_collection.total_amount, "", "__________________"], style: [@config[:currency_cell], @config[:currency_cell], nil, nil]

          sheet.add_row [nil, nil, nil, "__________________"]

          @payment_collection.loan_products.order(:is_entry_point).each do |loan_product|
            sheet.add_row [loan_product.code, @payment_collection.total_amount_by_loan_product(loan_product)], style: @config[:currency_cell]
          end

          sheet.add_row

          sheet.add_row [" Prepared By: "," Collected By: ", "", " Checked By: " , "", "", " Encoded By: ", "", "", " Posted By: "], style: @config[:default_cell]
          sheet.add_row
          sheet.add_row
          sheet.add_row [ @payment_collection.prepared_by, @payment_collection.collected_by, "", @payment_collection.checked_by, "", "", @payment_collection.encoded_by, "", "", @payment_collection.approved_by], style: @config[:default_cell]

          sheet.add_row
        end
      end

      p


    end

    private

    def build_total_row(payment_collection, loan_product)
      member_ids = payment_collection.payment_collection_records.where(loan_product_id: loan_product.id).pluck(:member_id).uniq

      row = []

      row << "Total"
      row << payment_collection.total_savings_balance(@savings_types.where(is_default: true).first, loan_product)

      total_loan_amount = 0.00
      Loan.active.where(member_id: member_ids, loan_product_id: loan_product.id).each do |loan|
        if loan.temp_total_amount_due
          total_loan_amount += loan.temp_total_amount_due
        end
      end

      row << total_loan_amount

      row << Loan.active.where(member_id: member_ids, loan_product_id: loan_product.id).sum(:remaining_balance)

      row << payment_collection.total_lp_by_loan_product(loan_product)
      @savings_types.each do |savings_type|
        row << payment_collection.total_amount_by_savings_type_and_loan_product(savings_type, loan_product)
      end

      @insurance_types.each do |insurance_type|
        row << payment_collection.total_amount_by_insurance_type_and_loan_product(insurance_type, loan_product)
      end

      @equity_types.each do |equity_type|
        row << payment_collection.total_amount_by_equity_type_and_loan_product(equity_type, loan_product)
      end

      row << payment_collection.total_wp_amount_by_loan_product(loan_product)
      row << payment_collection.total_cp_amount_by_loan_product(loan_product)
      row << payment_collection.total_amount_by_loan_product(loan_product)
    end

    def build_transaction_row(record, loan_product)
      row = []
      row << record.member.full_name_titleize
      
      row << record.member.savings_accounts.joins(:savings_type).where("savings_types.is_default = ?", true).first.try(:balance)

      if loan_product.is_entry_level?
        row << Loan.active.where(member_id: record.member.id, loan_product_id: loan_product.id).first.try(:temp_total_amount_due)
      elsif !Loan.active.where(member_id: record.member.id, loan_product_id: loan_product.id).first.nil?
        row << Loan.active.where(member_id: record.member.id, loan_product_id: loan_product.id).first.try(:temp_total_amount_due)
      else
        row << ""
      end

      if loan_product.is_entry_level?
        row << Loan.active.where(member_id: record.member.id, loan_product_id: loan_product.id).first.try(:remaining_balance)
      elsif !Loan.active.where(member_id: record.member.id, loan_product_id: loan_product.id).first.nil?
        row << Loan.active.where(member_id: record.member.id, loan_product_id: loan_product.id).first.try(:remaining_balance)
      else
        row << ""
      end

      if record.lp_amount_by_member > 0
        row << record.lp_amount_by_member
      else
        row << ""
      end

      @savings_types.each do |savings_type|
        if loan_product.is_entry_level?
          row << record.savings_by_type(savings_type)
        else
          row << ""
        end
      end

      @insurance_types.each do |insurance_type|
        if loan_product.is_entry_level?
          row << record.insurance_by_type(insurance_type)
        else
          row << ""
        end
      end

      @equity_types.each do |equity_type|
        if loan_product.is_entry_level?
          row << record.equity_by_type(equity_type)
        else
          row << ""
        end
      end

      row << record.total_wp_amount
      row << 0.00
      row << record.total_wp_amount + record.total_cp_amount

      row
    end

    def build_header_labels
      header_labels = []
      header_labels << "Name"
      header_labels << "CBU Balance"
      header_labels << "Loan Amt."
      header_labels << "Loan Bal."
      header_labels << "LP"
      @savings_types.each do |savings_type|
        header_labels << savings_type.code
      end
      @insurance_types.each do |insurance_type|
        header_labels << insurance_type.code
      end
      @equity_types.each do |equity_type|
        header_labels << equity_type.code
      end
      header_labels << "WP"
      header_labels << "CP"
      header_labels << "Amt Rec"
    end

    def generate_config!(wb)
        @config[:normal]              = wb.styles.add_style font_name: "Calibri", sz: 11, format_code: "#,##0.00",:border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :left }
        @config[:title_cell]          = wb.styles.add_style alignment: { horizontal: :left }, b: true, font_name: "Calibri", sz: 11
        @config[:label_cell]          = wb.styles.add_style b: true, font_name: "Calibri", sz: 11
        @config[:currency_cell]       = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri", sz: 11,:border => { :style => :thin, :color => "FF000000" }
        @config[:currency_cell_right] = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", sz: 11,:border => { :style => :thin, :color => "FF000000" }
        @config[:percent_cell]        = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri", sz: 11
        @config[:left_aligned_cell]   = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", sz: 11
        @config[:underline_cell]      = wb.styles.add_style u: true, font_name: "Calibri", sz: 11
        @config[:header_cells]        = wb.styles.add_style font_name: "Calibri", sz: 11, format_code: "#,##0.00",:border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :left }
        @config[:default_cell]        = wb.styles.add_style font_name: "Calibri", sz: 11
        @config[:space_row]           = wb.styles.add_style :border => { :style => :thin, :color => "FF000000" }, sz: 11
    end

    def generate_headers!(sheet)
      sheet
    end
  end
end
  
