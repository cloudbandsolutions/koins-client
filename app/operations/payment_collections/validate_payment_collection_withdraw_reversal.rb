module PaymentCollections
  class ValidatePaymentCollectionWithdrawReversal
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @errors = []
    end

    def execute!
      check_non_approved!
      @errors
    end

    private

    def check_non_approved!
      if !@payment_collection.approved?
        @errors << "Can't reverse non-approved transaction"
      end
    end
  end
end
