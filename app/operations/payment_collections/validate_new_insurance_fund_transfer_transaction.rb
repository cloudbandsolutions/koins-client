module PaymentCollections
  class ValidateNewInsuranceFundTransferTransaction
    def initialize(branch:, date_of_payment:, prepared_by:)
      @branch           = branch
      @date_of_payment  = date_of_payment
      @prepared_by      = prepared_by
      @errors           = []
    end

    def execute!
      check_parameters!

      @errors
    end

    private

    def check_parameters!
      if !@branch.present?
        @errors << "Branch required"
      end

      if !@date_of_payment.present?
        @errors << "Date of payment requred"
      end

      if !@prepared_by.present?
        @errors < "Prepared by brequired"
      end
    end
  end
end
