module PaymentCollections
  class ValidatePaymentCollectionInsuranceFundTransferReversal
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @errors = []
    end

    def execute!
      check_insurance_withdrawables!
      @errors
    end

    private

    def check_insurance_withdrawables!
      collection_transactions = CollectionTransaction.where(
                                  account_type: "INSURANCE_FUND_TRANSFER",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))

      collection_transactions.each do |ct|
        amount            = ct.amount
        member            = ct.payment_collection_record.member
        insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: InsuranceType.where(code: ct.account_type_code)).first
        if (insurance_account.balance - amount) < 0
          @errors << "Cannot withdraw #{amount} for #{insurance_account.insurance_type.code} insurance account of #{insurance_account.member.full_name}. Current balance: #{insurance_account.balance}"
        end
      end
    end
  end
end
