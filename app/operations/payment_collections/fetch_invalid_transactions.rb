module PaymentCollections
  class FetchInvalidTransactions
    def initialize
      @payment_collections        = PaymentCollection.approved
      @payment_collection_records = PaymentCollectionRecord.where(payment_collection_id: @payment_collections.pluck(:id))
      @collection_transactions    = CollectionTransaction.where(payment_collection_record_id: @payment_collection_records.pluck(:id))
      @data                       = []
    end

    def execute!
      @collection_transactions.each do |ct|
        # NOTE: WP Detection only
        date_posted = ct.payment_collection_record.payment_collection.paid_at
        member      = ct.payment_collection_record.member
        member_id   = member.id
        payment_collection = ct.payment_collection_record.payment_collection

        if ct.account_type == "WP"
          savings_account_transaction = SavingsAccountTransaction.approved.joins(:savings_account).where("savings_account_transactions.created_at = ? AND savings_accounts.member_id = ? AND transaction_type = 'wp'", date_posted, member_id).first

          if !savings_account_transaction
            @data << {
              payment_collection: payment_collection,
              member: member,
              date_posted: payment_collection.paid_at
            }
          end
        end
      end

      @data
    end
  end
end
