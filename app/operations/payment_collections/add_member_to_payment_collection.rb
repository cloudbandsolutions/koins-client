module PaymentCollections
  class AddMemberToPaymentCollection
    attr_accessor :payment_collection, :member

    def initialize(payment_collection:, member:)
      @savings_types    = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types     = EquityType.all
      @membership_types = MembershipType.all
      @income_insurance_types  = InsuranceType.where(code: "HIIP")
      @member           = member
      @payment_collection = payment_collection
    end

    def execute!
      if @payment_collection.membership?
        build_for_membership!
      elsif @payment_collection.insurance_fund_transfer?
        build_for_insurance_fund_transfer!
      elsif @payment_collection.deposit?
        build_for_deposit!
      elsif @payment_collection.withdraw?
        build_for_withdraw!
      elsif @payment_collection.insurance_withdrawal?
        build_for_insurance_withdrawal!
      elsif @payment_collection.insurance_remittance?
        build_for_insurance_remittance!
      elsif @payment_collection.income_insurance?
        build_for_income_insurance!
      elsif @payment_collection.equity_withdrawal?
        build_for_equity_withdrawal!
      end

      @payment_collection
    end

    private

    def build_for_equity_withdrawal!
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: @member
                                  )

      @insurance_types.where(code: "LIF").each do |insurance_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @payment_collection.payment_collection_records << payment_collection_record
    end

    def build_for_insurance_withdrawal!
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: @member
                                  )

      @insurance_types.each do |insurance_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @payment_collection.payment_collection_records << payment_collection_record
    end

    def build_for_withdraw!
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: @member
                                  )

      @savings_types.each do |savings_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: savings_type.code,
                                    account_type: "SAVINGS"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @insurance_types.each do |insurance_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @equity_types.each do |equity_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: equity_type.code,
                                    account_type: "EQUITY"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @payment_collection.payment_collection_records << payment_collection_record
    end

    def build_for_deposit!
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: @member
                                  )

      @savings_types.each do |savings_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: savings_type.code,
                                    account_type: "SAVINGS"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @insurance_types.each do |insurance_type|
        if Settings.activate_microinsurance
          if insurance_type.code == "LIF"
            amount = 15.00
          elsif insurance_type.code == "RF"
            amount = 5.00
          end  
        else
          amount = 0.00
        end

        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @equity_types.each do |equity_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: equity_type.code,
                                    account_type: "EQUITY"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @payment_collection.payment_collection_records << payment_collection_record
    end

    def build_for_insurance_fund_transfer!
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: @member
                                  )


      @insurance_types.each do |insurance_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE_FUND_TRANSFER"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @payment_collection.payment_collection_records << payment_collection_record
    end

    def build_for_membership!
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: @member
                                  )

      if Settings.id_payment_activated == true
        collection_transaction =  CollectionTransaction.new(
                                    amount: 0.00,
                                    account_type_code: "ID_PAYMENT",
                                    account_type: "ID_PAYMENT",
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @membership_types.each do |membership_type|
        amount = membership_type.fee

        if MembershipPayment.paid.where(member_id: member.id, membership_type_id: membership_type.id).count > 0
          amount = 0.00
        end

        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: membership_type.name,
                                    account_type: "MEMBERSHIP_PAYMENT"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end
      @savings_types.each do |savings_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: savings_type.code,
                                    account_type: "SAVINGS"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @insurance_types.each do |insurance_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @equity_types.each do |equity_type|
        amount = 0.00
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: equity_type.code,
                                    account_type: "EQUITY"
                                  )
        
        payment_collection_record.collection_transactions << collection_transaction
      end

      @payment_collection.payment_collection_records << payment_collection_record
    end

    def build_for_insurance_remittance!
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: @member
                                  )

      @insurance_types.each do |insurance_type|
        amount = 0.00

        if insurance_type.default_periodic_payment.present?
          amount = insurance_type.default_periodic_payment
        end

        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @payment_collection.payment_collection_records << payment_collection_record
    end

    def build_for_income_insurance!
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: @member
                                  )

      @income_insurance_types.each do |insurance_type|
        amount = 0.00

        if insurance_type.default_periodic_payment.present?
          amount = insurance_type.default_periodic_payment
        end

        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      @payment_collection.payment_collection_records << payment_collection_record
    end
  end
end
