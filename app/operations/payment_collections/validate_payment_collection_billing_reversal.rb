module PaymentCollections
  class ValidatePaymentCollectionBillingReversal
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @errors = []
    end

    def execute!
      #check_for_paid_loans!
      check_savings_withdrawables!
      check_insurance_withdrawables!
      check_equity_withdrawables!
      @errors
    end

    private

    def check_for_paid_loans!
      if @payment_collection.has_paid_loans?
        @errors << "This payment collection has a paid loan"
        @payment_collection.loans.each do |loan|
          if loan.paid?
            @errors << "Paid loan: #{loan.pn_number} (#{loan.id})"
          end
        end
      end
    end

    def check_savings_withdrawables!
      collection_transactions = CollectionTransaction.where(
                                  account_type: "SAVINGS",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))

      collection_transactions.each do |ct|
        amount          = ct.amount
        member          = ct.payment_collection_record.member
        savings_account = SavingsAccount.where(member_id: member.id, savings_type_id: SavingsType.where(code: ct.account_type_code)).first
        result = savings_account.balance - amount
        if (result < 0)
          @errors << "Cannot withdraw #{amount} for #{savings_account.savings_type.code} savings account of #{savings_account.member.full_name}. Resulting balance will be less than 0."
        end
      end
    end

    def check_insurance_withdrawables!
      collection_transactions = CollectionTransaction.where(
                                  account_type: "INSURANCE",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))

      collection_transactions.each do |ct|
        amount            = ct.amount
        member            = ct.payment_collection_record.member
        insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: InsuranceType.where(code: ct.account_type_code)).first
        if (insurance_account.balance - amount) < 0
          @errors << "Cannot withdraw #{amount} for #{insurance_account.insurance_type.code} insurance account of #{insurance_account.member.full_name}. Current balance: #{insurance_account.balance}"
        end
      end
    end

    def check_equity_withdrawables!
      collection_transactions = CollectionTransaction.where(
                                  account_type: "EQUITY",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))

      collection_transactions.each do |ct|
        amount            = ct.amount
        member            = ct.payment_collection_record.member
        equity_account = EquityAccount.where(member_id: member.id, equity_type_id: EquityType.where(code: ct.account_type_code)).first
        if (equity_account.balance - amount) < 0
          @errors << "Cannot withdraw #{amount} for #{equity_account.equity_type.code} equity account of #{equity_account.member.full_name}. Current blaance: #{equity_account.balance}"
        end
      end
    end
  end
end
