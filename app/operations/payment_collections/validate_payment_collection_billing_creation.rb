module PaymentCollections
  class ValidatePaymentCollectionBillingCreation
    def initialize(branch_id:, center_id:, paid_at:)
      @branch_id  = branch_id
      @center_id  = center_id

      @branch     = Branch.where(id: branch_id).first
      @center     = Center.where(id: center_id).first
      @paid_at    = paid_at
      @errors     = []
    end

    def execute!
      check_valid_parameters!
      check_existing_pending_transactions!

      @errors
    end

    private

    def check_existing_pending_transactions!
      if PaymentCollection.pending.where(payment_type: "billing", center_id: @center_id, branch_id: @branch_id).count > 0
        @errors << "There are current pending transactions for this center"
      end
    end

    def check_valid_parameters!
      if !@branch.present?
        @errors << "Branch required"
      end

      if !@center.present?
        @errors << "Center required"
      end

      if !@paid_at.present?
        @errors << "Date of payment required"
      end
    end
  end
end
