module PaymentCollections
  class ProduceVoucherForMembershipPaymentPaymentCollection
    attr_accessor :payment_collection, :voucher

    require 'application_helper'

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @particular         = build_particular
      @c_working_date     = ApplicationHelper.current_working_date

      if @payment_collection.pending?
        @voucher = Voucher.new(
                    status: "pending", 
                    branch: @payment_collection.branch, 
                    book: "CRB", 
                    date_prepared: @c_working_date,
                    prepared_by: @payment_collection.prepared_by, 
                    particular: @particular
                  )
      elsif @payment_collection.approved? or @payment_collection.reversed?
        @voucher = Voucher.where(reference_number: @payment_collection.reference_number, branch_id: @payment_collection.branch.id, book: "CRB").first
      end

      @bank             = @payment_collection.branch.bank
      @savings_types    = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types     = EquityType.all
      @membership_types = MembershipType.all
    end
    
    def execute!
      if @payment_collection.pending?
        build_entries
      end
      @voucher.or_number = @payment_collection.or_number
      @voucher
    end

    private

    def build_particular
      center = @payment_collection.center
      particular = "Payment of Membership/Deposit of Funds, DS Date #{Date.today}"

      if !@payment_collection.particular.nil?
        particular = @payment_collection.particular
      end

      particular
    end

    def build_entries
      build_debit_entries
      build_credit_entries
    end

    def build_debit_entries
      cash_in_bank = @payment_collection.total_amount
      journal_entry = JournalEntry.new(
                        amount: cash_in_bank,
                        post_type: 'DR',
                        accounting_code: @bank.accounting_code
                      )

      @voucher.journal_entries << journal_entry
    end

    def build_credit_entries
      if Settings.id_payment_activated == true
        build_entries_for_id_payments
      end

      build_entries_for_membership_payments
      build_entries_for_savings_deposits
      build_entries_for_insurance_deposits
      build_entries_for_equity_deposits
    end

    def build_entries_for_id_payments
      accounting_code = AccountingCode.find(Settings.id_payment_accounting_code_id)
      amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "ID_PAYMENT").sum(:amount)

      if amount > 0
        journal_entry = JournalEntry.new(
                          amount: amount,
                          post_type: 'CR',
                          accounting_code: accounting_code
                        )


        @voucher.journal_entries << journal_entry
      end
    end

    def build_entries_for_membership_payments
      @membership_types.each do |membership_type|
        accounting_code = membership_type.cr_accounting_entry
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "MEMBERSHIP_PAYMENT",
                                              account_type_code: membership_type.name)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end

    def build_entries_for_savings_deposits
      @savings_types.each do |savings_type|
        accounting_code = savings_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), 
                                              account_type: "SAVINGS", 
                                              account_type_code: savings_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end

    def build_entries_for_insurance_deposits
      @insurance_types.each do |insurance_type|
        accounting_code = insurance_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "INSURANCE",
                                              account_type_code: insurance_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end

    def build_entries_for_equity_deposits
      @equity_types.each do |equity_type|
        accounting_code = equity_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "EQUITY",
                                              account_type_code: equity_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end
  end
end
