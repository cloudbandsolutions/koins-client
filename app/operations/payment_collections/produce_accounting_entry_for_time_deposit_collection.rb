module PaymentCollections
  class ProduceAccountingEntryForTimeDepositCollection



    def initialize(payment_collection_id:)

      @payment_collection = PaymentCollection.find(payment_collection_id)
      @book  = @payment_collection.book
      @or_number  = @payment_collection.or_number
      @branch = Branch.where(id: Settings.branch_ids).first
      @c_working_date = ApplicationHelper.current_working_date
      @voucher = Voucher.new(
                    branch: @branch,
                    particular: @payment_collection.particular,
                    book: @book,
                    date_prepared: @c_working_date,
                    or_number: @or_number
                )

    end

    def execute!
      build_debit_entries
      build_credit_entries
      @voucher
    end

    private

    def build_debit_entries
      #accounting_code_debit = AccountingCode.find(429)
      accounting_code_debit = AccountingCode.find(@branch.bank.accounting_code_id)
      debit_amount = @payment_collection.total_time_deposit_amount
      journal_entry = JournalEntry.new(
                        amount: debit_amount,
                        post_type: "DR",
                        accounting_code: accounting_code_debit
                      )
      @voucher.journal_entries << journal_entry
    end

    def build_credit_entries        
      #accounting_code_credit = AccountingCode.find(429)
      accounting_code_credit = AccountingCode.find(SavingsType.find(5).deposit_accounting_code_id)
      credit_amount = @payment_collection.total_time_deposit_amount
      journal_entry = JournalEntry.new(
                        amount: credit_amount,
                        post_type: "CR",
                        accounting_code: accounting_code_credit
                      )
      @voucher.journal_entries << journal_entry
    end



  end
end
