module PaymentCollections
  class CreatePaymentCollectionForIncomeInsurance
    attr_accessor :branch, :center, :paid_at, :prepared_by, :payment_collection, :active_members

    def initialize(branch:, center:, paid_at:, prepared_by:)
      @branch           = branch
      @center_id        = center
      @paid_at          = paid_at
      @prepared_by      = prepared_by
      @insurance_types  = InsuranceType.where(code: "HIIP")
      
      if @center_id != ""
        @center           = Center.find(@center_id)
        @active_loans     = Loan.active.joins(:member).where("members.center_id = ?", @center.id).order("members.last_name")
        @active_members   = Member.pure_active.where(center_id: @center.id, id: @active_loans.pluck(:member_id).uniq)
        @active_non_entry_point_loans = @active_loans.where.not(id: @active_loans.entry_points.pluck("loans.id"))
        @active_entry_point_loans = @active_loans.entry_points
        @particular       = "HIIP Payments of #{@branch} branch - #{@center.name.titleize} center"
      else
        @active_members   = Member.pure_active.where(branch_id: @branch.id)
        @particular       = "HIIP Payments of #{@branch} branch"
      end 
        
      @payment_collection = PaymentCollection.new(
                              branch: @branch,
                              center: @center,
                              particular: @particular,
                              or_number: "#{Time.now.to_i}-CHANGE-ME",
                              prepared_by: @prepared_by,
                              paid_at: @paid_at,
                              payment_type: "income_insurance",
                              payee: @branch
                            )
    end

    def execute!
      @active_members.each do |member|

        hiip_insurance_account = member.insurance_accounts.joins(:insurance_type).where("insurance_types.code = ?", "HIIP").first
        if hiip_insurance_account.balance > 0
          hiip_last_transaction_date = hiip_insurance_account.insurance_account_transactions.last.transacted_at
        end
        
        # loan = member.loans.active.last
        
        # cycle_count = 0
        # member_loan_cycle = MemberLoanCycle.where(member_id: loan.member.id,loan_product_id: loan.loan_product.id).first.try(:cycle)

        # if !member_loan_cycle.nil?
        #   cycle_count = member_loan_cycle
        # end

        # # FIX: Using loan.loan_cycle_count for cycle_count
        # if loan.loan_cycle_count.present?
        #   cycle_count = loan.loan_cycle_count
        # end

        # Check if loan/s is 1 and above
        if member.loans.count > 1

          # Check if has payment in HIIP
          # if !hiip_last_transaction_date.nil?
            
            # Check if 11 the date passed 12 months less 5 days or 360 days
            # if hiip_last_transaction_date < 11.month.ago && hiip_last_transaction_date < 360.day.ago
              payment_collection_record = PaymentCollectionRecord.new(
                                            member: member
                                          )

              @insurance_types.each do |insurance_type|
                amount = 0.00

                if insurance_type.default_periodic_payment.present?
                  amount = insurance_type.default_periodic_payment
                end

                collection_transaction =  CollectionTransaction.new(
                                            amount: amount,
                                            account_type_code: insurance_type.code,
                                            account_type: "INSURANCE"
                                          )

                payment_collection_record.collection_transactions << collection_transaction
              end

              @payment_collection.payment_collection_records << payment_collection_record
            # end
          # end
        end
      end

      @payment_collection
    end
  end
end
