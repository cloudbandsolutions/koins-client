module PaymentCollections
  class ApproveInterestTimeDeposit
    def initialize
      @user = User.find(1)
      @c_working_date = ApplicationHelper.current_working_date
      @deposit_time_transaction = DepositTimeTransaction.where("status = ? and end_date <= ?","lock-in", @c_working_date)
      @voucher = ::PaymentCollections::GenerateAccountingEntryTimeDepositInterest.new(user: @user).execute! 
      @branch = Settings.branch_ids.last
    end

    def execute!
      @voucher.save!
      @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
      total_interest_amount = @deposit_time_transaction.sum(:time_deposit_interest_amount)
      total_principal_amount = @deposit_time_transaction.sum(:amount)

      total_time_deposit = total_principal_amount.to_f + total_interest_amount.to_f
      @payment_collection_details = PaymentCollection.create!(
                                                      reference_number: @voucher.reference_number,
                                                      particular: "To record time deposit-auto-renew",
                                                      prepared_by: @user.full_name,
                                                      status: "approved",
                                                      payment_type: "time_deposit",
                                                      book: "JVB",
                                                      total_time_deposit_amount: total_time_deposit,
                                                      branch_id: @branch,
                                                      paid_at: @c_working_date
                
                                                      )
      @deposit_time_transaction.each do |dtt|
        
          savings_account_transaction =  SavingsAccountTransaction.create!(
                        
                          savings_account_id: dtt.savings_account_id, 
                          amount: dtt.time_deposit_interest_amount, 
                          transaction_type: "interest", 
                          transacted_at: @c_working_date , 
                          particular: @voucher.particular ,
                          status: "approved", 
                          voucher_reference_number: @voucher.reference_number, 
                          transaction_date: @voucher.date_posted, 
                          savings_type_id: 4,
                          for_lock_in: dtt.id
                          )

              

            dt_data = DepositTimeTransaction.find(dtt.id).update(status: "deposit")

            total_lock_in = dtt.time_deposit_interest_amount + dtt.amount
        
            
            end_date = dtt.end_date.to_date + dtt.lock_in_period.to_i.day

            deposit_transaction = ::PaymentCollections::AddMemberToTimeDepositPaymentCollection.new(
                            member_account_id: dtt.savings_account_id, 
                            lock_in_amout: total_lock_in,
                            start_date: dtt.end_date,
                            lock_in_period: dtt.lock_in_period,
                            payment_ids: @payment_collection_details.id,
                            end_date: end_date

                            
                            ).execute!
                            #raise deposit_transaction.id.inspect

            
            savings_account_lock_in = SavingsAccount.find(dtt.savings_account_id).update(lock_in_amount: total_lock_in)
      
      end

      
      payment_collection_approved = PaymentCollection.find(@payment_collection_details.id).update!(status: "approved")

      lock_in_status = DepositTimeTransaction.where(payment_collection_id: @payment_collection_details.id)

      lock_in_status.each do |ls|
        DepositTimeTransaction.find(ls.id).update!(status: "lock-in")
      end


    end
  end
end
