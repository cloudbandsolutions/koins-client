module PaymentCollections
  class CreatePaymentCollectionForWithdrawals
    attr_accessor :branch, :paid_at, :prepared_by, :payment_collection

    def initialize(branch:, paid_at:, prepared_by:, members:, for_resignation:)
      @branch           = branch
      @paid_at          = paid_at
      @prepared_by      = prepared_by
      @savings_types    = SavingsType.all
      @insurance_types  = InsuranceType.all
      #@equity_types     = EquityType.all
      @members          = members
      @particular       = "Withdrawal of Funds #{branch}"
      @for_resignation  = for_resignation
      @payment_collection = PaymentCollection.new(
                              branch: @branch,
                              particular: @particular,
                              or_number: "WT-#{Time.now.to_i}",
                              prepared_by: @prepared_by,
                              paid_at: @paid_at,
                              payment_type: "withdraw",
                              for_resignation: @for_resignation
                            )
    end

    def execute!
      build_payment_collection_records
      @payment_collection
    end

    private

    def build_payment_collection_records
      @members.each do |member|
        payment_collection_record = PaymentCollectionRecord.new(
                                      member: member,
                                    )

        # Transactions for savings
        @savings_types.each do |savings_type|
          amount = 0.00
          collection_transaction =  CollectionTransaction.new(
                                      amount: amount,
                                      account_type_code: savings_type.code,
                                      account_type: "SAVINGS"
                                    )

          payment_collection_record.collection_transactions << collection_transaction
        end

        # Transactions for insurance
        @insurance_types.each do |insurance_type|
          amount = 0.00
          collection_transaction =  CollectionTransaction.new(
                                      amount: amount,
                                      account_type_code: insurance_type.code,
                                      account_type: "INSURANCE"
                                    )

          payment_collection_record.collection_transactions << collection_transaction
        end

        # Transactions for equity
#        @equity_types.each do |equity_type|
#          collection_transaction =  CollectionTransaction.new(
#                                      amount: 0.00,
#                                      account_type_code: equity_type.code,
#                                      account_type: "EQUITY"
#                                    )
#          
#          payment_collection_record.collection_transactions << collection_transaction
#        end
#
#        @payment_collection.payment_collection_records << payment_collection_record
      end
    end
  end
end
