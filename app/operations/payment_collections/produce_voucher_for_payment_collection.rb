module PaymentCollections
  class ProduceVoucherForPaymentCollection
    attr_accessor :payment_collection, :voucher

    require 'application_helper'

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @particular         = build_particular
      @book               = 'CRB'
      @c_working_date     = ApplicationHelper.current_working_date

      if @payment_collection.special_report?
        @book = 'JVB'
      end

      if @payment_collection.pending?
        @voucher = Voucher.new(
                    status: "pending", 
                    branch: @payment_collection.branch, 
                    book: @book, 
                    date_prepared: @c_working_date,
                    prepared_by: @payment_collection.prepared_by, 
                    particular: @particular
                  )
      elsif @payment_collection.approved? or @payment_collection.reversed?
        @voucher = Voucher.where(reference_number: @payment_collection.reference_number, branch_id: @payment_collection.branch.id, book: @book).first
      else
        raise "Invalid payment collection"
      end

      @bank                   = @payment_collection.branch.bank
      @savings_types          = SavingsType.all
      @insurance_types        = InsuranceType.all
      @equity_types           = EquityType.all
      @default_savings_type   = @savings_types.where(is_default: true).first
      @total_default_savings  = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), account_type: "SAVINGS", account_type_code: @default_savings_type.code).sum(:amount)
      @total_wp               = @payment_collection.total_wp_amount
    end

    def execute!
      if @payment_collection.pending?
        build_entries
      end
      @voucher.or_number = @payment_collection.or_number
      @voucher
    end

    private

    def build_particular
      center = @payment_collection.center

      if @payment_collection.particular.blank?
        particular = "Payment of Loan/Deposit of Funds - Center Name #{center.name}, DS Date #{@payment_collection.paid_at} DS Amount #{@payment_collection.total_cp_amount}"

        if @payment_collection.total_wp_amount > 0
          particular << ", WP Amount #{@payment_collection.total_wp_amount}"
        end
      else
        particular = @payment_collection.particular
      end

      particular
    end

    def build_entries
      build_debit_entries
      build_credit_entries
    end

    def build_debit_entries
      # DR Cash in Bank
      ac  = @bank.accounting_code

      if @payment_collection.debit_accounting_code.present?
        ac = @payment_collection.debit_accounting_code
      end

      journal_entry = JournalEntry.new(
                        amount: @payment_collection.total_cp_amount,
                        post_type: 'DR',
                        accounting_code: ac
                      )

      @voucher.journal_entries << journal_entry

      if @total_wp > @total_default_savings
        diff = (@total_wp - @total_default_savings).to_f
        journal_entry = JournalEntry.new(
                          amount: diff,
                          post_type: 'DR',
                          accounting_code: @default_savings_type.deposit_accounting_code
                        )

        @voucher.journal_entries << journal_entry
      end
    end

    def build_credit_entries
      build_loan_product_credit_entries
      build_savings_credit_entries
      build_insurance_credit_entries
      build_equity_credit_entries
    end

    def build_loan_product_credit_entries
      loan_products = LoanProduct.where(id: @payment_collection.payment_collection_records.pluck(:loan_product_id).uniq)

      # Loans Receivable per product
      loan_products.each do |loan_product|
        lp_principal_amount = 0.00
        lp_interest_amount = 0.00
        @payment_collection.payment_collection_records.where(loan_product_id: loan_product.id).each do |pc_record|
          pc_record.collection_transactions.each do |collection_transaction|
            if collection_transaction.account_type == "LOAN_PAYMENT"
              loan_payment = collection_transaction.loan_payment 
              lp_principal_amount += loan_payment.payment_stats[:principal]
              lp_interest_amount += loan_payment.payment_stats[:interest]
            end
          end
        end

        journal_entry = JournalEntry.new(
                          amount: lp_principal_amount,
                          post_type: 'CR',
                          accounting_code: loan_product.accounting_code_transaction
                        )
        @voucher.journal_entries << journal_entry

        journal_entry = JournalEntry.new(
                          amount: lp_interest_amount,
                          post_type: 'CR',
                          accounting_code: loan_product.interest_accounting_code
                        )
        @voucher.journal_entries << journal_entry
      end
    end

    def build_savings_credit_entries
      SavingsType.all.each do |savings_type|
        accounting_code = savings_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), 
                                              account_type: "SAVINGS", 
                                              account_type_code: savings_type.code)
                                              .sum(:amount)

        if savings_type.is_default == true and @payment_collection.total_wp_amount > 0
          amount -= @payment_collection.total_wp_amount
        end

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end

    def build_insurance_credit_entries
      @insurance_types.each do |insurance_type|
        accounting_code = insurance_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "INSURANCE",
                                              account_type_code: insurance_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end

    def build_equity_credit_entries
      @equity_types.each do |equity_type|
        accounting_code = equity_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "EQUITY",
                                              account_type_code: equity_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end
  end
end
