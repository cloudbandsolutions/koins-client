module PaymentCollections
  class ApprovePaymentCollectionTimeDeposit
    def initialize(payment_collection:, user: )
      @payment_collection_id = payment_collection
    
      
      @payment_collection = PaymentCollection.find(@payment_collection_id)
      
      @deposit_time_transaction =  DepositTimeTransaction.where(payment_collection_id: @payment_collection_id)
      @user = user
      @c_working_date = ApplicationHelper.current_working_date
      @voucher = ::PaymentCollections::ProduceAccountingEntryForTimeDepositCollection.new(payment_collection_id: @payment_collection_id).execute!

    end
    def execute!
      @voucher.save!
      @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
      @payment_collection.update!(
                                    status: "approved",
                                    paid_at: @voucher.date_posted,
                                    reference_number: @voucher.reference_number
                                )

      @deposit_time_transaction.each do |dtt|


          DepositTimeTransaction.find(dtt.id).update(status: "lock-in")
          
          savings_account_transaction =  SavingsAccountTransaction.create!(
                          savings_account_id: dtt.savings_account_id, 
                          amount: dtt.amount, 
                          transaction_type: "deposit", 
                          transacted_at: @c_working_date , 
                          particular: @voucher.particular ,
                          status: "approved", 
                          voucher_reference_number: @voucher.reference_number, 
                          transaction_date: @voucher.date_posted, 
                          savings_type_id: 4 ,
                          for_lock_in: dtt.id
                          )
            
            
          
          

          savings_lock_in = ((SavingsAccount.find(dtt.savings_account_id).lock_in_amount).to_f + dtt.amount)
          total_savings = ((SavingsAccount.find(dtt.savings_account_id).balance).to_f + dtt.amount)
        
          SavingsAccount.find(dtt.savings_account_id).update(lock_in_amount: savings_lock_in, balance: total_savings )
      end





    

    end
  end
end
