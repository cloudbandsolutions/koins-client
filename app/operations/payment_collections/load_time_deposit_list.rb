module PaymentCollections
  class LoadTimeDepositList
    def initialize(payment_id:)
      @payment_id = payment_id
      @data = {}
      @data[:list_of_member] = []
      @member_query = DepositTimeTransaction.where(payment_collection_id: @payment_id)
    end

    def execute!
      @member_query.each_with_index do |mq, i|
        tmp = {}
        tmp[:count]           = i + 1
        tmp[:member_name]     = Member.find(SavingsAccount.find(mq[:savings_account_id]).member_id).full_name
        tmp[:amount]          = mq[:amount]
        tmp[:lock_in_period]  = mq[:lock_in_period]
        tmp[:start_date]      = mq[:start_date]
        @data[:list_of_member] << tmp
      end
      
      @data


    end
  end
end
