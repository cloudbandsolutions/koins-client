module PaymentCollections
 class GenerateAccountingEntryTimeDepositInterest

  def initialize(user:)
    
    @c_working_date = ApplicationHelper.current_working_date
    @deposit_time_transaction = DepositTimeTransaction.where("status = ? and end_date <= ?","lock-in", @c_working_date)
    #@deposit_time_transaction = DepositTimeTransaction.all
    @user = user
    @book = "JVB"
    @branch = Branch.where(id: Settings.branch_ids).first
    #@c_working_date = working_date
    @voucher = Voucher.new(
                      status: "pending",
                      particular: "time deposit interest",
                      branch: @branch,
                      book: @book,
                      date_prepared: @c_working_date,
                      prepared_by: @user.to_s
              )


  end
  def execute!
    build_debit_entries
    build_credit_entries
    @voucher
  end

  def build_debit_entries
    #accounting_code_debit = AccountingCode.find(429)
    accounting_code_debit = AccountingCode.find(301)
    debit_amount = @deposit_time_transaction.sum(:time_deposit_interest_amount)
    journal_entry = JournalEntry.new(
                        amount: debit_amount,
                        post_type: "DR",
                        accounting_code: accounting_code_debit
                    )
    @voucher.journal_entries << journal_entry
  end

  def build_credit_entries
    #accounting_code_credit = AccountingCode.find(429) 
    accounting_code_credit = AccountingCode.find(SavingsType.find(5).deposit_accounting_code_id)
    credit_amount = @deposit_time_transaction.sum(:time_deposit_interest_amount)
    journal_entry = JournalEntry.new(
                        amount: credit_amount,
                        post_type: "CR",
                        accounting_code: accounting_code_credit
                    )
    @voucher.journal_entries << journal_entry
  end




 end
end
