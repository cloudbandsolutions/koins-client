module PaymentCollections
  class AddMemberToTimeDepositPaymentCollection
    
    def initialize(member_account_id:, lock_in_amout:, start_date:, lock_in_period:, payment_ids:, end_date: )
      
      @time_deposit = Settings.other_savings_type.first.term_map
      @lock_in_amout = lock_in_amout
      @lock_in_period = lock_in_period
      @start_date = start_date

      @deposit_time_transaction = DepositTimeTransaction.new
      @deposit_time_transaction.savings_account_id = member_account_id
      @deposit_time_transaction.amount = lock_in_amout
      @deposit_time_transaction.lock_in_period = lock_in_period
      @deposit_time_transaction.payment_collection_id = payment_ids
      @deposit_time_transaction.start_date = start_date

      @data = []
      @day_term = []

    end

    def execute!
    
      @time_deposit.each do |td|
        deposit_comp = {}
          
         if td.term == @lock_in_period.to_i
        
        

          #interest_rate_amount =   (@lock_in_amout.to_f * td.interest_rate).round(2).to_f
          #anual_interest_rate =  (@lock_in_period.to_f / 360.to_f).round(2).to_f
          #interest_amount = (interest_rate_amount * anual_interest_rate).round(2)
          interest_amount = (@lock_in_amout.to_f * td.interest_rate * @lock_in_period.to_f).round(2)

          @data = interest_amount 
          @day_term = td.days_term
          end
      
        
      end
      
  
      
 
      @deposit_time_transaction.end_date = @start_date.to_date + @day_term.to_i.day
      @deposit_time_transaction.time_deposit_interest_amount = @data

      @deposit_time_transaction.save!
    end



  end
end
