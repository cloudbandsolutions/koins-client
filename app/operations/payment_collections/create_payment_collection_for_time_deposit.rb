module PaymentCollections
  class CreatePaymentCollectionForTimeDeposit
    
    def initialize(total_amount:, branch:, date_of_payment:,  payment_type:, book:, prepared_by:, particular: )
      @payment_collection               = PaymentCollection.new
      @payment_collection.branch_id     = branch
      @payment_collection.paid_at       = date_of_payment
      @payment_collection.payment_type  = payment_type
      @payment_collection.book          = book
      @payment_collection.prepared_by   = prepared_by
      @payment_collection.particular    = particular
      @payment_collection.total_time_deposit_amount    = 0.0

    end

    def execute!
      @payment_collection.save!
      @payment_collection
    
    end

  end
end
