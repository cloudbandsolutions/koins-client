module PaymentCollections
  class GetTotalInsuranceDepositsFromBilling
    def initialize(payment_collection:)
      @payment_collection         = payment_collection
      @payment_collection_records = payment_collection.payment_collection_records
      @collection_transactions    = CollectionTransaction.where(payment_collection_record_id: @payment_collection_records.pluck(:id))
    end

    def execute!
      total = @collection_transactions.where(account_type: "INSURANCE").sum(:amount)

      total
    end
  end
end
