module PaymentCollections
  class ProduceVoucherForWithdrawalPaymentCollection
    attr_accessor :payment_collection, :voucher

    require 'application_helper'

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @particular         = build_particular
      @c_working_date     = ApplicationHelper.current_working_date
      @book               = 'JVB'
      @accounting_fund    = @payment_collection.accounting_fund

      if @payment_collection.book.present?
        @book = @payment_collection.book
      end

      if @payment_collection.pending?
        @voucher = Voucher.new(
                    status: "pending", 
                    branch: @payment_collection.branch, 
                    book: @book, 
                    date_prepared: @c_working_date,
                    prepared_by: @payment_collection.prepared_by, 
                    particular: @particular,
                    check_number: @payment_collection.check_number,
                    check_voucher_number: @payment_collection.check_voucher_number,
                    date_of_check: @payment_collection.date_of_check,
                    accounting_fund: @accounting_fund
                  )
      elsif @payment_collection.approved? or @payment_collection.reversed?
        @voucher = Voucher.where(reference_number: @payment_collection.reference_number, branch_id: @payment_collection.branch.id, book: "JVB").first
      else
        raise "Invalid payment collection"
      end

      @bank             = @payment_collection.branch.bank
      @savings_types    = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types     = EquityType.all
    end

    def execute!
      if @payment_collection.pending?
        build_entries
      end

      @voucher
    end

    private

    def build_particular
      particular = "Withdrawal of Funds - Branch: #{@payment_collection.branch} DS Date #{@payment_collection.paid_at} DS Amount #{@payment_collection.total_cp_amount}"

      if !@payment_collection.particular.nil?
        particular = @payment_collection.particular
      end

      particular
    end

    def build_entries
      build_savings_debit_entries
      build_insurance_debit_entries
      build_equity_debit_entries
    end

    def build_savings_debit_entries
      @savings_types.each do |savings_type|
        accounting_code = savings_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id), 
                                              account_type: "SAVINGS", 
                                              account_type_code: savings_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'DR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry

          cr_accounting_code = savings_type.withdraw_accounting_code

          if @payment_collection.use_withdrawal_accounting_code
            cr_accounting_code = @payment_collection.use_withdrawal_accounting_code
          end

          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: cr_accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end

    def build_insurance_debit_entries
      @insurance_types.each do |insurance_type|
        accounting_code = insurance_type.withdraw_accounting_code
        dr_accounting_code = insurance_type.deposit_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "INSURANCE",
                                              account_type_code: insurance_type.code)
                                              .sum(:amount)
        
        if @payment_collection.debit_accounting_code
          dr_accounting_code = @payment_collection.debit_accounting_code
        end
                                              
        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'DR',
                            accounting_code: dr_accounting_code
                          )

          @voucher.journal_entries << journal_entry

        if @payment_collection.credit_accounting_code
          accounting_code = @payment_collection.credit_accounting_code
        end

          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end

    def build_equity_debit_entries
      @equity_types.each do |equity_type|
        accounting_code = equity_type.withdraw_accounting_code
        amount = CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id),
                                              account_type: "EQUITY",
                                              account_type_code: equity_type.code)
                                              .sum(:amount)

        if amount > 0
          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'DR',
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries << journal_entry

          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: 'CR',
                            accounting_code: equity_type.deposit_accounting_code
                          )

          @voucher.journal_entries << journal_entry
        end
      end
    end
  end
end
