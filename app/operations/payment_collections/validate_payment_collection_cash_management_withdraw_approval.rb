module PaymentCollections
  class ValidatePaymentCollectionCashManagementWithdrawApproval
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @savings_types = SavingsType.all
      @insurance_types = InsuranceType.all
      @equity_types = EquityType.all
      @errors = []
    end

    def execute!
      check_for_pending!
      check_for_amount!
      #check_parameters!
      check_savings_withdrawables!
      check_insurance_withdrawables!
      check_equity_withdrawables!
      @errors
    end

    private

    def check_parameters!
      if !@payment_collection.check_number.present?
        @errors << "Check number required"
      end

      if !@payment_collection.check_voucher_number.present?
        @errors << "Check voucher number required"
      end

      if !@payment_collection.date_of_check.present?
        @errors << "Date of check required"
      end

      if !@payment_collection.check_name.present?
        @errors << "Check name required"
      end
    end

    def check_for_pending!
      if !@payment_collection.pending? 
        @errors << "This record is not pending"
      end
    end

    def check_for_amount!
      if @payment_collection.total_amount == 0
        @errors << "This record has no amount"
      end
    end

    def check_savings_withdrawables!
      collection_transactions = CollectionTransaction.where(
                                  account_type: "SAVINGS",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))

      collection_transactions.each do |ct|
        amount          = ct.amount
        member          = ct.payment_collection_record.member
        savings_account = SavingsAccount.where(member_id: member.id, savings_type_id: SavingsType.where(code: ct.account_type_code).first.id).first
        
        result = savings_account.balance - amount
        if result < savings_account.maintaining_balance
          @errors << "Cannot withdraw #{amount} for #{savings_account.savings_type.code} savings account of #{savings_account.member.full_name}. Maintaining balance: #{savings_account.maintaining_balance}"
        end
      end
    end

    def check_insurance_withdrawables!
      collection_transactions = CollectionTransaction.where(
                                  account_type: "INSURANCE",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))

      collection_transactions.each do |ct|
        amount            = ct.amount
        member            = ct.payment_collection_record.member
        insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: InsuranceType.where(code: ct.account_type_code).first.id).first
        if (insurance_account.balance - amount) < 0
          @errors << "Cannot withdraw #{amount} for #{insurance_account.insurance_type.code} insurance account of #{insurance_account.member.full_name}. Current balance: #{insurance_account.balance}"
        end
      end
    end

    def check_equity_withdrawables!
      collection_transactions = CollectionTransaction.where(
                                  account_type: "EQUITY",
                                  id: CollectionTransaction.where(payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)))

      collection_transactions.each do |ct|
        amount            = ct.amount
        member            = ct.payment_collection_record.member

        if amount > 0
          if member.loans.active.count > 0
            @errors << "Cannot withdraw equity amount of #{amount} from member #{member.full_name}. Member still has active loans"
          end

          if member.savings_accounts.sum(:balance) > 0
            @errors << "Cannot withdraw equity amount of #{amount} from member #{member.full_name}. Member still has savings"
          end

          equity_account = EquityAccount.where(member_id: member.id, equity_type_id: EquityType.where(code: ct.account_type_code).first.id).first
          if (equity_account.balance - amount) < 0
            @errors << "Cannot withdraw #{amount} for #{equity_account.equity_type.code} equity account of #{equity_account.member.full_name}. Current blaance: #{equity_account.balance}"
          end
        end
      end
    end
  end
end
