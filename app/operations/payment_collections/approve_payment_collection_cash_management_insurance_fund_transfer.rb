module PaymentCollections
  class ApprovePaymentCollectionCashManagementInsuranceFundTransfer
    attr_accessor :payment_collection, :errors

    require 'application_helper'

    def initialize(payment_collection:, user:)
      @payment_collection = payment_collection
      @user               = user
      @c_working_date     = ApplicationHelper.current_working_date
    end

    def execute!
      @payment_collection.update!(
        status: "approved", 
        reference_number: "#{Time.now.to_i}",
        paid_at: @c_working_date,
        updated_at: @c_working_date,
        approved_by: @user
      )

      create_insurance_deposits!
      @payment_collection
    end

    private

    def create_insurance_deposits!
      CollectionTransaction.where(account_type: "INSURANCE_FUND_TRANSFER", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)).each do |collection_transaction|
        if collection_transaction.amount > 0
          member            = collection_transaction.payment_collection_record.member
          insurance_type    = InsuranceType.where(code: collection_transaction.account_type_code).first
          insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).first
          
          if insurance_account.insurance_type_id == 1
            equity_amount = collection_transaction.amount.to_f / 2
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                              status: "pending",
                                              amount: collection_transaction.amount,
                                              transacted_at: @payment_collection.paid_at,
                                              created_at: @payment_collection.paid_at,
                                              particular: @payment_collection.particular,
                                              transaction_type: "fund_transfer_deposit",
                                              voucher_reference_number: @payment_collection.reference_number,
                                              insurance_account: insurance_account,
                                              equity_value: equity_amount + insurance_account.equity_value,
                                              is_adjustment: true
                                            )
            insurance_account_transaction.approve!(@user.full_name)
            insurance_account.update!(equity_value: insurance_account_transaction.ending_balance.to_f / 2)
          else
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                              status: "pending",
                                              amount: collection_transaction.amount,
                                              transacted_at: @payment_collection.paid_at,
                                              created_at: @payment_collection.paid_at,
                                              particular: @payment_collection.particular,
                                              transaction_type: "fund_transfer_deposit",
                                              voucher_reference_number: @payment_collection.reference_number,
                                              insurance_account: insurance_account,
                                              is_adjustment: true
                                            )
            insurance_account_transaction.approve!(@user.full_name)
          end
        end
      end
    end
  end
end
