module PaymentCollections
  class ValidateInsuranceWithdrawalFromCsvFile
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:, branch:, paid_at:)
      @payment_collection = payment_collection
      @paid_at = paid_at
      @branch = branch
      @errors = []
    end

    def execute!
      check_if_identification_number_present!
      check_if_identification_number_valid!
      check_if_insurance_types_present!
      check_if_branch_present!
      check_if_paid_at_present!   
      @errors
    end

    private

    def check_if_identification_number_valid!
      member = Member.where(identification_number: @payment_collection['identification_number']).first
      if member.nil?
        @errors << "No Member with Identification Number: #{@payment_collection['identification_number']}. "
      end
    end

    def check_if_identification_number_present!
      if @payment_collection['identification_number'].nil?
        @errors << "ID can't be blank. "
      end
    end

    def check_if_insurance_types_present!
      if @payment_collection['RF'].nil?
        @errors << "Retirement Fund can't be blank for #{@payment_collection['Member']}. "
      end

      if @payment_collection['LIF'].nil?
        @errors << "Life Insurance Fund can't be blank for #{@payment_collection['Member']}. "
      end  
    end

    def check_if_branch_present!
      if @branch.nil?
        @errors << "Branch cant be blank. "
      end
    end

    def check_if_paid_at_present!
      if @paid_at.nil?
        @errors << "Date of payment cant be blank. "
      end
    end
  end
end