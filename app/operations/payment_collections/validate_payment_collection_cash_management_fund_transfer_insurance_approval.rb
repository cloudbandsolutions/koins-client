module PaymentCollections
  class ValidatePaymentCollectionCashManagementFundTransferInsuranceApproval
    attr_accessor :payment_collection, :errors

    def initialize(payment_collection:)
      @payment_collection = payment_collection
      @insurance_types = InsuranceType.all
      @errors = []
    end

    def execute!
      check_for_members!
      check_for_pending!
      check_for_amount!
      check_for_valid_transactions!
      check_resigned_members!
      @errors
    end

    private

    def check_for_valid_transactions!
      CollectionTransaction.where(account_type: "INSURANCE_FUND_TRANSFER", payment_collection_record_id: @payment_collection.payment_collection_records.pluck(:id)).each do |collection_transaction|
        if collection_transaction.amount > 0
          member            = collection_transaction.payment_collection_record.member
          insurance_type    = InsuranceType.where(code: collection_transaction.account_type_code).first

          insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).first
          insurance_account_transaction = InsuranceAccountTransaction.new(
                                            amount: collection_transaction.amount,
                                            transacted_at: @payment_collection.paid_at,
                                            particular: @payment_collection.particular,
                                            transaction_type: "fund_transfer_deposit",
                                            voucher_reference_number: @payment_collection.reference_number,
                                            insurance_account: insurance_account
                                          )
          if !insurance_account_transaction.valid?
            insurance_account_transaction.errors.messages.each do |m|
              @errors << "Error for #{member.to_s}: #{m}"
            end
          end
        end
      end
    end

    def check_resigned_members!
      @payment_collection.payment_collection_records.each do |pcr|
        member = pcr.member
        if member.status == "resigned"
          @errors << "#{member.to_s} is already resigned"
        end
      end  
    end

    def check_for_members!
      if @payment_collection.payment_collection_records.count == 0
        @errors << "This record does not have any entries"
      end
    end

    def check_for_pending!
      if !@payment_collection.pending? 
        @errors << "This record is not pending"
      end
    end

    def check_for_amount!
      if @payment_collection.total_amount == 0
        @errors << "This record has no amount"
      end
    end
  end
end
