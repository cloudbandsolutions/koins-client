module Insurance
  class BulkRehash


    def initialize(config:)
      @config = config
      @branch = @config[:branch]
    end

    def execute!
      query!

      sets_array  = []

      member_account_sets = []

      @result.group_by{ |tx| tx["id"] }.each do |id, txs|
        balance = 0.00
        beginning_balance = 0.00
        ending_balance = 0.00

        temp_sets = txs.map.with_index{ |t, i|
                      updated_at      = Time.now + i.second
                      transaction_id  = t.fetch("transaction_id")

                      if t.fetch("transaction_type") == "deposit" or t.fetch("transaction_type") == 'fund_transfer_deposit' or t.fetch("transaction_type") == 'reverse_withdraw' or t.fetch("transaction_type") == 'interest'
                        ending_balance  = (beginning_balance + t.fetch("amount").to_f.round(2)).to_f.round(2)
                      elsif t.fetch("transaction_type") == "withdraw" or t.fetch("transaction_type") == 'reversed' or t.fetch("transaction_type") == 'fund_transfer_withdraw' or t.fetch("transaction_type") == 'reverse_deposit'
                        ending_balance  = (beginning_balance - t.fetch("amount").to_f.round(2)).to_f.round(2)
                      end

                      beginning_balance    = t.fetch("beginning_balance")
                      ending_balance       = t.fetch("ending_balance")

                      beginning_balance = ending_balance

                      "('#{transaction_id}','#{beginning_balance}', '#{ending_balance}', '#{updated_at.to_s(:db)}')"
                    }.join(",")

                    

        temp_sets.split(",").each do |o|
          sets_array << o
        end

        member_account_sets << "('#{id}', '#{ending_balance}')"
      end

      sets                = sets_array.join(",")
      member_account_sets = member_account_sets.join(",")


      if sets.present?
        query = "
          UPDATE insurance_account_transactions AS a SET
            beginning_balance = temp.beginning_balance::float,
            ending_balance = temp.ending_balance::float,
            updated_at = temp.updated_at::timestamp
          FROM (values
            #{sets}
          ) AS temp(transaction_id, beginning_balance, ending_balance, updated_at)
          WHERE temp.transaction_id = a.id::text
        "

        #binding.pry
        ActiveRecord::Base.connection.execute(query)

        # Update member accounts
        query = "
          UPDATE insurance_accounts AS m SET
            balance = temp.ending_balance::float
          FROM (values
            #{member_account_sets}
          ) AS temp(id, ending_balance)
          WHERE temp.id = m.id::text
        "
        ActiveRecord::Base.connection.execute(query)
      end
    end

    def query!
      @result = ActiveRecord::Base.connection.execute(<<-EOS).to_a
                  SELECT
                    insurance_accounts.id,
                    insurance_accounts.insurance_type_id,
                    insurance_account_transactions.id AS transaction_id,
                    insurance_account_transactions.beginning_balance::float AS beginning_balance,
                    insurance_account_transactions.amount,
                    insurance_account_transactions.ending_balance::float AS ending_balance,
                    insurance_account_transactions.transaction_type
                  FROM
                    insurance_accounts
                  INNER JOIN
                    insurance_account_transactions ON insurance_account_transactions.insurance_account_id = insurance_accounts.id AND insurance_account_transactions.status = 'approved'
                  WHERE
                    insurance_accounts.branch_id = '#{@branch.id}'
                  ORDER BY
                    insurance_accounts.id, insurance_accounts.insurance_type_id, insurance_account_transactions.transacted_at ASC, insurance_account_transactions.updated_at ASC
                EOS
    end
  end
end