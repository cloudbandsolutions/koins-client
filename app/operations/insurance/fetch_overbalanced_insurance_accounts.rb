module Insurance
  class FetchOverbalancedInsuranceAccounts
    def initialize
      @members        = Member.active.order("last_name ASC")
      @data           = {}
      @data[:members] = []
    end

    def execute!
      @members.each do |member|
        priority_type   = InsuranceType.where(code: Settings.insurance_type_priority).first
        secondary_type  = InsuranceType.where(code: Settings.insurance_type_secondary).first

        if priority_type and secondary_type
          priority_account  = InsuranceAccount.where(
                                member_id: member.id, insurance_type_id: priority_type.id
                              ).first
          secondary_account = InsuranceAccount.where(
                                member_id: member.id, insurance_type_id: secondary_type.id
                              ).first

          priority_balance  = priority_account.balance
          secondary_balance = secondary_account.balance

          if secondary_balance > priority_balance
            @data[:members] << {
              member: { id: member.id, name: member.full_name },
              priority_account: priority_account,
              secondary_account: secondary_account
            }
          end
        end
      end

      @data
    end
  end
end
