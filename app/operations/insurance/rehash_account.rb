module Insurance
  class RehashAccount
    def initialize(insurance_account:)
      @insurance_account              = insurance_account
      @insurance_account_transactions = InsuranceAccountTransaction.where(
                                          "insurance_account_id = ? AND amount > 0 AND status IN (?)", 
                                          @insurance_account.id, ["approved", "reversed"]
                                        ).order("transacted_at ASC")
    end

    def execute!
      @insurance_account.update!(balance: 0.00)
      @insurance_account_transactions.each do |sat|
        sat.update!(
          updated_at: sat.transacted_at,
          beginning_balance: 0.00,
          ending_balance: 0.00,
          status: "pending"
        )
      end

      @insurance_account_transactions.each do |sat|
        sat.beginning_balance = InsuranceAccount.find(@insurance_account.id).balance
        
        if sat.transaction_type == "withdraw" or sat.transaction_type == 'reversed' or sat.transaction_type == 'fund_transfer_withdraw' or sat.transaction_type == 'reverse_deposit'
          ending_balance = InsuranceAccount.find(@insurance_account.id).balance - sat.amount  
        elsif sat.transaction_type == "deposit" or sat.transaction_type == 'fund_transfer_deposit' or sat.transaction_type == 'reverse_withdraw' or sat.transaction_type == 'interest'
          ending_balance = InsuranceAccount.find(@insurance_account.id).balance + sat.amount
        end

        sat.ending_balance = ending_balance
        sat.approve!("")
      end

      @insurance_account
    end
  end
end


