module Insurance
  module Members
    class ValidateChangeOfRecognitionDate
      def initialize(member:, new_mii_recognition_date:)
        @member                   = member
        @new_mii_recognition_date = new_mii_recognition_date
        @errors                   = []
      end

      def execute!
        if !@new_mii_recognition_date.present?
          @errors << "New recognition date required #{@new_mii_recognition_date} wala"
        end

        @errors
      end
    end
  end
end
