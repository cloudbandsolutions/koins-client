module Insurance
  module Members
    class ValidateResign
      def initialize(member:, date_resigned:)
        @member             = member
        @date_resigned      = date_resigned
        @insurance_types    = InsuranceType.where(id: Settings.insurance_type_for_resign)
        @insurance_accounts = member.insurance_accounts.where(insurance_type_id: @insurance_types.pluck(:id))
        @errors             = []
      end

      def execute!
        @insurance_accounts.each do |insurance_account|
          if insurance_account.balance != 0
            @errors << "Inusrance Account #{insurance_account.id} still has balance. Please withdraw first"
          end
        end

        if !@insurance_types
          @errors << "No insurance types found"
        end

        if !@member.present?
          @errors << "Member not found"
        end

        if !@date_resigned.present?
          @errors << "Date resigned required"
        end

        @errors
      end
    end
  end
end
