module Insurance
  module Members
    class ChangeOfRecognitionDate
      def initialize(member:, new_mii_recognition_date:)
        @member                    =  member
        @new_mii_recognition_date  =  new_mii_recognition_date
      end

      def execute!
        @member.update!(
          previous_mii_member_since: @new_mii_recognition_date,
          )
      end
    end
  end
end
