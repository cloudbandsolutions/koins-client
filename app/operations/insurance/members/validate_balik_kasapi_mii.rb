module Insurance
  module Members
    class ValidateBalikKasapiMii
      def initialize(member:, new_recognition_date:)
        @member               = member
        @new_recognition_date = new_recognition_date
        @insurance_types    = InsuranceType.all
        @insurance_accounts = @member.insurance_accounts.where(insurance_type_id: @insurance_types.pluck(:id))
        @errors             = []
      end

      def execute!
        @insurance_accounts.each do |insurance_account|
          if insurance_account.balance > 0
            @errors << "Inusrance Account #{insurance_account.id} still has balance. Please withdraw first"
          end
        end

        # if !@insurance_types
        #   @errors << "No insurance types found"
        # end

        # if !@member.present?
        #   @errors << "Member not found"
        # end

        if !@new_recognition_date.present?
          @errors << "New recognition date required"
        end

        @errors
      end
    end
  end
end
