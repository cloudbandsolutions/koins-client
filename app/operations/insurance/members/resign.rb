module Insurance
  module Members
    class Resign
      def initialize(member:, date_resigned:, resigned_by:, reason:)
        @member         = member
        @date_resigned  = date_resigned
        @resigned_by    = resigned_by
        @reason         = reason 
      end

      def execute!
        @member.update!(
          status: "resigned", 
          date_resigned: @date_resigned,
          insurance_status: 'resigned',
          insurance_date_resigned: @date_resigned,
          resignation_reason: @reason,
          )
      end
    end
  end
end
