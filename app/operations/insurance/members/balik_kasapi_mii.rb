module Insurance
  module Members
    class BalikKasapiMii
      def initialize(member:, new_recognition_date:, balik_kasapi_by:)
        @member                =  member
        @old_recognition_date  =  member.previous_mii_member_since
        @new_recognition_date  =  new_recognition_date
        @balik_kasapi_by       =  balik_kasapi_by
      end

      def execute!
        @member.update!(
          status: "active", 
          old_previous_mii_member_since: @old_recognition_date,
          date_resigned: nil,
          resignation_reason: nil,
          insurance_status: 'dormant',
          insurance_date_resigned: nil,
          previous_mii_member_since: @new_recognition_date,
          is_balik_kasapi: true,
          )
      end
    end
  end
end
