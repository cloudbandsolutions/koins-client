module Insurance
  class FetchWithdrawalCollectionForHiip
    def execute!
      payment_collection_ids = PaymentCollection.withdraw.find_by_sql(<<-SQL)
        SELECT pc.id
        FROM payment_collections AS pc
        INNER JOIN vouchers AS v
          ON v.reference_number = pc.reference_number
          AND v.particular       = pc.particular
        INNER JOIN journal_entries AS je
          ON je.voucher_id = v.id
          AND je.accounting_code_id = 1906
        WHERE pc.status = 'approved'
          AND lower(pc.particular) LIKE '%hiip%'
        GROUP BY pc.id
      SQL

      PaymentCollection.where(id: payment_collection_ids)
    end
  end
end