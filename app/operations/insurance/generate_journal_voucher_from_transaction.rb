module Insurance
  class GenerateJournalVoucherFromTransaction
    def initialize(insurance_transaction:, prepared_by:, approved_by:)
    	@insurance_transaction = insurance_transaction
      @prepared_by = prepared_by
      @approved_by = approved_by
    end

    def execute!
      voucher = Voucher.new(
          particular: @insurance_transaction.particular,
          reference_number: @insurance_transaction.voucher_reference_number,
          status: 'approved',
          book: @insurance_transaction.transaction_type == "withdraw" ? 'JVB' : 'CRB',
          branch_id: @insurance_transaction.bank.id,
          date_prepared: @insurance_transaction.transacted_at,
          prepared_by: @prepared_by,
          approved_by: @approved_by
      )

      if @insurance_transaction.transaction_type == "withdraw"
        je_debit = JournalEntry.new(
            amount: @insurance_transaction.amount,
            post_type: "DR",
            accounting_code: @insurance_transaction.insurance_account.insurance_type.withdraw_accounting_code
          )

        voucher.journal_entries << je_debit

        je_credit = JournalEntry.new(
            amount: @insurance_transaction.amount,
            post_type: "CR",
            accounting_code: @insurance_transaction.accounting_code
          )

        voucher.journal_entries << je_credit


      elsif @insurance_transaction.transaction_type == "deposit"
        je_debit = JournalEntry.new(
            amount: @insurance_transaction.amount,
            post_type: "DR",
            accounting_code: @insurance_transaction.accounting_code
          )

        voucher.journal_entries << je_debit

        je_credit = JournalEntry.new(
            amount: @insurance_transaction.amount,
            post_type: "CR",
            accounting_code: @insurance_transaction.insurance_account.insurance_type.deposit_accounting_code
          )

        voucher.journal_entries << je_credit
      else
      # TODO: ERROR
      raise "Error in Insurance Account Service"
      end

      voucher.save!
    end
  end
end