module Insurance
  class FetchInsuredLoans
    def initialize(start_date:, end_date:, loan_status:, branch_id:)
      @records  = []
      @start_date = start_date.to_date
      @end_date = end_date.to_date
      @branch_id = branch_id
      @loan_status = loan_status
      @loans = []

      if @end_date.present? && @start_date.present? && @loan_status.present? && @branch_id.present?
        if @loan_status == "paid"
          #@entry_level_loans  = Loan.where("maturity_date >= ? AND maturity_date <= ? AND status = ?", @start_date, @end_date, @loan_status).insured
          #Loan.where("extract(month from maturity_date) = ? AND extract(year from maturity_date) = ? AND extract(month from maturity_date) = ? AND extract(year from maturity_date) = ? AND status = ?", @start_date.month, @start_date.year, @end_date.month, @end_date.year, @loan_status).each do |loan|
          Loan.where("status = ? AND maturity_date >= ? AND maturity_date <= ? AND branch_id = ?", @loan_status, @start_date, @end_date, @branch_id).each do |loan|
            accounting_entry = loan.accounting_entry
            if !accounting_entry.nil?
              clip = accounting_entry.journal_entries.where(accounting_code_id: 99).first
              if !clip.nil?
                @loans << loan
              end
            end
          end

          @entry_level_loans  = @loans
        elsif @loan_status == "active"
          Loan.where("date_approved >= ? AND date_approved <= ? AND status = ? AND branch_id = ?", @start_date, @end_date, "active", @branch_id).each do |loan|
            accounting_entry = loan.accounting_entry
            if !accounting_entry.nil?
              clip = accounting_entry.journal_entries.where(accounting_code_id: 99).first
              if !clip.nil?
                @loans << loan
              end
            end
          end

          @entry_level_loans  = @loans
          #@entry_level_loans  = Loan.where("date_approved > ? AND date_approved < ? AND status = ? AND clip_number != ?", @start_date, @end_date, @loan_status, "").insured
          # @entry_level_loans  = Loan.where("extract(month from date_approved) = ? AND extract(year from date_approved) = ? AND status = ? AND clip_number != ?", @start_date.month, @start_date.year, @end_date.month, @end_date.year, @loan_status, "")
        end
      else
        # @entry_level_loans  = Loan.joins(:member).insured.order("members.last_name ASC")
        Loan.all.joins(:member).where("date_approved >= ? AND date_approved <= ?", @start_date, @end_date).each do |loan|
          accounting_entry = loan.accounting_entry
          if !accounting_entry.nil?
            clip = accounting_entry.journal_entries.where(accounting_code_id: 99).first
            if !clip.nil?
              @loans << loan
            end
          end
        end
        
        @entry_level_loans  = @loans
      end

     # @loan_insurance_accounting_code = AccountingCode.where(id: Settings.loan_insurance_accounting_code_id).first
    end

    def execute!
      @entry_level_loans.each do |loan|
        record = {}
        record[:loan]         = loan
        record[:pn_number]    = loan.pn_number
        record[:member]        = loan.member.full_name_titleize
        record[:identification_number] = loan.member.identification_number
        record[:first_name]     = loan.member.first_name
        record[:middle_name]     = loan.member.middle_name
        record[:last_name]     = loan.member.last_name
        record[:loan_product]   = loan.loan_product.to_s
        record[:first_date_of_payment]  = loan.first_date_of_payment.strftime("%m-%d-%Y")
        record[:uuid]  = loan.uuid
        record[:status] = loan.status
        record[:gender] = loan.member.gender
        record[:date_of_birth]  = loan.member.date_of_birth
        
        if loan.override_installment_interval == true
          record[:amount] = loan.original_loan_amount
          record[:num_installments] = loan.original_num_installments
          record[:maturity_date]  = loan.maturity_date.strftime("%m-%d-%Y")  
          record[:approximated_date_released] = (loan.maturity_date - (loan.original_num_installments).weeks).strftime("%B %d, %Y")
        else
          record[:amount] = loan.amount
          record[:num_installments] = loan.num_installments
          record[:maturity_date]  = loan.maturity_date.strftime("%m-%d-%Y")
          #record[:approximated_date_released] = (loan.maturity_date - (loan.num_installments).weeks).strftime("%B %d, %Y")
          record[:approximated_date_released] = loan.date_approved.strftime("%m-%d-%Y")
        end

        # record[:amount] = loan.amount
        # record[:num_installments] = loan.num_installments
        record[:term] = loan.term

        record[:insured_amount] = loan.accounting_entry.journal_entries.where(accounting_code_id: 99).first.try(:amount)

        # lde = loan.loan_product.loan_deduction_entries.where(accounting_code_id: 99).first
        # if lde.present?
        #   if lde.is_percent and !lde.use_term_map
        #     p = (lde.val / 100).to_f
        #     record[:insured_amount] = (p * record[:amount])
        #   elsif lde.is_percent and lde.use_term_map
        #     case record[:num_installments]
        #     when 15
        #       p = lde.t_val_15.to_f
        #     when 25
        #       p = lde.t_val_25.to_f
        #     when 35
        #       p = lde.t_val_35.to_f
        #     when 50
        #       p = lde.t_val_50.to_f
        #     else
        #       if record[:term] == "semi-monthly" || record[:term] == "monthly"
        #         #p = lde.t_val_15
        #         p = lde.t_val_50.to_f
        #       elsif 
        #         p = lde.t_val_25.to_f
        #       end
        #     end

        #     p = p / 100
            
        #     if record[:term] == "semi-monthly"
        #       mos = (record[:num_installments] / 2).to_f
        #       record[:insured_amount] = (p * record[:amount]) * (mos / 12).to_f
        #     else  
        #       record[:insured_amount] = (p * record[:amount])
        #     end
        #   elsif lde.is_percent == false and lde.use_term_map == false
        #     record[:insured_amount] = lde.val.to_f
        #   elsif lde.is_percent == false and lde.use_term_map == true
        #     case record[:num_installments]
        #     when 15
        #       p = lde.t_val_15.to_f
        #     when 16..25
        #       p = lde.t_val_25.to_f
        #     when 35
        #       p = lde.t_val_35.to_f
        #     when 50
        #       p = lde.t_val_50.to_f
        #     else
        #       #p = lde.t_val_25
        #       if record[:term] == "semi-monthly" || record[:term] == "monthly"
        #         p = lde.t_val_15
        #       elsif 
        #         p = lde.t_val_25.to_f
        #       end
        #     end

        #     record[:insured_amount] = p
        #   else
        #     raise "Invalid loan deduction entry"
        #   end
        # end

        #record[:insured_amount] = loan.insured_amount
        #loan_accounting_entry = loan.accounting_entry
        #if loan_accounting_entry.present?
        #  if @loan_insurance_accounting_code.present?
        #    record[:insured_amount] = loan_accounting_entry.journal_entries.where(accounting_code_id: @loan_insurance_accounting_code.id).first.try(:amount)
        #  else
        #    record[:insurance_amount] = "ERR: Accounting entry not found"
        #  end
        #else
        #  record[:insurance_amount] = "ERR: Accounting entry not found"
        #end

        # record[:maturity_date]  = loan.maturity_date.strftime("%B %d, %Y")  
        # record[:approximated_date_released] = (loan.maturity_date - (loan.num_installments + 1).weeks).strftime("%B %d, %Y")

        @records << record
      end

      @records
    end
  end
end