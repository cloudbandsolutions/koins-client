module Insurance
	class ReverseInsuranceAccountTransaction
		def initialize(insurance_account_transaction:)
			@insurance_account_transaction =  insurance_account_transaction	
		end

		def execute!
			transaction_type = ["withdraw", "wp", "fund_transfer_withdraw"].include?(@insurance_account_transaction.transaction_type) ? "reverse_withdraw" : "reverse_deposit"

                  beginning_balance = @insurance_account_transaction.insurance_account.balance
                  ending_balance = transaction_type == "reverse_withdraw" ? beginning_balance + @insurance_account_transaction.amount : beginning_balance - @insurance_account_transaction.amount

                  iat = InsuranceAccountTransaction.new(
                                                amount: @insurance_account_transaction.amount,
                                                transaction_type: transaction_type,
                                                voucher_reference_number: @insurance_account_transaction.voucher_reference_number,
                                                particular: "REVERSED ENTRY",
                                                insurance_account: @insurance_account_transaction.insurance_account,
                                                status: 'reversed',
                                                beginning_balance: beginning_balance,
                                                ending_balance: ending_balance
                                              )
                  iat.save!
                  iat.generate_updates!
		end
	end
end