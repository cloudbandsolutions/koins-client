module Insurance
  class FetchPaymentCollectionWithdrawalForHiip
    def initialize
      @payment_collections = []
      @records  = []

      PaymentCollection.withdraw.each do |payment_collection|
        accounting_entry = payment_collection.accounting_entry

        if accounting_entry.present?
          hiip = accounting_entry.journal_entries.where(accounting_code_id: 1906).first
        
          if hiip.present?
            @payment_collections << payment_collection
          end
        end
      end  
    end

    def execute!
      @payment_collections.each do |payment_collection|
        ae = payment_collection.accounting_entry

        payment_collection.payment_collection_records.each do |payment_collection_record|
          record = {}

          record[:payment_collection_uuid] = payment_collection.uuid
          record[:payment_type] = payment_collection.payment_type
          record[:or_number] = payment_collection.or_number
          record[:payment_collection_record_uuid] = payment_collection_record.uuid
          record[:member] = payment_collection_record.member.full_name
          record[:member_uuid] = payment_collection_record.member.uuid
          record[:amount] = payment_collection_record.total_cp_amount
          record[:accounting_entry_uuid] = ae.uuid
          record[:voucher_reference_number] = ae.reference_number
          record[:particular] = ae.particular
          record[:approved_by] = ae.approved_by
          record[:prepared_by] = ae.prepared_by
          record[:book] = ae.book
          record[:date_approved] = ae.date_posted
          record[:date_prepared] = ae.date_prepared
          record[:master_reference_number] = ae.master_reference_number
        
          @records << record
        end
      end

      @records
    end
  end
end