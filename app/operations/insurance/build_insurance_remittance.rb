module Insurance
	class BuildInsuranceRemittance
		def initialize(start_date:, end_date:, branch:, center:)
			@start_date = start_date 
			@end_date = end_date 
			@branch = branch 
			@center = center
		end

		def execute!
			insurance_remittance = InsuranceRemittance.new(or_number: "#{Time.now.to_i}", start_date: @start_date, end_date: @end_date, particular: "Insurance Remittance for #{@center.to_s} - #{@branch.to_s}")
	    insurance_account_transactions = InsuranceAccountTransaction.all
	    insurance_account_transactions.each do |iat|
	      insurance_remittance.insurance_remittance_records << InsuranceRemittanceRecord.new(member: iat.insurance_account.member, insurance_type: iat.insurance_account.insurance_type, uuid: iat.uuid, amount: iat.amount)
	    end

	    insurance_remittance.save!

	    insurance_remittance
		end
	end
end