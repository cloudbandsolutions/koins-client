module Insurance
  class GenerateInsuranceAccountStatus
    def initialize(insurance_account:)
      @insurance_account  = insurance_account
      @member             = @insurance_account.member
      @data               = {}
      @start_date         = @member.previous_mii_member_since.try(:to_date)
      @current_date       = ApplicationHelper.current_working_date.to_date

      if @start_date.blank?
        insurance_membership_type_name = Settings.insurance_membership_type_name
        @member.memberships.each do |membership|
          if membership[:membership_type][:name] == insurance_membership_type_name
            @start_date = membership[:membership_payment][:paid_at].try(:to_date)
          end
        end
      end

      @latest_payment   = @insurance_account.insurance_account_transactions.order("transacted_at ASC").last
      #@current_balance  = @latest_payment ? @latest_payment.ending_balance : 0.00
      @current_balance = @insurance_account.balance

      @default_periodic_payment = @insurance_account.insurance_type.default_periodic_payment

      # Check
      if @start_date.blank?
        raise "No start date for insurance account #{@insurance_account.id}"
      end
    end

    def execute!
      @num_days   = (@current_date - @start_date).to_i
      # @num_weeks  = (@num_days / 7).to_i
      @num_weeks  = (@num_days / 7).to_i + 1
      @insured_amount = @num_weeks * @default_periodic_payment
      @latest_transaction_date  = @latest_payment ? @latest_payment.transacted_at.to_date : @start_date

      @num_days_insured   = (@latest_transaction_date.to_date  - @start_date).to_i
      @num_weeks_insured  = (@num_days_insured / 7).to_i

      @data[:insurance_type]    = @insurance_account.insurance_type.code
      @data[:insurance_type_id] = @insurance_account.insurance_type.id
      @data[:start_date]        = @start_date.strftime("%B %d, %Y")
      @data[:length_of_membership]  = (@current_date - @start_date).to_i
      @data[:current_date]      = @current_date.strftime("%B %d, %Y")
      @data[:last_trans_date]   = @latest_transaction_date.strftime("%B %d, %Y")
      @data[:num_weeks]         = @num_weeks
      @data[:insured_amount]    = @num_weeks  * @default_periodic_payment
      @data[:periodic_payment]  = @default_periodic_payment
      @data[:current_balance]   = @current_balance
      @data[:status]            = nil
      @data[:coverage_date]     = (@start_date + ((@current_balance / @default_periodic_payment).to_i).weeks).strftime("%B %d, %Y")
      @data[:amt_past_due]      = (@current_balance - @data[:insured_amount]) * -1
      @data[:num_weeks_past_due]  = (@data[:amt_past_due] / @default_periodic_payment).to_i

      @days_lapsed = (@current_date - @latest_transaction_date).to_i

      if @days_lapsed <= 45 && @current_balance > @data[:insured_amount]
        @data[:status] = "advanced"
      elsif @days_lapsed >= 45 && @current_balance > @data[:insured_amount]
        @data[:status] = "advanced"
      elsif @days_lapsed > 45 && @current_balance < @data[:insured_amount]
        @data[:status]  = "lapsed"
      elsif @days_lapsed <= 45 && @current_balance < @data[:insured_amount] && @data[:amt_past_due] >= 97
        @data[:status]  = "lapsed"  
      elsif @days_lapsed <= 45 && @current_balance < @data[:insured_amount] && @data[:amt_past_due] < 97
        @data[:status]  = "past due"  
      else
        @data[:status] = "normal"
      end

      @data
    end
  end
end
