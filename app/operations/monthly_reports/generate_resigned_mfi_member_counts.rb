module MonthlyReports
  class GenerateResignedMfiMemberCounts
    def initialize(user:, branch:, month:, year:)
      @user           = user
      @branch         = branch
      @month          = month
      @year           = year
      @c_working_date = ApplicationHelper.current_working_date
      @monthly_report = MonthlyReport.new(
                          generated_by: user.full_name,
                          month: @month,
                          year: @year,
                          branch_code: @branch.code,
                          report_type: "resigned-mfi-member-count",
                          date_generated: @c_working_date
                        )

      @data           = {}
      @data[:count]   = 0
      @data[:members] = []

      @active_members = Member.active.where(
                          "extract(month from date_resigned) = ? AND
                           extract(year from date_resigned) = ? AND
                           branch_id = ?",
                          @month,
                          @year,
                          @branch.id
                        )
    end

    def execute!
      @data[:count] = @active_members.count

      @active_members.order("date_resigned ASC").each do |m|
        @data[:members] << {
          id: m.id,
          identification_number: m.identification_number,
          first_name: m.first_name,
          last_name: m.last_name,
          middle_name: m.middle_name,
          date_resigned: m.date_resigned
        }
      end

      @monthly_report.data = @data
      @monthly_report.save!

      @monthly_report
    end
  end
end
