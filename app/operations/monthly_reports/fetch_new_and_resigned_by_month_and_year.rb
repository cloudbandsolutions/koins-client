module MonthlyReports
  class FetchNewAndResignedByMonthAndYear
    def initialize(month:, year:)
      @month  = month
      @year   = year
      @new_members_monthly_report       = MonthlyReport.new_mfi_member_count.where(
                                            year: @year,
                                            month: @month,
                                          ).last

      @resigned_members_monthly_report  = MonthlyReport.resigned_mfi_member_count.where(
                                            year: @year,
                                            month: @month
                                          ).last

      @data = {}
    end

    def execute!
      @data[:new_members]       = @new_members_monthly_report.data["members"]
      @data[:resigned_members]  = @resigned_members_monthly_report.data["members"]

      @data
    end
  end
end
