module MonthlyReports
  class FetchMfiNewAndResignedMonthlyCounts
    def initialize(year:, branch:)
      @year   = year
      @branch = branch
      @months = (1..12)
      @data   = { year: @year, branch: @branch.code, records: [] }
    end

    def execute!
      @months.each do |month|
        d = {}
        d[:month] = Date::MONTHNAMES[month]
        new_members_monthly_report  = MonthlyReport.new_mfi_member_count.where(
                                        year: @year,
                                        month: month,
                                        branch_code: @branch.code
                                      ).last

        if new_members_monthly_report
          d[:new_count] = new_members_monthly_report['data']['count']
        else
          d[:new_count] = 0
        end

        resigned_members_monthly_report = MonthlyReport.resigned_mfi_member_count.where(
                                            year: @year,
                                            month: month,
                                            branch_code: @branch.code
                                          ).last

        if resigned_members_monthly_report
          d[:resigned_count] = resigned_members_monthly_report['data']['count']
        else
          d[:resigned_count] = 0
        end

        @data[:records] << d
      end

      @data
    end
  end
end
