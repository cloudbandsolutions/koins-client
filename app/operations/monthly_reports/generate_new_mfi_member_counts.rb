module MonthlyReports
  class GenerateNewMfiMemberCounts
    def initialize(user:, branch:, month:, year:)
      @user           = user
      @branch         = branch
      @month          = month
      @year           = year
      @c_working_date = ApplicationHelper.current_working_date
      @monthly_report = MonthlyReport.new(
                          generated_by: user.full_name,
                          month: @month,
                          year: @year,
                          branch_code: @branch.code,
                          report_type: "new-mfi-member-count",
                          date_generated: @c_working_date
                        )

      @data           = {}
      @data[:count]   = 0
      @data[:members] = []

      @active_members = Member.active.where(
                          "extract(month from previous_mfi_member_since) = ? AND
                           extract(year from previous_mfi_member_since) = ? AND
                           branch_id = ?",
                          @month,
                          @year,
                          @branch.id
                        )
      @membership_payments = MembershipPayment.paid.where(
                              "extract(month from paid_at) = ? AND
                               extract(year from paid_at) = ? AND membership_type_id = ?",
                              @month,
                              @year,
                              1
                            )
    end

    def execute!
      #@data[:count] = @active_members.count
      @data[:count] = @membership_payments.count
      #@data[:count] = 0

#      @membership_payments.each do |membership_payment|
#        member  = membership_payment.member
#
#        if member.date_resigned
#          #raise "here"
#        else
#          @data[:count] += 1
#        end
#      end

      @active_members.order("previous_mfi_member_since ASC").each do |m|
        @data[:members] << {
          id: m.id,
          identification_number: m.identification_number,
          first_name: m.first_name,
          last_name: m.last_name,
          middle_name: m.middle_name,
          membership_date: m.previous_mfi_member_since
        }
      end

      @monthly_report.data = @data
      @monthly_report.save!

      @monthly_report
    end
  end
end
