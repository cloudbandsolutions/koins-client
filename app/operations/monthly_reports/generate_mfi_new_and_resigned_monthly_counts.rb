module MonthlyReports
  class GenerateMfiNewAndResignedMonthlyCounts
    def initialize(year:, branch:, user:)
      @year   = year
      @branch = branch
      @user   = user
      @months = (1..12)
    end

    def execute!
      @months.each do |month|
        ::MonthlyReports::GenerateResignedMfiMemberCounts.new(
          user: @user,
          branch: @branch,
          month: month,
          year: @year
        ).execute!

        ::MonthlyReports::GenerateNewMfiMemberCounts.new(
          user: @user,
          branch: @branch,
          month: month,
          year: @year
        ).execute!
      end
    end
  end
end
