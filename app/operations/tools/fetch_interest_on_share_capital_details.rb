module Tools
  class FetchInterestOnShareCapitalDetails
    def initialize(share_capital_interest_id:)
     @data = {}
     @share_capital_interest_id = share_capital_interest_id
     ioscc_details = {}
     ioscc = InterestOnShareCapitalCollection.find(@share_capital_interest_id)
     @ioscc_details = ioscc.interest_on_share_capital_collection_records 

      @data[:iosccr_id] = @share_capital_interest_id
      @data[:total_interest] =  ioscc.total_equity_amount
      @data[:total_interest_share] = ioscc.total_interest_amount
      @data[:interest_rate] = ioscc.equity_rate   
      @data[:status] = ioscc.status
      @data[:savings_account_amount] = ioscc.status
      @data[:cbu_account_amount] = ioscc.status



     @member_details = []
     @data[:member_details_group] = []
    end

    def execute!
      @ioscc_details.each do |iodetails|
        member_status = Member.find(iodetails.data["member_id"]).status
      
        if member_status == "cleared" || member_status == "archived"
        tmp = {}
        tmp[:equity_details] = []
          tmp[:member_id] = iodetails.data['member_id']
          tmp[:member_name] = iodetails.data['member_name']
          tmp[:member_center] = Center.find(Member.find(iodetails.data['member_id']).center_id).name
          tmp[:member_status] = member_status
          tmp[:total_share] = iodetails.data['first_loop']
          tmp[:total_interest_amount] = iodetails.data['total_interest_amount']
          tmp[:total_savings_amount] = iodetails.data['member_savings_amount_distribute']
          tmp[:total_cbu_amount] = iodetails.data['member_cbu_amount_distribute']
        
          #iodetails.data["equitytrans"].each do |b|
          iodetails.data["equtiytrans"].each do |b|
            tmpdet = {}
            tmpdet[:month] = b["month"]
            tmpdet[:amount] = b["amount"]
            tmp[:equity_details] << tmpdet
          end

        @member_details << tmp
        end
      end

        member_details_group = @member_details.group_by { |x| x[:member_status]  }.sort_by { |t, _| t[0] }

      

        member_details_group.each do |dt, array|
          mdg                   = {}
          mdg[:center_name]     = dt
          mdg[:center_details]  = []
          center_total_share = 0
          center_total_ave_amount   = 0
          center_total_savings_amount   = 0
          center_total_cbu_amount   = 0
    
          array.each do |a|
            arraylist                         = {}
            arraylist[:equity_details]        = []
            
            arraylist[:member_id]             = a[:member_id]
            arraylist[:member_name]           = a[:member_name]
            arraylist[:member_status]         = a[:member_status]

            arraylist[:total_share]           = a[:total_share]
            arraylist[:total_average_share]   = (a[:total_interest_amount]).round(2)
            arraylist[:total_savings_amount]  = (a[:total_savings_amount]).round(2)
            arraylist[:total_cbu_amount]      = (a[:total_cbu_amount]).round(2)

        
            center_total_share          += (arraylist[:total_share]).round(2)
            center_total_ave_amount     += (arraylist[:total_average_share]).round(2)
            center_total_savings_amount += (arraylist[:total_savings_amount]).round(2)
            center_total_cbu_amount     += (arraylist[:total_cbu_amount]).round(2)

            a[:equity_details].each do |ed|
              edetails          = {}
              edetails[:month]  = ed[:month]
              edetails[:amount] = ed[:amount]

              arraylist[:equity_details] << edetails
            end
    

            mdg[:total_share]     = (center_total_share).round(2)
            mdg[:total_ave_details]     = (center_total_ave_amount).round(2)
            mdg[:total_savings_details] = (center_total_savings_amount).round(2)
            mdg[:total_cbu_details]     = (center_total_cbu_amount).round(2)

            mdg[:center_details] << arraylist

          end
            

          @data[:member_details_group] << mdg
        
        end
        
#          @current_user = User.where(id: 1).first
#          @data[:voucher] = ::Tools::GenerateAccountingEntryForTransferInterestOnShareCapital.new(interest_on_share_capital_id: 2, user: @current_user).execute!




      @data
    end
  end
end
