module Tools
  class RepairIcprMultipleDecimal
    def initialize(share_capital_interest_id:)
     @data = {}
     @share_capital_interest_id = share_capital_interest_id
     ioscc_details = {}
     ioscc = InterestOnShareCapitalCollection.find(@share_capital_interest_id)
     @ioscc_details = ioscc.interest_on_share_capital_collection_records 

      @data[:iosccr_id] = @share_capital_interest_id
      @data[:total_interest] =  ioscc.total_equity_amount
      @data[:total_interest_share] = ioscc.total_interest_amount
      @data[:interest_rate] = ioscc.equity_rate   
      @data[:status] = ioscc.status
      @data[:savings_account_amount] = ioscc.status
      @data[:cbu_account_amount] = ioscc.status



     @member_details = []
     @data[:member_details_group] = []
    end

    def execute!
      @ioscc_details.each do |iodetails|
        tmp = {}
        tmp[:equity_details] = []
        tmp[:member_id] = iodetails.data['member_id']
        tmp[:member_name] = iodetails.data['member_name']
        tmp[:member_center] = Center.find(Member.find(iodetails.data['member_id']).center_id).name
        tmp[:total_share] = iodetails.data['first_loop']
        tmp[:total_interest_amount] = iodetails.data['total_interest_amount']
        tmp[:total_savings_amount] = iodetails.data['member_savings_amount_distribute']
        tmp[:total_cbu_amount] = iodetails.data['member_cbu_amount_distribute']
        
        #iodetails.data["equitytrans"].each do |b|
        iodetails.data["equtiytrans"].each do |b|
          tmpdet = {}
          tmpdet[:month] = b["month"]
          tmpdet[:amount] = b["amount"]
          tmp[:equity_details] << tmpdet
        end


        @member_details << tmp
      end

        member_details_group = @member_details.group_by { |x| x[:member_center]  }.sort_by { |t, _| t[0] }

        member_details_group.each do |dt, array|
          mdg                   = {}
          mdg[:center_name]     = dt
          mdg[:center_details]  = []
          center_total_share = 0
          center_total_ave_amount   = 0
          center_total_savings_amount   = 0
          center_total_cbu_amount   = 0

          array.each do |a|
        
            member_savings_account = SavingsAccount.where(member_id: a[:member_id], savings_type_id: 1)
            
            member_savings_account.each do |msa|
              savings_transaction = SavingsAccountTransaction.where("savings_account_id = ? and transaction_date >= ?" ,msa.id,"2018-07-31")
              old_savings_account_amount = SavingsAccount.find(msa.id).balance

              puts  "#{ msa.id }"
              savings_transaction.each do |st|
                SavingsAccountTransaction.find(st.id).update(amount: st.amount.round(2))
              end
            
            ::Savings::RehashAccount.new(savings_account: SavingsAccount.find(msa.id)).execute!
              
              new_savings_account_amount = SavingsAccount.find(msa.id).balance

              puts  "#{msa.id | old_savings_account_amount | new_savings_account_amount }"
            end
            
          end
            

          #@data[:member_details_group] << mdg
        
        end





      @data
    end
  end
end
