module Tools
  class FetchDoubleFundTransferMembers
    def initialize
      @data     = {}
      @members  = Member.active
    end

    def execute!
      @data
    end
  end
end
