module Tools
  class GenerateAccountingEntryForTransferInterestOnShareCapital
    def initialize(interest_on_share_capital_id:, user:)
      @interest_on_share_capital_id = interest_on_share_capital_id
      @user = user

      @book = "JVB"
      @particular = "To Record Transferred of account"
      @c_working_date = ApplicationHelper.current_working_date
      
      @branch= Branch.where(id: Settings.branch_ids).first
      @voucher = Voucher.new(
                              status: "pending",
                              branch: @branch,
                              particular: @particular,
                              book: @book,
                              date_prepared: @c_working_date,
                              prepared_by: @user.to_s 
                            )
    end

    def execute!
      build_debit_entries!
      build_credit_for_savings_entries!
      @voucher
    end


    def build_debit_entries!
      accounting_code_debit = AccountingCode.find(1)
      debit_amount = 100
      
      journal_entry = JournalEntry.new(
                          amount: debit_amount,
                          post_type: "DR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry


    end
    
    def build_credit_for_savings_entries!
      accounting_code_debit = AccountingCode.find(1)
      credit_amount = 100
      journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "CR",
                          accounting_code: accounting_code_debit
                        )
      @voucher.journal_entries << journal_entry
   end
      
  
  end
end

