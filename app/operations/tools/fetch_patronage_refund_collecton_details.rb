module Tools
  class PatronageRefundCollectonDetails
    def initialize(patroange_refund_id:)
      @data = {}
      @patronage_refund_id = patroange_refund_id

      @patronage_refund_collection_records = PatronageRefundCollectionRecord.where(patronage_refund_collection_id:  @patronage_refund_id).order(" data ->> 'member_name' ASC"   )
        
      @member_details = []
      @data[:member_details_group] = []

    end
    
    def execute!
      @patronage_refund_collection_records.each do |prcr|
        member_status = Member.find(prcr.data["member_id"]).status
        tmp = {}
        tmp[:patronage_monthly_details] = []
        tmp[:member_id]=  prcr.data['member_id']
        tmp[:member_name] = prcr.data['member_name']
        tmp[:member_center] = Center.find(Member.find(prcr.data['member_id']).center_id).name
        tmp[:total_loan_interest_amount] = prcr.data['member_loan_interest_amount']
        tmp[:member_loan_interest_rate] = prcr.data['member_loan_interest_rate']
        tmp[:member_savings_amount] = prcr.data['dtotal_savings']
        tmp[:member_cbu_amount] = prcr.data['dtotal_cbu']

        prcr.data['reasons'].each do |r|
          reasonsdetails = {}
          reasonsdetails[:month] = r['month']
          reasonsdetails[:amount] = r['amount']
          tmp[:patronage_monthly_details] << reasonsdetails
        end


        @member_details << tmp
      end
      
      
      
      member_details_group = @member_details.group_by { |x| x[:member_center] }
      
      
    

      member_details_group.each do |mdgroup, array|
        mdg = {}
        mdg[:center_name] = mdgroup
        mdg[:center_details] = []
        center_total_share = 0
        center_total_ave_amount   = 0
        center_total_savings_amount   = 0
        center_total_cbu_amount   = 0
        
        array.each do |a|
          arraylist = {} 
          arraylist[:monthly_total]                 = []
          arraylist[:member_id]                   = a[:member_id]
          arraylist[:member_name]                 = a[:member_name]
          arraylist[:total_loan_interest_amount]  = (a[:total_loan_interest_amount].to_f).round(2)
          arraylist[:member_loan_interest_rate]   = (a[:member_loan_interest_rate].to_f).round(2)
          arraylist[:member_savings_amount]      = (a[:member_savings_amount].to_f).round(2)
          arraylist[:member_cbu_amount]           = (a[:member_cbu_amount].to_f).round(2)

          
          center_total_share          += arraylist[:total_loan_interest_amount]
          center_total_ave_amount     +=  arraylist[:member_loan_interest_rate]
          center_total_savings_amount +=  arraylist[:member_savings_amount]  
          center_total_cbu_amount     +=  arraylist[:member_cbu_amount]

          
          a[:patronage_monthly_details].each do |pmd|
            pmdetails = {}
            pmdetails[:amount] = pmd[:amount]
            arraylist[:monthly_total] << pmdetails 
          end
          
  
          mdg[:total_share]           = (center_total_share).round(2)
          mdg[:total_ave_details]     = (center_total_ave_amount).round(2)
          mdg[:total_savings_details] = (center_total_savings_amount).round(2)
          mdg[:total_cbu_details]     = (center_total_cbu_amount).round(2)


          mdg[:center_details] << arraylist
        end

        @data[:member_details_group] << mdg
        
      end

      @data
    
    end
  
  
  end

end
