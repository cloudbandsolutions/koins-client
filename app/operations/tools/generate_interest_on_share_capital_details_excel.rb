module Tools
  class GenerateInterestOnShareCapitalDetailsExcel
    def initialize(share_capital_interest_id:)
      @p = Axlsx::Package.new
      @data = ::Tools::FetchInterestOnShareCapitalDetails.new(share_capital_interest_id: share_capital_interest_id).execute!

    end
    
    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          sheet.add_row ["#","member_id","Name","Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec","Total_Share","Interest_Amount","Savings_amount","CBU"]
        
          @data[:member_details_group].each do |mdg|
            iter= 0
            if mdg[:center_name] == "cleared"
              mdg[:center_details].each do |cdet|
                data_row = []
                data_row << iter
                data_row << cdet[:member_id]
                data_row << cdet[:member_name]
                cdet[:equity_details].each do |ed|
                  data_row << ed[:amount]
                end
                data_row << cdet[:total_share]
                data_row << cdet[:total_average_share]    
                data_row << cdet[:total_savings_amount]
                data_row << cdet[:total_cbu_amount]

      
                iter = iter + 1
                sheet.add_row(data_row)
              end
            end

          end
        end
      end
      @p
    end
    

  end
end
