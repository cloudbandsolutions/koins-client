module Tools
  class ApprovedAccountingEntryForTransferOnShareCapital
    def initialize(share_capital_interest_id:)
       
      @data = ::Tools::FetchInterestOnShareCapitalDetails.new(share_capital_interest_id: share_capital_interest_id).execute!
      @approved_by = User.find(1).full_name
    end
    def execute!
      @data[:member_details_group].each do |mdg|
        
          mdg[:center_details].each do |cdet|
            savings_account_id = SavingsAccount.where(member_id: cdet[:member_id], savings_type_id: 1)
            cbu_account_id = SavingsAccount.where(member_id: cdet[:member_id], savings_type_id: 3)

                 
            savings_account_id.each do |sai|
              savings_account_transaction = SavingsAccountTransaction.create!(
                              amount: cdet[:total_savings_amount],
                              transacted_at: DateTime.now.to_date,
                              created_at: DateTime.now.to_date,
                              transaction_type: "withdraw",
                              particular: "for sample",
                              voucher_reference_number: "32132131",
                              savings_account_id: sai.id
              )
              savings_account_transaction.approve!(@approved_by)
            end

            cbu_account_id.each do |cai|
              cbu_account_transaction = SavingsAccountTransaction.create!(
                              amount: cdet[:total_cbu_amount],
                              transacted_at: DateTime.now.to_date,
                              created_at: DateTime.now.to_date,
                              transaction_type: "withdraw",
                              particular: "for sample",
                              voucher_reference_number: "32132131",
                              savings_account_id: cai.id
              )
              cbu_account_transaction.approve!(@approved_by)
            end
          end
         
      
      end
    end
  end
end
