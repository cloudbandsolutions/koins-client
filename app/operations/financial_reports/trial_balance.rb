module FinancialReports  
  class TrialBalance
    extend ActionView::Helpers::NumberHelper

    def self.get_entries(start_date, end_date, branch)
      @expenses_group_id  = Settings.expenses_group_id
      @expense_group      = MajorGroup.find(@expenses_group_id)

      @revenue_group_id   = Settings.revenue_group_id
      @revenue_group      = MajorGroup.find(@revenue_group_id)

      @revenue_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @revenue_group.id)
      @expense_accounting_codes = AccountingCode.joins(:accounting_code_category => [:mother_accounting_code => [:major_account => :major_group]]).where("major_groups.id = ?", @expense_group.id)

      data = {}
      data[:company] = Settings.company
      data[:branch] = branch
      data[:entries] = []
      data[:start_date] = Date.parse(start_date.to_s).strftime("%m/%d/%Y")
      data[:end_date] = Date.parse(end_date.to_s).strftime("%m/%d/%Y")

      year = Date.parse(start_date.to_s).year

      max_date = Date.parse(start_date.to_s) - 1
      #raise max_date.inspect

      data[:total_beginning_debit] = 0
      data[:total_beginning_credit] = 0
      data[:total_debit] = 0
      data[:total_credit] = 0
      data[:total_ending_debit] = 0
      data[:total_ending_credit] = 0

      AccountingCode.select("*").order(:code).each do |accounting_code|
        entry = {}
        entry[:accounting_code] = accounting_code

        # Compute beginning and ending DR/CR for each accounting_code
        beginning_debit = 0
        beginning_credit = 0
        final_beginning_amount = 0

        journal_entries = []

        if branch.nil?
          #journal_entries = JournalEntry.approved_entries_by_max_date_and_accounting_code(max_date, accounting_code)
          if @expense_accounting_codes.pluck(:id).include?(accounting_code.id) || @revenue_accounting_codes.pluck(:id).include?(accounting_code.id)
            beginning_debit = JournalEntry.joins(:voucher).where(
                                "vouchers.status = 'approved' 
                                AND journal_entries.accounting_code_id = ? 
                                AND post_type = 'DR' 
                                AND vouchers.date_posted <= ? 
                                AND vouchers.is_year_end_closing IS NULL
                                AND extract(year from vouchers.date_posted) = ?", 
                                accounting_code.id, 
                                max_date,
                                year
                              ).sum(:amount)

            beginning_credit =  JournalEntry.joins(:voucher).where(
                                  "vouchers.status = 'approved' 
                                  AND journal_entries.accounting_code_id = ? 
                                  AND post_type = 'CR' 
                                  AND vouchers.date_posted <= ? 
                                  AND vouchers.is_year_end_closing IS NULL
                                  AND extract(year from vouchers.date_posted) = ?", 
                                  accounting_code.id, 
                                  max_date,
                                  year
                                ).sum(:amount)
          else
            if accounting_code.id == 337
              beginning_debit = JournalEntry.joins(:voucher).where(
                                  "vouchers.status = 'approved' 
                                  AND journal_entries.accounting_code_id = ? 
                                  AND post_type = 'DR' 
                                  AND vouchers.date_posted <= ?",
                                  accounting_code.id, 
                                  max_date
                                ).sum(:amount)

              beginning_credit =  JournalEntry.joins(:voucher).where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ? 
                                    AND post_type = 'CR' 
                                    AND vouchers.date_posted <= ?",
                                    accounting_code.id, 
                                    max_date
                                  ).sum(:amount)
            else
              beginning_debit = JournalEntry.joins(:voucher).where(
                                  "vouchers.status = 'approved' 
                                  AND journal_entries.accounting_code_id = ? 
                                  AND post_type = 'DR' 
                                  AND vouchers.date_posted <= ? 
                                  AND vouchers.is_year_end_closing IS NULL",
                                  accounting_code.id, 
                                  max_date
                                ).sum(:amount)

              beginning_credit =  JournalEntry.joins(:voucher).where(
                                    "vouchers.status = 'approved' 
                                    AND journal_entries.accounting_code_id = ? 
                                    AND post_type = 'CR' 
                                    AND vouchers.date_posted <= ? 
                                    AND vouchers.is_year_end_closing IS NULL",
                                    accounting_code.id, 
                                    max_date
                                  ).sum(:amount)
            end
          end
        else
          #journal_entries = JournalEntry.approved_entries_by_max_date_and_branch_and_accounting_code(max_date, branch, accounting_code)
          if @expense_accounting_codes.pluck(:id).include?(accounting_code.id) || @revenue_accounting_codes.pluck(:id).include?(accounting_code.id)
            beginning_debit = JournalEntry.joins(:voucher).where(
                                "vouchers.branch_id = ? 
                                AND vouchers.status = 'approved' 
                                AND journal_entries.accounting_code_id = ? 
                                AND post_type = 'DR' AND vouchers.date_posted <= ? 
                                AND vouchers.is_year_end_closing IS NULL
                                AND extract(year from vouchers.date_posted) = ?", 
                                branch.id, 
                                accounting_code.id, 
                                max_date,
                                year
                              ).sum(:amount)

            beginning_credit =  JournalEntry.joins(:voucher).where(
                                  "vouchers.branch_id = ? 
                                  AND vouchers.status = 'approved' 
                                  AND journal_entries.accounting_code_id = ? 
                                  AND post_type = 'CR' 
                                  AND vouchers.date_posted <= ? 
                                  AND vouchers.is_year_end_closing IS NULL
                                  AND extract(year from vouchers.date_posted) = ?", 
                                  branch.id, 
                                  accounting_code.id, 
                                  max_date,
                                  year
                                ).sum(:amount)
          else
            beginning_debit = JournalEntry.joins(:voucher).where(
                                "vouchers.branch_id = ? 
                                AND vouchers.status = 'approved' 
                                AND journal_entries.accounting_code_id = ? 
                                AND post_type = 'DR' AND vouchers.date_posted <= ? 
                                AND vouchers.is_year_end_closing IS NULL",
                                branch.id, 
                                accounting_code.id, 
                                max_date
                              ).sum(:amount)

            beginning_credit =  JournalEntry.joins(:voucher).where(
                                  "vouchers.branch_id = ? 
                                  AND vouchers.status = 'approved' 
                                  AND journal_entries.accounting_code_id = ? 
                                  AND post_type = 'CR' 
                                  AND vouchers.date_posted <= ? 
                                  AND vouchers.is_year_end_closing IS NULL",
                                  branch.id, 
                                  accounting_code.id, 
                                  max_date
                                ).sum(:amount)
          end
        end

        if accounting_code.accounting_code_category.mother_accounting_code.major_account.major_group.dc_code == "DR"
          entry[:final_beginning_amount] = beginning_debit - beginning_credit
          if entry[:final_beginning_amount] < 0
            entry[:beginning_debit] = ""
            entry[:beginning_credit] = entry[:final_beginning_amount] * -1
          else
            entry[:beginning_debit] = entry[:final_beginning_amount]
            entry[:beginning_credit] = ""
          end
        else
          entry[:final_beginning_amount] = beginning_credit - beginning_debit
          if entry[:final_beginning_amount] < 0
            entry[:beginning_debit] = entry[:final_beginning_amount] * -1
            entry[:beginning_credit] = ""
          else
            entry[:beginning_debit] = ""
            entry[:beginning_credit] = entry[:final_beginning_amount]
          end
        end

        # Compute current DR/CR for each accounting code
        debit_amount = 0
        credit_amount = 0
        final_amount = 0

        if branch.nil?
          #journal_entries = JournalEntry.approved_entries_by_date_range_and_accounting_code(start_date, end_date, accounting_code)
          debit_amount =  JournalEntry.joins(:voucher).where(
                            "vouchers.status = 'approved' 
                            AND journal_entries.accounting_code_id = ? 
                            AND post_type = 'DR' 
                            AND vouchers.date_posted >= ? 
                            AND vouchers.date_posted <= ? 
                            AND vouchers.is_year_end_closing IS NULL
                            AND extract(year from vouchers.date_posted) = ?", 
                            accounting_code.id, 
                            start_date, 
                            end_date,
                            year
                          ).sum(:amount)

          credit_amount = JournalEntry.joins(:voucher).where(
                            "vouchers.status = 'approved' 
                            AND journal_entries.accounting_code_id = ? 
                            AND post_type = 'CR' AND vouchers.date_posted >= ? 
                            AND vouchers.date_posted <= ? 
                            AND vouchers.is_year_end_closing IS NULL
                            AND extract(year from vouchers.date_posted) = ?", 
                            accounting_code.id, 
                            start_date, 
                            end_date,
                            year
                          ).sum(:amount)
        else
          #journal_entries = JournalEntry.approved_entries_by_date_range_and_branch_and_accounting_code(start_date, end_date, branch, accounting_code)
          debit_amount =  JournalEntry.joins(:voucher).where(
                            "vouchers.branch_id = ? 
                            AND vouchers.status = 'approved' 
                            AND journal_entries.accounting_code_id = ? 
                            AND post_type = 'DR' 
                            AND vouchers.date_posted >= ? 
                            AND vouchers.date_posted <= ? 
                            AND vouchers.is_year_end_closing IS NULL
                            AND extract(year from vouchers.date_posted) = ?", 
                            branch.id, 
                            accounting_code.id, 
                            start_date, 
                            end_date,
                            year
                          ).sum(:amount)
          credit_amount = JournalEntry.joins(:voucher).where(
                            "vouchers.branch_id = ? 
                            AND vouchers.status = 'approved' 
                            AND journal_entries.accounting_code_id = ? 
                            AND post_type = 'CR' 
                            AND vouchers.date_posted >= ? 
                            AND vouchers.date_posted <= ? 
                            AND vouchers.is_year_end_closing IS NULL
                            AND extract(year from vouchers.date_posted) = ?", 
                            branch.id, 
                            accounting_code.id, 
                            start_date, 
                            end_date,
                            year
                          ).sum(:amount)
        end

        entry[:debit_amount] = debit_amount
        entry[:credit_amount] = credit_amount

        if accounting_code.accounting_code_category.mother_accounting_code.major_account.major_group.dc_code == "DR"
          entry[:final_amount] = debit_amount - credit_amount
          entry[:debit_amount] = debit_amount
          entry[:credit_amount] = credit_amount
        else
          entry[:final_amount] = credit_amount - debit_amount
          entry[:debit_amount] = debit_amount
          entry[:credit_amount] = credit_amount
        end

        # Compute for ending DR/CR for each accounting_code
        if accounting_code.accounting_code_category.mother_accounting_code.major_account.major_group.dc_code == "DR"
          entry[:final_debit_amount] = entry[:final_beginning_amount] + entry[:final_amount]
          if entry[:final_debit_amount] < 0
            entry[:final_credit_amount] = entry[:final_debit_amount] * -1
            entry[:final_debit_amount] = ""
          else
            entry[:final_debit_amount] = entry[:final_debit_amount]
            entry[:final_credit_amount] = ""
          end
        else
          entry[:final_credit_amount] = entry[:final_beginning_amount] + entry[:final_amount]
          if entry[:final_credit_amount] < 0
            entry[:final_debit_amount] = entry[:final_credit_amount] * -1
            entry[:final_credit_amount] = ""
          else
            entry[:final_debit_amount] = ""
            entry[:final_credit_amount] = entry[:final_credit_amount]
          end
        end

        if !entry[:final_debit_amount].blank?
          if entry[:final_debit_amount] != 0 or entry[:debit_amount] > 0 or entry[:credit_amount] > 0
            data[:entries] << entry
          end
        elsif !entry[:final_credit_amount].blank?
          if entry[:final_credit_amount] != 0 or entry[:debit_amount] > 0 or entry[:credit_amount] > 0
            data[:entries] << entry
          end
        end
      end

      data[:entries].each do |de|
        data[:total_beginning_debit] += de[:beginning_debit] if !de[:beginning_debit].blank?
        data[:total_beginning_credit] += de[:beginning_credit] if !de[:beginning_credit].blank?
        data[:total_debit] += de[:debit_amount] if !de[:debit_amount].blank?
        data[:total_credit] += de[:credit_amount] if !de[:credit_amount].blank?
        data[:total_ending_debit] += de[:final_debit_amount] if !de[:final_debit_amount].blank?
        data[:total_ending_credit] += de[:final_credit_amount] if !de[:final_credit_amount].blank?
      end

      # Format
      data[:total_beginning_debit] = number_with_delimiter(number_with_precision(data[:total_beginning_debit], precision: 2), delimiter: ",", separator: ".")
      data[:total_beginning_credit] = number_with_delimiter(number_with_precision(data[:total_beginning_credit], precision: 2), delimiter: ",", separator: ".")
      data[:total_debit] = number_with_delimiter(number_with_precision(data[:total_debit], precision: 2), delimiter: ",", separator: ".")
      data[:total_credit] = number_with_delimiter(number_with_precision(data[:total_credit], precision: 2), delimiter: ",", separator: ".")

      data[:total_ending_debit] = number_with_delimiter(number_with_precision(data[:total_ending_debit], precision: 2), delimiter: ",", separator: ".")
      data[:total_ending_credit] = number_with_delimiter(number_with_precision(data[:total_ending_credit], precision: 2), delimiter: ",", separator: ".")

      data[:entries].each_with_index do |de, i|
        data[:entries][i][:beginning_debit] = number_with_delimiter(number_with_precision(data[:entries][i][:beginning_debit], precision: 2), delimiter: ",", separator: ".")
        data[:entries][i][:beginning_credit] = number_with_delimiter(number_with_precision(data[:entries][i][:beginning_credit], precision: 2), delimiter: ",", separator: ".")
        data[:entries][i][:debit_amount] = number_with_delimiter(number_with_precision(data[:entries][i][:debit_amount], precision: 2), delimiter: ",", separator: ".") if !data[:entries][i][:debit_amount].blank?
        data[:entries][i][:credit_amount] = number_with_delimiter(number_with_precision(data[:entries][i][:credit_amount], precision: 2), delimiter: ",", separator: ".") if !data[:entries][i][:credit_amount].blank?
        data[:entries][i][:final_debit_amount] = number_with_delimiter(number_with_precision(data[:entries][i][:final_debit_amount], precision: 2), delimiter: ",", separator: ".") if !data[:entries][i][:final_debit_amount].blank?
        data[:entries][i][:final_credit_amount] = number_with_delimiter(number_with_precision(data[:entries][i][:final_credit_amount], precision: 2), delimiter: ",", separator: ".") if !data[:entries][i][:final_credit_amount].blank?
      end

      data
    end
  end
end
