module FinancialReports
  class VoucherSummary
    extend ActionView::Helpers::NumberHelper

    def self.get_entries(start_date, end_date, branch, book)

      data = {}
      data[:company] = Settings.company
      data[:branch] = branch
      data[:entries] = []
      data[:start_date] = Date.parse(start_date.to_s).strftime("%m/%d/%Y")
      data[:end_date] = Date.parse(end_date.to_s).strftime("%m/%d/%Y")
      data[:book] = book

      data[:grand_total_debit] = 0
      data[:grand_total_credit] = 0

      AccountingCode.select("*").order(:code).each do |accounting_code|
        entry = {}
        entry[:accounting_code] = accounting_code
        entry[:total_debit] = 0
        entry[:total_credit] = 0

        journal_entries = []

        if branch.nil?
          journal_entries = JournalEntry.joins(:voucher).approved_entries_by_date_range_and_accounting_code(start_date, end_date, accounting_code).where("vouchers.book = ?", book)
        else
          journal_entries = JournalEntry.joins(:voucher).approved_entries_by_date_range_and_branch_and_accounting_code(start_date, end_date, branch, accounting_code).where("vouchers.book = ?", book)
        end

        journal_entries.each do |journal_entry|
          if journal_entry.post_type == "DR"
            entry[:total_debit] += journal_entry.amount
            data[:grand_total_debit] += journal_entry.amount
          end

          if journal_entry.post_type == "CR"
            entry[:total_credit] += journal_entry.amount
            data[:grand_total_credit] += journal_entry.amount
          end
        end

        if entry[:total_debit] > 0 or entry[:total_credit] > 0
          data[:entries] << entry
        end
      end

      # Format
      data[:entries].each_with_index do |de, i|
        data[:entries][i][:total_debit] = number_with_delimiter(number_with_precision(data[:entries][i][:total_debit], precision: 2), delimiter: ",", separator: ".")
        data[:entries][i][:total_credit] = number_with_delimiter(number_with_precision(data[:entries][i][:total_credit], precision: 2), delimiter: ",", separator: ".")
      end

      data[:grand_total_debit] = number_with_delimiter(number_with_precision(data[:grand_total_debit], precision: 2), delimiter: ",", separator: ".")
      data[:grand_total_credit] = number_with_delimiter(number_with_precision(data[:grand_total_credit], precision: 2), delimiter: ",", separator: ".")

      data
    end
  end
end
