module FinancialReports
	class GenerateExcelForVoucherSummary
		def initialize(data:, start_date:, end_date:, book:)
   		@end_date   = end_date
      @book       = book
   		@start_date = start_date
   		@data       = data 
      @p          = Axlsx::Package.new
      @entries    = []
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center = wb.styles.add_style(alignment: {horizontal: :center})
          sheet.add_row []
          sheet.add_row []
          sheet.add_row [ "", "VOUCHER SUMMARY  - #{@book}"], style: @header_cells
          sheet.add_row [ "", "#{Settings.company}", "", ""], style: @header_cells
          sheet.add_row [ "", "#{Settings.company_address}"], style: @header_cells
          sheet.add_row [ "", "#{@start_date} - #{@end_date}"], style: @header_cells
          sheet.add_row []
          sheet.add_row ["Code", "Name", "Total Debit", "Total Credit"], style: @left_aligned_bold_cell, widths: [25, 60, 20, 20]
        
          @data[:entries].each do |entry|
						sheet.add_row ["#{entry[:accounting_code].code}", "#{entry[:accounting_code].name}","#{entry[:total_debit]}", "#{entry[:total_credit]}"], style: [@default_cell, @default_cell, @currency_cell, @currency_cell]           
          end
          sheet.add_row ["", "Total", "#{@data[:grand_total_debit]}", "#{@data[:grand_total_credit]}"], style: [nil, @left_aligned_bold_cell, @currency_cell_left_bold, @currency_cell_left_bold]
        end
      end

      @p
    end

    private
    def initialize_formats!(wb)
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_left_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @right_aligned_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri" 
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
	end
end