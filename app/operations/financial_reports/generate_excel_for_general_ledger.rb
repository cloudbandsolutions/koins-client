module FinancialReports
	class GenerateExcelForGeneralLedger
		def initialize(data:, start_date:, end_date:, user:)
   		@end_date   = end_date
   		@start_date = start_date
   		@data       = data 
      @p          = Axlsx::Package.new
      @entries    = []
      @user       = user
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center = wb.styles.add_style(alignment: {horizontal: :center})
          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["", "", "", "GENERAL LEDGER"], style: @header_cells
          sheet.add_row ["", "", "", "#{Settings.company}", "", ""], style: @header_cells
          sheet.add_row ["", "", "", "#{Settings.company_address}"], style: @header_cells
          sheet.add_row ["", "", "", "VAT Registration Tin Number: #{Settings.tin_number}"], style: @header_cells
          sheet.add_row ["", "", "", "Coverage: #{@start_date.to_date.strftime("%m/%d/%Y")} - #{@end_date.to_date.strftime("%m/%d/%Y")}"], style: @header_cells
          sheet.add_row []
          
          if Settings.activate_microloans
            sheet.add_row ["Date", "Ref. No", "", "Particular", "Debit(PHP)", "Credit(PHP)", "Balance(PHP)"], style: @left_aligned_bold_cell, widths: [15, 20, 5, 70, 20, 20, 20]
          
            @data[:entries].each do |entry|
  						sheet.add_row ["", "#{entry[:accounting_code].code.rjust(8, '0')}","", "#{entry[:accounting_code].name}", "" ,"", ""], style: @left_aligned_bold_cell
              sheet.add_row ["", "", "", "Beginning Balance", "", "", "#{entry[:beginning_balance]}"], style: [nil, nil, nil, @left_aligned_bold_cell, nil, nil, @currency_cell_right_bold]
              
              entry[:journal_entries].each do |je|
                sheet.add_row [
                  "#{je[:date_posted]}", 
                  "##{je[:reference_number].rjust(10, '0')}", 
                  "#{je[:book]}", 
                  "#{je[:particular]}", 
                  je[:debit_amount], 
                  je[:credit_amount], 
                  je[:running_balance]], 
                  style: [
                    @default_cell, 
                    @ref_num_cell, 
                    @default_cell, 
                    @default_cell, 
                    @currency_cell_right, 
                    @currency_cell_right, 
                    @currency_cell_right
                  ]
              end
              sheet.add_row ["", "", "", "Ending Balance", "", "", "#{entry[:ending_balance]}"], style: [nil, nil, nil, @left_aligned_bold_cell, nil, nil, @currency_cell_right_bold]
              sheet.add_row []
            end
          elsif Settings.activate_microinsurance
            sheet.add_row ["Date", "Ref. No", "Sub Ref. No", "", "Particular", "Debit", "Credit", "Balance"], style: @left_aligned_bold_cell, widths: [15, 20, 20, 5, 70, 20, 20, 20]
        
            @data[:entries].each do |entry|
              sheet.add_row ["", "", "#{entry[:accounting_code].code.rjust(8, '0')}","", "#{entry[:accounting_code].name}", "" ,"", ""], style: @left_aligned_bold_cell
              sheet.add_row ["", "", "", "", "Beginning Balance", "", "", "#{entry[:beginning_balance]}"], style: [nil, nil, nil, nil, @left_aligned_bold_cell, nil, nil, @currency_cell_right_bold]
              
              entry[:journal_entries].each do |je|
                sheet.add_row [
                  "#{je[:date_posted]}", 
                  "##{je[:reference_number].rjust(10, '0')}", 
                  "#{je[:sub_reference_number]}",
                  "#{je[:book]}", 
                  "#{je[:particular]}", 
                  je[:debit_amount], 
                  je[:credit_amount], 
                  je[:running_balance]], 
                  style: [
                    @default_cell, 
                    @ref_num_cell, 
                    @default_cell,
                    @default_cell, 
                    @default_cell, 
                    @currency_cell_right, 
                    @currency_cell_right, 
                    @currency_cell_right
                  ]
              end
              sheet.add_row ["", "", "", "", "Ending Balance", "", "", "#{entry[:ending_balance]}"], style: [nil, nil, nil, nil, @left_aligned_bold_cell, nil, nil, @currency_cell_right_bold]
              sheet.add_row []
            end
          end

          sheet.add_row []
          if Settings.activate_microloans
            sheet.add_row ["#{SOFTWARE_NAME} Version #{BUILD_VERSION}", "Date Printed: #{Time.now.strftime("%m/%d/%Y %H:%M")} | #{@user.full_name}"]
          elsif Settings.activate_microinsurance
            sheet.add_row ["Date Printed: #{Time.now.strftime("%m/%d/%Y %H:%M")} | #{@user.full_name}"]
          end
        end
      end

      @p
    end

    private
    def initialize_formats!(wb)
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_left_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @right_aligned_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri" 
      @right_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri", b: true
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11
      @ref_num_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11, format_code: "#"
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
	end
end
