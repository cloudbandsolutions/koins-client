module FinancialReports
	class GenerateExcelForTrialBalance
		def initialize(data:, start_date:, end_date:, user:)
   		@end_date   = end_date
   		@start_date = start_date
   		@data       = data 
      @p          = Axlsx::Package.new
      @entries    = []
      @user       = user
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center = wb.styles.add_style(alignment: {horizontal: :center})
          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["", "", "", "TRIAL BALANCE"], style: @header_cells
          sheet.add_row ["", "", "", "#{Settings.company}", "", ""], style: @header_cells
          sheet.add_row ["", "", "", "#{Settings.company_address}"], style: @header_cells
          sheet.add_row ["", "", "", "VAT Registration Tin Number: #{Settings.tin_number}"], style: @header_cells
          sheet.add_row ["", "", "", "Coverage: #{@start_date.to_date.strftime("%m/%d/%Y")} - #{@end_date.to_date.strftime("%m/%d/%Y")}"], style: @header_cells
          sheet.add_row []
          sheet.add_row ["", "", "Beginning Balance", "", "Transaction for the Month", "", "Ending Balance"], style: @header_cells
          sheet.add_row ["Code", "Name", "Debit", "Credit", "Debit", "Credit", "Debit", "Credit"], style: @left_aligned_bold_cell, widths: [20, 45, 20, 20, 20, 20, 20, 20]
        
          @data[:entries].each do |entry|
						sheet.add_row [
              "#{entry[:accounting_code].code.rjust(8, '0')}", 
              "#{entry[:accounting_code].name}", 
              "#{entry[:beginning_debit]}", 
              "#{entry[:beginning_credit]}", 
              "#{entry[:debit_amount]}", 
              "#{entry[:credit_amount]}", 
              "#{entry[:final_debit_amount]}", 
              "#{entry[:final_credit_amount]}"
            ], style: [
              @default_cell, 
              @default_cell, 
              @currency_cell, 
              @currency_cell, 
              @currency_cell, 
              @currency_cell, 
              @currency_cell, 
              @currency_cell
            ]
        	end

        	sheet.add_row [
            "", 
            "Total", 
            "#{@data[:total_beginning_debit]}", 
            "#{@data[:total_beginning_credit]}", 
            "#{@data[:total_debit]}", 
            "#{@data[:total_credit]}", 
            "#{@data[:total_ending_debit]}", 
            "#{@data[:total_ending_credit]}"
          ], style: [
            nil, 
            @left_aligned_bold_cell, 
            @currency_cell_left_bold, 
            @currency_cell_left_bold, 
            @currency_cell_left_bold, 
            @currency_cell_left_bold, 
            @currency_cell_left_bold, 
            @currency_cell_left_bold
          ]

          sheet.add_row []
          sheet.add_row ["#{SOFTWARE_NAME} Version #{BUILD_VERSION}", "Date Printed: #{Time.now.strftime("%m/%d/%Y %H:%M")} | #{@user.full_name}"]
        end
      end

      @p
    end

    private
    def initialize_formats!(wb)
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_left_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @right_aligned_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri" 
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
	end
end
