module FinancialReports  
  class GeneralLedger
    extend ActionView::Helpers::NumberHelper

    def initialize(start_date:, end_date:, branch:, accounting_code_ids:, user:)
      @data = {}
      @start_date           = start_date
      @end_date             = end_date
      @branch               = branch
      @accounting_code_ids  = accounting_code_ids
      @user                 = user
      
    
    end

    def execute!
      @data = get_entries(@start_date, @end_date, @branch, @accounting_code_ids, @user, @entries)
      
      @data
    end
  
    def get_entries(start_date, end_date, branch, accounting_code_ids, user, entries)
      data = {}
      data[:branch] = branch
      data[:company] = Settings.company
      data[:entries] = []
      data[:start_date] = Date.parse(start_date.to_s).strftime("%m/%d/%Y")
      data[:end_date] = Date.parse(end_date.to_s).strftime("%m/%d/%Y")

      max_date = Date.parse(start_date.to_s) - 1.day

      accounting_codes = []
      if accounting_code_ids.present?
        accounting_codes = AccountingCode.where(id: accounting_code_ids).order(:code)
      else
        accounting_codes = AccountingCode.select("*").order(:code)
      end

      #data[:total_cr] = JournalEntry.approved_entries_by_max_date(max_date).where(accounting_code_id: accounting_codes.pluck(:id), post_type: 'CR').sum(:amount)
      #data[:total_dr] = JournalEntry.approved_entries_by_max_date(max_date).where(accounting_code_id: accounting_codes.pluck(:id), post_type: 'DR').sum(:amount)
      data[:total_cr] = 0
      data[:total_dr] = 0

      accounting_codes.each do |accounting_code|
        accounting_code_set = {}
        accounting_code_set[:accounting_code] = accounting_code
        accounting_code_set[:beginning_balance] = 0
        accounting_code_set[:ending_balance] = 0
        accounting_code_set[:journal_entries] = []

        # Compute beginning and ending DR/CR for each accounting_code
        beginning_debit = 0
        beginning_credit = 0
        final_beginning_amount = 0

        journal_entries = []

        if branch.nil?
          #journal_entries = JournalEntry.approved_entries_by_max_date_and_accounting_code(max_date, accounting_code)
          journal_entries = JournalEntry.joins(:voucher).where(
                              "vouchers.status = 'approved' 
                              AND journal_entries.accounting_code_id = ?
                              AND vouchers.date_posted <= ?",
                              accounting_code.id,
                              max_date
                            ).order("vouchers.date_posted ASC")
        else
          #journal_entries = JournalEntry.approved_entries_by_max_date_and_branch_and_accounting_code(max_date, branch, accounting_code)
          journal_entries = JournalEntry.joins(:voucher).where(
                              "vouchers.status = 'approved' 
                              AND journal_entries.accounting_code_id = ?
                              AND vouchers.date_posted <= ? AND vouchers.branch_id = ?",
                              accounting_code.id,
                              max_date,
                              branch.id
                            ).order("vouchers.date_posted ASC")
        end

        journal_entries.each do |journal_entry|
          if journal_entry.post_type == "DR"
            beginning_debit += journal_entry.amount
          end

          if journal_entry.post_type == "CR"
            beginning_credit += journal_entry.amount
          end
        end

        if accounting_code.mother_accounting_code.major_account.major_group.dc_code == "DR"
          accounting_code_set[:beginning_balance] += beginning_debit - beginning_credit
        else
          accounting_code_set[:beginning_balance] += beginning_credit - beginning_debit
        end

        accounting_code_set[:ending_balance] = accounting_code_set[:beginning_balance]


        journal_entries = []

        if branch.nil?
          journal_entries = JournalEntry.approved_entries_by_date_range_and_accounting_code(start_date, end_date, accounting_code).order("date_posted ASC")
        else
          journal_entries = JournalEntry.approved_entries_by_date_range_and_branch_and_accounting_code(start_date, end_date, branch, accounting_code).order("date_posted ASC")
        end

        journal_entries.each do |journal_entry|
          if journal_entry.amount > 0
            date_posted = journal_entry.voucher.date_posted.strftime("%m/%d/%Y")
            sub_reference_number = journal_entry.voucher.sub_reference_number
            reference_number = journal_entry.voucher.reference_number
            particular = journal_entry.voucher.particular
            book = "Invalid Book"
            if journal_entry.voucher.book == "JVB"
              book = "JV"
            elsif journal_entry.voucher.book == "CRB"
              book = "OR"
            elsif journal_entry.voucher.book == "CDB"
              book = "CD"
            end

            debit_amount = journal_entry.post_type == "DR" ? journal_entry.amount : 0
            credit_amount = journal_entry.post_type == "CR" ? journal_entry.amount : 0

            if accounting_code.mother_accounting_code.major_account.major_group.dc_code == "DR"
              accounting_code_set[:ending_balance] += debit_amount
              accounting_code_set[:ending_balance] -= credit_amount
            else
              accounting_code_set[:ending_balance] -= debit_amount
              accounting_code_set[:ending_balance] += credit_amount
            end

            if debit_amount == 0
              debit_amount = ""
              data[:total_cr] +=  credit_amount
              #credit_amount = number_with_delimiter(number_with_precision(credit_amount, precision: 2), delimiter: ",", separator: ".")
            end

            if credit_amount == 0
              credit_amount = ""
              data[:total_dr] +=  debit_amount
              #debit_amount = number_with_delimiter(number_with_precision(debit_amount, precision: 2), delimiter: ",", separator: ".")
            end

            accounting_code_set[:journal_entries] << { 
              date_posted: date_posted, 
              reference_number: reference_number, 
              sub_reference_number: sub_reference_number, 
              particular: particular, 
              debit_amount: debit_amount, 
              credit_amount: credit_amount, 
              book: book, 
              running_balance: accounting_code_set[:ending_balance]
            }
          end
        end

        accounting_code_set[:beginning_balance] = accounting_code_set[:beginning_balance]
        accounting_code_set[:ending_balance]    = accounting_code_set[:ending_balance]

        if accounting_code_set[:journal_entries].count > 0
          data[:entries] << accounting_code_set
        end
      end

      data
    
    end
  end
end
