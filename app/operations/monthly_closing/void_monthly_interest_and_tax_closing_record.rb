module MonthlyClosing
  class VoidMonthlyInterestAndTaxClosingRecord
    require 'application_helper'

    def initialize(monthly_interest_and_tax_closing_record:, user:)
      @monthly_interest_and_tax_closing_record  = monthly_interest_and_tax_closing_record
      @user                                     = user
      @voucher                                  = MonthlyClosing::ProduceVoucherForMonthlyInterestAndTaxClosingRecord.new(
                                                    monthly_interest_and_tax_closing_record: @monthly_interest_and_tax_closing_record,
                                                    user: @user
                                                  ).execute!

      @approved_by      = @user.full_name
      @current_date     = ApplicationHelper.current_working_date
    end

    def execute!
      ActiveRecord::Base.transaction do
        @reverse_voucher  = Accounting::GenerateReverseEntry.new(voucher: @voucher).execute!
        @particular       = @reverse_voucher.particular
        reverse_transactions!
        @monthly_interest_and_tax_closing_record.update!(
                                                  status: "void",
                                                  voided_by: @approved_by,
                                                  reverse_voucher_reference_number: @reverse_voucher.reference_number
                                                )
      end

      @monthly_interest_and_tax_closing_record
    end

    private

    def reverse_transactions!
      @monthly_interest_and_tax_closing_record.tax_and_interest_transactions.each do |t|
        savings_account = t.savings_account

        if t.tax_amount > 0
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: t.tax_amount,
                                          transacted_at: @current_date,
                                          created_at: @current_date,
                                          transaction_type: "deposit",
                                          particular: @particular,
                                          voucher_reference_number: @reverse_voucher.reference_number,
                                          savings_account: savings_account,
                                          is_adjustment: true
                                        )

          savings_account_transaction.approve!(@approved_by)
        end

        if t.interest_amount > 0
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: t.interest_amount,
                                          transacted_at: @current_date,
                                          created_at: @current_date,
                                          transaction_type: "withdraw",
                                          particular: @particular,
                                          voucher_reference_number: @reverse_voucher.reference_number,
                                          savings_account: savings_account,
                                          is_adjustment: true
                                        )

          savings_account_transaction.approve!(@approved_by)
        end
      end
    end
  end
end
