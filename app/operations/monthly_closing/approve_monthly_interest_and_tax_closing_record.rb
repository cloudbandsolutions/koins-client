module MonthlyClosing
  class ApproveMonthlyInterestAndTaxClosingRecord
    require 'application_helper'

    def initialize(monthly_interest_and_tax_closing_record:, user:)
      @monthly_interest_and_tax_closing_record  = monthly_interest_and_tax_closing_record
      @user                                     = user
      @approved_by                              = @user.full_name
      @voucher                                  = MonthlyClosing::ProduceVoucherForMonthlyInterestAndTaxClosingRecord.new(
                                                    monthly_interest_and_tax_closing_record: @monthly_interest_and_tax_closing_record, 
                                                    user: @user
                                                  ).execute!
    end

    def execute!
      @voucher.save!
      @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
      @monthly_interest_and_tax_closing_record.update!(
        status: "approved", 
        voucher_reference_number: @voucher.reference_number,
        closing_date: @voucher.date_prepared
      )

      @monthly_interest_and_tax_closing_record.tax_and_interest_transactions.each do |t|
        # Interest
        if t.interest_amount > 0
          #current_date = ApplicationHelper.current_working_date
          current_date = @voucher.date_posted
          savings_account_transaction = SavingsAccountTransaction.create!(
                                            amount: t.interest_amount,
                                            transacted_at: @monthly_interest_and_tax_closing_record.closing_date,
                                            created_at: @monthly_interest_and_tax_closing_record.closing_date,
                                            #transacted_at: current_date,
                                            #created_at: current_date,
                                            transaction_type: "interest",
                                            particular: @voucher.particular,
                                            voucher_reference_number: @monthly_interest_and_tax_closing_record.voucher_reference_number,
                                            savings_account: t.savings_account
                                          )


          savings_account_transaction.approve!(@approved_by)
        end

        # Tax
        if t.tax_amount > 0
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: t.tax_amount,
                                          transacted_at: @monthly_interest_and_tax_closing_record.closing_date,
                                          created_at: @monthly_interest_and_tax_closing_record.closing_date,
                                          #transacted_at: current_date,
                                          #created_at: current_date,
                                          transaction_type: "tax",
                                          particular: @voucher.particular,
                                          voucher_reference_number: @monthly_interest_and_tax_closing_record.voucher_reference_number,
                                          savings_account: t.savings_account
                                        )

          savings_account_transaction.approve!(@approved_by)
        end
      end

      @monthly_interest_and_tax_closing_record
    end
  end
end
