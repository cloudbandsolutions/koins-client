module MonthlyClosing
  class ValidateApproveMonthlyInterestAndTaxClosingRecord
    def initialize(monthly_interest_and_tax_closing_record:)
      @monthly_interest_and_tax_closing_record =  monthly_interest_and_tax_closing_record
      @errors = []
    end

    def execute!
      @errors
    end
  end
end
