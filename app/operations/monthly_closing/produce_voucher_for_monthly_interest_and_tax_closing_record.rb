module MonthlyClosing
  class ProduceVoucherForMonthlyInterestAndTaxClosingRecord
    require 'application_helper'

    def initialize(monthly_interest_and_tax_closing_record:, user:)
      @monthly_interest_and_tax_closing_record  = monthly_interest_and_tax_closing_record
      @user                                     = user
      @savings_types                            = SavingsType.where(id: Settings.savings_types_for_tax_and_interest)
      @closing_date = @monthly_interest_and_tax_closing_record.closing_date

      if @monthly_interest_and_tax_closing_record.special == true
        @closing_date = ApplicationHelper.current_working_date
      end
    end

    def execute!
      if @monthly_interest_and_tax_closing_record.pending?
        @voucher  = Voucher.new(
                      book: 'JVB',
                      particular: "Monthly interest and tax for #{@monthly_interest_and_tax_closing_record.branch}",
                      branch: @monthly_interest_and_tax_closing_record.branch,
                      date_prepared: @closing_date
                    )

        build_debit_entries!
        build_credit_entries!
      else
        @voucher = Voucher.where(book: 'JVB', reference_number: @monthly_interest_and_tax_closing_record.voucher_reference_number).first
      end

      @voucher
    end

    private

    def build_debit_entries!
      @savings_types.each do |savings_type|
        # Interest Expense
        amount = 0.00
        accounting_code = savings_type.expense_accounting_code

        @monthly_interest_and_tax_closing_record.tax_and_interest_transactions.each do |t|
          if t.savings_account.savings_type.id == savings_type.id
            amount += t.interest_amount + t.tax_amount
          end
        end

        journal_entry  = JournalEntry.new
        journal_entry.post_type = 'DR'
        journal_entry.accounting_code = accounting_code
        journal_entry.amount = amount

        @voucher.journal_entries << journal_entry
      end
    end

    def build_credit_entries!
      # Interest
      @savings_types.each do |savings_type|
        # Interest Expense
        amount = 0.00
        accounting_code = savings_type.interest_accounting_code

        @monthly_interest_and_tax_closing_record.tax_and_interest_transactions.each do |t|
          if t.savings_account.savings_type.id == savings_type.id
            amount += t.interest_amount
          end
        end

        journal_entry  = JournalEntry.new
        journal_entry.post_type = 'CR'
        journal_entry.accounting_code = accounting_code
        journal_entry.amount = amount

        @voucher.journal_entries << journal_entry
      end

      # Tax
      @savings_types.each do |savings_type|
        # Interest Expense
        amount = 0.00
        accounting_code = savings_type.tax_accounting_code

        @monthly_interest_and_tax_closing_record.tax_and_interest_transactions.each do |t|
          if t.savings_account.savings_type.id == savings_type.id
            amount += t.tax_amount
          end
        end

        journal_entry  = JournalEntry.new
        journal_entry.post_type = 'CR'
        journal_entry.accounting_code = accounting_code
        journal_entry.amount = amount

        @voucher.journal_entries << journal_entry
      end
    end
  end
end
