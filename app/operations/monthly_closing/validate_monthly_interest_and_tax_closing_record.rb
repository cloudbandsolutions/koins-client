module MonthlyClosing
  class ValidateMonthlyInterestAndTaxClosingRecord
    #closing_month as integer
    def initialize(closing_month:, savings_type_id:, special:)
      @closing_month  = closing_month
      @current_year   = Date.today.year
      @current_month  = Date.today.month
      @savings_type   = SavingsType.where(id: savings_type_id).first
      @special        = special
      @errors         = []
    end

    def execute!
      check_params!
      check_for_special!
      check_if_record_exists!
      check_for_pending!
      @errors
    end

    private

    def check_for_special!
      if @savings_type and !@special
        @errors << "Cannot generate record for #{@savings_type.to_s} for non-special"
      end
    end

    def check_for_pending!
      if MonthlyInterestAndTaxClosingRecord.pending.count > 0
        @errors << "Pending records still exist"
      end
    end

    def check_if_record_exists!
      if MonthlyInterestAndTaxClosingRecord.where(month: @current_month, year: @current_year).count > 0 and !@special
        @errors << "Already closed for #{@current_month}/#{@current_year}"
      end
    end

    def check_params!
      if @closing_month.blank?
        @errors << "Closing month required"
      else
        if ![1,2,3,4,5,6,7,8,9,10,11,12].include?(@closing_month)
          @errors << "Invalid closing month"
        end
      end
    end
  end
end
