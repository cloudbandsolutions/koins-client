module MonthlyClosing
  class ValidateVoidMonthlyInterestAndTaxClosingRecord
    require 'application_helper'

    def initialize(monthly_interest_and_tax_closing_record:)
      @monthly_interest_and_tax_closing_record =  monthly_interest_and_tax_closing_record
      @particular = "temp particular"
      @temp_ref_number = Time.now.to_i
      @current_date     = ApplicationHelper.current_working_date
      @approved_by      = "dummy"
      @errors = []
    end

    def execute!
      validate_transactions!
      @errors
    end

    def validate_transactions!
      @monthly_interest_and_tax_closing_record.tax_and_interest_transactions.each do |t|
        savings_account = t.savings_account

        if t.tax_amount > 0
          savings_account_transaction = SavingsAccountTransaction.new(
                                          amount: t.tax_amount,
                                          transacted_at: @current_date,
                                          created_at: @current_date,
                                          transaction_type: "deposit",
                                          particular: @particular,
                                          voucher_reference_number: @temp_ref_number,
                                          savings_account: savings_account,
                                          is_adjustment: true
                                        )
  
          
          if !savings_account_transaction.valid?
            savings_account_transaction.errors.messages.each do |m|
              @errors << m
            end
          end
        end

        if t.interest_amount > 0
          savings_account_transaction = SavingsAccountTransaction.new(
                                          amount: t.interest_amount,
                                          transacted_at: @current_date,
                                          created_at: @current_date,
                                          transaction_type: "withdraw",
                                          particular: @particular,
                                          voucher_reference_number: @temp_ref_number,
                                          savings_account: savings_account,
                                          is_adjustment: true
                                        )

          if !savings_account_transaction.valid?
            savings_account_transaction.errors.messages.each do |m|
              @errors << m
            end
          end
        end
      end
    end
  end
end
