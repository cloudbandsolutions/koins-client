module MonthlyClosing
  class GenerateMonthlyInterestAndTaxClosingRecord
    require 'application_helper'

    def initialize(closing_month:, branch:, savings_type_id:, special:)
      @current_working_date = ApplicationHelper.current_working_date
      @branch           = branch
      @closing_month    = closing_month
      @closing_year     = @current_working_date.year
      @members          = Member.pure_active.where(branch_id: @branch.id)
      @special          = special
      @savings_type     = SavingsType.where(id: savings_type_id).first
      @savings_type_ids = SavingsType.where(id: Settings.savings_types_for_tax_and_interest).pluck(:id)
      @closing_date     = last_working_day(@closing_month.to_i, @closing_year.to_i)

      if @savings_type
        @savings_type_ids = [@savings_type.id]
      end

      @savings_accounts = SavingsAccount.where(
                            savings_type_id: @savings_type_ids, 
                            member_id: @members.pluck(:id)
                          )

      @monthly_interest_and_tax_closing_record = MonthlyInterestAndTaxClosingRecord.new(
                                                  closing_date: @closing_date, 
                                                  branch: @branch,
                                                  special: @special
                                                )
    end

    def execute! 
      total_interest = 0.00
      total_tax = 0.00
      
      @savings_accounts.each do |savings_account|
        savings_type          = savings_account.savings_type
        monthly_tax_rate      = savings_type.monthly_tax_rate
        annual_interest_rate  = savings_type.annual_interest_rate
        

        if savings_type.id == 1
          



          savings_account_transaction =  SavingsAccountTransaction.where("
                                                                        savings_account_id = ? and 
                                                                        transaction_type <> ?", 
                                                                        SavingsAccount.where(member_id: savings_account.member.id, savings_type: 1).last.id,
                                                                        "interest").last
          if savings_account_transaction
            member_savings_account_transaction = savings_account_transaction.transacted_at
            final_date     = member_savings_account_transaction.to_date + 2.years
            if member_savings_account_transaction > final_date
               annual_interest_rate = 0
            
            else
                is_dormant_loaner     = ::Members::IsDormantLoaner.new(
                                                                        member: savings_account.member,
                                                                        threshold: Settings.dormant_loaner_threshold,
                                                                        current_date: @current_working_date,
                                                                        savings_type: savings_type
                                                                      ).execute!

                if is_dormant_loaner
                  annual_interest_rate  = Settings.dormant_loaner_annual_interest_rate || 0.02
                end
               
            end
          else
              annual_interest_rate = 0 
          end
        
        else
          
          is_dormant_loaner     = ::Members::IsDormantLoaner.new(
                                                                  member: savings_account.member,
                                                                  threshold: Settings.dormant_loaner_threshold,
                                                                  current_date: @current_working_date,
                                                                  savings_type: savings_type
                                                                ).execute!

          if is_dormant_loaner
            annual_interest_rate  = Settings.dormant_loaner_annual_interest_rate || 0.02
          end
        
        
        end

        #raise annual_interest_rate.inspect

        monthly_interest_rate = annual_interest_rate / 12

        closing_interest_and_tax = Savings::ComputeClosingInterestAndTax.new(
                                    savings_account: savings_account, 
                                    closing_date: @closing_date, 
                                    annual_interest_rate: annual_interest_rate, 
                                    tax_rate: monthly_tax_rate).execute!

        if !closing_interest_and_tax.nil?
          tax_and_interest_transaction = TaxAndInterestTransaction.new(
              savings_account: savings_account,
              tax_amount: closing_interest_and_tax[:total_tax],
              interest_amount: closing_interest_and_tax[:total_interest],
              monthly_tax_rate: monthly_tax_rate,
              monthly_interest_rate: monthly_interest_rate,
              annual_interest_rate: savings_type.annual_interest_rate,
              uuid: SecureRandom.uuid,
            )

          @monthly_interest_and_tax_closing_record.tax_and_interest_transactions << tax_and_interest_transaction

          total_interest += closing_interest_and_tax[:total_interest]
          total_tax +=  closing_interest_and_tax[:total_tax]
        end
      end

      @monthly_interest_and_tax_closing_record.total_interest = total_interest
      @monthly_interest_and_tax_closing_record.total_tax  = total_tax

      @monthly_interest_and_tax_closing_record
    end

    private

    def last_working_day(month, year)
      day = Date.new(year, month, 1).end_of_month + 1.day
      loop do
        day = day.prev_day
        break unless day.saturday? or day.sunday?
      end

      day
    end
  end
end
