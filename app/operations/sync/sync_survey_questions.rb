module Sync
  class SyncSurveyQuestions
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing Survey Questions"
      print "."
      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:content]}")
          print "."

          survey_question = SurveyQuestion.where(id: o[:id]).first

          if survey_question.blank?
            Rails.logger.info("Survey Question #{o[:id]} not found. Creating new one.")
            print "."

            SurveyQuestion.new do |sq|
              sq.id = o[:id]
              
              survey = Survey.where(id: o[:survey_id]).first
              if survey.blank?
                Rails.logger.error "Survey #{o[:survey_id]} not found"
                print "E"
              else
                sq.survey = survey
              end

              sq.uuid = o[:uuid]
              sq.priority = o[:priority]
              sq.content = o[:content]


              if sq.valid?
                Rails.logger.info("Saving new survey question #{o[:id]}")
                print "."
                sq.save!
              else
                Rails.logger.error("Error in creating survey question")
                print "E"
              end
            end
          else
           Rails.logger.info("Updating survey question #{o[:id]}")
            print "."

            survey = Survey.where(id: o[:survey_id]).first
            if survey.blank?
              Rails.logger.error "Survey #{o[:survey_id]} not found"
              print "E"
            else
              survey_question.update!(
                survey: survey,
                uuid: o[:uuid],
                priority: o[:priority],
                content: o[:content]
              )
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        SurveyQuestion.where.not(id: ids).destroy_all
        
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end

