module Sync
  class SyncAccountingCodeCategories
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing accounting code categories"
      print "."

      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]}")
          print "."

          accounting_code_category = AccountingCodeCategory.where(id: o[:id]).first
          mother_accounting_code = MotherAccountingCode.where(id: o[:mother_accounting_code_id]).first

          if accounting_code_category.nil?
            Rails.logger.info("Accounting code category: #{o[:id]} not found. Creating new one.")
            print "."

            AccountingCodeCategory.new do |acc|
              acc.id = o[:id]
              acc.name = o[:name]
              acc.mother_accounting_code = mother_accounting_code
              acc.sub_code = o[:sub_code]

              if acc.valid?
                Rails.logger.info("Saving new accounting code category #{o[:id]}")
                print "."
                acc.save!
              else
                Rails.logger.error("Error in creating accounting code category: #{acc.errors.messages}")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating accounting code category #{o[:id]}")
            print "."
            
            if accounting_code_category.update(
              name: o[:name],
              sub_code: o[:sub_code],
              mother_accounting_code: mother_accounting_code
            )
              Rails.logger.info("Successfully updated accounting code category #{o[:id]}")
              print "."
            else
              print "E"
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        AccountingCodeCategory.where.not(id: ids).destroy_all

      elsif http.response_code == 404
        Rails.logger.error("404: Not Found - #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
