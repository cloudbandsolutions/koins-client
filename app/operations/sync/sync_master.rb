module Sync
  class SyncMaster
    MASTER_SERVER = Settings.master_server
    API_KEY = Settings.api_key

    # URL constants
    MASTER_URL_SYNC_MEMBER_DATA = "#{MASTER_SERVER}/api/v1/sync/members"
    MASTER_URL_SYNC_CENTER_DATA = "#{MASTER_SERVER}/api/v1/sync/centers"

    def self.sync_members!
      Rails.logger.info "Syncing centers"

      params = {
        api_key: API_KEY,
        centers: Center.all.to_json
      }

      http = Curl.post(MASTER_URL_SYNC_CENTER_DATA, params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        puts "Successfully synced centers!"
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{MASTER_URL_SYNC_CENTER_DATA}")
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
      end

      puts "Syncing members..."
      Rails.logger.info "Syncing members..."

      Member.all.each_with_index do |member, i|
        params = {
          api_key: API_KEY,
          member: member.to_hash.to_json
        }

        http = Curl.post(MASTER_URL_SYNC_MEMBER_DATA, params)

        if http.response_code == 200
          data = JSON.parse(http.body_str, symbolize_names: true)[:data]
          Rails.logger.debug data
          puts "#{i}: Done syncing member: #{member.uuid}"
          Rails.logger.info "#{i}: Done syncing member: #{member.uuid}"
        elsif http.response_code == 404
          Rails.logger.error("404: Not Found: #{MASTER_URL_SYNC_MEMBER_DATA}")
        else
          Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        end
      end
    end
  end
end
