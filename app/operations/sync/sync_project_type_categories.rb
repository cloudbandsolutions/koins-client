module Sync
  class SyncProjectTypeCategories
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing Project Type Categories"
      print "."

      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]}")
          print "."

          project_type_categoy = ProjectTypeCategory.where(id: o[:id]).first

          if project_type_categoy.blank?
            Rails.logger.info("Project type category #{o[:name]} not found. Creating new one.")
            print "."

            ProjectTypeCategory.new do |ptc|
              ptc.id = o[:id]
              ptc.name = o[:name]
              ptc.code = o[:code]

              if ptc.valid?
                Rails.logger.info("Saving new project type category #{o[:id]}")
                print "."
                ptc.save!
              else
                Rails.logger.error("Error in creating project type categoy")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating project type categoy #{o[:id]}")
            print "."

            project_type_categoy.update!(
              name: o[:name],
              code: o[:code]
            )
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ProjectTypeCategory.where.not(id: ids).destroy_all

      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end

