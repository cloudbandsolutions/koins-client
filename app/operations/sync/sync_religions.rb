module Sync
  class SyncReligions
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing Religions"
      print "."

      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]}")
          print "."

          religion = Religion.where(id: o[:id]).first

          if religion.blank?
            Rails.logger.info("Religion #{o[:name]} not found. Creating new one.")
            print "."

            Religion.new do |r|
              r.id = o[:id]
              r.name = o[:name]

              if r.valid?
                Rails.logger.info("Saving new religion #{o[:id]}")
                print "."
                r.save!
              else
                Rails.logger.error("Error in creating religion")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating religion #{o[:id]}")
            print "."

            religion.update!(
              name: o[:name]
            )
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        Religion.where.not(id: ids).destroy_all

      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
