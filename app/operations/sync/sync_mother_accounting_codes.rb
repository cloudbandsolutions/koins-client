module Sync
  class SyncMotherAccountingCodes
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing mother accounting codes"
      print "."
      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          mother_accounting_code = MotherAccountingCode.where(id: o[:id]).first
          major_account = MajorAccount.where(id: o[:major_account_id]).first

          if mother_accounting_code.blank?
            Rails.logger.info("Mother accounting code #{o[:id]} not found. Creating new one.")
            print "."

            MotherAccountingCode.new do |mac|
              mac.id = o[:id]
              mac.name = o[:name]
              mac.code = o[:code]
              mac.subcode = o[:subcode]

              major_account = MajorAccount.where(id: o[:major_account_id]).first

              if major_account.blank?
                Rails.logger.error "Major account #{o[:major_account_id]} not found"
                print "E"
              else
                mac.major_account = major_account
              end

              if mac.valid?
                Rails.logger.info("Saving new mother accounting code #{o[:id]}")
                print "."
                mac.save!
              else
                Rails.logger.error("Error in creating mother accounting code")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating mother accounting code #{o[:id]}")
            print "."
            if major_account.blank?
              Rails.logger.error "Major account #{o[:major_account_id]} not found"
              print "E"
            else
              if mother_accounting_code.update(
                name: o[:name], 
                code: o[:code],
                subcode: o[:subcode],
                major_account: major_account
              )
              else
                Rails.logger.error("Error: #{mother_accounting_code.errors.messages}")
                print "E"
              end
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        MotherAccountingCode.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
