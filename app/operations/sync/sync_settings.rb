module Sync
  class SyncSettings
    MASTER_SERVER = Settings.master_server
    API_KEY = Settings.api_key
    PARAMS = { api_key: API_KEY }

    def self.sync_banks!
      Rails.logger.info "Syncing banks"
      print "."

      url = "#{MASTER_SERVER}/api/v1/sync/finance/settings/banks"
      http = Curl.post(url, PARAMS)
    end
  end
end
