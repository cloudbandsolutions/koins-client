module Sync
  class SyncAccountingCodes
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing accounting codes"
      print "."

      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          accounting_code = AccountingCode.where(id: o[:id]).first
          accounting_code_category = AccountingCodeCategory.where(id: o[:accounting_code_category_id]).first

          if accounting_code.blank?
            Rails.logger.info("Accounting code #{o[:name]} not found. Creating new one.")
            print "."

            AccountingCode.new do |ac|
              ac.id = o[:id]
              ac.name = o[:name]
              ac.code = o[:code]
              ac.beginning_balance = 0.00
              ac.ending_balance = 0.00
              ac.normal_transaction = o[:normal_transaction]
              ac.sub_code = o[:sub_code]
              ac.beginning_debit = o[:beginning_debit]
              ac.beginning_credit = o[:beginning_credit]
              ac.ending_debit = o[:ending_debit]
              ac.ending_credit = o[:ending_credit]
              if accounting_code_category.blank?
                Rails.logger.error "Accounting code category #{o[:accounting_code_category_id]} not found"
                print "E"
              else
                ac.accounting_code_category = accounting_code_category
              end

              if ac.valid?
                Rails.logger.info("Saving new accounting code #{o[:id]}")
                print "."
                ac.save!
              else
                Rails.logger.error("Error in creating accounting code: #{ac.errors.messages}")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating accounting code #{o[:id]}")
            print "."

            if accounting_code_category.blank?
              Rails.logger.error "Accounting code category #{o[:accounting_code_category_id]} not found"
              print "E"
            else
              accounting_code.update!(
                name: o[:name], 
                code: o[:code],
                sub_code: o[:sub_code],
                main_code: o[:main_code],
                normal_transaction: o[:normal_transaction],
                accounting_code_category: accounting_code_category
              )
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        AccountingCode.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found - #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
