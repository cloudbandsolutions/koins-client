module Sync
  class SyncClusterInformation
    MASTER_SERVER = Settings.master_server
    API_KEY = Settings.api_key
    PARAMS = { api_key: API_KEY }

    # URL constants
    MASTER_URL_SYNC_CLUSTERS = "#{MASTER_SERVER}/api/v1/sync/cluster_office/information/clusters"
    MASTER_URL_SYNC_AREAS = "#{MASTER_SERVER}/api/v1/sync/cluster_office/information/areas"
    MASTER_URL_SYNC_BANKS = "#{MASTER_SERVER}/api/v1/sync/cluster_office/information/banks"

    def self.sync_cluster_information!
      Sync::SyncClusterInformation.sync_areas!
      Sync::SyncClusterInformation.sync_clusters!
      Sync::SyncClusterInformation.sync_branches_and_centers!
    end

    def self.sync_banks!
      Rails.logger.info "Syncing banks"
      print "."
      
      url_banks = MASTER_URL_SYNC_BANKS
      Rails.logger.info "Syncing bank information from #{url_banks}"
      print "."

      http = Curl.post(url_banks, PARAMS)
      response_code = http.response_code
      
      if response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."
        data.each do |o|
          Rails.logger.info "Syncing Bank #{o[:name]}"
          print "."

          bank = Bank.where(id: o[:id]).first

          if bank.blank?
            Rails.logger.info "Creating new bank #{o[:name]}"
            print "."

            Bank.new do |br|
              br.id = o[:id]
              br.name = o[:name]
              br.code = o[:code]
              br.accounting_code = AccountingCode.where(id: o[:accounting_code_id]).first

              if br.valid?
                br.save!
                Rails.logger.info "Successfully created new bank: #{br.to_s}"
              else
                Rails.logger.error "Error in creating bank: #{br.error.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating bank #{o[:id]}"
            print "."

            bank.update!(
              name: o[:name],
              code: o[:code],
              accounting_code_id: o[:accounting_code_id]
            )

            Rails.logger.info "Updated bank #{bank.to_s}"
          end

          Rails.logger.info "Deleting unwanted data"
          print "."
          ids = (data.map { |o| o[:id] }).uniq
          Bank.where.not(id: ids).destroy_all
        end
      elsif response_code == 404
        Rails.logger.error "404: Not Found - #{url_banks}"
        print "E"
      else 
        Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end
    end

    def self.sync_branches_and_centers!
      Rails.logger.info "Syncing branches"
      branch_ids = Settings.branch_ids
      Rails.logger.info "Branch Ids: #{branch_ids}"
      url_branches = "#{MASTER_SERVER}/api/v1/sync/cluster_office/information/branches"
      Rails.logger.info "Syncing branch information from #{url_branches}"
      print "."

      http = Curl::Easy.http_post(url_branches, { api_key: API_KEY, branch_ids: branch_ids }.to_json) do |curl|
        curl.headers['Accept'] = 'application/json'
        curl.headers['Content-Type'] = 'application/json'
        curl.headers['Api-Version'] = '2.2'
      end
      response_code = http.response_code
      
      if response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."
        data.each do |o|
          Rails.logger.info "Syncing Branch #{o[:name]}"
          print "."

          branch = Branch.where(id: o[:id]).first

          if branch.blank?
            Rails.logger.info "Creating new branch #{o[:name]}"
            print "."

            Branch.new do |br|
              br.id = o[:id]
              br.name = o[:name]
              br.abbreviation = o[:abbreviation]
              br.code = o[:code]
              br.address = o[:address]
              br.cluster = Cluster.find(o[:cluster_id])
              br.member_counter = 0
              br.bank = Bank.find(o[:bank_id])

              if br.valid?
                br.save!
                Rails.logger.info "Successfully created new branch: #{br.to_s}"
              else
                Rails.logger.error "Error in creating branch: #{br.error.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating branch #{o[:id]}"
            print "."

            branch.update!(
              name: o[:name],
              code: o[:code],
              abbreviation: o[:abbreviation],
              address: o[:address],
              cluster_id: o[:cluster_id],
              bank_id: o[:bank_id]
            )

            Rails.logger.info "Updated branch #{branch.to_s}"
          end

          Rails.logger.info "Deleting unwanted data"
          print "."
          ids = (data.map { |o| o[:id] }).uniq
          Branch.where.not(id: ids).destroy_all
        end
      elsif response_code == 404
        Rails.logger.error "404: Not Found - #{url_branches}"
        print "E"
      else 
        Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end

      # sync centers
      if response_code == 200
        Branch.all.each do |branch|
          url_centers = "#{MASTER_SERVER}/api/v1/sync/cluster_office/information/branch/#{branch.id}/centers"
          http = Curl.post(url_centers, PARAMS)
          Rails.logger.info "Starting sync of centers for branch #{branch.to_s} #{branch.id}"
          print "."

          if http.response_code == 200
            data = JSON.parse(http.body_str, symbolize_names: true)[:data]
            Rails.logger.debug data

            ids = (data.map { |o| o[:id] }).uniq
            Rails.logger.debug "IDs: #{ids.inspect}"
            print "."

            data.each do |o|
              center = Center.where(uuid: o[:uuid]).first

              if center.blank?
                Rails.logger.info "Creating new center #{o[:name]}"
                print "."

                Center.new do |cr|
                  cr.id = o[:id]
                  cr.name = o[:name]
                  cr.abbreviation = o[:abbreviation]
                  cr.code = o[:code]
                  cr.address = o[:address]
                  cr.branch = Branch.find(o[:branch_id])
                  cr.meeting_day = o[:meeting_day]
                  cr.officer_in_charge = o[:officer_in_charge]
                  cr.uuid = o[:uuid]

                  if cr.valid?
                    cr.save!
                    Rails.logger.info "Successfully created new center: #{cr.to_s}"
                  else
                    Rails.l;ogger.error "Error in creating center: #{cr.error.messages}"
                    print "E"
                  end
                end
              else
                Rails.logger.info "Updating center #{o[:id]}"
                print "."

                center.update!(
                  name: o[:name],
                  abbreviation: o[:abbreviation],
                  code: o[:code],
                  address: o[:address],
                  branch_id: o[:branch_id],
                  meeting_day: o[:meeting_day],
                  officer_in_charge: o[:officer_in_charge],
                  uuid: o[:uuid]
                )

                Rails.logger.info "Updated center #{center.to_s}"
              end
            end
          elsif http.response_code == 404
            Rails.logger.error "404: Not Found - #{url_centers}"
            print "E"
          else
            Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
            print "E"
          end
        end
      end
    end

    def self.sync_clusters!
      Rails.logger.info "Syncing clusters"
      print "."

      http = Curl.post(MASTER_URL_SYNC_CLUSTERS, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info "Syncing Cluster #{o[:name]}"
          print "."

          cluster = Cluster.where(id: o[:id]).first

          if cluster.blank?
            Rails.logger.info "Creating new cluster #{o[:name]}"
            print "."

            Cluster.new do |cl|
              cl.id = o[:id]
              cl.abbreviation = o[:abbreviation]
              cl.name = o[:name]
              cl.code = o[:code]
              cl.address = o[:address]
              cl.area = Area.find(o[:area_id])

              if cl.valid?
                cl.save!
                Rails.logger.info "Successfully created new cluster: #{cl.to_s}"
              else
                Rails.logger.error "Error in creating cluster: #{cl.errors.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating cluster #{o[:id]}"
            print "."

            cluster.update!(
              abbreviation: o[:abbreviation],
              code: o[:code],
              address: o[:address],
              area_id: o[:area_id]
            )

            Rails.logger.info "Updated cluster #{cluster.to_s}"
          end

          Rails.logger.info "Deleting unwanted data"
          print "."
          ids = (data.map { |o| o[:id] }).uniq
          Cluster.where.not(id: ids).destroy_all
        end
      elsif http.response_code == 404
        Rails.logger.error "404: Not Found - #{MASTER_URL_SYNC_CLUSTERS}"
        print "E"
      else Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end
    end

    def self.sync_areas!
      Rails.logger.info "Syncing areas"
      print "."

      http = Curl.post(MASTER_URL_SYNC_AREAS, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info "Syncing Area #{o[:name]}"
          print "."

          area = Area.where(id: o[:id]).first

          if area.blank?
            Rails.logger.info "Creating new area #{o[:name]}"
            print "."

            Area.new do |a|
              a.id = o[:id]
              a.name = o[:name]
              a.code = o[:code]
              a.abbreviation = o[:abbreviation]
              a.address = o[:address]

              if a.valid?
                a.save!
                Rails.logger.info "Successfully created new area: #{a.to_s}"
              else
                Rails.l;ogger.error "Error in creating area: #{a.error.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating area #{o[:id]}"
            print "."

            area.update!(
              id: o[:id],
              name: o[:name],
              code: o[:code],
              abbreviation: o[:abbreviation],
              address: o[:address]
            )

            Rails.logger.info "Updated area #{area.to_s}"
          end

          Rails.logger.info "Deleting unwanted data"
          print "."
          ids = (data.map { |o| o[:id] }).uniq
          Area.where.not(id: ids).destroy_all
        end

        Rails.logger.info "Done syncing areas..."
        print "."
      elsif http.response_code == 404
        Rails.logger.error "404: Not Found - #{MASTER_URL_SYNC_AREAS}"
        print "E"
      else Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end
    end
  end
end
