module Sync
  class SyncSurveys
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing Surveys"
      print "."
      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:title]}")
          print "."

          survey = Survey.where(id: o[:id]).first

          if survey.blank?
            Rails.logger.info("Survey #{o[:id]} not found. Creating new one.")
            print "."

            Survey.new do |s|
              s.id = o[:id]
              s.uuid = o[:uuid]
              s.title = o[:title]
              s.is_default = o[:is_default]

              if s.valid?
                Rails.logger.info("Saving new survey #{o[:id]}")
                print "."
                s.save!
              else
                Rails.logger.error("Error in creating survey")
                print "E"
              end
            end
          else
           Rails.logger.info("Updating survey #{o[:id]}")
            print "."

            survey.update!(
              uuid: o[:uuid],
              title: o[:title],
              is_default: o[:is_default]
            )
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        Survey.where.not(id: ids).destroy_all
        
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end

