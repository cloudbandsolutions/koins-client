module Sync
  class SyncLoanProducts
    MASTER_SERVER = Settings.master_server
    API_KEY = Settings.api_key
    PARAMS = { api_key: API_KEY }

    # URL constants
    MASTER_URL_SYNC_LOAN_PRODUCTS = "#{MASTER_SERVER}/api/v1/sync/finance/loans/loan_products"
    MASTER_URL_SYNC_LOAN_PRODUCT_TYPES = "#{MASTER_SERVER}/api/v1/sync/finance/loans/loan_product_types"
    MASTER_URL_SYNC_LOAN_PRODUCT_DEPENDENCIES = "#{MASTER_SERVER}/api/v1/sync/finance/loans/loan_product_dependencies"
    MASTER_URL_SYNC_LOAN_DEDUCTIONS = "#{MASTER_SERVER}/api/v1/sync/finance/loans/loan_deductions"
    MASTER_URL_SYNC_LOAN_DEDUCTION_ENTRIES = "#{MASTER_SERVER}/api/v1/sync/finance/loans/loan_deduction_entries"
    MASTER_URL_SYNC_LOAN_INSURANCE_DEDUCTION_ENTRIES = "#{MASTER_SERVER}/api/v1/sync/finance/loans/loan_insurance_deduction_entries"
    MASTER_URL_SYNC_LOAN_PRODUCT_EP_CYCLE_COUNT_MAX_AMOUNTS = "#{MASTER_SERVER}/api/v1/sync/finance/loans/loan_product_ep_cycle_count_max_amounts"

    def self.sync_loan_products!
      SyncLoanProducts.setup!

      Rails.logger.info "Syncing loan products"
      print "."

      # LoanProduct
      http = Curl.post(MASTER_URL_SYNC_LOAN_PRODUCTS, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data
        
        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info "Syncing LoanProduct #{o[:name]}"
          Rails.logger.debug "o: #{o.inspect}"
          print "."

          loan_product = LoanProduct.where(id: o[:id]).first

          if loan_product.blank?
            Rails.logger.info "Creating new loan_product #{o[:name]}"
            Rails.logger.debug "loan product blank o: #{o.inspect}"
            print "."

            LoanProduct.new do |lp|
              lp.id = o[:id]
              lp.name = o[:name]
              lp.code = o[:code]
              lp.insured = o[:insured]
              lp.description = o[:description]
              lp.amount_released_accounting_code = AccountingCode.find(o[:amount_released_accounting_code_id])
              lp.accounting_code_transaction = AccountingCode.find(o[:accounting_code_transaction_id])
              lp.savings_type = SavingsType.find(o[:savings_type_id])
              lp.default_maintaining_balance_val = o[:default_maintaining_balance_val]
              lp.interest_accounting_code = AccountingCode.find(o[:interest_accounting_code_id])
              lp.accounting_code_processing_fee = AccountingCode.find(o[:accounting_code_processing_fee_id])
              lp.next_application_interval = o[:next_application_interval]
              lp.priority = o[:priority]
              lp.is_entry_point = o[:is_entry_point]
              lp.maintaining_balance_exception = o[:maintaining_balance_exception]
              lp.maintaining_balance_exception_minimum_amount = o[:maintaining_balance_exception_minimum_amount]
              lp.waive_processing_fee_for_existing_entry_point_loans = o[:waive_processing_fee_for_existing_entry_point_loans]
              lp.maintaining_balance_exception_cycle_count = o[:maintaining_balance_exception_cycle_count]
              lp.max_loan_amount = o[:max_loan_amount]
              lp.writeoff_dr_accounting_code = AccountingCode.find(o[:writeoff_cr_accounting_code_id])
              lp.writeoff_cr_accounting_code = AccountingCode.find(o[:writeoff_cr_accounting_code_id])
              lp.always_have_processing_fee = o[:always_have_processing_fee]
              lp.use_amount_released_accounting_code = o[:use_amount_released_accounting_code]
              lp.overridable_interest_rate = o[:overridable_interest_rate]
              lp.default_annual_interest_rate = o[:default_annual_interest_rate]
              lp.uuid = o[:uuid]

              if lp.valid?
                lp.save!
                Rails.logger.info "Successfully created new loan_product: #{lp.to_s}"
                print "."
              else
                Rails.logger.error "Error in creating loan_product: #{lp.errors.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating loan_product #{o[:id]}"
            print "."

            loan_product.update!(
              name: o[:name],
              code: o[:code],
              insured: o[:insured],
              description: o[:description],
              amount_released_accounting_code_id: o[:amount_released_accounting_code_id],
              accounting_code_transaction_id: o[:accounting_code_transaction_id],
              savings_type_id: o[:savings_type_id],
              default_maintaining_balance_val: o[:default_maintaining_balance_val],
              interest_accounting_code_id: o[:interest_accounting_code_id],
              accounting_code_processing_fee_id: o[:accounting_code_processing_fee_id],
              next_application_interval: o[:next_application_interval],
              priority: o[:priority],
              is_entry_point: o[:is_entry_point],
              maintaining_balance_exception: o[:maintaining_balance_exception],
              maintaining_balance_exception_minimum_amount: o[:maintaining_balance_exception_minimum_amount],
              waive_processing_fee_for_existing_entry_point_loans: o[:waive_processing_fee_for_existing_entry_point_loans],
              maintaining_balance_exception_cycle_count: o[:maintaining_balance_exception_cycle_count],
              max_loan_amount: o[:max_loan_amount],
              writeoff_dr_accounting_code_id: o[:writeoff_cr_accounting_code_id],
              writeoff_cr_accounting_code_id: o[:writeoff_cr_accounting_code_id],
              always_have_processing_fee: o[:always_have_processing_fee],
              use_amount_released_accounting_code: o[:use_amount_released_accounting_code],
              overridable_interest_rate: o[:overridable_interest_rate],
              default_annual_interest_rate: o[:default_annual_interest_rate]
            )

            Rails.logger.info "Updated loan_product #{loan_product.to_s}"
          end
        end

        Rails.logger.info "Deleting unwanted data"
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        LoanProduct.where.not(id: ids).destroy_all

      elsif http.response_code == 404
        Rails.logger.error "404: Not Found - #{MASTER_URL_SYNC_LOAN_PRODUCTS}"
        print "E"
      else
        Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end

      # LoanProductType
      http = Curl.post(MASTER_URL_SYNC_LOAN_PRODUCT_TYPES, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data
        
        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info "Syncing LoanProductType #{o[:name]}"
          print "."

          loan_product_type = LoanProductType.where(id: o[:id]).first

          if loan_product_type.blank?
            Rails.logger.info "Creating new loan_product_type #{o[:name]}"
            print "."

            LoanProductType.new do |lpt|
              lpt.id = o[:id]
              lpt.name = o[:name]
              lpt.loan_product = LoanProduct.find(o[:loan_product_id])
              lpt.processing_fee_15 = o[:processing_fee_15]
              lpt.processing_fee_25 = o[:processing_fee_25]
              lpt.processing_fee_35 = o[:processing_fee_35]
              lpt.processing_fee_50 = o[:processing_fee_50]
              lpt.fixed_processing_fee = o[:fixed_processing_fee]
              lpt.interest_rate = o[:interest_rate]

              if lpt.valid?
                lpt.save!
                Rails.logger.info "Successfully created new loan_product_type: #{lpt.to_s}"
                print "."
              else
                Rails.logger.error "Error in creating loan_product_type: #{lpt.errors.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating loan_product_type #{o[:id]}"
            print "."

            loan_product_type.update!(
              id: o[:id],
              name: o[:name],
              loan_product: o[:loan_product_id],
              processing_fee_15: o[:processing_fee_15],
              processing_fee_25: o[:processing_fee_25],
              processing_fee_35: o[:processing_fee_35],
              processing_fee_50: o[:processing_fee_50],
              fixed_processing_fee: o[:fixed_processing_fee],
              interest_rate: o[:interest_rate]
            )

            Rails.logger.info "Updated loan_product_type #{loan_product_type.to_s}"
          end
        end

        Rails.logger.info "Deleting unwanted data"
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        LoanProductType.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error "404: Not Found - #{MASTER_URL_SYNC_LOAN_PRODUCT_TYPES}"
        print "E"
      else
        Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end

      # LoanProductDependency
      http = Curl.post(MASTER_URL_SYNC_LOAN_PRODUCT_DEPENDENCIES, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data
        
        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info "Syncing LoanProductDependency #{o[:name]}"
          print "."

          loan_product_dependency = LoanProductDependency.where(id: o[:id]).first

          if loan_product_dependency.blank?
            Rails.logger.info "Creating new loan_product_dependency #{o[:name]}"
            print "."

            LoanProductDependency.new do |lpd|
              lpd.id = o[:id]
              lpd.cycle_count = o[:cycle_count]
              lpd.dependent_loan_product = LoanProduct.find(o[:dependent_loan_product_id])
              lpd.loan_product = LoanProduct.find(o[:loan_product_id])

              if lpd.valid?
                lpd.save!
                Rails.logger.info "Successfully created new loan_product_dependency: #{lpd.to_s}"
                print "."
              else
                Rails.logger.error "Error in creating loan_product_dependency: #{lpd.errors.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating loan_product_dependency #{o[:id]}"
            print "."

            loan_product_dependency.update!(
              id: o[:id],
              cycle_count: o[:cycle_count],
              dependent_loan_product_id: o[:dependent_loan_product_id],
              loan_product_id: o[:loan_product_id]
            )

            Rails.logger.info "Updated loan_product_dependency #{loan_product_dependency.to_s}"
          end
        end

        Rails.logger.info "Deleting unwanted data"
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        LoanProductDependency.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error "404: Not Found - #{MASTER_URL_SYNC_LOAN_PRODUCT_DEPENDENCIES}"
        print "E"
      else
        Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end

      # LoanDeduction
      http = Curl.post(MASTER_URL_SYNC_LOAN_DEDUCTIONS, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data
        
        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info "Syncing LoanDeduction #{o[:name]}"
          print "."

          loan_deduction = LoanDeduction.where(id: o[:id]).first

          if loan_deduction.blank?
            Rails.logger.info "Creating new loan_deduction #{o[:name]}"
            print "."

            LoanDeduction.new do |ld|
              ld.id = o[:id]
              ld.name = o[:name]
              ld.code = o[:code]
              ld.is_one_time_fee = o[:is_one_time_fee]

              if ld.valid?
                ld.save!
                Rails.logger.info "Successfully created new loan_deduction: #{ld.to_s}"
                print "."
              else
                Rails.logger.error "Error in creating loan_deduction: #{ld.errors.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating loan_deduction #{o[:id]}"
            print "."

            loan_deduction.update!(
              id: o[:id],
              name: o[:name],
              code: o[:code],
              is_one_time_fee: o[:is_one_time_fee]
            )

            Rails.logger.info "Updated loan_deduction #{loan_deduction.to_s}"
          end
        end

        Rails.logger.info "Deleting unwanted data"
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        LoanDeduction.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error "404: Not Found - #{MASTER_URL_SYNC_LOAN_DEDUCTIONS}"
        print "E"
      else
        Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end

      # LoanDeductionEntry
      http = Curl.post(MASTER_URL_SYNC_LOAN_DEDUCTION_ENTRIES, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data
        
        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info "Syncing LoanDeductionEntry #{o[:name]}"
          Rails.logger.debug "#{o.inspect}"
          print "."

          loan_deduction_entry = LoanDeductionEntry.where(id: o[:id]).first

          if loan_deduction_entry.blank?
            Rails.logger.info "Creating new loan_deduction_entry #{o[:name]}"
            Rails.logger.debug "#{o.inspect}"
            print "."

            LoanDeductionEntry.new do |lde|
              lde.id = o[:id]
              lde.loan_product = LoanProduct.find(o[:loan_product_id])
              lde.loan_deduction = LoanDeduction.find(o[:loan_deduction_id])
              lde.val = o[:val]
              lde.is_percent = o[:is_percent]
              lde.accounting_code = AccountingCode.find(o[:accounting_code_id])
              lde.is_one_time_deduction = o[:is_one_time_deduction]
              lde.use_term_map = o[:use_term_map]
              lde.t_val_15 = o[:t_val_15]
              lde.t_val_25 = o[:t_val_25]
              lde.t_val_50 = o[:t_val_50]
              lde.t_val_35 = o[:t_val_35]
              lde.use_for_special_loan_fund  = o[:use_for_special_loan_fund]
              lde.skip_for_special_loan_fund = o[:skip_for_special_loan_fund]

              if lde.valid?
                lde.save!
                Rails.logger.info "Successfully created new loan_deduction_entry: #{lde.to_s}"
                print "."
              else
                Rails.logger.error "Error in creating loan_deduction_entry: #{lde.errors.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating loan_deduction_entry #{o[:id]}"
            print "."

            loan_deduction_entry.update!(
              id: o[:id],
              loan_product_id: o[:loan_product_id],
              loan_deduction_id: o[:loan_deduction_id],
              val: o[:val],
              is_percent: o[:is_percent],
              accounting_code_id: o[:accounting_code_id],
              is_one_time_deduction: o[:is_one_time_deduction],
              use_term_map: o[:use_term_map],
              t_val_15: o[:t_val_15],
              t_val_25: o[:t_val_25],
              t_val_50: o[:t_val_50],
              t_val_35: o[:t_val_35],
              use_for_special_loan_fund: o[:use_for_special_loan_fund],
              skip_for_special_loan_fund: o[:skip_for_special_loan_fund]
            )

            Rails.logger.info "Updated loan_deduction_entry #{loan_deduction_entry.to_s}"
          end
        end

        Rails.logger.info "Deleting unwanted data"
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        LoanDeductionEntry.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error "404: Not Found - #{MASTER_URL_SYNC_LOAN_DEDUCTION_ENTRIES}"
        print "E"
      else
        Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end

      # LoanInsuranceDeductionEntry
      http = Curl.post(MASTER_URL_SYNC_LOAN_INSURANCE_DEDUCTION_ENTRIES, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data
        
        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info "Syncing LoanInsuranceDeductionEntry #{o[:name]}"
          print "."

          loan_insurance_deduction_entry = LoanInsuranceDeductionEntry.where(id: o[:id]).first

          if loan_insurance_deduction_entry.blank?
            Rails.logger.info "Creating new loan_insurance_deduction_entry #{o[:name]}"
            print "."

            LoanInsuranceDeductionEntry.new do |lide|
              lide.id = o[:id]
              lide.loan_product = LoanProduct.find(o[:loan_product_id])
              lide.insurance_type = InsuranceType.find(o[:insurance_type_id])
              lide.val = o[:val]
              lide.is_percent = o[:is_percent]
              lide.accounting_code = AccountingCode.find(o[:accounting_code_id])
              lide.is_one_time_deduction = o[:is_one_time_deduction]

              if lide.valid?
                lide.save!
                Rails.logger.info "Successfully created new loan_insurance_deduction_entry: #{lide.to_s}"
                print "."
              else
                Rails.logger.error "Error in creating loan_insurance_deduction_entry: #{lide.errors.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating loan_insurance_deduction_entry #{o[:id]}"
            print "."

            loan_insurance_deduction_entry.update!(
              id: o[:id],
              loan_product_id: o[:loan_product_id],
              insurance_type_id: o[:insurance_type_id],
              val: o[:val],
              is_percent: o[:is_percent],
              accounting_code_id: o[:accounting_code_id],
              is_one_time_deduction: o[:is_one_time_deduction]
            )

            Rails.logger.info "Updated loan_insurance_deduction_entry #{loan_insurance_deduction_entry.to_s}"
          end
        end

        Rails.logger.info "Deleting unwanted data"
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        LoanInsuranceDeductionEntry.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error "404: Not Found - #{MASTER_URL_SYNC_LOAN_INSURANCE_DEDUCTION_ENTRIES}"
        print "E"
      else
        Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end

      # LoanProductEpCycleCountMaxAmount
      http = Curl.post(MASTER_URL_SYNC_LOAN_PRODUCT_EP_CYCLE_COUNT_MAX_AMOUNTS, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data
        
        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info "Syncing LoanProductEpCycleCountMaxAmount #{o[:name]}"
          print "."

          loan_product_ep_cycle_count_max_amount = LoanProductEpCycleCountMaxAmount.where(id: o[:id]).first

          if loan_product_ep_cycle_count_max_amount.blank?
            Rails.logger.info "Creating new loan_product_ep_cycle_count_max_amount #{o[:name]}"
            print "."

            LoanProductEpCycleCountMaxAmount.new do |lpeccma|
              lpeccma.id = o[:id]
              lpeccma.loan_product = LoanProduct.find(o[:loan_product_id])
              lpeccma.cycle_count = o[:cycle_count]
              lpeccma.max_amount = o[:max_amount]

              if lpeccma.valid?
                lpeccma.save!
                Rails.logger.info "Successfully created new loan_product_ep_cycle_count_max_amount: #{lpeccma.to_s}"
                print "."
              else
                Rails.logger.error "Error in creating loan_product_ep_cycle_count_max_amount: #{lpeccma.errors.messages}"
                print "E"
              end
            end
          else
            Rails.logger.info "Updating loan_product_ep_cycle_count_max_amount #{o[:id]}"
            print "."

            loan_product_ep_cycle_count_max_amount.update!(
              id: o[:id],
              loan_product_id: o[:loan_product_id],
              cycle_count: o[:cycle_count],
              max_amount: o[:max_amount]
            )

            Rails.logger.info "Updated loan_product_ep_cycle_count_max_amount #{loan_product_ep_cycle_count_max_amount.to_s}"
          end
        end

        Rails.logger.info "Deleting unwanted data"
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        LoanProductEpCycleCountMaxAmount.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error "404: Not Found - #{MASTER_URL_SYNC_LOAN_PRODUCT_EP_CYCLE_COUNT_MAX_AMOUNTS}"
        print "E"
      else
        Rails.logger.error "Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}"
        print "E"
      end
    end

    def self.setup!
      Rails.logger.info "Destroying all loan product related records"
      LoanDeductionEntry.destroy_all
      LoanInsuranceDeductionEntry.destroy_all
      LoanProductType.destroy_all
      LoanProductDependency.destroy_all
      LoanProductEpCycleCountMaxAmount.destroy_all
      LoanProduct.destroy_all
      Rails.logger.info "Done destroying all loan product related records"
    end
  end
end
