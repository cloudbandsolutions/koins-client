module Sync
  class SyncLoanProductMembershipPayments
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing loan product membership payments"
      print "."
      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o}")
          print "."

          loan_product_membership_payment = LoanProductMembershipPayment.where(id: o[:id]).first
          loan_product = LoanProduct.where(id: o[:loan_product_id]).first
          membership_type = MembershipType.where(id: o[:membership_type_id]).first

          if loan_product_membership_payment.blank?
            Rails.logger.info("Loan product membership payment #{o[:id]} not found. Creating new one.")
            print "."

            LoanProductMembershipPayment.new do |loan_product_membership_payment|

              if loan_product.blank?
                Rails.logger.error "Loan product #{o[:loan_product_id]} not found"
                print "E"
              else
                loan_product_membership_payment.loan_product = loan_product
              end

              if membership_type.blank?
                Rails.logger.error "Membership type #{o[:membership_type_id]} not found"
                print "E"
              else
                loan_product_membership_payment.membership_type = membership_type
              end

              loan_product_membership_payment.id = o[:id]
              loan_product_membership_payment.loan_product = loan_product
              loan_product_membership_payment.membership_type = membership_type

              if loan_product_membership_payment.valid?
                Rails.logger.info("Saving new loan_product_membership_payment #{o[:id]}")
                print "."
                loan_product_membership_payment.save!
              else
                Rails.logger.error("Error in creating loan_product_membership_payment account")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating loan product membership payment #{o[:id]}")
            print "."

            if loan_product_membership_payment.update(
                  loan_product: loan_product,
                  membership_type: membership_type
                )
              Rails.logger.info "Loan product membership type #{o[:id]} updated"
            else
              Rails.logger.error "Loan product membership type #{o[:id]} error in updating"
              print "E"
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        LoanProductMembershipPayment.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
