module Sync
  class SyncAccounts
    MASTER_SERVER = Settings.master_server
    API_KEY = Settings.api_key
    PARAMS = { api_key: API_KEY }
   
    # URL constants
    MASTER_URL_SYNC_ACCOUNT_TYPES = "#{MASTER_SERVER}/api/v1/sync/finance/accounts/account_types"

    def self.setup!
      SavingsType.destroy_all
      InsuranceType.destroy_all
      EquityType.destroy_all
    end

    def self.sync_account_types!
      self.setup!

      Rails.logger.info "Syncing account types"
      print "."
      url = MASTER_URL_SYNC_ACCOUNT_TYPES
      http = Curl.post(url, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        savings_types = data[:savings_types]
        savings_type_ids = []
        savings_types.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          savings_type = SavingsType.where(id: o[:id]).first

          if savings_type.blank?
            Rails.logger.info("Creating new savings type #{o[:name]}")
            print "."

            SavingsType.new do |st|
              st.id = o[:id]
              st.name = o[:name]
              st.code = o[:code]
              st.is_default = o[:is_default]
              st.withdraw_accounting_code_id = o[:withdraw_accounting_code_id]
              st.deposit_accounting_code_id = o[:deposit_accounting_code_id]
              st.monthly_interest_rate = o[:monthly_interest_rate]
              st.monthly_tax_rate = o[:monthly_tax_rate]
              st.expense_accounting_code_id = o[:expense_accounting_code_id]
              st.tax_accounting_code_id = o[:tax_accounting_code_id]
              st.interest_accounting_code_id = o[:interest_accounting_code_id]
              st.annual_interest_rate = o[:annual_interest_rate]

              if st.valid?
                st.save!
                Rails.logger.info("Successfully created new savings type: #{st.to_s}")
                print "."

                savings_type_ids << st.id
              else
                Rails.logger.error("Error in creating savings type: #{st.errors.messages}")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating savings type #{o[:id]}")
            print "."

            savings_type.update!(
              name: o[:name],
              code: o[:code],
              is_default: o[:is_default],
              withdraw_accounting_code_id: o[:withdraw_accounting_code_id],
              deposit_accounting_code_id: o[:deposit_accounting_code_id],
              monthly_interest_rate: o[:monthly_interest_rate],
              monthly_tax_rate: o[:monthly_tax_rate],
              expense_accounting_code_id: o[:expense_accounting_code_id],
              tax_accounting_code_id: o[:tax_accounting_code_id],
              interest_accounting_code_id: o[:interest_accounting_code_id],
              annual_interest_rate: o[:annual_interest_rate]
            )

            savings_type_ids << savings_type.id

            Rails.logger.info("Updated savings type #{savings_type.to_s}")
          end
        end

        invalid_savings_types = SavingsType.where.not(id: savings_type_ids)
        Rails.logger.info "Deleting savings_types #{invalid_savings_types.pluck(:id)}"
        print "."
        invalid_savings_types.destroy_all

        insurance_types = data[:insurance_types]
        insurance_type_ids = []
        insurance_types.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          insurance_type = InsuranceType.where(id: o[:id]).first

          if insurance_type.blank?
            Rails.logger.info("Creating new insurance type #{o[:name]}")
            print "."

            InsuranceType.new do |st|
              st.id = o[:id]
              st.name = o[:name]
              st.code = o[:code]
              st.default_periodic_payment = o[:default_periodic_payment]
              st.is_default = o[:is_default]
              st.withdraw_accounting_code_id = o[:withdraw_accounting_code_id]
              st.deposit_accounting_code_id = o[:deposit_accounting_code_id]
              st.monthly_interest_rate = o[:monthly_interest_rate]
              st.monthly_tax_rate = o[:monthly_tax_rate]
              st.use_resign_rules = o[:use_resign_rules]
              st.resign_rule_num_years  = o[:resign_rule_num_years]
              st.resign_rule_withdrawal_percentage = o[:resign_rule_withdrawal_percentage]

              if st.valid?
                st.save!
                Rails.logger.info("Successfully created new insurance type: #{st.to_s}")
                print "."

                insurance_type_ids << st.id
              else
                Rails.logger.error("Error in creating insurance type: #{st.errors.messages}")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating insurance type #{o[:id]}")
            print "."

            insurance_type.update!(
              name: o[:name],
              code: o[:code],
              default_periodic_payment: o[:default_periodic_payment],
              is_default: o[:is_default],
              withdraw_accounting_code_id: o[:withdraw_accounting_code_id],
              deposit_accounting_code_id: o[:deposit_accounting_code_id],
              fund_transfer_withdraw_accounting_code_id: o[:fund_transfer_withdraw_accounting_code_id],
              fund_transfer_deposit_accounting_code_id: o[:fund_transfer_deposit_accounting_code_id],
              monthly_interest_rate: o[:monthly_interest_rate],
              monthly_tax_rate: o[:monthly_tax_rate],
              use_resign_rules: o[:use_resign_rules],
              resign_rule_num_years: o[:resign_rule_num_years],
              resign_rule_withdrawal_percentage: o[:resign_rule_withdrawal_percentage]
            )

            Rails.logger.info("Updated insurance type #{insurance_type.to_s}")
            insurance_type_ids << insurance_type.id
          end
        end

        invalid_insurance_types = InsuranceType.where.not(id: insurance_type_ids)
        Rails.logger.info "Deleting insurance_types #{invalid_insurance_types.pluck(:id)}"
        print "."
        invalid_insurance_types.destroy_all

        equity_types = data[:equity_types]
        equity_type_ids = []

        equity_types.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          equity_type = EquityType.where(id: o[:id]).first

          if equity_type.blank?
            Rails.logger.info("Creating new equity type #{o[:name]}")
            print "."

            EquityType.new do |st|
              st.id = o[:id]
              st.name = o[:name]
              st.code = o[:code]
              st.is_default = o[:is_default]
              st.withdraw_accounting_code_id = o[:withdraw_accounting_code_id]
              st.deposit_accounting_code_id = o[:deposit_accounting_code_id]
              st.monthly_interest_rate = o[:monthly_interest_rate]
              st.monthly_tax_rate = o[:monthly_tax_rate]

              if st.valid?
                st.save!
                Rails.logger.info("Successfully created new equity type: #{st.to_s}")
                print "."

                equity_type_ids << st.id
              else
                Rails.logger.error("Error in creating equity type: #{st.errors.messages}")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating equity type #{o[:id]}")
            print "."

            equity_type.update!(
              name: o[:name],
              code: o[:code],
              is_default: o[:is_default],
              withdraw_accounting_code_id: o[:withdraw_accounting_code_id],
              deposit_accounting_code_id: o[:deposit_accounting_code_id],
              monthly_interest_rate: o[:monthly_interest_rate],
              monthly_tax_rate: o[:monthly_tax_rate],
            )

            Rails.logger.info("Updated equity type #{equity_type.to_s}")

            equity_type_ids << equity_type.id
          end
        end

        invalid_equity_types = EquityType.where.not(id: equity_type_ids)
        Rails.logger.info "Deleting equity_types #{invalid_equity_types.pluck(:id)}"
        print "."
        invalid_equity_types.destroy_all
      else
        Rails.logger.error "Error in connecting to endpoint"
        print "E"
      end
    end
  end
end
