module Sync
  class SyncLoanInsuranceTypes
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing Loan Insurance Types"
      print "."

      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]}")
          print "."

          loan_insurance_type = LoanInsuranceType.where(id: o[:id]).first

          if loan_insurance_type.blank?
            Rails.logger.info("Loan Insurance Type #{o[:name]} not found. Creating new one.")
            print "."

            LoanInsuranceType.new do |lit|
              lit.id = o[:id]
              lit.name = o[:name]
              lit.accounting_code_id = o[:accounting_code_id]
              lit.cash_debit_accounting_code_id = o[:cash_debit_accounting_code_id]
              lit.cash_credit_accounting_code_id = o[:cash_credit_accounting_code_id]
              lit.unremitted_debit_accounting_code_id = o[:unremitted_debit_accounting_code_id]
              lit.unremitted_credit_accounting_code_id = o[:unremitted_credit_accounting_code_id]
              lit.accounting_fund_id = o[:accounting_fund_id]

              if lit.valid?
                Rails.logger.info("Saving new loan insurance type #{o[:id]}")
                print "."
                lit.save!
              else
                Rails.logger.error("Error in creating loan insurance type")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating loan insurance type #{o[:id]}")
            print "."

            loan_insurance_type.update!(
              name: o[:name],
              accounting_code_id: o[:accounting_code_id],
              cash_debit_accounting_code_id: o[:cash_debit_accounting_code_id],
              cash_credit_accounting_code_id: o[:cash_credit_accounting_code_id],
              unremitted_debit_accounting_code_id: o[:unremitted_debit_accounting_code_id],
              unremitted_credit_accounting_code_id: o[:unremitted_credit_accounting_code_id],
              accounting_fund_id: o[:accounting_fund_id]
            )
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        LoanInsuranceType.where.not(id: ids).destroy_all

      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
