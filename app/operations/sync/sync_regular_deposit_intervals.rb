module Sync
  class SyncRegularDepositIntervals
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing Regular Deoposit Intervals"
      print "."
      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]}")
          print "."

          regular_deposit_interval = RegularDepositInterval.where(id: o[:id]).first

          if regular_deposit_interval.blank?
            Rails.logger.info("Regular Deposit Interval #{o[:id]} not found. Creating new one.")
            print "."

            RegularDepositInterval.new do |rdi|
              rdi.id = o[:id]
              rdi.name = o[:name]
              rdi.interval = o[:interval]

              if rdi.valid?
                Rails.logger.info("Saving new regular deposit interval #{o[:id]}")
                print "."
                rdi.save!
              else
                Rails.logger.error("Error in creating regular deposit interval")
                print "E"
              end
            end
          else
           Rails.logger.info("Updating regular deposit interval #{o[:id]}")
            print "."

            regular_deposit_interval.update!(
              name: o[:name],
              interval: o[:interval]
            )
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        RegularDepositInterval.where.not(id: ids).destroy_all
        
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end

