module Sync
  class SyncRegularDepositAmounts
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing Regular Deposit Amounts"
      print "."
      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:min_amount]} #{o[:max_amount]} #{o[:deposit_amount]}")
          print "."

          regular_deposit_amount = RegularDepositAmount.where(id: o[:id]).first

          if regular_deposit_amount.blank?
            Rails.logger.info("Regular Deposit Amount #{o[:id]} not found. Creating new one.")
            print "."

            RegularDepositAmount.new do |rda|
              rda.id = o[:id]
              rda.min_amount = o[:min_amount]
              rda.max_amount = o[:max_amount]
              rda.deposit_amount = o[:deposit_amount]

              regular_deposit_interval = RegularDepositInterval.where(id: o[:regular_deposit_interval_id]).first
              if regular_deposit_interval.blank?
                Rails.logger.error "Regular Deposit Interval #{o[:regular_deposit_interval_id]} not found"
                print "E"
              else
                rda.regular_deposit_interval = regular_deposit_interval
              end              


              if rda.valid?
                Rails.logger.info("Saving new Regular Deposit Amount #{o[:id]}")
                print "."
                rda.save!
              else
                Rails.logger.error("Error in creating Regular Deposit Amount")
                print "E"
              end
            end
          else
           Rails.logger.info("Updating Regular Deposit Amount #{o[:id]}")
            print "."

            regular_deposit_interval = RegularDepositInterval.where(id: o[:regular_deposit_interval_id]).first
            if regular_deposit_interval.blank?
              Rails.logger.error "Regular Deposit Interval #{o[:regular_deposit_interval_id]} not found"
              print "E"
            else
              regular_deposit_amount.update!(
                min_amount: o[:min_amount],
                max_amount: o[:max_amount],
                deposit_amount: o[:deposit_amount],
                regular_deposit_interval: regular_deposit_interval
              )
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        RegularDepositAmount.where.not(id: ids).destroy_all
        
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end

