module Sync
  class SyncAccountingFunds
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing Accounting Funds"
      print "."

      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]}")
          print "."

          accounting_fund = AccountingFund.where(id: o[:id]).first

          if accounting_fund.blank?
            Rails.logger.info("Accounting Fund #{o[:name]} not found. Creating new one.")
            print "."

            AccountingFund.new do |af|
              af.id = o[:id]
              af.name = o[:name]

              if af.valid?
                Rails.logger.info("Saving new accounting fund #{o[:id]}")
                print "."
                af.save!
              else
                Rails.logger.error("Error in creating accounting fund")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating accounting fund #{o[:id]}")
            print "."

            accounting_fund.update!(
              name: o[:name]
            )
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        AccountingFund.where.not(id: ids).destroy_all

      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
