module Sync
  class SyncMembershipTypes
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing membership types"
      print "."

      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]}")
          print "."

          membership_type = MembershipType.where(id: o[:id]).first

          dr_accounting_entry = AccountingCode.where(id: o[:dr_accounting_entry_id]).first
          cr_accounting_entry = AccountingCode.where(id: o[:cr_accounting_entry_id]).first

          if membership_type.nil?
            Rails.logger.info("Membership type: #{o[:id]} not found. Creating new one.")
            print "."

            MembershipType.new do |mt|
              mt.id = o[:id]
              mt.name = o[:name]
              mt.fee = o[:fee]
              mt.dr_accounting_entry = dr_accounting_entry
              mt.cr_accounting_entry = cr_accounting_entry
              mt.activated_for_insurance = o[:activated_for_insurance]
              mt.activated_for_loans = o[:activated_for_loans]

              if mt.valid?
                Rails.logger.info("Saving new membership type #{o[:id]}")
                print "."
                mt.save!
              else
                Rails.logger.error("Error in creating membership type: #{mt.errors.messages}")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating membership type #{o[:id]}")
            print "."

            if membership_type.update(
              name: o[:name],
              fee: o[:fee],
              dr_accounting_entry: dr_accounting_entry,
              cr_accounting_entry: cr_accounting_entry,
              activated_for_insurance: o[:activated_for_insurance],
              activated_for_loans: o[:activated_for_loans]
            )
              Rails.logger.info("Successfully updated membership type #{o[:id]}")
              print "."
            else
              Rails.logger.error("Error: #{membership_type.errors.messages}")
              print "E"
            end
          end
        end
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found - #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
