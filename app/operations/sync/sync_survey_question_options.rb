module Sync
  class SyncSurveyQuestionOptions
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing Survey Question Options"
      print "."
      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:option]}")
          print "."

          survey_question_option = SurveyQuestionOption.where(id: o[:id]).first

          if survey_question_option.blank?
            Rails.logger.info("Survey Question Option #{o[:id]} not found. Creating new one.")
            print "."

            SurveyQuestionOption.new do |sqo|
              sqo.id = o[:id]
              
              survey_question = SurveyQuestion.where(id: o[:survey_question_id]).first
              if survey_question.blank?
                Rails.logger.error "Survey Question #{o[:survey_question_id]} not found"
                print "E"
              else
                sqo.survey_question = survey_question
              end
              sqo.option = o[:option]
              sqo.uuid = o[:uuid]
              sqo.priority = o[:priority]
              sqo.score = o[:score]


              if sqo.valid?
                Rails.logger.info("Saving new survey question option #{o[:id]}")
                print "."
                sqo.save!
              else
                Rails.logger.error("Error in creating survey question option")
                print "E"
              end
            end
          else
           Rails.logger.info("Updating survey question option #{o[:id]}")
            print "."

            survey_question = SurveyQuestion.where(id: o[:survey_question_id]).first
            if survey_question.blank?
              Rails.logger.error "Survey Question #{o[:survey_question_id]} not found"
              print "E"
            else
              survey_question_option.update!(
                survey_question: survey_question,
                option: o[:option],
                uuid: o[:uuid],
                priority: o[:priority],
                score: o[:score]
              )
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        SurveyQuestionOption.where.not(id: ids).destroy_all
        
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end

