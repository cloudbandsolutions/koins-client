module Sync
  class SyncMajorAccounts
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing major accounts"
      print "."

      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          major_account = MajorAccount.where(id: o[:id]).first

          if major_account.blank?
            Rails.logger.info("Major account #{o[:name]} not found. Creating new one.")
            print "."

            MajorAccount.new do |ma|
              ma.id = o[:id]
              ma.name = o[:name]
              ma.code = o[:code]
              major_group = MajorGroup.where(id: o[:major_group_id]).first
              if major_group.blank?
                Rails.logger.error "Major account #{o[:major_group_id]} not found"
                print "E"
              else
                ma.major_group = major_group
              end

              if ma.valid?
                Rails.logger.info("Saving new major account #{o[:id]}")
                print "."
                ma.save!
              else
                Rails.logger.error("Error in creating major account")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating major account #{o[:id]}")
            print "."

            major_group = MajorGroup.where(id: o[:major_group_id]).first
            if major_group.blank?
              Rails.logger.error "Major group #{o[:major_group_id]} not found"
              print "E"
            else
              major_account.update!(
                name: o[:name], 
                code: o[:code],
                major_group: major_group
              )
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        MajorAccount.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
