module Sync
  class SyncAccounting
    MASTER_SERVER = Settings.master_server
    API_KEY = Settings.api_key
    PARAMS = { api_key: API_KEY }
    
    # URL constants
    MASTER_URL_SYNC_ACCOUNTING_CODES = "#{MASTER_SERVER}/api/v1/sync/finance/accounting/accounting_codes"

    def self.sync_accounting_codes!
      Rails.logger.info "Syncing accounting codes"
      print "."

      #url = "#{MASTER_SERVER}/api/v1/sync/finance/accounting/accounting_codes"
      url = MASTER_URL_SYNC_ACCOUNTING_CODES
      http = Curl.post(url, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          accounting_code = AccountingCode.where(id: o[:id]).first

          if accounting_code.blank?
            Rails.logger.info("Accounting code #{o[:name]} not found. Creating new one.")
            print "."

            AccountingCode.new do |ac|
              ac.id = o[:id]
              ac.name = o[:name]
              ac.beginning_balance = 0.00
              ac.ending_balance = 0.00
              ac.normal_transaction = o[:normal_transaction]
              ac.main_code = o[:main_code]
              ac.sub_code = o[:sub_code]
              ac.beginning_debit = o[:beginning_debit]
              ac.beginning_credit = o[:beginning_credit]
              ac.ending_debit = o[:ending_debit]
              ac.ending_credit = o[:ending_credit]
              mother_accounting_code = MotherAccountingCode.where(id: o[:mother_accounting_code_id]).first
              if mother_accounting_code.blank?
                Rails.logger.error "Mother accounting code #{o[:mother_accounting_code_id]} not found"
                print "E"
              else
                ac.mother_accounting_code = mother_accounting_code
              end

              if ac.valid?
                Rails.logger.info("Saving new accounting code #{o[:id]}")
                print "."
                ac.save!
              else
                Rails.logger.error("Error in creating accounting code: #{ac.errors.messages}")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating accounting code #{o[:id]}")
            print "."

            mother_accounting_code = MotherAccountingCode.where(id: o[:mother_accounting_code_id]).first
            if mother_accounting_code.blank?
              Rails.logger.error "Mother accounting code #{o[:mother_accounting_code_id]} not found"
              print "E"
            else
              accounting_code.update!(
                name: o[:name], 
                code: o[:code],
                sub_code: o[:sub_code],
                main_code: o[:main_code],
                normal_transaction: o[:normal_transaction],
                mother_accounting_code: mother_accounting_code
              )
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        AccountingCode.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found - #{url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end

    def self.sync_mother_accounting_codes!
      Rails.logger.info "Syncing mother accounting codes"
      print "."

      url = "#{MASTER_SERVER}/api/v1/sync/finance/accounting/mother_accounting_codes"
      http = Curl.post(url, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          mother_accounting_code = MotherAccountingCode.where(id: o[:id]).first

          if mother_accounting_code.blank?
            Rails.logger.info("Mother accounting code #{o[:name]} not found. Creating new one.")
            print "."

            MotherAccountingCode.new do |mac|
              mac.id = o[:id]
              mac.name = o[:name]
              mac.code = o[:code]
              mac.subcode = o[:subcode]
              major_account = MajorAccount.where(id: o[:major_account_id]).first
              if major_account.blank?
                Rails.logger.error "Major account #{o[:major_account_id]} not found"
                print "E"
              else
                mac.major_account = major_account
              end

              if mac.valid?
                Rails.logger.info("Saving new mother accounting code #{o[:id]}")
                print "."
                if mac.save
                  Rails.logger.error("Error in saving mother accounting code: #{mac.errors.messages}")
                  print "E"
                end
              else
                Rails.logger.error("Error in creating mother accounting code")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating mother accounting code #{o[:id]}")
            print "."

            major_account = MajorAccount.where(id: o[:major_account_id]).first
            if major_account.blank?
              Rails.logger.error "Major account #{o[:major_account_id]} not found"
              print "E"
            else
              if mother_accounting_code.update(
                name: o[:name], 
                code: o[:code],
                subcode: o[:subcode],
                major_account: major_account
              )
              else
                Rails.logger.error("Error: #{mother_accounting_code.errors.messages}")
                print "E"
              end
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        MotherAccountingCode.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end

    def self.sync_major_accounts!
      Rails.logger.info "Syncing major accounts"
      print "."

      url = "#{MASTER_SERVER}/api/v1/sync/finance/accounting/major_accounts"
      http = Curl.post(url, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          major_account = MajorAccount.where(id: o[:id]).first

          if major_account.blank?
            Rails.logger.info("Major account #{o[:name]} not found. Creating new one.")
            print "."

            MajorAccount.new do |ma|
              ma.id = o[:id]
              ma.name = o[:name]
              ma.code = o[:code]
              major_group = MajorGroup.where(id: o[:major_group_id]).first
              if major_group.blank?
                Rails.logger.error "Major account #{o[:major_group_id]} not found"
                print "E"
              else
                ma.major_group = major_group
              end

              if ma.valid?
                Rails.logger.info("Saving new major account #{o[:id]}")
                print "."
                ma.save!
              else
                Rails.logger.error("Error in creating major account")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating major account #{o[:id]}")
            print "."

            major_group = MajorGroup.where(id: o[:major_group_id]).first
            if major_group.blank?
              Rails.logger.error "Major group #{o[:major_group_id]} not found"
              print "E"
            else
              major_account.update!(
                name: o[:name], 
                code: o[:code],
                major_group: major_group
              )
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        MajorAccount.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end

    def self.sync_major_groups!
      Rails.logger.info "Syncing major groups"
      print "."

      url = "#{MASTER_SERVER}/api/v1/sync/finance/accounting/major_groups"
      http = Curl.post(url, PARAMS)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          major_group = MajorGroup.where(id: o[:id]).first

          if major_group.blank?
            Rails.logger.info("Major group #{o[:name]} not found. Creating new one.")
            print "."

            MajorGroup.new do |mj|
              mj.id = o[:id]
              mj.name = o[:name]
              mj.code = o[:code]
              mj.dc_code = o[:dc_code]

              if mj.valid?
                Rails.logger.info("Saving new major group #{o[:id]}")
                print "."
                mj.save!
              else
                Rails.logger.error("Error in creating major group")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating major group #{o[:id]}")
            print "."

            major_group.update!(
              name: o[:name], 
              code: o[:code],
              dc_code: o[:dc_code]
            )
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        MajorGroup.where.not(id: ids).destroy_all

      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
