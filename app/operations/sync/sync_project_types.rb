module Sync
  class SyncProjectTypes
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing project types"
      print "."
      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"
        print "."

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          project_type = ProjectType.where(id: o[:id]).first

          if project_type.blank?
            Rails.logger.info("Project type #{o[:id]} not found. Creating new one.")
            print "."

            ProjectType.new do |pt|
              pt.id = o[:id]
              pt.name = o[:name]
              pt.code = o[:code]
              
              project_type_category = ProjectTypeCategory.where(id: o[:project_type_category_id]).first
              if project_type_category.blank?
                Rails.logger.error "Project type #{o[:project_type_category_id]} not found"
                print "E"
              else
                pt.project_type_category = project_type_category
              end

              if pt.valid?
                Rails.logger.info("Saving new project type #{o[:id]}")
                print "."
                pt.save!
              else
                Rails.logger.error("Error in creating project type")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating project type #{o[:id]}")
            print "."

            project_type_category = ProjectTypeCategory.where(id: o[:project_type_category_id]).first
            if project_type_category.blank?
              Rails.logger.error "Project type category #{o[:project_type_category_id]} not found"
              print "E"
            else
              project_type.update!(
                name: o[:name], 
                code: o[:code],
                project_type_category: project_type_category
              )
            end
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        ids = (data.map { |o| o[:id] }).uniq
        ProjectType.where.not(id: ids).destroy_all
      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end

