module Sync
  class SyncMajorGroups
    def initialize(api_key:, url:)
      @api_key = api_key
      @url = url
      @params = { api_key: @api_key }
    end

    def execute!
      Rails.logger.info "Syncing accounting code categories"
      print "."

      http = Curl.post(@url, @params)

      if http.response_code == 200
        data = JSON.parse(http.body_str, symbolize_names: true)[:data]
        Rails.logger.debug data

        ids = (data.map { |o| o[:id] }).uniq
        Rails.logger.debug "IDs: #{ids.inspect}"

        data.each do |o|
          Rails.logger.info("Syncing #{o[:name]} - #{o[:code]}")
          print "."

          major_group = MajorGroup.where(id: o[:id]).first

          if major_group.blank?
            Rails.logger.info("Major group #{o[:name]} not found. Creating new one.")
            print "."

            MajorGroup.new do |mj|
              mj.id = o[:id]
              mj.name = o[:name]
              mj.code = o[:code]
              mj.dc_code = o[:dc_code]

              if mj.valid?
                Rails.logger.info("Saving new major group #{o[:id]}")
                print "."
                mj.save!
              else
                Rails.logger.error("Error in creating major group")
                print "E"
              end
            end
          else
            Rails.logger.info("Updating major group #{o[:id]}")
            print "."

            major_group.update!(
              name: o[:name], 
              code: o[:code],
              dc_code: o[:dc_code]
            )
          end
        end

        Rails.logger.info("Deleting unwanted data")
        print "."
        MajorGroup.where.not(id: ids).destroy_all

      elsif http.response_code == 404
        Rails.logger.error("404: Not Found: #{@url}")
        print "E"
      else
        Rails.logger.error("Something went wrong. Reply: #{JSON.parse(http.body_str, symbolize_names: true)[:info]}")
        print "E"
      end
    end
  end
end
