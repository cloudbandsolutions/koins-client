module Cooperative
  class FetchInvalidMemberships
    def initialize
      @membership_type  = MembershipType.where(name: Settings.cooperative_membership_type).first

      if !@membership_type
        raise "cooperative_membership_type #{Settings.cooperative_membership_type} settings not found!"
      end

      @membership_payments  = MembershipPayment.paid.where(membership_type_id: @membership_type.id)
      @data                 = []
    end

    def execute!
      # TODO: Make this rule more fine grained
#      @membership_payments  = @membership_payments.where(
#                                "extract(month from created_at) <> extract(month from paid_at) AND amount_paid > 0"
#                              )
      @membership_payments  = @membership_payments.where("amount_paid > 0")

      @membership_payments.each do |membership_payment|
        if membership_payment.created_at < membership_payment.paid_at
          if (membership_payment.paid_at.to_datetime - membership_payment.created_at.to_datetime) >= 1.week
            @data << membership_payment
          end
        elsif membership_payment.paid_at < membership_payment.created_at
          if (membership_payment.created_at.to_datetime - membership_payment.paid_at.to_datetime) >= 1.week
          #if (membership_payment.paid_at - membership_payment.created_at) >= 1.week
            @data << membership_payment
          end
        end
      end

      @data
    end
  end
end
