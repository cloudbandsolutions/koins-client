module Cooperative
  class FetchInvalidMembershipMembers
    def initialize
      @invalid_memberships  = ::Cooperative::FetchInvalidMemberships.new.execute!
      @members              = []
    end

    def execute!
      if @invalid_memberships.size > 0
        member_ids  = @invalid_memberships.pluck(:member_id)
        @members    = Member.where(id: member_ids)
      end

      @members
    end
  end
end
