module Reinstatements
  class ReverseReinstatement
    def initialize(reinstatement:, user:)
      @user               = user
      @approved_by        = @user.full_name
      @reinstatement      = reinstatement
    end

    def execute!
      @reinstatement.update!(status: "reversed")

      @reinstatement.reinstatement_records.each do |reinstatement_record|
        reinstatement_record.update!(
          status: "pending"
          )

        member = reinstatement_record.member

        member.update!(
          is_reinstate: false,
          old_previous_mii_member_since: nil,
          previous_mii_member_since: reinstatement_record.old_recognition_date
          )
      end
    end
  end
end
