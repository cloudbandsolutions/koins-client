module Reinstatements
	class AddMemberToReinstatement
		attr_accessor :reinstatement, :member

		def initialize(reinstatement:, member:)
      @member         = member
      @reinstatement  = reinstatement
    end

    def execute!
      build_reinstatement_record!
      @reinstatement
    end

    private

   	def build_reinstatement_record!
   		reinstatement_record = ReinstatementRecord.new(
   													member: @member,
                            old_recognition_date: @member.previous_mii_member_since,
                            reinstatement_date: "",
   													status: "pending",
   			)
   		@reinstatement.reinstatement_records << reinstatement_record
    end
	end
end