module Reinstatements
  class ValidateReinstatementForApproval
    def initialize(reinstatement:)
      @reinstatement      = reinstatement
      @branch             = reinstatement.branch
      @errors = []
    end

    def execute!
      check_params!
      check_reinstatement_records_params!
      @errors
    end

    private

    def check_params!
      if @branch.nil?
        @errors << "Branch cant be blank."
      end

      if @reinstatement.date_prepared.nil?
        @errors << "Date Prepared cant be blank."  
      end
    end

    def check_reinstatement_records_params!
      @reinstatement.reinstatement_records.each do |rr|
        if !rr.reinstatement_date.present?
          @errors << "Reinstatement date can't be blank"
        end

        if !rr.old_recognition_date.present?
          @errors << "Old recognition date can't be blank"  
        end
      end  
    end
  end
end
