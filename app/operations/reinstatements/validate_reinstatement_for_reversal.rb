module Reinstatements
  class ValidateReinstatementForReversal
    attr_accessor :reinstatement, :errors

    def initialize(reinstatement:)
      @reinstatement = reinstatement
      @errors = []
    end

    def execute!
      @errors
    end
  end
end
