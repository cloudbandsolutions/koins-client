module Reinstatements
  class CreateReinstatement
    attr_accessor :branch, :date_prepared, :prepared_by, :reinstatement

    def initialize(branch:, date_prepared:, prepared_by:)
      @branch              = branch
      @date_prepared       = date_prepared
      @prepared_by         = prepared_by
    end

    def execute!
      build_reinstatement!
      @reinstatement
    end

    private

    def build_reinstatement!
        @reinstatement      = Reinstatement.new(
                              branch: @branch,
                              prepared_by: @prepared_by,
                              date_prepared: @date_prepared,
                              )
        @reinstatement
    end
  end
end
