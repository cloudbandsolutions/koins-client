module Reinstatements
  class ApproveReinstatement
    attr_accessor :reinstatement, :errors

    require 'application_helper'

    def initialize(reinstatement:, user:)
      @reinstatement  = reinstatement
      @user            = user
      @c_working_date  = ApplicationHelper.current_working_date
    end

    def execute!
      @reinstatement.update!(
        status: "approved", 
        updated_at: @c_working_date,
        approved_by: @user
      )
      
      approved_reinstatement_record_status!
    end

    private

    def approved_reinstatement_record_status!
      @reinstatement.reinstatement_records.each do |reinstatement_record|
        reinstatement_record.update!(
          status: "approved"
          )
       
        member = reinstatement_record.member

        member.update!(
          is_reinstate: true,
          old_previous_mii_member_since: reinstatement_record.old_recognition_date,
          previous_mii_member_since: reinstatement_record.reinstatement_date
          )
      end
    end
  end
end
