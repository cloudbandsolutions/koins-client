module DeceasedAndTotalAndPermanentDisabilities
  class ValidateDeceasedAndTotalAndPermanentDisabilityForApproval
    def initialize(deceased_and_total_and_permanent_disability:)
      @deceased_and_total_and_permanent_disability = deceased_and_total_and_permanent_disability
      @branch                                      = deceased_and_total_and_permanent_disability.branch
      @errors = []
    end

    def execute!
      check_params!
      check_deceased_and_total_and_permanent_disability_records!
      @errors
    end

    private

    def check_params!
      if @branch.nil?
        @errors << "Branch cant be blank."
      end

      if @deceased_and_total_and_permanent_disability.date_prepared.nil?
        @errors << "Date Prepared cant be blank."  
      end
    end

    def check_deceased_and_total_and_permanent_disability_records!
      @deceased_and_total_and_permanent_disability.deceased_and_total_and_permanent_disability_records.each do |dtr|
        if !dtr.classification.present?
          @errors << "Classification for #{dtr.legal_dependent} can't be blank"
        end

        if !dtr.deceased_tpd_date.present?
          @errors << "Deceased - TPD date for #{dtr.legal_dependent} can't be blank"  
        end
      end  
    end
  end
end
