module DeceasedAndTotalAndPermanentDisabilities
  class ReverseDeceasedAndTotalAndPermanentDisability
    def initialize(deceased_and_total_and_permanent_disability:, user:)
      @user                                        = user
      @approved_by                                 = @user.full_name
      @deceased_and_total_and_permanent_disability = deceased_and_total_and_permanent_disability
    end

    def execute!
      @deceased_and_total_and_permanent_disability.update!(status: "reversed")

      @deceased_and_total_and_permanent_disability.deceased_and_total_and_permanent_disability_records.each do |deceased_and_total_and_permanent_disability_record|
        deceased_and_total_and_permanent_disability_record.update!(
          status: "pending"
          )

        legal_dependent = deceased_and_total_and_permanent_disability_record.legal_dependent

        if deceased_and_total_and_permanent_disability_record.classification == "Deceased"
          legal_dependent.update!(
            is_deceased: false,
          )
        elsif deceased_and_total_and_permanent_disability_record.classification == "Total and Permanent Disability"
          legal_dependent.update!(
            is_tpd: false,
          )
        end
      end
    end
  end
end
