module DeceasedAndTotalAndPermanentDisabilities
  class ValidateDeceasedAndTotalAndPermanentDisabilityForReversal
    attr_accessor :deceased_and_total_and_permanent_disability, :errors

    def initialize(deceased_and_total_and_permanent_disability:)
      @deceased_and_total_and_permanent_disability = deceased_and_total_and_permanent_disability
      @errors = []
    end

    def execute!
      @errors
    end
  end
end
