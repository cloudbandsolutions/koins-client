module DeceasedAndTotalAndPermanentDisabilities
	class AddLegalDependentToDeceasedAndTotalAndPermanentDisability
		attr_accessor :deceased_and_total_and_permanent_disability, :legal_dependent

		def initialize(deceased_and_total_and_permanent_disability:, legal_dependent:)
      @legal_dependent                             = legal_dependent
      @deceased_and_total_and_permanent_disability = deceased_and_total_and_permanent_disability
    end

    def execute!
      build_deceased_and_total_and_permanent_disability_record!
      @deceased_and_total_and_permanent_disability
    end

    private

   	def build_deceased_and_total_and_permanent_disability_record!
   		deceased_and_total_and_permanent_disability_record = DeceasedAndTotalAndPermanentDisabilityRecord.new(
   													legal_dependent: @legal_dependent,
                            deceased_tpd_date: nil,
                            classification: nil,
   													status: "pending",
   			)
   		@deceased_and_total_and_permanent_disability.deceased_and_total_and_permanent_disability_records << deceased_and_total_and_permanent_disability_record
    end
	end
end