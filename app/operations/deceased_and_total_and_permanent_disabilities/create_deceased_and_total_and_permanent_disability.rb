module DeceasedAndTotalAndPermanentDisabilities
  class CreateDeceasedAndTotalAndPermanentDisability
    attr_accessor :branch, :date_prepared, :prepared_by, :deceased_and_total_and_permanent_disability

    def initialize(branch:, date_prepared:, prepared_by:)
      @branch              = branch
      @date_prepared       = date_prepared
      @prepared_by         = prepared_by
    end

    def execute!
      build_deceased_and_total_and_permanent_disability!
      @deceased_and_total_and_permanent_disability
    end

    private

    def build_deceased_and_total_and_permanent_disability!
        @deceased_and_total_and_permanent_disability = DeceasedAndTotalAndPermanentDisability.new(
                              branch: @branch,
                              prepared_by: @prepared_by,
                              date_prepared: @date_prepared,
                              )
        @deceased_and_total_and_permanent_disability
    end
  end
end
