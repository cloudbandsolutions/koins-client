module DeceasedAndTotalAndPermanentDisabilities
  class ApproveDeceasedAndTotalAndPermanentDisability
    attr_accessor :deceased_and_total_and_permanent_disability, :errors

    require 'application_helper'

    def initialize(deceased_and_total_and_permanent_disability:, user:)
      @deceased_and_total_and_permanent_disability  = deceased_and_total_and_permanent_disability
      @user            = user
      @c_working_date  = ApplicationHelper.current_working_date
    end

    def execute!
      @deceased_and_total_and_permanent_disability.update!(
        status: "approved", 
        updated_at: @c_working_date,
        approved_by: @user
      )
      
      approved_deceased_and_total_and_permanent_disability_record_status!
    end

    private

    def approved_deceased_and_total_and_permanent_disability_record_status!
      @deceased_and_total_and_permanent_disability.deceased_and_total_and_permanent_disability_records.each do |deceased_and_total_and_permanent_disability_record|
        deceased_and_total_and_permanent_disability_record.update!(
          status: "approved"
          )
       
        legal_dependent = deceased_and_total_and_permanent_disability_record.legal_dependent

        if deceased_and_total_and_permanent_disability_record.classification == "Deceased"
          legal_dependent.update!(
            is_deceased: true,
          )
        elsif deceased_and_total_and_permanent_disability_record.classification == "Total and Permanent Disability"
          legal_dependent.update!(
            is_tpd: true,
          )
        end
      end
    end
  end
end
