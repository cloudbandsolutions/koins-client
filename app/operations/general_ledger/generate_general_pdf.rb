module GeneralLedger
  class GenerateGeneralPdf
    def initialize(start_date: , end_date:)
      @data = {}       
      @start_date = start_date
      @end_date = end_date
      
    end

    def execute!
      @data[:accounting_codes] = []
      AccountingCode.select("*").order(:code).each do |accounting_code|
        accounting_code_data = {}
        accounting_code_data[:id] = accounting_code.id
        accounting_code_data[:name] = accounting_code.name
        #accounting_code_data[:beggining_balance] = 0
        accounting_code_data[:code] = accounting_code.code

        beginning_debit = 0
        beginning_credit = 0
        final_beginning = 0

        ending_debit = 0
        ending_credit = 0
        
        
        max_date_one = Date.parse(@start_date.to_s) - 1
        max_date = Date.parse(@start_date.to_s)
        max_end  = Date.parse(@end_date.to_s)

        beginning_debit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'DR' AND vouchers.date_prepared <= ?", accounting_code.id, max_date_one).sum(:amount)

        beginning_credit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'CR' AND vouchers.date_prepared <= ?", accounting_code.id, max_date_one).sum(:amount)

        total_end_credit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND post_type = 'CR' AND vouchers.date_prepared >= ? AND vouchers.date_prepared <= ?", max_date ,max_end).sum(:amount)
        
        total_end_debit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND post_type = 'DR' AND vouchers.date_prepared >= ? AND vouchers.date_prepared <= ?", max_date ,max_end).sum(:amount)
        
        @data[:cr_end] = total_end_credit
        @data[:db_end] = total_end_debit

        if accounting_code.mother_accounting_code.major_account.major_group.dc_code == "DR"
          accounting_code_data[:beg] =  beginning_debit - beginning_credit
        else
         accounting_code_data[:beg] =  beginning_credit - beginning_debit
        end
        ending_debit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'DR' AND vouchers.date_prepared <= ?", accounting_code.id, max_end).sum(:amount)   
      

        ending_credit = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'CR' AND vouchers.date_prepared <= ?", accounting_code.id , max_end).sum(:amount)
       
        
        if accounting_code.mother_accounting_code.major_account.major_group.dc_code == "DR"
          accounting_code_data[:end] =  ending_debit - ending_credit
        else
          accounting_code_data[:end] =  ending_credit - ending_debit
        end
        
       # accounting_code_data[:beg] = debit_acc

       accounting_code_data[:or_debit] = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'DR' AND vouchers.book = 'CRB' AND vouchers.date_prepared >=? AND vouchers.date_prepared <=?", accounting_code.id, max_date , max_end).sum(:amount)
      
        
       accounting_code_data[:or_credit] = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'CR' AND vouchers.book = 'CRB' AND vouchers.date_prepared >=? AND vouchers.date_prepared <=?", accounting_code.id, max_date , max_end).sum(:amount)

      accounting_code_data[:cd_credit] = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'CR' AND vouchers.book = 'CDB' AND vouchers.date_prepared >=? AND vouchers.date_prepared <=?", accounting_code.id, max_date , max_end).sum(:amount)

      accounting_code_data[:cd_debit] = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'DR' AND vouchers.book = 'CDB' AND vouchers.date_prepared >=? AND vouchers.date_prepared <=?", accounting_code.id, max_date , max_end).sum(:amount)


      accounting_code_data[:jv_debit] = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'DR' AND vouchers.book = 'JVB' AND vouchers.date_prepared >=? AND vouchers.date_prepared <=?", accounting_code.id,max_date , max_end).sum(:amount)

      accounting_code_data[:jv_credit] = JournalEntry.joins(:voucher).where("vouchers.status = 'approved' AND journal_entries.accounting_code_id = ? AND post_type = 'CR' AND vouchers.book = 'JVB' AND vouchers.date_prepared >=? AND vouchers.date_prepared <=?", accounting_code.id,max_date , max_end).sum(:amount)
      accounting_code_data[:jvoucher] = []

        Voucher.select("*").joins(:journal_entries).where("journal_entries.accounting_code_id = ? AND vouchers.date_prepared >=? AND vouchers.date_prepared <= ?", accounting_code.id , max_date , max_end).each do |jvoucher|
        
          jvoucher_data = {}

          jvoucher_data[:particular] = jvoucher.particular
          jvoucher_data[:date_posted] = jvoucher.date_posted
          jvoucher_data[:post_type] = jvoucher.post_type
          jvoucher_data[:amt] = JournalEntry.find(jvoucher.id).amount
          jvoucher_data[:book] = jvoucher.book
            
          
          
          accounting_code_data[:jvoucher] << jvoucher_data

        end
        

      @data[:accounting_codes] << accounting_code_data
      
      end
      
      @data
    end
  end
end
