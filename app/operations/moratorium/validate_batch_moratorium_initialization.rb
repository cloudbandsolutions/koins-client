module Moratorium  
  class ValidateBatchMoratoriumInitialization
    def initialize(date_initialized:, center_ids:, number_of_days:, start_of_moratorium:)
      @date_initialized     = date_initialized
      @number_of_days       = number_of_days
      @start_of_moratorium  = start_of_moratorium
      @errors = []

      if center_ids.size > 0
        @centers = Center.where(id: center_ids)
      else
        @centers = Center.all
      end
    end

    def execute!
      validate_parameters!
      @errors
    end

    private

    def validate_parameters!
      if !@date_initialized.present?
        @errors << "No date initialized specified"
      end

      if !@number_of_days.present?
        @errors << "No number of days specified"
      end

      if !@start_of_moratorium.present?
        @errors << "No start of moratorium specified"
      end

      if @centers.size == 0
        @errors << "No centers present"
      end
    end
  end
end