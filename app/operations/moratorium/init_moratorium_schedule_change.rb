module Moratorium  
  class InitMoratoriumScheduleChange
    def initialize(batch_moratorium:, centers:)
      @batch_moratorium     = batch_moratorium
      @centers              = centers
      @number_of_days       = batch_moratorium.number_of_days
      @start_of_moratorium  = batch_moratorium.start_of_moratorium
      @initialized_by       = batch_moratorium.initialized_by
      @reason               = batch_moratorium.reason
      @centers = centers
    end

    def execute!
      @data = []
      @loans = Loan.where(id: AmmortizationScheduleEntry.unpaid.joins(:loan).where("center_id IN (?) AND ammortization_schedule_entries.due_at >= ?", @centers.pluck(:id), @start_of_moratorium).pluck(:loan_id).uniq)
      @loans.each do |loan|
        d = {}
        d[:loan_remaining_balance] = loan.remaining_balance
        d[:loan_product] = loan.loan_product.name
        d[:member_name] = loan.member.full_name
        d[:branch] = loan.branch.name
        d[:center] = loan.center.name
        d[:schedule_change] = []
        @amortization_schedule_entries = AmmortizationScheduleEntry.unpaid.joins(:loan).where("center_id IN (?) AND ammortization_schedule_entries.due_at >= ? AND loan_id = ?", @centers.pluck(:id), @start_of_moratorium, loan.id)
        @amortization_schedule_entries.each do |ase|
          a = {}
          a[:previous_due_at] = ase.due_at
          a[:next_date_of_payment] = ase.due_at + @number_of_days.days
          a[:amount] = ase.remaining_balance
          d[:schedule_change] << a
        end

        @data << d
      end

      @data
    end
  end
end