module Moratorium
	class Reamortize
		def initialize(loan_moratorium:, num_days:)
			@loan_moratorium  = loan_moratorium	
      @num_days         = num_days
		end

		def execute!
			ActiveRecord::Base.transaction do
        amortization_schedule_entry = @loan_moratorium.ammortization_schedule_entry
        due_at_counter = @loan_moratorium.new_due_at
        AmmortizationScheduleEntry.unpaid.where(
          loan_id: amortization_schedule_entry.loan.id,
          is_void: nil
        ).order("due_at ASC").each do |ase|
          if ase.id >= amortization_schedule_entry.id
            if @num_days.present?
              previous_due_at = ase.due_at
              ase.update!(due_at: previous_due_at + @num_days.days)
            else
              ase.update!(due_at: due_at_counter)
            end

            if @num_days.present?
              due_at_counter  = due_at_counter + @num_days.days
            else
              if amortization_schedule_entry.loan.term == "weekly"
                due_at_counter = due_at_counter + 1.week
              elsif amortization_schedule_entry.loan.term == "monthly"
                due_at_counter = due_at_counter + 1.month
              elsif amortization_schedule_entry.loan.term ==  "semi-monthly"
                due_at_counter  = due_at_counter + 15.days
              end
            end
          end
        end
      end
		end
	end
end
