module Moratorium  
  class InitMoratorium
    def initialize(batch_moratorium_id:, date_initialized:, center_ids:, number_of_days:, start_of_moratorium:, initialized_by:, reason:)

      @date_initialized     = date_initialized
      @centers              = Center.where(id: center_ids)
      @number_of_days       = number_of_days
      @start_of_moratorium  = start_of_moratorium
      @initialized_by       = initialized_by
      @reason               = reason
      @batch_moratorium_id  = batch_moratorium_id
    end

    def execute!
      if @batch_moratorium_id.present?
        @batch_moratorium = BatchMoratorium.find(@batch_moratorium_id)
        @batch_moratorium.update(
                            date_initialized: @date_initialized,
                            start_of_moratorium: @start_of_moratorium,
                            number_of_days: @number_of_days,
                            initialized_by: @initialized_by,
                            reason: @reason
                          )
      else
        @batch_moratorium = BatchMoratorium.create(
                              date_initialized: @date_initialized,
                              start_of_moratorium: @start_of_moratorium,
                              number_of_days: @number_of_days,
                              initialized_by: @initialized_by,
                              reason: @reason
                            )
      end

      @batch_moratorium
    end
  end
end