module Sanitizers
  class SanitizeLoans
    def initialize
      @loans  = Loan.where.not(status: ["pending"])
    end

    def execute!
      @loans.each do |loan|
        puts "Updating interest to #{loan.total_interest}"
        loan.update!(
          interest_amount: loan.total_interest
        )
      end
    end
  end
end
