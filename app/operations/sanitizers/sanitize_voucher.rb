module Sanitizers
  class SanitizeVoucher
    def initialize(voucher:)
      @voucher                      = voucher
      @date_prepared                = @voucher.date_prepared
      @date_posted                  = @voucher.date_posted
      @created_at                   = @voucher.created_at
      @updated_at                   = @voucher.updated_at
    end

    def execute!
      # date_prepared
      actual_date_prepared    = ActiveSupport::TimeZone.new('Asia/Manila').local_to_utc(@date_prepared.to_time)
      puts "Actual date_prepared: #{actual_date_prepared}"
      corrected_date_prepared = actual_date_prepared
      
      if actual_date_prepared.day != @date_prepared.day || actual_date_prepared.year != @date_prepared.year
        corrected_date_prepared = corrected_date_prepared.change(day: @date_prepared.day)
        corrected_date_prepared = corrected_date_prepared.change(year: @date_prepared.year)

        query = "UPDATE vouchers SET date_prepared='#{corrected_date_prepared.to_date}' WHERE id=#{@voucher.id}"
        ActiveRecord::Base.connection.execute(query)

        puts "Changed voucher #{@voucher.id} date_prepared: #{@date_prepared} to #{corrected_date_prepared}"
      end

      # date_posted
      if @date_posted.present?
        actual_date_posted    = ActiveSupport::TimeZone.new('Asia/Manila').local_to_utc(@date_posted.to_time)
        puts "Actual date_posted: #{actual_date_posted}"
        corrected_date_posted = actual_date_posted
        
        if actual_date_posted.day != @date_posted.day || actual_date_posted.year != @date_posted.year
          corrected_date_posted = corrected_date_posted.change(day: @date_posted.day)
          corrected_date_posted = corrected_date_posted.change(year: @date_posted.year)

          query = "UPDATE vouchers SET date_posted='#{corrected_date_posted.to_date}' WHERE id=#{@voucher.id}"
          ActiveRecord::Base.connection.execute(query)

          puts "Changed voucher #{@voucher.id} date_posted: #{@date_posted} to #{corrected_date_posted}"
        end
      end

      # created_at
      actual_created_at    = ActiveSupport::TimeZone.new('Asia/Manila').local_to_utc(@created_at)
      corrected_created_at = actual_created_at
      
      if actual_created_at.day != @created_at.day || actual_created_at.year != @created_at.year
        corrected_created_at = corrected_created_at.utc.change(day: @created_at.day)
        corrected_created_at = corrected_created_at.utc.change(year: @created_at.year)

        query = "UPDATE vouchers SET created_at='#{corrected_created_at}' WHERE id=#{@voucher.id}"
        ActiveRecord::Base.connection.execute(query)

        puts "Changed voucher #{@voucher.id} created_at: #{@created_at} to #{corrected_created_at}"
      end

      # updated_at
      actual_updated_at    = ActiveSupport::TimeZone.new('Asia/Manila').local_to_utc(@updated_at)
      corrected_updated_at = actual_updated_at
      
      if actual_updated_at.day != @updated_at.day || actual_updated_at.year != @updated_at.year
        corrected_updated_at  = corrected_updated_at.utc.change(day: @updated_at.day)
        corrected_updated_at  = corrected_updated_at.utc.change(year: @updated_at.year)

        query = "UPDATE vouchers SET updated_at='#{corrected_updated_at}' WHERE id=#{@voucher.id}"
        ActiveRecord::Base.connection.execute(query)
        puts "Changed voucher #{@voucher.id} updated_at: #{@updated_at} to #{corrected_updated_at}"
      end
    end
  end
end
