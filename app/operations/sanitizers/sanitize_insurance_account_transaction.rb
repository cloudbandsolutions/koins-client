module Sanitizers
  class SanitizeInsuranceAccountTransaction
    def initialize(insurance_account_transaction:)
      @insurance_account_transaction  = insurance_account_transaction
      @transaction_date               = @insurance_account_transaction.transaction_date
      @transacted_at                  = @insurance_account_transaction.transacted_at
      @created_at                     = @insurance_account_transaction.created_at
      @updated_at                     = @insurance_account_transaction.updated_at
    end

    def execute!
      # transaction_date
      if !@transaction_date.present?
        puts "No transaction_date found for insurance_account_transaction #{@insurance_account_transaction.id}"
        @transaction_date = @transacted_at.to_date
      end

      actual_transaction_date    = ActiveSupport::TimeZone.new('Asia/Manila').local_to_utc(@transaction_date.to_time)
      puts "Actual transaction_date: #{actual_transaction_date}"
      corrected_transaction_date = actual_transaction_date
      
      if actual_transaction_date.day != @transaction_date.day || actual_transaction_date.year != @transaction_date.year
        corrected_transaction_date = corrected_transaction_date.change(day: @transaction_date.day)
        corrected_transaction_date = corrected_transaction_date.change(year: @transaction_date.year)

        query = "UPDATE insurance_account_transactions SET transaction_date='#{corrected_transaction_date.to_date}' WHERE id=#{@insurance_account_transaction.id}"
        ActiveRecord::Base.connection.execute(query)

        puts "Changed insurance_account_transaction #{@insurance_account_transaction.id} transaction_date: #{@transaction_date} to #{corrected_transaction_date}"
      end

      # transacted_at
      actual_transacted_at    = ActiveSupport::TimeZone.new('Asia/Manila').local_to_utc(@transacted_at)
      puts "Actual transacted_at: #{actual_transacted_at}"
      corrected_transacted_at = actual_transacted_at
      
      if actual_transacted_at.day != @transacted_at.day || actual_transacted_at.year != @transacted_at.year
        corrected_transacted_at = corrected_transacted_at.utc.change(day: @transacted_at.day)
        corrected_transacted_at = corrected_transacted_at.utc.change(year: @transacted_at.year)

        query = "UPDATE insurance_account_transactions SET transacted_at='#{corrected_transacted_at}' WHERE id=#{@insurance_account_transaction.id}"
        ActiveRecord::Base.connection.execute(query)

        puts "Changed savings account transaction #{@insurance_account_transaction.id} transacted_at: #{@transacted_at} to #{corrected_transacted_at}"
      end

      # created_at
      actual_created_at    = ActiveSupport::TimeZone.new('Asia/Manila').local_to_utc(@created_at)
      corrected_created_at = actual_created_at
      
      if actual_created_at.day != @created_at.day || actual_created_at.year != @created_at.year
        corrected_created_at = corrected_created_at.utc.change(day: @created_at.day)
        corrected_created_at = corrected_created_at.utc.change(year: @created_at.year)

        query = "UPDATE insurance_account_transactions SET created_at='#{corrected_created_at}' WHERE id=#{@insurance_account_transaction.id}"
        ActiveRecord::Base.connection.execute(query)

        puts "Changed savings account transaction #{@insurance_account_transaction.id} created_at: #{@created_at} to #{corrected_created_at}"
      end

      # updated_at
      actual_updated_at    = ActiveSupport::TimeZone.new('Asia/Manila').local_to_utc(@updated_at)
      corrected_updated_at = actual_updated_at
      
      if actual_updated_at.day != @updated_at.day || actual_updated_at.year != @updated_at.year
        corrected_updated_at  = corrected_updated_at.utc.change(day: @updated_at.day)
        corrected_updated_at  = corrected_updated_at.utc.change(year: @updated_at.year)

        query = "UPDATE insurance_account_transactions SET updated_at='#{corrected_updated_at}' WHERE id=#{@insurance_account_transaction.id}"
        ActiveRecord::Base.connection.execute(query)
        puts "Changed savings account transaction #{@insurance_account_transaction.id} updated_at: #{@updated_at} to #{corrected_updated_at}"
      end
    end
  end
end
