class SnapshotOperation
  def self.create_savings_snapshot!
    SavingsType.all.each do |savings_type|
      savings_snapshot = SavingsSnapshot.new
      cluster = Cluster.find(Settings.cluster_id)
      
      savings_snapshot.cluster_code = cluster.code
      savings_snapshot.num_active_accounts = SavingsAccount.where(savings_type_id: savings_type.id, status: 'active').count
      savings_snapshot.num_inactive_accounts = SavingsAccount.where(savings_type_id: savings_type.id, status: 'inactive').count
      savings_snapshot.total_withdraw = SavingsAccountTransaction.approved.joins(:savings_type).where("savings_types.id = ? AND savings_account_transactions.transaction_type = ?", savings_type.id, 'withdraw').sum("savings_account_transactions.amount")
      savings_snapshot.total_wp = SavingsAccountTransaction.approved.joins(:savings_type).where("savings_types.id = ? AND savings_account_transactions.transaction_type = ?", savings_type.id, 'wp').sum("savings_account_transactions.amount")
      savings_snapshot.total_deposit = SavingsAccountTransaction.approved.joins(:savings_type).where("savings_types.id = ? AND savings_account_transactions.transaction_type = ?", savings_type.id, 'deposit').sum("savings_account_transactions.amount")
      savings_snapshot.total_reversed = SavingsAccountTransaction.approved.joins(:savings_type).where("savings_types.id = ? AND savings_account_transactions.transaction_type = ?", savings_type.id, 'reversed').sum("savings_account_transactions.amount")
      savings_snapshot.total_interest = SavingsAccountTransaction.approved.joins(:savings_type).where("savings_types.id = ? AND savings_account_transactions.transaction_type = ?", savings_type.id, 'interest').sum("savings_account_transactions.amount")
      savings_snapshot.total_tax = SavingsAccountTransaction.approved.joins(:savings_type).where("savings_types.id = ? AND savings_account_transactions.transaction_type = ?", savings_type.id, 'tax').sum("savings_account_transactions.amount")
      savings_snapshot.total_fund_transfer_withdraw = SavingsAccountTransaction.approved.joins(:savings_type).where("savings_types.id = ? AND savings_account_transactions.transaction_type = ?", savings_type.id, 'fund_transfer_withdraw').sum("savings_account_transactions.amount")
      savings_snapshot.total_fund_transfer_deposit = SavingsAccountTransaction.approved.joins(:savings_type).where("savings_types.id = ? AND savings_account_transactions.transaction_type = ?", savings_type.id, 'fund_transfer_deposit').sum("savings_account_transactions.amount")
      savings_snapshot.savings_type_code = savings_type.code
      savings_snapshot.total_amount = SavingsAccountTransaction.approved.joins(:savings_type).where("savings_types.id = ?").sum("savings_account_transactions.amount")

      savings_snapshot.save!
    end
  end

  def self.create_snapshot!
    snapshot = Snapshot.new

    cluster = Cluster.find(Settings.cluster_id)
    area = cluster.area

    snapshot.area_code = area.code
    snapshot.cluster_code = cluster.code
    snapshot.num_active_loans = Loan.active.count
    snapshot.num_pending_loans = Loan.pending.count
    snapshot.num_writeoff_loans = Loan.writeoff.count
    snapshot.num_paid_loans = Loan.paid.count

    snapshot.num_loans_released = Loan.released.size
    snapshot.num_loans_overdue = Loan.overdue.size
    snapshot.num_loans_par = Loan.par.size
    snapshot.num_loans_good_standing = Loan.good_standing.size

    snapshot.total_loan_portfolio = 0.00
    Loan.released.each do |loan_released|
      snapshot.total_loan_portfolio += loan_released.temp_total_amount_due
    end

    snapshot.total_loan_balance = Loan.active.sum(:remaining_balance)
    snapshot.total_loan_payments = LoanPayment.approved.sum(:amount)
    snapshot.total_loan_wp = LoanPayment.approved.sum(:wp_amount)
    snapshot.total_loan_principal_paid = LoanPayment.approved.sum(:paid_principal)
    snapshot.total_loan_interest_paid = LoanPayment.approved.sum(:paid_interest)

    total_principal_balance = 0
    total_interest_balance = 0
    AmmortizationScheduleEntry.joins(:loan).where("loans.status = ?", 'active').each do |ase|
      total_principal_balance += ase.remaining_principal
      total_interest_balance += ase.remaining_interest
    end

    snapshot.total_loan_principal_balance = total_principal_balance
    snapshot.total_loan_interest_balance = total_interest_balance
    
    snapshot.num_active_male_members = Member.active.where(gender: "Male").count
    snapshot.num_active_female_members = Member.active.where(gender: "Female").count
    snapshot.num_active_others_members = Member.active.where(gender: "Others").count
    snapshot.num_branches = cluster.branches.count
    snapshot.num_centers = Center.where(branch_id: cluster.branches.pluck(:id)).count

    snapshot.save!
  end
end
