module MemberWithdrawals
  class GenerateNewMemberWithdrawal
    def initialize(member:, paid_at:, prepared_by:)
      @member             = member
      @paid_at            = paid_at
      @prepared_by        = prepared_by
      @savings_accounts   = @member.savings_accounts
      @insurance_accounts = @member.insurance_accounts
      @equity_accounts    = @member.equity_accounts
      @book               = 'JVB'

      @member_withdrawal  = MemberWithdrawal.new(
                              member: @member,
                              paid_at: @paid_at,
                              prepared_by: @prepared_by,
                              total_withdrawals: 0.00,
                              total_payments: 0.00
                            )
    end

    def execute!
      if Settings.activate_microloans
        build_member_withdrawal_savings_fund_balance!
      end
      
      build_member_withdrawal_insurance_fund_balance!

      if Settings.activate_microloans
        build_member_withdrawal_equity_fund_balance!
      end

      @member_withdrawal.save!

      @member_withdrawal
    end

    private

    def build_member_withdrawal_savings_fund_balance!
      @savings_accounts.each do |savings_account|
        @member_withdrawal.member_withdrawal_fund_balances  <<  MemberWithdrawalFundBalance.new(
                                                                  amount: savings_account.balance,
                                                                  accout_type: savings_account.savings_type.code,
                                                                  withdrawal_amount: savings_account.balance
                                                                )
      end
    end

    def build_member_withdrawal_insurance_fund_balance!
      @insurance_accounts.each do |insurance_account|
        amount  = insurance_account.balance
        @member_withdrawal.member_withdrawal_fund_balances  <<  MemberWithdrawalFundBalance.new(
                                                                  amount: amount,
                                                                  account_type: insurance_account.insurance_type.code,
                                                                  withdrawal_amount: amount
                                                                )
      end
    end

    def build_member_withdrawal_equity_fund_balance!
      @quity_accounts.each do |equity_account|
        @member_withdrawal  <<  MemberFundBalance.new(
                                  amount: equity_account.balance,
                                  account_type: equity_account.equity_type.code,
                                  withdrawal_amount: 0.00
                                )
      end
    end
  end
end
