module Setup
  class BuildJson
    def initialize
      @areas    = Area.all
      @clusters = Cluster.all
      @branches = Branch.all
      @centers  = Center.all
    end

    def execute!
    end
  end
end
