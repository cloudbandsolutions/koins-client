module PostProcessing
  class ProcessStatementOfAccountFunds
    def initialize
      @savings_tyeps  = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types = EquityType.all
      @valid_reference_numbers = PaymentCollection.approved.pluck(:reference_number)
    end

    def execute!
    end
  end
end
