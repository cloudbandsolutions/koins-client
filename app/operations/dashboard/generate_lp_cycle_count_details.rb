module Dashboard
  class GenerateLpCycleCountDetails
    def initialize(loan_product_id:, loan_cycle:)
      @loan_product_id = loan_product_id
      @loan_cycle = loan_cycle
      
      #@list_of_member = Loan.where(status: "active", loan_product_id: @loan_product_id, loan_cycle_count: @loan_cycle)
      

      @list_of_member = Loan.joins(:center).where("status = ? and loan_product_id = ? and loan_cycle_count =?","active",@loan_product_id,@loan_cycle).order("centers.name ASC")
      @data = {}
      @data[:loan_product] = LoanProduct.find(@loan_product_id).name
      @data[:list_of_member_loans] = []

    end
    def execute!
      @list_of_member.each do |lm|
        tmp = {}
        tmp[:member_name] = Member.find(lm[:member_id]).to_s
        #raise tmp[:member_name].inspect
        tmp[:center_name] = Center.find(lm[:center_id]).to_s
        tmp[:amount] = lm[:amount]
        tmp[:maturity_date]= lm[:maturity_date]
        @data[:list_of_member_loans] << tmp
      end

      @data
    end
  end
end
