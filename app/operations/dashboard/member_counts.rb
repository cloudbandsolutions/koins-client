module Dashboard
  class MemberCounts
    def initialize
      @as_of                        = ApplicationHelper.current_working_date
      @active_loan_entry_points     = Loan.active
      @active_members               = Member.pure_active
      @pending_members              = Member.pending
      @resigned_members             = Member.resigned
      @coop_membership_type         = MembershipType.where(name: Settings.cooperative_membership_type).first
      @membership_payments          = MembershipPayment.paid.where(
                                        membership_type_id: @coop_membership_type.id
                                      )

      @invalid_coop_memberships     = ::Cooperative::FetchInvalidMembershipMembers.new.execute!

      if !@coop_membership_type
        raise "Membership type for coop not found"
      end

      @member_ids_with_positive_savings         = SavingsAccount.joins(:member).where(
                                                    "balance >= 0 AND
                                                     members.status = ?",
                                                     'active'
                                                  ).pluck(:member_id).uniq

      @member_ids_with_active_entry_point_loans = @active_loan_entry_points.pluck(:member_id).uniq
      @member_ids_active_only                   = @member_ids_with_positive_savings | @member_ids_with_active_entry_point_loans

      @data = {}

      @data[:inforce_members_male]    = 0
      @data[:inforce_members_female]  = 0
      @data[:inforce_members_others]  = 0
      @data[:inforce_members_total]   = 0


      @data[:lapsed_members_male]    = 0
      @data[:lapsed_members_female]  = 0
      @data[:lapsed_members_others]  = 0
      @data[:lapsed_members_total]   = 0

      @data[:pure_savers_male]        = 0
      @data[:pure_savers_female]      = 0
      @data[:pure_savers_others]      = 0
      @data[:pure_savers_total]       = 0

      @data[:active_loaners_male]     = 0
      @data[:active_loaners_female]   = 0
      @data[:active_loaners_others]   = 0
      @data[:active_loaners_total]    = 0

      @data[:active_members_male]     = 0
      @data[:active_members_female]   = 0
      @data[:active_members_others]   = 0
      @data[:active_members_total]    = 0

      @data[:pending_members_male]    = 0
      @data[:pending_members_female]  = 0
      @data[:pending_members_others]  = 0
      @data[:pending_members_total]   = 0

      @data[:resigned_members_male]    = 0
      @data[:resigned_members_female]  = 0
      @data[:resigned_members_others]  = 0
      @data[:resigned_members_total]   = 0

      @data[:invalid_coop_membership_male]    = 0
      @data[:invalid_coop_membership_female]  = 0
      @data[:invalid_coop_membership_others]  = 0
      @data[:invalid_coop_membership_total]   = 0
    end

    def execute!
      build_pure_savers
      build_active_loaners
      build_active_members
      build_pending_members
      build_resigned_members
      build_inforce_members
      build_lapsed_members
      build_invalid_coop_memberships

      ::Summaries::InsertMemberCounts.new(
        as_of: @as_of,
        data: @data
      ).execute!

      @data
    end

    private

    def build_inforce_members
      @data[:inforce_members_male]    = @active_members.where(gender: "Male", insurance_status: "inforce").count
      @data[:inforce_members_female]  = @active_members.where(gender: "Female", insurance_status: "inforce").count
      @data[:inforce_members_others]  = @active_members.where(gender: "Others", insurance_status: "inforce").count
      @data[:inforce_members_total]   = @data[:inforce_members_male] + @data[:inforce_members_female] + @data[:inforce_members_others]
    end

    def build_lapsed_members
      @data[:lapsed_members_male]    = @active_members.where(gender: "Male", insurance_status: "lapsed").count
      @data[:lapsed_members_female]  = @active_members.where(gender: "Female", insurance_status: "lapsed").count
      @data[:lapsed_members_others]  = @active_members.where(gender: "Others", insurance_status: "apsed").count
      @data[:lapsed_members_total]   = @data[:lapsed_members_male] + @data[:lapsed_members_female] + @data[:lapsed_members_others]
    end


    def build_invalid_coop_memberships
      if @invalid_coop_memberships.size > 0
        @data[:invalid_coop_membership_male]    = @invalid_coop_memberships.where(gender: "Male").count
        @data[:invalid_coop_membership_female]  = @invalid_coop_memberships.where(gender: "Female").count
        @data[:invalid_coop_membership_others]  = @invalid_coop_memberships.where(gender:" Others").count
        @data[:invalid_coop_membership_total]   = @data[:invalid_coop_membership_male] + @data[:invalid_coop_membership_female] + @data[:invalid_coop_membership_others]
      end
    end

    def build_resigned_members
      @data[:resigned_members_male]    = @resigned_members.where(gender: "Male").count
      @data[:resigned_members_female]  = @resigned_members.where(gender: "Female").count
      @data[:resigned_members_others]  = @resigned_members.where(gender: "Others").count
      @data[:resigned_members_total]   = @data[:resigned_members_male] + @data[:resigned_members_female] + @data[:resigned_members_others]
    end

    def build_pure_savers
      @data[:pure_savers_male]      = @active_members.where(id: @member_ids_with_positive_savings, gender: "Male").where.not(id: @member_ids_with_active_entry_point_loans).count
      @data[:pure_savers_female]    = @active_members.where(id: @member_ids_with_positive_savings, gender: "Female").where.not(id: @member_ids_with_active_entry_point_loans).count
      @data[:pure_savers_others]    = @active_members.where(id: @member_ids_with_positive_savings, gender: "Others").where.not(id: @member_ids_with_active_entry_point_loans).count
      @data[:pure_savers_total]     = @data[:pure_savers_male] + @data[:pure_savers_female] + @data[:pure_savers_others]
    end

    def build_active_loaners
      @data[:active_loaners_male]   = @active_members.where(id: @member_ids_with_active_entry_point_loans, gender: "Male").count
      @data[:active_loaners_female] = @active_members.where(id: @member_ids_with_active_entry_point_loans, gender: "Female").count
      @data[:active_loaners_others] = @active_members.where(id: @member_ids_with_active_entry_point_loans, gender: "Others").count
      @data[:active_loaners_total]  = @data[:active_loaners_male] + @data[:active_loaners_female] + @data[:active_loaners_others]
    end

    def build_active_members
      @data[:active_members_male]   = @active_members.where(id: @member_ids_active_only, gender: "Male").count
      @data[:active_members_female] = @active_members.where(id: @member_ids_active_only, gender: "Female").count
      @data[:active_members_others] = @active_members.where(id: @member_ids_active_only, gender: "Others").count
      @data[:active_members_total]  = @data[:active_members_male] + @data[:active_members_female] + @data[:active_members_others]
    end

    def build_pending_members
      @data[:pending_members_male]    = @pending_members.where(gender: "Male").count
      @data[:pending_members_female]  = @pending_members.where(gender: "Female").count
      @data[:pending_members_others]  = @pending_members.where(gender: "Others").count
      @data[:pending_members_total]   = @data[:pending_members_male] + @data[:pending_members_female] + @data[:pending_members_others]
    end
  end
end
