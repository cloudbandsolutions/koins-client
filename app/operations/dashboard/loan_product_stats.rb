module Dashboard
  class LoanProductStats
    require 'application_helper'
    def initialize(as_of:, loan_product_id:)
      @as_of          = as_of
      @loan_products  = LoanProduct.all
      @temp_as_of     = as_of
      @current_working_date = ApplicationHelper.current_working_date

      #if @current_working_date.to_date == @as_of.to_date
      if @as_of.to_date >= @current_working_date.to_date
        @temp_as_of = @as_of.to_date - 1.day
      end

      if loan_product_id
        @loan_products = @loan_products.where(id: loan_product_id)
      end

      #@active_loans   = Loan.active.where("date_approved <= ?", @as_of)
      @active_loans = ::Loans::FetchActiveLoans.new(as_of: @as_of).execute!
      @data           = {}
      @data[:entries] = []
      @data[:as_of]   = @as_of

      @data[:total_active_loans]  = 0
      @data[:total_portfolio]     = 0
      @data[:total_cum_due]       = 0
      @data[:total_temp_cum_due]  = 0
      @data[:total_past_due_amt]  = 0
      @data[:total_p_past_due_amt]= 0
      @data[:total_principal_paid]= 0
      @data[:total_par_amount]    = 0
      @data[:total_par_rate]      = 0
      @data[:total_repayment_rate]= 0
      @data[:total_p_balance]     = 0
      @data[:total_temp_principal_paid] = 0
    end

    def execute!
      #@loan_products.each do |loan_product|
      #  entry = build_entry(loan_product)

        #if entry[:active_loans] > 0
        #  @data[:entries] << entry
        #end
      #end

      #@data
      repayments_data = Reports::GenerateRepayments.new(branch_id: nil, so: nil, center_id: nil, loan_product_id: nil, as_of: @as_of).execute!
      repayments_data[:entries] = repayments_data[:loan_products]

      repayments_data
    end

    private

    def portfolio(loan)
      principal_due = loan.ammortization_schedule_entries.sum(:principal)
      paid_principal = loan.paid_principal_as_of(@as_of)

      principal_due - paid_principal
    end

    def build_entry(loan_product)
      c_loans = @active_loans.where(loan_product_id: loan_product.id)

      c_p_cum_due             = 0
      c_i_cum_due             = 0
      temp_cum_due            = 0
      temp_loan_cum_due       = 0
      c_p_total_paid          = 0
      c_i_total_paid          = 0
      c_total_paid            = 0
      c_p_total_amt_past_due  = 0
      c_i_total_amt_past_due  = 0
      c_total_amt_past_due    = 0
      c_repayment_rate        = 0
      c_portfolio             = 0
      c_par_amount            = 0
      total_temp_principal_paid = 0

      turkey_principal_amt_past_due = 0

      c_loans.each do |loan|
        t_temp_loan_cum_due     = loan.temp_total_amount_due_as_of(@temp_as_of)

        t_c_p_total_paid        = loan.paid_principal_as_of(@as_of)
        t_c_i_total_paid        = loan.paid_interest_as_of(@as_of)

        principal_cum_due       = loan.temp_total_principal_amount_due_as_of(@as_of)
        temp_principal_cum_due  = loan.temp_total_principal_amount_due_as_of(@temp_as_of)

        principal_paid          = loan.paid_principal_as_of(@temp_as_of)

        if principal_paid > temp_principal_cum_due
          total_temp_principal_paid += temp_principal_cum_due
        else
          total_temp_principal_paid += principal_paid
        end

        temp_c_par_amount       = temp_principal_cum_due - total_temp_principal_paid
        if temp_c_par_amount < 0
          temp_c_par_amount = 0
        end

        temp_portfolio          = portfolio(loan)

        ###################################################################

        temp_cum_due            += principal_cum_due
        c_p_cum_due             += temp_principal_cum_due
        temp_loan_cum_due       += temp_principal_cum_due

        c_p_total_paid          += t_c_p_total_paid
        c_i_total_paid          += t_c_i_total_paid

        c_total_paid            += t_c_p_total_paid + t_c_i_total_paid
        c_p_total_amt_past_due  += principal_cum_due - t_c_p_total_paid

        #c_par_amount            += temp_c_par_amount
        #unpaid_records  = Loans::UnpaidAmortizationEntries.new(
        #                    loan_id: loan.id,
        #                    as_of: @as_of,
        #                    temp_as_of: @temp_as_of
        #                  ).execute!
        unpaid_records = loan.ammortization_schedule_entries.unpaid.where("due_at <= ?", @temp_as_of)

        num_days = unpaid_records.size > 0 ? (@as_of.to_date - unpaid_records.first.due_at) : 0

        if num_days >= 1
          c_par_amount          += loan.principal_balance_as_of(@temp_as_of)
        else
          c_par_amount          += 0
        end

        c_portfolio             += temp_portfolio

        amt_due_adder = (t_temp_loan_cum_due - (t_c_p_total_paid + t_c_i_total_paid))
        if amt_due_adder < 0
          amt_due_adder = 0
        end

        c_total_amt_past_due    += amt_due_adder

        turkey_principal_amt_past_due += loan.principal_amt_past_due_as_of(@temp_as_of)
      end

      d                   = {}
      d[:name]            = loan_product.to_s
      d[:active_loans]    = c_loans.count
      d[:portfolio]       = c_portfolio
      d[:cum_due]         = c_p_cum_due
      d[:temp_cum_due]    = temp_cum_due
      d[:past_due_amt]    = c_total_amt_past_due
      d[:principal_paid]  = c_p_total_paid
      #d[:p_past_due_amt]  = c_p_cum_due - c_p_total_paid
      d[:p_past_due_amt]  = turkey_principal_amt_past_due
      d[:total_temp_principal_paid] = total_temp_principal_paid

      if d[:p_past_due_amt] < 0
        d[:p_past_due_amt] = 0
      end

      if c_par_amount < 0
        d[:par_amount]    = 0
      else
        d[:par_amount]    = c_par_amount
      end
      
      if d[:portfolio] > 0
        d[:par_rate] = ((d[:par_amount] / d[:portfolio]) * 100).round(2)
      else
        d[:par_rate] = 0
      end

      if d[:cum_due] == 0
        d[:rr] = 100.0
      else
        d[:rr] = ((d[:principal_paid] / d[:temp_cum_due]) * 100).round(2)
        if d[:rr] > 100
          d[:rr] = 100.0
        end
      end

      # Aggro for total
      @data[:total_active_loans]  += d[:active_loans]
      @data[:total_portfolio]     += d[:portfolio]
      @data[:total_cum_due]       += d[:cum_due]
      @data[:total_temp_cum_due]  += d[:temp_cum_due]
      @data[:total_past_due_amt]  += d[:past_due_amt]
      @data[:total_principal_paid]+= d[:principal_paid]
      @data[:total_p_past_due_amt]+= d[:p_past_due_amt]
      @data[:total_par_amount]    += d[:par_amount]
      @data[:total_temp_principal_paid] += d[:total_temp_principal_paid]
      
      if @data[:total_past_due_amt] == 0 and @data[:total_principal_paid] == 0
        @data[:total_rr] = 0
      elsif @data[:total_past_due_amt] == 0 and @data[:total_principal_paid] > 0
        @data[:total_rr] = 100
      else
        @data[:total_rr] = ((@data[:total_temp_principal_paid] / (@data[:total_p_past_due_amt] + @data[:total_temp_principal_paid])) * 100).round(2)

        if @data[:total_rr] > 100
          @data[:total_rr] = 100
        end
      end

      if @data[:total_portfolio] > 0
        @data[:total_par_rate] = ((@data[:total_par_amount] / @data[:total_portfolio]) * 100).round(2)

        if @data[:total_par_rate] > 100
          @data[:total_par_rate] = 100
        end
      else
        @data[:total_par_rate] = 0
      end

      d
    end
  end
end
