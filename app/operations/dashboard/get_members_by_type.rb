module Dashboard
  class GetMembersByType
    def initialize(member_type)
      @active_loan_entry_points     = Loan.active_entry_points
      @active_members               = Member.active
      @pending_members              = Member.pending
      @member_ids_with_positive_savings         = SavingsAccount.where("balance > 0").pluck(:member_id).uniq
      @member_ids_with_active_entry_point_loans = @active_loan_entry_points.pluck(:member_id)
      @member_ids_active_only                   = @member_ids_with_positive_savings | @member_ids_with_active_entry_point_loans

      @member_type = member_type

      @members = []
    end

    def execute!
      if @member_type == "pure_savers"
        @members = @active_members.where(id: @member_ids_with_positive_savings).order("last_name ASC")
      elsif @member_type == "active_loaners"
      elsif @member_type == "active"
      elsif @member_type == "pending"
      end

      @members
    end
  end
end
