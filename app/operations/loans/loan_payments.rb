module Loans
	class LoanPayments
		def initialize(loan:)
			@loan = loan
		end

		def execute!
			LoanPayment.where(loan_id: @loan.id).order("created_at DESC")
		end
	end
end
