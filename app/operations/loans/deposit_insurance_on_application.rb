module Loans
	class DepositInsuranceOnApplication
		def initialize(loan:, user:, cycle_count:)
			@loan = loan
			@user = user
			@cycle_count = cycle_count
		end

		def execute!
			if @cycle_count > 1
        num_installments = (@loan.num_installments + 1)

        @loan.loan_product.loan_insurance_deduction_entries.each do |insurance_deduction|
          insurance_type = insurance_deduction.insurance_type
          insurance_account = InsuranceAccount.where(member_id: @loan.member.id, insurance_type_id: insurance_type.id).first
          amount = insurance_deduction.val * (num_installments + 1)
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            transaction_type: 'deposit',
                                            insurance_account: insurance_account,
                                            amount: amount,
                                            voucher_reference_number: @loan.voucher_reference_number,
                                            status: 'approved',
                                            bank: @loan.bank,
                                            accounting_code: @loan.bank.accounting_code)

          insurance_account_transaction.generate_updates!
        end
      else
        @loan.loan_product.loan_insurance_deduction_entries.each do |insurance_deduction|
          insurance_type = insurance_deduction.insurance_type
          insurance_account = InsuranceAccount.where(member_id: @loan.member.id, insurance_type_id: insurance_type.id).first
          amount = insurance_deduction.val

          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            transaction_type: 'deposit',
                                            insurance_account: insurance_account,
                                            amount: amount,
                                            voucher_reference_number: @loan.voucher_reference_number,
                                            status: 'approved',
                                            bank: @loan.bank,
                                            accounting_code: @loan.bank.accounting_code)

          insurance_account_transaction.generate_updates!
        end
      end
		end
	end
end