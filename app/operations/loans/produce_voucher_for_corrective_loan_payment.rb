module Loans
  class ProduceVoucherForCorrectiveLoanPayment
    require 'application_helper'

    def initialize(loan_payment:, total_principal_paid:, total_interest_paid:, user:)
      @user                 = user
      @loan_payment         = loan_payment
      @loan                 = loan_payment.loan
      @loan_product         = @loan.loan_product
      @principal_paid       = loan_payment.paid_principal
      @interest_paid        = loan_payment.paid_interest
      @amount               = @principal_paid + @interest_paid
      @total_principal_paid = total_principal_paid
      @total_interest_paid  = total_interest_paid
      @total_paid           = total_principal_paid + total_interest_paid
      @paid_at              = loan_payment.paid_at
      @branch               = @loan_payment.loan.branch
      @bank                 = @branch.bank
      @voucher              = Voucher.new(
                                book: 'JVB',
                                branch: @branch,
                                date_prepared: @paid_at,
                                prepared_by: @user.full_name,
                                particular: "Correcting entry for loan #{@loan.pn_number}",
                                status: 'pending'
                              )
    end

    def execute!
      cr_principal_amount = @principal_paid - @total_principal_paid
      cr_interest_amount  = @interest_paid - @total_interest_paid
      dr_amount           = @amount - @total_paid

      # Reverse trasnactions
      journal_entry = JournalEntry.new(
                        amount: @total_principal_paid,
                        post_type: 'DR',
                        accounting_code: @loan_product.accounting_code_transaction
                      )

      @voucher.journal_entries << journal_entry

      journal_entry = JournalEntry.new(
                        amount: @total_interest_paid,
                        post_type: 'DR',
                        accounting_code: @loan_product.interest_accounting_code
                      )

      @voucher.journal_entries << journal_entry

      journal_entry = JournalEntry.new(
                        amount: @total_principal_paid + @total_interest_paid,
                        post_type: 'CR',
                        accounting_code: @bank.accounting_code
                      )

      @voucher.journal_entries << journal_entry

      # Actual
      journal_entry = JournalEntry.new(
                        amount: @principal_paid,
                        post_type: 'CR',
                        accounting_code: @loan_product.accounting_code_transaction
                      )

      @voucher.journal_entries << journal_entry

      journal_entry = JournalEntry.new(
                        amount: @interest_paid,
                        post_type: 'CR',
                        accounting_code: @loan_product.interest_accounting_code
                      )

      @voucher.journal_entries << journal_entry

      journal_entry = JournalEntry.new(
                        amount: @principal_paid + @interest_paid,
                        post_type: 'DR',
                        accounting_code: @bank.accounting_code
                      )

      @voucher.journal_entries << journal_entry

      @voucher
    end
  end
end
