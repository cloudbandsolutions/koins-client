module Loans
	class ValidateNewNonEntryPointLoanApplication
		def initialize(member:, loan_product:)
			@member = member
			@loan_product = loan_product
		end

		def execute!
			# If the loan product is not an entry point product
      # 1) Do not allow application if member has more than 1 active or pending entry point products
      if !@loan_product.is_entry_point
        if Loan.pending_entry_points.where("loans.member_id = ?", @member.id).count > 0 or Loan.active_entry_points.count > 0 and @loan_product.is_entry_point == true
          return false
        end
      end

      return true
		end
	end
end