module Loans
	class TempApproveLoanPaymentFromCollection
		def initialize(loan_payment:, approved_by:, prepared_by:)
			@loan_payment = loan_payment
			@approved_by = approved_by
			@prepared_by = prepared_by
		end

		def execute!
			loan = @loan_payment.loan
      ammortization_schedule = AmmortizationScheduleEntry.where(loan_id: loan.id, paid: false).order(:created_at)
      temp_amount = @loan_payment.loan_payment_amount
      stats = { principal: 0, interest: 0, ase: [], loan_product: loan.loan_product }

      ammortization_schedule.each do |as|
        if temp_amount >= 0
          puts "ASE amount: #{as.amount.to_f} principal: #{as.principal.to_f} interest: #{as.interest.to_f}"
          puts "Amount: #{temp_amount.to_f}"

          if as.interest_paid?
            puts "Interest already paid..."

            remaining_principal = as.remaining_principal
            puts "Remaining principal: #{remaining_principal.to_f}"

            if temp_amount <= remaining_principal
              as.paid_principal += temp_amount

              if temp_amount == remaining_principal
                as.paid = true
              end

              stats[:principal] += temp_amount

              # nothing more to pay
              puts "Nothing more to pay"
              temp_amount = 0
            else
              as.paid_principal += remaining_principal
              as.paid = true

              stats[:principal] += remaining_principal

              temp_amount -= remaining_principal
            end

            puts "temp_amount: #{temp_amount.to_f}"
          else
            remaining_interest = as.remaining_interest

            puts "Remaining interest: #{remaining_interest}"

            if temp_amount <= remaining_interest
              as.paid_interest += temp_amount

              stats[:interest] += temp_amount

              # nothing more to pay
              temp_amount = 0

            else
              as.paid_interest += remaining_interest

              stats[:interest] += remaining_interest

              temp_amount -= remaining_interest

              # Pay off principal
              remaining_principal = as.remaining_principal
              puts "Remaining principal: #{remaining_principal.to_f}"

              if temp_amount <= remaining_principal
                as.paid_principal += temp_amount

                if temp_amount == remaining_principal
                  as.paid = true
                end

                stats[:principal] += temp_amount

                # nothing more to pay
                puts "Nothing more to pay"
                temp_amount = 0
              else
                as.paid_principal += remaining_principal
                as.paid = true

                stats[:principal] += remaining_principal

                temp_amount -= remaining_principal
              end

              puts "temp_amount: #{temp_amount.to_f}"

            end
          end

          #as.save!
          stats[:ase] << as
        end
      end

      stats
		end
	end
end
