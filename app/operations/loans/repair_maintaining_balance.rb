module Loans
  class RepairMaintainingBalance
    def initialize
      @invalid_loans = Loans::FetchInvalidMaintainingBalanceLoans.new.execute!
    end

    def execute!
      @invalid_loans.each do |loan|
        loan.update(updated_at: Time.now)
      end

      nil
    end
  end
end
