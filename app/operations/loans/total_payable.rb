module Loans
	class TotalPayable
		def initialize
		end

		def execute
			total = 0.00

      Loan.all.each do |l|
        total += l.remaining_balance
      end

      total
		end
	end
end