module Loans
  class InitializeAmortizationSchedule
    def initialize(loan:)
      @loan = loan
    end

    def execute!
      interest_rate     = @loan.loan_product_type.interest_rate
      amount            = @loan.amount

      num_installments  = @loan.num_installments
      if @loan.override_installment_interval == true
        num_installments  = @loan.installment_override
      end

      term              = @loan.term

      schedule =  Loans::CreateAmortizationSchedule.new(
                    loan: @loan, 
                    interest_rate: interest_rate, 
                    principal: amount, 
                    term: num_installments, 
                    mode_of_payment: term
                  ).execute!

      if !@loan.first_date_of_payment.nil?
        date_counter = @loan.first_date_of_payment
      end

      amortization_schedule = []
      schedule[:payments].each do |p|
        ase = AmmortizationScheduleEntry.new(
                amount: p[:principal] + p[:interest],
                principal: p[:principal],
                interest: p[:interest],
                loan_id: @loan.id
              )

        if p[:principal] == 0 and p[:interest] == 0
          ase.paid = true
        end

        if !@loan.first_date_of_payment.nil?
          ase.due_at = date_counter
          if @loan.term == "weekly"
            date_counter = date_counter + 1.week
          elsif @loan.term == "monthly"
            date_counter = date_counter + 1.month
          elsif @loan.term == "semi-monthly"
            date_counter = date_counter + 15.days
          end
        end

        if !ase.valid?
          raise ase.errors.inspect
        end

        #amortization_schedule << ase
        ase.save!
      end

      @loan
      #@loan.ammortization_schedule_entries = amortization_schedule
    end
  end
end
