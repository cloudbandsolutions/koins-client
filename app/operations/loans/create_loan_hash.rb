module Loans
  class CreateLoanHash
    def initialize(loan:)
      @loan = loan
      @data = {}
    end

    def execute!
      @data = loan.attributes
    end
  end
end
