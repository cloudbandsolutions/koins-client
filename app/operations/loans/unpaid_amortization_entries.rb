module Loans
  class UnpaidAmortizationEntries
    def initialize(loan_id:, as_of:, temp_as_of:)
      @unpaid_entries                 = []
      @loan                           = Loan.find(loan_id)
      @as_of                          = as_of
      @temp_as_of                     = temp_as_of
      @amortization_schedule_entries  = @loan.ammortization_schedule_entries.where("due_at <= ?", @temp_as_of).order("due_at ASC")
      @total_payment                  = @loan.loan_payments.approved.where("paid_at <= ?", @as_of).sum(:amount)
    end

    def execute!
      temp_total_payment  = @total_payment
      running_payment     = 0
      @amortization_schedule_entries.each do |ase|
        running_payment += ase.amount
        if running_payment > temp_total_payment
          @unpaid_entries << ase
        end
      end

      @unpaid_entries
    end
  end
end
