module Loans
	class PendingPaymentsForLoanPayment
		def initialize
		end

		def execute!
			LoanPayment.where(status: "pending").sum(:amount)
		end
	end
end