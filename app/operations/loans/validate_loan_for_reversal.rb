module Loans
  class ValidateLoanForReversal
    def initialize(loan:)
      @loan   = loan
      @errors = []
    end

    def execute!
      check_params!
      check_if_loan_has_payments!
      @errors
    end

    private

    def check_if_loan_has_payments!
      if !@loan.no_payment_yet?
        @errors << "Cannot reverse loan with ongoing payments"
      end
    end

    def check_params!
      if @loan.pending?
        @errors << "Loan is still pending"
      end

      if @loan.paid?
        @errors << "Loan is already paid"
      end

      if @loan.writeoff?
        @errors << "Loan is already written off"
      end
    end
  end
end
