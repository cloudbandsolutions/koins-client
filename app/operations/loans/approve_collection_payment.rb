module Loans
	class ApproveCollectionPayment
		def initialize(collection_payment:, approved_by:, prepared_by:)
			@collection_payment = collection_payment
			@approved_by = approved_by
			@prepared_by = prepared_by
			@paid_at = Time.now
		end

		def execute!
			# Main voucher
      crb_voucher = Voucher.new(
                      particular: @collection_payment.loan_payment_particular,
                      or_number: @collection_payment.or_number,
                      reference_number: Vouchers::VoucherNumber.new(book: "CRB", branch: @collection_payment.branch).execute!,
                      status: 'approved',
                      book: 'CRB',
                      branch_id: @collection_payment.branch_id,
                      date_prepared: @paid_at,
                      approved_by: @approved_by,
                      prepared_by: @prepared_by
                    )

      # get results of collection_payment's loan_payments
      results = []
      @collection_payment.loan_payments.each do |loan_payment|
        # results << Loans::LoanPaymentOperation.approve_loan_payment_from_collection!(loan_payment, @approved_by, @prepared_by)
      	results << Loans::ApproveLoanPaymentFromCollection.new(loam_payment: loan_payment, approved_by: @approved_by, prepared_by: @prepared_by).execute!
      end

      # Deposits (for non-paying members)
      @collection_payment.savings_account_transactions.each do |sat|
        sat.approve!(@approved_by)
      end

      # Insurance Deposits (for non-paying members)
      @collection_payment.insurance_account_transactions.each do |iat|
        iat.approve!(@approved_by)
      end

      # update total principal and total interest for this collection payment
      total_principal_amount = 0.00
      total_interest_amount = 0.00

      LoanProduct.all.each do |loan_product|
        loan_product_principal_amount = 0.00
        loan_product_interest_amount = 0.00

        results.each do |result|
          total_principal_amount += result[:principal]
          total_interest_amount += result[:interest]

          if result[:loan_product].id == loan_product.id
            loan_product_principal_amount += result[:principal]
            loan_product_interest_amount += result[:interest]
          end
        end

        if loan_product_principal_amount > 0
          je_credit_loans_receivable = JournalEntry.new(
                                        amount: loan_product_principal_amount,
                                        post_type: 'CR',
                                        accounting_code: loan_product.accounting_code_transaction
                                      )

          crb_voucher.journal_entries << je_credit_loans_receivable
        end

        if loan_product_interest_amount > 0
          je_credit_interest_revenue = JournalEntry.new(
                                        amount: loan_product_interest_amount,
                                        post_type: 'CR',
                                        accounting_code: loan_product.interest_accounting_code
                                        )

          crb_voucher.journal_entries << je_credit_interest_revenue
        end
      end

      # Insurance deposit main voucher accounting entry
      @collection_payment.collection_payment_insurance_amounts.each do |collection_payment_insurance_amount|
        accounting_code = collection_payment_insurance_amount.insurance_type.deposit_accounting_code

        je_credit_insurance_deposit = JournalEntry.new(
                                        amount: collection_payment_insurance_amount.total_amount,
                                        post_type: 'CR',
                                        accounting_code: accounting_code
                                      )

        crb_voucher.journal_entries << je_credit_insurance_deposit
      end

      # Deposits
      if @collection_payment.total_deposits > 0
        SavingsType.all.each do |savings_type|
          d_amount = 0.00
          @collection_payment.loan_payments.each do |loan_payment|
            d_amount += loan_payment.savings_account_transactions.joins(:savings_account).where("savings_accounts.savings_type_id = ?", savings_type.id).sum(:amount)
          end

          # from normal deposits
          d_amount += @collection_payment.savings_account_transactions.joins(:savings_account).where("savings_accounts.savings_type_id = ?", savings_type.id).sum(:amount)

          if d_amount > 0
            deposit_accounting_code = savings_type.deposit_accounting_code
            je_credit_savings_deposit = JournalEntry.new(
                                          amount: d_amount,
                                          post_type: 'CR',
                                          accounting_code: deposit_accounting_code
                                        )

            crb_voucher.journal_entries << je_credit_savings_deposit
          end
        end
      end

      # Amount for inclusion of wp_payment
      bank = Bank.find(@collection_payment.loan_payments.joins(:loan).pluck("loans.bank_id").uniq.first)
      temp_amount = @collection_payment.amount
      je_debit_cash_in_bank = JournalEntry.new(
                                amount: temp_amount,
                                post_type: 'DR',
                                accounting_code: bank.accounting_code
                              )

      crb_voucher.journal_entries << je_debit_cash_in_bank
      #raise "#{crb_voucher.total_debit.to_f} #{crb_voucher.total_credit.to_f}"

      @collection_payment.loan_payment_voucher_number = crb_voucher.reference_number
      @collection_payment.reference_number = crb_voucher.reference_number
      crb_voucher.save!

      @collection_payment.loan_payments.each do |loan_payment|
        if loan_payment.loan.remaining_balance == 0
          loan_payment.loan.update!(status: 'paid')
          puts "Loan is finally paid"
        end

        loan_payment.update!(voucher_reference_number: @collection_payment.loan_payment_voucher_number)
      end

      @collection_payment.status = "approved"
      @collection_payment.approved_by = @approved_by
      @collection_payment.save!
      #collection_payment.update!(status: "approved", approved_by: approved_by)
		end
	end
end