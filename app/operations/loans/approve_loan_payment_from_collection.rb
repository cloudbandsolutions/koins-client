module Loans
	class AppoveLoanPaymentFromCollection
		def initialize(loan_payment:, approved_by:, prepared_by:)
			@loan_payment = loan_payment
			@approved_by = approved_by
			@prepared_by = prepared_by
		end

		def execute!
			result = @loan_payment.approve_payment!
      voucher_number = loan_payment.collection_payment.reference_number

      # Perform withdrawal
      sat = nil
      if @loan_payment.has_wp_amount?
        sat = SavingsAccountTransaction.create!(
                amount: @loan_payment.wp_amount,
                transaction_type: 'wp',
                transacted_at: @loan_payment.collection_payment.paid_at,
                is_withdraw_payment: true,
                particular: @loan_payment.collection_payment.wp_particular,
                voucher_reference_number: voucher_number,
                savings_account: @loan_payment.savings_account
              )

        # Update the savings account
        sat.approve!(@approved_by)
      end

      # Insurance deposits
      if @loan_payment.has_insurance_deposit?
        @loan_payment.loan_payment_insurance_account_deposits.each do |deposit|
          if deposit.amount > 0
            insurance_transaction = InsuranceAccountTransaction.create!(
                                      transacted_at: @loan_payment.collection_payment.paid_at,
                                      amount: deposit.amount,
                                      transaction_type: "deposit",
                                      particular: "deposit from collection",
                                      voucher_reference_number: voucher_number,
                                      insurance_account: deposit.insurance_account
                                    )

            insurance_transaction.approve!(@approved_by)
          end
        end
      end

      # Savings deposit
      if @loan_payment.has_deposit_amount?
        @loan_payment.savings_account_transactions.each do |savings_account_transaction|
          if savings_account_transaction.amount > 0
            savings_account_transaction.update!(transacted_at: @loan_payment.collection_payment.paid_at)
            savings_account_transaction.approve!(@approved_by)
          else
            savings_account_transaction.destroy!
          end
        end
      end
      #if @loan_payment.has_deposit_amount?
        # filter only those with amounts
        #new_savings_account_transactions = []
        #old_savings_account_transactions = @loan_payment.savings_account_transactions
        #old_savings_account_transactions.each do |savings_account_transaction|
          #if savings_account_transaction.amount > 0
            #new_savings_account_transactions << savings_account_transaction
          #else
            #savings_account_transaction.destroy!
          #end
        #end

        #@loan_payment.savings_account_transactions = new_savings_account_transactions

        #@loan_payment.savings_account_transactions.each do |savings_account_transaction|
          # Update the savings account
          #savings_account_transaction.approve!(@approved_by)
        #end
      #else
        #@loan_payment.savings_account_transactions.each do |savings_account_transaction|
          #savings_account_transaction.destroy!
        #end
      #end

      # Return result of loan_payment for computation of interest and principal
      result
		end
	end
end