module Loans
  class CheckVoucherExcel
    def initialize(loan:, voucher:)
      @loan     = loan
      @voucher  = voucher
      @config   = {}
    end

    def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          generate_config!(wb)

          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["", "", "", "", "", @loan.voucher_check_number], style: @config[:title_cell]
          sheet.add_row []
          sheet.add_row ["", @loan.member.full_name, "", @loan.voucher_date_requested.strftime("%b %d, %Y")], style: @config[:normal]
          sheet.add_row ["", @loan.branch, "", @loan.bank, "", @loan.bank_check_number], style: @config[:normal]
          sheet.add_row ["", "", "", "", "CLIP #", @loan.clip_number], style: @config[:normal]
          sheet.add_row []
          sheet.add_row ["", "#{@loan.loan_product} Loan disbursement of:"], style: @config[:normal]
          sheet.add_row ["", "\t#{@loan.center.name}", "", "#{@loan.amount}"], style: @config[:currency_cell]
          sheet.add_row ["", "\t#{@loan.member.full_name}"], style: @config[:normal]
          sheet.add_row ["", "Less: #{}"]

          @voucher.journal_entries.each do |journal_entry|
            if journal_entry.post_type == 'CR' and journal_entry.accounting_code.id != @loan.bank.accounting_code.id
              sheet.add_row ["", journal_entry.accounting_code.name, "", "", "", journal_entry.amount], style: @config[:currency_cell]
            end
          end

          sheet.add_row []

          @voucher.journal_entries.each do |journal_entry|
            if journal_entry.post_type == 'CR' and journal_entry.accounting_code.id == @loan.bank.accounting_code.id
              sheet.add_row ["", "", "", "", "", journal_entry.amount], style: @config[:currency_cell]
            end
          end

          sheet.add_row []
          sheet.add_row []

          @voucher.journal_entries.each do |journal_entry|
            if journal_entry.post_type == 'DR'
              sheet.add_row ["", journal_entry.accounting_code.name, "", "", journal_entry.amount, ""], style: @config[:currency_cell]
            end
          end

          @voucher.journal_entries.each do |journal_entry|
            if journal_entry.post_type == 'CR'
              sheet.add_row ["", "\t#{journal_entry.accounting_code.name}", "", "", "", journal_entry.amount], style: @config[:currency_cell]
            end
          end
        end
      end

      p
    end

    private

    def generate_config!(wb)
      @config[:normal]              = wb.styles.add_style font_name: "Calibri", sz: 11, format_code: "#,##0.00", alignment: { horizontal: :left }
      @config[:title_cell]          = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", sz: 11
      @config[:label_cell]          = wb.styles.add_style b: true, font_name: "Calibri", sz: 11
      @config[:currency_cell]       = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri", sz: 11
      @config[:currency_cell_right] = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", sz: 11
      @config[:percent_cell]        = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri", sz: 11
      @config[:left_aligned_cell]   = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", sz: 11
      @config[:underline_cell]      = wb.styles.add_style u: true, font_name: "Calibri", sz: 11
      @config[:header_cells]        = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri", sz: 11
      @config[:default_cell]        = wb.styles.add_style font_name: "Calibri", sz: 11
    end

  end
end
