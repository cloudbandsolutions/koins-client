module Loans
	class ReverseLoanPayment
		def initialize(loan_payment:)
			@loan_payment = loan_payment
		end

		def execute!
			errors = []

      # validate transactions in loan payment
      @loan_payment.savings_account_transactions.each do |savings_account_transaction|
        if !savings_account_transaction.valid_for_reversal?
          errors << { message: "Cannot reverse savings_account_transaction #{savings_account_transaction.uuid}" }
        end
      end

      if errors.size > 0
        return false
      else
        # Reverse savings account transactions by creating withdraw transactions
        @loan_payment.savings_account_transactions.each do |savings_account_transaction|
          #savings_account_transaction.reverse!
          transaction_type = "reverse_deposit"
          sat = SavingsAccountTransaction.new(
                                      amount: savings_account_transaction.amount,
                                      transaction_type: transaction_type,
                                      voucher_reference_number: @loan_payment.voucher_reference_number,
                                      particular: "REVERSED ENTRY",
                                      savings_account: savings_account_transaction.savings_account,
                                      status: 'reversed'
                                    )
          sat.save!
          sat.generate_updates!
        end

        # Reverse insurance account transactions by creating withdraw transactions
        @loan_payment.loan_payment_insurance_account_deposits.each do |loan_payment_insurance_account_deposit|
          #InsuranceAccountTransaction.where(transaction_type: 'deposit', voucher_reference_number: loan_payment.voucher_reference_number).each do |insurance_account_transaction|
            #insurance_account_transaction.reverse!
          #end
          iat = InsuranceAccountTransaction.new(
                                        amount: loan_payment_insurance_account_deposit.amount,
                                        transaction_type: 'reverse_deposit',
                                        voucher_reference_number: @loan_payment.voucher_reference_number,
                                        particular: "REVERSED ENTRY",
                                        insurance_account: loan_payment_insurance_account_deposit.insurance_account,
                                        status: 'reversed'
                                      )
          iat.save!
          iat.generate_updates!
        end

        # Reverse withdraw payment
        if @loan_payment.has_wp_amount?
          #SavingsAccountTransaction.where(transaction_type: 'wp', voucher_reference_number: loan_payment.wp_voucher_reference_number).each do |savings_account_transaction|
          #  savings_account_transaction.reverse!
          #end
          sat = SavingsAccountTransaction.new(
                                      amount: @loan_payment.wp_amount,
                                      transaction_type: 'reverse_withdraw',
                                      voucher_reference_number: @loan_payment.wp_voucher_reference_number,
                                      particular: "REVERSED ENTRY",
                                      savings_account: @loan_payment.savings_account,
                                      status: 'reversed'
                                    )
          sat.save!
          sat.generate_updates!

          #savings_account = loan_payment.savings_account
          #savings_account.update!(balance: savings_account.balance + loan_payment.wp_amount)
        end

        temp_paid_interest = @loan_payment.paid_interest
        temp_paid_principal = @loan_payment.paid_principal

        # Adjust amortization schedule entry
        amortization_schedule_entries = []
        d_results = []
        tr_principal = 0.00
        tr_interest = 0.00
        LoanPaymentAmortizationScheduleEntry.where(loan_payment_id: @loan_payment.id).each do |lpase|
          ase = lpase.ammortization_schedule_entry
          if !ase.nil?
            tr_principal += lpase.principal_paid
            tr_interest += lpase.interest_paid
            ase.update!(paid: 'f', paid_interest: (ase.paid_interest - lpase.interest_paid), paid_principal: (ase.paid_principal - lpase.principal_paid))
            ase.loan.update_remaining_balance!
          end
        end

        @loan_payment.update!(status: 'reversed')

        return true
      end
		end
	end
end
