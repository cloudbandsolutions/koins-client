module Loans
	class ByBatchTransactionReferenceNumber
		def initialize(batch_transaction_reference_number:, branch:)
			@batch_transaction_reference_number = batch_transaction_reference_number
			@branch = branch
		end

		def execute!
			data = []
      loans = Loan.where(batch_transaction_reference_number: @batch_transaction_reference_number, branch_id: @branch.id, status: ["pending", "active"])
      
      loans.each do |loan|
        l = {}
        l[:id] = loan.id
        l[:member] = loan.member.list_name
        l[:type] = loan.loan_product.name
        l[:status] = loan.status
        l[:amount] = loan.amount
        l[:balance] = loan.remaining_balance

        data << l
      end

      data
		end
	end
end