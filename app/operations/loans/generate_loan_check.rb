module Loans
  class GenerateLoanCheck
  	def initialize(loan:)
  		@loan = loan
  	end

  	def execute!
  		p = Axlsx::Package.new
      wb = p.workbook
      wb.add_worksheet(name: "Loan Check") do |sheet|
        sheet.add_row(["Check for Loan #{@loan}"])
        sheet.add_row(["Check Number", @loan.bank_check_number])
        sheet.add_row(["Bank", @loan.bank])
        sheet.add_row(["Member", @loan.member])
        sheet.add_row(["Amount", @loan.amount])
      end

      p
  	end
  end
end