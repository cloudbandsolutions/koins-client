module Loans
  class CreateAmortizationSchedule
    attr_reader :loan, :interest_rate, :principal, :term, :mode_of_payment
    N_YEARLY = 1
    N_SEMI_ANNUALY = 2
    N_QUARTERLY = 4
    N_MONTHLY = 12
    N_WEEKLY = 52
    N_DAILY = 365

    def initialize(loan:, interest_rate:, principal:, term:, mode_of_payment:)
      @loan             = loan

      if ["monthly", "weekly"].include? mode_of_payment
        @interest_rate = interest_rate / 100
      elsif mode_of_payment == "semi-monthly"
        @interest_rate = ((interest_rate / 12) / 2) / 100
      else
        raise "Invalid mode_of_payment value"
      end

      # 0 interest if override_interest is true
      if @loan.override_interest == true
        @interest_rate = 0.00
      end

      # If interest rate value is overriden
      if @loan.override_interest_rate.nil?
        @loan.override_interest_rate = 0.00
      end

      if @loan.override_interest_rate > 0
        #@interest_rate = (@loan.override_interest_rate / 100.00)
        if ["monthly", "weekly"].include? mode_of_payment
          @interest_rate = @loan.override_interest_rate / 100
        elsif mode_of_payment == "semi-monthly"
          @interest_rate = ((@loan.override_interest_rate / 12) / 2) / 100
        else
          raise "Invalid mode_of_payment value"
        end
      end

      if @loan.override_interest_rate == 0.00 and @loan.override_installment_interval == true
        @interest_rate = 0.00
      end

      @principal        = principal
      @term             = term
      @mode_of_payment  = mode_of_payment


      if @loan.override_installment_interval == true
        @principal    = loan.original_loan_amount
        @term         = loan.original_num_installments
      end
      #raise @term.inspect
    end

    def execute!
      if @interest_rate > 0
        @periodic_interest = nil
        @n_mode = nil

        case @mode_of_payment
        when "yearly"
          @periodic_interest = @interest_rate / N_YEARLY
          @n_mode = N_YEARLY
        when "semi-annually"
          @periodic_interest = @interest_rate / N_SEMI_ANNUALLY
          @n_mode = N_SEMI_ANNUALLY
        when "quarterly"
          @periodic_interest = @interest_rate / N_QUARTERLY
          @n_mode = N_QUARTERLY
        when "monthly"
          @periodic_interest = @interest_rate / N_MONTHLY
          @n_mode = N_MONTHLY
        when "semi-monthly"
          @periodic_interest = @interest_rate / N_MONTHLY
          @n_mode = N_MONTHLY
        when "weekly"
          @periodic_interest = @interest_rate / N_WEEKLY
          @n_mode = N_WEEKLY
        when "daily"
          @periodic_interest = @interest_rate / N_DAILY
          @n_mode = N_DAILY
        else
          raise "invalid mode of payment"
        end

        if @mode_of_payment == "weekly"
          build_weekly!
        elsif @mode_of_payment == "monthly"
          build_monthly!
        elsif @mode_of_payment == "semi-monthly"
          build_semi_monthly!
        end
      else
        build_non_interest!
      end

      @schedule
    end

    private

    def build_monthly!
      @periodic_interest = ((@interest_rate / 12))
      d_factor = ((((1 + @periodic_interest)**@term)*@principal)*@periodic_interest)/(((1+@periodic_interest)**@term)-1) 
      monthly_payment = d_factor.round(0)
      #monthly_payment = ((((@principal / @term) + ((@principal / @term).round(0) * @periodic_interest)))).round(0)

      total_interest = ((@term * monthly_payment) - @principal).round(0)
      total_principal = (@term * monthly_payment).round(0)

      #raise "Monthly Payment: #{monthly_payment.to_f.inspect}"
      #raise "Total Interest: #{total_interest.to_f.inspect} Total Principal: #{total_principal.to_f.inspect}"
      payments = []
      temp_amortization_entries = []
      @term.times do |t|
        t_interest = (@principal * @periodic_interest).round(0)
        t_principal = monthly_payment - t_interest
        t_balance = @principal - t_principal
        @principal = t_balance
        payments << {
          amount: (t_interest + t_principal).to_f,
          interest: t_interest.to_f,
          principal: t_principal.to_f,
          balance: t_balance.to_f
        }
      end

      last_payment = payments.last[:balance]
      payments = payments.reverse

      new_payments = []
      if last_payment > 0
        payments.each do |p|
          if last_payment > 0
            p[:interest] = p[:interest] - 1
            p[:principal] = p[:principal] + 1
            p[:balance]= p[:balance] - last_payment
            p[:amount] = p[:interest] + p[:principal]
            last_payment = last_payment - 1
          elsif last_payment < 0
            last_payment = last_payment * -1
          end
          new_payments << p
        end
      elsif last_payment <= 0
        last_payment1= last_payment * -1
        payments.each do |p|
          if last_payment1 > 0
            p[:interest] = p[:interest] + 1
            p[:principal] = p[:principal] - 1
            p[:balance] = p
            p[:amount] = p[:interest] + p[:principal]
            last_payment1 = last_payment1 - 1
          end

          new_payments << p
        end
      else
        raise "invalid"
      end

      corrected_payments = []
      if loan.override_installment_interval == true
        temp_old_principal_balance = loan.old_principal_balance
        temp_old_interest_balance = loan.old_interest_balance

        loan.installment_override.times do |t|
          new_p = {}
          if temp_old_interest_balance <= payments.reverse.reverse[t][:interest]
            new_p[:interest] = temp_old_interest_balance
            temp_old_interest_balance = 0
          else
            new_p[:interest] = payments.reverse.reverse[t][:interest]
            temp_old_interest_balance -= new_p[:interest]
          end

          if temp_old_principal_balance <= payments.reverse.reverse[t][:principal]
            new_p[:principal] = temp_old_principal_balance
            temp_old_principal_balance = 0
          else
            new_p[:principal] = payments.reverse.reverse[t][:principal]
            temp_old_principal_balance -= new_p[:principal]
          end

          new_p[:balance] = new_p[:interest] + new_p[:principal]
          new_p[:amount] = payments.reverse.reverse[t][:amount]

          corrected_payments << new_p
        end

        # For remaining old interest and principal, add it to first amortization schedule
        if temp_old_interest_balance > 0
          corrected_payments[corrected_payments.size - 1][:interest] += temp_old_interest_balance
          corrected_payments[corrected_payments.size - 1][:amount] += temp_old_interest_balance
          corrected_payments[corrected_payments.size - 1][:balance] += temp_old_interest_balance
        end

        if temp_old_principal_balance > 0
          corrected_payments[corrected_payments.size - 1][:principal] += temp_old_principal_balance
          corrected_payments[corrected_payments.size - 1][:amount] += temp_old_principal_balance
          corrected_payments[corrected_payments.size - 1][:balance] += temp_old_principal_balance
        end

        corrected_payments = corrected_payments.reverse
      else
        corrected_payments = payments.reverse
      end

      total_balance = 0.00
      total_interest = 0.00
      temp_negative_interest  = 0.00

      corrected_payments.each_with_index do |cp, i|
        total_balance += cp[:principal]
        total_interest += cp[:interest]

        if cp[:interest] < 0
          temp_negative_interest += (cp[:interest] * -1)
          corrected_payments[i][:interest] = 0
        end
      end

      if temp_negative_interest > 0
        corrected_payments.each_with_index do |cp, i|
          if corrected_payments[i][:interest] - temp_negative_interest >= 0
            corrected_payments[i][:interest] -= temp_negative_interest
            temp_negative_interest  = 0
          end
        end
        #corrected_payments[0][:interest]  -=  temp_negative_interest
      end

      @schedule = {
        periodic_payment: monthly_payment,
        total_balance: total_principal,
        total_interest: total_interest,
        payments: corrected_payments
      }
    end

    def build_weekly!
      i_rate = (@interest_rate / @n_mode).to_s
      i_rate_cv = (i_rate[1,10]).to_f
      x = (((1 + i_rate_cv)**@term) - 1).to_f / (i_rate_cv*(1 + i_rate_cv)**@term).to_f
      d_factor = ((x+0.0000000000001).to_s[0,16]).to_f
      w_payment = (@principal / d_factor).round(0)
      total_balance = w_payment * @term
      total_interest = total_balance - @principal

      temp_amortization_entries = []
      payments = []


      @term.times do |t|
        t_interest = (@principal * i_rate_cv).round(0)
        t_principal = w_payment - t_interest
        t_balance = @principal - t_principal
        @principal = t_balance
        payments << {
          amount: w_payment,
          balance: t_balance,
          principal: t_principal,
          interest: t_interest
        }
      end

      last_payment = payments.last[:balance]
      payments = payments.reverse

      new_payments = []
      if last_payment > 0
        payments.each do |p|
          if last_payment > 0 
            p[:interest]=p[:interest]-1
            p[:principal]=p[:principal]+1
            p[:balance]=p[:balance]-last_payment
            last_payment=last_payment-1
          elsif last_payment < 0
            last_payment=last_payment*-1
          end 
          new_payments << p
        end

      elsif last_payment < 0
        last_payment1=last_payment * -1
        payments.each do |p|
          if last_payment1 > 0 
            p[:interest]=p[:interest]+1
            p[:principal]=p[:principal]-1
            p[:balance]=p[:balance]-last_payment1
            last_payment1=last_payment1-1
          end 
          new_payments << p
        end
      end 

      corrected_payments = []
      if loan.override_installment_interval == true
        temp_old_principal_balance = loan.old_principal_balance
        temp_old_interest_balance = loan.old_interest_balance

        loan.installment_override.times do |t|
          new_p = {}
          if temp_old_interest_balance <= payments.reverse.reverse[t][:interest]
            new_p[:interest] = temp_old_interest_balance
            temp_old_interest_balance = 0
          else
            new_p[:interest] = payments.reverse.reverse[t][:interest]
            temp_old_interest_balance -= new_p[:interest]
          end

          if temp_old_principal_balance <= payments.reverse.reverse[t][:principal]
            new_p[:principal] = temp_old_principal_balance
            temp_old_principal_balance = 0
          else
            new_p[:principal] = payments.reverse.reverse[t][:principal]
            temp_old_principal_balance -= new_p[:principal]
          end

          new_p[:balance] = new_p[:interest] + new_p[:principal]
          new_p[:amount] = payments.reverse.reverse[t][:amount]

          corrected_payments << new_p
        end

        # For remaining old interest and principal, add it to first amortization schedule
        if temp_old_interest_balance > 0
          corrected_payments[corrected_payments.size - 1][:interest] += temp_old_interest_balance
          corrected_payments[corrected_payments.size - 1][:amount] += temp_old_interest_balance
          corrected_payments[corrected_payments.size - 1][:balance] += temp_old_interest_balance
        end

        if temp_old_principal_balance > 0
          corrected_payments[corrected_payments.size - 1][:principal] += temp_old_principal_balance
          corrected_payments[corrected_payments.size - 1][:amount] += temp_old_principal_balance
          corrected_payments[corrected_payments.size - 1][:balance] += temp_old_principal_balance
        end

        corrected_payments = corrected_payments.reverse
      else
        corrected_payments = payments.reverse
      end

      total_balance           = 0.00
      total_interest          = 0.00
      temp_negative_interest  = 0.00

      corrected_payments.each_with_index do |cp, i|
        total_balance += cp[:principal]
        total_interest += cp[:interest]

        if cp[:interest] < 0
          temp_negative_interest += (cp[:interest] * -1)
          corrected_payments[i][:interest] = 0
        end
      end

      if temp_negative_interest > 0
        #corrected_payments[0][:interest]  -=  temp_negative_interest
        corrected_payments.each_with_index do |cp, i|
          if corrected_payments[i][:interest] - temp_negative_interest >= 0
            corrected_payments[i][:interest] -= temp_negative_interest
            temp_negative_interest  = 0
          end
        end
      end

      @schedule = { 
        periodic_payment: w_payment,
        total_balance: total_balance,
        total_interest: total_interest,
        payments: corrected_payments
      }
    end

    def build_non_interest!
      payments = []
      periodic_payment = (principal / @term).round(0)

      @term.times do |i|
        payments << {
          amount: periodic_payment,
          balance: periodic_payment,
          principal: periodic_payment,
          interest: 0.00
        }
      end

      total_balance = principal
      total_interest = 0.00

      corrected_payments = []
      if loan.override_installment_interval == true
        #raise "BALANCE: #{@loan.amount.to_f} OVERRIDE INTEREST RATE: #{@loan.override_interest_rate.to_f} TERM: #{@term} PRINCIPAL: #{principal.to_f} PERIODIC PAYMENT: #{periodic_payment.to_f}"

        if @loan.override_interest_rate == 0.00
          temp_periodic = (@loan.amount / @loan.installment_override).round(0)
          loan.installment_override.times do |t|
            new_p = {}
            new_p[:interest] = 0.00
            new_p[:principal] = temp_periodic

            corrected_payments << new_p
          end
        else
          temp_old_principal_balance = loan.old_principal_balance
          temp_old_interest_balance = loan.old_interest_balance

          loan.installment_override.times do |t|
            new_p = {}

            if temp_old_interest_balance <= payments.reverse.reverse[t][:interest]
              new_p[:interest] = temp_old_interest_balance
              temp_old_interest_balance = 0.00
            else
              new_p[:interest] = payments.reverse.reverse[t][:interest]
              temp_old_interest_balance -= new_p[:interest]
            end

            if temp_old_principal_balance <= payments.reverse.reverse[t][:principal]
              new_p[:principal] = temp_old_principal_balance
              temp_old_principal_balance -= new_p[:principal]
            else
              new_p[:principal] = payments.reverse.reverse[t][:principal]
              temp_old_principal_balance -= new_p[:principal]
            end

            new_p[:balance] = new_p[:interest] + new_p[:principal]

            new_p[:amount] = payments.reverse.reverse[t][:amount]

            corrected_payments << new_p
          end

          # For remaining old interest and principal, add it to first amortization schedule
          if temp_old_interest_balance > 0
            corrected_payments[corrected_payments.size - 1][:interest] += temp_old_interest_balance
            corrected_payments[corrected_payments.size - 1][:amount] += temp_old_interest_balance
            corrected_payments[corrected_payments.size - 1][:balance] += temp_old_interest_balance
          end

          if temp_old_principal_balance > 0
            corrected_payments[corrected_payments.size - 1][:principal] += temp_old_principal_balance
            corrected_payments[corrected_payments.size - 1][:amount] += temp_old_principal_balance
            corrected_payments[corrected_payments.size - 1][:balance] += temp_old_principal_balance
          end

          corrected_payments = corrected_payments.reverse
        end
      else
        corrected_payments = payments.reverse
      end

      total_balance = 0.00
      total_interest = 0.00
      temp_negative_interest  = 0.00

      corrected_payments.each_with_index do |cp, i|
        total_balance += cp[:principal]
        total_interest += cp[:interest]

        if cp[:interest] < 0
          temp_negative_interest += (cp[:interest] * -1)
          corrected_payments[i][:interest] = 0
        end
      end

      if temp_negative_interest > 0
        #corrected_payments[0][:interest]  -=  temp_negative_interest
        corrected_payments.each_with_index do |cp, i|
          if corrected_payments[i][:interest] - temp_negative_interest >= 0
            corrected_payments[i][:interest] -= temp_negative_interest
            temp_negative_interest  = 0
          end
        end
      end

      @schedule = {
        periodic_payment: periodic_payment,
        total_balance: total_balance,
        total_interest: total_interest,
        payments: corrected_payments
      }
    end

    def build_semi_monthly!
      @periodic_interest = (@interest_rate)

      #raise @periodic_interest.to_f.inspect
      d_factor = ((((1 + @periodic_interest)**@term)*@principal)*@periodic_interest)/(((1+@periodic_interest)**@term)-1) 
      monthly_payment = d_factor.round(0)

      total_interest = ((@term * monthly_payment) - @principal).round(0)
      total_principal = (@term * monthly_payment).round(0)

      payments = []
      temp_amortization_entries = []
      @term.times do |t|
        t_interest = (@principal * @periodic_interest).round(0)
        t_principal = monthly_payment - t_interest
        t_balance = @principal - t_principal
        @principal = t_balance
        payments << {
          amount: t_interest + t_principal,
          interest: t_interest,
          principal: t_principal,
          balance: t_balance
        }
      end

      last_payment = payments.last[:balance]
      payments = payments.reverse

      new_payments = []
      if last_payment > 0
        payments.each do |p|
          if last_payment > 0
            p[:interest] = p[:interest] - 1
            p[:principal] = p[:principal] + 1
            p[:balance]= p[:balance] - last_payment
            p[:amount] = p[:interest] + p[:principal]
            last_payment = last_payment - 1
          elsif last_payment < 0
            last_payment = last_payment * -1
          end
          new_payments << p
        end
      elsif last_payment < 0
        last_payment1= last_payment * -1
        payments.each do |p|
          if last_payment1 > 0
            p[:interest] = p[:interest] + 1
            p[:principal] = p[:principal] - 1
            p[:balance] = p
            p[:amount] = p[:interest] + p[:principal]
            last_payment1 = last_payment1 - 1
          end

          new_payments << p
        end
      else
        #raise "#{last_payment.to_f} invalid"
      end
      #raise new_payments.size.inspect

      corrected_payments = []
      if loan.override_installment_interval == true
        #raise "BALANCE: #{@loan.amount.to_f} OVERRIDE INTEREST RATE: #{@loan.override_interest_rate.to_f} TERM: #{@term} PRINCIPAL: #{principal.to_f} PERIODIC PAYMENT: #{periodic_payment.to_f}"

        if @loan.override_interest_rate == 0.00
          temp_periodic = (@loan.amount / @loan.installment_override).round(0)
          loan.installment_override.times do |t|
            new_p = {}
            new_p[:interest] = 0.00
            new_p[:principal] = temp_periodic

            corrected_payments << new_p
          end
        else
          temp_old_principal_balance = loan.old_principal_balance
          temp_old_interest_balance = loan.old_interest_balance

          loan.installment_override.times do |t|
            new_p = {}

            if temp_old_interest_balance <= payments.reverse.reverse[t][:interest]
              new_p[:interest] = temp_old_interest_balance
              temp_old_interest_balance = 0.00
            else
              new_p[:interest] = payments.reverse.reverse[t][:interest]
              temp_old_interest_balance -= new_p[:interest]
            end

            if temp_old_principal_balance <= payments.reverse.reverse[t][:principal]
              new_p[:principal] = temp_old_principal_balance
              temp_old_principal_balance -= new_p[:principal]
            else
              new_p[:principal] = payments.reverse.reverse[t][:principal]
              temp_old_principal_balance -= new_p[:principal]
            end

            new_p[:balance] = new_p[:interest] + new_p[:principal]

            new_p[:amount] = payments.reverse.reverse[t][:amount]

            corrected_payments << new_p
          end

          # For remaining old interest and principal, add it to first amortization schedule
          if temp_old_interest_balance > 0
            corrected_payments[corrected_payments.size - 1][:interest] += temp_old_interest_balance
            corrected_payments[corrected_payments.size - 1][:amount] += temp_old_interest_balance
            corrected_payments[corrected_payments.size - 1][:balance] += temp_old_interest_balance
          end

          if temp_old_principal_balance > 0
            corrected_payments[corrected_payments.size - 1][:principal] += temp_old_principal_balance
            corrected_payments[corrected_payments.size - 1][:amount] += temp_old_principal_balance
            corrected_payments[corrected_payments.size - 1][:balance] += temp_old_principal_balance
          end

          corrected_payments = corrected_payments.reverse
        end
      else
        corrected_payments = payments.reverse
      end

      total_balance = 0.00
      total_interest = 0.00
      temp_negative_interest  = 0.00

      corrected_payments.each_with_index do |cp, i|
        total_balance += cp[:principal]
        total_interest += cp[:interest]

        if cp[:interest] < 0
          temp_negative_interest += (cp[:interest] * -1)
          corrected_payments[i][:interest] = 0
        end
      end

      if temp_negative_interest > 0
        #corrected_payments[0][:interest]  -=  temp_negative_interest
        corrected_payments.each_with_index do |cp, i|
          if corrected_payments[i][:interest] - temp_negative_interest >= 0
            corrected_payments[i][:interest] -= temp_negative_interest
            temp_negative_interest  = 0
          end
        end
      end

      @schedule = {
        periodic_payment: monthly_payment,
        total_balance: total_principal,
        total_interest: total_interest,
        payments: corrected_payments
      }
    end
  end
end
