module Loans
  class FetchInvalidMaintainingBalanceLoans
    def initialize
      @invalid_loans = []
    end

    def execute!
      Loan.all.each do |loan|
        current_maintaining_balance = loan.maintaining_balance_val
        actual_maintaining_balance  = 0.00

        if loan.override_maintaining_balance == true
          actual_maintaining_balance = 0.00
        else
          # NOTE: Amount basis for override
          amount_basis = loan.amount

          if loan.override_installment_interval == true
            amount_basis = loan.original_loan_amount
          end

          if loan.loan_product.maintaining_balance_exception == true
            maintaining_balance_exception_minimum_amount = loan.loan_product.maintaining_balance_exception_minimum_amount.nil? ? 0 : loan.loan_product.maintaining_balance_exception_minimum_amount
            #if amount_basis >= loan.loan_product.maintaining_balance_exception_minimum_amount

            if amount_basis >= maintaining_balance_exception_minimum_amount
              actual_maintaining_balance = (loan.loan_product.default_maintaining_balance_val / 100) * amount_basis if (!amount_basis.nil? and !loan.loan_product.nil?)
            else
              actual_maintaining_balance = 0.00
            end
          else
            actual_maintaining_balance = (loan.loan_product.default_maintaining_balance_val / 100) * amount_basis if (!amount_basis.nil? and !loan.loan_product.nil?)
          end
        end

        if current_maintaining_balance != actual_maintaining_balance
          @invalid_loans << loan
        end
      end

      @invalid_loans
    end
  end
end
