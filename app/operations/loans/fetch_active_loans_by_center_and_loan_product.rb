module Loans
  class FetchActiveLoansByCenterAndLoanProduct
    def initialize(as_of:, center:, loan_product:)
      @as_of  = as_of.to_date
      @center = center
      @loan_product = loan_product
    end

    def execute!
      @previous_active_loans  = Loan.paid.where(
                                  "loans.date_approved <= ? AND loans.center_id = ? AND loans.loan_product_id = ?",
                                  @as_of,
                                  @center.id,
                                  @loan_product.id
                                ).where("id IN (?)", LoanPayment.approved.where("paid_at >= ?", @as_of).pluck(:loan_id).uniq)

      valid_ids = []
      @previous_active_loans.each do |pal|
        principal_paid  = pal.paid_principal_as_of(@as_of)
        interest_paid   = pal.paid_interest_as_of(@as_of)
        total_paid      = principal_paid + interest_paid
        loan_amount     = pal.amount
        interest_amount = pal.total_interest
        total_due       = loan_amount + interest_amount
        
        if total_paid < total_due
          valid_ids << pal.id
        end
      end

      @active_loans           = Loan.joins(:member)
                                  .where(
                                    "loans.date_approved <= ? AND loans.center_id = ? AND loans.loan_product_id =  ?",
                                    @as_of,
                                    @center.id,
                                    @loan_product.id
                                    #'active',
                                    #@previous_active_loans.pluck(:id).uniq
                                  ).where(
                                    "loans.status = ? OR loans.id IN (?)",
                                    'active',
                                    #@previous_active_loans.pluck(:id)
                                    valid_ids
                                  )
                                  .order("members.last_name")
      @active_loans
    end
  end
end
