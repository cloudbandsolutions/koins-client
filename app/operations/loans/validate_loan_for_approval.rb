module Loans
  class ValidateLoanForApproval
    def initialize(loan:, user:)
      @loan   = loan
      @user   = user
      @errors = []
    end

    def execute!
      check_authorization!
      check_params!
      @errors
    end

    private

    def check_authorization!
      if !["MIS", "BK"].include? @user.role
        @errors << "Unauthorized"
      end
    end

    def check_params!
      if @loan.status != "pending"
        @errors << "Cannot approve non-pending loan"
      end

      if @loan.first_date_of_payment.nil?
        @errors << "Cannot approve unamortized loan"
      end
    end
  end
end
