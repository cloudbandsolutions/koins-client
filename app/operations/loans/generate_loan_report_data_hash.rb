module Loans
  class GenerateLoanReportDataHash
    def initialize(loan:, as_of:)
      @loan       = loan
      @as_of      = as_of

      found_date = false
      @temp_as_of = Utils::DateUtil.new(date: @as_of.to_date).previous_business_day
      while found_date != true do
        if Holiday.where(holiday_at: @temp_as_of).count > 0
          @temp_as_of = Utils::DateUtil.new(date: @temp_as_of).previous_business_day
        else
          found_date = true
        end
      end
    end

    def execute!
      loan  = @loan
      principal_cum_due           = loan.temp_total_principal_amount_due_as_of(@as_of)      # actual
      temp_loan_cum_due           = loan.temp_total_amount_due_as_of(@temp_as_of)           # adjusted
      principal_amt_past_due      = loan.principal_amt_past_due_as_of(@temp_as_of)          # adjusted
      temp_principal_cum_due      = loan.temp_total_principal_amount_due_as_of(@temp_as_of) # adjusted

      loan_data                           = {}
      loan_data[:name]                    = loan.member.full_name_titleize
      loan_data[:member_id]               = loan.member.id
      loan_data[:loan_id]                 = loan.id
      loan_data[:center]                  = loan.member.center.to_s
      loan_data[:loan_product]            = loan.loan_product.to_s
      loan_data[:date_released]           = loan.date_released_formatted
      loan_data[:loan_amount]             = loan.amount
      loan_data[:interest_amount]         = loan.total_interest
      loan_data[:principal_paid]          = loan.paid_principal_as_of(@as_of)
      loan_data[:interest_paid]           = loan.paid_interest_as_of(@as_of)
      loan_data[:cum_due]                 = loan.temp_total_amount_due_as_of(@as_of)
      #loan_data[:principal_amt_past_due]  = principal_amt_past_due
      loan_data[:principal_amt_past_due]  = (loan_data[:principal_paid] - principal_cum_due).abs
      loan_data[:principal_cum_due]       = temp_principal_cum_due
      loan_data[:total_paid]              = loan_data[:principal_paid] + loan_data[:interest_paid]
      loan_data[:amt_past_due]            = (temp_loan_cum_due - loan_data[:total_paid])
      loan_data[:rr]                      = ((loan_data[:total_paid] / loan_data[:cum_due]) * 100).round(2)
      loan_data[:principal_rr]            = ((loan_data[:principal_paid] / loan_data[:principal_cum_due]) * 100).round(2)

      loan_data
    end
  end
end
