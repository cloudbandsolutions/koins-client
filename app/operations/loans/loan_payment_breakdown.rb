module Loans
  class LoanPaymentBreakdown
    def initialize(loan_id:)
      @loan_id = loan_id
      @loan_details = Loan.find(loan_id).loan_product_id
      @loan_product = LoanProduct.find(@loan_details).priority
      @insurancecode = InsuranceType.all
      @savingstypes = SavingsType.all.order("code DESC")
      @data = {}
      @data[:membername] = Loan.find(loan_id).member.full_name
      @data[:loanname] = Loan.find(loan_id).loan_product.name
      #raise @data[:loanname].inspect
      @data[:loanpayment] = []
      @data[:memberpayment] = []
      @data[:insurancedetails] = []
      @loanpayment = LoanPayment.where("loan_id = ?", @loan_id)
      
      #@data[:collectiontypeinsurance] =  CollectionTransaction.where("account_type = ?", "INSURANCE").order("account_type_code")
      
    end
    def execute!
      build_regular_payment
    end
    def build_regular_payment
      @loanpayment.each do |lp|
        tmp = {}
        tmp[:insurancedetails] = []
        tmp[:savingsdetails] = []
        tmp[:equity] = []
        tmp[:wpdetails] = []
        tmp[:paymentcollectionamount] = []
        tmp[:loanid] = lp.id
        tmp[:principal_amount] = lp.paid_principal
        tmp[:interest_amount] = lp.paid_interest
        tmp[:paid_at] = lp.paid_at
       
       paymentcollectionid = CollectionTransaction.where("loan_payment_id = ?",lp.id).pluck(:payment_collection_record_id)
      
        paymentcollectiondetails = PaymentCollectionRecord.find(paymentcollectionid)
  
        paymentcollectiondetails.each do |pcd|
          paymentcollectiontrans = {}
          paymentcollectiontrans[:transid] = pcd.payment_collection_id
          paymentcollectiontrans[:cashpayment] = pcd.total_cp_amount

          membercount = PaymentCollectionRecord.where(payment_collection_id: pcd.payment_collection_id, member_id: pcd.member_id).count          

          totalloanamount = PaymentCollectionRecord.joins(:collection_transactions).where("payment_collection_records.member_id = ? and payment_collection_records.payment_collection_id = ? and collection_transactions.account_type = 'LOAN_PAYMENT'", pcd.member_id,paymentcollectiontrans[:transid]).sum(:amount).to_f

          #raise paymentcollectionotherloans.inspect
          
          paymentcollectiontrans[:otherloantotal] = totalloanamount - (lp.paid_principal + lp.paid_interest)
        
          gettotalwp = PaymentCollectionRecord.joins(:collection_transactions).where("payment_collection_records.member_id = ? and payment_collection_records.payment_collection_id = ? and collection_transactions.account_type = 'WP'", pcd.member_id,paymentcollectiontrans[:transid]).sum(:amount).to_f

          
          forcashpaymentdetails = PaymentCollectionRecord.joins(:collection_transactions).where("payment_collection_records.member_id = ? and payment_collection_records.payment_collection_id = ? and collection_transactions.account_type <> 'WP'", pcd.member_id,paymentcollectiontrans[:transid]).sum(:amount).to_f
          
          if gettotalwp > forcashpaymentdetails
            totalcppayment = gettotalwp - forcashpaymentdetails
          elsif gettotalwp < forcashpaymentdetails
            totalcppayment = forcashpaymentdetails - gettotalwp
          else
            totalcppayment = 0.0
          end

          paymentcollectiontrans[:cashpaymenttotal] = totalcppayment
          tmp[:paymentcollectionamount] << paymentcollectiontrans
          
          #para sa insurance
          @insurancecode.each do |ic|
            dep = {}
            dep[:insuranceamount] = PaymentCollectionRecord.joins(:collection_transactions).where("payment_collection_records.member_id = ? and payment_collection_records.payment_collection_id=? and account_type_code = ?",pcd.member_id,paymentcollectiontrans[:transid],ic.code).sum(:amount)
            tmp[:insurancedetails] << dep
          end

           #end ng para sa insurance
          
          @savingstypes.each do |st|
            savingtransaction = {}
            savingtransaction[:savingamount] = PaymentCollectionRecord.joins(:collection_transactions).where("payment_collection_records.member_id = ? and payment_collection_records.payment_collection_id=? and account_type_code = ?",pcd.member_id,paymentcollectiontrans[:transid],st.code).sum(:amount)

           tmp[:savingsdetails] << savingtransaction
            
          end

            
          paymentcollectiontransactionwp = CollectionTransaction.where(payment_collection_record_id: pcd.id, account_type: 'EQUITY')
          paymentcollectiontransactionwp.each do |pcw|
            wptransaction = {}
            wptransaction[:wpdetails] = pcw.amount
            tmp[:equity] << wptransaction
          end

        
          paymentcollectiontransactionequity = PaymentCollectionRecord.joins(:collection_transactions).where("payment_collection_records.member_id = ? and payment_collection_records.payment_collection_id = ? and collection_transactions.account_type = 'WP'", pcd.member_id,paymentcollectiontrans[:transid]).sum(:amount).to_f
            
            equitytransaction = {}
            equitytransaction[:equitydetails] = paymentcollectiontransactionequity
            tmp[:wpdetails] << equitytransaction
        
        end

        @data[:memberpayment] << tmp
      end 
        @data
        
    end
   end
end
