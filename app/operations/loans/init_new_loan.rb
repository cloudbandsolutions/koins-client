module Loans
	class InitNewLoan
		def initialize(loan_product:, member:)
			@loan_product = loan_product
			@member = member
		end

		def execute!
			loan = Loan.new(amount: 0.00, term: "weekly", payment_type: "cash", book: "CDB", loan_product: @loan_product, bank: @member.branch.bank, bank_check_number: " ", voucher_particular: " ")

      loan
		end
	end
end
