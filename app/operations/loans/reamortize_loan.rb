module Loans
  class ReamortizeLoan
    def initialize(loan:, new_interest_rate:, new_num_installments:, new_term:, paid_at:, user:)
      @user                 = user
      @paid_at              = paid_at
      @loan                 = loan
      @interest_rate        = loan.interest_rate
      @num_installments     = loan.num_installments
      @new_interest_rate    = new_interest_rate
      @new_term             = new_term
      @new_num_installments = new_num_installments
      @total_payment        = loan.amount_paid
      @particular           = "Loan payment for reamortization of loan #{@loan.id}"
      @loan_payments        = LoanPayment.where(
                                "loan_id = ? AND is_void IS NULL AND (amount > 0 OR paid_principal > 0)",
                                loan.id
                              )

      @total_paid_principal = @loan_payments.sum(:paid_principal)
      @total_paid_interest  = @loan_payments.sum(:paid_interest)

      @current_amortization_schedule_entries  = AmmortizationScheduleEntry.where(
                                                  loan_id: @loan.id,
                                                  is_void: nil
                                                )
    end

    def execute!
      void_current_amortization_schedule
      generate_new_amortization_schedule
      @loan.update_remaining_balance!
      generate_negative_loan_payments
      generate_new_payment

      if @new_payment.paid_principal != @total_paid_principal or @new_payment.paid_interest != @total_paid_interest
        generate_accounting_entry
      end
    end

    private

    def generate_accounting_entry
      voucher = ::Loans::ProduceVoucherForCorrectiveLoanPayment.new(
                  loan_payment: @new_payment,
                  total_principal_paid: @total_paid_principal,
                  total_interest_paid:  @total_paid_interest,
                  user: @user
                ).execute!

      voucher.save!
      voucher = ::Accounting::ApproveVoucher.new(
                  voucher: voucher, 
                  user: @user
                ).execute!
    end

    def generate_new_payment
      @new_payment = LoanPayment.new(
                      amount: @total_payment,
                      cp_amount: @total_payment,
                      particular: @particular,
                      loan_payment_amount: @total_payment,
                      paid_at: @paid_at,
                      loan: @loan
                    )

      @new_payment.save!
      @new_payment.approve_payment!
    end

    def generate_new_amortization_schedule
      if @loan.override_installment_interval == true
        if  @loan.update(
              interest_rate: @new_interest_rate,
              installment_override: @new_num_installments,
              term: @new_term
            )

          ::Loans::InitializeAmortizationSchedule.new(loan: @loan).execute!
        else
          raise "Cannot update loan #{@loan.id}"
        end
      else
        if  @loan.update(
              interest_rate: @new_interest_rate,
              num_installments: @new_num_installments,
              term: @new_term
            )

          ::Loans::InitializeAmortizationSchedule.new(loan: @loan).execute!
        else
          raise "Cannot update loan #{@loan.id}"
        end
      end
    end

    def generate_negative_loan_payments
      @loan_payments.each do |loan_payment|
        ::Loans::GenerateNegativeLoanPayment.new(
          loan: @loan,
          paid_at: @paid_at,
          principal: loan_payment.paid_principal,
          interest: loan_payment.paid_interest
        ).execute!

        loan_payment.update!(is_void: true)
      end
    end

    def void_current_amortization_schedule
      @current_amortization_schedule_entries.each do |ase|
        ase.update!(is_void: true)
      end
    end
  end
end
