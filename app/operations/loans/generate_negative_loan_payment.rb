module Loans
  class GenerateNegativeLoanPayment
    def initialize(loan:, paid_at:, principal:, interest:)
      @loan           = loan
      @paid_at        = paid_at
      @paid_principal = principal * -1
      @paid_interest  = interest  * -1
      @cp_amount      = (@paid_principal + @paid_interest)
      @amount         = @cp_amount
      @particular     = "Negative payment for loan"
      @loan_payment   = LoanPayment.new(
                          loan: @loan,
                          paid_at: @paid_at,
                          amount: @amount,
                          cp_amount: @cp_amount,
                          status: 'approved',
                          particular: @particular,
                          paid_principal: @paid_principal,
                          paid_interest: @paid_interest,
                          is_void: true
                        )
    end

    def execute!
      @loan_payment.save!
    end
  end
end
