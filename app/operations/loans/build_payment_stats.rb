module Loans
  class BuildPaymentStats
    def initialize(amount:, loan:)
      @amount       = amount
      @loan         = loan
      @loan_product = loan.loan_product
    end

    def execute!
      temp_amount = @amount
      stats       = { principal: 0, interest: 0, ase: [], loan_product: @loan_product }
      ammortization_schedule  = @loan.ammortization_schedule_entries.where(paid: false).order(:created_at)

      ammortization_schedule.each do |as|
        if temp_amount > 0
          ase_paid_principal = 0.00
          ase_paid_interest = 0.00

          if as.interest_paid?
            remaining_principal = as.remaining_principal

            if temp_amount <= remaining_principal
              as.paid_principal += temp_amount

              if temp_amount == remaining_principal
                as.paid = true
              end

              stats[:principal] += temp_amount
              ase_paid_principal += temp_amount

              temp_amount = 0
            else
              as.paid_principal += remaining_principal
              as.paid = true

              stats[:principal] += remaining_principal
              ase_paid_principal += remaining_principal

              temp_amount -= remaining_principal
            end
          else
            remaining_interest = as.remaining_interest

            if temp_amount <= remaining_interest
              as.paid_interest += temp_amount

              stats[:interest] += temp_amount
              ase_paid_interest += temp_amount

              temp_amount = 0
            else
              as.paid_interest += remaining_interest

              stats[:interest] += remaining_interest
              ase_paid_interest += remaining_interest

              temp_amount -= remaining_interest

              remaining_principal = as.remaining_principal

              if temp_amount <= remaining_principal
                as.paid_principal += temp_amount

                if temp_amount == remaining_principal
                  as.paid = true
                end

                stats[:principal] += temp_amount
                ase_paid_principal += temp_amount

                temp_amount = 0
              else
                as.paid_principal += remaining_principal
                as.paid = true

                stats[:principal] += remaining_principal
                ase_paid_principal += remaining_principal

                temp_amount -= remaining_principal
              end
            end
          end

          stats[:ase] << { ase: as, principal_paid: ase_paid_principal, interest_paid: ase_paid_interest }
        end
      end

      stats
    end
  end
end
