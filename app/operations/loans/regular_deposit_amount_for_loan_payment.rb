module Loans
	class RegularDepositAmountForLoanPayment
		def initialize(loan_amount:)
			@loan_amount = loan_amount
		end

		def execute!
			a = 0
      if @loan_amount >= 1000 and @loan_amount < 2000
        a = 74
      elsif @loan_amount >= 2000 and @loan_amount < 3000
        a = 77
      elsif @loan_amount >= 3000 and @loan_amount < 4000
        a = 81
      elsif @loan_amount >= 4000 and @loan_amount < 5000
        a = 85
      elsif @loan_amount >= 5000 and @loan_amount < 6000
        a = 89
      elsif @loan_amount >= 6000 and @loan_amount < 7000
        a = 92
      elsif @loan_amount >= 7000 and @loan_amount < 8000
        a = 96
      elsif @loan_amount >= 8000 and @loan_amount < 9000
        a = 100
      elsif @loan_amount >= 9000 and @loan_amount < 10000
        a = 104
      elsif @loan_amount >= 10000 and @loan_amount < 11000
        a = 107
      elsif @loan_amount >= 11000 and @loan_amount < 12000
        a = 111
      elsif @loan_amount >= 12000 and @loan_amount < 13000
        a = 115
      elsif @loan_amount >= 13000 and @loan_amount < 14000
        a = 118
      elsif @loan_amount >= 14000 and @loan_amount < 15000
        a = 122
      elsif @loan_amount >= 15000 and @loan_amount < 16000
        a = 126
      elsif @loan_amount >= 16000 and @loan_amount < 17000
        a = 130
      elsif @loan_amount >= 17000 and @loan_amount < 18000
        a = 133
      elsif @loan_amount >= 18000 and @loan_amount < 19000
        a = 137
      elsif @loan_amount >= 19000 and @loan_amount < 20000
        a = 141
      elsif @loan_amount >= 20000
        a = 145
      end

      a
		end
	end
end