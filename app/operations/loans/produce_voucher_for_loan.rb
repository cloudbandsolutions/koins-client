module Loans
  class ProduceVoucherForLoan
    require 'application_helper'
    attr_accessor :loan, :voucher

    def initialize(loan:, user:)
      @loan             = loan
      @loan_product     = loan.loan_product
      @member           = loan.member
      @user             = user
      @amount_released  = @loan.amount
      @c_working_date   = ApplicationHelper.current_working_date
    end

    def execute!
      build_particular
      build_voucher

      if !@voucher.nil?
        @voucher.load_defaults
      end
      @voucher
    end

    def get_amount_released
      @amount_released
    end

    private

    def build_particular
    end

    def build_voucher
      if @loan.pending? or @loan.deleted?
        @voucher = Voucher.new(
                    book: @loan.book,
                    date_of_check: @loan.voucher_date_requested,
                    check_number: @loan.bank_check_number,
                    check_voucher_number: @loan.voucher_check_number,
                    payee: @loan.voucher_payee,
                   )
        build_debit_entries
        build_credit_entries
      else
        @voucher = Voucher.where(book: @loan.book, reference_number: @loan.voucher_reference_number).first
      end
    end

    def build_debit_entries
      # Loans Receivable
      entry = JournalEntry.new(
                amount: loan.amount,
                post_type: "DR",
                #accounting_code: loan.loan_product.amount_released_accounting_code
                accounting_code: loan.loan_product.accounting_code_transaction
              )

      @voucher.journal_entries << entry


     # build_others_fees_debit_entries #--> k-benepisyo ready
    end

    def build_credit_entries
      build_loan_deduction_entries
      build_membership_fees_entries

      #build_others_fees_credit_entries #--> k-benepisyo ready

      if @loan.override_installment_interval != true
        if @loan.override_insurance_deductions != true
          # Skip insurance deduction if special loan fund
          if @member.member_type_special_loan_fund? != true
            if @loan.insurance_as_disbursement_correction == true
              # Do the insurance as disbursement thing
              build_insurance_as_disbursement_entries
            else
              build_insurance_deduction_entries
            end
          else
            if @loan.insurance_as_disbursement_correction == true
              # Do the insurance as disbursement thing
              build_insurance_as_disbursement_entries
            end
          end
        end
      end

      if @loan.loan_product.waive_processing_fee_for_existing_entry_point_loans
        loan_entry_point = Loan.joins(:loan_product).where("loans.member_id = ? and loans.status = ? and loan_products.is_entry_point = ?", @member.id,'active',true).count
        #raise loan_entry_point.inspect
         if loan_entry_point == 0   
          build_processing_fee_deductions
        end
      else
        build_processing_fee_deductions
      end

      # Check if we have a flag to deposit equity on loans
      if Settings.deposit_equity_on_loans.present?
        if Settings.deposit_equity_on_loans == true
          build_equity_deductions
        end
      end

      build_amount_released
      build_other_voucher_params
    end


    def build_equity_deductions
      loan_deduction_accounting_code_id = Settings.equity_loan_deduction_accounting_code_id
      maximum_equity_amount             = Settings.equity_maximum_amount.try(:to_f)

      if loan_deduction_accounting_code_id and maximum_equity_amount
        current_equity_amount = @loan.member.equity_balance
        diff  = maximum_equity_amount - current_equity_amount
        if diff > 0
          amount  = diff
          entry   = JournalEntry.new(
                      post_type: "CR",
                      accounting_code: AccountingCode.find(loan_deduction_accounting_code_id),
                      amount: amount
                    )
          @amount_released -= entry.amount
          @voucher.journal_entries << entry
        end
      end
    end

    def build_loan_deduction_entry(lde)
      entry = JournalEntry.new(
                post_type: "CR",
                accounting_code: lde.accounting_code



              )

      if lde.is_percent and !lde.use_term_map
        p = (lde.val / 100)
        entry.amount = (p * loan.amount)
      elsif lde.is_percent and lde.use_term_map
        case loan.num_installments
        when 15
          p = lde.t_val_15
        when 25
          p = lde.t_val_25
        when 35
          p = lde.t_val_35
        when 50
          p = lde.t_val_50
        else
          if @loan.term == "semi-monthly" || @loan.term == "monthly"
            #p = lde.t_val_15
            p = lde.t_val_50
          else
            p = lde.t_val_25
          end
        end

        p = p / 100
        #@amount_released -= (p * loan.amount).round(2)
        entry.amount = (p * loan.amount).round(2)

        if @loan.term == "semi-monthly"
          entry.amount = (entry.amount / 2).round(2)
        end

      elsif lde.is_percent == false and lde.use_term_map == false
        #@amount_released -= lde.val
        entry.amount = lde.val
      elsif lde.is_percent == false and lde.use_term_map == true
        case loan.num_installments
        when 15
          p = lde.t_val_15
        when 16..25
          p = lde.t_val_25
        when 35
          p = lde.t_val_35
        when 50
          p = lde.t_val_50
        else
          #p = lde.t_val_25
          if @loan.term == "semi-monthly" || @loan.term == "monthly"
            p = lde.t_val_15
          else
            p = lde.t_val_25
          end
        end

        #@amount_released -= p
        entry.amount = p
      else
        raise "Invalid loan deduction entry"
      end

      @amount_released -= entry.amount
      @voucher.journal_entries << entry
    end

    def build_loan_deduction_entries
      cycle_count = MemberLoanCycle.where(member_id: @member.id, loan_product_id: @loan_product.id).first.try(:cycle)
      if cycle_count.nil?
        cycle_count = 0
      end

      @loan_product.loan_deduction_entries.each do |lde|
        if (lde.is_one_time_deduction == true and loan.loan_cycle_count > 1) or loan.override_installment_interval?
          # Do nothing
        else
          if @member.member_type_special_loan_fund?
            if lde.use_for_special_loan_fund == true
              build_loan_deduction_entry(lde)
            elsif lde.skip_for_special_loan_fund != true
              build_loan_deduction_entry(lde)
            end
          else
            if lde.use_for_special_loan_fund != true
              build_loan_deduction_entry(lde)
            end
          end
        end
      end
    end

    def build_membership_fees_entries
      # CR for membership fees
      # Credit to loan's bank's accounting code
      @loan_product.loan_product_membership_payments.each do |loan_product_membership_payment|
        membership_type = loan_product_membership_payment.membership_type
        if !loan.member.is_member?(membership_type) and membership_type.fee > 0
          je_credit = JournalEntry.new
          je_credit.post_type = "CR"
          #je_credit.accounting_code = @loan.bank.accounting_code
          je_credit.accounting_code = membership_type.cr_accounting_entry
          je_credit.amount = membership_type.fee
          @amount_released -= membership_type.fee

          @voucher.journal_entries << je_credit
        end
      end
    end

    def build_insurance_as_disbursement_entries
      # all amounts go to single accounting code defined in Settings.insurance_as_disbursement_accounting_code_id
      accounting_code = AccountingCode.find(Settings.insurance_as_disbursement_accounting_code_id)

      @loan_product.loan_insurance_deduction_entries.each do |loan_insurance_deduction_entry|
        if @member.has_insurance_account_type?(loan_insurance_deduction_entry.insurance_type)
          if @loan.loan_cycle_count > 1
            je_credit = JournalEntry.new
            je_credit.post_type = "CR"
            je_credit.accounting_code = accounting_code

            # Settings: num_additional_advanced_insurance_payments
            temp_adder = Settings.try(:num_additional_advanced_insurance_payments)
            if temp_adder.nil?
              temp_adder = 0
            end

            multiplier  = @loan.num_installments

            if @loan.term == "monthly"
              multiplier  = (multiplier * 4.3333333).ceil.to_i
            elsif @loan.term  ==  "semi-monthly"
              multiplier = multiplier * 2
            end

            #amount = loan_insurance_deduction_entry.val * (loan.num_installments + temp_adder)
            amount = loan_insurance_deduction_entry.val * (multiplier + temp_adder)
            je_credit.amount = amount

            @amount_released -= amount
            @voucher.journal_entries << je_credit
          else
            je_credit = JournalEntry.new
            je_credit.post_type = "CR"
            je_credit.accounting_code = accounting_code
            amount = loan_insurance_deduction_entry.val
            je_credit.amount = amount

            @amount_released -= amount
            @voucher.journal_entries << je_credit
          end
        end
      end
    end

    # NOTE: Add +1 for insurance of that week if loan_cycle_count > 1. Else, allow the member to pay for 1 week to be automatically insured
    def build_insurance_deduction_entries
      @loan_product.loan_insurance_deduction_entries.each do |loan_insurance_deduction_entry|
        if @member.has_insurance_account_type?(loan_insurance_deduction_entry.insurance_type)
          if @loan.loan_cycle_count > 1
            je_credit = JournalEntry.new
            je_credit.post_type = "CR"
            je_credit.accounting_code = loan_insurance_deduction_entry.accounting_code

            # Settings: num_additional_advanced_insurance_payments
            temp_adder = Settings.try(:num_additional_advanced_insurance_payments)
            if temp_adder.nil?
              temp_adder = 0
            end

            multiplier  = @loan.num_installments
            first_date_of_payment = @loan.first_date_of_payment

            if @loan.term == "monthly"
              temp_adder = 0
              multiplier  = (multiplier * 4.3333333).ceil.to_i
            elsif @loan.term  ==  "semi-monthly"
              # weird unique rule for 12 semi-monthly
              if @loan.num_installments == 12
                multiplier = 12.5 * 2
              elsif @loan.num_installments == 6
                multiplier = 15
              else
                multiplier = multiplier * 2
              end
            end

            #amount = loan_insurance_deduction_entry.val * (loan.num_installments + temp_adder)
            amount = loan_insurance_deduction_entry.val * (multiplier + temp_adder)

            je_credit.amount = amount

            @amount_released -= amount
            @voucher.journal_entries << je_credit
          else
            je_credit = JournalEntry.new
            je_credit.post_type = "CR"
            je_credit.accounting_code = loan_insurance_deduction_entry.accounting_code
            amount = loan_insurance_deduction_entry.val
            je_credit.amount = amount

            @amount_released -= amount
            @voucher.journal_entries << je_credit
          end
        end
      end
    end

    def build_processing_fee_deductions
      je_credit = JournalEntry.new
      je_credit.post_type = "CR"
      je_credit.accounting_code = loan.loan_product.accounting_code_processing_fee
      je_credit.amount = @loan.processing_fee
      
      @voucher.journal_entries << je_credit
      @amount_released -= @loan.processing_fee
    end

    def build_amount_released
      je_credit = JournalEntry.new
      je_credit.post_type = "CR"

      if @loan.override_installment_interval == true
        je_credit.accounting_code = AccountingCode.find(Settings.loans_override_installment_cash_accounting_code_id)
      else
        if @loan.loan_product.use_amount_released_accounting_code == true
          je_credit.accounting_code = @loan.loan_product.amount_released_accounting_code
        else
          je_credit.accounting_code = @loan.bank.accounting_code
        end
      end
      je_credit.amount = @amount_released
      
      @voucher.journal_entries << je_credit
    end

    def build_other_voucher_params
      # Name of Receiver
      @voucher.name_of_receiver = @member.to_s

      # Date approved
      @voucher.date_posted = Time.now
      # Voucher Parameter: Bank Check Number
      if !loan.bank_check_number.blank?
        v_bank_check_number = VoucherOption.new
        v_bank_check_number.name = "Bank Check Number"
        v_bank_check_number.val = @loan.bank_check_number

        @voucher.voucher_options << v_bank_check_number
      end

      # Voucher Parameter: Name on Check
      v_name_on_check = VoucherOption.new
      v_name_on_check.name = "Name on Check"
      v_name_on_check.val = @member.check_name

      @voucher.voucher_options << v_name_on_check

      # Voucher Parameter: Check Voucher Number
      if !loan.voucher_check_number.blank?
        v_check_voucher_number = VoucherOption.new
        v_check_voucher_number.name = "Check Voucher Number"
        v_check_voucher_number.val = @loan.voucher_check_number

        @voucher.voucher_options << v_check_voucher_number
      end

      # Voucher Parameter: Check Amount
      v_check_amount = VoucherOption.new
      v_check_amount.name = "Check Amount"
      v_check_amount.val = @amount_released

      @voucher.voucher_options << v_check_amount

      # Voucher Parameter: Date Requested
      v_date_requested = VoucherOption.new
      v_date_requested.name = "Date Requested"
      v_date_requested.val = @loan.voucher_date_requested.to_s

      @voucher.voucher_options << v_date_requested

      # Voucher Parameter: Bank
      v_bank = VoucherOption.new
      v_bank.name = "Bank"
      v_bank.val = @loan.bank.name

      @voucher.voucher_options << v_bank
      @voucher.date_prepared = @c_working_date
      @voucher.particular = @loan.voucher_particular
      @voucher.branch = @member.branch
      @voucher.approved_by = @user.full_name.upcase
    end


    def build_others_fees_credit_entries
      # CR for membership fees
      # Credit to loan's bank's accounting code
      loan_prouct_add =  Settings.loan_product_additional_entries
      loan_prouct_add.each do |lpa|
      
        if @loan_product.id == lpa.loan_product_id 
          lpa.particular.each do |lpa_particular|
            je_credit = JournalEntry.new
            je_credit.post_type = "CR"
          
            je_credit.accounting_code = AccountingCode.find(lpa_particular.credit_accounting_code)
            
            lpa_particular.terms_map.each do |term_map|
              if @loan.num_installments == term_map.term
                je_credit.amount = @loan.amount * term_map.val
              end
            end
        

            @voucher.journal_entries << je_credit
          end
        end
      end
    end


    def build_others_fees_debit_entries
      # CR for membership fees
      # Credit to loan's bank's accounting code
      loan_prouct_add =  Settings.loan_product_additional_entries
      loan_prouct_add.each do |lpa|
      
        if @loan_product.id == lpa.loan_product_id 
          lpa.particular.each do |lpa_particular|
            je_credit = JournalEntry.new
            je_credit.post_type = "DR"
          
            je_credit.accounting_code = AccountingCode.find(lpa_particular.debit_accounting_code)
            
            lpa_particular.terms_map.each do |term_map|
              if @loan.num_installments == term_map.term
                je_credit.amount = @loan.amount * term_map.val
              end
            end
        

            @voucher.journal_entries << je_credit
          end
        end
      end
    end
  end
end
