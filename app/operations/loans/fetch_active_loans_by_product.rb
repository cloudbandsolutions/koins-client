module Loans
  class FetchActiveLoansByProduct
    def initialize(loan_product_id:, as_of:)
      @loan_products  = LoanProduct.select("*")
      if loan_product_id
        @loan_products = @loan_products.where(id: loan_product_id)
      end
      @as_of          = as_of.to_date
      @active_loans   = ::Loans::FetchActiveLoans.new(as_of: @as_of).execute!
      @data           = {}
    end

    def execute!
      @data[:as_of]         = @as_of
      @data[:total_count]   = @active_loans.count
      @data[:loan_products] = []

      @loan_products.each do |loan_product|
        d = {}
        d[:loan_product]  = loan_product.name
        d[:total]         =  @active_loans.where(loan_product_id: loan_product.id).count

        if d[:total] > 0
          @data[:loan_products] << d
        end
      end

      @data
    end
  end
end
