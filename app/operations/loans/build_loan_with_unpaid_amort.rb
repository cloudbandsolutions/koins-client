module Loans
  class BuildLoanWithUnpaidAmort
    def initialize(filename:)
      filename = filename
      @record = []
      #@data = {}
      #@data[:list] = []
      @details = []
      csv_text = File.read(filename)
      @csv = CSV.parse(csv_text, headers: true)
      @csv_details = []       

    end

    def execute!
	    @csv.each do |row|
	      r= {
		    first_id: row['old_id'],
	      second_id: row['new_id']
	      }
        @csv_details << r	
      end
       
      data = {
        old_id: [] 
      
      }


     @csv_details.each do |cvd|
        @loan = Loan.find(cvd[:first_id])
        tmp = {
               old_id: Loan.find(cvd[:first_id]).id,
               id: cvd[:second_id] ,
               ammortization_schedule_entries: []
              }
        


      @loan.ammortization_schedule_entries.unpaid.each do |ase|
          tmp[:ammortization_schedule_entries] << {
            uuid: ase.uuid,
            amount: ase.amount,
            principal: ase.principal,
            interest: ase.interest,
            due_at: ase.due_at,
            paid_principal: 0.00,
            paid_interest: 0.00
          }
          
      
      end
      data[:old_id] << tmp
     end

  

      data[:old_id]

       
    end
  end
end
