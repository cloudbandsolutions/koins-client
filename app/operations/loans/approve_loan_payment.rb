module Loans
	class ApproveLoanPayment
		def initialize(loan_payment:, approved_by:, prepared_by:)
			@loan_payment = loan_payment
			@approved_by = approved_by
			@prepared_by = prepared_by
		end

		def execute!
			result = @loan_payment.approve_payment!

      # Perform withdrawal
      sat = nil
      if @loan_payment.has_wp_amount?
        voucher_number = Vouchers::VoucherNumber.new().execute!
        sat = SavingsAccountTransaction.create!(
                amount: @loan_payment.wp_amount,
                transaction_type: 'withdraw',
                is_withdraw_payment: true,
                particular: @loan_payment.particular,
                voucher_reference_number: voucher_number,
                savings_account: @loan_payment.savings_account
              )

        # Update the savings account
        sat.approve!(@approved_by)

        # Create voucher and accounting entry for WP
        jvb_voucher = Voucher.new(
                        particular: @loan_payment.wp_particular,
                        reference_number: voucher_number,
                        status: 'approved',
                        book: 'JVB',
                        branch_id: @loan_payment.loan.branch.id,
                        date_prepared: Time.now,
                        prepared_by: @prepared_by,
                        approved_by: @approved_by
                      )

        je_debit = JournalEntry.new(
                      amount: @loan_payment.wp_amount,
                      post_type: 'DR',
                      accounting_code: @loan_payment.savings_account.savings_type.withdraw_accounting_code
                    )
        
        jvb_voucher.journal_entries << je_debit

        je_credit = JournalEntry.new(
                      amount: @loan_payment.wp_amount,
                      post_type: 'CR',
                      accounting_code: @loan_payment.loan.bank.accounting_code
                    )

        jvb_voucher.journal_entries << je_credit

        jvb_voucher.save!
      end

      # main voucher number for reference
      voucher_number = Vouchers::VoucherNumber.new().execute!

      # Insurance deposits
      if @loan_payment.has_insurance_deposit?
        @loan_payment.loan_payment_insurance_account_deposits.each do |deposit|
          if deposit.amount > 0
            insurance_transaction = InsuranceAccountTransaction.create!(
                                      amount: deposit.amount,
                                      transaction_type: "deposit",
                                      particular: @loan_payment.particular,
                                      voucher_reference_number: voucher_number,
                                      insurance_account: deposit.insurance_account
                                    )

            insurance_transaction.approve!(@approved_by)
          end
        end
      end

      # Savings deposit
      if @loan_payment.has_deposit_amount?
        # filter only those with amounts
        new_savings_account_transactions = []
        old_savings_account_transactions = @loan_payment.savings_account_transactions
        old_savings_account_transactions.each do |savings_account_transaction|
          if savings_account_transaction.amount > 0
            new_savings_account_transactions << savings_account_transaction
          else
            savings_account_transaction.destroy!
          end
        end

        @loan_payment.savings_account_transactions = new_savings_account_transactions

        @loan_payment.savings_account_transactions.each do |savings_account_transaction|
          # Update the savings account
          savings_account_transaction.approve!(@approved_by)
        end
      else
        @loan_payment.savings_account_transactions.each do |savings_account_transaction|
          savings_account_transaction.destroy!
        end
      end

      # Main voucher
      crb_voucher = Voucher.new(
                      particular: @loan_payment.particular,
                      reference_number: voucher_number,
                      status: 'approved',
                      book: 'CRB',
                      branch_id: @loan_payment.loan.branch.id,
                      date_prepared: Time.now,
                      approved_by: @approved_by,
                      prepared_by: @prepared_by
                    )

      if result[:principal] > 0
        amount = result[:principal]

        je_credit_loans_receivable = JournalEntry.new(
                                        amount: amount,
                                        post_type: 'CR',
                                        accounting_code: @loan_payment.loan.loan_product.accounting_code_transaction
                                      )

        crb_voucher.journal_entries << je_credit_loans_receivable
      end

      if result[:interest] > 0
        amount = result[:interest]
        je_credit_interest_revenue = JournalEntry.new(
                                        amount: amount,
                                        post_type: 'CR',
                                        accounting_code: @loan_payment.loan.loan_product.interest_accounting_code
                                      )

        crb_voucher.journal_entries << je_credit_interest_revenue
      end

      # Insurance deposit main voucher accounting entry
      if @loan_payment.has_insurance_deposit?
        @loan_payment.loan_payment_insurance_account_deposits.each do |deposit|
          if deposit.amount > 0
            accounting_code = deposit.insurance_account.insurance_type.deposit_accounting_code
            je_credit_insurance_deposit = JournalEntry.new(
                                            amount: deposit.amount,
                                            post_type: 'CR',
                                            accounting_code: accounting_code
                                          )

            crb_voucher.journal_entries << je_credit_insurance_deposit
          end
        end
      end

      # Deposit amount accounting entry
      if @loan_payment.has_deposit_amount?
        @loan_payment.savings_account_transactions.each do |savings_account_transaction|
          je_credit_savings_deposit = JournalEntry.new(
                                        amount: savings_account_transaction.amount,
                                        post_type: 'CR',
                                        accounting_code: savings_account_transaction.savings_account.savings_type.deposit_accounting_code
                                      )

          crb_voucher.journal_entries << je_credit_savings_deposit
        end
      end

      # Amount for inclusion of wp_payment
      temp_amount = @loan_payment.amount

      je_debit_cash_in_bank = JournalEntry.new(
                                amount: temp_amount,
                                post_type: 'DR',
                                accounting_code: @loan_payment.loan.bank.accounting_code
                              )

      crb_voucher.journal_entries << je_debit_cash_in_bank

      crb_voucher.save!

      if @loan_payment.loan.remaining_balance == 0
        @loan_payment.loan.update!(status: 'paid')
        puts "Loan is finally paid"
      end
		end
	end
end