module Loans
  class ComputeLoanEir
    attr_accessor :loan, :voucher

    def initialize(loan:, user:)
      @loan = loan
      @user = user
      @voucher = Loans::ProduceVoucherForLoan.new(loan: @loan, user: @user).execute!
    end

    def execute!
      amount_released = 0.00
      if !loan.amount_released.nil?
        amount_released = loan.amount_released
      end
      eir = ((loan.amount - loan.amount_released) + loan.total_interest) / loan.amount

      eir
    end
  end
end
