module Loans
	class ValidateNewLoanApplicationWithEpCycleCountMaxAmounts
		def initialize(member:, loan:)
			@member = member
			@loan = loan
		end

		def execute!
			valid = true
      loan_product = @loan.loan_product
      amount = @loan.amount || 0.00

      if loan_product.is_entry_point != true
        loan_product.loan_product_ep_cycle_count_max_amounts.each do |loan_product_ep_cycle_count_max_amount|
          valid_cycle_count = loan_product_ep_cycle_count_max_amount.cycle_count
          max_amount = loan_product_ep_cycle_count_max_amount.max_amount

          Loan.active_entry_points.where("loans.member_id = ?", @member.id).each do |l|
            if l.loan_cycle_count < valid_cycle_count and amount > max_amount
              valid = false
            end
          end
        end
      end

      return valid
		end
	end
end