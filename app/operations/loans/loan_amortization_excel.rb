module Loans
	class LoanAmortizationExcel
		def initialize(loan:)
			@loan = loan
		end

		def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
          label_cell = wb.styles.add_style b: true, font_name: "Calibri"
          currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
          left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
          underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
          header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
          default_cell = wb.styles.add_style font_name: "Calibri"

          header_styles = [label_cell, nil, nil, label_cell, left_aligned_cell]

          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["#{Settings.company}"], style: title_cell
          sheet.merge_cells("A3:E3")
          sheet.add_row ["Loan Amortization Schedule"], style: title_cell
          sheet.merge_cells("A4:E4")

          sheet.add_row []

          sheet.add_row ["Client Beneficiary:", "#{@loan.member.to_s}", "", "Loan Term:", @loan.num_installments], style: header_styles
          sheet.merge_cells("B6:C6")

          sheet.add_row ["Center:", @loan.center.to_s, "", "Mode of Payment:", @loan.term], style: header_styles
          sheet.merge_cells("B7:C7")

          sheet.add_row ["Loan Type:", @loan.loan_product.to_s, "", "Interest Method:", "Declining Balance"], style: header_styles
          sheet.merge_cells("B8:C8")

          sheet.add_row ["Loan Amount:", @loan.amount, "", "Rate Per Month:", @loan.interest_rate_per_month / 100], style: [label_cell, currency_cell, default_cell, label_cell, percent_cell]
          sheet.merge_cells("B9:C9")

          sheet.add_row ["Interest Amount:", @loan.total_interest, "", "EIR:", @loan.eir], style: [label_cell, currency_cell, default_cell, label_cell, percent_cell]
          sheet.merge_cells("B10:C10")

          sheet.add_row ["Loan Deduction:", @loan.deduction_amount, "", "Date of First Payment", @loan.first_date_of_payment], style: [label_cell, underline_cell]
          sheet.merge_cells("B11:C11")

          sheet.add_row []

          sheet.add_row ["# of Period", "Due Date", "Principal Payment", "Interest Payment", "Balance", "Weekly Payment"], style: header_cells
          sheet.add_row [nil, nil, nil, nil, @loan.temp_total_amount_due, @loan.ammortization_schedule_entries[0].amount], style: [default_cell, default_cell, default_cell, currency_cell_right, currency_cell_right]

          temp_total_amount_due = @loan.temp_total_amount_due
          @loan.ammortization_schedule_entries.each_with_index do |ase, i|
            temp_total_amount_due -= ase.amount
            sheet.add_row [i+1, ase.due_at.nil? ? "" : ase.due_at.strftime("%b %d, %Y"), ase.principal, ase.interest, temp_total_amount_due, ase.amount], style: [default_cell, default_cell, currency_cell_right, currency_cell_right, currency_cell_right, currency_cell_right]
          end

          sheet.add_row ["TOTAL", @loan.amount, @loan.total_interest, nil, @loan.temp_total_amount_due], style: [default_cell, currency_cell_right, currency_cell_right, nil, currency_cell_right]

          sheet.page_setup.fit_to width: 1, height: 1
        end
      end

      p
		end
	end
end