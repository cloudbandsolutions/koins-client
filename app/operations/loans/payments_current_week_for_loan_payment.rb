module Loans
	class PaymentsCurrentWeekForLoanPayment
		def initialize
		end

		def execute!
			d = Date.today

      LoanPayment.where(status: "approved", paid_at: d.at_beginning_of_week..(d.at_beginning_of_week + 7.days)).sum(:amount)
		end
	end
end