module Loans
	class ReverseLoan
		def initialize(loan:)
			@loan = loan
      @member = loan.member
		end

		def execute!
			book = "CDB"
      if !@loan.book.blank?
        book = @loan.book
      end

      voucher = Voucher.where(book: book, reference_number: @loan.voucher_reference_number).first
      Accounting::GenerateReverseEntryForLoan.new(voucher: voucher, particular: "To reverse entry for member #{@loan.member} #{voucher.book} ##{voucher.reference_number} dated #{voucher.date_prepared.strftime("%m/%d/%Y")}").execute!

      # Generate a new voucher number for this loan
      new_voucher_number = Vouchers::VoucherNumber.new(book: book, branch: @loan.branch).execute!

      @loan.update!(status: "pending", voucher_reference_number: new_voucher_number)

      ActiveRecord::Base.transaction do

        # Update member's maintaining balance
        savings_account = SavingsAccount.where(member_id: @loan.member.id, savings_type: @loan.loan_product.savings_type.id).first
        Savings::UpdateMaintainingBalance.new(savings_account: savings_account).execute!

        # Update member cycle (we're sure we have at least + 1 cycle from previous loan)
        member_loan_cycle = MemberLoanCycle.where(member_id: @loan.member.id, loan_product_id: @loan.loan_product_id).first
        member_loan_cycle.update!(cycle: member_loan_cycle.cycle - 1)

        # Reverse any memberships paid by this loan
        @loan.loan_product.loan_product_membership_payments.each do |loan_product_membership_payment|
          membership_type = loan_product_membership_payment.membership_type
          MembershipPayment.where(member_id: @loan.member.id, loan_id: @loan.id, membership_type_id: membership_type.id, status: 'paid').destroy_all
        end

        # Reverse any insurance deposits on application
        if @loan.override_installment_interval != true
          if @loan.override_insurance_deductions != true
            if @member.member_type_special_loan_fund? != true
              if @loan.insurance_as_disbursement_correction == true
                # do nothing since it won't affect subsidiary
              else
                if member_loan_cycle.cycle > 1
                  num_installments = @loan.num_installments

                  if @loan.override_installment_interval != true
                    @loan.loan_product.loan_insurance_deduction_entries.each do |insurance_deduction|
                      insurance_type = insurance_deduction.insurance_type
                      insurance_account = InsuranceAccount.where(member_id: @loan.member.id, insurance_type_id: insurance_type.id).first

                      # Settings: num_additional_advanced_insurance_payments
                      temp_adder = Settings.try(:num_additional_advanced_insurance_payments)
                      if temp_adder.nil?
                        temp_adder = 0
                      end

                      amount = insurance_deduction.val * (num_installments + temp_adder)

                      insurance_account_transaction = InsuranceAccountTransaction.create!(
                                                        transaction_type: 'reverse_deposit',
                                                        insurance_account: insurance_account,
                                                        amount: amount,
                                                        voucher_reference_number: @loan.voucher_reference_number,
                                                        particular: 'REVERSED ENTRY',
                                                        status: 'reversed',
                                                        bank: @loan.bank,
                                                        accounting_code: @loan.bank.accounting_code
                                                      )

                      insurance_account_transaction.generate_updates!
                    end
                  end
                else
                  @loan.loan_product.loan_insurance_deduction_entries.each do |insurance_deduction|
                    insurance_type = insurance_deduction.insurance_type
                    insurance_account = InsuranceAccount.where(member_id: @loan.member.id, insurance_type_id: insurance_type.id).first
                    amount = insurance_deduction.val
                    insurance_account_transaction = InsuranceAccountTransaction.create!(
                                                      transaction_type: 'reverse_deposit',
                                                      insurance_account: insurance_account,
                                                      amount: amount,
                                                      voucher_reference_number: @loan.voucher_reference_number,
                                                      particular: 'REVERSED ENTRY',
                                                      status: 'reversed',
                                                      bank: @loan.bank,
                                                      accounting_code: @loan.bank.accounting_code
                                                    )

                    insurance_account_transaction.generate_updates!
                  end
                end
              end
            end
          end
        end
      end	
		end
	end
end
