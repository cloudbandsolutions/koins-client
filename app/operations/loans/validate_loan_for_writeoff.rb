module Loans
  class ValidateLoanForWriteoff
    def initialize(loan:)
      @loan   = loan
      @errors = []
    end

    def execute!
      check_status!
      @errors
    end

    private

    def check_status!
      if !@loan.active?
        @errors << "This loan is not active"
      end
    end
  end
end
