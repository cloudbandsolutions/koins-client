module Loans
  class Transaction
   def self.approve_loan!(loan, user)
      # Activate member
      if loan.member.status == "pending"
        loan.member.update!(status: "active")
      end

      # Update member's maintaining balance
      savings_account = SavingsAccount.where(member_id: loan.member.id, savings_type: loan.loan_product.savings_type.id).first
      Savings::UpdateMintainingBalance.new(savings_account: savings_account).execute!

      # Create voucher
      Loans::GenerateVoucher.new(loan: loan, user: user, is_temp: false).execute!

      # Update status
      loan.update!(status: "active", maturity_date: loan.ammortization_schedule_entries.last.due_at)

      # Pay for membership
      loan.loan_product.loan_product_membership_payments.each do |loan_product_membership_payment|
        membership_type = loan_product_membership_payment.membership_type

        if !loan.member.is_member?(membership_type)
          member_membership_payment = MembershipPayment.create!(status: 'paid', membership_type: membership_type, amount_paid: membership_type.fee, loan: loan, member: loan.member, paid_at: loan.date_prepared)
        end
      end

      # Update member cycle
      member_loan_cycle = MemberLoanCycle.where(member_id: loan.member.id, loan_product_id: loan.loan_product_id).first

      if loan.loan_cycle_count.nil?
        if member_loan_cycle.nil?
          member_loan_cycle = MemberLoanCycle.create!(member: loan.member, loan_product: loan.loan_product, cycle: 1)
          loan.update!(loan_cycle_count: member_loan_cycle.cycle)
        else
          member_loan_cycle.update!(cycle: member_loan_cycle.cycle + 1)
          loan.update!(loan_cycle_count: member_loan_cycle.cycle)
        end
      else
        if member_loan_cycle.nil?
          member_loan_cycle = MemberLoanCycle.create!(member: loan.member, loan_product: loan.loan_product, cycle: loan.loan_cycle_count)
        else
          member_loan_cycle.update!(cycle: loan.loan_cycle_count)
        end
      end

      # Deposit to Insurance Account
      Loans::DepositInsuranceOnApplication.new(loan: loan, user: user, cycle_count: member_loan_cycle.cycle).execute!
      #if member_loan_cycle.cycle > 1
      #  Loans::DepositInsuranceOnApplication.new(loan: loan, user: user, cycle_count: member_loan_cycle.cycle).execute!
      #end
    end
  end
end
