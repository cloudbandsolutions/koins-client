module Loans
  class WriteoffLoan
    def initialize(loan:, user:)
      @loan       = loan
      @user       = user
      @particular = "To write-off delinquent accounts"
    end

    def execute!
      if !@loan.active?
        raise "Cannot writeoff non-active loan"
      end

      dr_accounting_code = @loan.loan_product.writeoff_dr_accounting_code
      cr_accounting_code = @loan.loan_product.writeoff_cr_accounting_code
      book = "JVB"
      voucher = Voucher.new(
                  reference_number: Vouchers::VoucherNumber.new(book: "JVB", branch: @loan.branch).execute!,
                  book: "JVB",
                  date_prepared: Time.now,
                  branch: @loan.branch,
                  prepared_by: @user.full_name,
                  approved_by: @user.full_name,
                  status: "approved",
                  particular: @particular)

      # DR
      dr_journal_entry = JournalEntry.create!(
                          amount: @loan.remaining_balance, 
                          post_type: "DR",
                          accounting_code: dr_accounting_code)

      voucher.journal_entries << dr_journal_entry

      # CR
      cr_journal_entry = JournalEntry.create!(
                          amount: @loan.remaining_balance,
                          post_type: "CR",
                          accounting_code: cr_accounting_code)

      voucher.journal_entries << cr_journal_entry

      voucher.save!

      @loan.update!(status: "writeoff")

      @loan
    end
  end
end