module Loans
  class ValidateIndividualLoanPayment
    def initialize(amount:, loan:, particular:)
      @amount         = amount
      @loan           = loan
      @particular     = particular
      @center         = loan.center
      @errors         = []
    end

    def execute!
      validate_parameters!
      validate_pending_payments!

      @errors
    end

    private

    def validate_pending_payments!
      if PaymentCollection.billing.pending.where(center_id: @center.id).count > 0
        @errors << "There are still pending transactions for center #{@center.to_s}"
      end
    end

    def validate_parameters!
      if @amount.blank?
        @errors << "Amount required"
      end

      if @particular.blank?
        @errors << "Particular required"
      end
    end
  end
end
