module Loans
  class ApproveLoan
    attr_accessor :loan, :errors

    def initialize(loan:, user:)
      @loan             = loan
      @loan_product     = @loan.loan_product
      @member           = @loan.member
      @savings_account  = SavingsAccount.where(member_id: @member.id, savings_type: @loan_product.savings_type.id).first
      @user             = user
      @voucher          = Loans::ProduceVoucherForLoan.new(loan: @loan, user: @user).execute!
      @member_loan_cycle_count = 0
      @c_working_date     = ApplicationHelper.current_working_date
      @date_of_release   = loan.date_of_release
    end

    def execute!
      activate_member
      update_member_maintaining_balance
      pay_membership_fees
      update_member_cycle

      if @loan.override_installment_interval != true
        if @loan.override_insurance_deductions != true
          # Skip insurance deduction if special loan fund
          if @member.member_type_special_loan_fund? != true
            if @loan.insurance_as_disbursement_correction == true
              # do nothing since it won't affect subsidiary
            else
              deposit_insurance_deductions
            end
          end
        end
      end

      save_voucher

      # Check if we have a flag to deposit equity on loans
      if Settings.deposit_equity_on_loans.present?
        if Settings.deposit_equity_on_loans == true
          deposit_equity_deductions
        end
      end

      update_loan_status
    end

    private

    def deposit_equity_deductions
      loan_deduction_accounting_code_id = Settings.equity_loan_deduction_accounting_code_id
      maximum_equity_amount             = Settings.equity_maximum_amount.try(:to_f)

      if loan_deduction_accounting_code_id and maximum_equity_amount
        current_equity_amount = @loan.member.equity_balance
        diff  = maximum_equity_amount - current_equity_amount
        if diff > 0
          member          = @loan.member
          amount          = diff
          equity_type     = EquityType.first
          equity_account  = EquityAccount.where(member_id: member.id, equity_type_id: equity_type.id).first
          equity_account_transaction  = EquityAccountTransaction.new(
                                          equity_account: equity_account, 
                                          amount: amount, 
                                          transaction_type: "deposit",
                                          voucher_reference_number: @voucher.reference_number,
                                          transacted_at: @c_working_date,
                                          created_at: @c_working_date
                                        )

          equity_account_transaction.save!
          equity_account_transaction.approve!(@user.full_name)
        end
      end
    end

    def deposit_insurance_deductions
      if @member_loan_cycle_count > 1
        # Settings: num_additional_advanced_insurance_payments
        temp_adder = Settings.try(:num_additional_advanced_insurance_payments)
        if temp_adder.nil?
          temp_adder = 0
        end

        multiplier  = @loan.num_installments

        if @loan.term == "monthly"
          temp_adder = 0
          multiplier  = (multiplier * 4.3333333).ceil.to_i
        elsif @loan.term  ==  "semi-monthly"
          # weird unique rule for 12 semi-monthly
          if @loan.num_installments == 12
            multiplier = 12.5 * 2
          elsif @loan.num_installments == 6
            multiplier = 15
          else
            multiplier = multiplier * 2
          end
        end

        num_installments = (multiplier + temp_adder)
        
        @loan_product.loan_insurance_deduction_entries.each do |insurance_deduction|
          insurance_type = insurance_deduction.insurance_type
          insurance_account = InsuranceAccount.where(member_id: @member.id, insurance_type_id: insurance_type.id).first
          amount = insurance_deduction.val * num_installments
          
          if insurance_account.insurance_type_id == 1
            equity_amount = amount.to_f / 2
             
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            transaction_type: 'deposit',
                                            transacted_at: @c_working_date,
                                            created_at: @c_working_date,
                                            insurance_account: insurance_account,
                                            amount: amount,
                                            voucher_reference_number: @loan.voucher_reference_number,
                                            status: 'approved',
                                            bank: @loan.bank,
                                            accounting_code: @loan.bank.accounting_code,
                                            equity_value: insurance_account.equity_value + equity_amount
                                          )
            insurance_account.update!(equity_value: insurance_account_transaction.ending_balance.to_f / 2)
            insurance_account_transaction.generate_updates!  
          else
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            transaction_type: 'deposit',
                                            transacted_at: @c_working_date,
                                            created_at: @c_working_date,
                                            insurance_account: insurance_account,
                                            amount: amount,
                                            voucher_reference_number: @loan.voucher_reference_number,
                                            status: 'approved',
                                            bank: @loan.bank,
                                            accounting_code: @loan.bank.accounting_code
                                          )
            insurance_account_transaction.generate_updates!
          end
        end
      else
        if @loan.override_installment_interval != true
          @loan_product.loan_insurance_deduction_entries.each do |insurance_deduction|
            insurance_type = insurance_deduction.insurance_type
            insurance_account = InsuranceAccount.where(member_id: loan.member.id, insurance_type_id: insurance_type.id).first
            amount = insurance_deduction.val

            if insurance_account.insurance_type_id == 1
              equity_amount = amount.to_f / 2
              
              insurance_account_transaction = InsuranceAccountTransaction.create!(
                                              transaction_type: 'deposit',
                                              transacted_at: @c_working_date,
                                              created_at: @c_working_date,
                                              insurance_account: insurance_account,
                                              amount: amount,
                                              voucher_reference_number: @loan.voucher_reference_number,
                                              status: 'approved',
                                              bank: @loan.bank,
                                              accounting_code: @loan.bank.accounting_code,
                                              equity_value: equity_amount
                                              )

              insurance_account.update!(equity_value: equity_amount)

              insurance_account_transaction.generate_updates!
            else

              insurance_account_transaction = InsuranceAccountTransaction.create!(
                                              transaction_type: 'deposit',
                                              transacted_at: @c_working_date,
                                              created_at: @c_working_date,
                                              insurance_account: insurance_account,
                                              amount: amount,
                                              voucher_reference_number: @loan.voucher_reference_number,
                                              status: 'approved',
                                              bank: @loan.bank,
                                              accounting_code: @loan.bank.accounting_code)

              insurance_account_transaction.generate_updates!
            end
          end
        end
      end
    end

    def update_member_cycle
      member_loan_cycle = MemberLoanCycle.where(member_id: @member.id, loan_product_id: @loan_product_id).first

      if @loan.loan_cycle_count.nil?
        if member_loan_cycle.nil?
          member_loan_cycle = MemberLoanCycle.create!(member: @member, loan_product: @loan_product, cycle: 1)
          @loan.update!(loan_cycle_count: member_loan_cycle.cycle)
        else
          member_loan_cycle.update!(cycle: member_loan_cycle.cycle + 1)
        end

        @loan.update!(loan_cycle_count: member_loan_cycle.cycle)
      else
        if member_loan_cycle.nil?
          member_loan_cycle = MemberLoanCycle.create(member: loan.member, loan_product: loan.loan_product, cycle: loan.loan_cycle_count)
        else
          member_loan_cycle.update!(cycle: loan.loan_cycle_count)
        end
      end

      @member_loan_cycle_count = member_loan_cycle.cycle
    end

    def pay_membership_fees
      @loan_product.loan_product_membership_payments.each do |loan_product_membership_payment|
        membership_type = loan_product_membership_payment.membership_type

        if !@date_of_release.nil?
          @paid_at = @date_of_release
        else
          @paid_at = @c_working_date
        end

        if !loan.member.is_member?(membership_type)
          member_membership_payment = MembershipPayment.create!(
                                        status: 'paid', 
                                        membership_type: membership_type, 
                                        amount_paid: membership_type.fee, 
                                        loan: loan, 
                                        member: loan.member, 
                                        paid_at: @paid_at
                                      )
        end
      end
    end

    def update_loan_status
      @loan.update!(status: "active", maturity_date: @loan.ammortization_schedule_entries.last.due_at)
    end

    def save_voucher
      @voucher.save!
      @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
      @loan.update!(voucher_reference_number: @voucher.reference_number, date_approved: @voucher.date_posted)
    end

    def activate_member
      if @member.status == "pending"
        @member.update!(status: "active")
      end

      if @member.insurance_status == "pending" 
        @member.update!(insurance_status: "inforce")
      end

      if @member.previous_mii_member_since.nil?
        if !@date_of_release.nil?
          @member.update!(previous_mii_member_since: @date_of_release)
        else
          @member.update!(previous_mii_member_since: @c_working_date)
        end
      end
    end

    def update_member_maintaining_balance
      Membership::UpdateMemberMaintainingBalance.new(member: @member, savings_account: @savings_account).execute!
    end
  end
end
