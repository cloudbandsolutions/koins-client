module Loans
	class GenerateReversedVouchersForLoanPayment
		def initilize(loan_payment:)
			@loan_payment = loan_payment
		end

		def execute!
			reversed_vouchers = []
      loan_payment_voucher = Voucher.where(reference_number: @loan_payment.voucher_reference_number).first

      reversed_voucher = Accounting::GenerateReverseEntry.new(loan_payment_voucher: loan_payment_voucher).execute!
      reversed_vouchers << reversed_voucher

      LoanPaymentReversedVoucher.create!(loan_payment: @loan_payment, voucher: reversed_voucher)

      wp_voucher = Voucher.where(reference_number: @loan_payment.wp_voucher_reference_number).first

      if !wp_voucher.nil?
        reversed_voucher =Accounting::GenerateReverseEntry.new(wp_voucher: wp_voucher).execute!
        reversed_vouchers << reversed_voucher
        LoanPaymentReversedVoucher.create!(loan_payment: @loan_payment, voucher: reversed_voucher)
      end

      reversed_vouchers
		end
	end
end