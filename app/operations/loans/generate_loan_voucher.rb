module Loans
  class GenerateLoanVoucher
  	def initialize(loan:, voucher:)
  		@loan = loan
  		@voucher = voucher
  	end

  	def execute!
  		p = Axlsx::Package.new
      wb = p.workbook
      wb.add_worksheet(name: "Loan Voucher") do |sheet|
        sheet.add_row(["Voucher for Loan #{@loan}"])
        sheet.add_row(["Reference Number", "#{@voucher.reference_number}"])
        sheet.add_row(["Date", @voucher.date_prepared.strftime("%B %d, %Y")])
        sheet.add_row(["Accounting Code", "Debit", "Credit"])
        @voucher.journal_entries.where(post_type: "DR").each do |entry|
          sheet.add_row([entry.accounting_code, entry.amount, ""])
        end
        @voucher.journal_entries.where(post_type: "CR").each do |entry|
          sheet.add_row([entry.accounting_code, "", entry.amount])
        end
        sheet.add_row(["Particular", @voucher.particular])
      end

      p
  	end
  end
end