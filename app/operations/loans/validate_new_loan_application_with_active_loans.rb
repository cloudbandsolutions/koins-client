module Loans
	class ValidateNewLoanApplicationWithActiveLoans
		def initialize(member:, loan_product:)
			@member = member
			@loan_product = loan_product
		end

		def execute!
			if Loan.where(member_id: @member.id, status: "active", loan_product_id: @loan_product.id).count > 0 and @loan_product.is_entry_point == true
        return false
      end

      return true
		end
	end
end