module Loans  
  class LoanOperation

    N_YEARLY = 1
    N_SEMI_ANNUALY = 2
    N_QUARTERLY = 4
    N_MONTHLY = 12
    N_WEEKLY = 52
    N_DAILY = 365

    def self.compute_amortization_schedule(loan, interest_rate, principal, term, mode_of_payment)
      # If loan is overriden, make sure we set the principal to the original amount and term to the original number of installments
      if loan.override_installment_interval == true
        principal = loan.original_loan_amount 
        term = loan.original_num_installments
      end

      if interest_rate > 0
        periodic_interest = nil
        n_mode = nil

        case mode_of_payment
        when "yearly"
          periodic_interest = interest_rate / N_YEARLY
          n_mode = N_YEARLY
        when "semi-annually"
          periodic_interest = interest_rate / N_SEMI_ANNUALLY
          n_mode = N_SEMI_ANNUALLY
        when "quarterly"
          periodic_interest = interest_rate / N_QUARTERLY
          n_mode = N_QUARTERLY
        when "monthly"
          periodic_interest = interest_rate / N_MONTHLY
          n_mode = N_MONTHLY
        when "weekly"
          periodic_interest = interest_rate / N_WEEKLY
          n_mode = N_WEEKLY
        when "daily"
          periodic_interest = interest_rate / N_DAILY
          n_mode = N_DAILY
        else
          raise "invalid mode of payment"
        end

        amount_prin=principal
        no_payment=term

        iRate=(interest_rate/n_mode).to_s
        iRate_cv=(iRate[1,10]).to_f #interest rate computation

        x = (((1 + iRate_cv) ** no_payment)-1).to_f / (iRate_cv*(1+iRate_cv)**no_payment).to_f #f

        dFactor= ((x+0.0000000000001).to_s[0,16]).to_f #discount factor computation

        wPayment= (amount_prin/dFactor).round(0) #weekly payment
        total_balance= wPayment * no_payment #pag compute ng total balance
        total_interest= total_balance-amount_prin # pag compute ng total interest

        temp_ammortization_entries = [] #dagdag

        payments = []

        temp_ammortization_entries=[]
        no_payment.times do |t|
          tInterest=(amount_prin * iRate_cv).round(0)
          tPrincipal=wPayment-tInterest
          tBalance=amount_prin-tPrincipal
          amount_prin=tBalance
          payments << {
            amount: wPayment,
            balance:tBalance,
            principal:tPrincipal,
            interest:tInterest
          }
        end

        #puts payments.last[:balance]

        last_payment=payments.last[:balance]
        payments=payments.reverse

        new_payments=[]
        if last_payment > 0
          payments.each do |p|
            if last_payment > 0 
              p[:interest]=p[:interest]-1
              p[:principal]=p[:principal]+1
              p[:balance]=p[:balance]-last_payment
              last_payment=last_payment-1
            elsif last_payment < 0
              last_payment=last_payment*-1
            end 
            new_payments << p
          end
        elsif last_payment < 0
          last_payment1=last_payment * -1
          payments.each do |p|
            if last_payment1 > 0 
              p[:interest]=p[:interest]+1
              p[:principal]=p[:principal]-1
              p[:balance]=p[:balance]-last_payment1
              last_payment1=last_payment1-1
            end 
            new_payments << p
          end
        end 

        corrected_payments = []
        if loan.override_installment_interval == true
          temp_old_principal_balance = loan.old_principal_balance
          temp_old_interest_balance = loan.old_interest_balance

          loan.installment_override.times do |t|
            new_p = {}
            if temp_old_interest_balance <= payments.reverse.reverse[t][:interest]
              new_p[:interest] = temp_old_interest_balance
              temp_old_interest_balance = 0
            else
              new_p[:interest] = payments.reverse.reverse[t][:interest]
              temp_old_interest_balance -= new_p[:interest]
            end

            if temp_old_principal_balance <= payments.reverse.reverse[t][:principal]
              new_p[:principal] = temp_old_principal_balance
              temp_old_principal_balance = 0
            else
              new_p[:principal] = payments.reverse.reverse[t][:principal]
              temp_old_principal_balance -= new_p[:principal]
            end

            new_p[:balance] = new_p[:interest] + new_p[:principal]
            new_p[:amount] = payments.reverse.reverse[t][:amount]

            corrected_payments << new_p
          end

          # For remaining old interest and principal, add it to first amortization schedule
          if temp_old_interest_balance > 0
            corrected_payments[corrected_payments.size - 1][:interest] += temp_old_interest_balance
            corrected_payments[corrected_payments.size - 1][:amount] += temp_old_interest_balance
            corrected_payments[corrected_payments.size - 1][:balance] += temp_old_interest_balance
          end

          if temp_old_principal_balance > 0
            corrected_payments[corrected_payments.size - 1][:principal] += temp_old_principal_balance
            corrected_payments[corrected_payments.size - 1][:amount] += temp_old_principal_balance
            corrected_payments[corrected_payments.size - 1][:balance] += temp_old_principal_balance
          end

          corrected_payments = corrected_payments.reverse
        else
          corrected_payments = payments.reverse
        end

        total_balance = 0.00
        total_interest = 0.00

        corrected_payments.each do |cp|
          total_balance += cp[:principal]
          total_interest += cp[:interest]
        end

        schedule = { 
          periodic_payment: wPayment,
          total_balance: total_balance,
          total_interest: total_interest,
          payments: corrected_payments
        }
      else
        payments = []
        periodic_payment = (principal / term).round(0)
        term.times do |i|
          payments << {
            amount: periodic_payment,
            balance: periodic_payment,
            principal: periodic_payment,
            interest: 0.00
          }
        end

        total_balance = principal
        total_interest = 0

        corrected_payments = []
        if loan.override_installment_interval == true
          temp_old_principal_balance = loan.old_principal_balance
          temp_old_interest_balance = loan.old_interest_balance

          loan.installment_override.times do |t|
            new_p = {}

            if temp_old_interest_balance <= payments.reverse.reverse[t][:interest]
              new_p[:interest] = temp_old_interest_balance
              temp_old_interest_balance = 0
            else
              new_p[:interest] = payments.reverse.reverse[t][:interest]
              temp_old_interest_balance -= new_p[:interest]
            end

            if temp_old_principal_balance <= payments.reverse.reverse[t][:principal]
              new_p[:principal] = temp_old_principal_balance
              temp_old_principal_balance -= new_p[:principal]
            else
              new_p[:principal] = payments.reverse.reverse[t][:principal]
              temp_old_principal_balance -= new_p[:principal]
            end

            new_p[:balance] = new_p[:interest] + new_p[:principal]

            new_p[:amount] = payments.reverse.reverse[t][:amount]

            corrected_payments << new_p
          end

          # For remaining old interest and principal, add it to first amortization schedule
          if temp_old_interest_balance > 0
            corrected_payments[corrected_payments.size - 1][:interest] += temp_old_interest_balance
            corrected_payments[corrected_payments.size - 1][:amount] += temp_old_interest_balance
            corrected_payments[corrected_payments.size - 1][:balance] += temp_old_interest_balance
          end

          if temp_old_principal_balance > 0
            corrected_payments[corrected_payments.size - 1][:principal] += temp_old_principal_balance
            corrected_payments[corrected_payments.size - 1][:amount] += temp_old_principal_balance
            corrected_payments[corrected_payments.size - 1][:balance] += temp_old_principal_balance
          end

          corrected_payments = corrected_payments.reverse
        else
          corrected_payments = payments.reverse
        end

        total_balance = 0.00
        total_interest = 0.00

        corrected_payments.each do |cp|
          total_balance += cp[:principal]
          total_interest += cp[:interest]
        end

        schedule = {
          periodic_payment: periodic_payment,
          total_balance: total_balance,
          total_interest: total_interest,
          payments: corrected_payments
        }
      end
    end

    def self.total_payable
      total = 0.00

      Loan.all.each do |l|
        total += l.remaining_balance
      end

      total
    end

    def self.approved_loan_payments(loan)
      LoanPayment.where(loan_id: loan.id, status: "approved")
    end

    def self.loan_payments(loan)
      #LoanPayment.where(loan_id: loan.id).order("created_at DESC")
      LoanPayment.where(
        "loan_id = ? AND is_void IS NULL AND status = ? AND (paid_principal + paid_interest) > 1",
        loan.id,
        "approved"
      ).order("paid_at ASC")
    end

    def self.has_active_loan(member, loan_product)
      if Loan.active.where(member_id: member.id, loan_product_id: loan_product.id).count > 0
        return true
      else
        return false
      end
    end

    # if cycle_count > 1 pay for insurance for the extra week
    # else pay for the first week
    def self.deposit_insurance_on_application(loan, user, cycle_count)
      if cycle_count > 1
        num_installments = (loan.num_installments + 1)

        loan.loan_product.loan_insurance_deduction_entries.each do |insurance_deduction|
          insurance_type = insurance_deduction.insurance_type
          insurance_account = InsuranceAccount.where(member_id: loan.member.id, insurance_type_id: insurance_type.id).first
          amount = insurance_deduction.val * num_installments

          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            transaction_type: 'deposit',
                                            insurance_account: insurance_account,
                                            amount: amount,
                                            voucher_reference_number: loan.voucher_reference_number,
                                            status: 'approved',
                                            bank: loan.bank,
                                            accounting_code: loan.bank.accounting_code)

          insurance_account_transaction.generate_updates!
        end
      else
        loan.loan_product.loan_insurance_deduction_entries.each do |insurance_deduction|
          insurance_type = insurance_deduction.insurance_type
          insurance_account = InsuranceAccount.where(member_id: loan.member.id, insurance_id: insurance_type.id).first
          amount = insurance_deduction.val

          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            transaction_type: 'deposit',
                                            insurance_account: insurance_account,
                                            amount: amount,
                                            voucher_reference_number: loan.voucher_reference_number,
                                            status: 'approved',
                                            bank: loan.bank,
                                            accounting_code: loan.bank.accounting_code)

          insurance_account_transaction.generate_updates!
        end
      end
    end

    def self.generate_voucher!(loan, user, is_temp)
      voucher = Voucher.new

      #============= START DEBIT SECTION (amount received by Member)
      je_debit = JournalEntry.new
      je_debit.amount = loan.amount
      je_debit.post_type = "DR"
      je_debit.accounting_code = loan.loan_product.amount_released_accounting_code

      voucher.journal_entries << je_debit

      #============= END DEBIT SECTION

      #============= START CREDIT SECTION
      cycle_count = 0
      if !MemberLoanCycle.where(member_id: loan.member.id, loan_product_id: loan.loan_product.id).first.try(:cycle).nil?
        cycle_count = MemberLoanCycle.where(member_id: loan.member.id, loan_product_id: loan.loan_product.id).first.try(:cycle)
      end

      amount_released = loan.amount
      loan.loan_product.loan_deduction_entries.each do |lde|
        if lde.is_one_time_deduction == true and cycle_count <= 1
          je_credit = JournalEntry.new
          je_credit.post_type = "CR"
          je_credit.accounting_code = lde.accounting_code
          if lde.is_percent == true
            p = (lde.val / 100)
            amount_released -= (p * loan.amount)
            je_credit.amount = (p * loan.amount)
          else
            amount_released -= lde.val
            je_credit.amount = lde.val
          end

          voucher.journal_entries << je_credit
        end
      end

      # For insurance account
      loan.loan_product.loan_insurance_deduction_entries.each do |loan_insurance_deduction_entry|
        if loan.member.has_insurance_account_type?(loan_insurance_deduction_entry.insurance_type)
          if cycle_count > 1
            je_credit = JournalEntry.new
            je_credit.post_type = "CR"
            je_credit.accounting_code = loan_insurance_deduction_entry.accounting_code
            je_credit.amount = loan_insurance_deduction_entry.val * loan.num_installments
            amount_released -= loan_insurance_deduction_entry.val * loan.num_installments

            voucher.journal_entries << je_credit
          end
        end
      end

      # Processing fee
      je_credit = JournalEntry.new
      je_credit.post_type = "CR"
      je_credit.accounting_code = loan.loan_product.accounting_code_processing_fee
      je_credit.amount = loan.processing_fee
      
      voucher.journal_entries << je_credit

      amount_released -= loan.processing_fee

      je_credit = JournalEntry.new
      je_credit.post_type = "CR"
      je_credit.accounting_code = loan.loan_product.amount_released_accounting_code
      je_credit.amount = amount_released
      
      voucher.journal_entries << je_credit

      #============ END CREDIT SECTION

      # Name of Receiver
      voucher.name_of_receiver = loan.member.to_s

      # Voucher Parameter: Name on Check
      v_name_on_check = VoucherOption.new
      v_name_on_check.name = "Name on Check"
      v_name_on_check.val = loan.member.check_name

      voucher.voucher_options << v_name_on_check

      # Voucher Parameter: Bank Check Number
      v_bank_check_number = VoucherOption.new
      v_bank_check_number.name = "Bank Check Number"
      v_bank_check_number.val = loan.bank_check_number

      voucher.voucher_options << v_bank_check_number

      # Voucher Parameter: Check Voucher Number
      v_check_voucher_number = VoucherOption.new
      v_check_voucher_number.name = "Check Voucher Number"
      v_check_voucher_number.val = loan.voucher_check_number

      voucher.voucher_options << v_check_voucher_number

      # Voucher Parameter: Check Amount
      v_check_amount = VoucherOption.new
      v_check_amount.name = "Check Amount"
      v_check_amount.val = loan.amount_released

      voucher.voucher_options << v_check_amount

      # Voucher Parameter: Date Requested
      v_date_requested = VoucherOption.new
      v_date_requested.name = "Date Requested"
      v_date_requested.val = loan.voucher_date_requested.to_s

      voucher.voucher_options << v_date_requested

      # Voucher Parameter: Bank
      v_bank = VoucherOption.new
      v_bank.name = "Bank"
      v_bank.val = loan.bank.name

      voucher.voucher_options << v_bank

      voucher.reference_number = loan.voucher_reference_number
      voucher.date_prepared = loan.date_prepared
      voucher.particular = loan.voucher_particular
      voucher.book = "CDB"
      voucher.branch = loan.member.branch
      voucher.status = "approved"
      voucher.approved_by = user.full_name.upcase

      # save voucher  if is_temp flag is set to false
      if is_temp == false
        voucher.save!
      end

      voucher
    end
  end
end
