module Loans
  class ApproveNegativeLoanPayment
    def initialize(negative_loan_payment:)
      @negative_loan_payment  = negative_loan_payment
      @loan                   = negative_loan_payment.loan
      @schedule               = @loan.ammortization_schedule_entries.where(is_void: nil).order("due_at DESC")
      @affected_schedule      = []

      @schedule.each do |ase|
        if ase.amount != ase.remaining_balance
          @affected_schedule << ase
        end
      end
    end

    def execute!
      @affected_schedule
    end
  end
end
