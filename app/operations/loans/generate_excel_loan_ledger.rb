module Loans
  class GenerateExcelLoanLedger
    def initialize(loan:, user_prepared_by:)
      @loan                   = loan
      @p                      = Axlsx::Package.new
      @temp_total_amount_due  = @loan.temp_total_amount_due
      @total_interest         = @loan.total_interest
      @user_prepared_by       = user_prepared_by
      @user_approved_by       = User.where(role: "FM").first
      @voucher        = Loans::ProduceVoucherForLoan.new(
                          loan: @loan, 
                          user: @user_approved_by
                        ).execute!
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)

          header              = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center              = wb.styles.add_style(alignment: {horizontal: :center})
          left_aligned_cell   = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
          label_cell          = wb.styles.add_style b: true, font_name: "Calibri"
          currency_cell       = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          percent_cell        = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
          header_cells        = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
          header_styles       = [label_cell, nil, nil, nil, nil, label_cell, left_aligned_cell]
          entry_cell          = [nil, @currency_cell_right, @currency_cell_right, nil, @currency_cell_right, @currency_cell_right, nil]
          unpaid_entry_cell   = [nil, nil, nil, nil, @currency_cell_right, nil, nil]

          sheet.add_row [Settings.company], style: @title_cell
          sheet.merge_cells("A1:G1")
          sheet.add_row ["Loan Ledger"], style: @title_cell
          sheet.merge_cells("A2:G2")

          sheet.add_row ["Member:", "#{@loan.member.list_name}", "", "", "", "Loan Term:", @loan.num_installments], style: header_styles
          sheet.add_row ["Center:", @loan.center.to_s, "", "", "", "Mode of Payment:", @loan.term], style: header_styles
          sheet.add_row ["Loan Type:", @loan.loan_product.to_s, "", "", "", "Interest Method:", "Declining Balance"], style: header_styles
          sheet.add_row ["Cycle:", @loan.loan_cycle_count, "", "", "", "Rate per Annum:", "#{@loan.interest_rate}%"], style: header_styles

          sheet.add_row ["Loan Amount:", @loan.amount, "", "", "", "Date Released:", "#{@loan.date_released_formatted} (Posted: #{@voucher.date_posted.strftime("%b-%d-%Y")})"], 
                          style: [@label_cell, @currency_cell_right, nil, nil, nil, @label_cell, left_aligned_cell]
          sheet.add_row ["Interest Amount:", @loan.total_interest, "", "", "", "Maturity Date:", @loan.maturity_date_formatted], 
                          style: [@label_cell, @currency_cell_right, nil, nil, nil, @label_cell, left_aligned_cell]

          sheet.add_row []
          sheet.add_row ["Transaction Date", "Principal", "Interest", "", "Amount", "Running Balance", "Transaction Type"], style: @title_cell

          # Add row for release
          sheet.add_row [@loan.date_released_formatted, @loan.amount, @total_interest, "", @temp_total_amount_due, @temp_total_amount_due, "Release"], style: entry_cell

          # Amortization
          total_paid_principal = 0
          total_paid_interest = 0
          ::Loans::LoanOperation.loan_payments(@loan).each_with_index do |loan_payment, i|
            total_paid_principal += loan_payment.paid_principal
            total_paid_interest += loan_payment.paid_interest

            @temp_total_amount_due -= loan_payment.loan_payment_amount
            sheet.add_row [loan_payment.paid_at.strftime("%B-%d-%Y"), loan_payment.paid_principal, loan_payment.paid_interest, "", loan_payment.loan_payment_amount, @temp_total_amount_due, "Payment"], style: entry_cell
          end

#          @loan.ammortization_schedule_entries.unpaid.order(:due_at).each_with_index do |ase, i|
#            sheet.add_row [ase.due_at.strftime("%B-%d-%Y"), "", "", "", ase.remaining_balance, "", ""], style: unpaid_entry_cell
#          end

          # Footer
          sheet.add_row ["Loan Amount:", @loan.amount, @total_interest, "", "", @loan.amount + @total_interest], 
                          style: [label_cell, @currency_cell_right_total, @currency_cell_right_total, nil, nil, @currency_cell_right_total]
          sheet.add_row ["Payments:", total_paid_principal, total_paid_interest, "", "", total_paid_principal + total_paid_interest],
                          style: [label_cell, @currency_cell_right_total, @currency_cell_right_total, nil, nil, @currency_cell_right_total]
          sheet.add_row ["Balance:", @loan.amount - total_paid_principal, @total_interest - total_paid_interest, "", "", (@loan.amount + @total_interest) - (total_paid_principal + total_paid_interest)], 
                          style: [@label_cell, @currency_cell_right_total, @currency_cell_right_total, nil, nil, @currency_cell_right_total]

          sheet.add_row []
          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["Prepared By:", @user_prepared_by.full_name, "", "", "Approved By:", @user_approved_by.try(:full_name)]
          sheet.add_row ["", @user_prepared_by.role, "", "", "", @user_approved_by.try(:role)]
        end
      end

      @p
    end

    private

    def initialize_formats!(wb)
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri"
    end
  end
end
