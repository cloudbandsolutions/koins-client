module Loans
	class ValidateSimilarLoanProductLoanApplication
		def initialize(member:, loan_product:)
			@member = member
			@loan_product = loan_product
		end

		def execute!
			if Loan.where(member_id: @member.id, status: "pending", loan_product_id: @loan_product.id).count > 0
        return false
      end

      return true
		end
	end
end