module Loans
  class RepairUnpaidLoan
    def initialize(loan:)
      @loan = loan
    end

    def execute!
      if @loan.old_interest_balance.present? and loan.ammortization_schedule_entries.paid.count == 0
        interest_balance        = @loan.interest_balance
        old_interest_balance    = @loan.old_interest_balance
        old_principal_balance   = @loan.old_principal_balance
        amount                  = @loan.amount
        remaining_balance       = @loan.remaining_balance
        true_remaining_balance  = old_principal_balance + old_interest_balance
        principal_balance       = @loan.principal_balance
        old_principal_balance   = @loan.old_principal_balance

        if remaining_balance != true_remaining_balance
          diff_remaining_balance  = remaining_balance - true_remaining_balance
          puts "Loan #{@loan.id} has invalid remaining balance: Remaining Balance: #{remaining_balance} True Remaining Balance: #{true_remaining_balance}"
          puts "Repairing remaining balance..."
          @loan.update!(remaining_balance: true_remaining_balance)
        end

        if @loan.old_interest_balance > 0 and interest_balance != old_interest_balance
          diff_interest = interest_balance - old_interest_balance
          first_amort     = @loan.ammortization_schedule_entries.unpaid.order("due_at ASC").first
          first_interest  = first_amort.interest
          puts "Loan #{@loan.id} with interest balance #{interest_balance} != #{old_interest_balance} (difference: #{diff_interest}) and no payments yet"
          puts "Interest of first amortization: #{first_interest}"
          puts "Reparing interest..."
          first_amort.update!(interest: (first_interest - diff_interest))
        end
      end
    end
  end
end
