module Loans
  class LoanVoucher
    def self.get_voucher(loan, user)
      v = nil

      if loan.status == "pending"
        v = Loans::GenerateVoucher.new(loan: loan, user: user, is_temp: true).execute!
      else
        book = "CDB"
        if !loan.book.blank?
          book = loan.book
        end

        v = Voucher.where(book: book, reference_number: loan.voucher_reference_number, branch_id: loan.branch.id).first
      end
    end

    def self.generate_voucher!(loan, user, is_temp)
      voucher = Voucher.new

      #============= START DEBIT SECTION (amount received by Member)
      loans_receivable = loan.amount

      # Loans receivable
      je_debit = JournalEntry.new
      je_debit.amount = loans_receivable
      je_debit.post_type = "DR"
      #je_debit.accounting_code = loan.bank.accounting_code
      je_debit.accounting_code = loan.loan_product.amount_released_accounting_code

      voucher.journal_entries << je_debit

      #============= END DEBIT SECTION

      #============= START CREDIT SECTION
      cycle_count = 0
      if !MemberLoanCycle.where(member_id: loan.member.id, loan_product_id: loan.loan_product.id).first.try(:cycle).nil?
        cycle_count = MemberLoanCycle.where(member_id: loan.member.id, loan_product_id: loan.loan_product.id).first.try(:cycle)
      end

      amount_released = loan.amount
      loan.loan_product.loan_deduction_entries.each do |lde|
        if lde.is_one_time_deduction == true and loan.loan_cycle_count > 1
          # Do nothing
        else
          je_credit = JournalEntry.new
          je_credit.post_type = "CR"
          je_credit.accounting_code = lde.accounting_code

          if lde.is_percent == true and lde.use_term_map == false
            p = (lde.val / 100)
            amount_released -= (p * loan.amount)
            je_credit.amount = (p * loan.amount)
          elsif lde.is_percent == true and lde.use_term_map == true
            case loan.num_installments
            when 5
              p = lde.t_val_5
            when 10
              p = lde.t_val_10
            when 15
              p = lde.t_val_15
            when 25
              p = lde.t_val_25
            when 35
              p = lde.t_val_35
            when 50
              p = lde.t_val_50
            else
              p = 0.00
            end

            p = p / 100
            amount_released -= (p * loan.amount)
            je_credit.amount = (p * loan.amount)
          elsif lde.is_percent == false and lde.use_term_map == false
            amount_released -= lde.val
            je_credit.amount = lde.val
          elsif lde.is_percent == false and lde.use_term_map == true
            case loan.num_installments
            when 5
              p = lde.t_val_5
            when 10
              p = lde.t_val_10
            when 15
              p = lde.t_val_15
            when 25
              p = lde.t_val_25
            when 35
              p = lde.t_val_35
            when 50
              p = lde.t_val_50
            else
              p = 0.00
            end

            amount_released -= p
            je_credit.amount = p
          else
            raise "Invalid loan deduction entry"
          end

          voucher.journal_entries << je_credit
        end
      end

      # CR for membership fees
      # Credit to loan's bank's accounting code
      loan.loan_product.loan_product_membership_payments.each do |loan_product_membership_payment|
        membership_type = loan_product_membership_payment.membership_type
        if !loan.member.is_member?(membership_type) and membership_type.fee > 0
          je_credit = JournalEntry.new
          je_credit.post_type = "CR"
          #je_credit.accounting_code = membership_type.cr_accounting_entry
          je_credit.accounting_code = loan.bank.accounting_code
          je_credit.amount = membership_type.fee
          amount_released -= membership_type.fee

          voucher.journal_entries << je_credit
        end
      end

      # For insurance account
      # NOTE: Add +1 for insurance of that week if loan_cycle_count > 1. Else, allow the member to pay for 1 week to be automatically insured
      loan.loan_product.loan_insurance_deduction_entries.each do |loan_insurance_deduction_entry|
        if loan.member.has_insurance_account_type?(loan_insurance_deduction_entry.insurance_type)
          if loan.loan_cycle_count > 1
            je_credit = JournalEntry.new
            je_credit.post_type = "CR"
            je_credit.accounting_code = loan_insurance_deduction_entry.accounting_code
            amount = loan_insurance_deduction_entry.val * (loan.num_installments + 1)
            je_credit.amount = amount
            amount_released -= amount

            voucher.journal_entries << je_credit
          else
            je_credit = JournalEntry.new
            je_credit.post_type = "CR"
            je_credit.accounting_code = loan_insurance_deduction_entry.accounting_code
            amount = loan_insurance_deduction_entry.val
            je_credit.amount = amount
            amount_released -= amount

            voucher.journal_entries << je_credit
          end
        end
      end

      #je_credit = JournalEntry.new
      #je_credit.post_type = "CR"
      #je_credit.accounting_code = loan.loan_product.accounting_code_processing_fee
      #je_credit.amount = loan.processing_fee
      
      #voucher.journal_entries << je_credit

      #amount_released -= loan.processing_fee

      # Amount released
      je_credit = JournalEntry.new
      je_credit.post_type = "CR"
      #je_credit.accounting_code = loan.loan_product.amount_released_accounting_code
      je_credit.accounting_code = loan.bank.accounting_code
      je_credit.amount = amount_released
      
      voucher.journal_entries << je_credit

      #============ END CREDIT SECTION

      # Name of Receiver
      voucher.name_of_receiver = loan.member.to_s

      # Date approved
      voucher.date_posted = Time.now

      # Voucher Parameter: Bank Check Number
      if !loan.bank_check_number.blank?
        v_bank_check_number = VoucherOption.new
        v_bank_check_number.name = "Bank Check Number"
        v_bank_check_number.val = loan.bank_check_number

        voucher.voucher_options << v_bank_check_number
      end

      # Voucher Parameter: Name on Check
      v_name_on_check = VoucherOption.new
      v_name_on_check.name = "Name on Check"
      v_name_on_check.val = loan.member.check_name

      voucher.voucher_options << v_name_on_check

      # Voucher Parameter: Check Voucher Number
      if !loan.voucher_check_number.blank?
        v_check_voucher_number = VoucherOption.new
        v_check_voucher_number.name = "Check Voucher Number"
        v_check_voucher_number.val = loan.voucher_check_number

        voucher.voucher_options << v_check_voucher_number
      end

      # Voucher Parameter: Check Amount
      v_check_amount = VoucherOption.new
      v_check_amount.name = "Check Amount"
      v_check_amount.val = loan.amount_released

      voucher.voucher_options << v_check_amount

      # Voucher Parameter: Date Requested
      v_date_requested = VoucherOption.new
      v_date_requested.name = "Date Requested"
      v_date_requested.val = loan.voucher_date_requested.to_s

      voucher.voucher_options << v_date_requested

      # Voucher Parameter: Bank
      v_bank = VoucherOption.new
      v_bank.name = "Bank"
      v_bank.val = loan.bank.name

      voucher.voucher_options << v_bank

      puts "Voucher Options: #{voucher.voucher_options.inspect}"

      #voucher.date_prepared = loan.date_prepared
      voucher.date_prepared = Time.now
      voucher.particular = loan.voucher_particular

      book = "CDB"

      if !loan.book.blank?
        book = loan.book
      end

      #raise voucher.voucher_options.inspect

      voucher.book = book
      voucher.branch = loan.member.branch
      voucher.status = "approved"
      voucher.approved_by = user.full_name.upcase

      voucher.load_defaults

      # save voucher  if is_temp flag is set to false
      if is_temp == false
        voucher.reference_number = Vouchers::VoucherNumber.new(book: book, branch: loan.branch).execute!
        voucher.save!
      end

      # Updated the loan
      loan.update!(amount_released: amount_released, voucher_reference_number: voucher.reference_number)

      voucher
    end
  end
end
