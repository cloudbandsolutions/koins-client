module Loans
  class FetchJournalEntries
    def initialize(config:)
      @config = config

      @accounting_code_id = @config[:accounting_code_id]
      @branch             = @config[:branch]
      @account_subtype    = @config[:account_subtype]
      
      @data = {
        branch: {
          id: @branch.id,
          name: @branch.name
        },
        records: []
      }
    end

    def execute!
      query!

      @data[:records] = @result.map{ |r|
                          id                                = r.fetch("loan_id")
                          principal                         = r.fetch("principal").to_f.round(2)
                          first_date_of_payment             = r.fetch("first_date_of_payment")
                          maturity_date                     = r.fetch("maturity_date")
                          voucher_id                        = r.fetch("voucher_id")
                          journal_entry_id                  = r.fetch("journal_entry_id")
                          amount                            = r.fetch("amount").to_f.round(2)
                          loan_product_id                   = r.fetch("loan_product_id")
                          loan_product_name                 = r.fetch("loan_product_name")
                          date_of_release                   = r.fetch("date_of_release")
                          date_approved                     = r.fetch("date_approved")
                          member_id                         = r.fetch("member_id")
                          reference_number                  = r.fetch("reference_number")
                          book                              = r.fetch("book")
                          insurance_account_id              = r.fetch("insurance_account_id")
                          num_installments                  = r.fetch("num_installments")
                          term                              = r.fetch("term")
                          insurance_account_transaction_id  = r.fetch("insurance_account_transaction_id")
                          status                            = r.fetch("status")

                          {
                            id: id,
                            principal: principal,
                            first_date_of_payment: first_date_of_payment,
                            maturity_date: maturity_date,
                            voucher_id: voucher_id,
                            journal_entry_id: journal_entry_id,
                            amount: amount,
                            loan_product_id: loan_product_id,
                            loan_product_name: loan_product_name,
                            member_id: member_id,
                            date_approved: date_approved,
                            date_of_release: date_of_release,
                            reference_number: reference_number,
                            book: book,
                            insurance_account_id: insurance_account_id,
                            term: term,
                            num_installments: num_installments,
                            insurance_account_transaction_id: insurance_account_transaction_id,
                            status: status
                          }
                        }
      @data
    end

    private

    def query!
      @result = ActiveRecord::Base.connection.execute(<<-EOS).to_a
                  SELECT
                    loans.id AS loan_id,
                    loans.amount AS principal,
                    loans.first_date_of_payment,
                    loans.maturity_date,
                    vouchers.id AS voucher_id,
                    vouchers.book,
                    vouchers.reference_number,
                    journal_entries.id AS journal_entry_id,
                    journal_entries.amount,
                    loan_products.id AS loan_product_id,
                    loan_products.name AS loan_product_name,
                    loans.date_approved,
                    loans.date_of_release,
                    loans.num_installments,
                    loans.term,
                    loans.status,
                    members.id AS member_id,
                    insurance_accounts.id AS insurance_account_id,
                    insurance_account_transactions.id AS insurance_account_transaction_id
                  FROM
                    loans
                  INNER JOIN members
                    ON
                      members.id = loans.member_id
                  INNER JOIN loan_products
                    ON
                      loan_products.id = loans.loan_product_id
                  INNER JOIN vouchers
                    ON
                      vouchers.reference_number = loans.voucher_reference_number
                      AND
                      vouchers.book = loans.book
                      AND
                      vouchers.status = 'approved'
                      AND
                      vouchers.branch_id = '#{@branch.id}'
                  INNER JOIN insurance_accounts
                    ON
                      insurance_accounts.member_id = members.id
                  INNER JOIN insurance_types
                    ON
                      insurance_types.id = insurance_accounts.insurance_type_id
                      AND
                      insurance_types.name = '#{@account_subtype}'
                  INNER JOIN journal_entries
                    ON
                      journal_entries.voucher_id = vouchers.id
                      AND
                      journal_entries.accounting_code_id = '#{@accounting_code_id}'
                  LEFT JOIN insurance_account_transactions
                    ON insurance_account_transactions.insurance_account_id = insurance_accounts.id
                    AND insurance_account_transactions.status = 'approved'
                    AND ROUND(insurance_account_transactions.amount, 2) = ROUND(journal_entries.amount, 2)
                    AND DATE(insurance_account_transactions.transacted_at) = DATE(loans.date_approved)
                  WHERE
                    loans.status IN ('active', 'paid') AND loans.branch_id = '#{@branch.id}'
                EOS
    end
  end
end
