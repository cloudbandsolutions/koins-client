module Loans
  class ComputeAmountReleased
    def initialize(loan:)
      @loan             = loan
      @amount_released  = @loan.amount
      @loan_product     = loan.loan_product
      @voucher          = Loans::ProduceVoucherForLoan.new(loan: @loan, user: User.first).execute!
    end

    def execute!
      if @voucher
        @voucher.voucher_options.each do |vo|
          if vo.name == "Check Amount"
            @amount_released = vo.val
          end
        end
      end

      @amount_released
    end
  end
end
