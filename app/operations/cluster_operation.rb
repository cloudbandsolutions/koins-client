class ClusterOperation
  MASTER_SERVER = Settings.master_server
  API_KEY = Settings.api_key
  PARAMS = { api_key: API_KEY }
  CLUSTER_PING_URL = "/api/v1/utils/cluster_ping"

  def self.cluster_ping!
    cluster = Cluster.where(id: Settings.cluster_id).first
    url = "#{MASTER_SERVER}#{CLUSTER_PING_URL}"
    Rails.logger.info "URL: #{url}"
    http = Curl.post(url, { api_key: API_KEY, code: cluster.code })
    if http.response_code == 200
      Rails.logger.info "Successfully pinged cluster"
    else
      Rails.logger.error "Error in cluster ping for cluster #{cluster.inspect}. Reply: #{JSON.parse(http.body_str, symbolize_names: true)}"
    end
  end
end
