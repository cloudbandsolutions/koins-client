module Postings
  class PostStatistics
    MASTER_SERVER = Settings.master_server
    API_KEY = Settings.api_key
    PARAMS = { api_key: API_KEY }
    MASTER_URL_POST_STATISTICS  = "#{MASTER_SERVER}/api/v1/data/post_stats"

    def initialize(daily_report:)
      @daily_report         = daily_report
      @date_generated_hour  = @daily_report.created_at.hour
      @date_generated_min   = @daily_report.created_at.min
    end

    def execute!
      Rails.logger.info "Posting daily report statistics"
      params  = build_params!

      begin
        url     = MASTER_URL_POST_STATISTICS
        http    = Curl.post(url, params)

        if http.response_code == 200
          true
        else
          Rails.logger.info http.body_str
          false
        end
      rescue
        Rails.logger.error "Error in posting to master server"
        false
      end
    end

    def build_params!
      params                            = PARAMS
      params[:branch_id]                = Branch.first.id
      params[:as_of]                    = @daily_report.as_of
      params[:generated_hour]           = @date_generated_hour
      params[:generated_min]            = @date_generated_min
      params[:loan_product_stats]       = @daily_report.loan_product_stats.to_json
      params[:repayments]               = @daily_report.repayments.to_json
      params[:par]                      = @daily_report.par.to_json
      params[:member_stats]             = @daily_report.member_stats.to_json
      params[:watch_list]               = @daily_report.watchlist.to_json

      params[:loan_product_cycle_count_stats] = @daily_report.loan_product_cycle_count_stats
      params[:so_member_counts]               = @daily_report.so_member_counts

      params[:active_loans_by_loan_product]   = @daily_report.active_loans_by_loan_product.to_json

      params
    end
  end
end
