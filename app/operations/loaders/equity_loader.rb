module Loaders
  class EquityLoader
    def initialize(filename:)

      # CSV Filename
      @filename = filename
      
      # Data formed by parsing CSV file
      @data = {}

      @data[:records] = []
    end

    def execute!
      csv_text  = File.read(@filename)
      csv       = CSV.parse(csv_text, headers: true)
      
      csv.each do |row|
        # build data according to desired field (i.e. member_id) and csv_header name
        r = {
          member_id: row['uuid'],
          member_name: row['Name'],
          equtiytrans: [
            
             { month: "January",amount: row["Jan"].to_f },
             { month: "February",amount: row["Feb"].to_f },
             { month: "March",amount: row["Mar"].to_f },
             { month: "April",amount: row["Apr"].to_f },
             { month: "May",amount: row["May"].to_f },
             { month: "June",amount: row["June"].to_f },
             { month: "July",amount: row["July"].to_f },
             { month: "August",amount: row["Aug"].to_f },
             { month: "September",amount: row["Sept"].to_f },
             { month: "October",amount: row["Oct"].to_f },
             { month: "November",amount: row["Nov"].to_f },
             { month: "December",amount: row["Dec"].to_f }

          

            
          ],
          first_loop: row['Total_Share'].to_f,
          aver_first_loop: row['Ave_Share'].to_f,
          total_interest_amount: row['Interest_Amount'].to_f,
          member_savings_amount_distribute: row['Savings_amount'].to_f,
          member_cbu_amount_distribute: row['CBU'].to_f
        }

        # insert to array
        @data[:records] << r
      end

      # Return data
      @data
    end
  end
end
