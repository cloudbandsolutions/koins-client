module Loaders
  class PatronageLoader
    def initialize(filename:)
      #raise 
      #raise "drilon"
      @filename = filename
      @data= {}

      @data[:records] = []
    
    end

    def execute!
      csv_text  = File.read(@filename)
      csv       = CSV.parse(csv_text, headers: true)

      csv.each do |row|
        r = {
          member_id: row["uuid"],
          #raise row["member_id"].inspect
          member_name: row ["Name"],
          reasons: [
            { month: "January",   amount: row["Jan"].to_f },
            { month: "February",  amount: row["Feb"].to_f },
            { month: "March",     amount: row["Mar"].to_f },
            { month: "April",     amount: row["Apr"].to_f },
            { month: "May",       amount: row["May"].to_f },
            { month: "June",      amount: row["June"].to_f},
            { month: "July",      amount: row["July"].to_f},
            { month: "August",    amount: row["Aug"].to_f},
            { month: "September", amount: row["Sept"].to_f},
            { month: "October",   amount: row["Oct"].to_f},
            { month: "November",  amount: row["Nov"].to_f},
            { month: "December",  amount: row["Dec"].to_f}

          ],
          member_loan_interest_amount:  row["total_interest"],
          member_loan_interest_rate:    row["Patronage_Refund"],
          dtotal_savings:               row["savings"],
          dtotal_cbu:                   row["cbu"]
        
        
        }
        
        
        @data[:records] << r
        #raise @data[:records].inspect
      end
      @data
    end
    
   # @data
  
  end
end
