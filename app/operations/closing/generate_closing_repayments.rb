module Closing
  class GenerateDailyClosingRepayments
    def initialize(daily_closing:)
      @daily_closing  = daily_closing
      @as_of          = daily_closing.closing_date
      @data           = Reports::GenerateRepayments.new(branch_id: nil, so: nil, center_id: nil, loan_product_id: nil, as_of: @as_of).execute!
    end

    def execute!
      generate_branch_repayments
      generate_so_repayments
      generate_center_repayments
      generate_member_repayments
      @daily_closing.save!
    end

    private

    def generate_member_repayments
      @data[:loan_products].each do |data_loan_product|
        data_loan_product[:branches].each do |data_branch|
        end
      end
    end

    def generate_center_repayments
      @data[:loan_products].each do |data_loan_product|
        data_loan_product[:branches].each do |data_branch|
        end
      end
    end

    def generate_so_repayments
      @data[:loan_products].each do |data_loan_product|
        data_loan_product[:branches].each do |data_branch|
          data_branch[:so_list].each do |data_so|
            daily_closing_so_repayment = DailyClosingSoRepayment.new

            @daily_closing.daily_closing_so_repayments << daily_closing_so_repayment 
          end
        end
      end
    end

    def generate_branch_repayments
      @data[:loan_products].each do |data_loan_product|
        data_loan_product[:branches].each do |data_branch|
          daily_closing_branch_repayment = DailyClosingBranchRepayment.new
          daily_closing_branch_repayment.name = data_branch[:name]
          daily_closing_branch_repayment.num_members = Loan.active.where(loan_product_id: data_loan_product[:loan_product_id]).count
          daily_closing_branch_repayment.loan_product_code = data_loan_product[:loan_product]

          daily_closing_branch_repayment.loan_amount = 0.00
          daily_closing_branch_repayment.principal_paid = 0.00
          daily_closing_branch_repayment.loan_balance = 0.00
          daily_closing_branch_repayment.interest_amount = 0.00
          daily_closing_branch_repayment.interest_paid = 0.00
          daily_closing_branch_repayment.interest_balance = 0.00
          daily_closing_branch_repayment.total_paid = 0.00
          daily_closing_branch_repayment.cummulative_due = 0.00
          daily_closing_branch_repayment.amount_past_due = 0.00

          data_branch[:so_list].each do |data_so|
            daily_closing_branch_repayment.loan_amount += data_so[:total_loan_amount]
            daily_closing_branch_repayment.principal_paid += data_so[:total_principal_paid]
            daily_closing_branch_repayment.loan_balance += data_so[:total_loan_balance]
            daily_closing_branch_repayment.interest_amount += data_so[:total_interest_amount]
            daily_closing_branch_repayment.interest_paid += data_so[:total_interest_paid]
            daily_closing_branch_repayment.interest_balance += data_so[:total_interest_balance]
            daily_closing_branch_repayment.total_paid += data_so[:total_paid]
            daily_closing_branch_repayment.cummulative_due += data_so[:cum_due]
            daily_closing_branch_repayment.amount_past_due += data_so[:amt_past_due]
          end

          #daily_closing_branch_repayment.repayment_rate = ((daily_closing_branch_repayment.total_paid / daily_closing_branch_repayment.cummulative_due_ * 100).round(2)

          #@daily_closing.daily_closing_branch_repayments << daily_closing_branch_repayment
        end
      end
    end
  end
end
