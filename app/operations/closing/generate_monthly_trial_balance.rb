module Closing
  class GenerateMonthlyTrialBalance
    def initialize(year:, month:, branch:)
      @year       = year
      @month      = month
      @start_date = Date.new(@year, @month, 1)
      @end_date   = @start_date.next_month.prev_day
      @branch     = branch
    end

    def execute!
      data = FinancialReports::TrialBalance.get_entries(@start_date, @end_date, @branch)

      monthly_trial_balance             = MonthlyTrialBalance.new
      monthly_trial_balance.start_date  = @start_date
      monthly_trial_balance.end_date    = @end_date
      monthly_trial_balance.data        = data

      monthly_trial_balance.save!
    end
  end
end
