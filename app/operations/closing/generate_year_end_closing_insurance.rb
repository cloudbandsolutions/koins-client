module Closing
  class GenerateYearEndClosingInsurance
    def initialize(year:, prepared_by:, start_date:, end_date:)
      @data             =   {}
      @start_date       =   start_date
      @end_date         =   end_date  
      @accounting_funds =   Settings.accounting_funds
      @year             =   year
      @prepared_by      =   prepared_by
    end

    def execute!
      @data[:closing_records] = []

      @accounting_funds.each do |accounting_fund|
        @data[:closing_records] << Closing::GenerateYearEndClosingInsuranceByAccountingFund.new(
                                start_date: @start_date,
                                end_date: @end_date,
                                year: @year,
                                prepared_by: @prepared_by,
                                accounting_fund: AccountingFund.find(accounting_fund[:accounting_fund_id]),
                                net_income_expense_entry_id: accounting_fund[:net_income_expense_entry_id]

          ).execute! 
        end   

      @data
    end
    
  end
end
