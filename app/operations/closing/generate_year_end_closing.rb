module Closing
  class GenerateYearEndClosing
    def initialize(year:, prepared_by:)
      @year               = year
      @prepared_by        = prepared_by
      @net_entry_id       = Settings.net_income_expense_entry_id
      @net_code           = AccountingCode.find(@net_entry_id)
      @expenses_group_id  = Settings.expenses_group_id
      @expense_group      = MajorGroup.find(@expenses_group_id)
      @revenue_group_id   = Settings.revenue_group_id
      @revenue_group      = MajorGroup.find(@revenue_group_id)
      @date_prepared      = ApplicationHelper.current_working_date.strftime("%b %d, %Y")
      @expense_codes      = []
      @revenue_codes      = []
      @particular         = "To close the year #{@year} dated #{@date_prepared}"

      @original_data      = {}
      @closed_data        = {}
      @data               = {}
      @data[:date_prepared]      = @date_prepared
      @data[:original_total_dr]  = 0.00
      @data[:original_total_cr]  = 0.00
      @data[:closed_total_dr]    = 0.00
      @data[:closed_total_cr]    = 0.00
      @data[:particular]         = @particular

      @accounting_entry   = {}
      @accounting_entry[:debit_entries]   = []
      @accounting_entry[:credit_entries]  = []
      @accounting_entry[:particular]      = @particular
      @accounting_entry[:prepared_by]     = @prepared_by
    end

    def execute!
      build_codes!
      build_original_data!
      build_closed_data!

      @data[:original_data]           = @original_data
      @data[:closed_data]             = @closed_data

      build_accounting_entry!

      @data
    end

    private

    def build_accounting_entry!
      @accounting_entry[:debit_entries]     = @data[:closed_data][:debit_entries]
      @accounting_entry[:credit_entries]    = @data[:closed_data][:credit_entries]
      @data[:accounting_entry]              = @accounting_entry
    end

    def build_closed_data!
      @closed_data[:debit_entries]  = []
      @closed_data[:credit_entries] = []

      @original_data[:credit_entries].each do |credit_entry|
        d = {
          accounting_code_id: credit_entry[:accounting_code_id],
          accounting_code: credit_entry[:accounting_code],
          debit: credit_entry[:credit],
          credit: credit_entry[:debit]
        }

        @closed_data[:debit_entries] << d
      end

      @original_data[:debit_entries].each do |debit_entry|
        d = {
          accounting_code_id: debit_entry[:accounting_code_id],
          accounting_code: debit_entry[:accounting_code],
          debit: debit_entry[:credit],
          credit: debit_entry[:debit]
        }

        @closed_data[:credit_entries] << d
      end

      d = {
        accounting_code_id: @net_code.id,
        accounting_code: @net_code.to_s,
        debit: 0,
        credit: 0
      }

      if @data[:original_total_dr] > @data[:original_total_cr]
        d[:debit] = @data[:original_total_dr] - @data[:original_total_cr]
        @closed_data[:debit_entries] << d
      elsif @data[:original_total_cr] > @data[:original_total_dr]
        d[:credit] = @data[:original_total_cr] - @data[:original_total_dr]
        @closed_data[:credit_entries] << d
      end
    end

    def build_original_data!
      @original_data[:debit_entries]  = []
      @original_data[:credit_entries] = []

      @expense_codes.each do |expense_code|
        total_debit   = JournalEntry.joins(:voucher).where(
                          "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id = #{expense_code.id}"
                        ).sum(:amount)
        total_credit  = JournalEntry.joins(:voucher).where(
                          "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id = #{expense_code.id}"
                        ).sum(:amount)

        total_net     = total_debit - total_credit

        if total_net != 0
          @data[:original_total_dr] += total_net
          @data[:original_total_cr] += 0

          d = {
            accounting_code_id: expense_code.id,
            accounting_code: expense_code.to_s,
            debit: total_net,
            credit: 0
          }

          @original_data[:debit_entries] << d
        end
      end

      @revenue_codes.each do |revenue_code|
        total_debit   = JournalEntry.joins(:voucher).where(
                          "vouchers.status = 'approved' AND post_type = 'DR' AND accounting_code_id = #{revenue_code.id}"
                        ).sum(:amount)
        total_credit  = JournalEntry.joins(:voucher).where(
                          "vouchers.status = 'approved' AND post_type = 'CR' AND accounting_code_id = #{revenue_code.id}"
                        ).sum(:amount)

        total_net     = total_credit - total_debit

        d = {
          accounting_code_id: revenue_code.id,
          accounting_code: revenue_code.to_s,
          debit: 0,
          credit: total_net
        }

        if total_net != 0
          @data[:original_total_dr] += 0
          @data[:original_total_cr] += total_net

          @original_data[:credit_entries] << d
        end
      end
    end

    def build_codes!
      @expense_group.major_accounts.each do |ma|
        ma.mother_accounting_codes.each do |mac|
          mac.accounting_code_categories.each do |acc|
            acc.accounting_codes.each do |ac|
              @expense_codes << ac
            end
          end
        end
      end

      @revenue_group.major_accounts.each do |ma|
        ma.mother_accounting_codes.each do |mac|
          mac.accounting_code_categories.each do |acc|
            acc.accounting_codes.each do |ac|
              @revenue_codes << ac
            end
          end
        end
      end
    end
  end
end
