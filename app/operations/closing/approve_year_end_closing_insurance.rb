module Closing
  class ApproveYearEndClosingInsurance
    def initialize(data:, user:, year:)
      @data = data
      @user = user
      @year = year
      @branch = Branch.find(41)
      @c_working_date = ApplicationHelper.current_working_date
      @start_date     = Date.new(@c_working_date.year, @c_working_date.month)
      @end_date       = Date.civil(@start_date.year, @start_date.month, -1)
    end

    def execute!
      # @trial_balance  = ::FinancialReports::TrialBalance.get_entries(
      #                     Date.new(@c_working_date.year, @c_working_date.month),
      #                     @c_working_date,
      #                     @branch
      #                   )

     @trial_balance  = ::Accounting::GenerateTrialBalance.new(
                         start_date: @start_date,
                         end_date: @end_date,
                         branch: @branch,
                         accounting_fund: nil
                       ).execute!

      @income_statement = ::Accounting::GenerateIncomeStatement.new(
                            as_of: @c_working_date,
                            prepared_by: @user.full_name
                          ).execute!

      @balance_sheet    = ::Accounting::GenerateBalanceSheet.new(
                              as_of: @c_working_date,
                              prepared_by: @user.full_name
                            ).execute!

      year_end_closing_record_data  = {
        vouchers: []
      }

      @data[:closing_records].each do |v|
        v = post_voucher!(v)

        v.save!

        v = ::Accounting::ApproveVoucher.new(
              voucher: v,
              user: @user
            ).execute!

        year_end_closing_record_data[:vouchers] << build_voucher_hash(v)
      end

      # data  = year_end_closing_records.data.with_indifferent_access

      # Display journal entries:
      # vouchers = []
      # data[:vouchers].each do |v|
      #   voucher = Voucher.find(v[:id])
      #   journal_entries = voucher.journal_entries

      YearEndClosingRecord.create!(
        year: @year,
        trial_balance: @trial_balance,
        date_closing: @c_working_date,
        balance_sheet: @balance_sheet,
        income_statement: @income_statement,
        for_insurance: true,
        data: year_end_closing_record_data
      )
    end

    private

    def build_voucher_hash(v)
      data  = {
        id: v.id,
        branch: {
          id: @branch.id,
          name: @branch.name
        },
        prepared_by: v.prepared_by,
        approved_by: v.approved_by,
        date_prepared: v.date_prepared,
        particular: v.particular,
        debit_entries: [],
        credit_entries: []
      }

      # Add debit entries
      v.journal_entries.debit_entries.each do |o|
        data[:debit_entries] << o
      end

      # Add credit entries
      v.journal_entries.credit_entries.each do |o|
        data[:credit_entries] << o
      end

      data
    end

    def post_voucher!(v)
      voucher = Voucher.new(
                  book: 'MISC',
                  branch: @branch,
                  prepared_by: @user.full_name,
                  approved_by: @user.full_name,
                  is_year_end_closing: true,
                  status: 'pending',
                  date_prepared: @c_working_date,
                  particular: v[:particular],
                  accounting_fund: v[:accounting_fund]
              )

      debit_entries   = build_debit_entries!(v)

      debit_entries.each do |j|
        voucher.journal_entries << j
      end

      credit_entries  = build_credit_entries!(v)

      credit_entries.each do |j|
        voucher.journal_entries << j
      end

      voucher
    end

    def build_debit_entries!(v)
      journal_entries = []

      v[:closed_data][:debit_entries].each do |debit_entry|
        entry = JournalEntry.new(
                  amount: debit_entry[:debit],
                  post_type: 'DR',
                  accounting_code: AccountingCode.find(debit_entry[:accounting_code_id]),
                  is_year_end_closing: true
                )

        journal_entries << entry
      end

      journal_entries
    end

    def build_credit_entries!(v)
      journal_entries = []

      v[:closed_data][:credit_entries].each do |credit_entry|
        entry = JournalEntry.new(
                  amount: credit_entry[:credit],
                  post_type: 'CR',
                  accounting_code: AccountingCode.find(credit_entry[:accounting_code_id]),
                  is_year_end_closing: true
                )

        journal_entries << entry
      end

      journal_entries
    end
  end
end
