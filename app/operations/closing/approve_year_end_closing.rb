module Closing
  class ApproveYearEndClosing
    def initialize(data:, user:, year:)
      @data = data
      @user = user
      @year = year
      @branch = Branch.first
      @c_working_date = ApplicationHelper.current_working_date
      @start_date     = Date.new(@c_working_date.year, @c_working_date.month)
      @end_date       = Date.civil(@start_date.year, @start_date.month, -1)
    end

    def execute!
      @trial_balance  = ::FinancialReports::TrialBalance.get_entries(
                          Date.new(@c_working_date.year, @c_working_date.month),
                          @c_working_date,
                          @branch
                        )

#      @trial_balance  = ::Accounting::GenerateTrialBalance.new(
#                          start_date: @start_date,
#                          end_date: @end_date,
#                          branch: @branch,
#                          accounting_fund: nil
#                        ).execute!

      @income_statement = ::Accounting::GenerateIncomeStatement.new(
                            as_of: @c_working_date,
                            prepared_by: @user.full_name
                          ).execute!

      @balance_sheet    = ::Accounting::GenerateBalanceSheet.new(
                              as_of: @c_working_date,
                              prepared_by: @user.full_name
                            ).execute!

      @voucher =  Voucher.new(
                    book: 'MISC',
                    branch: @branch,
                    prepared_by: @user.full_name,
                    approved_by: @user.full_name,
                    is_year_end_closing: true,
                    status: 'pending',
                    date_prepared: @c_working_date,
                    particular: @data[:particular]
                  )

      build_debit_entries!
      build_credit_entries!

      @voucher.save!

      @voucher  = ::Accounting::ApproveVoucher.new(
                    voucher: @voucher,
                    user: @user
                  ).execute!

      YearEndClosingRecord.create!(
        voucher: @voucher,
        year: @year,
        trial_balance: @trial_balance,
        date_closing: @c_working_date,
        balance_sheet: @balance_sheet,
        income_statement: @income_statement
      )
    end

    private

    def build_debit_entries!
      @data[:closed_data][:debit_entries].each do |debit_entry|
        entry = JournalEntry.new(
                  amount: debit_entry[:debit],
                  post_type: 'DR',
                  accounting_code: AccountingCode.find(debit_entry[:accounting_code_id]),
                  is_year_end_closing: true
                )

        @voucher.journal_entries << entry
      end
    end

    def build_credit_entries!
      @data[:closed_data][:credit_entries].each do |credit_entry|
        entry = JournalEntry.new(
                  amount: credit_entry[:credit],
                  post_type: 'CR',
                  accounting_code: AccountingCode.find(credit_entry[:accounting_code_id]),
                  is_year_end_closing: true
                )

        @voucher.journal_entries << entry
      end
    end
  end
end
