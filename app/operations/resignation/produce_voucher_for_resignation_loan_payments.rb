module Resignation
  class ProduceVoucherForResignationLoanPayments
    def initialize(member_resignation:, user:)
      @member_resignation = member_resignation
      @member             = @member_resignation.member
      @branch             = @member.branch
      @user               = user
      @particular         = "Payment of loans for resigning member #{@member.full_name}"

      @voucher  = Voucher.new(
                    status: "pending", 
                    branch: @branch, 
                    book: 'JVB', 
                    date_prepared: Date.today, 
                    prepared_by: @user.full_name.upcase, 
                    particular: @particular, 
                    or_number: @member_resignation.or_number
                  )

      @bank = @branch.bank
      @savings_types    = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types     = EquityType.all

      @total_insurance_withdrawals  = 0.00
      @current_insurance_balance    = @member.insurance_accounts.sum(:balance)
    end

    def execute!
      build_entries!

      @voucher
    end

    private
    
    def build_entries!
      build_debit_entries
      build_credit_entries
    end

    def build_debit_entries
      @savings_types.each do |savings_type|
        amount  = ResignationLoanBalancePayment.where(
                    account_type: "SAVINGS", 
                    account_type_code: savings_type.code, 
                    resignation_loan_balance_id: @member_resignation.resignation_loan_balances.pluck(:id)
                  ).sum(:amount)

        journal_entry = JournalEntry.new(
                          amount: amount,
                          post_type: 'DR',
                          accounting_code: savings_type.deposit_accounting_code,
                        )

        @voucher.journal_entries << journal_entry
      end

      @insurance_types.each do |insurance_type|
        amount  = ResignationLoanBalancePayment.where(
                    account_type: "INSURANCE", 
                    account_type_code: insurance_type.code, 
                    resignation_loan_balance_id: @member_resignation.resignation_loan_balances.pluck(:id)
                  ).sum(:amount)

        #zero_out_amount  = @member.insurance_accounts.where(insurance_type_id: insurance_type.id).first.balance

        journal_entry = JournalEntry.new(
                          amount: amount,
                          post_type: 'DR',
                          accounting_code: insurance_type.deposit_accounting_code,
                        )

        @voucher.journal_entries << journal_entry

        @total_insurance_withdrawals += amount
      end

    end

    def build_credit_entries
      loan_products = LoanProduct.where(id: @member_resignation.resignation_loan_balances.joins(:loan).pluck("loans.loan_product_id").uniq)

      # Loans Receivable per product
      loan_products.each do |loan_product|
        lp_principal_amount = 0.00
        lp_interest_amount = 0.00
        
        @member_resignation.resignation_loan_balances.joins(:loan).where("loans.loan_product_id = ?", loan_product.id).each do |resignation_loan_balance|
          amount = resignation_loan_balance.resignation_loan_balance_payments.sum(:amount)
          stats = Loans::BuildPaymentStats.new(amount: amount, loan: resignation_loan_balance.loan).execute!

          lp_principal_amount += stats[:principal]
          lp_interest_amount += stats[:interest]
        end

        journal_entry = JournalEntry.new(
                          amount: lp_principal_amount,
                          post_type: 'CR',
                          accounting_code: loan_product.accounting_code_transaction
                        )

        @voucher.journal_entries << journal_entry

        journal_entry = JournalEntry.new(
                          amount: lp_interest_amount,
                          post_type: 'CR',
                          accounting_code: loan_product.interest_accounting_code
                        )
        @voucher.journal_entries << journal_entry
      end
    end
  end
end
