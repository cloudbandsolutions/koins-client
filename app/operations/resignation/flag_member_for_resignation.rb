module Resignation
  class FlagMemberForResignation
    def initialize(member:, resignation_type:, resignation_code:, resignation_particular:, date_resigned:, user:)
      @user                   = user
      @member                 = member
      @resignation_type       = resignation_type
      @active_loans           = Loan.active.where(member_id: @member.id)
      @date_resigned          = date_resigned
      @savings_types          = SavingsType.all
      @insurance_types        = InsuranceType.all
      @equity_types           = EquityType.all
      @resignation_code       = resignation_code
      @resignation_particular = resignation_particular
      @c_working_date         = ApplicationHelper.current_working_date
    end

    def execute!
      member_resignation = MemberResignation.new(
                            member: @member,
                            resignation_type: @resignation_type,
                            date_resigned: @date_resigned,
                            resignation_type_particular: @resignation_particular,
                            resignation_type_code: @resignation_code
                          )

      @savings_types.each do |savings_type|
        savings_account = @member.savings_accounts.where(savings_type_id: savings_type.id).first

        amount = 0.00
        if savings_account
          amount = savings_account.balance
        end

        member_resignation.resignation_withdrawals.build({
                            amount: amount,
                            account_type: "SAVINGS",
                            account_type_code: savings_type.code,
                          })
      end

      @insurance_types.each do |insurance_type|
        insurance_account = @member.insurance_accounts.where(insurance_type_id: insurance_type.id).first
        
        amount = 0.00
        if insurance_account
          amount = insurance_account.balance
        end

        if insurance_type.use_resign_rules? and insurance_account.try(:balance) > 0
          # get membership_type
          date_of_insurance_membership  = MembershipPayment.paid.joins(:membership_type).where("membership_types.name = ?", Settings.insurance_membership_type).first.try(:paid_at)

          if !@member.previous_mii_member_since.nil?
            date_of_insurance_membership = @member.previous_mii_member_since
          end

          if date_of_insurance_membership
            num_days_diff = (@c_working_date.to_date - date_of_insurance_membership.to_date)

            if num_days_diff.days >= insurance_type.resign_rule_num_years.years
              amount  = (insurance_account.balance * (insurance_type.resign_rule_withdrawal_percentage / 100)).round(2)
            else
              amount  = 0.00
            end
          end
        end

        member_resignation.resignation_withdrawals.build({
                            amount: amount,
                            account_type: "INSURANCE",
                            account_type_code: insurance_type.code
                          })
      end

      @equity_types.each do |equity_type|
        equity_account = @member.equity_accounts.where(equity_type_id: equity_type.id).first

        amount = 0.00
        if equity_account
          amount = equity_account.balance
        end
      
        member_resignation.resignation_withdrawals.build({
                            amount: amount,
                            account_type: "EQUITY",
                            account_type_code: equity_type.code,
                          })
      end


      @active_loans.each do |active_loan|
        resignation_loan_balance_payments = []

        @savings_types.each do |savings_type|
          resignation_loan_balance_payments << ResignationLoanBalancePayment.new(account_type: "SAVINGS", account_type_code: savings_type.code, amount: 0.00)
        end

        @equity_types.each do |equity_type|
          resignation_loan_balance_payments << ResignationLoanBalancePayment.new(account_type: "EQUITY", account_type_code: equity_type.code, amount: 0.00)
        end

        @insurance_types.each do |insurance_type|
          resignation_loan_balance_payments << ResignationLoanBalancePayment.new(account_type: "INSURANCE", account_type_code: insurance_type.code, amount: 0.00)
        end

        member_resignation.resignation_loan_balances.build({
                                loan: active_loan,
                                balance: active_loan.remaining_balance,
                                principal_balance: active_loan.principal_balance,
                                interest_balance: active_loan.interest_balance,
                                resignation_loan_balance_payments: resignation_loan_balance_payments,
                              })
      end
      member_resignation.save!

      @member.update!(status: 'for-resignation')
      zero_out_maintaining_balance!
    end

    private

    def zero_out_maintaining_balance!
      @member.savings_accounts.each do |savings_account|
        savings_account.zero_out!
      end
    end
  end
end
