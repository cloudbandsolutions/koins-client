module Resignation
  class ValidateForResignation
    def initialize(member:)
      @member = member
      @errors = []
    end

    def execute!
      if @member.loans.active.count > 0
        @errors << "Member #{@member.full_name} still has active loans"
      end

      #if @member.savings_accounts.sum(:balance) > 0
      #  @errors << "Member #{@member.full_name} still has savings balance"
      #end

      #if @member.insurance_accounts.joins(:insurance_type).where("insurance_types.is_default = ?", true).sum(:balance) > 0
      #  @errors << "Member #{@member.full_name} still has insurance balance"
      #end

      @member.savings_accounts.each do |savings_account|
        center = savings_account.center
        if !center 
          @errors << "No Center Found For Savings Account #{savings_account.savings_type} with ID #{savings_account.id}"
        end
      end

      @member.equity_accounts.each do |equity_account|
        center = equity_account.center
        if !center 
          @errors << "No Center Found For Equity Account #{equity_account.equity_type} with ID #{equity_account.id}"
        end
        
      end
      
      if @errors.size > 0
        @errors << "Contact MIS Immediately"
      end

      @errors
    end
  end
end
