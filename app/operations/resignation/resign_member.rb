module Resignation
  class ResignMember
    def initialize(member:, date_resigned:, resignation_type:, resignation_code:, resignation_reason:, user:, particular:)
      @member           = member
      @equity_accounts  = EquityAccount.where(member_id: @member.id)
      @equity_balance   = @equity_accounts.sum(:balance)
      @date_resigned    = date_resigned
      @resignation_type = resignation_type
      @resignation_code = resignation_code
      @reason           = resignation_reason
      @book             = 'JVB'
      @current_date     = ApplicationHelper.current_working_date
      @user             = user
      @branch           = member.branch
      @particular       = particular

      if !@particular
        @particular = "Resign member #{@member.full_name}"
      end

      @debit_accounting_code  = AccountingCode.find(Settings.equity_debit_accounting_code_id)
      
      #@debit_accounting_code  = AccountingCode.where(id: Settings.equity_resign_debit_accounting_code_id).first
      #raise @debit_accounting_code.inspect
      @credit_accounting_code  = AccountingCode.where(id: Settings.equity_resign_credit_accounting_code_id).first
      #raise @credit_accounting_code.inspect
      #@credit_accounting_code = AccountingCode.find(Settings.equity_credit_accounting_code_id)
      @voucher                = build_accounting_entry!
    end

    def execute!
      if @equity_balance > 0
        @voucher  = Accounting::ApproveVoucher.new(
                      voucher: @voucher, 
                      user: @user
                    ).execute!

        @member.savings_accounts.each do |savings_account|
          savings_account.update!(maintaining_balance: 0)
        end

        @member.update!(
          status: "resigned", 
          date_resigned: @date_resigned,
          insurance_status: 'resigned',
          insurance_date_resigned: @date_resigned,
          resignation_type: @resignation_type,
          resignation_code: @resignation_code,
          resignation_reason: @reason,
          resignation_accounting_reference_number: @voucher.reference_number
        )

        MemberShare.where(member_id: @member.id).update(
                            is_void: true)

        equity_account_transaction = EquityAccountTransaction.create!(
                                        amount: @equity_balance,
                                        transacted_at: @current_date,
                                        created_at: @current_date,
                                        transaction_type: "withdraw",
                                        particular: @particular,
                                        voucher_reference_number: @voucher.reference_number,
                                        equity_account: @member.equity_accounts.first
                                      )

        equity_account_transaction.approve!(@user.full_name)
      end

      @member
    end

    private

    def build_accounting_entry!
      @voucher  = Voucher.new(
                    branch: @branch,
                    book: @book,
                    particular: @particular,
                    date_prepared: @current_date,
                    status: 'pending'
                  )

      @voucher.journal_entries  <<  JournalEntry.new(
                                      amount: @equity_balance,
                                      post_type: "DR",
                                      accounting_code: @debit_accounting_code
                                    )
      @voucher.journal_entries  <<  JournalEntry.new(
                                      amount: @equity_balance,
                                      post_type: "CR",
                                      accounting_code: @credit_accounting_code
                                    )
      @voucher
    end
  end
end
