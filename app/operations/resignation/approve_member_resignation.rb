module Resignation
  class ApproveMemberResignation
    require 'application_helper'

    def initialize(member_resignation:, user:)
      @member_resignation = member_resignation
      @member             = member_resignation.member
      @c_working_date     = ApplicationHelper.current_working_date
      @particular         = "Payment of loan on Resignation"
      @user               = user
    end

    def execute!
      @voucher             = ::Resignation::ProduceVoucherForResignationLoanPayments.new(
                              member_resignation: @member_resignation,
                              user: @user
                            ).execute!

      @voucher  = Accounting::ApproveVoucher.new(
                    voucher: @voucher, 
                    user: @user
                  ).execute!

      @reference_number   = @voucher.reference_number

      if @member_resignation.resignation_loan_balances.size > 0
        #perform_transactions!
        perform_transactions_from_payment_collection!
      end

      update_status!
      deactivate_accounts!

      update_member_resignation!
    end

    private

    def update_member_resignation!
      @member_resignation.update!(
        status: 'approved',
        reference_number: @voucher.reference_number
      )
    end

    def perform_transactions_from_payment_collection!
      @member_resignation.resignation_loan_balances.each do |resignation_loan_balance|
        resignation_loan_balance.resignation_loan_balance_payments.each do |collection_transaction|
          account_type_code = collection_transaction.account_type_code
          amount            = collection_transaction.amount

          if amount > 0
            if collection_transaction.account_type == "SAVINGS"
              savings_account = @member.savings_accounts.joins(:savings_type).where("savings_types.code = ?", account_type_code).first

              if savings_account
                savings_account_transaction = SavingsAccountTransaction.create!(
                                                  amount: amount,
                                                  transacted_at: @c_working_date,
                                                  created_at: @c_working_date,
                                                  particular: @particular,
                                                  transaction_type: "withdraw",
                                                  voucher_reference_number: @reference_number,
                                                  savings_account: savings_account,
                                                  for_resignation:  true,
                                                  for_loan_payments: true
                                                )

                savings_account_transaction.approve!(@user.full_name)
              end
            end

            if collection_transaction.account_type == "INSURANCE"
              insurance_account = @member.insurance_accounts.joins(:insurance_type).where("insurance_types.code = ?", account_type_code).first

              #amount = insurance_account.balance

              if insurance_account
                insurance_account_transaction = InsuranceAccountTransaction.create!(
                                                  amount: amount,
                                                  transacted_at: @c_working_date,
                                                  created_at: @c_working_date,
                                                  particular: @particular,
                                                  transaction_type: "withdraw",
                                                  voucher_reference_number: @reference_number,
                                                  insurance_account: insurance_account,
                                                  for_resignation: true,
                                                  for_loan_payments: true
                                                )

                insurance_account_transaction.approve!(@user.full_name)
              end
            end
          end
        end
      end

      @member_resignation.resignation_loan_balances.each do |resignation_loan_balance|
        resignation_loan_balance.resignation_loan_balance_payments.each do |resignation_loan_balance_payment|
          loan          = resignation_loan_balance.loan
          amount        = resignation_loan_balance_payment.amount

          if amount > 0
            loan_payment  = LoanPayment.create(
                              amount:               amount,
                              paid_at:              @c_working_date,
                              loan:                 loan,
                              loan_payment_amount:  amount,
                              cp_amount:            amount,
                              wp_amount:            0,
                              deposit_amount:       0,
                              savings_account:      loan.member.default_savings_account,
                              particular:           @particular,
                              wp_particular:        "n/a"
                            )

            loan_payment.approve_payment!
          end
        end
      end
    end

    def update_status!
      @member.update!(status: "resigned")
    end

    def deactivate_accounts!
      @member.savings_accounts.each do |savings_account|
        savings_account.deactivate!
      end

      @member.insurance_accounts.each do |insurance_account|
        insurance_account.deactivate!
      end

      @member.equity_accounts.each do |equity_account|
        equity_account.deactivate!
      end
    end
  end
end
