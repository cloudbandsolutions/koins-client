module Resignation
  class ValidateForApproveMemberResignation
    def initialize(member_resignation:)
      @member_resignation = member_resignation
      @errors             = []
    end

    def execute!
      @errors
    end
  end
end
