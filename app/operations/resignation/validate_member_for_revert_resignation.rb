module Resignation
  class ValidateMemberForRevertResignation
    def initialize(member:)
      @member = member
      @errors = []
    end

    def execute!
      validate_status! 
      validate_member_resignation_record!
      @errors
    end

    def validate_member_resignation_record!
      if @member.member_resignations.where(status: 'approved').count > 0
        @errors << "Member has approved resignation record"
      end
    end

    def validate_status!
      if !@member.for_resignation?
        @errors << "Cannot revert member not flagged for resignation"
      end
    end
  end
end
