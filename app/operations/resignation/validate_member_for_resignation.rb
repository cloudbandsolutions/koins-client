module Resignation
  class ValidateMemberForResignation
    def initialize(member:, resignation_type:, resignation_code:, resignation_particular:, date_resigned:)
      @member                 = member
      @resignation_type       = resignation_type
      @resignation_code       = resignation_code
      @resignation_particular = resignation_particular
      @date_resigned          = date_resigned
      @errors                 = []
    end

    def execute!
      validate_status!
      validate_parameters!
      validate_member_pending_payments!
      validate_resignation_particular!

      @errors
    end

    private

    def validate_resignation_particular!
      if !@resignation_code.present?
        @errors << "Resignation code required"
      end

      if !@resignation_particular.present?
        @ererors << "Resignation particular required"
      end
    end

    def validate_member_pending_payments!
      pending_payments = PaymentCollectionRecord.joins(:payment_collection).where("payment_collection_records.member_id = ? AND status = ?", @member.id, "pending")
      if pending_payments.count > 0
        pending_payments.each do |pending_payment|
          @errors << "Member still has pending payment: #{pending_payment.payment_collection.payment_type} (ID: #{pending_payment.payment_collection.id})"
        end
      end
    end

    def validate_parameters!
      if !@resignation_type.present?
        @errors << "Resignation type required"
      end

      if !@date_resigned.present?
        @errors << "Date resigned requierd"
      end

      if !@member.present?
        @errors << "Member required"
      end
    end

    def validate_status!
      if !@member.active?
        @errors << "Cannot resign non-active member"
      end
    end
  end
end
