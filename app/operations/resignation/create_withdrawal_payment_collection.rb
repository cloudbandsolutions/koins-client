module Resignation
  class CreateWithdrawalPaymentCollection
    def initialize(member:, member_resignation:, user:)
      @user = user
      @member = member
      @member_resignation = member_resignation
      @savings_types = SavingsType.all
      @insurance_types = InsuranceType.all
      @equity_types = EquityType.all
      
      @savings_accounts = @member.savings_accounts
      @insurance_accounts = @member.insurance_accounts
      @equity_accounts = @member.equity_accounts

      @branch = @member.branch
      @paid_at = Date.today
      @prepared_by = @user.full_name.upcase

      @particular = "Withdrawal for #{@member.full_name} resignation"

      @payment_collection = PaymentCollection.new(
                              branch: @branch,
                              particular: @particular,
                              or_number: "WT-#{Time.now.to_i}",
                              prepared_by: @prepared_by,
                              paid_at: @paid_at,
                              payment_type: "withdraw",
                              status: "pending",
                              for_resignation: true
                            )
    end

    def execute!
      build_payment_collection_records
      @payment_collection
    end

    private

    def build_payment_collection_records
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: @member,
                                  )

      # Transactions for savings
      @savings_types.each do |savings_type|
        amount = @savings_accounts.where(savings_type_id: savings_type.id).first.try(:balance)
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: savings_type.code,
                                    account_type: "SAVINGS"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      # Transactions for insurance
      @insurance_types.each do |insurance_type|
        amount = @insurance_accounts.where(insurance_type_id: insurance_type.id).first.try(:balance)
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      # Transactions for equity
      @equity_types.each do |equity_type|
        amount = @equity_accounts.where(equity_type_id: equity_type.id).first.try(:balance)
        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: equity_type.code,
                                    account_type: "EQUITY"
                                  )
        
        payment_collection_record.collection_transactions << collection_transaction
      end

      @payment_collection.payment_collection_records << payment_collection_record
    end
  end
end
