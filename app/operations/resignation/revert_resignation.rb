module Resignation
  class RevertResignation
    def initialize(member:)
      @member = member
    end

    def execute!
      PaymentCollection.where(id: @member.member_resignations.pluck(:payment_collection_id).uniq).destroy_all
      @member.member_resignations.destroy_all
      @member.update(status: 'active')

      @member.savings_accounts.each do |savings_account|
        Savings::UpdateMaintainingBalance.new(savings_account: savings_account).execute!
      end
    end
  end
end
