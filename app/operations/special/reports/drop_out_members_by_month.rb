module Special
  module Reports
    class DropOutMembersByMonth
      def initialize(month:, year:, officer_in_charge:)
        @month              = month
        @year               = year
        @threshold_date     = Date.civil(@year, @month, -1)
        @new_members        = []
        @resigned_members   = []
        @current_members    = []
        @data               = {}
        @active_members     = Member.active
        @officer_in_charge  = officer_in_charge
        @officers_in_charge = Center.all.pluck(:officer_in_charge).uniq.sort

        if @officer_in_charge
          @officers_in_charge = [@officer_in_charge]
          @active_members     = Member.active.joins(:center).where("centers.officer_in_charge = ?", @officer_in_charge)
        end
      end

      def execute!
        @active_members.each do |m|
          if m.previous_mfi_member_since
            if m.previous_mfi_member_since < Date.civil((@threshold_date - 1.month).year, (@threshold_date - 1.month).month, -1)
              @current_members << m
            end
          end
        end

        @new_members  = @active_members.where(
                          "extract(month from previous_mfi_member_since) = ? AND extract(year from previous_mfi_member_since) = ? AND date_resigned IS NULL", 
                          @threshold_date.month, 
                          @threshold_date.year
                        )

        @resigned_members = @active_members.where(
                              "extract(month from date_resigned) = ? AND extract(year from date_resigned) = ?",
                              @threshold_date.month,
                              @threshold_date.year
                            )

        @data[:new_members_count]       = @new_members.size
        @data[:resigned_members_count]  = @resigned_members.size
        @data[:current_members_count]   = @current_members.size
        @data[:actual_members_count]    = @data[:current_members_count] + @data[:new_members_count] - @data[:resigned_members_count]
        @data[:drop_out_rate]           = ((@data[:current_members_count] + @data[:new_members_count] - @data[:actual_members_count]).to_f / @data[:current_members_count].to_f).round(2)

        @data[:as_of] = @threshold_date

        @data[:officers_in_charge]  = []

        @officers_in_charge.each do |oic|
          d = {
            officer_in_charge: oic,
            new_members_count: 0,
            resigned_members_count: 0,
            current_members_count: 0,
            drop_out_rate: 0
          }

          @active_members.joins(:center).each do |m|
            if m.previous_mfi_member_since && m.center.try(:officer_in_charge) == oic
              if m.previous_mfi_member_since < Date.civil((@threshold_date - 1.month).year, (@threshold_date - 1.month).month, -1)
                d[:current_members_count] += 1
              end
            end
          end

          d[:new_members_count] = @active_members.joins(:center).where(
                                    "centers.officer_in_charge = ? AND 
                                     extract(month from previous_mfi_member_since) = ? AND
                                     extract(year from previous_mfi_member_since) = ? AND date_resigned IS NULL", 
                                     oic,
                                     @month,
                                     @year
                                  ).count

          d[:resigned_members_count]  = @active_members.joins(:center).where(
                                          "centers.officer_in_charge = ? AND 
                                           extract(month from date_resigned) = ? AND
                                           extract(year from date_resigned) = ?", 
                                           oic,
                                           @month,
                                           @year
                                        ).count

          d[:actual_members_count]  = d[:current_members_count] + d[:new_members_count] - d[:resigned_members_count]
          d[:drop_out_rate] = ((d[:current_members_count] + d[:new_members_count] - d[:actual_members_count]).to_f / d[:current_members_count].to_f).round(2)

          @data[:officers_in_charge] << d
        end

        @data
      end
    end
  end
end
