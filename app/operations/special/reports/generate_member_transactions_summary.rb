module Special
  module Reports
    class GenerateMemberTransactionsSummary
      def initialize(as_of:)
        @as_of            = as_of.to_date
        @data             = {}
        @members          = Member.active
        @centers          = Center.where(id: @members.pluck(:center_id).uniq).order("name ASC")
        @savings_types    = SavingsType.all
        @insurance_types  = InsuranceType.all
        @loan_products    = LoanProduct.all
      end

      def execute!
        @centers.each do |center|
          center_data                               = {}
          center_data[:members]                     = []
          center_data[:total_savings_withdrawals]   = 0
          center_data[:total_savings_deposits]      = 0
          center_data[:total_insurance_withdrawals] = 0
          center_data[:total_insurance_deposits]    = 0
          center_data[:total_loan_payments]         = 0

          center.members.active.each do |m|
            member_data = {
              id: m.id,
              name: m.full_name,
              identification_number: m.identification_number,
              savings: [],
              insurance: [],
              loan_payments: []
            }

            @savings_types.each do |savings_type|
              temp = {
                savings_type: savings_type.code,
                withdrawals:  SavingsAccountTransaction.approved.joins(:savings_account).where(
                                "savings_accounts.savings_type_id = ?
                                 AND savings_account_transactions.transacted_at <= ?
                                 AND transaction_type IN (?)",
                                savings_type.id,
                                as_of,
                                ["withdraw", "wp", "tax"]
                              ).sum(:amount),
                deposit:      SavingsAccount.approved.joins(:savings_account).where(
                                "savings_accounts.savings_type_id = ?
                                 AND savings_account_transactions.transacted_at <= ?
                                 AND transaction_type IN (?)",
                                savings_type.id,
                                as_of,
                                ["deposit", "interest"]
                              ).sum(:amount)
                            
              }

              member_data[:savings] << temp
            end

            @insurance_types.each do |insurance_type|
              temp = {
                insurance_type: insurance_type.code,
                withdrawals:  SavingsAccountTransaction.approved.joins(:insurance_account).where(
                                "insurance_accounts.insurance_type_id = ?
                                 AND insurance_account_transactions.transacted_at <= ?
                                 AND transaction_type IN (?)",
                                insurance_type.id,
                                as_of,
                                ["withdraw", "wp", "tax"]
                              ).sum(:amount),
                deposit:      SavingsAccount.approved.joins(:insurance_account).where(
                                "insurance_accounts.insurance_type_id = ?
                                 AND insurance_account_transactions.transacted_at <= ?
                                 AND transaction_type IN (?)",
                                insurance_type.id,
                                as_of,
                                ["deposit", "interest"]
                              ).sum(:amount)
                            
              }

              member_data[:insurance] << temp
            end

            @loan_products.each do |loan_product|
            end
          end
        end
      end
    end
  end
end
