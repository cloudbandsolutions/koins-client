module Special
  module Reports
    class CdaMemberCountSavingsTypes
      def initialize(year:, month:)
        @year           = year
        @month          = month
        @threshold_date = Date.civil(@year, @month, -1)
        @savings_types  = SavingsType.all
        @member_ids     = []
        @members        = []

        ActiveRecord::Base.connection.execute("SELECT id FROM members WHERE previous_mfi_member_since < '#{@threshold_date}'").each do |tuple|
          @member_ids << tuple["id"]
        end

        #@members              = Member.active.where(id: @member_ids)
        @members              = Member.where("previous_mfi_member_since < ? and status = ? ",@threshold_date, "active")
        @invalid_members      = Member.active.where.not(id: @member_ids)
        @data                 = {}
        @data[:savings_types] = []
        @data[:as_of]         = @threshold_date
      end

      def execute!
        @savings_types.each do |savings_type|
          member_count  = 0
          @members.each do |m|
            sa            = m.savings_accounts.where(savings_type_id: savings_type.id).first
            transactions  = sa.savings_account_transactions.approved.where("transacted_at <= ?", @threshold_date).order("transacted_at ASC")
            latest_trans  = transactions.last

            if latest_trans
              if latest_trans.ending_balance > 0
                member_count += 1
              end
            end
          end

          @data[:savings_types] << {
            savings_type: savings_type.name,
            member_count: member_count
          }
        end

        @data
      end
    end
  end
end
