module Special 
  module Generator
    class ResetIdNumbers
      def initialize
        @members  = Member.active
      end

      def execute!
        @members.each do |member|
          meta  = {}
          if member.meta
            meta = member.meta
          end

          if !meta['identification_numbers']
            meta['identification_numbers'] = []
          end

          meta['identification_numbers'] << member.identification_number

          new_identification_number = Members::GenerateMemberIdentificationNumber.new(
                                        cluster_code: member.branch.cluster.code, 
                                        branch_code: member.branch.code, 
                                        branch: member.branch
                                      ).execute!

          member.branch.update(
            member_counter: member.branch.member_counter + 1
          )

          member.update!(
            identification_number: new_identification_number,
            meta: meta
          )
        end
      end
    end
  end
end
