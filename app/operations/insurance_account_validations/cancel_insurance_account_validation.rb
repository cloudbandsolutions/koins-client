module InsuranceAccountValidations
  class CancelInsuranceAccountValidation
    attr_accessor :insurance_account_validation, :errors

    require 'application_helper'

    def initialize(insurance_account_validation:, user:)
      @insurance_account_validation = insurance_account_validation
      @user                         = user
      @c_working_date               = ApplicationHelper.current_working_date
    end

    def execute!
      @insurance_account_validation.update!(
        status: "cancelled",
        date_cancelled: @c_working_date,
        cancelled_by: @user.full_name
      )

      @insurance_account_validation
    end
  end
end
