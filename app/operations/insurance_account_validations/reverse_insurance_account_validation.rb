module InsuranceAccountValidations
  class ReverseInsuranceAccountValidation
    def initialize(insurance_account_validation:, user:)
      @user                         = user
      @approved_by                  = @user.full_name
      @insurance_account_validation = insurance_account_validation
    end

    def execute!
      @insurance_account_validation.update!(status: "reversed")

      @insurance_account_validation.insurance_account_validation_records.each do |insurance_account_validation_record|
        insurance_account_validation_record.update!(
          status: "pending"
          )
      end
    end
  end
end
