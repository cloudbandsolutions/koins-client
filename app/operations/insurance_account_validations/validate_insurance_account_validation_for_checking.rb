module InsuranceAccountValidations
  class ValidateInsuranceAccountValidationForChecking

    def initialize(insurance_account_validation:)
      @insurance_account_validation = insurance_account_validation
      @errors = []
    end

    def execute!
      if !@insurance_account_validation.pending? && !@insurance_account_validation.cancelled?
        @errors << "Invalid status"
      end

      @errors
    end
  end
end
