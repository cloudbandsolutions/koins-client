module InsuranceAccountValidations
	class ValidateMember
		attr_accessor :insurance_account_validation, :member, :errors, :resignation_date

		def initialize(insurance_account_validation:, member:, resignation_date:)
	     	@member                        = member
	     	# @resignation_date              = resignation_date
	     	@insurance_account_validation  = insurance_account_validation
	      	@errors = []
	    end

	    def execute!
	      #validate_member!
	      validate_duplicate_member!
	      validate_if_member_is_already_validated_to_pending_for_appoval_for_validation!
	      validate_member_balik_kasapi!
	      # validate_resignation_date!
	      @errors
	    end

	    private

	   	def validate_member!
			@lif = InsuranceType.where(code: "LIF").first
	     	@lif_insurance_account = @member.insurance_accounts.where(insurance_type_id: @lif.id, member_id: @member.id).first
	     	@rf = InsuranceType.where(code: "RF").first
	     	@rf_insurance_account = @member.insurance_accounts.where(insurance_type_id: @rf.id, member_id: @member.id).first
	     	@data = Insurance::GenerateInsuranceAccountDetailsForLifAndRfForValidation.new(member: @member, lif_insurance_account: @lif_insurance_account, rf_insurance_account: @rf_insurance_account).execute!
	      
	     	@number_of_days_lapsed = @data[:lif_num_weeks_past_due] * 7	   		
	    
	     	if @number_of_days_lapsed > 45
	     		@errors << "Member is lapsed"
	     	end
	    end

	    def validate_if_member_is_already_validated_to_pending_for_appoval_for_validation!
	    	insurance_account_validation_records = InsuranceAccountValidationRecord.where("status = ? OR status = ? OR status = ?", "for-approval", "for-validation", "pending")
	    	insurance_account_validation_records.each do |rec|
	    		if rec.member.identification_number == @member.identification_number
	    			@errors << "Member's Insurance Account is already validated"
		    	end
	    	end
		end

		def validate_member_balik_kasapi!
	    	insurance_account_validation_records = InsuranceAccountValidationRecord.where("status = ?", "approved")
	    	insurance_account_validation_records.each do |rec|
	    		if rec.member.meta.nil?
	    			if rec.member.identification_number == @member.identification_number
	    				if !rec.is_void?
	    					@errors << "Member's Insurance Account is already validated"
		    			end
	    			end
		    	else
		    		insurance_account_validation_recordss = InsuranceAccountValidationRecord.where("status = ? OR status = ? OR status = ?", "for-approval", "for-validation", "pending")
		    		insurance_account_validation_recordss.each do |recc|
	    				if recc.member.identification_number == @member.identification_number
	    					if !recc.is_void?
	    						@errors << "Member's Insurance Account is already validated"
		    				end
		    			end
	    			end
		    	end
	    	end
		end	

		# def validate_resignation_date!
		# 	if @resignation_date.empty?
		# 		@errors << "Resignation date cant be blank"
		# 	end
		# end

	    def validate_duplicate_member!
	    	@insurance_account_validation.insurance_account_validation_records.each do |rec|
	    		if rec.member_id == @member.id
	    			@errors << "Duplicate Member"
	    		end
	    	end	
	    end
	end
end