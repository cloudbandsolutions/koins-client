module InsuranceAccountValidations
  class ValidateInsuranceAccountValidationForApproval
    def initialize(insurance_account_validation:)
      @insurance_account_validation = insurance_account_validation
      @branch                       = insurance_account_validation.branch
      @errors = []
    end

    def execute!
      check_params!
      #check_insurance_account_validation_records_params!
      @errors
    end

    private

    def check_params!
      if @branch.nil?
        @errors << "Branch cant be blank."
      end

      if @insurance_account_validation.date_prepared.nil?
        @errors << "Date Prepared cant be blank."  
      end
    end

    def check_insurance_account_validation_records_params!  
    end
  end
end
