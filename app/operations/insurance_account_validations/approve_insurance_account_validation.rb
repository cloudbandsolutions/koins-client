module InsuranceAccountValidations
  class ApproveInsuranceAccountValidation
    attr_accessor :insurance_account_validation, :errors

    require 'application_helper'

    def initialize(insurance_account_validation:, user:)
      @insurance_account_validation = insurance_account_validation
      @user                         = user
      @interest_amount              = @insurance_account_validation.insurance_account_validation_records.sum(:interest)
      @voucher                      = InsuranceAccountValidations::ProduceVoucherForInterestDeposit.new(
                                      insurance_account_validation: @insurance_account_validation,
                                      is_remote: @insurance_account_validation.is_remote
                                      
                                      ).execute!
      @c_working_date               = ApplicationHelper.current_working_date
    end

    def execute!
      # if @interest_amount > 0
      #   @voucher.save!
      #   @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
      # end

      @voucher.save!
      @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!

      @insurance_account_validation.update!(
        status: "approved",
        reference_number: @voucher.reference_number,
        updated_at: @c_working_date,
        approved_by: @user,
        date_approved: @c_working_date
      )
       
      create_rf_insurance_deposits!

      # COMMMENT OUT
      create_lif_insurance_deposits!

      if Settings.activate_microloans
        withdraw_lif_and_rf_deposit_to_savings!
      elsif Settings.activate_microinsurance
        withdraw_lif_and_rf_deposit_to_zero_account!
      end

      approved_insurance_account_validation_record_status!     
    end

    private

    def approved_insurance_account_validation_record_status!
      @insurance_account_validation.insurance_account_validation_records.each do |insurance_account_validation_record|
        insurance_account_validation_record.update!(
          status: "approved",
          transaction_number: insurance_account_validation_record.id.to_s.rjust(6, "0")
          )
        
        if Settings.activate_microloans
          if insurance_account_validation_record.member_classification == "EXIT AGE (GK)"
            insurance_account_validation_record.member.update!(
              insurance_status: "resigned",
              insurance_date_resigned: insurance_account_validation_record.resignation_date,
              member_type: "GK"
            )
          elsif insurance_account_validation_record.member_classification == "DECEASED"
            insurance_account_validation_record.member.update!(
              insurance_status: "resigned",
              insurance_date_resigned: insurance_account_validation_record.resignation_date,
              is_deceased: true
            )
          else
            insurance_account_validation_record.member.update!(
              insurance_status: "resigned",
              insurance_date_resigned: insurance_account_validation_record.resignation_date
            )
          end
        elsif Settings.activate_microinsurance
          insurance_account_validation_record.member.update!(
            status: "resigned",
            insurance_status: "resigned",
            insurance_date_resigned: insurance_account_validation_record.resignation_date,
            date_resigned: insurance_account_validation_record.resignation_date,
            resignation_reason: "Resigned to MFI"
          )
        end  
      end
    end

    def create_rf_insurance_deposits!
      @insurance_account_validation.insurance_account_validation_records.each do |insurance_account_validation_record|
          
        member          = insurance_account_validation_record.member
        insurance_type    = InsuranceType.where(code: "RF").first
        insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).first
        insurance_account_transaction = InsuranceAccountTransaction.create!(
                                          amount: insurance_account_validation_record.interest.round(2),
                                          transacted_at: @c_working_date,
                                          created_at: @c_working_date,
                                          particular: "Interest of Retirement Fund per annum",
                                          transaction_type: "interest",
                                          voucher_reference_number: @voucher.reference_number,
                                          insurance_account: insurance_account,
                                          for_resignation: true
                                        )
        insurance_account_transaction.approve!(@user.full_name)
      end
    end

    def create_lif_insurance_deposits!
      @insurance_account_validation.insurance_account_validation_records.each do |insurance_account_validation_record|
          
        member          = insurance_account_validation_record.member
        insurance_type    = InsuranceType.where(code: "LIF").first
        insurance_account = InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).first
        insurance_account_transaction = InsuranceAccountTransaction.create!(
                                          amount: insurance_account_validation_record.equity_interest.round(2),
                                          transacted_at: @c_working_date,
                                          created_at: @c_working_date,
                                          particular: "Interest of Equity per week",
                                          transaction_type: "interest",
                                          voucher_reference_number: @voucher.reference_number,
                                          insurance_account: insurance_account,
                                          for_resignation: true,
                                          equity_value: insurance_account.equity_value
                                        )
        insurance_account_transaction.approve!(@user.full_name)
      end
    end

    def withdraw_lif_and_rf_deposit_to_zero_account!
      @insurance_account_validation.insurance_account_validation_records.each do |insurance_account_validation_record|
          
        member          = insurance_account_validation_record.member
        member.insurance_accounts.each do |insurance_account|

          if insurance_account.insurance_type.code == "LIF"
            equity_value = 0.00
            latest_ev = insurance_account.equity_value
            equity_value = latest_ev - latest_ev

            insurance_account.update!(equity_value: equity_value)
          else  
            equity_value = 0.00
          end
          
          balance = insurance_account.balance
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: balance.round(2),
                                            transacted_at: @c_working_date,
                                            created_at: @c_working_date,
                                            particular: "Withdrawal of #{insurance_account.insurance_type}",
                                            transapproved_action_type: "withdraw",
                                            voucher_reference_number: @voucher.reference_number,
                                            insurance_account: insurance_account,
                                            for_resignation: true,
                                            equity_value: equity_value
                                          )
          insurance_account_transaction.approve!(@user.full_name)
        end
      end
    end

    def withdraw_lif_and_rf_deposit_to_savings!
      @insurance_account_validation.insurance_account_validation_records.each do |insurance_account_validation_record|
          
        member          = insurance_account_validation_record.member
        member.insurance_accounts.each do |insurance_account|
          
          if insurance_account.insurance_type.code == "LIF"
            equity_value = 0.00
            latest_ev = insurance_account.equity_value
            equity_value = latest_ev - latest_ev

            insurance_account.update!(equity_value: equity_value)
          else  
            equity_value = 0.00
          end

          balance = insurance_account.balance
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: balance.round(2),
                                            transacted_at: @c_working_date,
                                            created_at: @c_working_date,
                                            particular: "Withdrawal of #{insurance_account.insurance_type}",
                                            transaction_type: "withdraw",
                                            voucher_reference_number: @voucher.reference_number,
                                            insurance_account: insurance_account,
                                            for_resignation: true,
                                            equity_value: equity_value
                                          )
          insurance_account_transaction.approve!(@user.full_name)
        end

        # half_adv_lif = insurance_account_validation_record.try(:advance_lif) / 2 
        due_to_members = (insurance_account_validation_record.try(:equity_interest) + insurance_account_validation_record.try(:interest) + insurance_account_validation_record.try(:rf) + insurance_account_validation_record.try(:advance_rf) + insurance_account_validation_record.try(:lif_50_percent) + insurance_account_validation_record.try(:advance_lif))

        if !insurance_account_validation_record.try(:policy_loan).nil?
          due_to_members = due_to_members - insurance_account_validation_record.try(:policy_loan)
        end 

        if insurance_account_validation_record.member_classification == "EXIT AGE (GK)"
          savings_type    = SavingsType.find(2)

          savings_account = SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id).first
          savings_account_transaction = SavingsAccountTransaction.create!(
                                            amount: due_to_members.round(2),
                                            transacted_at: @c_working_date,
                                            created_at: @c_working_date,
                                            particular: "Deposit of savings",
                                            transaction_type: "deposit",
                                            voucher_reference_number: @voucher.reference_number,
                                            savings_account: savings_account,
                                            for_resignation: true
                                          )
        else
          savings_type    = SavingsType.find(1)

          savings_account = SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id).first
          savings_account_transaction = SavingsAccountTransaction.create!(
                                            amount: due_to_members.round(2),
                                            transacted_at: @c_working_date,
                                            created_at: @c_working_date,
                                            particular: "Deposit of savings",
                                            transaction_type: "deposit",
                                            voucher_reference_number: @voucher.reference_number,
                                            savings_account: savings_account,
                                            for_resignation: true
                                          )
        end

        savings_account_transaction.approve!(@user.full_name)
      end
    end
  end
end
