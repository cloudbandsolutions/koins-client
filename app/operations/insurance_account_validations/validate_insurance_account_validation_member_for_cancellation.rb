module InsuranceAccountValidations
  class ValidateInsuranceAccountValidationMemberForCancellation

    def initialize(member:, reason:, date_cancelled:)
      @member = member
      @date_cancelled = date_cancelled
      @reason =reason
      @errors = []
    end

    def execute!
      if @reason.nil?
        @errors << "Reason cant be blank."
      end

      if @date_cancelled.nil?
        @errors << "Date cancelled cant be blank."  
      end

      if @member.nil?
        @errors << "Member cant be blank."
      end 

      @errors     
    end
  end
end
