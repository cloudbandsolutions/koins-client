module InsuranceAccountValidations
	class AddMemberToInsuranceAccountValidation
		attr_accessor :insurance_account_validation, :member, :resignation_date, :member_classification

		def initialize(insurance_account_validation:, member:, resignation_date:, member_classification:)
      @member                        = member
      @resignation_date              = resignation_date
      @member_classification         = member_classification
      # @resignation_date            = member.insurance_accounts.first.insurance_account_transactions.last.transaction_date.to_s 
      # COMMENT OUT
      @equity_interest_implementation_date =  Settings.equity_interest_implementation_date.to_date
      #@equity_interest_implementation_date = "2019-12-01".to_date
      @lif_50_percent                = 0.00
      @advance_lif                   = 0.00
      @advance_rf                    = 0.00
      @total                         = 0.00

      @lif = InsuranceType.where(code: "LIF").first
      @lif_insurance_account = @member.insurance_accounts.where(insurance_type_id: @lif.id, member_id: @member.id).first
      @rf = InsuranceType.where(code: "RF").first
      @rf_insurance_account = @member.insurance_accounts.where(insurance_type_id: @rf.id, member_id: @member.id).first
      @pl = InsuranceType.where(code: "PL").first
      @pl_insurance_account = @member.insurance_accounts.where(insurance_type_id: @pl.id, member_id: @member.id).first

      if @pl_insurance_account.present?
        @policy_loan = @pl_insurance_account.balance
      end

      @data = Insurance::GenerateInsuranceAccountDetailsForLifAndRfForValidation.new(member: @member, lif_insurance_account: @lif_insurance_account, rf_insurance_account: @rf_insurance_account, resignation_date: @resignation_date).execute!
      
      @equity_value         = @lif_insurance_account.equity_value.to_f

      @lif_current_balance = @data[:lif_current_balance]
      @rf_current_balance = @data[:rf_current_balance]
      @lif_amt_past_due = @data[:lif_amt_past_due]
      @rf_amt_past_due = @data[:rf_amt_past_due]
      @lif_num_weeks_past_due = @data[:lif_num_weeks_past_due]
      @rf_num_weeks_past_due = @data[:rf_num_weeks_past_due]
      @lif_insured_amount = @data[:lif_insured_amount]
      @rf_insured_amount = @data[:rf_insured_amount]
      @rf_num_weeks = @data[:rf_num_weeks]
      @number_of_days_lapsed = @rf_num_weeks_past_due * 7

      # Check if member has advance, lapsed and normal payment
      if @lif_num_weeks_past_due < 0 && @rf_num_weeks_past_due < 0
        @rf_amount = @rf_current_balance - (@rf_amt_past_due * -1)
        @lif_amount = @lif_current_balance - (@lif_amt_past_due * -1) 
        @advance_rf = @rf_amt_past_due * -1
        @advance_lif = @lif_amt_past_due * -1
      elsif @lif_num_weeks_past_due < 0 && @rf_num_weeks_past_due > 0
        @rf_amount = @rf_current_balance
        @lif_amount = @lif_current_balance - (@lif_amt_past_due * -1) 
        @advance_rf = @rf_amt_past_due * 0
        @advance_lif = @lif_amt_past_due * -1
      elsif @lif_num_weeks_past_due < 0 && @rf_num_weeks_past_due == 0
        @rf_amount = @rf_current_balance
        @lif_amount = @lif_current_balance - (@lif_amt_past_due * -1) 
        @advance_lif = @lif_amt_past_due * -1  
      elsif @lif_num_weeks_past_due == 0 && @rf_num_weeks_past_due > 0
        @rf_amount = @rf_current_balance
        @lif_amount = @lif_current_balance - (@lif_amt_past_due * -1) 
        @advance_rf = @rf_amt_past_due * 0
      elsif @lif_num_weeks_past_due == 0 && @rf_num_weeks_past_due == 0
        @rf_amount = @rf_current_balance
        @lif_amount = @lif_current_balance
      elsif @lif_num_weeks_past_due > 0 && @rf_num_weeks_past_due > 0
        @rf_amount = @rf_current_balance
        @lif_amount = @lif_current_balance
      end  

      # Check if member is 3 years above in KMBA
        @current_date = ApplicationHelper.current_working_date
        @recognition_date = @member.try(:previous_mii_member_since).try(:to_date)

        if !@recognition_date.nil?  
          @seconds_between = (@current_date.to_time - @recognition_date.to_time).abs
          @days_between = @seconds_between / 60 / 60 / 24
          @number_of_months = (@days_between / 30.44).floor
          @years = (@days_between / 365.242199).floor
          @months = @number_of_months - (@years * 12)
          # if @years >= 3
          #   if @lif_amount >= 2340
          #     @lif_50_percent = @lif_amount / 2
          #   end
          # end

          if @advance_lif > 0
            @lif_50_percent = (@equity_value - (@advance_lif / 2))
          else
            @lif_50_percent = @equity_value
          end

          # Old validation
          # @lif_50_percent = @lif_amount / 2
        end
      
      if @years >= 1 && @rf_current_balance >= 260 
        @interest = ::InsuranceAccountValidations::GenerateInsuranceInterest.new(insurance_account: @rf_insurance_account, insured_amount: @rf_insured_amount, num_weeks: @rf_num_weeks, num_weeks_past_due: @rf_num_weeks_past_due).execute!
        @interest_amount = @interest[:interest_table].last[:running_interest].to_f
      else
        @interest_amount = 0.00
      end

      # For equity interest
      w = ((@resignation_date.to_date - @equity_interest_implementation_date).to_i)/7
      if w >= 1
        @equity_interest = ::InsuranceAccountValidations::GenerateEquityInterest.new(lif_current_balance: @lif_current_balance, lif_insurance_account: @lif_insurance_account, resignation_date: @resignation_date, equity_interest_implementation_date: @equity_interest_implementation_date, ).execute!
        # @equity_interest_amount = @equity_interest[:equity_interest].last[:running_interest].to_f
        @equity_interest_amount = @equity_interest[:equity_interest].last[:interest].to_f
      else
        @equity_interest_amount = 0.00
      end

      @total = (@lif_50_percent + @rf_amount + @advance_lif + @advance_rf + @interest_amount + @equity_interest_amount).round(2)

      if @pl_insurance_account.present?
        if @policy_loan > 0
          @total = (@total - @policy_loan).round(2)
        end
      end

      @insurance_account_validation  = insurance_account_validation
    end

    def execute!
      build_insurance_account_validation_record!
      @insurance_account_validation
    end

    private

   	def build_insurance_account_validation_record!
   		insurance_account_validation_record = InsuranceAccountValidationRecord.new(
   													member: @member,
                            transaction_number: "",
                            lif_50_percent: @lif_50_percent,
                            resignation_date: @resignation_date,
                            member_classification: @member_classification,
                            rf: @rf_amount,
                            advance_lif: @advance_lif,
                            advance_rf: @advance_rf,
                            interest: @interest_amount,
                            total: @total,
                            equity_interest: @equity_interest_amount,
                            equity_value: @equity_value,
                            policy_loan: @policy_loan

   			)
   		@insurance_account_validation.insurance_account_validation_records << insurance_account_validation_record
    end
	end
end