module InsuranceAccountValidations
  class ValidateInsuranceAccountValidationForValidation

    def initialize(insurance_account_validation:)
      @insurance_account_validation = insurance_account_validation
      @errors = []
    end

    def execute!
      # if !@insurance_account_validation.for_validation?
      #   @errors << "Invalid status"
      # end

      @errors
    end
  end
end
