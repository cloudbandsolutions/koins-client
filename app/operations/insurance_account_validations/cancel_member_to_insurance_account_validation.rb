module InsuranceAccountValidations
  class CancelMemberToInsuranceAccountValidation
    attr_accessor :member, :date_cancelled, :reason, :insurance_account_validation_id, :errors

    require 'application_helper'

    def initialize(member:, reason:, date_cancelled:, user:, insurance_account_validation_id:)
      @reason                         = reason
      @member                         = member
      @insurance_account_validation_id   = insurance_account_validation_id
      @date_cancelled                 = date_cancelled
      @user                           = user
      @c_working_date                 = ApplicationHelper.current_working_date
    end

    def execute!
      create_insurance_account_validation_cancellation!

      @insurance_account_validation_cancellation.save!
    end

    def create_insurance_account_validation_cancellation!
      @insurance_account_validation_cancellation = InsuranceAccountValidationCancellation.new(
        member_id: @member,
        insurance_account_validation_id: @insurance_account_validation_id,
        date_cancelled: @date_cancelled,
        reason: @reason
        )

      @insurance_account_validation_cancellation
    end
  end
end
