module InsuranceAccountValidations
  class ProduceVoucherForInterestDeposit
    attr_accessor :insurance_account_validation, :voucher

    require 'application_helper'

    def initialize(insurance_account_validation:, is_remote: false)
      @insurance_account_validation = insurance_account_validation
      @particular                   = build_particular
      @c_working_date               = ApplicationHelper.current_working_date
      @book                         = 'JVB'
      @accounting_fund              = AccountingFund.where(name: "Mutual Benefit Fund").first
      @is_remote                    = is_remote

      @members  = Member.where(id: @insurance_account_validation.insurance_account_validation_records.pluck(:member_id))

      @lif_insurance_accounts = InsuranceAccount.joins(:insurance_type).where("insurance_types.code = ? AND insurance_accounts.member_id IN (?)", "LIF", @members.pluck(:id))
      @total_lif_balance      = @lif_insurance_accounts.sum(:balance)

      if @insurance_account_validation.pending? || @insurance_account_validation.for_approval? || @insurance_account_validation.for_validation? || @insurance_account_validation.cancelled? 
        if Settings.activate_microloans  
          @voucher = Voucher.new(
                      status: "pending", 
                      branch: @insurance_account_validation.branch, 
                      book: @book,
                      date_prepared: @c_working_date,
                      prepared_by: @insurance_account_validation.prepared_by, 
                      particular: @particular,
                      # or_number: @insurance_account_validation.or_number
                    )
        elsif Settings.activate_microinsurance  
          @voucher = Voucher.new(
                      status: "pending", 
                      branch: @insurance_account_validation.branch, 
                      book: @book,
                      date_prepared: @c_working_date,
                      prepared_by: @insurance_account_validation.prepared_by, 
                      particular: @particular,
                      accounting_fund: @accounting_fund
                    )
        end
      elsif @insurance_account_validation.approved? or @insurance_account_validation.reversed?
        @voucher = Voucher.where(reference_number: @insurance_account_validation.reference_number, branch_id: @insurance_account_validation.branch.id, book: @book).last
      else
        raise "Invalid insurance account validation"
      end

      @bank             = @insurance_account_validation.branch.bank
      @insurance_types  = InsuranceType.all
    end

    def execute!
      if @insurance_account_validation.pending? || @insurance_account_validation.for_approval? || @insurance_account_validation.for_validation? || @insurance_account_validation.cancelled?
        build_entries
      end

      @voucher
    end

    private

    def build_particular
      branch = @insurance_account_validation.branch
      members_for_particular = []
      @insurance_account_validation.insurance_account_validation_records.each do |iavr|
        members_for_particular << iavr.member.full_name_formatted
      end

      if Settings.activate_microloans
        particular = "Transfer of RF, Equity Value, Equity Interest and RF Interest to savings account of #{members_for_particular.join(', ')} - #{branch.name}"
      elsif Settings.activate_microinsurance
        particular = "Withdrawal of RF, LIFE, Equity Interest and RF Interest of #{members_for_particular.join(', ')} - #{branch.name}"
      end

      if !@insurance_account_validation.particular.nil?
        particular = @insurance_account_validation.particular
      end

      particular
    end

    def build_entries
      build_debit_entries
      build_credit_entries
    end

    def compute_rf_and_equity_interest
      rf_amount = @insurance_account_validation.insurance_account_validation_records.sum(:interest)
      equity_amount = @insurance_account_validation.insurance_account_validation_records.sum(:equity_interest)

      # TODO: Make this configurable
      if @is_remote
        dr_accounting_code = AccountingCode.where(id: 164).first

        @voucher.journal_entries << JournalEntry.new(
                                    amount: rf_amount, 
                                    post_type: 'DR', 
                                    accounting_code: dr_accounting_code
                                  )
      else
        dr_accounting_code = AccountingCode.where(id: Settings.receivable_from_mba_id).first
      
        @voucher.journal_entries << JournalEntry.new(
                                    amount: rf_amount + equity_amount, 
                                    post_type: 'DR', 
                                    accounting_code: dr_accounting_code
                                  )
      end
    end

    # for old validation process
    # def compute_rf_interest
    #   amount    = @insurance_account_validation.insurance_account_validation_records.sum(:interest)

    #   # TODO: Make this configurable
    #   if @is_remote
    #     dr_accounting_code = AccountingCode.where(id: 164).first
    #   else
    #     dr_accounting_code = AccountingCode.where(id: Settings.receivable_from_mba_id).first
    #   end

    #   @voucher.journal_entries << JournalEntry.new(
    #                                 amount: amount, 
    #                                 post_type: 'DR', 
    #                                 accounting_code: dr_accounting_code
    #                               )
    # end

    # For KMBA only
    def compute_equity_interest
      amount    = @insurance_account_validation.insurance_account_validation_records.sum(:equity_interest)

      # TODO: Make this configurable
      dr_accounting_code = AccountingCode.where(id: 312).first
      
      if amount > 0
        @voucher.journal_entries << JournalEntry.new(
                                    amount: amount, 
                                    post_type: 'DR', 
                                    accounting_code: dr_accounting_code
                                  )
      end
    end  

    def compute_total_lif_and_equity_interest
      # TODO: Config accounting code for total_lif_accounting_code
      if @is_remote
        lif_50_percent_amount     = @insurance_account_validation.insurance_account_validation_records.sum(:lif_50_percent)
        equity_interest           = @insurance_account_validation.insurance_account_validation_records.sum(:equity_interest)
        amount                    = lif_50_percent_amount + equity_interest
        total_lif_accounting_code = AccountingCode.find(160)
      else
        total_lif_amount          = @lif_insurance_accounts.sum(:balance)
        equity_interest           = @insurance_account_validation.insurance_account_validation_records.sum(:equity_interest)
        amount = total_lif_amount + equity_interest
        total_lif_accounting_code = AccountingCode.find(96)
      end

      @voucher.journal_entries << JournalEntry.new(
                                    amount: amount,
                                    post_type: 'DR',
                                    accounting_code: total_lif_accounting_code
                                  )
    end

    # for old validation process
    def compute_total_lif
      # TODO: Config accounting code for total_lif_accounting_code
      if @is_remote
        total_lif_amount          = @insurance_account_validation.insurance_account_validation_records.sum(:lif_50_percent)
        total_lif_accounting_code = AccountingCode.find(160)
      else
        total_lif_amount          = @lif_insurance_accounts.sum(:balance)
        total_lif_accounting_code = AccountingCode.find(96)
      end

      @voucher.journal_entries << JournalEntry.new(
                                    amount: total_lif_amount,
                                    post_type: 'DR',
                                    accounting_code: total_lif_accounting_code
                                  )
    end

    # For KMBA only
    def compute_lif_advanced
      amount          = @insurance_account_validation.insurance_account_validation_records.sum(:advance_lif)
      accounting_code = AccountingCode.find(136)

      @voucher.journal_entries << JournalEntry.new(
                                    amount: amount,
                                    post_type: 'DR',
                                    accounting_code: accounting_code
                                  )
    end

    def compute_rf_and_interest
      # RF + Interest
      rf_and_interest_amount = @insurance_account_validation.insurance_account_validation_records.sum(:rf) + @insurance_account_validation.insurance_account_validation_records.sum(:advance_rf) + @insurance_account_validation.insurance_account_validation_records.sum(:interest)

      if @is_remote
        rf_and_interest_accounting_code = AccountingCode.find(95)
      else
        rf_and_interest_accounting_code = AccountingCode.find(97)
      end

      @voucher.journal_entries << JournalEntry.new(
                                    amount: rf_and_interest_amount,
                                    post_type: 'DR',
                                    accounting_code: rf_and_interest_accounting_code
                                  )
    end


    def build_debit_entries
      # new implementation
      compute_rf_and_equity_interest
      # COMMENT OUT
      # compute_rf_interest
      # compute_total_lif
      compute_total_lif_and_equity_interest

      if @is_remote
        compute_lif_advanced 
        compute_equity_interest       
      end

      compute_rf_and_interest
    end

    def build_credit_entries
      compute_interest_credit
      compute_lif_withdrawal_and_savings
      compute_lif_withdrawal_and_savings_for_gk
      # COMMENT OUT
      compute_equity_interest_credit

      if @insurance_account_validation.total_policy_loan > 0
        compute_policy_loan
      end
    end

    # Compute equity interest entry
    def compute_equity_interest_credit
      if @is_remote
        cr_accounting_code  = AccountingCode.where(id: 80).first
      else
        cr_accounting_code  = AccountingCode.where(id: 96).first
      end

      amount    = @insurance_account_validation.insurance_account_validation_records.sum(:equity_interest)

      if amount > 0
        @voucher.journal_entries << JournalEntry.new(
                                      amount: amount,
                                      post_type: 'CR',
                                      accounting_code: cr_accounting_code
                                    )

      end
    end

    def compute_policy_loan
      amount              = @insurance_account_validation.insurance_account_validation_records.sum(:policy_loan)
      
      cr_accounting_code  = AccountingCode.where(id: 2049).first

      if amount > 0
        @voucher.journal_entries << JournalEntry.new(
                                      amount: amount,
                                      post_type: 'CR',
                                      accounting_code: cr_accounting_code
                                    )

      end
    end

    def compute_interest_credit
      if @is_remote
        cr_accounting_code  = AccountingCode.where(id: 95).first
      else
        cr_accounting_code  = AccountingCode.where(id: Settings.payable_to_mba_rf_id).first
      end

      amount    = @insurance_account_validation.insurance_account_validation_records.sum(:interest)

      if amount > 0
        @voucher.journal_entries << JournalEntry.new(
                                      amount: amount,
                                      post_type: 'CR',
                                      accounting_code: cr_accounting_code
                                    )

      end
    end

    def compute_lif_withdrawal_and_savings_for_gk
      # WIP: Savings = RF + Interest + Equity Interest + Advanced Payment + 50%
      # FOR GK MEMBERS
      if @insurance_account_validation.insurance_account_validation_records.where("member_classification = ?", "EXIT AGE (GK)").count > 0
        savings_amount            = 0.00

        if @is_remote
          savings_accounting_code = AccountingCode.find(171)
        else
          savings_accounting_code = AccountingCode.find(288)
        end

        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification = ?", "EXIT AGE (GK)").sum(:rf)
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification = ?", "EXIT AGE (GK)").sum(:advance_rf)
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification = ?", "EXIT AGE (GK)").sum(:interest)
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification = ?", "EXIT AGE (GK)").sum(:advance_lif)
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification = ?", "EXIT AGE (GK)").sum(:lif_50_percent)
        # COMMENT OUT
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification = ?", "EXIT AGE (GK)").sum(:equity_interest)
        # savings_amount += @total_lif_balance / 2

        if @insurance_account_validation.total_policy_loan > 0
          savings_amount -= @insurance_account_validation.insurance_account_validation_records.where("member_classification = ?", "EXIT AGE (GK)").sum(:policy_loan)
        end

        @voucher.journal_entries << JournalEntry.new(
                                      amount: savings_amount,
                                      post_type: 'CR',
                                      accounting_code: savings_accounting_code
                                    )
      end
    end

    def compute_lif_withdrawal_and_savings
      if !@is_remote
        lif_withdrawal_amount  = @total_lif_balance
        lif_withdrawal_amount -= (@insurance_account_validation.insurance_account_validation_records.sum(:advance_lif) / 2)
        lif_withdrawal_amount -= @insurance_account_validation.insurance_account_validation_records.sum(:equity_value)
        # lif_withdrawal_amount -= @insurance_account_validation.insurance_account_validation_records.sum(:lif_50_percent)

        if Settings.activate_microloans
          # MBA Life Withdrawal
          lif_withdrawal_accounting_code = AccountingCode.find(311)

          @voucher.journal_entries << JournalEntry.new(
                                        amount: lif_withdrawal_amount,
                                        post_type: 'CR',
                                        accounting_code: lif_withdrawal_accounting_code
                                      )
        end
      end

      # WIP: Savings = RF + Interest + Equity Interest + Advanced Payment + 50%
      # FOR NOT GK MEMBERS
      if @insurance_account_validation.insurance_account_validation_records.where("member_classification != ?", "EXIT AGE (GK)").count > 0
        savings_amount            = 0.00

        if @is_remote
          savings_accounting_code = AccountingCode.find(171)
        else
          # Due to members
          savings_accounting_code = AccountingCode.find(102)
        end

        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification != ?", "EXIT AGE (GK)").sum(:rf)
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification != ?", "EXIT AGE (GK)").sum(:advance_rf)
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification != ?", "EXIT AGE (GK)").sum(:interest)
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification != ?", "EXIT AGE (GK)").sum(:advance_lif)
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification != ?", "EXIT AGE (GK)").sum(:lif_50_percent)
        # COMMENT OUT
        savings_amount += @insurance_account_validation.insurance_account_validation_records.where("member_classification != ?", "EXIT AGE (GK)").sum(:equity_interest)
        # savings_amount += @total_lif_balance / 2

        if @insurance_account_validation.total_policy_loan > 0
          savings_amount -= @insurance_account_validation.insurance_account_validation_records.where("member_classification != ?", "EXIT AGE (GK)").sum(:policy_loan)
        end

        @voucher.journal_entries << JournalEntry.new(
                                      amount: savings_amount,
                                      post_type: 'CR',
                                      accounting_code: savings_accounting_code
                                    )
      end
    end
  end
end
