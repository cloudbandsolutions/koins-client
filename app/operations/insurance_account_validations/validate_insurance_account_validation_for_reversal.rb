module InsuranceAccountValidations
  class ValidateInsuranceAccountValidationForReversal
    attr_accessor :insurance_account_validation, :errors

    def initialize(insurance_account_validation:)
      @insurance_account_validation = insurance_account_validation
      @errors = []
    end

    def execute!
      @errors
    end
  end
end
