module InsuranceAccountValidations
  class ValidateInsuranceAccountValidation
    attr_accessor :insurance_account_validation, :errors

    require 'application_helper'

    def initialize(insurance_account_validation:, user:)
      @insurance_account_validation = insurance_account_validation
      @user                         = user
      @c_working_date               = ApplicationHelper.current_working_date
    end

    def execute!
      @insurance_account_validation.update!(
        status: "for-approval",
        date_validated: @c_working_date,
        validated_by: @user.full_name
      )

      # Yung date kung kelan inapproved/validate ng AO ang magiging resignation date
      # @insurance_account_validation.insurance_account_validation_records.each do |iavr|
      #   iavr.update!(
      #     resignation_date: @c_working_date
      #   )
      # end

      @insurance_account_validation
    end
  end
end
