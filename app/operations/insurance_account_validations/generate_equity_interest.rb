module InsuranceAccountValidations
  class GenerateEquityInterest
    def initialize(lif_insurance_account:, resignation_date:, equity_interest_implementation_date:, lif_current_balance:)
      @equity_interest_implementation_date = equity_interest_implementation_date
      @resignation_date                    = resignation_date.to_date
      @lif_insurance_account               = lif_insurance_account
      @lif_current_balance                 = lif_current_balance
      @lif_ending_balance                  = @lif_insurance_account.insurance_account_transactions.where(
                                                            "transaction_date < ? AND status = ?",
                                                            equity_interest_implementation_date, 
                                                            "approved").order("id ASC").last.try(:ending_balance)
      if @lif_ending_balance.nil?
        @lif_50_percent = 0.00
        @equity_value   = 0.00
      else
        @lif_50_percent = @lif_ending_balance.try(:to_f) / 2
        @equity_value   = @lif_insurance_account.equity_value.try(:to_f)
      end

      @amount_after_implementation = @lif_current_balance - (@lif_50_percent * 2)
      @num_weeks_after_implementation = @amount_after_implementation.to_i / 15

      @data                                = {}
      @weekly_payment                      = 7.50

      @data[:equity_interest]  = []

      @num_weeks = ((@resignation_date - @equity_interest_implementation_date).to_i)/7
      # TODO: Change this to parameter/settings
      # @interest_rate      = 0.01
      # @weekly             = 0.0000961
      @interest_rate_weekly = 0.00019230769

      @equity_interest   = {}
    end

    def execute!
      tmp = {}

      # Old
      # @equity_interest = ((@lif_50_percent * @interest_rate_weekly) * @num_weeks).round(2)
      @equity_interest = ((@equity_value * @interest_rate_weekly) * @num_weeks).round(2)

      if @num_weeks_after_implementation > 0
        @equity_interest_after = ((@amount_after_implementation * @interest_rate_weekly) * @num_weeks_after_implementation).round(2)
      else
        @equity_interest_after = 0.00
      end

      tmp[:interest] = @equity_interest + @equity_interest_after

      @data[:equity_interest] << tmp
      
      # running_balance  = 0.00
      # running_interest = 0.00

      # # For weekly computation
      # @num_weeks.times do |i|
      #   running_balance       = @lif_50_percent + running_interest + @weekly_payment
      #   tmp                   = {}
      #   c                     = i + 1
      #   tmp[:weekly_index]     = c
      #   tmp[:running_balance] = running_balance
      #   tmp[:interest]        = (running_balance * @interest_rate_weekly).round(2)
      #   running_interest      += tmp[:interest].round(2)
      #   tmp[:running_balance_save_interest] = (tmp[:running_balance] + running_interest).round(2)
      #   tmp[:running_interest] = running_interest

      #   running_balance       += running_balance + tmp[:interest]

      #   @data[:equity_interest] << tmp
      # end

      @data
    end
  end
end
