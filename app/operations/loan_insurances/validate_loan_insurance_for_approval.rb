module LoanInsurances
  class ValidateLoanInsuranceForApproval
    def initialize(loan_insurance:)
      @loan_insurance      = loan_insurance
      @loan_insurance_type = loan_insurance.loan_insurance_type
      @branch              = loan_insurance.branch
      @errors = []
    end

    def execute!
      check_params!
      @errors
    end

    private

    def check_params!
      if @loan_insurance_type.nil?
        @errors << "Loan Insurance Type cant be blank."
      end

      if @branch.nil?
        @errors << "Branch cant be blank."
      end

      if @loan_insurance.date_prepared.nil?
        @errors << "Date Prepared cant be blank."  
      end

      if @loan_insurance.total_amount.nil?
        @errors << "Total Amount cant be blank."
      end
    end
  end
end
