module LoanInsurances
  class LoadLoanInsuranceRecordsFromCsvFile
    attr_accessor :collection_type, :file, :loan_insurance_type, :branch, :date_prepared, :prepared_by

    def initialize(collection_type:, file:, loan_insurance_type:, branch:, date_prepared:, prepared_by:)
      @collection_type     = collection_type
      @file                = file
      @branch              = branch
      @prepared_by         = prepared_by
      @date_prepared       = date_prepared
      @loan_insurance_type = loan_insurance_type
      @particular          = "Loan Insurance #{@branch}"
      @loan_insurance
    end

    def execute!    
      load_csv_file!
      @loan_insurance
    end

    private

    def load_csv_file!
      @loan_insurance = LoanInsurance.new(
          collection_type: @collection_type,
          branch: @branch,
          loan_insurance_type: @loan_insurance_type,
          particular: @particular,
          prepared_by: @prepared_by,
          date_prepared: @date_prepared,
          total_amount: 0.00
        )
        
      CSV.foreach(@file.path, headers: true) do |row|
        if !row['identification_number'].nil?
          identification_number = row['identification_number']
        end

        member = Member.where(identification_number: identification_number).first           
        if !member.nil?
          loan_insurance_record = LoanInsuranceRecord.new(
            member: member,
            loan_category: row['loan_category'],
            pn_number: row['pn_number'],
            date_released: row['date_released'],
            maturity_date: row['maturity_date'],
            loan_term: row['loan_term'],
            loan_amount: row['loan_amount'],
            loan_insurance_amount: row['loan_insurance_amount'],
            uuid: row['uuid']
            )
          @loan_insurance.loan_insurance_records << loan_insurance_record
        end 
      end
      @loan_insurance.save!    
    end
  end
end  