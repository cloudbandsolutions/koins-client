module LoanInsurances
  class ApproveLoanInsurance
    attr_accessor :loan_insurance, :errors

    require 'application_helper'

    def initialize(loan_insurance:, user:)
      @loan_insurance  = loan_insurance
      @user            = user
      @c_working_date  = ApplicationHelper.current_working_date
    end

    def execute!
      voucher = LoanInsurances::ProduceVoucherForLoanInsurance.new(loan_insurance: @loan_insurance, user: @user).execute!
      voucher.save!
      Accounting::ApproveVoucher.new(voucher: voucher, user: @user).execute!

      @loan_insurance.update!(
        status: "approved", 
        reference_number: voucher.reference_number,
        updated_at: @c_working_date
      )
      
      approved_loan_insurance_record_status!
    end

    private

    def approved_loan_insurance_record_status!
      @loan_insurance.loan_insurance_records.each do |loan_insurance_record|
        loan_insurance_record.update!(
          status: "approved"
          )
      end
    end
  end
end
