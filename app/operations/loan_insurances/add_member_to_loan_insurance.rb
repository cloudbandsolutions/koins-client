module LoanInsurances
	class AddMemberToLoanInsurance
		attr_accessor :loan_insurance, :member

		def initialize(loan_insurance:, member:)
      @member         = member
      @loan_insurance = loan_insurance
    end

    def execute!
      build_loan_insurance_record!
      @loan_insurance
    end

    private

   	def build_loan_insurance_record!
   		loan_insurance_record = LoanInsuranceRecord.new(
   													member: @member,
                            loan_term: 0,
                            loan_category: "",
                            date_released: "",
                            maturity_date: "",
   													loan_amount: 0.00,
   													loan_insurance_amount: 0.00,
   													loan_amount: 0.00
   			)
   		@loan_insurance.loan_insurance_records << loan_insurance_record
    end
	end
end