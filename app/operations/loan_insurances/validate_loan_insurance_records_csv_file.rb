module LoanInsurances
  class ValidateLoanInsuranceRecordsCsvFile
    attr_accessor :collection_type, :loan_insurance_record, :branch, :loan_insurance_type, :date_prepared, :errors

    def initialize(collection_type:, loan_insurance_record:, loan_insurance_type:, branch:, date_prepared:)
      @collection_type = collection_type
      @loan_insurance_record = loan_insurance_record
      @loan_insurance_type = loan_insurance_type
      @branch = branch
      @date_prepared = date_prepared
      @errors = []
    end

    def execute!
      check_parameters!
      # check_member_parameters!
      validate_has_pending_transactions!
      @errors
    end

    private

    def check_parameters!
      if !@collection_type.present?
        @errors << "Collection Type required."
      end

      if !@loan_insurance_type.present?
        @errors << "Loan Insurance Type required."
      end

      if !@branch.present?
        @errors << "Branch required"
      end

      if !@date_prepared
        @errors << "Date prepared required"
      end

      if @loan_insurance_record['loan_category'].nil?
        @errors << "Loan Insurance category cant be blank."
      end

      if @loan_insurance_record['date_released'].nil?
        @errors << "Date Released cant be blank."
      end

      if @loan_insurance_record['maturity_date'].nil?
        @errors << "Maturity Date cant be blank."
      end

      if @loan_insurance_record['loan_term'].nil?
        @errors << "Loan Term cant be blank."
      end

      if @loan_insurance_record['loan_insurance_amount'].nil?
        @errors << "Loan Insurance Amount cant be blank."
      end

      if @loan_insurance_record['loan_amount'].nil?
        @errors << "Loan Amount cant be blank."
      end
    end

    # def check_member_parameters!
    #   if !@loan_insurance_record['first_name'].present?
    #     @errors << "Member first name required."
    #   end

    #   if !@loan_insurance_record['last_name'].present?
    #     @errors << "Member last name required"
    #   end
    # end

    def validate_has_pending_transactions!
      if @branch.present?
        if LoanInsurance.where(branch_id: @branch.id, status: "pending").count > 0
          @errors << "This branch still has pending deposit transactions"
        end
      end
    end
  end
end