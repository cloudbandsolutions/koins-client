module LoanInsurances
  class CreateLoanInsurance
    attr_accessor :collection_type, :branch, :date_prepared, :loan_insurance_type, :prepared_by, :loan_insurance

    def initialize(collection_type:, branch:, loan_insurance_type:, date_prepared:, prepared_by:)
      @collection_type     = collection_type
      @branch              = branch
      @loan_insurance_type = loan_insurance_type
      @date_prepared       = date_prepared
      @prepared_by         = prepared_by
      @particular          = "Loan Insurance #{@branch} - #{@loan_insurance_type}"
    end

    def execute!
      build_loan_insurance!
      @loan_insurance
    end

    private

    def build_loan_insurance!
        @loan_insurance      = LoanInsurance.new(
                              collection_type: @collection_type,
                              branch: @branch,
                              loan_insurance_type: @loan_insurance_type,
                              particular: @particular,
                              prepared_by: @prepared_by,
                              date_prepared: @date_prepared,
                              total_amount: 0.00,
                            )
        @loan_insurance
    end
  end
end
