module LoanInsurances
  class ValidateLoanInsuranceForReversal
    attr_accessor :loan_insurance, :errors

    def initialize(loan_insurance:)
      @loan_insurance = loan_insurance
      @errors = []
    end

    def execute!
      @errors
    end
  end
end
