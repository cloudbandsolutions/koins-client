module LoanInsurances
	class ProduceVoucherForLoanInsurance
		attr_accessor :loan_insurance, :voucher

        def initialize(loan_insurance:, user:)
        	@loan_insurance        = loan_insurance
        	@loan_insurance_type   = loan_insurance.loan_insurance_type
            @accounting_fund       = loan_insurance.loan_insurance_type.accounting_fund
            @branch                = loan_insurance.branch
        	@particular     	   = "Loan Insurance #{@branch} - #{@loan_insurance_type}"
        	@user 				   = user 
        end

        def execute!
        	build_voucher 
            @voucher
        end

        private

        def build_voucher
            if @loan_insurance.pending? && @loan_insurance.cash?
                @voucher = Voucher.new(
                    or_number: @loan_insurance.or_number, 
                    status: "pending", 
                    branch: @branch, 
                    book: "CRB", 
                    date_prepared: Date.today, 
                    prepared_by: @loan_insurance.prepared_by, 
                    particular: @particular,
                    accounting_fund: @accounting_fund
                       )
                build_cash_debit_entries!
                build_cash_credit_entries!

            elsif @loan_insurance.pending? && @loan_insurance.unremitted?
                @voucher = Voucher.new(
                    or_number: @loan_insurance.or_number, 
                    status: "pending", 
                    branch: @branch, 
                    book: "JVB", 
                    date_prepared: Date.today, 
                    prepared_by: @loan_insurance.prepared_by, 
                    particular: @particular,
                    accounting_fund: @accounting_fund
                    )
                build_unremitted_credit_entries!
                build_unremitted_debit_entries!
            else
                @voucher = Voucher.where(or_number: @loan_insurance.or_number).first
            end
        end

        def build_cash_credit_entries!
            amount = @loan_insurance.total_amount
            journal_entry = JournalEntry.new(
                    amount: amount,
                    post_type: 'CR',
                    accounting_code: @loan_insurance.loan_insurance_type.cash_credit_accounting_code
                  )

            @voucher.journal_entries << journal_entry
        end

        def build_cash_debit_entries!
            amount = @loan_insurance.total_amount
            journal_entry = JournalEntry.new(
                    amount: amount,
                    post_type: 'DR',
                    accounting_code: @loan_insurance.loan_insurance_type.cash_debit_accounting_code
                  )

            @voucher.journal_entries << journal_entry    
        end

        def build_unremitted_credit_entries!
            amount = @loan_insurance.total_amount
            journal_entry = JournalEntry.new(
                    amount: amount,
                    post_type: 'CR',
                    accounting_code: @loan_insurance.loan_insurance_type.unremitted_credit_accounting_code
                  )

            @voucher.journal_entries << journal_entry 
        end

        def build_unremitted_debit_entries!
            amount = @loan_insurance.total_amount
            journal_entry = JournalEntry.new(
                    amount: amount,
                    post_type: 'DR',
                    accounting_code: @loan_insurance.loan_insurance_type.unremitted_debit_accounting_code
                  )

            @voucher.journal_entries << journal_entry               
        end
	end
end
