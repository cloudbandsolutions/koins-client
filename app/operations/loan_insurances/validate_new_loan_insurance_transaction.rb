module LoanInsurances
  class ValidateNewLoanInsuranceTransaction
    attr_accessor :collection_type, :branch_id, :date_prepared, :errors, :loan_insurance_type_id

    def initialize(branch_id:, date_prepared:, loan_insurance_type_id:, collection_type:)
      @branch              = Branch.where(id: branch_id).first
      @loan_insurance_type = LoanInsuranceType.where(id: loan_insurance_type_id).first
      @date_prepared       = date_prepared
      @collection_type     = collection_type
      @errors              = []
    end

    def execute!
      validate_required_parameters!
      validate_has_pending_transactions!
      @errors
    end

    private

    def validate_required_parameters!
      if !@collection_type.present?
        @errors << "Collection Type required."
      end

      if !@branch.present?
        @errors << "Branch required"
      end

      if !@loan_insurance_type.present?
        @errors << "Loan Insurance Type required"
      end

      if !@date_prepared.present?
        @errors << "Date prepared required"
      end
    end

    def validate_has_pending_transactions!
      if @branch.present?
        if LoanInsurance.where(branch_id: @branch.id, status: "pending").count > 0
          @errors << "This branch still has pending deposit transactions"
        end
      end
    end
  end
end