module LoanInsurances
  class ReverseLoanInsurance
    def initialize(loan_insurance:, user:)
      @user               = user
      @approved_by        = @user.full_name
      @loan_insurance     = loan_insurance
    end

    def execute!
      if @loan_insurance.unremitted?
        reverse_unremitted_loan_insurance!
        @loan_insurance.update!(status: "reversed")

        @loan_insurance.loan_insurance_records.each do |loan_insurance_record|
          loan_insurance_record.update!(
            status: "pending"
            )
        end
      elsif @loan_insurance.cash?
        reverse_cash_loan_insurance!
        new_or_number = "#{@loan_insurance.or_number} - REVERSED - #{Time.now.to_i}"
        @loan_insurance.update!(status: "reversed", or_number: new_or_number)

        @loan_insurance.loan_insurance_records.each do |loan_insurance_record|
          loan_insurance_record.update!(
            status: "pending"
            )
        end
      else
        raise "Invalid loan insurance type"
      end
    end

    private

    def reverse_unremitted_loan_insurance!
      # Generate reverse voucher
      voucher = LoanInsurances::ProduceVoucherForLoanInsurance.new(loan_insurance: @loan_insurance, user: @user).execute!
      reversed_voucher = Accounting::GenerateReverseEntryForLoanInsurance.new(voucher: voucher).execute!
      @loan_insurance.reversed_reference_number = reversed_voucher.reference_number
    end

    def reverse_cash_loan_insurance!
      # Generate reverse voucher
      voucher = LoanInsurances::ProduceVoucherForLoanInsurance.new(loan_insurance: @loan_insurance, user: @user).execute!
      reversed_voucher = Accounting::GenerateReverseEntryForLoanInsurance.new(voucher: voucher).execute!
      @loan_insurance.reversed_reference_number = reversed_voucher.reference_number
    end
  end
end
