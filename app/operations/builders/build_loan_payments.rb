module Builders
  class BuildLoanPayments
    def initialize
      @loan_payments  = LoanPayment.where(status: "approved", is_void: nil)

      @records        = []
    end

    def execute!
      @loan_payments.each do |loan_payment|
        loan  = loan_payment.loan
        meta  = {
          particular: loan_payment.particular,
          member_account_uuid: loan_payment.savings_account.try(:uuid),
          accounting_entry_reference_number: loan_payment.voucher_reference_number,
          wp_particular: loan_payment.wp_particular
        }

        @records << {
          uuid: loan_payment.uuid,
          amount: (loan_payment.paid_principal + loan_payment.paid_interest),
          loan_uuid: loan.uuid,
          transaction_type: "LOAN_PAYMENT",
          transaction_date: loan_payment.paid_at,
          transaciton_hour: 0,
          transaction_minute: 0,
          transacted_at: loan_payment.paid_at.to_time,
          status: 'approved',
          meta: meta
        }
      end

      @records
    end
  end
end
