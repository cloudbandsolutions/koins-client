module Builders
  class BuildUsers
    def initialize
      @data   = []
      @users  = User.all
    end

    def execute!
      @users.each do |user|
        meta = {
        }

        @data << {
          email: user.email,
          encrypted_password: user.encrypted_password,
          identification_number: user.identification_number ,
          first_name: user.first_name,
          last_name: user.last_name,
          role: user.role,
          username: user.username,
          meta: meta
        }
      end

      @data
    end
  end
end
