module Builders
  class BuildAccountTransactions
    def initialize
      @data = []
    end

    def execute!
      build_savings_account_transactions!
      build_insurance_account_transactions!
      build_equity_account_transactions!

      @data
    end

    private 
    
    def build_savings_account_transactions!
      SavingsAccountTransaction.all.order("transacted_at ASC").each do |sat|
        meta  = {
          particular: sat.particular,
          is_adjustment: sat.is_adjustment,
          for_resignation: sat.for_resignation,
          for_loan_payments: sat.for_loan_payments,
          beginning_balance: sat.beginning_balance,
          ending_balance: sat.ending_balance,
          accounting_entry_reference_number: sat.voucher_reference_number,
          legacy: sat
        }

        @data <<  {
          uuid: sat.uuid,
          amount: sat.amount,
          member_account_uuid: sat.savings_account.uuid,
          transaction_type: sat.transaction_type,
          transaction_date: sat.transacted_at.to_date,
          transaction_hour: sat.transacted_at.hour,
          transaction_minute: sat.transacted_at.min,
          transacted_at: sat.transacted_at,
          status: sat.status,
          meta: meta
        }
      end
    end

    def build_insurance_account_transactions!
      InsuranceAccountTransaction.all.each do |iat|
        meta  = {
          particular: iat.particular,
          is_adjustment: iat.is_adjustment,
          for_resignation: iat.for_resignation,
          for_loan_payments: iat.for_loan_payments,
          for_exit_age: iat.for_exit_age,
          beginning_balance: iat.beginning_balance,
          ending_balance: iat.ending_balance,
          accounting_entry_reference_number: iat.voucher_reference_number
        }
        
        @data <<  {
          uuid: iat.uuid,
          amount: iat.amount,
          member_account_uuid: iat.insurance_account.uuid,
          transaction_type: iat.transaction_type,
          transaction_date: iat.transaction_date,
          transaction_hour: iat.transacted_at.hour,
          transaction_minute: iat.transacted_at.min,
          transacted_at: iat.transacted_at,
          status: iat.status,
          meta: meta
        }
      end
    end

    def build_equity_account_transactions!
      EquityAccountTransaction.all.each do |eat|
        meta  = {
          particular: eat.particular,
          is_adjustment: eat.is_adjustment,
          for_resignation: eat.for_resignation,
          beginning_balance: eat.beginning_balance,
          ending_balance: eat.ending_balance,
          accounting_entry_reference_number: eat.voucher_reference_number
        }

        @data <<  {
          uuid: eat.uuid,
          amount: eat.amount,
          member_account_uuid: eat.equity_account.uuid,
          transaction_type: eat.transaction_type,
          transaction_date: eat.transaction_date,
          transaction_hour: eat.transacted_at.hour,
          transaction_minute: eat.transacted_at.min,
          transacted_at: eat.transacted_at,
          status: eat.status,
          meta: meta
        }
      end
    end
  end
end
