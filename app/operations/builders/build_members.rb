module Builders
  class BuildMembers
    def initialize
      @data     = []
      @members  = Member.all
    end

    def execute!
      @members.each do |member|
        meta  = {
          processed_by: member.processed_by,
          approved_by: member.approved_by,
          old_certificate_number: member.old_certificate_number,
          certificates: build_member_shares(member),
          resignation_records: build_resignation_records(member),
          old_meta: member.meta,
          previous_mfi_member_since: member.previous_mfi_member_since,
          previous_mii_member_since: member.previous_mii_member_since
        }

        @data << {
          uuid: member.uuid,
          identification_number: member.identification_number,
          first_name: member.first_name,
          middle_name: member.middle_name,
          last_name: member.last_name,
          gender: member.gender,
          date_of_birth: member.date_of_birth.to_s,
          civil_status: member.civil_status,
          spouse_date_of_birth: member.spouse_date_of_birth,
          home_number: member.home_number,
          address_street: member.address_street,
          address_barangay: member.address_barangay,
          address_city: member.address_city,
          mobile_number: member.mobile_number,
          place_of_birth: member.place_of_birth,
          sss_number: member.sss_number,
          spouse_first_name: member.spouse_first_name,
          spouse_middle_name: member.spouse_middle_name,
          spouse_last_name: member.spouse_last_name,
          status: member.status,
          member_type: member.member_type,
          spouse_occupation: member.spouse_occupation,
          insurance_status: member.insurance_status,
          previous_mfi_member_since: member.previous_mfi_member_since,
          previous_mii_member_since: member.previous_mii_member_since,
          pag_ibig_number: member.pag_ibig_number,
          phil_health_number: member.phil_health_number,
          type_of_housing: member.type_of_housing,
          num_children: member.num_children,
          tin_number: member.tin_number,
          religion: member.religion.try(:name),
          proof_of_housing: member.proof_of_housing,
          reason_for_joining: member.reason_for_joining,
          is_experienced_with_microfinance: member.is_experienced_with_microfinance,
          experience_with_microfinance: member.experience_with_microfinance,
          num_months_business: member.num_months_business,
          old_previous_mii_member_since: member.old_previous_mii_member_since,
          is_reinstate: member.is_reinstate,
          branch_id: member.branch_id,
          center_code: member.center.code,
          center_name: member.center.name,
          center_uuid: member.center.uuid,
          bank: member.bank,
          bank_account_type: member.bank_account_type,
          meta: meta,
          beneficiaries: build_beneficiaries(member),
          legal_dependents: build_legal_dependents(member)
        }
      end

      @data
    end

    def build_legal_dependents(member)
      legal_dependents  = []

      member.legal_dependents.each do |ld|
        legal_dependents << {
          first_name: ld.first_name,
          middle_name: ld.middle_name,
          last_name: ld.last_name,
          date_of_birth: ld.date_of_birth.to_s,
          uuid: ld.uuid,
          educational_attainment: ld.educational_attainment,
          course: ld.course,
          is_deceased: ld.is_deceased,
          relationship: ld.relationship,
          is_tpd: ld.is_tpd
        }
      end

      legal_dependents
    end

    def build_beneficiaries(member)
      beneficiaries = []

      member.beneficiaries.each do |b|
        beneficiaries << {
          first_name: b.first_name,
          middle_name: b.middle_name,
          last_name: b.last_name,
          relationship: b.relationship,
          date_of_birth: b.date_of_birth.to_s,
          is_primary: b.is_primary,
          uuid: b.uuid,
          beneficiary_for: b.beneficiary_for,
          is_deceased: b.is_deceased
        }
      end

      beneficiaries
    end

    def build_resignation_records(member)
      records = []

      records << {
        date_resigned: member.date_resigned,
        type: member.resignation_type,
        code: member.resignation_code,
        reason: member.resignation_reason,
        accounting_reference_number: member.resignation_accounting_reference_number,
        category: "MFI"
      }

      if member.insurance_date_resigned
        records << {
          date_resigned: member.insurance_date_resigned,
          category: "MII"
        }
      end

      records
    end

    def build_member_shares(member)
      records = []

      member.member_shares.each do |member_share|
        records << {
          uuid: member_share.uuid,
          certificate_number: member_share.certificate_number,
          date_of_issue: member_share.date_of_issue,
          number_of_shares: member_share.number_of_shares,
          no_original_shares: member_share.no_original_shares,
          no_of_shares_transferred: member_share.no_of_shares_transferred,
          no_original_certificate: member_share.no_original_certificate,
          status: member_share.status,
          printed: member_share.printed,
          date_printed: member_share.date_printed
        }
      end

      records
    end
  end
end
