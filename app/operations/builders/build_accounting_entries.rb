module Builders
  class BuildAccountingEntries
    def initialize
      @data   = []
      @accounting_entries  = Voucher.approved.order("date_prepared ASC")
    end

    def execute!
      @accounting_entries.each do |accounting_entry|
        journal_entries = accounting_entry.journal_entries

        j_entries = process_journal_entries(journal_entries)

        meta = {
          or_number: accounting_entry.or_number,
          name_of_receiver: accounting_entry.name_of_receiver,
          date_of_check: accounting_entry.date_of_check.try(:to_s),
          check_number: accounting_entry.check_number,
          check_voucher_number: accounting_entry.check_voucher_number,
          payee: accounting_entry.payee,
          accounting_fund_id: accounting_entry.accounting_fund_id
        }

        @data << {
          uuid: accounting_entry.uuid,
          reference_number: accounting_entry.reference_number,
          date_prepared: accounting_entry.date_prepared.try(:to_s),
          date_approved: accounting_entry.date_prepared.try(:to_s),
          date_posted: accounting_entry.date_posted.try(:to_s),
          book: accounting_entry.book,
          branch_id: accounting_entry.branch.id,
          is_year_end_closing: accounting_entry.is_year_end_closing,
          sub_reference_number: accounting_entry.sub_reference_number,
          approved_by: accounting_entry.approved_by,
          prepared_by: accounting_entry.prepared_by,
          particular: accounting_entry.particular,
          status: accounting_entry.status,
          meta: meta,
          journal_entries: j_entries[:journal_entries],
          total_debit: j_entries[:total_debit],
          total_credit: j_entries[:total_credit]
        }
      end

      @data
    end

    private

    def process_journal_entries(journal_entries)
      result        = []
      total_debit   = 0.00
      total_credit  = 0.00

      journal_entries.each do |journal_entry|
        result << {
          uuid: journal_entry.uuid,
          accounting_code_id: journal_entry.accounting_code_id,
          amount: journal_entry.amount,
          post_type: journal_entry.post_type,
          accounting_entry_uuid: journal_entry.voucher.uuid
        }

        if journal_entry.post_type == "DR"
          total_debit += journal_entry.amount
        end

        if journal_entry.post_type == "CR"
          total_credit += journal_entry.amount
        end
      end

      {
        journal_entries: result,
        total_debit: total_debit,
        total_credit: total_credit
      }
    end

    def build_journal_entries(accounting_entry)
      journal_entries = []

      accounting_entry.journal_entries.each do |journal_entry|
        journal_entries << {
          uuid: journal_entry.uuid,
          accounting_code_id: journal_entry.accounting_code_id,
          amount: journal_entry.amount,
          post_type: journal_entry.post_type,
          accounting_entry_uuid: accounting_entry.uuid
        }
      end

      journal_entries
    end
  end
end
