module Builders
  class BuildMembershipPayments
    def initialize
      @data = []
    end

    def execute!
      MembershipPayment.where(status: "paid").each do |membership_payment|
        meta = {
          membership_type_name: membership_payment.membership_type.name,
          legacy: JSON.parse(membership_payment.to_json)
        }

        @data << {
          uuid: membership_payment.uuid,
          amount: membership_payment.amount_paid,
          member_identification_number: membership_payment.member.identification_number,
          status: "approved",
          transaction_date: membership_payment.paid_at,
          transacted_at: membership_payment.paid_at.to_time,
          transaction_hour: membership_payment.paid_at.to_time.hour,
          transaction_minute: membership_payment.paid_at.to_time.min,
          meta: meta
        }
      end

      @data
    end
  end
end
