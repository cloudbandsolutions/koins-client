module Builders
  class BuildMemberAccounts
    def initialize
      @members  = Member.all
      @accounts = []
    end

    def execute!
      build_savings_accounts!
      build_insurance_accounts!
      build_equity_accounts!
      @accounts
    end

    private

    def build_savings_accounts!
      @members.all.each do |member|
        member.savings_accounts.each do |savings_account|
          meta  = {
          }

          @accounts << {
            uuid: savings_account.uuid,
            member_uuid: savings_account.member.uuid,
            account_number: savings_account.account_number,
            maintaining_balance: savings_account.maintaining_balance,
            center_code: savings_account.center.code,
            branch_id: savings_account.branch_id,
            account_type: "SAVINGS",
            account_subtype: savings_account.savings_type.code,
            current_balance: savings_account.balance,
            status: savings_account.status,
            meta: meta
          }
        end
      end
    end

    def build_insurance_accounts!
      @members.each do |member|
        member.insurance_accounts.each do |insurance_account|
          meta  = {
          }

          @accounts << {
            uuid: insurance_account.uuid,
            member_uuid: insurance_account.member.uuid,
            account_number: insurance_account.account_number,
            maintaining_balance: 0.00,
            center_code: insurance_account.center.code,
            branch_id: insurance_account.branch_id,
            account_type: "INSURANCE",
            account_subtype: insurance_account.insurance_type.code,
            current_balance: insurance_account.balance,
            status: insurance_account.status,
            meta: meta
          }
        end
      end
    end

    def build_equity_accounts!
      @members.each do |member|
        member.equity_accounts.each do |equity_account|
          meta  = {
          }

          @accounts << {
            uuid: equity_account.uuid,
            member_uuid: equity_account.member.uuid,
            account_number: equity_account.account_number,
            maintaining_balance: 0.00,
            center_code: equity_account.center.code,
            branch_id: equity_account.branch_id,
            account_type: "EQUITY",
            account_subtype: equity_account.equity_type.code,
            current_balance: equity_account.balance,
            status: equity_account.status,
            meta: meta
          }
        end
      end
    end
  end
end
