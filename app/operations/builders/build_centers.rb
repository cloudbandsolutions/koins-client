module Builders
  class BuildCenters
    def initialize
      @data   = []
      @centers  = Center.all
    end

    def execute!
      @centers.each do |center|
        meta = {
        }

        @data << {
          uuid: center.uuid,
          name: center.name,
          code: center.code,
          address: center.address,
          branch_id: center.branch_id,
          meeting_day: center.meeting_day,
          meta: meta
        }
      end

      @data
    end
  end
end
