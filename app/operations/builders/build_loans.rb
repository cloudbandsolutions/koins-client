module Builders
  class BuildLoans
    def initialize
      @loans    = Loan.all
      @records  = []
    end

    def execute!
      @loans.each do |loan|
        @records << {
          uuid: loan.uuid,
          member_identification_number: loan.member.identification_number,
          center_uuid: loan.center.uuid,
          date_prepared: loan.date_prepared,
          principal: loan.amount,
          interest: loan.total_interest,
          amount_released: loan.amount_released,
          annual_interest_rate: (loan.interest_rate / 100.0).to_f,
          payment_type: loan.payment_type,
          loan_product_code: loan.loan_product.code,
          term: loan.term,
          pn_number: loan.pn_number,
          num_installments: loan.num_installments,
          status: loan.status,
          first_date_of_payment: loan.first_date_of_payment,
          processing_fee: loan.processing_fee,
          date_approved: loan.date_approved,
          date_released: loan.date_of_release,
          date_completed: loan.date_completed,
          is_insured: loan.is_insured,
          insured_amount: loan.insured_amount,
          amortization_schedule: build_amortization_schedule(loan),
          meta: build_meta(loan)
        }
      end

      @records
    end

    def build_amortization_schedule(loan)
      amortization_schedule = []

      loan.ammortization_schedule_entries.where("is_void IS NULL").order("due_at ASC").each do |ase|
        amortization_schedule << {
          uuid: ase.uuid,
          amount: ase.amount,
          principal: ase.principal,
          interest: ase.interest,
          due_date: ase.due_at,
          meta: {
            paid_principal: ase.paid_principal,
            paid_interest: ase.paid_interest,
          }
        }
      end

      amortization_schedule
    end

    def build_deductions(loan)
      deductions        = []
      accounting_entry  = ::Loans::ProduceVoucherForLoan.new(
                            loan: loan,
                            user: User.first
                          ).execute!

      amount_released_accounting_code_id  = nil

      if loan.override_installment_interval == true
        amount_released_accounting_code_id = AccountingCode.find(Settings.loans_override_installment_cash_accounting_code_id).id
      else
        if loan.loan_product.use_amount_released_accounting_code == true
          amount_released_accounting_code_id  = loan.loan_product.amount_released_accounting_code.id
        else
          amount_released_accounting_code_id  = loan.bank.accounting_code.id
        end
      end

      accounting_entry.journal_entries.each do |journal_entry|
        if journal_entry.accounting_code.id != amount_released_accounting_code_id
          deductions << {
            accounting_code_id: journal_entry.accounting_code_id,
            name: journal_entry.accounting_code.name,
            amount: journal_entry.amount
          }
        end
      end

      deductions
    end

    def build_meta(loan)
      meta  = {
        accounting_entry_reference_number: loan.voucher_reference_number,
        voucher_date_requested: loan.voucher_date_requested,
        voucher_payee: loan.voucher_payee,
        bank_check_number: loan.bank_check_number,
        project_type: loan.project_type,
        project_type_category: loan.project_type_category,
        maintaining_balance: loan.maintaining_balance_val,
        override_processing_fee: loan.override_processing_fee,
        override_installment_interval: loan.override_installment_interval,
        original_loan_amount: loan.original_loan_amount,
        old_principal_balance: loan.old_principal_balance,
        old_interest_balance: loan.old_interest_balance,
        override_interest: loan.override_interest,
        override_interest_rate: loan.override_interest_rate,
        num_years_business: loan.num_years_business,
        num_months_business: loan.num_months_business,
        num_workers_business: loan.num_workers_business,
        override_insurance_deductions: loan.override_insurance_deductions,
        insurance_as_disbursement_correction: loan.insurance_as_disbursement_correction,
        beneficiary_first_name: loan.beneficiary_first_name,
        beneficiary_middle_name: loan.beneficiary_middle_name,
        beneficiary_last_name: loan.beneficiary_last_name,
        beneficiary_date_of_birth: loan.beneficiary_date_of_birth,
        beneficiary_relationship: loan.beneficiary_relationship,
        bank: loan.bank,
        member_weekly_expenses: loan.member_weekly_expenses,
        member_weekly_incomes: loan.member_weekly_incomes,
        co_maker: loan.co_maker_one,
        particular: loan.voucher_particular,
        book: loan.book,
        deductions: build_deductions(loan)
      }

      meta
    end
  end
end
