module Adjustments
  class ProduceVoucherForTransferAdjustment
    def initialize(transfer_adjustment:, user:)
      @transfer_adjustment  = transfer_adjustment
      @user                   = user
      @book                   = 'JVB'
    end

    def execute!
      if @transfer_adjustment.pending?
        @voucher = Voucher.new(
                    branch: @transfer_adjustment.branch,
                    book: @book,
                    particular: @transfer_adjustment.particular,
                    date_prepared: ApplicationHelper.current_working_date
                  )

        build_entries
      else
        @voucher  = Voucher.approved.where(
                      book: 'JVB', 
                      reference_number: @transfer_adjustment.accounting_entry_reference_number
                    ).first
      end

      @voucher
    end

    def build_entries
      savings_type = SavingsType.where(code: @transfer_adjustment.account_code).first

      if savings_type.present?
        if @transfer_adjustment.add?
          accounting_code = savings_type.deposit_accounting_code
          amount  = @transfer_adjustment.amount
          post_type = 'CR'

          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: post_type,
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries  <<  journal_entry
        end

        if @transfer_adjustment.deduct?
          accounting_code = savings_type.deposit_accounting_code
          amount  = @transfer_adjustment.amount
          post_type = 'DR'

          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: post_type,
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries  <<  journal_entry
        end
      end

      insurance_type = InsuranceType.where(code: @transfer_adjustment.account_code).first
      
      if insurance_type.present?
        if @transfer_adjustment.add?
          accounting_code = insurance_type.deposit_accounting_code
          amount  = @transfer_adjustment.amount
          post_type = 'CR'

          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: post_type,
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries  <<  journal_entry
        end

        if @transfer_adjustment.deduct?
          accounting_code = insurance_type.withdraw_accounting_code
          amount  = @transfer_adjustment.amount
          post_type = 'DR'

          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: post_type,
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries  <<  journal_entry
        end
      end

      equity_type = EquityType.where(code: @transfer_adjustment.account_code).first

      if equity_type.present?
        if @transfer_adjustment.add?
          accounting_code = equity_type.deposit_accounting_code
          amount  = @transfer_adjustment.amount
          post_type = 'CR'

          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: post_type,
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries  <<  journal_entry
        end

        if @transfer_adjustment.deduct?
          accounting_code = equity_type.withdraw_accounting_code
          amount  = @transfer_adjustment.amount
          post_type = 'DR'

          journal_entry = JournalEntry.new(
                            amount: amount,
                            post_type: post_type,
                            accounting_code: accounting_code
                          )

          @voucher.journal_entries  <<  journal_entry
        end
      end

      @transfer_adjustment.transfer_adjustment_records.each do |transfer_adjustment_record|
        amount          = transfer_adjustment_record.amount
        accounting_code = transfer_adjustment_record.accounting_code
        post_type       = @transfer_adjustment.add? ? "DR" : "CR"
        journal_entry = JournalEntry.new(
                          amount: amount,
                          post_type: post_type,
                          accounting_code: accounting_code
                        )

        @voucher.journal_entries  <<  journal_entry
      end
    end
  end
end
