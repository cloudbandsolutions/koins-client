module Adjustments
  class ProduceVoucherForLoanPaymentAdjustment
    def initialize(loan_payment_adjustment:, user:)
      @loan_payment_adjustment  = loan_payment_adjustment
      @user                   = user
      @book                   = 'JVB'
    end

    def execute!
      nil
    end

    private

    def build_debit_entries
    end

    def build_credit_entries
    end
  end
end
