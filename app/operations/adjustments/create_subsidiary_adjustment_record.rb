module Adjustments
  class CreateSubsidiaryAdjustmentRecord
    def initialize(member:, account_code:, adjustment_type:, amount:, accounting_code:, subsidiary_adjustment:)
      @member                 = member
      @account_code           = account_code
      @adjustment_type        = adjustment_type
      @amount                 = amount
      @accounting_code        = accounting_code
      @subsidiary_adjustment  = subsidiary_adjustment

      @subsidiary_adjustment_record = SubsidiaryAdjustmentRecord.new(
                                        member: @member,
                                        account_code: @account_code,
                                        adjustment_type: @adjustment_type,
                                        amount: @amount,
                                        accounting_code: @accounting_code,
                                        subsidiary_adjustment: @subsidiary_adjustment
                                      )
    end

    def execute!
      @subsidiary_adjustment_record.save!
      @subsidiary_adjustment  = SubsidiaryAdjustment.find(@subsidiary_adjustment.id)

      @subsidiary_adjustment
    end
  end
end
