module Adjustments
  class ValidateCreateSubsidiaryAdjustment
    def initialize(branch:, particular:)
      @branch     = branch
      @particular = particular
      @errors     = []
    end

    def execute!
      if !@branch
        @errors << "Branch required"
      end

      if !@particular
        @errors << "Particular required"
      end

      @errors
    end
  end
end
