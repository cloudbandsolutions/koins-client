module Adjustments
  class ValidateApproveSubsidiaryAdjustment
    def initialize(subsidiary_adjustment:, user:)
      @subsidiary_adjustment  = subsidiary_adjustment
      @user                   = user
      @errors                 = []
    end

    def execute!
      validate_accounting_entries
      validate_adjustment_records
      @errors
    end

    def validate_adjustment_records
      @subsidiary_adjustment.subsidiary_adjustment_records.each do |r|
        account_code    = r.account_code
        member          = r.member
        adjustment_type = r.adjustment_type
        amount          = r.amount
        accounting_code = r.accounting_code

        # Check Savings
        savings_account = SavingsAccount.joins(:savings_type).where("savings_types.code = ? AND savings_accounts.member_id = ?", account_code, member.id).first

        if savings_account and adjustment_type == "debit"
          if (savings_account.balance - amount) < savings_account.maintaining_balance
            @errors << "Cannot adjust debit for #{member.full_name} #{account_code} account. Below maintaining balance of #{savings_account.maintaining_balance}"
          end
        end

        # Check Insurance
        insurance_account = InsuranceAccount.joins(:insurance_type).where("insurance_types.code = ? AND insurance_accounts.member_id = ?", account_code, member.id).first

        if insurance_account and adjustment_type == "debit"
          if (insurance_account.balance - amount) < 0
            @errors << "Cannot adjust debit for #{member.full_name} #{account_code} account. Below 0"
          end
        end

        # Check Equity
        equity_account = EquityAccount.joins(:equity_type).where("equity_types.code = ? AND equity_accounts.member_id = ?", account_code, member.id).first

        if equity_account and adjustment_type == "debit"
          if (equity_account.balance - amount) < 0
            @errors << "Cannot adjust debit for #{member.full_name} #{account_code} account. Below 0"
          end
        end

        # No accounts were found with corresponding code
        if savings_account.nil? and insurance_account.nil? and equity_account.nil?
          @errors << "No account found for member #{member.to_s} with code #{account_code}"
        end
      end
    end

    def validate_accounting_entries
      @voucher = Adjustments::ProduceVoucherForSubsidiaryAdjustment.new(
                  subsidiary_adjustment: @subsidiary_adjustment, 
                  user: @user).execute!

      if !@voucher.valid?
        @errors << "Accounting entry is invalid" 
      end
    end
  end
end
