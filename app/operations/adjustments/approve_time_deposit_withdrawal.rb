module Adjustments
  class ApproveTimeDepositWithdrawal
    def initialize(time_deposit_id:, user:)
      @time_deposit_withdrawal =  time_deposit_id
    
      @user = user

      
    
      @voucher = ::Adjustments::ProduceVoucherForTimeDepositWithdrwal.new(time_deposit_withdrawal: time_deposit_id, user: @user).execute!



          #@voucher = ::PaymentCollections::GenerateAccountingEntryForTimeDepositCollection.new(payment_collection_id: id).execute!
      @c_working_date     = ApplicationHelper.current_working_date
    end
    def execute!
      
      @voucher.save!
      @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
      
      @time_deposit_withdrawal.update!(
          status: "approved",
          refference_number: @voucher.reference_number,
          posting_date: @voucher.date_posted
      )

      if @time_deposit_withdrawal.total_interest.to_i > 0.to_i 
        time_deposit_interest = SavingsAccountTransaction.create!(
                                                                  savings_account_id: @time_deposit_withdrawal.savings_accounts_id,
                                                                  amount: @time_deposit_withdrawal.total_interest,
                                                                  transaction_type: "interest",
                                                                  transacted_at: @c_working_date,
                                                                  particular: "test",
                                                                  status: "approved",
                                                                  transaction_date: @c_working_date


      
                                                                  )

      
        ::Savings::RehashAccount.new(savings_account: SavingsAccount.find(@time_deposit_withdrawal.savings_accounts_id)).execute!
      end


      time_deposit_withdraw = SavingsAccountTransaction.create!(
                                                                  savings_account_id: @time_deposit_withdrawal.savings_accounts_id,
                                                                  amount: @time_deposit_withdrawal.total_withdrawal_amount,
                                                                  transaction_type: "withdraw",
                                                                  transacted_at: @c_working_date,
                                                                  particular: "test",
                                                                  status: "approved",
                                                                  transaction_date: @c_working_date


      
                                                                  )
      ::Savings::RehashAccount.new(savings_account: SavingsAccount.find(@time_deposit_withdrawal.savings_accounts_id)).execute!

      
      timeDepositAccount = SavingsAccount.find(@time_deposit_withdrawal.savings_accounts_id).lock_in_amount
      timeDepositLockIn = SavingsAccountTransaction.find(@time_deposit_withdrawal.savings_transactions).ending_balance
      deductLockin = timeDepositAccount - timeDepositLockIn

      a =  SavingsAccount.find(@time_deposit_withdrawal.savings_accounts_id).update(lock_in_amount: deductLockin)
      savings_account_trans = SavingsAccountTransaction.find(@time_deposit_withdrawal.savings_transactions).for_lock_in
      b = DepositTimeTransaction.find(savings_account_trans).update(status: "withdraw")         
    end
  end
end
