module Adjustments
  class CreateNewSubsidiaryAdjustment
    def initialize(user:, branch:, particular:)
      @user       = user
      @branch     = branch
      @particular = particular
      @record     = SubsidiaryAdjustment.new(
                      prepared_by: @user.full_name,
                      branch: @branch,
                      particular: @particular
                    )
    end

    def execute!
      @record.save!

      @record
    end
  end
end
