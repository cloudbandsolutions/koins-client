module Adjustments 
  class ProduceVoucherForTimeDepositWithdrwal
    def initialize(time_deposit_withdrawal:, user:)
      
      @time_deposit_withdrawal = TimeDepositWithdrawal.find(time_deposit_withdrawal.id)
      @user = user
      @book = 'JVB'
      @particular = "test"
      @branch = Branch.find(Settings.branch_ids.last)
    end
    def execute!

        
          if @time_deposit_withdrawal.status == "pending"
            @voucher = Voucher.new(
                    branch: @branch,
                    book: @book,
                    particular: @particular,
                    date_prepared: ApplicationHelper.current_working_date
                  )

          end
        
        build_debit_entries
        build_debit_entries_withdraw
        build_credit_entries
        build_credit_entries_withdraw
      

      @voucher
    end
   
#----------------------- deposit interest    
    def build_debit_entries
      accounting_code_debit = AccountingCode.find(301)
      debit_amount = @time_deposit_withdrawal.total_interest
      journal_entry = JournalEntry.new(
                        amount: debit_amount,
                        post_type: "DR",
                        accounting_code: accounting_code_debit
                      )
      @voucher.journal_entries << journal_entry
    end

    def build_credit_entries

          accounting_code_debit = AccountingCode.find(1918)
          credit_amount = @time_deposit_withdrawal.total_interest

          journal_entry = JournalEntry.new(
                          amount: credit_amount,
                          post_type: "CR",
                          accounting_code: accounting_code_debit
                        )

          @voucher.journal_entries << journal_entry
      
    end
 #---------------------- end deposit interest

    def build_credit_entries_withdraw
      accounting_code_debit = AccountingCode.find(@branch.bank.accounting_code_id)
      credit_amount = @time_deposit_withdrawal.total_withdrawal_amount
      journal_entry = JournalEntry.new(
                        amount: credit_amount,
                        post_type: "CR",
                        accounting_code: accounting_code_debit
                      )
      @voucher.journal_entries << journal_entry
    end

    def build_debit_entries_withdraw

      accounting_code_debit = AccountingCode.find(1918)
      debit_amount = @time_deposit_withdrawal.total_withdrawal_amount
      journal_entry = JournalEntry.new(
                        amount: debit_amount,
                        post_type: "DR",
                        accounting_code: accounting_code_debit
                      )
      @voucher.journal_entries << journal_entry
      
    end

  end
end
