module Adjustments
  class ValidateApproveTransferAdjustment
    def initialize(transfer_adjustment:, user:)
      @transfer_adjustment  = transfer_adjustment
      @user                 = user
      @errors               = []
    end

    def execute!
      validate_account
      @errors
    end

    def validate_account
      account_code      = @transfer_adjustment.account_code
      member            = @transfer_adjustment.member
      amount            = @transfer_adjustment.amount
      transaction_type  = @transfer_adjustment.transaction_type

      savings_account = SavingsAccount.joins(:savings_type).where("savings_types.code = ? AND savings_accounts.member_id = ?", account_code, member.id).first

      if savings_account.present?
        if @transfer_adjustment.deduct?
          net = savings_account.balance - amount
          
          if net < savings_account.maintaining_balance
            @errors << "Cannot withdraw #{amount.to_f} from savings #{account_code} with maintaining balance of #{savings_account.maintaining_balance}"
          end
  
          if net < 0
            @errors << "Cannot withdraw #{amount.to_f} from savings #{account_code} with balance #{savings_account.balance}"
          end
        end
      end

      insurance_account = InsuranceAccount.joins(:insurance_type).where("insurance_types.code = ? AND insurance_accounts.member_id = ?", account_code, member.id).first

      if insurance_account.present?
        if @transfer_adjustment.deduct?
          net = insurance_account.balance - amount

          if net < 0
            @errors << "Cannot withdraw #{amount.to_f} from insurance #{account_code} with balance #{insurance_account.balance}"
          end
        end
      end

      equity_account = EquityAccount.joins(:equity_type).where("equity_types.code = ? AND equity_accounts.member_id = ?", account_code, member.id).first

      if equity_account.present?
        if @transfer_adjustment.deduct?
          net = equity_account.balance - amount

          if net < 0
            @errors << "Cannot withdraw #{amount.to_f} from equity #{account_code} with balance #{equity_account.balance}"
          end
        end
      end
    end
  end
end
