module Adjustments
  class CreateTimeDepositInterest
    def initialize(amount:, current_date:)
      @amount = amount
      @number_of_days = 78
      @interest_per_month = 0.02
    end
    def execute!
      comp = (@interest_per_month/12) * @number_of_days / 30
      amount = @amount
      formula_data  = comp * amount

      raise formula_data.round(2).inspect
    end
  end
end
