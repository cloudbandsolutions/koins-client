module Adjustments
  class WithdrawalTimeDeposit
    def initialize(savings_account_transaction:, withdraw_date:)
      @member_id = savings_account_transaction.savings_account.member.id
      @savings_account             = savings_account_transaction.savings_account_id
      @savings_account_transaction_id = savings_account_transaction.id
      
      @savings_amount              = savings_account_transaction.amount.to_f
      
      @savings_transacted_at       = savings_account_transaction.transacted_at
      

      @current_date = ApplicationHelper.current_working_date

      @c_working_date     = withdraw_date.to_date
      @end_of_today_month  = @c_working_date.end_of_month.to_date
      
      

      if @c_working_date == @end_of_today_month

        @previous_month = @c_working_date.to_date
      else
        @previous_month = (@c_working_date.end_of_month.to_date - 1.month).to_date
      end

      #raise @previous_month.inspect
      
      
      
      #@previous_month = ("2019-07-31").to_date

      
      #@dep_trans = DepositTimeTransaction.where(savings_account_id: @savings_account, status: "lock-in").ids
      @dep_trans = DepositTimeTransaction.find(savings_account_transaction.for_lock_in).id
      

    end
    def execute!

      active_loan = Loan.where(member_id: @member_id, status: "active")
    

      #@interest_per_month =  active_loan.size == 0 ? (0.01) : (0.02)
      @interest_per_month =  0.02
      


      @used_end_date = @c_working_date == @end_of_today_month ? (@c_working_date) : (@previous_month)
      
       

      @number_of_days =  (@used_end_date.to_date - @savings_transacted_at.to_date).to_i
      #raise @number_of_days.inspect
      
      comp = (@interest_per_month/12) * @number_of_days / 30
      amount = @savings_amount


      formula_data_details   = (comp * amount).round(2)
      
      if formula_data_details <= 0 
        formula_data  = 0
      else
        formula_data = formula_data_details
      end

      total_withdrawable = @savings_amount + formula_data
     #raise total_withdrawable.inspect 
    
      time_deposit_withdrawal = TimeDepositWithdrawal.create!(
                                                              total_interest: formula_data, 
                                                              savings_transactions: @savings_account_transaction_id, 
                                                              savings_accounts_id: @savings_account,
                                                              total_withdrawal_amount: total_withdrawable,
                                                              deposit_time_transaction_id: @dep_trans
                                                            )
      
      @data = time_deposit_withdrawal


    end
  end
end
