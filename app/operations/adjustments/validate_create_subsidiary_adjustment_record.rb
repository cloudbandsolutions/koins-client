module Adjustments
  class ValidateCreateSubsidiaryAdjustmentRecord
    def initialize(member:, account_code:, adjustment_type:, amount:, accounting_code:, subsidiary_adjustment:)
      @member                 = member
      @account_code           = account_code
      @adjustment_type        = adjustment_type
      @amount                 = amount
      @accounting_code        = accounting_code
      @subsidiary_adjustment  = subsidiary_adjustment
      @errors                 = []

      @subsidiary_adjustment_record = SubsidiaryAdjustmentRecord.new(
                                        member: @member,
                                        account_code: @account_code,
                                        adjustment_type: @adjustment_type,
                                        amount: @amount,
                                        accounting_code: @accounting_code,
                                        subsidiary_adjustment: @subsidiary_adjustment
                                      )
    end

    def execute!
      # Check amount
      if !@amount
        @errors << "Amount required"
      elsif @amount <= 0
        @errors << "Amount should have positive value"
      end

      # # Check if member is already in adjustment record
      # if !@member
      #   @errors << "Member not defined"
      # elsif @subsidiary_adjustment.subsidiary_adjustment_records.where(member_id: @member.id).count > 0
      #   @errors << "Member already in adjustment"
      # end

      if !@subsidiary_adjustment.valid?
        @subsidiary_adjustment.errors.messages.each do |m|
          @errors << m
        end
      end

      @errors
    end
  end
end
