module Adjustments
  class ApproveBatchMoratorium
    def initialize(batch_moratorium:, user:)
      @batch_moratorium = batch_moratorium
      @user             = user
      @active_loans     = Loan.active

      if @batch_moratorium.center.present?
        @active_loans = @active_loans.where(center_id: @batch_moratorium.center.try(:id))
      end

      @amortization_schedules = []
      @active_loans.each do |active_loan|
        ase = active_loan.ammortization_schedule_entries.unpaid.order(
                "due_at asc").where(
                  "due_at > ?", @batch_moratorium.date_initialized
                ).first
                
        if ase.present?
          @amortization_schedules << ase
        end
      end
    end

    def execute!
      @amortization_schedules.each do |as|
        loan_moratorium = LoanMoratorium.new(
                            ammortization_schedule_entry: as,
                            previous_due_at:  as.due_at,
                            new_due_at: as.due_at + @batch_moratorium.number_of_days.days,
                            reason: @batch_moratorium.reason
                          )

        if loan_moratorium.save
          Moratorium::Reamortize.new(
            loan_moratorium: loan_moratorium,
            num_days: @batch_moratorium.number_of_days
          ).execute!
        else
          raise "Error in saving loan moratorium: #{loan_moratorium.errors}"
        end
      end

      @batch_moratorium.approve!(@user.full_name)

      @batch_moratorium
    end
  end
end
