module Adjustments
  class ApproveTransferAdjustment
    def initialize(transfer_adjustment:, user:)
      @transfer_adjustment  = transfer_adjustment
      @user                   = user
      @c_date                 = ApplicationHelper.current_working_date
      @voucher                = Adjustments::ProduceVoucherForTransferAdjustment.new(transfer_adjustment: @transfer_adjustment, user: @user).execute!
      @approved_by            = user.full_name
    end

    def execute!
      if @transfer_adjustment.pending?
        @voucher.save!
        @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
        @transfer_adjustment.update!(
          status: "approved",
          accounting_entry_reference_number: @voucher.reference_number,
          date_approved: @c_date,
          approved_by: @user.full_name,
        )

        post_transactions!
      end

      @transfer_adjustment
    end

    def post_transactions!
      r                 = @transfer_adjustment
      account_code      = r.account_code
      member            = r.member
      transaction_type  = r.transaction_type
      amount            = r.amount

      # Post Savings
      savings_account = SavingsAccount.joins(:savings_type).where("savings_types.code = ? AND savings_accounts.member_id = ?", account_code, member.id).first
      if savings_account and @transfer_adjustment.deduct?
        savings_account_transaction = SavingsAccountTransaction.create!(
                                        amount: amount,
                                        transacted_at: @c_date,
                                        created_at: @c_date,
                                        transaction_type: "withdraw",
                                        particular: @transfer_adjustment.particular,
                                        voucher_reference_number: @transfer_adjustment.voucher_reference_number,
                                        savings_account: savings_account,
                                        is_adjustment: true
                                      )

        savings_account_transaction.approve!(@approved_by)
      elsif savings_account and @transfer_adjustment.add?
        savings_account_transaction = SavingsAccountTransaction.create!(
                                        amount: amount,
                                        transacted_at: @c_date,
                                        created_at: @c_date,
                                        transaction_type: "deposit",
                                        particular: @transfer_adjustment.particular,
                                        voucher_reference_number: @transfer_adjustment.voucher_reference_number,
                                        savings_account: savings_account,
                                        is_adjustment: true
                                      )

        savings_account_transaction.approve!(@approved_by)
      end

      # Post Insurance
      insurance_account = InsuranceAccount.joins(:insurance_type).where("insurance_types.code = ? AND insurance_accounts.member_id = ?", account_code, member.id).first
      if insurance_account and @transfer_adjustment.deduct?
        if insurance_account.insurance_type_id == 1
          equity_amount = amount.to_f / 2
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "withdraw",
                                            particular: @transfer_adjustment.particular,
                                            voucher_reference_number: @transfer_adjustment.voucher_reference_number,
                                            insurance_account: insurance_account,
                                            equity_value: insurance_account.equity_value - equity_amount,
                                            is_adjustment: true
                                          )

          insurance_account_transaction.approve!(@approved_by)
          insurance_account.update!(equity_value: insurance_account_transaction.ending_balance.to_f / 2)
        else
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "withdraw",
                                            particular: @transfer_adjustment.particular,
                                            voucher_reference_number: @transfer_adjustment.voucher_reference_number,
                                            insurance_account: insurance_account,
                                            is_adjustment: true
                                          )

          insurance_account_transaction.approve!(@approved_by)
        end
      elsif insurance_account and @transfer_adjustment.add?
        if insurance_account.insurance_type_id == 1
          equity_amount = amount.to_f / 2
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "deposit",
                                            particular: @transfer_adjustment.particular,
                                            voucher_reference_number: @transfer_adjustment.voucher_reference_number,
                                            insurance_account: insurance_account,
                                            equity_value: equity_amount + insurance_account.equity_value,
                                            is_adjustment: true
                                          )

          insurance_account_transaction.approve!(@approved_by)
          insurance_account.update!(equity_value: insurance_account_transaction.ending_balance.to_f / 2)
        else
          insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "deposit",
                                            particular: @transfer_adjustment.particular,
                                            voucher_reference_number: @transfer_adjustment.voucher_reference_number,
                                            insurance_account: insurance_account,
                                            is_adjustment: true
                                          )

          insurance_account_transaction.approve!(@approved_by)
        end
      end

      # Post Equity
      equity_account = EquityAccount.joins(:equity_type).where("equity_types.code = ? AND equity_accounts.member_id = ?", account_code, member.id).first
      if equity_account and @transfer_adjustment.deduct?
        equity_account_transaction = EquityAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @c_date,
                                          created_at: @c_date,
                                          transaction_type: "withdraw",
                                          particular: @transfer_adjustment.particular,
                                          voucher_reference_number: @transfer_adjustment.voucher_reference_number,
                                          equity_account: equity_account,
                                          is_adjustment: true
                                        )

        equity_account_transaction.approve!(@approved_by)
      elsif equity_account and @transfer_adjustment.add?
        equity_account_transaction = EquityAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @c_date,
                                          created_at: @c_date,
                                          transaction_type: "deposit",
                                          particular: @transfer_adjustment.particular,
                                          voucher_reference_number: @transfer_adjustment.voucher_reference_number,
                                          equity_account: equity_account,
                                          is_adjustment: true
                                        )

        equity_account_transaction.approve!(@approved_by)
      end
    end
  end
end
