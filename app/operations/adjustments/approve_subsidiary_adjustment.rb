module Adjustments
  class ApproveSubsidiaryAdjustment
    def initialize(subsidiary_adjustment:, user:)
      @subsidiary_adjustment  = subsidiary_adjustment
      @user                   = user
      @c_date                 = ApplicationHelper.current_working_date
      @voucher                = Adjustments::ProduceVoucherForSubsidiaryAdjustment.new(subsidiary_adjustment: @subsidiary_adjustment, user: @user).execute!
      @approved_by            = user.full_name
    end

    def execute!
      if @subsidiary_adjustment.pending?
        @voucher.save!
        @voucher = Accounting::ApproveVoucher.new(voucher: @voucher, user: @user).execute!
        @subsidiary_adjustment.update!(
          status: "approved",
          voucher_reference_number: @voucher.reference_number,
          date_approved: @c_date,
          approved_by: @user.full_name,
        )

        post_transactions!
      end

      @subsidiary_adjustment
    end

    def post_transactions!
      @subsidiary_adjustment.subsidiary_adjustment_records.each do |r|
        account_code    = r.account_code
        member          = r.member
        adjustment_type = r.adjustment_type
        amount          = r.amount

        # Post Savings
        savings_account = SavingsAccount.joins(:savings_type).where("savings_types.code = ? AND savings_accounts.member_id = ?", account_code, member.id).first
        if savings_account and adjustment_type == "debit"
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @c_date,
                                          created_at: @c_date,
                                          transaction_type: "withdraw",
                                          particular: @subsidiary_adjustment.particular,
                                          voucher_reference_number: @subsidiary_adjustment.voucher_reference_number,
                                          savings_account: savings_account,
                                          is_adjustment: true
                                        )

          savings_account_transaction.approve!(@approved_by)
        elsif savings_account and adjustment_type == "credit"
          savings_account_transaction = SavingsAccountTransaction.create!(
                                          amount: amount,
                                          transacted_at: @c_date,
                                          created_at: @c_date,
                                          transaction_type: "deposit",
                                          particular: @subsidiary_adjustment.particular,
                                          voucher_reference_number: @subsidiary_adjustment.voucher_reference_number,
                                          savings_account: savings_account,
                                          is_adjustment: true
                                        )

          savings_account_transaction.approve!(@approved_by)
        end

        # Post Insurance
        insurance_account = InsuranceAccount.joins(:insurance_type).where("insurance_types.code = ? AND insurance_accounts.member_id = ?", account_code, member.id).first
        if insurance_account and adjustment_type == "debit"
          if insurance_account.insurance_type_id == 1
            equity_amount = amount.to_f / 2
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "withdraw",
                                            particular: @subsidiary_adjustment.particular,
                                            voucher_reference_number: @subsidiary_adjustment.voucher_reference_number,
                                            insurance_account: insurance_account,
                                            equity_value: insurance_account.equity_value - equity_amount,
                                            is_adjustment: true
                                          )

            insurance_account_transaction.approve!(@approved_by)
            insurance_account.update!(equity_value: insurance_account_transaction.ending_balance.to_f / 2)
          else
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "withdraw",
                                            particular: @subsidiary_adjustment.particular,
                                            voucher_reference_number: @subsidiary_adjustment.voucher_reference_number,
                                            insurance_account: insurance_account,
                                            is_adjustment: true
                                          )

            insurance_account_transaction.approve!(@approved_by)
          end
        elsif insurance_account and adjustment_type == "credit"
          if insurance_account.insurance_type_id == 1
            equity_amount = amount.to_f / 2
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "deposit",
                                            particular: @subsidiary_adjustment.particular,
                                            voucher_reference_number: @subsidiary_adjustment.voucher_reference_number,
                                            insurance_account: insurance_account,
                                            equity_value: equity_amount + insurance_account.equity_value,
                                            is_adjustment: true
                                          )

            insurance_account_transaction.approve!(@approved_by)
            insurance_account.update!(equity_value: insurance_account_transaction.ending_balance.to_f / 2)
          else
            insurance_account_transaction = InsuranceAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "deposit",
                                            particular: @subsidiary_adjustment.particular,
                                            voucher_reference_number: @subsidiary_adjustment.voucher_reference_number,
                                            insurance_account: insurance_account,
                                            is_adjustment: true
                                          )

            insurance_account_transaction.approve!(@approved_by)
          end
        end

        # Post Equity
        equity_account = EquityAccount.joins(:equity_type).where("equity_types.code = ? AND equity_accounts.member_id = ?", account_code, member.id).first
        if equity_account and adjustment_type == "debit"
          equity_account_transaction = EquityAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "withdraw",
                                            particular: @subsidiary_adjustment.particular,
                                            voucher_reference_number: @subsidiary_adjustment.voucher_reference_number,
                                            equity_account: equity_account,
                                            is_adjustment: true
                                          )

          equity_account_transaction.approve!(@approved_by)
        elsif equity_account and adjustment_type == "credit"
          equity_account_transaction = EquityAccountTransaction.create!(
                                            amount: amount,
                                            transacted_at: @c_date,
                                            created_at: @c_date,
                                            transaction_type: "deposit",
                                            particular: @subsidiary_adjustment.particular,
                                            voucher_reference_number: @subsidiary_adjustment.voucher_reference_number,
                                            equity_account: equity_account,
                                            is_adjustment: true
                                          )

          equity_account_transaction.approve!(@approved_by)
        end
      end
    end
  end
end
