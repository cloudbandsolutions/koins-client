module Adjustments
  class ProduceVoucherForSubsidiaryAdjustment
    def initialize(subsidiary_adjustment:, user:)
      @subsidiary_adjustment  = subsidiary_adjustment
      @user                   = user
      @book                   = 'JVB'
    end

    def execute!
      if @subsidiary_adjustment.pending?
        @voucher = Voucher.new(
                    branch: @subsidiary_adjustment.branch,
                    book: @book,
                    accounting_fund: @subsidiary_adjustment.accounting_fund,
                    particular: @subsidiary_adjustment.particular,
                    date_prepared: ApplicationHelper.current_working_date
                  )

        build_debit_entries
        build_credit_entries
      else
        if Settings.activate_microloans
          @voucher = Voucher.approved.where(book: 'JVB', reference_number: @subsidiary_adjustment.voucher_reference_number).first
        else
          @voucher = Voucher.approved.where(book: 'JVB', particular: @subsidiary_adjustment.particular).first
        end
      end

      @voucher
    end

    def build_debit_entries
      @subsidiary_adjustment.subsidiary_adjustment_records.where(adjustment_type: 'debit').each do |entry|
        journal_entry = JournalEntry.new(
                          amount: entry.amount,
                          post_type: "DR",
                          accounting_code: entry.accounting_code
                        )

        @voucher.journal_entries << journal_entry
      end
    end

    def build_credit_entries
      @subsidiary_adjustment.subsidiary_adjustment_records.where(adjustment_type: 'credit').each do |entry|
        journal_entry = JournalEntry.new(
                          amount: entry.amount,
                          post_type: "CR",
                          accounting_code: entry.accounting_code
                        )

        @voucher.journal_entries << journal_entry
      end
    end
  end
end
