module Adjustments
  class AdjustEquityAccountTransactionsByMonth
    def initialize(in_month:,cor_date:)
      @in_month   = in_month
      @cor_date   = cor_date
      @trans_ids  = EquityAccountTransaction.where("extract(month from transacted_at) = #{@in_month}").pluck(:id)
    end

    def execute!
      @trans_ids.each do |trans_id|
        query="update equity_account_transactions set transacted_at='#{@cor_date}', transaction_date = '#{@cor_date}' where id =#{trans_id}"
        ActiveRecord::Base.connection.execute(query)
      end
    end
  end
end

