module Equity
  class RehashAccount
    def initialize(equity_account:)
      @equity_account              = equity_account
      @equity_account_transactions = EquityAccountTransaction.where(
                                          "equity_account_id = ? AND amount > 0 AND status IN (?)", 
                                          @equity_account.id, ["approved", "reversed"]
                                        ).order("id ASC")
    end

    def execute!
      @equity_account.update!(balance: 0.00)
      @equity_account_transactions.each do |sat|
        sat.update!(
          updated_at: sat.transacted_at,
          beginning_balance: 0.00,
          ending_balance: 0.00,
          status: "pending"
        )
      end

      @equity_account_transactions.each do |sat|
        sat.beginning_balance = EquityAccount.find(@equity_account.id).balance
        sat.approve!("")
      end

      @equity_account
    end
  end
end
