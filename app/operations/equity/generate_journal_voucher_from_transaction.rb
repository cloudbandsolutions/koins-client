module Equity
  class GenerateJournalVoucherFromTransaction
    def initialize(equity_transaction:, prepared_by:, approved_by:)
    	@equity_transaction = equity_transaction
      @prepared_by = prepared_by
      @approved_by = approved_by
    end

    def execute!
      voucher = Voucher.new(
          particular: @equity_transaction.particular,
          reference_number: @equity_transaction.voucher_reference_number,
          status: 'approved',
          book: @equity_transaction.transaction_type == "withdraw" ? 'JVB' : 'CRB',
          branch_id: @equity_transaction.bank.id,
          date_prepared: @equity_transaction.transacted_at,
          prepared_by: @prepared_by,
          approved_by: @approved_by
        )

        if @equity_transaction.transaction_type == "withdraw"
          je_debit = JournalEntry.new(
                  amount: @equity_transaction.amount,
                  post_type: "DR",
                  accounting_code: @equity_transaction.equity_account.equity_type.withdraw_accounting_code
                )

          voucher.journal_entries << je_debit

          je_credit = JournalEntry.new(
                    amount: @equity_transaction.amount,
                    post_type: "CR",
                    accounting_code: @equity_transaction.accounting_code
                  )

          voucher.journal_entries << je_credit


        elsif @equity_transaction.transaction_type == "deposit"
          je_debit = JournalEntry.new(
                  amount: @equity_transaction.amount,
                  post_type: "DR",
                  accounting_code: @equity_transaction.accounting_code
                )

          voucher.journal_entries << je_debit

          je_credit = JournalEntry.new(
                    amount: @equity_transaction.amount,
                    post_type: "CR",
                    accounting_code: @equity_transaction.equity_account.equity_type.deposit_accounting_code
                  )

          voucher.journal_entries << je_credit
        else
         # TODO: ERROR
          raise "Error in Equity Account Service"
        end

      voucher.save!
    end
  end
end