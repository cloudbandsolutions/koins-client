module Equity
	class ReverseEquityAccountTransaction
		def initialize(equity_account_transaction:)
			@equity_account_transaction =  equity_account_transaction	
		end

		def execute!
			transaction_type = ["withdraw", "wp", "fund_transfer_withdraw"].include?(@equity_account_transaction.transaction_type) ? "reverse_withdraw" : "reverse_deposit"

                  beginning_balance = @equity_account_transaction.equity_account.balance
                  ending_balance = transaction_type == "reverse_withdraw" ? beginning_balance + @equity_account_transaction.amount : beginning_balance - @equity_account_transaction.amount

                  eat = EquityAccountTransaction.new(
                                    amount: @equity_account_transaction.amount,
                                    transaction_type: transaction_type,
                                    voucher_reference_number:  @equity_account_transaction.voucher_reference_number,
                                    particular: "REVERSED ENTRY",
                                    equity_account: @equity_account_transaction.equity_account,
                                    status: 'reversed',
                                    beginning_balance: beginning_balance,
                                    ending_balance: ending_balance
                                  )

                  eat.save!
                  eat.generate_updates!
		end
	end
end