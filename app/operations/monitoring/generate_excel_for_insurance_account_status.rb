module Monitoring
  class GenerateExcelForInsuranceAccountStatus
    def initialize(report:)
      @p = Axlsx::Package.new
      @report = report
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center = wb.styles.add_style(alignment: {horizontal: :center})
          sheet.add_row [ "Monitoring Tool" ], style: @left_aligned_bold_cell
          sheet.add_row []
          @report.data['centers'].each do |center_data|
            sheet.add_row ["#{center_data['center']}"], style: @left_aligned_bold_cell

            header_insurance_type_labels = []
            InsuranceType.all.each do |insurance_type|
              header_insurance_type_labels << insurance_type.code
            end

            header_data = []
            header_data << "Member"
            header_data << "Recognition Date"
            header_data << "Status"
            header_data << "Length of Membership"
            header_data << "Cert. #"
            header_insurance_type_labels.each do |i|
              header_data << i
              header_data << "Coverage date"
              header_data << "Weeks"
              header_data << "Amt Past Due"
              header_data << "Status"
            end
            sheet.add_row header_data, style: @title_cell, widths: [40, 30, 15, 25, 25, 25, 15, 25, 20, 15, 25, 15, 25, 20, 15]

              center_data['members'].each do |member_data|

                members_data = []
                members_data << "#{member_data['last_name']}, #{member_data['first_name']} #{member_data['middle_name']}"
                members_data << "#{member_data['recognition_date']}"
                members_data << "#{member_data['status']}"
                members_data << "#{member_data['length_of_membership']}"
                members_data << "#{member_data['identification_number']}"
                member_data['accounts'].each do |account_data|
                  members_data << "#{account_data['current_balance']}"
                  members_data << "#{account_data['coverage_date']}"
                  members_data << "#{account_data['num_weeks_past_due']}"
                  members_data << "#{account_data['amt_past_due']}"
                  if account_data['status'] == "past due"
                    members_data << "Past Due"
                  elsif account_data['status'] == "advanced"
                    members_data << "Advanced"
                  elsif account_data['status'] == "lapsed"
                    members_data << "Lapsed"
                  else
                    members_data << "Normal"
                  end
                end   
                sheet.add_row members_data, style: [@left_cell, @date_format_cell, @default_cell, @default_cell, @currency_cell_right, @date_format_cell, @default_cell, @currency_cell_right, @default_cell, @currency_cell_right, @default_cell, @default_cell, @currency_cell_right, @default_cell ], widths: [40, 30, 15, 25, 25, 25, 15, 25, 20, 15, 25, 15, 25, 20, 15]
            end
            sheet.add_row []
            sheet.add_row []
          end
        end
      end

      @p
    end

    private
    def initialize_formats!(wb)
      @date_format_cell = wb.styles.add_style format_code: "mm-dd-yyyy", font_name: "Calibri", alignment: { horizontal: :right }
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_left_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri" 
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :right }, sz: 11
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
  end
end
