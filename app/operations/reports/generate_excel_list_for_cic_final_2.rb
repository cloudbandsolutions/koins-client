module Reports
  class GenerateExcelListForCic
    def initialize(start_date:, end_date:)
      @p = Axlsx::Package.new
      @start_date = start_date.to_date
      @end_date= end_date.to_date
    
      @file_version = "1.0"
      @submison_type = "0"
      
      @member_details =  MembershipPayment.where("paid_at >=? and paid_at <= ? and membership_type_id = ?",  @start_date,@end_date,1)
#      @member_details = Member.find()
      #@member_details = MembershipPayment.joins(:member).where("membership_payments.paid_at >=? and membership_payments.paid_at <= ? and  membership_payments.membership_type_id = ? and members.tin_number !=? and sss_number != ?",@start_date,@end_date,1,"","")
      active_branch = Member.pluck("branch_id").first

      @branch_code = Branch.find(active_branch).code
      @provider_code = Settings.cic_provider_code
    end
    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          sheet.add_row [
            "HD",
            @provider_codes,
            @end_date.to_date.strftime("%d%m%Y"),
            @file_version,
            @submission_type,
            "FOR THE MONTH OF #{@end_date.to_date.strftime('%B %Y').upcase}"
            ]
            first_loop_details = 1
            first_loop_total = 0
            @member_details.each do |md|
        
              md.member.gender == "Female" ? mTitle = 11 : mTitle = 10 #member title
              md.member.gender == "Female" ? mGender = "F" : mGender = "M"
          
              #civil status
              if md.member.civil_status == "SINGLE" || md.member.civil_status == "May Kinakasama"
                cStatus = 1
              elsif md.member.civil_status = "Kasal"
                cStatus = 2
              elsif md.member.civil_status = "Hiwalay"
                cStatus = 3
              elsif md.member.civil_status = "Biyudo/a"
                cStatus = 4
              else
                cStatus = 1
              end
              
              if md.member.tin_number != "" #government_id
                gov_identification_type = 10
                id_number = md.member.tin_number.split("-").join("").to_s
                #if id_number.length == 10
                  gov_identification_number = md.member.tin_number
                #else
                 # gov_identification_number = "Invalid"
                #end
              elsif md.member.sss_number != ""
                gov_identification_type = 11
                id_number = md.member.sss_number.split("-").join("").to_s
                #if id_number.length == 11
                  gov_identification_number = md.member.sss_number
                #else
                 # gov_identification_number = "Invalid"
                #end
              elsif md.member.phil_health_number != ""
                gov_identification_type = 13
                id_number = md.member.phil_health_number.split("-").join("").to_s
                #if id_number.length == 13
                  gov_identification_number = md.member.phil_health_number
                #else
                #  gov_identification_number = "Invalid"
                #end
              else
                gov_identification_type = ""
                gov_identification_number = ""
              end
              
              if md.member.mobile_number != ""
                mContactType = 3
                mContactNumber = md.member.mobile_number.to_s
              elsif md.member.home_number != ""
                mContactType = 1
                mContactNumber = md.member.home_number
              elsif
                mContactType = 7
                mContactNumber = "nocontact@noemail.com"
              end
              first_loop_total += first_loop_details
              sheet.add_row [
                "ID",
                @provider_code,
                @branch_code,
                @end_date.to_date.strftime("%d%m%Y"),
                md.member.identification_number,
                mTitle,
                md.member.first_name,
                md.member.last_name,
                md.member.middle_name,
                "","","",
                mGender,
                md.member.date_of_birth.strftime("%d%m%Y"),
                "","",
                "PH",
                "",
                cStatus,
                "","","","","","","","","","","","",
                "MI",
                md.member.address_street + " " + md.member.address_barangay + " " + md.member.address_city,
                "","","","","","","","","",
                "AI",
                md.member.address_street + " " + md.member.address_barangay + " " + md.member.address_city,
                "","","","","","","","","",
                gov_identification_type,
                gov_identification_number.split("-").join("").to_s,
                "","","","","","","","","","","","","","","","","","","","","","",
                mContactType,
                mContactNumber.split("-").join("").to_s
              ]
            end #end ng member
            
            contract_status = "NA"
            old_loan_member_details = Loan.where("date_approved <= ? and status = ?",@end_date,"active")
            second_loop_count = 1
            second_loop_total = 0
            old_loan_member_details.each do |olmd|
          
              
              if olmd.term == "weekly"
                paymentPeriodicity = "W"
              elsif olmd.term == "semi-monthly"
                paymentPeriodicity = "F"
              elsif olmd.term == "monthly"
                paymentPeriodicity = "M"
              end

              #if memberCount == 0
                memberLastLoanPaymentCount = LoanPayment.where(loan_id: olmd.id, status: "approved").count
                
                if memberLastLoanPaymentCount != 0
                  
                  last_payment_amount_details = LoanPayment.where("loan_id = ? and  status = ? and loan_payment_amount > ? ", olmd.id, "approved", "0").count
                  if last_payment_amount_details == 0 
                    last_payment_amount = ""

                  else
                  
                   loan_payment_details = LoanPayment.where("loan_id = ? and  status = ? and loan_payment_amount > ? ", olmd.id, "approved", "0").last
                   

                   last_payment_amount = loan_payment_details.loan_payment_amount
                  last_payment_date =  loan_payment_details.paid_at.strftime("%d%m%Y")

                  end

                else
                  last_payment_date = ""
                  last_payment_amount = ""

                end
                firstdate_of_payment = AmmortizationScheduleEntry.where(loan_id: olmd.id, is_void: NIL).first.amount

                overdue_payment_amount_details = olmd.principal_amt_past_due_as_of(@end_date) + olmd.interest_balance_as_of(@end_date)
                if overdue_payment_amount_details <= 0 
                  overdue_payment_amount = olmd.remaining_balance
                elsif
                  overdue_payment_amount = overdue_payment_amount_details
                end
                overdue_payment_weekly = overdue_payment_amount / firstdate_of_payment
                overdue_day_number = overdue_payment_weekly * 7

                if overdue_day_number == 0
                  overdueday = "N"
                elsif overdue_day_number >= 1 && overdue_day_number <= 30
                  overdueday = "1"
                elsif overdue_day_number >= 31 && overdue_day_number <= 60
                  overdueday = "2"
                elsif overdue_day_number >= 61 && overdue_day_number <= 90
                  overdueday = "3"
                elsif overdue_day_number >= 91 && overdue_day_number <= 180
                  overdueday = "4"
                elsif overdue_day_number >= 181 && overdue_day_number <= 365
                  overdueday = "5"
                else
                  overdueday = "6"
                  
                end

                last_payment_date_count = 0
                contractType = "20"
                contractPhase = "AC"
                second_loop_total += second_loop_count
                sheet.add_row [
                  "CI",
                  @provider_code,
                  @branch_code,
                  olmd.date_approved.strftime("%d%m%Y"),
                  olmd.member.identification_number,
                  "B",
                  "PN - #{olmd.pn_number}",
                  contractType,
                  contractPhase,
                  contract_status,
                  "PHP",
                  "PHP",
                  olmd.date_approved.strftime("%d%m%Y"),
                  "",
                  olmd.maturity_date.strftime("%d%m%Y"),
                  "",
                  last_payment_date,
                  "",
                  "",
                  olmd.principal_balance.to_i,
                  olmd.num_installments.to_i,
                  "NA",
                  "",
                  paymentPeriodicity,
                  "CAS",
                  "",
                  olmd.first_date_of_payment.strftime("%d%m%Y"),
                  last_payment_amount.to_i,
                  "","",
                  olmd.remaining_num_installments.to_i,
                  olmd.remaining_balance.to_i,
                  overdue_payment_weekly.to_i,
                  overdue_payment_amount.to_i,
                  overdueday,
                  "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
                  "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
                  "","","","","","","","","","","","","","","","","","","","","","","","","","","",""

                  #olmd.id
                  
                ]
              #end
            end
              loan_maturity_date = Loan.where("maturity_date >= ? and maturity_date <= ? and status = ? ",@start_date, @end_date,"paid")
              contract_type = 20
              contract_status = "NA"
              third_loop_details = 1
              third_loop_total = 0

              loan_maturity_date.each do |lmd|
                
                loan_payment_details = LoanPayment.where("loan_id = ? and  status = ? and loan_payment_amount > ? ", lmd.id, "approved", "0").last
                last_payment_amount = loan_payment_details.loan_payment_amount
                last_payment_date =  loan_payment_details.paid_at.strftime("%d%m%Y")
                
                contractPhase = "CL"
                contract_actual_end_date =  LoanPayment.where(loan_id: lmd.id,is_void: NIL).pluck(:paid_at).last 
                if lmd.term == "weekly"
                  payment_periodicity = "W"
                elsif lmd.term == "semi-monthly"
                  payment_periodicity = "F"
                elsif lmd.term == "monthly"
                  payment_periodicity = "M"
                end

                third_loop_total += third_loop_details
                sheet.add_row [
                  "CI",
                  @provider_code,
                  @branch_code,
                  lmd.date_approved.strftime("%d%m%Y"),
                  lmd.member.identification_number,
                  "B",
                  "PN - #{lmd.pn_number}",
                  contract_type,
                  contractPhase,
                  contract_status,
                  "PHP",
                  "PHP",
                  "","","",
                  contract_actual_end_date,
                  "","","",
                  lmd.amount,
                  lmd.num_installments,
                  "NA",
                  "",
                  payment_periodicity,
                  "CAS",
                  "",
                  lmd.first_date_of_payment.strftime("%d%m%Y"),
                  last_payment_amount,
                  "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
                  "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
                  "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""


                  #lmd.id

                ]
              end
                

              #footer
              total_number_of_data = first_loop_total + second_loop_total + third_loop_total
              sheet.add_row [
                "FT",
                @provider_code,
                @end_date.to_date.strftime("%d%m%Y"),
                total_number_of_data


              ]
          
        end
          
      end
      @p
    end
  end
end
