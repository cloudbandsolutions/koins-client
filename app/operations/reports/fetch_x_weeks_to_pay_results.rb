module Reports
  class FetchXWeeksToPayResults
    def initialize(num_weeks:, loan_product:)
      @num_weeks  = num_weeks

      if num_weeks.blank?
        @num_weeks  = 4
      end

      if loan_product.present?
        @loan_products  = LoanProduct.where(id: loan_product.id)
      else
        @loan_products  = LoanProduct.all
      end

      @centers  = Center.all
      @loans    = Loan.active.where(loan_product_id: @loan_products.pluck(:id))
      @data     = {}
      @data[:num_weeks] = @num_weeks
      @as_of  = ApplicationHelper.current_working_date
      @data[:as_of]   = @as_of.to_date.strftime("%B %d, %Y")
      @data[:loan_products] = []
      @data[:x_weeks_from_now] = (@as_of + @num_weeks.to_i.weeks).to_date.strftime("%B %d, %Y")
    end

    def execute!
      grand_total_loan_amount = 0.0
      grand_total_principal_balance = 0.0
      grand_total_interest_balance = 0.0
      grand_taotal = 0.0
      @loan_products.each do |loan_product|
        lp_data                 = {}
        lp_data[:loan_product]  = { id: loan_product.id, code: loan_product.code }
        lp_data[:code]          = loan_product.code
        lp_data[:centers]       = []

        @centers.each do |center|
          d = {}
          d[:center]  = { id: center.id, name: center.to_s }
          d[:center_name] = center.to_s
          d[:loans]   = []

          total_loan_amount       = 0.00
          total_principal_balance = 0.00
          total_interest_balance  = 0.00
          
      
          loans = @loans.joins(:member).where(
                    "loans.center_id = ? AND loans.loan_product_id = ? AND maturity_date <= ? AND maturity_date >= ?",
                    center.id, 
                    loan_product.id,
                    @data[:x_weeks_from_now],
                    @as_of.to_date
                  ).order("members.last_name ASC")

          loans.each_with_index do |loan, i|
            principal_balance = loan.ammortization_schedule_entries.sum("(principal - paid_principal)").round(2)
            interest_balance = loan.ammortization_schedule_entries.sum("(interest - paid_interest)").round(2)
            total_balance = principal_balance + interest_balance
            d[:loans] << {
              index: i+1,
              loan_id: loan.id,
              member_id: loan.member.id,
              member_identification_number: loan.member.identification_number,
              member_name: loan.member.full_name,
              pn_number:  loan.pn_number,
              principal_balance: principal_balance,
              interest_balance: interest_balance,
              total_balance: total_balance,
              maturity_date:  loan.maturity_date.strftime("%B %d, %Y"),
              loan_amount: loan.amount
            }
            # TODO: Compute totals
            total_loan_amount += loan.amount
            total_principal_balance += principal_balance
            total_interest_balance += interest_balance
          end

          d[:total]   = {
            total_loan_amount: total_loan_amount,
            total_principal_balance: total_principal_balance,
            total_interest_balance: total_interest_balance,
            total_balance: total_principal_balance + total_interest_balance
            
          }
          d[:total_loan_amount] = total_loan_amount
          d[:total_principal_balance] = total_principal_balance
          d[:total_interest_balance] = total_interest_balance
          d[:total_balance] = total_principal_balance + total_interest_balance
            grand_total_loan_amount += total_loan_amount
            grand_total_principal_balance += total_principal_balance
            grand_total_interest_balance += total_interest_balance

    
          

          if d[:loans].size > 0
            lp_data[:centers] << d
          end
        end

        if lp_data[:centers].size > 0
          @data[:loan_products] << lp_data
        end
      end
      @data[:grandtotalloan] = grand_total_loan_amount
      @data[:grandtotalprincipalbalance] = grand_total_principal_balance
      @data[:grandtotalinterestbalance] = grand_total_interest_balance
      @data[:grandtotal] = grand_total_principal_balance + grand_total_interest_balance
      @data
    end
  end
end
