module Reports
  class GenerateDailyReportInsuranceAccountStatuses
    def initialize(branch:, as_of:)
      @branch   = branch
      @as_of    = as_of.to_date
      @centers  = @branch.centers
      @insurance_types  = InsuranceType.all

      @data     = {}
    end

    def execute!
      @report = DailyReportInsuranceAccountStatus.new(
                  branch: @branch,
                  as_of: @as_of
                )

      @data[:num_members] = Member.active.count
      @data[:centers] = []

      @centers.order("name ASC").each do |center|
        d = {}
        d[:center]    = center.to_s
        d[:center_id] = center.id
        d[:members]   = []

        members = Member.active_and_resigned.where(center_id: center.id).order("last_name ASC")
        members.each do |member|
          if member.previous_mii_member_since and member.equity_value != 0
            member_record = {}
            member_record[:first_name]        = member.first_name
            member_record[:last_name]         = member.last_name
            member_record[:middle_name]       = member.middle_name
            member_record[:recognition_date]  = member.recognition_date_formatted
            member_record[:status]            = member.status
            member_record[:identification_number] = member.identification_number
            member_record[:insurance_status]  = member.insurance_status
            member_record[:length_of_membership]  = member.length_of_stay

            # begin
            #   Date.parse(member_record[:recognition_date])
            #   member_record[:length_of_membership]  = (@as_of - member_record[:recognition_date].to_date).to_i
            # rescue ArgumentError
            #   puts "Error date for member #{member.id} #{member_record[:recognition_date]}"
            #   member_record[:length_of_membership] = -1
            # end

            member_record[:accounts]    = []

            @insurance_types.each do |insurance_type|
              insurance_account = member.insurance_accounts.where(insurance_type_id: insurance_type.id).first

              if insurance_account
                member_record[:accounts] << Insurance::GenerateInsuranceAccountStatus.new(insurance_account: insurance_account).execute!
              end
            end

            d[:members] << member_record
          end
        end

        @data[:centers] << d
      end

      @report.data  = @data

      @report.save!

      @report
    end
  end
end
