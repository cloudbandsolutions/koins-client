module Reports
  class FetchListOfTimeDepositInterest
    def initialize
      @data = {}
      @data[:member_list] = []
      @c_working_date = ApplicationHelper.current_working_date
      #@member_query = DepositTimeTransaction.where("end_date <= ?", @c_working_date)
      @payment_list = PaymentCollection.where(payment_type: "time_deposit", status: "approved")
      @member_query = DepositTimeTransaction.all
    end
    def execute!
      @payment_list.each do |pl|
        @member_query.where(payment_collection_id: pl.id).each do |mq|
          tmp = {}
          tmp[:member_name] = Member.find(SavingsAccount.find(mq.savings_account_id).member_id).full_name
          tmp[:lock_in_amount] = mq.amount
          tmp[:lock_in_period] = mq.lock_in_period
          tmp[:lock_in_start_date] = mq.start_date
          tmp[:lock_in_end_date] = mq.end_date
          tmp[:total_interest] = mq.end_date
          tmp[:time_deposit_interest_amount] = mq.time_deposit_interest_amount
          @data[:member_list] << tmp

        end
      end


      @data

    end
  end
end
