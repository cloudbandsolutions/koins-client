module Reports
  class GenerateMasterListOfClientLoans
    require 'application_helper'

    def initialize(branch_id:, center_id:, loan_product_id:, as_of:)
      @current_working_date = ApplicationHelper.current_working_date
      @branches = Branch.all
      if branch_id.present?
        @branches = Branch.where(id: branch_id)
      end

      @center_id = center_id

      @loan_products = LoanProduct.all
      if loan_product_id.present?
        @loan_products = LoanProduct.where(id: loan_product_id)
      end

      @as_of          = as_of.to_date
      @data           = {}
      @data[:as_of]   = @as_of.strftime("%B %d, %Y")
      @data[:company_name]             = Settings.company_name
      @data[:total_loan_amount]        = 0.00
      @data[:total_principal_paid]     = 0.00
      @data[:total_principal_balance]  = 0.00
      @data[:total_interest_amount]    = 0.00
      @data[:total_interest_paid]      = 0.00
      @data[:total_interest_balance]   = 0.00
      @data[:total_balance]            = 0.00
      @data[:records] = []
    end

    def execute!
      @loan_products.each do |loan_product|
        record                            = {}
        record[:loan_product]             = loan_product.to_s
        record[:total_loan_amount]        = 0.00
        record[:total_principal_paid]     = 0.00
        record[:total_principal_balance]  = 0.00
        record[:total_interest_amount]    = 0.00
        record[:total_interest_paid]      = 0.00
        record[:total_interest_balance]   = 0.00
        record[:total_balance]            = 0.00
        record[:branch_records]           = []

        @branches.each do |branch|
          branch_record                           = {}
          branch_record[:branch]                  = branch.to_s
          branch_record[:total_loan_amount]       = 0.00
          branch_record[:total_principal_paid]    = 0.00
          branch_record[:total_principal_balance] = 0.00
          branch_record[:total_interest_amount]   = 0.00
          branch_record[:total_interest_paid]     = 0.00
          branch_record[:total_interest_balance]  = 0.00
          branch_record[:total_balance]           = 0.00
          branch_record[:center_records]          = []

          centers = branch.centers
          if @center_id.present?
            centers = branch.centers.where(id: @center_id)
          end

          if centers.size > 0
            centers.each do |center|
              center_record                           = {}
              center_record[:center]                  = center.to_s
              center_record[:total_loan_amount]       = 0.00
              center_record[:total_principal_paid]    = 0.00
              center_record[:total_principal_balance] = 0.00
              center_record[:total_interest_amount]   = 0.00
              center_record[:total_interest_paid]     = 0.00
              center_record[:total_interest_balance]  = 0.00
              center_record[:total_balance]           = 0.00
              center_record[:loan_records]            = []

              loans = ::Loans::FetchActiveLoansByCenterAndLoanProduct.new(
                        as_of: @as_of, 
                        center: center, 
                        loan_product: loan_product
                      ).execute!

              if loans.size > 0
                loans.each_with_index do |loan, i|
                  loan_record = {}
                  loan_record[:index]             = i + 1
                  loan_record[:member_name]       = loan.member.full_name_titleize
                  loan_record[:member_id]         = loan.member.id
                  loan_record[:date_released]     = loan.date_released.strftime("%m/%d/%Y")
                  loan_record[:maturity_date]     = loan.maturity_date_formatted
                  loan_record[:loan_amount]       = loan.amount
                  loan_record[:principal_paid]    = loan.temp_total_principal_amount_due_as_of(@as_of)
                  loan_record[:principal_balance] = loan.principal_balance_as_of(@as_of)
                  loan_record[:interest_amount]   = loan.total_interest
                  loan_record[:interest_paid]     = loan.paid_interest_as_of(@as_of)
                  loan_record[:interest_balance]  = loan_record[:interest_amount] - loan_record[:interest_paid]
                  loan_record[:total_balance]     = loan_record[:principal_balance] + loan_record[:interest_balance]

                  # Aggregate loan_amount
                  center_record[:total_loan_amount] += loan_record[:loan_amount]
                  branch_record[:total_loan_amount] += loan_record[:loan_amount]
                  record[:total_loan_amount]        += loan_record[:loan_amount]
                  @data[:total_loan_amount]         += loan_record[:loan_amount]

                  # Aggregate principal_paid
                  center_record[:total_principal_paid] += loan_record[:principal_paid]
                  branch_record[:total_principal_paid] += loan_record[:principal_paid]
                  record[:total_principal_paid]        += loan_record[:principal_paid]
                  @data[:total_principal_paid]         += loan_record[:principal_paid]

                  # Aggregate principal_balance
                  center_record[:total_principal_balance] += loan_record[:principal_balance]
                  branch_record[:total_principal_balance] += loan_record[:principal_balance]
                  record[:total_principal_balance]        += loan_record[:principal_balance]
                  @data[:total_principal_balance]         += loan_record[:principal_balance]

                  # Aggregate interest_amount
                  center_record[:total_interest_amount] += loan_record[:interest_amount]
                  branch_record[:total_interest_amount] += loan_record[:interest_amount]
                  record[:total_interest_amount]        += loan_record[:interest_amount]
                  @data[:total_interest_amount]         += loan_record[:interest_amount]

                  # Aggregate interest_paid
                  center_record[:total_interest_paid] += loan_record[:interest_paid]
                  branch_record[:total_interest_paid] += loan_record[:interest_paid]
                  record[:total_interest_paid]        += loan_record[:interest_paid]
                  @data[:total_interest_paid]         += loan_record[:interest_paid]

                  # Aggregate interest_balance
                  center_record[:total_interest_balance] += loan_record[:interest_balance]
                  branch_record[:total_interest_balance] += loan_record[:interest_balance]
                  record[:total_interest_balance]        += loan_record[:interest_balance]
                  @data[:total_interest_balance]         += loan_record[:interest_balance]

                  # Aggregate total_balance
                  center_record[:total_balance] += loan_record[:total_balance]
                  branch_record[:total_balance] += loan_record[:total_balance]
                  record[:total_balance]        += loan_record[:total_balance]
                  @data[:total_balance]         += loan_record[:total_balance]

                  center_record[:loan_records] << loan_record
                end

                if center_record[:loan_records].size > 0
                  center_record[:count] = center_record[:loan_records].size
                  branch_record[:center_records] << center_record
                end
              end
            end
          end

          if branch_record[:center_records].size > 0
            record[:branch_records] << branch_record
          end
        end

        @data[:records] << record
      end

      @data
    end
  end
end
