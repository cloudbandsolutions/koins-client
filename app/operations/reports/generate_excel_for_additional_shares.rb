module Reports
  class GenerateExcelForAdditionalShares
    def initialize(branch_id:, center_id:, start_date:, end_date:)
    @start_date = start_date
    @end_date   = end_date
    @branch     = Branch.select("*")
    @center     = Center.select("*")
    @members_rec = []
    if center_id.present?
      @center_id = @center.where(id: center_id).pluck(:id).shift
      @members = Member.joins(:membership_payments, :equity_accounts).where('members.status= ? and membership_payments.membership_type_id=? and membership_payments.paid_at >= ? and membership_payments.paid_at <= ? and equity_accounts.balance= ? and equity_accounts.status= ? and members.center_id = ? and membership_payments.status = ? ','active','1',@start_date,@end_date,'100','active',@center_id,'paid').order('membership_payments.paid_at ASC').pluck(:identification_number, 'membership_payments.paid_at', 'members.status','equity_accounts.balance','members.center_id')
      
      else
      @members = Member.joins(:membership_payments, :equity_accounts).where('members.status= ? and membership_type_id=? and membership_payments.paid_at >= ? and membership_payments.paid_at <= ? and equity_accounts.balance= ? and equity_accounts.status= ? and membership_payments.status = ?','active','1',@start_date,@end_date,'100','active','paid').order('membership_payments.paid_at ASC').pluck(:identification_number, 'membership_payments.paid_at', 'members.status','equity_accounts.balance','members.center_id')
    end
   
   
   if branch_id.present?
      @branch = @branch.where(id: branch_id)
    end

    @p = Axlsx::Package.new
   
   end

    def execute!
     
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          sheet.add_row ["Identification Number","Member Name", "Membership Date", "Status", "Center", "Equity Account Balance"," Required Additional Share Capital", "CBU Balance", "Share Capital For Payment", "Savings Balance"]
        
          @members.each do |members_data|
              
              member_id= Member.where(identification_number: members_data[0]).ids
                member_id.each do |mems|
                tmp = {}
                tmp[:member_id]= Member.find(mems).identification_number
                tmp[:member_name]= Member.find(mems).full_name
                tmp[:memship_date]= members_data[1]
                tmp[:mem_status]= members_data[2]
                tmp[:equity_account] = members_data[3]
                tmp[:member_center]= Center.find(Member.find(mems).center_id).name
                st_yr= members_data[1].year
                et_yr= Time.now.year
                multiplier= (et_yr - st_yr) * 100
                tmp[:RASC] = multiplier
                tmp[:cbu]= SavingsAccount.where(member_id: mems, savings_type_id: 3).pluck(:balance).shift
                tmp[:impok] = SavingsAccount.where(member_id: mems, savings_type_id: 1).pluck(:balance).shift
                sc_payment = tmp[:RASC] - tmp[:cbu]
                if sc_payment <= 0
                tmp[:sc_payment]= 0
                else
                tmp[:sc_payment]= sc_payment
                end
                #raise tmp[:sc_payment].inspect
                @members_rec << tmp
                end
             end
             
            @members_rec.each do |members_rec|
              sheet.add_row [
                members_rec[:member_id],
                members_rec[:member_name],
                members_rec[:memship_date],
                members_rec[:mem_status],
                members_rec[:member_center],
                members_rec[:equity_account],
                members_rec[:RASC],
                members_rec[:cbu],
                members_rec[:sc_payment],
                members_rec[:impok]
                ]  
                
            end
        end 
      end
    
    @p
    
    
    end
  end
end
