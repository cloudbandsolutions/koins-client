module Reports                                                                      
  class FetchMonthlyIncentivesData
    def initialize(start_date: start_date, end_date: end_date)
      @data                 = {}
      @end_date             = end_date
      @start_date           = start_date
      @cluster              = Cluster.where("id = ?" ,  Branch.first.cluster_id)
      @data[:date_today]    = Date.today
      @data[:branch]        = Branch.first.name
      @so_list              = []
      @data[:sample]        = []
      @data[:incentive_sum] = []
      @cluster.each do |clust|
        @data[:cl] = clust.name
      end
            

      if end_date.present?
        @resigned_member    = Member.resigned
        @beg_date           = Date.parse(start_date.to_s) - 1
        @date_inc           = Date.parse(start_date.to_s)
        @daily_report_last  = DailyReport.where("as_of <= ? ", @beg_date).last
        @daily_report       = DailyReport.where("as_of <= ? ", end_date).last
        @data[:dr_id]       = DailyReport.where("as_of <= ? ", end_date).last.id
        @repayments         = DailyReport.find(@daily_report.id).repayments
        @user_data          = User.all
        @data[:active_loaners] = DailyReport.find(@daily_report.id).member_stats["active_loaners_total"]
        @data[:total_loan_disbursed] = Loan.where("date_approved >=? and date_approved <= ? and status != 'deleted'" ,@start_date, @end_date).sum(:amount)
        @year = end_date.to_date
        @data[:year] = @year.year
        @data[:month] = @year.month

      end

    end

    def execute!
      build_curr_data!
    end

    def build_curr_data!
      
      if @end_date.present?
      @repayments["loan_products"].each do |loan_product_data|
        loan_product_data["branches"].each do |branch_data|
          branch_data["so_list"].each do |so_data|
            @so_list << so_data["so"]
          end
        end
      end
      
      @so_list        = @so_list.uniq
      @count          = @daily_report.so_member_counts["so_data"]
      @count_beg      = @daily_report_last.so_member_counts["so_data"]
      total_incentive = 0
      @so_list.each do |so_name|
        
        d = {}
        d[:name]                              = so_name 
        d[:loan_products]                     = []
        d[:total_loan_amount]                 = 0.00
        d[:total_loan_balance]                = 0.00
        d[:total_interest_balance]            = 0.00
        d[:total_num_loans]                   = 0.00
        d[:total_num_members]                 = 0.00
        d[:total_principal_paid]              = 0.00
        d[:total_temp_principal_paid]         = 0.00    # for at par values
        d[:total_interest_amount]             = 0.00
        d[:total_interest_paid]               = 0.00
        d[:total_total_paid]                  = 0.00
        d[:total_cum_due]                     = 0.00
        d[:total_principal_cum_due]           = 0.00
        d[:total_amt_past_due]                = 0.00
        d[:total_principal_amt_past_due]      = 0.00
        d[:total_rr]                          = 0.00
        d[:total_principal_rr]                = 0.00
        d[:total_portfolio]                   = 0.00
        d[:total_par_amount]                  = 0.00

        d[:resigned] =  Member.joins(:center).where("members.date_resigned >= ? and members.date_resigned <= ? and centers.officer_in_charge = ?", @start_date , @end_date , so_name).count
      
        d[:new_mem] =  Member.active.joins(:center).where("extract(month from previous_mfi_member_since) = ? and extract(year from previous_mfi_member_since) = ? and centers.officer_in_charge = ? and date_resigned IS NULL", @data[:month] , @data[:year] , so_name).count
        
        d[:balik_kasapi] = Member.joins(:center).where("members.status = 'active' and members.previous_mfi_member_since >= ? and members.previous_mfi_member_since <= ? and centers.officer_in_charge = ? and meta IS NOT NULL",@start_date , @end_date , so_name).count
        #d[:new_mem] = MonthlyReport.new_mfi_member_count.where(year: @data[:year] , month: @data[:month]).last
        d[:so_active] = User.where("concat(first_name,' ',last_name) = ? " , so_name).last.is_active
        d[:so_stat] = User.where("concat(first_name,' ',last_name) = ? " , so_name).last.is_regular
        d[:so_date_eligible] = User.where("concat(first_name,' ',last_name) = ? " , so_name).last.incentive_eligble_date

        d[:disbursement] = Loan.joins(:center).where("date_approved >=? and date_approved <= ? and status = 'active' and centers.officer_in_charge = ?" ,@start_date, @end_date, so_name).sum(:amount)

        @count.each do |o|
          if o["so"] == so_name
            d[:active_members_total] = o["active_members_total"]
            d[:active_loaners_total] = o["active_loaners_total"]
            d[:pure_savers_total] = o["pure_savers_total"]
          end
        end


      
        @count_beg.each do |os|
          if os["so"] == so_name
             d[:active_members_total_beg] = os["active_members_total"]
             d[:active_loaners_total_beg] = os["active_loaners_total"]

          end
        end
        
        if d[:active_loaners_total_beg].nil?
          mid_beg_date = User.where("concat(first_name,' ',last_name) = ? " , so_name).last.created_at
          @mid_beg_date = mid_beg_date.to_date
          @mid_beg_date = Date.parse(@mid_beg_date.to_s) + 1
          @mid_beg_report = DailyReport.where("as_of <= ? ", @mid_beg_date).last
          @mid_beg_report_data = @mid_beg_report.so_member_counts["so_data"]
          @mid_beg_report_data.each do |mid_count|
            if mid_count["so"] == so_name
              #d[:active_loaners_total_beg] = mid_count["active_loaners_total"]
              d[:active_members_total_beg] = mid_count["active_loaners_total"]
            end
          end

        end
        #d[:do_rate] = ((((d[:active_loaners_total_beg] + d[:new_mem]) - d[:active_loaners_total]) / d[:active_loaners_total_beg].to_f) * 100).round(2)
        
        d[:do_rate] = ((((d[:active_members_total_beg] + d[:new_mem]) - d[:active_members_total]) / d[:active_members_total_beg].to_f) * 100).round(2)

        @repayments["loan_products"].each do |loan_product_data|
          d_loan = {}
          d_loan[:loan_product]                     = loan_product_data["loan_product"]
          d_loan[:total_loan_amount]                = 0.00
          d_loan[:total_loan_balance]               = 0.00
          d_loan[:total_interest_balance]           = 0.00
          d_loan[:total_num_loans]                  = 0
          d_loan[:total_num_members]                = 0
          d_loan[:total_principal_paid]             = 0.00
          d_loan[:total_temp_principal_paid]        = 0.00    # for at par values
          d_loan[:total_interest_amount]            = 0.00
          d_loan[:total_interest_paid]              = 0.00
          d_loan[:total_total_paid]                 = 0.00
          d_loan[:total_cum_due]                    = 0.00
          d_loan[:total_principal_cum_due]          = 0.00
          d_loan[:total_amt_past_due]               = 0.00
          d_loan[:total_principal_amt_past_due]     = 0.00
          d_loan[:total_rr]                         = 0.00
          d_loan[:total_principal_rr]               = 0.00
          d_loan[:total_portfolio]                  = 0.00
          d_loan[:total_par_amount]                 = 0.00
        

          loan_product_data["branches"].each do |branch_data|
            branch_data["so_list"].each do |so_data|
              if so_data["so"] == so_name
                d_loan[:total_loan_amount]            =  so_data["total_loan_amount"].to_f
                d_loan[:total_loan_balance]           =  so_data["total_loan_balance"].to_f
                d_loan[:total_interest_balance]       =  so_data["total_interest_balance"].to_f
                d_loan[:total_num_loans]              =  so_data["total_num_loans"].to_i
                d_loan[:total_num_members]            =  so_data["total_num_members"].to_i
                d_loan[:total_principal_paid]         =  so_data["total_principal_paid"].to_f
                d_loan[:total_temp_principal_paid]    =  so_data["total_temp_principal_paid"].to_f
                d_loan[:total_interest_amount]        =  so_data["total_interest_amount"].to_f
                d_loan[:total_interest_paid]          =  so_data["total_interest_paid"].to_f
                d_loan[:total_total_paid]             =  so_data["total_total_paid"].to_f
                d_loan[:total_cum_due]                =  so_data["total_cum_due"].to_f
                d_loan[:total_principal_cum_due]      =  so_data["total_principal_cum_due"].to_f
                d_loan[:total_amt_past_due]           =  so_data["total_amt_past_due"].to_f
                d_loan[:total_principal_amt_past_due] =  so_data["total_principal_amt_past_due"].to_f
                d_loan[:total_rr]                     =  so_data["total_rr"].to_f
                d_loan[:total_principal_rr]           =  so_data["total_principal_rr"].to_f
                d_loan[:active_loaners_total]         =  so_data["active_loaners_total"].to_f


                t = []
                so_data["centers"].each do |center_data|
                  center_data["loans"].each do |loan_data|
                    d_loan[:total_portfolio] += loan_data["portfolio"].to_f

                    if loan_data["amt_past_due"].to_f > 0
                      d_loan[:total_par_amount] += loan_data["loan_balance"].to_f
                    end
                  end
                end

                d_loan[:total_par_rate]               =  ((d_loan[:total_par_amount] / d_loan[:total_portfolio]) * 100).round(2)

                d[:loan_products] << d_loan

                d[:total_loan_amount]             +=  so_data["total_loan_amount"].to_f
                d[:total_loan_balance]            +=  so_data["total_loan_balance"].to_f
                d[:total_interest_balance]        +=  so_data["total_interest_balance"].to_f
                d[:total_num_loans]               +=  so_data["total_num_loans"].to_i
                d[:total_num_members]             +=  so_data["total_num_members"].to_i
                d[:total_principal_paid]          +=  so_data["total_principal_paid"].to_f
                d[:total_temp_principal_paid]     +=  so_data["total_temp_principal_paid"].to_f
                d[:total_interest_amount]         +=  so_data["total_interest_amount"].to_f
                d[:total_interest_paid]           +=  so_data["total_interest_paid"].to_f
                d[:total_total_paid]              +=  so_data["total_total_paid"].to_f
                d[:total_cum_due]                 +=  so_data["total_cum_due"].to_f
                d[:total_principal_cum_due]       +=  so_data["total_principal_cum_due"].to_f
                d[:total_amt_past_due]            +=  so_data["total_amt_past_due"].to_f
                d[:total_principal_amt_past_due]  +=  so_data["total_principal_amt_past_due"].to_f
                d[:total_portfolio]               +=  d_loan[:total_portfolio]
                d[:total_par_amount]              +=  d_loan[:total_par_amount]
              end
            end
          end
        end

        d[:total_rr] = ((d[:total_total_paid] / d[:total_cum_due]) * 100).round(2)
        if d[:total_rr] > 100
          d[:total_rr] = 100
        end

        d[:total_principal_rr] = ((d[:total_temp_principal_paid] / (d[:total_principal_cum_due])) * 100).round(2)
        if d[:total_principal_rr] > 100
          d[:total_principal_rr] = 100
        end
        
        if d[:so_stat] == true
          d[:so_status] = 'Regular'
        else
          d[:so_status] = 'Trainee / Probee'
        end

        d[:total_par_rate]  = ((d[:total_par_amount] / d[:total_portfolio]) * 100).round(2)
        if d[:so_stat] == true and Date.parse(d[:so_date_eligible].to_s) <= @date_inc and d[:disbursement] >= 600000
        # 2.1 portfolio 
          if d[:total_principal_rr] >= 99 and d[:active_loaners_total] >= 250 and d[:total_loan_balance] >= 2100000 and d[:total_par_rate] <= 2.99
            d[:incentive] = 2500
            d[:do_demerits] = d[:resigned] * 20
          elsif d[:total_principal_rr] >= 98 and d[:total_principal_rr] <= 98.99 and d[:active_loaners_total] >= 250 and d[:total_loan_balance] >= 2100000 and d[:total_par_rate] <= 2.99
            d[:incentive] = 2000
            d[:do_demerits] = d[:resigned] * 20
          # 1.8 portfolio
          elsif d[:total_principal_rr] >= 99 and d[:active_loaners_total] >= 250 and d[:total_loan_balance] >= 1800000 and d[:total_loan_balance] <= 2099999  and d[:total_par_rate] <= 2.99
            d[:incentive] = 2000
            d[:do_demerits] = d[:resigned] * 20
          elsif d[:total_principal_rr] >= 98 and d[:total_principal_rr] <= 98.99 and d[:active_loaners_total] >= 250 and d[:total_loan_balance] >= 1800000 and d[:total_loan_balance] <= 2099999  and d[:total_par_rate] <= 2.99
            d[:incentive] = 1500
            d[:do_demerits] = d[:resigned] * 20
          #1.5 portfolio
          elsif d[:total_principal_rr] >= 99 and d[:active_loaners_total] >= 250 and d[:total_loan_balance] >= 1500000 and d[:total_loan_balance] <= 1799999  and d[:total_par_rate] <= 2.99
            d[:incentive] = 1500
            d[:do_demerits] = d[:resigned] * 20
          elsif d[:total_principal_rr] >= 98 and d[:total_principal_rr] <= 98.99 and d[:active_loaners_total] >= 250 and d[:total_loan_balance] >= 1500000 and d[:total_loan_balance] <= 1799999  and d[:total_par_rate] <= 2.99
            d[:incentive] = 1000
            d[:do_demerits] = d[:resigned] * 20
          #1.2 portfolio
          elsif d[:total_principal_rr] >= 99 and d[:active_loaners_total] >= 250 and d[:total_loan_balance] >= 1200000 and d[:total_loan_balance] <= 1499999  and d[:total_par_rate] <= 2.99
            d[:incentive] = 1000
            d[:do_demerits] = d[:resigned] * 20

          elsif d[:total_principal_rr] >= 98 and d[:total_principal_rr] <= 98.99 and d[:active_loaners_total] >= 250 and d[:total_loan_balance] >= 1200000 and d[:total_loan_balance] <= 1499999  and d[:total_par_rate] <= 2.99
            d[:incentive] = 500
            d[:do_demerits] = d[:resigned] * 20

          else
            d[:incentive] = 0 
            d[:do_demerits] = 0
          end

        else
          d[:incentive] = 0
          d[:do_demerits] = 0
        end
          
          d[:vw_demerits] = 0
          d[:ww_demerits] = 0
          
          d[:total_demerits] = d[:do_demerits] + d[:vw_demerits] + d[:ww_demerits]
          d[:final_incentives] = d[:incentive] - d[:total_demerits]
          
          total_incentive =+  d[:final_incentives]
        @data[:sample] << d
        @data[:incentive_sum] << total_incentive
      end
      @data[:incentive_sum] = @data[:incentive_sum].sum

      @data[:regular_so] = User.where("role = ? and is_active = ? and is_regular = ?" , 'SO' , 'true' , 'true').count

      @data[:ave_incentive] =  @data[:incentive_sum].to_f / @data[:regular_so]

      @data[:som_inc] = @data[:ave_incentive] * 0.75
      end
      #@data[:incentive_sum] = total_incentive
      @data
    
    end

  end
end
