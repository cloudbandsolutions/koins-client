module Reports
  class GenerateSoListFromDailyReport
    def initialize(daily_report:)
      @daily_report = daily_report
      @repayments   = daily_report.repayments
      @data         = []
      @so_list      = []
    end

    def execute!
      @repayments["loan_products"].each do |loan_product_data|
        loan_product_data["branches"].each do |branch_data|
          branch_data["so_list"].each do |so_data|
            @so_list << so_data["so"]
          end
        end
      end

      @so_list = @so_list.uniq

      @so_list.each do |so_name|
        d = {}
        d[:name]                              = so_name
        d[:loan_products]                     = []
        d[:total_loan_amount]                 = 0.00
        d[:total_loan_balance]                = 0.00
        d[:total_interest_balance]            = 0.00
        d[:total_num_loans]                   = 0.00
        d[:total_num_members]                 = 0.00
        d[:total_principal_paid]              = 0.00
        d[:total_temp_principal_paid]         = 0.00    # for at par values
        d[:total_interest_amount]             = 0.00
        d[:total_interest_paid]               = 0.00
        d[:total_total_paid]                  = 0.00
        d[:total_cum_due]                     = 0.00
        d[:total_principal_cum_due]           = 0.00
        d[:total_amt_past_due]                = 0.00
        d[:total_principal_amt_past_due]      = 0.00
        d[:total_rr]                          = 0.00
        d[:total_principal_rr]                = 0.00
        d[:total_portfolio]                   = 0.00
        d[:total_par_amount]                  = 0.00

        @repayments["loan_products"].each do |loan_product_data|
          d_loan = {}
          d_loan[:loan_product]                     = loan_product_data["loan_product"]
          d_loan[:total_loan_amount]                = 0.00
          d_loan[:total_loan_balance]               = 0.00
          d_loan[:total_interest_balance]           = 0.00
          d_loan[:total_num_loans]                  = 0
          d_loan[:total_num_members]                = 0
          d_loan[:total_principal_paid]             = 0.00
          d_loan[:total_temp_principal_paid]        = 0.00    # for at par values
          d_loan[:total_interest_amount]            = 0.00
          d_loan[:total_interest_paid]              = 0.00
          d_loan[:total_total_paid]                 = 0.00
          d_loan[:total_cum_due]                    = 0.00
          d_loan[:total_principal_cum_due]          = 0.00
          d_loan[:total_amt_past_due]               = 0.00
          d_loan[:total_principal_amt_past_due]     = 0.00
          d_loan[:total_rr]                         = 0.00
          d_loan[:total_principal_rr]               = 0.00
          d_loan[:total_portfolio]                  = 0.00
          d_loan[:total_par_amount]                 = 0.00

          loan_product_data["branches"].each do |branch_data|
            branch_data["so_list"].each do |so_data|
              if so_data["so"] == so_name
                d_loan[:total_loan_amount]            =  so_data["total_loan_amount"].to_f
                d_loan[:total_loan_balance]           =  so_data["total_loan_balance"].to_f
                d_loan[:total_interest_balance]       =  so_data["total_interest_balance"].to_f
                d_loan[:total_num_loans]              =  so_data["total_num_loans"].to_i
                d_loan[:total_num_members]            =  so_data["total_num_members"].to_i
                d_loan[:total_principal_paid]         =  so_data["total_principal_paid"].to_f
                d_loan[:total_temp_principal_paid]    =  so_data["total_temp_principal_paid"].to_f
                d_loan[:total_interest_amount]        =  so_data["total_interest_amount"].to_f
                d_loan[:total_interest_paid]          =  so_data["total_interest_paid"].to_f
                d_loan[:total_total_paid]             =  so_data["total_total_paid"].to_f
                d_loan[:total_cum_due]                =  so_data["total_cum_due"].to_f
                d_loan[:total_principal_cum_due]      =  so_data["total_principal_cum_due"].to_f
                d_loan[:total_amt_past_due]           =  so_data["total_amt_past_due"].to_f
                d_loan[:total_principal_amt_past_due] =  so_data["total_principal_amt_past_due"].to_f
                d_loan[:total_rr]                     =  so_data["total_rr"].to_f
                d_loan[:total_principal_rr]           =  so_data["total_principal_rr"].to_f

                t = []
                so_data["centers"].each do |center_data|
                  center_data["loans"].each do |loan_data|
                    d_loan[:total_portfolio] += loan_data["portfolio"].to_f

                    if loan_data["amt_past_due"].to_f > 0
                      d_loan[:total_par_amount] += loan_data["loan_balance"].to_f
                    end
                  end
                end

                d_loan[:total_par_rate]               =  ((d_loan[:total_par_amount] / d_loan[:total_portfolio]) * 100).round(2)

                d[:loan_products] << d_loan

                d[:total_loan_amount]             +=  so_data["total_loan_amount"].to_f
                d[:total_loan_balance]            +=  so_data["total_loan_balance"].to_f
                d[:total_interest_balance]        +=  so_data["total_interest_balance"].to_f
                d[:total_num_loans]               +=  so_data["total_num_loans"].to_i
                d[:total_num_members]             +=  so_data["total_num_members"].to_i
                d[:total_principal_paid]          +=  so_data["total_principal_paid"].to_f
                d[:total_temp_principal_paid]     +=  so_data["total_temp_principal_paid"].to_f
                d[:total_interest_amount]         +=  so_data["total_interest_amount"].to_f
                d[:total_interest_paid]           +=  so_data["total_interest_paid"].to_f
                d[:total_total_paid]              +=  so_data["total_total_paid"].to_f
                d[:total_cum_due]                 +=  so_data["total_cum_due"].to_f
                d[:total_principal_cum_due]       +=  so_data["total_principal_cum_due"].to_f
                d[:total_amt_past_due]            +=  so_data["total_amt_past_due"].to_f
                d[:total_principal_amt_past_due]  +=  so_data["total_principal_amt_past_due"].to_f
                d[:total_portfolio]               +=  d_loan[:total_portfolio]
                d[:total_par_amount]              +=  d_loan[:total_par_amount]
              end
            end
          end
        end

        d[:total_rr] = ((d[:total_total_paid] / d[:total_cum_due]) * 100).round(2)
        if d[:total_rr] > 100
          d[:total_rr] = 100
        end

        d[:total_principal_rr] = ((d[:total_temp_principal_paid] / (d[:total_principal_cum_due])) * 100).round(2)
        if d[:total_principal_rr] > 100
          d[:total_principal_rr] = 100
        end
        
        d[:total_par_rate]  = ((d[:total_par_amount] / d[:total_portfolio]) * 100).round(2)

        @data << d
      end

      @data
    end
  end
end
