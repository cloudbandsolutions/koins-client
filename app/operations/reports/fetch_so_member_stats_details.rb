module Reports
  class FetchSoMemberStatsDetails
    def initialize(so_name:,stat_type:)
      @data = {}
      @so_name = so_name
      @stat_type = stat_type
      @active_members = Member.joins(:center).where("centers.officer_in_charge = ?", @so_name)
      @active_loan_entry_points = Loan.active
      
      @member_ids_with_entry_point_loans = @active_loan_entry_points.pluck(:member_id).uniq
      
      #@member_ids_active_only = @member_ids_with_entry_point_loans

      #data for display

      @data[:pure_savers] = @active_members.where("members.status='active'").where.not("members.id IN (?)", @member_ids_with_entry_point_loans).order("centers.name ASC")
      @data[:active_loaners] = @active_members.where("members.id IN (?)", @member_ids_with_entry_point_loans).order("centers.name  ASC")

      @data[:active_members] = @active_members.where("status = ?", "active").order("centers.name  ASC")

      @data[:pending] = @active_members.where("status = ?", "pending").order("centers.name ASC")

      @data[:list_of_so_stats] = []
    end
    def execute!
      
      
        
        
          #@data[:pure_savers].each do |ps|
          #@data[:active_loaners].each do |ps|
          #@data[:active_members].each do |ps|
          #raise @stat_type.inspect
        if @stat_type =="Pure Savers"

          @data[:pure_savers].each do |ps|
            tmp = {}
            tmp[:member_name] = ps.full_name
            #tmp[:center_name] = Center.find(ps[:center_id])
            tmp[:center_name] = ps.try(:center).try(:name)
            tmp[:gender] = ps[:gender]
            #tmp[:so_name] = Center.find(ps[:center_id]).officer_in_charge
            tmp[:so_name] = ps.try(:center).try(:officer_in_charge)
            tmp[:status] = ps[:status]
            @data[:list_of_so_stats] << tmp
          end
        elsif @stat_type == "Active Member"
          tmp = {}
          @data[:active_members].each do |ps|
            tmp[:member_name] = ps.full_name
            tmp[:center_name] = ps.try(:center).try(:name)
            tmp[:gender] = ps[:gender]
            tmp[:so_name] = ps.try(:center).try(:officer_in_charge)
            tmp[:status] = ps[:status]
            #@data[:list_of_so_stats] << tmp
          end
          @data[:list_of_so_stats] << tmp
        elsif @stat_type == "Pending"
          @data[:pending].each do |ps|
            tmp = {}
            tmp[:member_name] = ps.full_name
            tmp[:center_name] = ps.try(:center).try(:name)
            tmp[:gender] = ps.gender
            tmp[:so_name] = ps.try(:center).try(:officer_in_charge)
            tmp[:status] = ps.status
            @data[:list_of_so_stats] << tmp
          end
        elsif @stat_type = "Active Loaner"
          @data[:active_loaners].each do |ps|
            tmp = {}
            tmp[:member_name] = ps.full_name
            tmp[:center_name] = ps.try(:center).try(:name)
            tmp[:so_name] = ps.try(:center).try(:officer_in_charge)
            tmp[:status] = ps.status
            @data[:list_of_so_stats] << tmp
          end
    
          
        end
          
          
    
      @data
    end
  
  end
end
