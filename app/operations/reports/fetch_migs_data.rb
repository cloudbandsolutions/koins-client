module Reports
  class FetchMigsData
    def initialize(migs_date: migs_date)
      @data             = {}
      @migs_date        = migs_date
      @watchlist        = DailyReport.where("as_of <= ?" , @migs_date).last.watchlist
      @data[:watchlist]   = []
      @data[:al] = []
    end

    def execute!
      #raise @data[:watchlist].inspect
      watchlist_ids = []
      @watchlist.each do |watchlist|
        wl = {}
        wl[:wl_mem_id]    = watchlist["member_id"]
        @data[:watchlist] <<  wl

        watchlist_ids << watchlist["member_id"]
      end
      
      Loan.active.joins(:center => :members).where("loans.date_approved <= ? and member_id NOT IN (?)" , @migs_date , watchlist_ids ).order('centers.name ASC').each do |active_loaners|
        al = {}
        al[:al_mem_id] = active_loaners.member_id

        al[:al_mem_name] = active_loaners.member.full_name
        al_center  = active_loaners.center_id
        al[:al_center] = Center.find(al_center).name
      @data[:al] << al
      end
 
      
      @data
    end
  end
end
