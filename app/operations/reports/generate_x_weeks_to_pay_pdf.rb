module Reports
  class GenerateXWeeksToPayPdf
    def initialize
    
      @data = {}
      @data[:ariel] = Settings.company
    end
    
    def execute!
      @data
    end
  end
end
