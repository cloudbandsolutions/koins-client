module Reports
	class PersonalFunds
		def initialize(branch_id:, so:, center_id:, account_types:, as_of:, detailed:)
			@branch_id      = branch_id
			@so             = so
			@center_id      = center_id
			@account_types  = account_types
      @as_of          = as_of.to_date
      @detailed       = detailed

		end

		def execute!
			@savings_types = SavingsType.all
      insurance_types = InsuranceType.all
      equity_types = EquityType.all

      @data = {}
      @data[:detailed] = @detailed
      @data[:cluster] = Cluster.first.to_s

      branches = Branch.all

      if @branch_id.present?
        branches = Branch.where(id: @branch_id)
      end

      #data[:as_of] = Time.now.strftime("%b %d, %Y")
      @data[:as_of]        = @as_of
      @data[:company_name] = Settings.company_name

      @data[:branches] = []

      @data[:account_types] = []
      if @account_types.size == 0
        SavingsType.all.each do |savings_type|
          @data[:account_types] << savings_type.code.to_s
        end

        InsuranceType.all.each do |insurance_type|
          @data[:account_types] << insurance_type.code.to_s
        end

        EquityType.all.each do |equity_type|
          @data[:account_types] << equity_type.code.to_s
        end
      else
        if @account_types.include? "Savings"
          SavingsType.all.each do |savings_type|
            @data[:account_types] << savings_type.code.to_s
          end
        end

        if @account_types.include? "Insurance"
          InsuranceType.all.each do |insurance_type|
            @data[:account_types] << insurance_type.code.to_s
          end
        end

        if @account_types.include? "Equity"
          EquityType.all.each do |equity_type|
            @data[:account_types] << equity_type.code.to_s
          end
        end
      end

      branches.each do |branch|
        d = {}
        d[:name] = branch.name

        so_list = []
        if @so.present?
          so_list << @so
        else
          so_list = Center.where(branch_id: branch.id).pluck(:officer_in_charge).uniq
        end

        d[:so_list] = []
        so_list.each do |so|
          dd = {}
          dd[:so] = so
          dd[:total_funds] = 0
          dd[:subtotals] = []

          if @account_types.size == 0
            SavingsType.all.each do |savings_type|
              dd[:subtotals] << 0
            end

            InsuranceType.all.each do |insurance_type|
              dd[:subtotals] << 0
            end

            EquityType.all.each do |equity_type|
              dd[:subtotals] << 0
            end
          else
            if @account_types.include? "Savings"
              SavingsType.all.each do |savings_type|
                dd[:subtotals] << 0
              end
            end

            if @account_types.include? "Insurance"
              InsuranceType.all.each do |insurance_type|
                dd[:subtotals] << 0
              end
            end

            if @account_types.include? "Equity"
              EquityType.all.each do |equity_type|
                dd[:subtotals] << 0
              end
            end
          end

          centers = Center.where(officer_in_charge: so)
          if @center_id.present?
            centers = Center.where(id: @center_id)
          end

          dd[:member_count] = Member.where(status: ['active', 'resigned', 'cleared'], center_id: centers.pluck(:id)).count
    
          dd[:centers] = []
          centers.each do |center|
            ddd = {}

            #members = Member.where(status: ['active', 'resigned', 'cleared'], center_id: center.id).order(:last_name)
            #temp_members = Member.where.not(status: 'pending', center_id: center.id).order(:last_name)
            #temp_members = Member.where("status != ? AND center_id = ?", "pending", center.id).order(:last_name)
            temp_members = Member.where("status NOT IN (?) AND center_id = ?", ["archived" , "cleared" , "transferred"], center.id).order(:last_name)
            members = []
            temp_members.each do |m|
              members << m
#              if m.transferred? or m.cleared?
#                transfer_request = m.member_transfer_request
#
#                if transfer_request
#                  if transfer_request.date_of_request < @as_of
#                    members << m
#                  end
#                end
#              elsif m.resigned?
#                if m.date_resigned.present?
#                  if m.date_resigned < @as_of
#                    members << m
#                  end
#                end
#              else
#                members << m
#              end
            end
            
            ddd[:name] = center.name
            ddd[:member_count] = members.count
            ddd[:total_funds] = 0.00
            ddd[:funds] = []

            ddd[:member_accounts] = []
            members.each do |m|
              ddd[:member_accounts] << { member: m.full_name_titleize, member_recognition_date_mii: m.previous_mii_member_since, member_identification_number: m.identification_number, member_status: m.status, total_for_member: 0.00, accounts: [] }
            end

            counter = 0

            if @account_types.size == 0
              SavingsType.all.each do |savings_type|
                #t = SavingsAccount.where(member_id: members.pluck(:id), savings_type_id: savings_type.id).sum(:balance)

                #ddd[:funds] << t
                #ddd[:total_funds] += t
                #dd[:total_funds] += t
                #dd[:subtotals][counter] += t

                t_fund = 0.00
                members.each_with_index do |m, i|
                  #savings_account_balance = SavingsAccount.where(savings_type_id: savings_type.id, member_id: m.id).first.try(:balance)

                  latest_transaction =  m.savings_accounts.where(
                                          savings_type_id: savings_type.id
                                        ).first.savings_account_transactions.approved.where(
                                          "date(transaction_date) <= ?", @as_of
                                        ).order(
                                          "transaction_date DESC, id DESC"
                                        ).first

                  balance = latest_transaction.try(:ending_balance)

                  if !balance
                    balance = 0
                  end

                  #ddd[:member_accounts][i][:accounts][counter] = savings_account_balance
                  #ddd[:member_accounts][i][:total_for_member] += savings_account_balance

                  ddd[:member_accounts][i][:accounts][counter] = balance
                  ddd[:member_accounts][i][:total_for_member] += balance

                  ddd[:total_funds]       += balance
                  dd[:total_funds]        += balance
                  dd[:subtotals][counter] += balance
                  t_fund                  += balance
                end
                
                ddd[:funds] << t_fund
                counter += 1
              end

              InsuranceType.all.each do |insurance_type|
                #t = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: members.pluck(:id)).sum(:balance)
                #ddd[:funds] << t
                #ddd[:total_funds] += t
                #dd[:total_funds] += t
                #dd[:subtotals][counter] += t

                t_fund = 0.00
                members.each_with_index do |m, i|
                  #insurance_account_balance = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: m.id).first.try(:balance)

                  latest_transaction  = m.insurance_accounts.where(
                                          insurance_type_id: insurance_type.id, 
                                          member_id: m.id
                                        ).first.insurance_account_transactions.approved.where(
                                          "date(created_at) <= ?", @as_of
                                        ).order(
                                          "id ASC"
                                        ).last

                  balance = latest_transaction.try(:ending_balance)

                  if !balance
                    balance = 0
                  end

                  #ddd[:member_accounts][i][:accounts][counter] = insurance_account_balance
                  #ddd[:member_accounts][i][:total_for_member] += insurance_account_balance

                  ddd[:member_accounts][i][:accounts][counter] = balance
                  ddd[:member_accounts][i][:total_for_member] += balance

                  ddd[:total_funds]       += balance
                  dd[:total_funds]        += balance
                  dd[:subtotals][counter] += balance
                  t_fund                  += balance
                end

                ddd[:funds] << t_fund
                counter += 1
              end

              EquityType.all.each do |equity_type|
                #t = EquityAccount.where(equity_type_id: equity_type.id, member_id: members.pluck(:id)).sum(:balance)
                #ddd[:funds] << t
                #ddd[:total_funds] += t
                #dd[:total_funds] += t
                #dd[:subtotals][counter] += t

                t_fund = 0.00
                members.each_with_index do |m, i|
                  #equity_account_balance = EquityAccount.where(equity_type_id: equity_type.id, member_id: m.id).first.try(:balance)

                  #need to change id to transacted_date in order
                 
                  latest_transaction  = m.equity_accounts.where(
                                          equity_type_id: equity_type.id, 
                                          member_id: m.id
                                        ).first.equity_account_transactions.approved.where(
                                          "date(transaction_date) <= ?", @as_of
                                        ).order(
                                          "id ASC"
                                        ).last

                  balance = latest_transaction.try(:ending_balance)
                  if !balance
                    balance = 0
                  end

                  #ddd[:member_accounts][i][:accounts][counter] = equity_account_balance
                  #ddd[:member_accounts][i][:total_for_member] += equity_account_balance

                  ddd[:member_accounts][i][:accounts][counter] = balance
                  ddd[:member_accounts][i][:total_for_member] += balance

                  ddd[:total_funds]       += balance
                  dd[:total_funds]        += balance
                  dd[:subtotals][counter] += balance
                  t_fund                  += balance
                end

                ddd[:funds] << t_fund
                counter += 1
              end
            else
              if @account_types.include? "Savings"
                SavingsType.all.each do |savings_type|
                  #t = SavingsAccount.where(savings_type_id: savings_type.id, member_id: members.pluck(:id)).sum(:balance)
                  #ddd[:funds] << t
                  #ddd[:total_funds] += t
                  #dd[:total_funds] += t
                  #dd[:subtotals][counter] += t

                  t_fund = 0.00
                  members.each_with_index do |m, i|
                    #savings_account_balance = SavingsAccount.where(savings_type_id: savings_type.id, member_id: m.id).first.try(:balance)

                   # latest_transaction = m.savings_accounts.where(savings_type_id: savings_type.id, member_id: m.id).first.savings_account_transactions.approved.where("date(transaction_date) <= ?", @as_of).order("transaction_date ASC").last

                    latest_transaction  = m.savings_accounts.where(
                                            savings_type_id: savings_type.id, 
                                            member_id: m.id
                                          ).first.savings_account_transactions.approved.where(
                                            "date(transaction_date) <= ?", @as_of
                                          ).order(
                                            "transaction_date DESC, id DESC"
                                          ).first
            
            #latest_transaction = m.savings_accounts.where(savings_type_id: savings_type.id, member_id: m.id).first.savings_account_transactions.approved.where("date(transaction_date) <= ?", @as_of).order("transaction_date DESC").first




                    balance = latest_transaction.try(:ending_balance)

                    if !balance
                      balance = 0
                    end

                    #ddd[:member_accounts][i][:accounts][counter] = savings_account_balance
                    #ddd[:member_accounts][i][:total_for_member] += savings_account_balance

                    ddd[:member_accounts][i][:accounts][counter] = balance
                    ddd[:member_accounts][i][:total_for_member] += balance

                    ddd[:total_funds]       += balance
                    dd[:total_funds]        += balance
                    dd[:subtotals][counter] += balance
                    t_fund                  += balance
                  end

                  ddd[:funds] << t_fund
                  counter += 1
                end
              end

              if @account_types.include? "Insurance"
                InsuranceType.all.each do |insurance_type|
                  #t = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: members.pluck(:id)).sum(:balance)
                  #ddd[:funds] << t
                  #ddd[:total_funds] += t
                  #dd[:total_funds] += t
                  #dd[:subtotals][counter] += t

                  t_fund = 0.00
                  members.each_with_index do |m, i|
                    #insurance_account_balance = InsuranceAccount.where(insurance_type_id: insurance_type.id, member_id: m.id).first.try(:balance)

                    latest_transaction  = m.insurance_accounts.where(
                                            insurance_type_id: insurance_type.id, 
                                            member_id: m.id
                                          ).first.insurance_account_transactions.approved.where(
                                            "date(transaction_date) <= ?", @as_of
                                          ).order(
                                            "id ASC"
                                          ).last

                    balance = latest_transaction.try(:ending_balance)
                    if !balance
                      balance = 0
                    end

                    #ddd[:member_accounts][i][:accounts][counter] = insurance_account_balance
                    #ddd[:member_accounts][i][:total_for_member] += insurance_account_balance

                    ddd[:member_accounts][i][:accounts][counter] = balance
                    ddd[:member_accounts][i][:total_for_member] += balance

                    ddd[:total_funds]       += balance
                    dd[:total_funds]        += balance
                    dd[:subtotals][counter] += balance
                    t_fund                  += balance
                  end

                  ddd[:funds] << t_fund
                  counter += 1
                end
              end

              if @account_types.include? "Equity"
                EquityType.all.each do |equity_type|
                  #t = EquityAccount.where(equity_type_id: equity_type.id, member_id: members.pluck(:id)).sum(:balance)
                  #ddd[:funds] << t
                  #ddd[:total_funds] += t
                  #dd[:total_funds] += t
                  #dd[:subtotals][counter] += t

                  t_fund = 0.00
                  members.each_with_index do |m, i|
                    #equity_account_balance = EquityAccount.where(equity_type_id: equity_type.id, member_id: m.id).first.try(:balance)
                    #need to change id to transacted_date in order
                    latest_transaction  = m.equity_accounts.where(
                                            equity_type_id: equity_type.id, 
                                            member_id: m.id
                                          ).first.equity_account_transactions.approved.where(
                                            "date(transaction_date) <= ?", @as_of
                                          ).order(
                                            "id ASC"
                                          ).last

                    balance = latest_transaction.try(:ending_balance)
                    if !balance
                      balance = 0
                    end



                    #ddd[:member_accounts][i][:accounts][counter] = equity_account_balance
                    #ddd[:member_accounts][i][:total_for_member] += equity_account_balance

                    ddd[:member_accounts][i][:accounts][counter] = balance
                    ddd[:member_accounts][i][:total_for_member] += balance

                    ddd[:total_funds]       += balance
                    dd[:total_funds]        += balance
                    dd[:subtotals][counter] += balance
                    t_fund                  += balance
                  end

                  ddd[:funds] << t_fund
                  counter += 1
                end
              end
            end

            dd[:centers] << ddd
          end

          d[:so_list] << dd
        end

        branch_total_funds = 0
        branch_total_funds_breakdown = []
        if @account_types.size == 0
          SavingsType.all.each do |savings_type|
            branch_total_funds_breakdown << 0
          end

          InsuranceType.all.each do |insurance_type|
            branch_total_funds_breakdown << 0
          end

          EquityType.all.each do |equity_type|
            branch_total_funds_breakdown << 0
          end
        else
          if @account_types.include? "Savings"
            SavingsType.all.each do |savings_type|
              branch_total_funds_breakdown << 0
            end
          end

          if @account_types.include? "Insurance"
            InsuranceType.all.each do |insurance_type|
              branch_total_funds_breakdown << 0
            end
          end

          if @account_types.include? "Equity"
            EquityType.all.each do |equity_type|
              branch_total_funds_breakdown << 0
            end
          end
        end

        d[:so_list].each do |o|
          branch_total_funds += o[:total_funds]
          o[:subtotals].each_with_index do |t, i|
            branch_total_funds_breakdown[i] += t
          end
        end
       
        #d[:branch_member_total] = Member.where(status: ['active', 'transferred', 'cleared'], branch_id: branch.id).count
        #d[:branch_member_total] = Member.where(status: ['active', 'resigned', 'cleared'], branch_id: branch.id).count
        d[:branch_member_total] = Member.where.not(status: ['archived','cleared','transferred']).where(branch_id: branch.id).count
        d[:branch_total_funds] = branch_total_funds
        d[:branch_total_funds_breakdown] = branch_total_funds_breakdown
        @data[:branches] << d
      end

      #all_members = Member.active.where(branch_id: branches)
      #all_members = Member.where(status: ['active', 'transferred', 'cleared']).where(branch_id: branches)
      #all_members = Member.where(status: ['active', 'resigned', 'cleared']).where(branch_id: branches)
      #members = Member.where(status: ['active', 'resigned', 'cleared'], center_id: center.id).order(:last_name)
      #temp_members = Member.where.not(status: 'pending').where(branch_id: branches)
      temp_members = Member.where("status NOT IN (?) AND branch_id IN (?)", ["archived" , "cleared" , "transferred"], branches.pluck(:id)).order(:last_name)
      all_members = []
      temp_members.each do |m|
        all_members << m
#        if m.transferred? or m.cleared?
#          transfer_request = m.member_transfer_request
#
#          if transfer_request
#            if transfer_request.date_of_request < @as_of
#              all_members << m
#            end
#          end
#        elsif m.resigned?
#          if m.date_resigned.present?
#            if m.date_resigned < @as_of
#              all_members << m
#            end
#          end
#        else
#          all_members << m
#        end
      end

      @data[:grand_total_member_count] = all_members.count
      @data[:grand_total_funds] = 0

      @data[:branches].each do |b|
        @data[:grand_total_funds] += b[:branch_total_funds]
      end

      @data[:grand_total_account_type_funds] = []
      if @account_types.size == 0
        SavingsType.all.each do |savings_type|
          #data[:grand_total_account_type_funds] << SavingsAccount.where(member_id: all_members.pluck(:id), savings_type_id: savings_type.id).sum(:balance)
          t = 0.00
          all_members.each_with_index do |m, i|
            latest_transaction  = m.savings_accounts.where(
                                    savings_type_id: savings_type.id, 
                                    member_id: m.id
                                  ).first.savings_account_transactions.approved.where(
                                    "date(transaction_date) <= ?", @as_of
                                  ).order(
                                    "transaction_date DESC, id DESC"
                                  ).first


            balance = latest_transaction.try(:ending_balance)

            if !balance
              balance = 0
            end

            t += balance
          end

          @data[:grand_total_account_type_funds] << t
        end

        InsuranceType.all.each do |insurance_type|
          #data[:grand_total_account_type_funds] << InsuranceAccount.where(member_id: all_members.pluck(:id), insurance_type_id: insurance_type.id).sum(:balance)
          t = 0.00
          all_members.each_with_index do |m, i|
            latest_transaction  = m.insurance_accounts.where(
                                    insurance_type_id: insurance_type.id, 
                                    member_id: m.id
                                  ).first.insurance_account_transactions.approved.where(
                                    "date(transaction_date) <= ?", @as_of
                                  ).order(
                                    "id ASC"
                                  ).last

            balance = latest_transaction.try(:ending_balance)
            if !balance
              balance = 0
            end

            t += balance
          end

          @data[:grand_total_account_type_funds] << t
        end

        EquityType.all.each do |equity_type|
          #data[:grand_total_account_type_funds] << EquityAccount.where(member_id: all_members.pluck(:id), equity_type_id: equity_type.id).sum(:balance)
          t = 0.00
          all_members.each_with_index do |m, i|

          #need to change id to transacted_date in order
            latest_transaction  = m.equity_accounts.where(
                                    equity_type_id: equity_type.id, 
                                    member_id: m.id
                                  ).first.equity_account_transactions.approved.where(
                                    "date(transaction_date) <= ?", @as_of
                                  ).order("id ASC").last

            balance = latest_transaction.try(:ending_balance)
            if !balance
              balance = 0
            end

            t += balance
          end

          @data[:grand_total_account_type_funds] << t
        end
      else
        if @account_types.include? "Savings"
          SavingsType.all.each do |savings_type|
            #data[:grand_total_account_type_funds] << SavingsAccount.where(member_id: all_members.pluck(:id), savings_type_id: savings_type.id).sum(:balance)
            t = 0.00
            all_members.each_with_index do |m, i|
              latest_transaction  = m.savings_accounts.where(
                                      savings_type_id: savings_type.id, 
                                      member_id: m.id
                                    ).first.savings_account_transactions.approved.where(
                                      "date(transaction_date) <= ?", @as_of
                                    ).order(
                                      "transaction_date DESC, id DESC"
                                    ).first

              balance = latest_transaction.try(:ending_balance)
              if !balance
                balance = 0
              end

              t += balance
            end

            @data[:grand_total_account_type_funds] << t
          end
        end

        if @account_types.include? "Insurance"
          InsuranceType.all.each do |insurance_type|
            #data[:grand_total_account_type_funds] << InsuranceAccount.where(member_id: all_members.pluck(:id), insurance_type_id: insurance_type.id).sum(:balance)
            t = 0.00
            all_members.each_with_index do |m, i|
              latest_transaction  = m.insurance_accounts.where(
                                      insurance_type_id: insurance_type.id, 
                                      member_id: m.id
                                    ).first.insurance_account_transactions.approved.where(
                                      "date(transaction_date) <= ?", @as_of
                                    ).order(
                                      "id ASC"
                                    ).last

              balance = latest_transaction.try(:ending_balance)
              if !balance
                balance = 0
              end

              t += balance
            end

            @data[:grand_total_account_type_funds] << t
          end
        end

        if @account_types.include? "Equity"
          EquityType.all.each do |equity_type|
            #data[:grand_total_account_type_funds] << EquityAccount.where(member_id: all_members.pluck(:id), equity_type_id: equity_type.id).sum(:balance)
            t = 0.00
            all_members.each_with_index do |m, i|

              #need to change id to transacted_date in order
              latest_transaction  = m.equity_accounts.where(
                                      equity_type_id: equity_type.id, 
                                      member_id: m.id
                                    ).first.equity_account_transactions.approved.where(
                                      "date(transaction_date) <= ?", @as_of
                                    ).order(
                                      "id ASC"
                                    ).last

              balance = latest_transaction.try(:ending_balance)
              if !balance
                balance = 0
              end

              t += balance
            end

            @data[:grand_total_account_type_funds] << t
          end
        end
      end

      @data
		end
	end
end
