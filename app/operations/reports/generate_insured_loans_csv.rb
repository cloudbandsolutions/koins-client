module Reports
	class GenerateInsuredLoansCsv
		def initialize(data:)
			@data = data
		end

		def execute!
	       CSV.generate do |csv|
                        csv << [ 
                            :identification_number,
                            :first_name,
                            :middle_name,
                            :last_name,
                            :pn_number,
                            :date_released,
                            :maturity_date,
                            :loan_category,
                            :loan_term,
                            :loan_insurance_amount,
                            :loan_amount,
                            :uuid
                        ]
                @data.each do |data|
                    
                    csv << [
                        data[:identification_number],
                        data[:last_name],
                        data[:first_name],
                        data[:middle_name],
                        data[:pn_number],
                        data[:approximated_date_released].to_date.strftime("%Y-%m-%d"),
                        data[:maturity_date].to_date.strftime("%Y-%m-%d"),
                        data[:loan_product],  
                        data[:num_installments],
                        data[:insured_amount], 
                        data[:amount],
                        data[:uuid]
                        
                    ]
                end
            end
		end
	end
end