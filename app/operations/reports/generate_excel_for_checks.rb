module Reports
  class GenerateExcelForChecks
    def initialize(checks:)
      @checks = checks
      @p      = Axlsx::Package.new
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          sheet.add_row ["CHECKS"]
          sheet.add_row ["Date", "Check Number", "Name", "Amount", "Branch"]

          @checks.each do |check|
            sheet.add_row [check.date_prepared, check.check_number, check.payee, check.total_debit, check.branch.name]
          end
        end
      end

      @p
    end
  end
end
