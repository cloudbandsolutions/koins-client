module Reports
  class FetchTransferredMembers
    def initialize
      @data                 = []
      @transferred_loans    = Loan.transferred
      @loan_ids             = @transferred_loans.pluck(:id)
      @transferred_members  = Member.where(id: @transferred_loans.pluck(:member_id).uniq).order("members.last_name ASC")
    end

    def execute!
      @transferred_members.each do |member|
        loans             = member.loans.where(id: @loan_ids)
        savings_accounts  = member.savings_accounts.order("savings_type_id ASC")
        loans_size        = loans.size
        accounts_size     = savings_accounts.size

        member_data = {}
        member_data[:member]  = {}
        member_data[:member][:id]         = member.id
        member_data[:member][:uuid]       = member.uuid
        member_data[:member][:full_name]  = member.full_name
        member_data[:member][:branch]     = member.branch.to_s
        member_data[:member][:center]     = member.center.to_s
        member_data[:member][:num_sa]     = accounts_size
        member_data[:member][:num_loans]  = loans_size
        member_data[:member][:num_rows]   = loans_size > accounts_size ? loans_size : accounts_size
        member_data[:member][:savings]    = []
        
        savings_accounts.each do |savings_account|
          amount =  savings_account.savings_account_transactions.approved.order(
                      "transacted_at ASC"
                    ).first.try(:amount)

          if !amount
            amount  = 0.00
          end
          member_data[:member][:savings] << {
            amount: amount,
            savings_type: savings_account.savings_type.to_s
          }
        end

        member_data[:member][:loans]      = []
        loans.where(id: @loan_ids).each do |loan|
          member_data[:member][:loans] << {
            original_amount: loan.original_loan_amount,
            principal: loan.amount,
            interest: loan.total_interest,
            loan_product: loan.loan_product.to_s,
            date_released: loan.date_released,
            maturity_date: loan.maturity_date_formatted
          }
        end

        @data << member_data
      end

      @data
    end
  end
end
