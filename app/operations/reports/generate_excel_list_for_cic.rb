module Reports
  class GenerateExcelListForCic
    def initialize(start_date:, end_date:)
      @p = Axlsx::Package.new
      @start_date = start_date.to_date
      @end_date= end_date.to_date
    
      @file_version = "1.0"
      @submison_type = "0"
      
      #@member_details =  MembershipPayment.where("paid_at >=? and paid_at <= ? and membership_type_id = ?",  @start_date,@end_date,1)
      @member_details =  MembershipPayment.joins(:member).where("members.status = ? and membership_type_id = ?","active",1)
      @for_member_loan = []
       @for_member_loan = @member_details.ids
      @branch_code = Branch.find(Settings.branch_ids.last).code
      @provider_code = Settings.cic_provider_code
    end
    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          ################ header ###############
            sheet.add_row [
              "hd",
              @provider_code,
              @end_date.to_date.strftime("%d%m%Y"),
              @file_version,
              0,
              "FOR THE MONTH OF #{@end_date.to_date.strftime('%B %Y').upcase}"
              ], style: [nil, nil],
                 types: [nil, 
                         :string, 
                         nil, 
                         :string, 
                         :string,
                         :string]
          ################ end of header ##########
            first_loop_total = 0 

            @member_details.each do |md|
              
              md.member.gender == "Female" ? mTitle = 11 : mTitle = 10 #member title
              md.member.gender == "Female" ? mGender = "F" : mGender = "M"
          
              #civil status
              if md.member.civil_status == "SINGLE" || md.member.civil_status == "May Kinakasama"
                cStatus = 1
              elsif md.member.civil_status = "Kasal"
                cStatus = 2
              elsif md.member.civil_status = "Hiwalay"
                cStatus = 3
              elsif md.member.civil_status = "Biyudo/a"
                cStatus = 4
              else
                cStatus = 1
              end

              
          ################### government id ###########################
           
              tmp ={}
              tmp[:list_of_government] = []
              data = [
                      {:government_name => "SSS", :government_number => md.member.sss_number_details, :cic_number => "11"},
                      {:government_name => "TIN NUMBER", :government_number => md.member.tin_number_details,:cic_number => "10"},
                      {:government_name => "PHILHEALTH NUMBER", :government_number => md.member.phil_health_number_details,:cic_number => "13"}
                    ]

              data.each do |a|
                tmpGov={}
                if a[:government_number] != "Invalid"
                  tmpGov[:government_name] =  a[:government_name]
                  tmpGov[:government_number] =  a[:government_number]
                  tmpGov[:cic_number] =  a[:cic_number]

                  tmp[:list_of_government] <<  tmpGov
                end


              end #end of data
    
          ################### end of government id ####################

          ################### mobile number ###########################



              if md.member.mobile_number.size > 0          
                mobile_number_formated =  md.member.mobile_number.split("-").first && md.member.mobile_number.split("/").first
              
                if mobile_number_formated.size == 11
                  mobile_number_details = mobile_number_formated
                else
                  mobile_number_formated =  md.member.mobile_number.split("-").last && md.member.mobile_number.split("/").last
                  if mobile_number_formated == 11
                    mobile_number_details = mobile_number_formated 
                  end
                end


                if mobile_number_formated.size > 0
                  mContactType = 3
                  mContactNumber = mobile_number_formated.to_s
                end
              else

                mContactType = 7
                mContactNumber = "nocontact@noemail.com"
              

              end
          ################### end of mobile number ####################

              government_id_count =  tmp[:list_of_government].size
        

              if government_id_count > 0
                cic_gov_number = tmp[:list_of_government][0][:cic_number]
                cic_member_number = tmp[:list_of_government][0][:government_number]

              else
                cic_gov_number = ""
                cic_member_number = ""
              end if 
                first_loop_total += 1
                sheet.add_row [
                  "ID",
                  @provider_code,
                  @branch_code,
                  @end_date.to_date.strftime("%d%m%Y"),
                  md.member.identification_number,
                  mTitle,
                  md.member.first_name,
                  md.member.last_name,
                  md.member.middle_name,
                  "","","",  #3
                  mGender,
                  md.member.date_of_birth.strftime("%d%m%Y"),
                  "","", #2
                  "PH",
                  "", #1
                  cStatus,
                  "","","","","","","","","","","","", #12
                  "MI",
                  md.member.address_street + " " + md.member.address_barangay + " " + md.member.address_city,
                  "","","","","","","","","", #9
                  "AI",
                  md.member.address_street + " " + md.member.address_barangay + " " + md.member.address_city,
                  "","","","","","","","","", #9
                  cic_gov_number,
                  cic_member_number,
                  #"",
                  #"",
                  "","","","","","","","","","","","","","","","","","","","","","", #22
                  mContactType,
                  mContactNumber,
                  "","","","","","","","","","","","","","","","","","","","","","", #22
                  "","","","","","","","","","","","","","","","","","","","","","","end_of_data", #22
                  
                
                ], style: [ nil, nil ],
                   types: [ nil,:string,:string,nil,:string,nil,nil,nil,nil,
                            nil,nil,nil, #3
                            nil,:string,
                            nil,nil, #2
                            nil,
                            nil, #1
                            nil,
                            nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil, #12
                            nil,:string,
                            nil,nil,nil,nil,nil,nil,nil,nil,nil, #9
                            nil,:string,
                            nil,nil,nil,nil,nil,nil,nil,nil,nil, #9
                            nil,:string,
                            nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil, #22
                            nil,:string,
                            nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil, #22
                            nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,nil,:string #22
                          ]
              
              
              
              #end #end government id count
            end #end of member details

            contract_status = ""
            old_loan_member_details = Loan.where("date_approved <= ? and status = ? and member_id IN (?)",@end_date,"active",@for_member_loan)
            second_loop_count = 1
            second_loop_total = 0
            old_loan_member_details.each do |olmd|
          
              
              if olmd.term == "weekly"
                paymentPeriodicity = "W"
              elsif olmd.term == "semi-monthly"
                paymentPeriodicity = "F"
              elsif olmd.term == "monthly"
                paymentPeriodicity = "M"
              end

                firstdate_of_payment = AmmortizationScheduleEntry.where(loan_id: olmd.id, is_void: NIL).order(:due_at).first.amount
                firstdate_of_amort = AmmortizationScheduleEntry.where(loan_id: olmd.id, is_void: NIL).order(:due_at).first.due_at
                
                firstdate_of_payment_date = firstdate_of_amort.to_date - 7.days
              #if memberCount == 0
                memberLastLoanPaymentCount = LoanPayment.where(loan_id: olmd.id, status: "approved").count
                
                if memberLastLoanPaymentCount != 0
                  
                  last_payment_amount_details = LoanPayment.where("loan_id = ? and  status = ? and loan_payment_amount > ? ", olmd.id, "approved", "0").count
                  if last_payment_amount_details == 0 
                    last_payment_amount = 0

                  else
                  
                   loan_payment_details = LoanPayment.where("loan_id = ? and  status = ? and loan_payment_amount > ? ", olmd.id, "approved", "0").order(:paid_at).last
                   
                  
                   last_payment_amount = loan_payment_details.loan_payment_amount



                   last_payment_date_details =  loan_payment_details.paid_at.to_date
                   if last_payment_date_details >= firstdate_of_payment_date.to_date

                    last_payment_date =  loan_payment_details.paid_at.to_date.strftime("%d%m%Y")

                   else
                    
                    last_payment_date = "jef"
                   end
                  
                  

                  end

                else
                  last_payment_date = ""
                  last_payment_amount = 0

                end
                
                monthly_payment_amount = firstdate_of_payment * 4
                overdue_payment_amount_details = olmd.principal_amt_past_due_as_of(@end_date) + olmd.interest_balance_as_of(@end_date)
                if overdue_payment_amount_details <= 0 
                  overdue_payment_amount = olmd.remaining_balance
                elsif
                  overdue_payment_amount = overdue_payment_amount_details
                end
                overdue_payment_weekly = overdue_payment_amount / firstdate_of_payment
                overdue_day_number = (overdue_payment_weekly * 7).to_i

                
                if overdue_day_number == 0
                  overdueday = "N"
                elsif overdue_day_number >= 1 && overdue_day_number <= 30
                  overdueday = "1"
                elsif overdue_day_number >= 31 && overdue_day_number <= 60
                  overdueday = "2"
                elsif overdue_day_number >= 61 && overdue_day_number <= 90
                  overdueday = "3"
                elsif overdue_day_number >= 91 && overdue_day_number <= 180
                  overdueday = "4"
                elsif overdue_day_number >= 181 && overdue_day_number <= 365
                  overdueday = "5"
                else
                  overdueday = "6"
                  
                end
      
                last_payment_date_count = 0
                contractType = "20"
                contractPhase = "AC"
                second_loop_total += second_loop_count
                #if firstdate_of_payment_date.to_date.month >= @end_date.to_date.month          
                  sheet.add_row [
                    "CI",
                    @provider_code,
                    @branch_code,
                    @end_date.to_date.strftime("%d%m%Y"),
                    olmd.member.identification_number,
                    "B",
                    "PN - #{olmd.pn_number}",
                    contractType,
                    contractPhase,
                    contract_status,
                    "PHP",
                    "PHP",
                    firstdate_of_payment_date.to_date.strftime("%d%m%Y"),
                    "",
                    olmd.maturity_date.to_date.strftime("%d%m%Y"),
                    "",
                    last_payment_date,
                    "",
                    "",
                    olmd.principal_balance.to_i,
                    olmd.num_installments.to_i,
                    "NA",
                    "",
                    paymentPeriodicity,
                    "CAS",
                    monthly_payment_amount,
                    firstdate_of_amort.to_date.strftime("%d%m%Y"),
                    last_payment_amount.to_i,
                    "",
                    "",
                    olmd.remaining_num_installments.to_i,
                    olmd.remaining_balance.to_i,
                    overdue_payment_weekly.to_i,
                    overdue_payment_amount.to_i,
                    overdueday,
                    "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
                    "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
                    "","","","","","","","","","","","","","","","","","","","","","","","","","","","end_of_data"
                  ],style: [nil, nil],
                    types: [nil,nil,nil,:string,nil,nil,nil,nil,nil,nil,nil,:string,:string,nil,:string,nil,:string,nil,nil,:string,:string,nil,nil,:string,nil,nil,:string,:string,nil,nil,:string,nil,:string,:string]


              #end
            end
              loan_maturity_date = Loan.where("maturity_date >= ? and maturity_date <= ? and status = ? and member_id IN (?) ",@start_date, @end_date,"paid",@for_member_loan)
              contract_type = 20
              contract_status = ""
              third_loop_details = 1
              third_loop_total = 0

              loan_maturity_date.each do |lmd|
                
                loan_payment_details = LoanPayment.where("loan_id = ? and  status = ? and loan_payment_amount > ? ", lmd.id, "approved", "0").order(:paid_at).last
                last_payment_amount = loan_payment_details.loan_payment_amount


                firstdate_of_amort = lmd.first_date_of_payment 
                firstdate_of_payment_date = firstdate_of_amort.to_date - 7.days

                if loan_payment_details.paid_at.to_date >= firstdate_of_payment_date

                  last_payment_date =  loan_payment_details.paid_at.strftime("%d%m%Y")
                else
                  last_payment_date = "drilon"
                end
                
                firstdate_of_payment_amount = AmmortizationScheduleEntry.where(loan_id: lmd.id, is_void: NIL).order(:due_at).first.amount
                
                #lastdate_of_payment_date = AmmortizationScheduleEntry.where(loan_id: lmd.id, is_void: NIL).order(:due_at).last.due_at
                lastdate_of_payment_date = last_payment_date
                monthly_payment_amount =  firstdate_of_payment_amount * 4
                
                contractPhase = "CL"
                contract_actual_end_date =  LoanPayment.where(loan_id: lmd.id,is_void: NIL).order(:paid_at).pluck(:paid_at).last 
                if lmd.term == "weekly"
                  payment_periodicity = "W"
                elsif lmd.term == "semi-monthly"
                  payment_periodicity = "F"
                elsif lmd.term == "monthly"
                  payment_periodicity = "M"
                end

                third_loop_total += third_loop_details
                sheet.add_row [
                  "CI",
                  @provider_code,
                  @branch_code,
                  @end_date.to_date.strftime("%d%m%Y"),
                  lmd.member.identification_number,
                  "B",
                  "PN - #{lmd.pn_number}",
                  contract_type,
                  contractPhase,
                  contract_status,
                  "PHP",
                  "PHP",
                  firstdate_of_payment_date.to_date.strftime("%d%m%Y"),
                  "",
                  lmd.maturity_date.to_date.strftime("%d%m%Y"),
                  contract_actual_end_date.to_date.strftime("%d%m%Y"),
                  lmd.maturity_date.to_date.strftime("%d%m%Y"),
                  "","",
                  lmd.amount.to_i,
                  lmd.num_installments,
                  "NA",
                  "",
                  payment_periodicity,
                  "CAS",
                  monthly_payment_amount.to_i,
                  firstdate_of_amort.to_date.strftime("%d%m%Y"),*
                  last_payment_amount.to_i,
                  "","",0,0,
                  0,
                  0,
                  "N","",
                  "",
                  "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
                  "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",
                  "","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","end_of_data"


                
                ], style: [nil,nil],
                   types: [nil,nil,nil,:string,nil,nil,nil,nil,nil,nil,nil,:string,:string,nil,:string,:string,:string,nil,nil,:string,nil,nil,nil,nil,nil,:string,:string,nil,:string,:string,:string,:string,:string,:string,:string]
              end

 
        
        ################### footer ####################
              sheet.add_row [
                "FT",
                @provider_code,
                @end_date.to_date.strftime("%d%m%Y"),
                first_loop_total


              ]

        ################### end of footer ####################






        end  #end of wb

      end #end of @p
      @p



    end

    




  end
end
