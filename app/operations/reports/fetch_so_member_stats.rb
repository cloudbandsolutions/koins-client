module Reports
  class FetchSoMemberStats
    def initialize
      @as_of                        = ApplicationHelper.current_working_date.to_date
      @active_members               = Member.active.joins(:center)
      @pending_members              = Member.pending.joins(:center)
      @data                         = {}
      @active_loan_entry_points     = Loan.active
      @centers                      = Center.all
      @so_list                      = @centers.pluck(:officer_in_charge).uniq
      @member_ids_with_positive_savings         = SavingsAccount.where("balance >= 0").pluck(:member_id).uniq
      @member_ids_with_active_entry_point_loans = @active_loan_entry_points.pluck(:member_id)
      @member_ids_active_only                   = @member_ids_with_positive_savings | @member_ids_with_active_entry_point_loans

      @data[:as_of]   = @as_of.strftime("%B %d, %Y")
      @data[:so_data] = []
    end

    def execute!
      @so_list.each do |so|
        if so.present?
          d = {}
          d[:so] = so
          d[:pure_savers_male]        = @active_members.where("
                                          members.id IN (?) AND 
                                          members.gender = 'Male' AND
                                          centers.officer_in_charge = ? AND
                                          members.status = 'active'
                                        ",  @member_ids_with_positive_savings,
                                            so
                                        ).where.not("members.id IN (?)",
                                          @member_ids_with_active_entry_point_loans
                                        ).count

          d[:pure_savers_female]      = @active_members.where("
                                          members.id IN (?) AND
                                          members.gender = 'Female' AND
                                          centers.officer_in_charge = ? AND
                                          members.status = 'active'
                                        ",  @member_ids_with_positive_savings,
                                            so
                                        ).where.not("members.id IN (?)",
                                          @member_ids_with_active_entry_point_loans
                                        ).count

          d[:pure_savers_others]      = @active_members.where("
                                          members.id IN (?) AND
                                          members.gender = 'Others' AND
                                          centers.officer_in_charge = ? AND
                                          members.status = 'active'
                                        ",  @member_ids_with_positive_savings,
                                            so
                                        ).where.not("members.id IN (?)",
                                          @member_ids_with_active_entry_point_loans
                                        ).count

          d[:pure_savers_total]       = d[:pure_savers_male] + d[:pure_savers_female] + d[:pure_savers_others]

          d[:active_loaners_male]     = @active_members.where("
                                          members.id IN (?) AND
                                          members.gender = 'Male' AND
                                          centers.officer_in_charge = ?
                                        ",  @member_ids_with_active_entry_point_loans,
                                            so
                                        ).count

          d[:active_loaners_female]   = @active_members.where("
                                          members.id IN (?) AND
                                          members.gender = 'Female' AND
                                          centers.officer_in_charge = ?
                                        ",  @member_ids_with_active_entry_point_loans,
                                            so
                                        ).count

          d[:active_loaners_others]   = @active_members.where("
                                          members.id IN (?) AND
                                          members.gender = 'Others' AND
                                          centers.officer_in_charge = ?
                                        ",  @member_ids_with_active_entry_point_loans,
                                            so
                                        ).count

          d[:active_loaners_total]    = d[:active_loaners_male] + d[:active_loaners_female] + d[:active_loaners_others]

          #d[:active_members_male]     = @active_members.where("
           #                               members.id IN (?) AND
            #                              members.gender = 'Male' AND
             #                             centers.officer_in_charge = ?
              #                          ",  @member_ids_active_only,
               #                             so
                #                        ).count
          d[:active_members_male]     = @active_members.where(
                                          "status = 'active' AND
                                           members.gender = 'Male' AND
                                           centers.officer_in_charge = ?
                                          ",so
                                          ).count

          #d[:active_members_female]   = @active_members.where("
           #                               members.id IN (?) AND
            #                              members.gender = 'Female' AND
             #                             centers.officer_in_charge = ?
              #                          ",  @member_ids_active_only,
               #                             so
                #                        ).count

            d[:active_members_female]  = @active_members.where("
                                          members.status = 'active' AND
                                          members.gender = 'Female' AND
                                          centers.officer_in_charge = ?
                
                                        ", so
                                        ).count

          #d[:active_members_others]   = @active_members.where("
           #                               members.id IN (?) AND
            #                              members.gender = 'Others' AND
             #                             centers.officer_in_charge = ?
              #                          ",  @member_ids_active_only,
               #                             so
                #                        ).count
            d[:active_members_others]   = @active_members.where("
                                            members.status = 'active' AND
                                            members.gender = 'Others' AND
                                            centers.officer_in_charge = ?
                                          ", so
                                          ).count

          d[:active_members_total]    = d[:active_members_male] + d[:active_members_female] + d[:active_members_others]

          d[:pending_members_male]    = @pending_members.where(
                                          "centers.officer_in_charge = ? AND gender = 'Male'", so
                                        ).count
          d[:pending_members_female]  = @pending_members.where(
                                          "centers.officer_in_charge = ? AND gender = 'Female'", so
                                        ).count
          d[:pending_members_others]  = @pending_members.where(
                                          "centers.officer_in_charge = ? AND gender = 'Others'", so
                                        ).count

          d[:pending_members_total]   = d[:pending_members_male] + d[:pending_members_female] + d[:pending_members_others]

          d[:total] = d[:pure_savers_total] + d[:active_loaners_total] + d[:active_members_total] + d[:pending_members_total]

          @data[:so_data] << d
        end
      end

      @data
    end
  end
end
