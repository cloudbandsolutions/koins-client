module Reports
	class ActiveLoaners
		def initialize(branch:, year:)
			@branch = branch
			@year = year
		end

		def execute!
			ret = {}
      data_set = []
      records = []
      labels = Date::MONTHNAMES.slice(1, 12)
      voucher_numbers = Loan.active.where(branch_id: @branch.id).pluck(:voucher_reference_number)
      
      labels.each_with_index do |e, i|
        date = DateTime.new(@year, Date::MONTHNAMES.index(e));
        data_set << Voucher.approved.where(reference_number: voucher_numbers, date_prepared: date..date.next_month).count
        records << { month: e, count: data_set[i] }
      end

      data = {
        labels: labels,
        datasets: [
          {
            label: "Active Loaners",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: data_set
          }
        ]
      }

      ret = { data: data, records: records }
		end
	end
end