module Reports
  class FetchSavingsAndJournalEntries 
    def initialize(month:, year:)
      @month = month
      @year = year
      @withdrawals = savings

      @data = {}
      @data[:withdrawals] = SavingsAccountTransaction.approved_debits.where( 
        "extract(month from transacted_at) = ?  
        AND extract(year from transacted_at) = ?", 
        @month, 
        @year
      )

      @data[:total_withdrawals] = @data[:withdrawals].sum(:amount)

      @data[:deposits] = SavingsAccountTransaction.approved_debits.where(
        "extract(month from transacted_at) = ?
         AND extract(year from transacted_at) = ?",
         @month,
         @year
      )

      @data[:total_deposits] = @data[:deposits].sum(:amount)

      @data[:credits] = JournalEntry.approved_credit_entries_by_month_and_year_and_accounting_codes(@month, @year, Settings.savings_accounting_code_ids)
      @data[:total_credits] = @data[:credits].sum(:amount)

      @data[:debits] = JournalEntry.approved_debit_entries_by_month_and_year_and_accounting_codes(@month, @year, Settings.savings_accounting_code_ids)
      @data[:total_debits] = @data[:debits].sum(:amount)
    end

    def execute!
      return @data
    end
  end
end
