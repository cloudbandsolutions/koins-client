module Reports
  class GenerateExcelForMemberRegistry
    def initialize
      @p      = Axlsx::Package.new
      @data   = ::Members::GenerateRegistryOfMembers.new(
                  center: nil
                ).execute!
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          # Headers
          sheet.add_row [
            "Membership Number",
            "Name of Member",
            "Center",
            "Tax Identification Number",
            "Date Accepted",
            "BOD Resolution Number",
            "Regular",
            "Associate",
            "Number of Shares Subscribed",
            "Amount Subscribed",
            "Initial Payment",
            "Street",
            "BRGY",
            "City",
            "Date of Birth",
            "Age",
            "Gender",
            "Civil Status",
            "Highest Educational Attainment",
            "Occupation/Income Source",
            "Number of Dependents",
            "Religious/Social Affiliation",
            "Annual Income",
            "Date",
            "BOD Resolution",
            "Contact Number",
            "SATO"
          ]

          # Content
          @data.each do |member_data|
            sheet.add_row [
              member_data[:membership_number],
              member_data[:name_of_member],
              member_data[:center],
              member_data[:tin_number],
              member_data[:date_accepted],
              member_data[:bod_res_num_acc],
              member_data[:membership_kind_regular],
              member_data[:membership_kind_assoc],
              member_data[:number_of_shares_subs],
              member_data[:amount_subscribed],
              member_data[:initial_payment],
              #member_data[:address],
              member_data[:address_street],
              member_data[:address_barangay],
              member_data[:address_city],
              member_data[:date_of_birth],
              member_data[:age],
              member_data[:gender],
              member_data[:civil_status],
              member_data[:highest_education],
              member_data[:occupation],
              member_data[:num_dependents],
              member_data[:religion],
              member_data[:annual_income],
              member_data[:date],
              "",
              member_data[:mobile_number],
              member_data[:sato]
            ]
          end
        end
      end

      @p
    end
  end
end
