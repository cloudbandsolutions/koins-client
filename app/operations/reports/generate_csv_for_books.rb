module Reports
  class GenerateCSVForBooks
    def initialize(book:, vouchers:, user:)
      @book     = book
      @vouchers = vouchers
      @user     = user
    end

    def book_full_name(book)
      if book == "JVB"
        "General Journal"
      elsif book == "CRB"
        "Cash Receipts"
      elsif book == "CDB"
        "Cash Disbursements"
      end
    end

    def execute!
      CSV.generate do |csv|
        csv <<  []
        csv <<  ["Book: #{book_full_name(@book)}"]
        csv <<  ["#{Settings.company}", "", ""]
        csv <<  ["#{Settings.company_address}"]
        csv <<  ["VAT Registration Tin Number: #{Settings.tin_number}"]
        @vouchers.each do |voucher|
          csv <<  [
            "(#{voucher.id}) #{voucher.book} Reference Number: #{voucher.reference_number.rjust(8, '0') if !voucher.reference_number.nil?}", 
            "", 
            "Date: #{voucher.date_prepared.strftime('%m/%d/%Y') if !voucher.date_prepared.nil?}"
          ]
          csv <<  [
            "Accounting Code", 
            "Debit", 
            "Credit"
          ]

          voucher.journal_entries.order_accounting.each do |journal_entry|
            if journal_entry.post_type == "DR" and journal_entry.amount > 0
              csv <<  ["#{journal_entry.accounting_code.code} - #{journal_entry.accounting_code.name}", journal_entry.amount, ""]
            end
          end

          voucher.journal_entries.order_accounting.each do |journal_entry|
            if journal_entry.post_type == "CR" and journal_entry.amount > 0
              csv <<  ["#{journal_entry.accounting_code.code} - #{journal_entry.accounting_code.name}", "", journal_entry.amount]
            end
          end
          csv <<  ["", "", ""]
          csv <<  ["Particular: #{voucher.particular}", ""]
          csv <<  []
        end
        csv <<  []
        csv <<  ["#{SOFTWARE_NAME} Version #{BUILD_VERSION}", "Date Printed: #{Time.now.strftime("%m/%d/%Y %H:%M")} | #{@user.full_name}"]
      end
    end
  end
end
