module Reports
	class CollectionReports
		def initialize(start_date:, end_date:, branch_id:)
			@branch_id        =  branch_id
      @start_date       =  start_date.to_date
      @end_date         =  end_date.to_date
      
      if @branch_id.present? && @start_date.present? && @end_date.present?
        @insurance_account_transactions = InsuranceAccountTransaction.joins(:insurance_account).where("insurance_accounts.branch_id = ? AND transacted_at >= ? AND transacted_at <= ?", @branch_id, @start_date, @end_date)
        @deposits = InsuranceAccountTransaction.joins(:insurance_account).where("insurance_accounts.branch_id = ? AND transacted_at >= ? AND transacted_at <= ? AND transaction_type = ?", @branch_id, @start_date, @end_date, "deposit") 
        @fund_transfer_deposits = InsuranceAccountTransaction.joins(:insurance_account).where("insurance_accounts.branch_id = ? AND transacted_at >= ? AND transacted_at <= ? AND transaction_type = ?", @branch_id, @start_date, @end_date, "fund_transfer_deposit") 
        @reverse_withdraws = InsuranceAccountTransaction.joins(:insurance_account).where("insurance_accounts.branch_id = ? AND transacted_at >= ? AND transacted_at <= ? AND transaction_type = ?", @branch_id, @start_date, @end_date, "reverse_withdraw")
      end
    end

		def execute!
			@data = {}
      @data[:deposits] = []
      @data[:totals] = []

      @total_deposit = 0.00
      @total_fund_transfer_deposit = 0.00
      @total_reverse_withdraw = 0.00
      
      @deposits.each_with_index do |deposit, i|
        @total_deposit = @total_deposit + deposit.amount
      end

      @fund_transfer_deposits.each_with_index do |fund_transfer_deposit, i|
        @total_fund_transfer_deposit = @total_fund_transfer_deposit + fund_transfer_deposit.amount
      end

      @reverse_withdraws.each_with_index do |reverse_withdraw, i|
        @total_reverse_withdraw = @total_reverse_withdraw + reverse_withdraw.amount
      end

      total = {}
      total[:total_deposit] = @total_deposit
      total[:total_fund_transfer_deposit] = @total_fund_transfer_deposit
      total[:total_reverse_withdraw] = @total_reverse_withdraw
    
      @data[:totals] << total

      @data
		end
	end
end
