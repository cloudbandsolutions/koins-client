module Reports
  class FetchListOfResigns
    def initialize(type_of_resign:, start_date:, end_date:)
      @type_of_resign     = type_of_resign
      @start_date = start_date
      @end_date = end_date
      @resignation_types  = Settings.member_resignation_types
           
      #raise @end_date.inspect

      if @type_of_resign.present?
        @resignation_types.each do |r|
          if r[:name] == @type_of_resign
            @resignation_types = [r]
          end
        end
      end

      @members              = Member.all
      @data                 = {}
      @data[:member_count]  = @members.where("date_resigned >= ? and date_resigned <= ? and status = ?", @start_date, @end_date, "resigned").count
      @data[:resignation_types] = []
      


      #raise @data[:test].inspect
  
      #@data               = []
    end

    def execute!
      @resignation_types.each do |r|
        tmp = {}
        tmp[:test] = @members.where("date_resigned >= ? and date_resigned <= ? and status = ?", @start_date, @end_date, "resigned").count
        tmp[:resignation_type]  = r[:name]
        tmp[:member_count]      = @members.where("resignation_type = ? and date_resigned >= ? and date_resigned <= ?",r[:name], @start_date, @end_date).count
        
        tmp[:member_count] = tmp[:member_count]
        
      
        tmp[:reasons]           = []
  
        r[:particulars].each do |p|
          dep = {}
          dep[:code] = p[:code]
          dep[:name] = p[:name]
          dep[:member_count] = @members.where("resignation_type = ? and resignation_code = ? and date_resigned >= ? and date_resigned <= ? and status = ?",r[:name],p[:code], @start_date, @end_date, "resigned").count

          
            
            dep[:member_count] = dep[:member_count]
          

            #@data[member_total] =           
           tmp[:reasons] << dep
        end
          #@data[:member_total] = @total_resign

        @data[:resignation_types] << tmp
      end
       @data
    end
  end
end
