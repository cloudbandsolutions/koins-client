module Reports
	class PersonalFundsExcel
		def initialize(data:, detailed:)
			@data     = data
      @detailed = detailed
		end	

		def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
          label_cell = wb.styles.add_style b: true, font_name: "Calibri"
          currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right_bold = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
          percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
          left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
          underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
          header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
          date_format_cell = wb.styles.add_style format_code: "mm-dd-yyyy", font_name: "Calibri", alignment: { horizontal: :right }
          default_cell = wb.styles.add_style font_name: "Calibri"
          black_white_date = wb.styles.add_style(:bg_color => "000000", :fg_color => "FFFFFF", format_code: "mm-dd-yyyy")
          blue_white_date = wb.styles.add_style(:bg_color => "0000FF", :fg_color => "FFFFFF", format_code: "mm-dd-yyyy")
          black_white = wb.styles.add_style(:bg_color => "000000", :fg_color => "FFFFFF")
          blue_white = wb.styles.add_style(:bg_color => "0000FF", :fg_color => "FFFFFF")
          green_black = wb.styles.add_style(:bg_color => "00FFFF", :fg_color => "000000")

          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["#{Settings.company}"], style: title_cell
          sheet.add_row ["Personal Funds as of #{@data[:as_of].to_date.strftime('%b %d, %Y')}"], style: title_cell

          if @detailed == "true"
            @data[:branches].each do |branch|
              sheet.add_row []
              sheet.add_row ["Branch: #{branch[:name]}"], style: label_cell
              sheet.add_row []

              branch[:so_list].each do |so|
                sheet.add_row ["Officer: #{so[:so]}"], style: label_cell
                
                so[:centers].each do |center|
                  sheet.add_row ["#{center[:name]}"], style: label_cell
                
                  t_head = []
                  t_head << ""
                  t_head << "Name"
                  t_head << "Status"
                  t_head << "Identification Number"
                  t_head << "Insurance Recognition Date"
                  t_head << "Total"
                  @data[:account_types].each do |account_type|
                    t_head << account_type
                  end
                  sheet.add_row t_head, style: [nil, title_cell, title_cell, title_cell, title_cell, title_cell, title_cell, title_cell, title_cell, title_cell, title_cell]
                    
                    center[:member_accounts].each do |member|
                      member_row = []
                      member_row << "#{center[:name]}"
                      member_row << member[:member]
                      member_row << member[:member_status]
                      member_row << member[:member_identification_number]
                      member_row << member[:member_recognition_date_mii]
                      member_row << member[:total_for_member]
                      member[:accounts].each do |account|
                        member_row << account
                      end
                      
                      sc = EquityType.first

                      if member[:member_status] == "resigned"
                        if member[:accounts].last == 0  && member[:accounts].third > 0 
                          sheet.add_row member_row, style: [black_white, black_white, black_white, black_white, black_white_date, black_white, black_white, black_white, green_black, green_black, green_black]
                        else
                          sheet.add_row member_row, style: [black_white, black_white, black_white, black_white, black_white_date, black_white, black_white, black_white, black_white, black_white, black_white]
                        end
                      elsif member[:member_status] == "archived"
                        sheet.add_row member_row, style: [blue_white, blue_white, blue_white, blue_white, blue_white_date, blue_white, blue_white, blue_white, blue_white, blue_white, blue_white]
                      else 
                        sheet.add_row member_row, style: [default_cell, default_cell, default_cell, default_cell, date_format_cell, currency_cell, currency_cell, currency_cell, currency_cell, currency_cell]
                      end
                    end

                  center_sub_total_row = []
                  center_sub_total_row << ""
                  center_sub_total_row << ""
                  center_sub_total_row << "Total"
                  center_sub_total_row << center[:member_count]
                  center_sub_total_row << center[:total_funds]
                  center[:funds].each do |c|
                     center_sub_total_row << c
                   end

                  sheet.add_row center_sub_total_row, style: [default_cell, label_cell, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
                  
                  sheet.add_row []
                  sheet.add_row []
                end    
                
                  sub_total_row = []
                  sub_total_row << ""
                  sub_total_row << ""
                  sub_total_row << "Total for #{so[:so]}"
                  sub_total_row << so[:member_count]
                  sub_total_row << so[:total_funds]
                  so[:subtotals].each do |s|
                    sub_total_row << s
                  end

                  sheet.add_row sub_total_row, style: [default_cell, label_cell, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]

                sheet.add_row []
                sheet.add_row []
              end

                branch_total_row = []
                branch_total_row << ""
                branch_total_row << ""
                branch_total_row << "#{branch[:name]} Grand Total"
                branch_total_row << branch[:branch_member_total]
                branch_total_row << branch[:branch_total_funds]
                branch[:branch_total_funds_breakdown].each do |t|
                  branch_total_row << t
                end

                sheet.add_row branch_total_row, style: [default_cell, label_cell, label_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
            end
          end
          
          if @detailed == "false"
            @data[:branches].each do |branch|
              sheet.add_row []
              sheet.add_row ["Branch: #{branch[:name]}"], style: label_cell
              sheet.add_row []

              branch[:so_list].each do |so|
                sheet.add_row ["Officer: #{so[:so]}"], style: label_cell
                t_head = []
                t_head << ""
                t_head << "Name"
                t_head << ""
                t_head << "Total"
                @data[:account_types].each do |account_type|
                  t_head << account_type
                end

                sheet.add_row t_head, style: title_cell

                so[:centers].each do |center|
                  center_row = []
                  center_row << center[:name]
                  center_row << center[:member_count]
                  center_row << center[:total_funds]
                  center[:funds].each do |f|
                    center_row << f
                  end

                  sheet.add_row center_row
                end

                sub_total_row = []
                sub_total_row << ""
                sub_total_row << "Total"
                sub_total_row << so[:member_count]
                sub_total_row << so[:total_funds]
                so[:subtotals].each do |s|
                  sub_total_row << s
                end

                sheet.add_row sub_total_row
              end

              total_header = []
              total_header << ""
              total_header << "Name"
              total_header << ""
              total_header << "Total"
              @data[:account_types].each do |t|
                total_header << t
              end

              branch_total_row = []
              branch_total_row << ""
              branch_total_row << "#{branch[:name]} Total"
              branch_total_row << branch[:branch_member_total]
              branch_total_row << branch[:branch_total_funds]
              branch[:branch_total_funds_breakdown].each do |t|
                branch_total_row << t
              end

              sheet.add_row branch_total_row
              sheet.add_row []
            end 
          end

          sheet.add_row []
        end
      end

      p
		end
	end
end
