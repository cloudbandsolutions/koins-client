module Reports
  class AdditionalShares
    def initialize(branch_id:,center_id:,start_date: , end_date:)
      @branches = Branch.select("*")
      @center   = Center.select("*")
      if branch_id.present?
        @branches = @branches.where(id: branch_id)
      end
            @members_re= []
      @data = {}
      @data[:list_of_additional_shares]= []
      @start_date = start_date
      @end_date   = end_date
      if center_id.present?
        @center_id = @center.where(id: center_id).pluck(:id).shift
        @members = Member.joins(:membership_payments, :equity_accounts).where('members.status= ? and membership_payments.membership_type_id=? and membership_payments.paid_at >= ? and membership_payments.paid_at <= ? and equity_accounts.balance= ? and equity_accounts.status= ? and members.center_id = ? and membership_payments.status = ? ','active','1',@start_date,@end_date,'100','active',@center_id, 'paid').order('membership_payments.paid_at ASC').pluck(:identification_number, 'membership_payments.paid_at', 'members.status','equity_accounts.balance','members.center_id')
      
      else
      @members = Member.joins(:membership_payments, :equity_accounts).where('members.status= ? and membership_type_id=? and membership_payments.paid_at >= ? and membership_payments.paid_at <= ? and equity_accounts.balance= ? and equity_accounts.status= ? and membership_payments.status = ? ','active','1',@start_date,@end_date,'100','active','paid').order('membership_payments.paid_at ASC').pluck(:identification_number, 'membership_payments.paid_at', 'members.status','equity_accounts.balance','members.center_id')
    end


   
   end

    def execute!
       
        @members.each do |mem|
          mems=  Member.where(identification_number: mem[0]).ids
            mems.each do |d|
            tp = {}
            tp[:member_name]= Member.find(d).full_name
            center_name= Center.where(id: mem[4]).pluck(:name).shift
            tp[:member_center]= center_name
            tp[:member_id]= mem[0]
            tp[:membership_date]=mem[1]
            tp[:status]=mem[2]
            tp[:balance]=mem[3]
            st_yr= mem[1].year
            et_yr= Time.now.year
            multiplier= (et_yr - st_yr) * 100
            tp[:RASC]= multiplier
            tp[:cbu]= SavingsAccount.where(savings_type_id: 3, member_id: d).pluck(:balance).shift
            tp[:scPayment]= tp[:RASC] - tp[:cbu]
            tp[:savings]= SavingsAccount.where(savings_type_id: 1 ,member_id: d).pluck(:balance).shift
          @members_re << tp
            end
          end

          
          member_records_details = @members_re.group_by { |x| x[:member_center]  }.sort_by { |t, _| t[0] }
            member_records_details.each do |dt, array|
            mdg= {}
            mdg[:center_name]= dt
            mdg[:center_detail_group]= []
              array.each do |a|
                arraylist = {}
                arraylist[:member_name]     = a[:member_name]
                arraylist[:membership_date] = a[:membership_date]
                arraylist[:status]          = a[:status]
                arraylist[:balance]         = a[:balance]
                arraylist[:RASC]            = a[:RASC].round(2)
                arraylist[:scPayment]       = a[:scPayment].round(2)
                arraylist[:cbu]             = a[:cbu].round(2)
                arraylist[:savings]         = a[:savings].round(2)

                mdg[:center_detail_group] << arraylist
               
              end
        

            @data[:list_of_additional_shares] << mdg
            end
          
     @data
    end
  end
end

