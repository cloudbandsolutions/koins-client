module Reports
  class FetchMonthlyRr
    def initialize(year:)
      @year = year
      @data = {}
    end

    def execute!
      @data[:records] = []

      (1..12).each do |month|
        date          = Date.civil(@year, month, -1)
        daily_report  = DailyReport.where(as_of: date).first

        d             = {}
        d[:date]      = date.strftime("%b %d, %Y")

        if daily_report
          if daily_report.repayments['total_principal_rr'].nil?
            d[:rr]  = 0.00
          else
            d[:rr]  = daily_report.repayments['total_principal_rr']
          end
        else
          d[:rr]  = 0.00
        end

        @data[:records] << d
      end

      @data
    end
  end
end
