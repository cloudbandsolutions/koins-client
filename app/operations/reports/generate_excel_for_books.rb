module Reports
  class GenerateExcelForBooks
    def initialize(book:, vouchers:, user:, start_date:, end_date:)
      @book        = book
      @vouchers    = vouchers
      @p           = Axlsx::Package.new
      @user        = user
      @start_date  = start_date
      @end_date    = end_date
    end

    def book_full_name(book)
      if book == "JVB"
        "General Journal"
      elsif book == "CRB"
        "Cash Receipts"
      elsif book == "CDB"
        "Cash Disbursements"
      end
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          initialize_formats!(wb)
          header = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          center = wb.styles.add_style(alignment: {horizontal: :center}) 
          sheet.add_row []
          sheet.add_row []
          sheet.add_row ["", "#{Settings.company}", "", ""], style: @header_cells
          sheet.add_row ["", "#{Settings.company_address}"], style: @header_cells
          sheet.add_row ["", "VAT Registration Tin Number: #{Settings.tin_number}"], style: @header_cells
          sheet.add_row ["", "Transaction Date: #{@start_date} - #{@end_date}"], style: @header_cells
          sheet.add_row []
          sheet.add_row ["", "#{book_full_name(@book)} Book"], style: @header_cells
          sheet.add_row []

          if Settings.activate_microinsurance
            if @book == "JVB"
              sheet.add_row ["Date","JV Number", "#", "Account Title", "Debit", "Credit"], style: [ @title_cell, @title_cell, @title_cell, @title_cell, @title_cell, @title_cell]
            elsif @book == "CRB"
              sheet.add_row ["Date","CRB Number", "OR Number", "Payor", "Account Title", "Debit", "Credit"], style: [ @title_cell, @title_cell, @title_cell, @title_cell, @title_cell, @title_cell, @title_cell]
            elsif @book == "CDB"
              sheet.add_row ["Date","CDB Number", "Check Number",  "Voucher Number", "Payee", "Account Title", "Debit", "Credit"], style: [ @title_cell, @title_cell, @title_cell, @title_cell, @title_cell, @title_cell, @title_cell, @title_cell]
            end  
          end

          @vouchers.each do |voucher|
            # if Settings.activate_microinsurance
            #   if voucher.crb?
            #     sheet.add_row ["Voucher #: #{voucher.sub_reference_number}", "OR #: #{voucher.or_number}", "", ""], style: [@border_left_cell_bold, @border_left_cell_bold, @border_left_cell_bold, @border_left_cell_bold] 
            #   elsif voucher.cdb?
            #     sheet.add_row ["CV #: #{voucher.sub_reference_number}", "Check #: #{voucher.check_number}", "", ""], style: [@border_left_cell_bold, @border_left_cell_bold, @border_left_cell_bold, @border_left_cell_bold]
            #   elsif voucher.jvb?
            #     sheet.add_row ["JV #: #{voucher.sub_reference_number}", "", "", ""], style: [@border_left_cell_bold, @border_left_cell_bold, @border_left_cell_bold, @border_left_cell_bold]
            #   end
            # end

            if Settings.activate_microloans
              sheet.add_row [
                "Date",
                "Account Title", 
                "Debit", 
                "Credit"
              ], style: [
                @border_left_cell_bold,
                @border_left_cell_bold,
                @border_right_cell_bold,
                @border_right_cell_bold
              ]
            end

            date_printed  = false
            num1 = false
            num2 = false
            num3 = false
            num4 = false
            voucher.journal_entries.order_accounting.each_with_index do |journal_entry, i|
              if journal_entry.post_type == "DR" and journal_entry.amount > 0
                d = ""
                if !date_printed and !voucher.date_prepared.nil?
                  d = voucher.date_prepared.strftime('%m/%d/%Y')
                  date_printed  = true
                end

                if Settings.activate_microinsurance
                  n1 = ""
                  n2 = ""
                  n3 = ""
                  n4 = ""
                  if voucher.crb?  
                    if !num1 and !num2 and !voucher.sub_reference_number.nil? and !voucher.or_number.nil?
                      n1 = voucher.sub_reference_number
                      n2 = voucher.or_number
                      n3 = voucher.payee
                      num1 = true
                      num2 = true
                      num3 = true
                    end
                  elsif voucher.cdb?
                    if !num1 and !num2 and !voucher.sub_reference_number.nil? and !voucher.check_number.nil?
                      n1 = voucher.sub_reference_number
                      n2 = voucher.check_number
                      n3 = voucher.check_voucher_number
                      n4 = voucher.payee
                      num1 = true
                      num2 = true
                      num3 = true
                      num4 = true
                    end
                  elsif voucher.jvb?
                    if !num1 and !num2 and !voucher.sub_reference_number.nil?
                      n1 = voucher.sub_reference_number
                      n2 = ""
                      num1 = true
                      num2 = true
                    end
                  end  
                
                  if voucher.crb?
                    sheet.add_row [d, n1, n2, n3, "#{journal_entry.accounting_code.name}", journal_entry.amount, ""], style: [@left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @right_aligned_cell, @right_aligned_cell]
                  elsif voucher.cdb?
                    sheet.add_row [d, n1, n2, n3, n4, "#{journal_entry.accounting_code.name}", journal_entry.amount, ""], style: [@left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @right_aligned_cell, @right_aligned_cell]
                  elsif voucher.jvb?
                    sheet.add_row [d, n1, n2, "#{journal_entry.accounting_code.name}", journal_entry.amount, ""], style: [@left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @right_aligned_cell, @right_aligned_cell]
                  end    
                elsif Settings.activate_microloans
                  sheet.add_row [d, "#{journal_entry.accounting_code.name}", journal_entry.amount, ""], style: [@border_left_cell, @border_left_cell, @border_right_cell, @border_right_cell]
                end
              end
            end

            voucher.journal_entries.order_accounting.each do |journal_entry|
              if journal_entry.post_type == "CR" and journal_entry.amount > 0
                if Settings.activate_microloans
                  sheet.add_row ["", "          #{journal_entry.accounting_code.name}", "", journal_entry.amount], style: [@border_left_cell, @border_left_cell, @border_right_cell, @border_right_cell]
                elsif Settings.activate_microinsurance
                  if voucher.cdb?
                    sheet.add_row ["","","","","", "          #{journal_entry.accounting_code.name}", "", journal_entry.amount], style: [@left_aligned_cell, @left_aligned_cell, @left_aligned_cell, @right_aligned_cell, @right_aligned_cell, @right_aligned_cell, @right_aligned_cell]
                  elsif voucher.crb?
                    sheet.add_row ["","","","", "          #{journal_entry.accounting_code.name}", "", journal_entry.amount], style: [@left_aligned_cell, @left_aligned_cell, @right_aligned_cell, @right_aligned_cell, @right_aligned_cell, @right_aligned_cell]      
                  else  
                    sheet.add_row ["","","", "          #{journal_entry.accounting_code.name}", "", journal_entry.amount], style: [@left_aligned_cell, @left_aligned_cell, @right_aligned_cell, @right_aligned_cell, @right_aligned_cell, @right_aligned_cell]
                  end
                end
              end
            end

            if Settings.activate_microloans
              sheet.add_row ["", "", "", ""], style: @border_center_cell
              sheet.add_row ["", "Particular: #{voucher.particular}", "", ""], style: @border_left_cell, height: 30
              sheet.add_row []
              sheet.add_row []
              sheet.add_row []
            elsif  Settings.activate_microinsurance
              if voucher.cdb?
                sheet.add_row ["","","","", "", "Particular: #{voucher.particular}", "", ""], style: @left_aligned_cell, height: 30
              elsif voucher.crb?
                sheet.add_row ["","","","", "Particular: #{voucher.particular}", "", ""], style: @left_aligned_cell, height: 30 
              else
                sheet.add_row ["","","", "Particular: #{voucher.particular}", "", ""], style: @left_aligned_cell, height: 30
              end
            end  
          end
          
          if Settings.activate_microloans
            sheet.add_row []
            sheet.add_row ["#{SOFTWARE_NAME} Version #{BUILD_VERSION}", "Date Printed: #{Time.now.strftime("%m/%d/%Y %H:%M")} | #{@user.full_name}"]
          elsif Settings.activate_microinsurance
            sheet.add_row ["Date Printed: #{Time.now.strftime("%m/%d/%Y %H:%M")} | #{@user.full_name}"]
          end
        end
      end

      @p
    end

    def initialize_formats!(wb)
      @border_center_cell_bold = wb.styles.add_style font_name: "Calibri", b: true, sz: 11, :border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :center }
      @border_left_cell_bold = wb.styles.add_style font_name: "Calibri", b: true, sz: 11, :border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :left }
      @border_right_cell_bold = wb.styles.add_style font_name: "Calibri", b: true, sz: 11, :border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :right }
      @border_center_cell = wb.styles.add_style font_name: "Calibri", sz: 11, :border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :center }
      @border_left_cell = wb.styles.add_style font_name: "Calibri", sz: 11, :border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :left, wrap_text: true }
      @border_right_cell = wb.styles.add_style font_name: "Calibri", sz: 11, :border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :right }
      @border_currency_cell = wb.styles.add_style font_name: "Calibri", sz: 11, format_code: "#,##0.00",:border => { :style => :thin, :color => "FF000000" }, alignment: { horizontal: :center }
      @title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
      @label_cell = wb.styles.add_style b: true, font_name: "Calibri" 
      @currency_cell_right_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_left_bold = wb.styles.add_style num_fmt: 3, b: true, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :left }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
      @currency_cell_right_total = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
      @percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
      @left_aligned_bold_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri", b: true
      @right_aligned_cell = wb.styles.add_style alignment: { horizontal: :right }, font_name: "Calibri" 
      @underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
      @underline_bold_cell = wb.styles.add_style u: true, font_name: "Calibri", b: true, alignment: { horizontal: :center }
      @header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
      @default_cell = wb.styles.add_style font_name: "Calibri", alignment: { horizontal: :left }, sz: 11
      @center_cell  = wb.styles.add_style alignment: { horizontal: :center }, font_name: "Calibri"
      @header_border = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri", :border => { :style => :thin, :color => "FF000000" }
    end
  end
end
