module Reports
	class GeneratePar
    require 'application_helper'

		def initialize(branch_id:, so:, center_id:, loan_product_id:, as_of: , detailed: false)
			@branch_id        = branch_id
			@so               = so
			@center_id        = center_id
			@loan_product_id  = loan_product_id
			@loan_products    = LoanProduct.all
      @branches         = Branch.all
      @as_of            = as_of.to_date
      @temp_as_of       = @as_of
      @detailed         = detailed
      @current_working_date  = ApplicationHelper.current_working_date

      #if @current_working_date.to_date == @as_of.to_date
      #  @temp_as_of = @current_working_date.to_date - 1.day
      #end
      #if @current_working_date.to_date >= @as_of.to_date
      if @as_of.to_date >= @current_working_date.to_date
        #@temp_as_of =  @as_of.to_date - 1.day
        found_date = false
        @temp_as_of = Utils::DateUtil.new(date: @as_of.to_date).previous_business_day
        while found_date != true do
          if Holiday.where(holiday_at: @temp_as_of).count > 0
            @temp_as_of = Utils::DateUtil.new(date: @temp_as_of).previous_business_day
          else
            found_date = true
          end
        end
      end 

      if @loan_product_id.present?
        @loan_products = LoanProduct.where(id: @loan_product_id)
      end

      if @branch_id.present?
        @branches = Branch.where(id: @branch_id)
      end
		end

		def execute!
      data = {}
      data[:detailed]                               = @detailed
      data[:cluster]                                = Cluster.all.first
      data[:as_of]                                  = @as_of
      data[:company_name]                           = Settings.company_name
      data[:loan_products]                          = []
      data[:num_loans]                              = 0.00
      data[:amount_due]                             = 0.00
      data[:outstanding_balance_of_loans_with_due]  = 0.00
      data[:par_bins]                               = []

      7.times do
        data[:par_bins] << { num_loans: 0, amount_due: 0.00, outstanding_balance_of_loans_with_due: 0.00 }
      end

      @loan_products.each do |loan_product|
        d = {}
        d[:loan_product]                          = loan_product.to_s
        d[:branches]                              = []
        d[:num_loans]                             = 0.00
        d[:amount_due]                            = 0.00
        d[:outstanding_balance_of_loans_with_due] = 0.00
        d[:par_bins]                              = []

        7.times do
          d[:par_bins] << { num_loans: 0, amount_due: 0.00, outstanding_balance_of_loans_with_due: 0.00 }
        end

        @branches.each do |branch|
          so_list = []
          if @so.present?
            so_list << @so
          else
            so_list = Center.where(branch_id: branch.id).pluck(:officer_in_charge).uniq
          end

          dd                                          = {}
          dd[:name]                                   = branch.to_s
          dd[:so_list]                                = []
          dd[:num_loans]                              = 0.00
          dd[:amount_due]                             = 0.00
          dd[:outstanding_balance_of_loans_with_due]  = 0.00
          dd[:par_bins]                               = []

          7.times do
            dd[:par_bins] << { num_loans: 0, amount_due: 0.00, outstanding_balance_of_loans_with_due: 0.00 }
          end

          so_list.each do |so|
            centers = Center.where(officer_in_charge: so)

            if @center_id.present?
              centers = Center.where(id: @center_id)
            end

            ddd                                         = {}
            ddd[:so]                                    = so
            ddd[:centers]                               = []
            ddd[:num_loans]                             = 0.00
            ddd[:amount_due]                            = 0.00
            ddd[:outstanding_balance_of_loans_with_due] = 0.00
            ddd[:par_bins]                              = []

            7.times do
              ddd[:par_bins] << { num_loans: 0, amount_due: 0.00, outstanding_balance_of_loans_with_due: 0.00 }
            end

            centers.each do |center|
              #loans = Loan.active.joins(:member).where(center_id: center.id, loan_product_id: loan_product.id).order("members.last_name")
              loans       = ::Loans::FetchActiveLoansByCenterAndLoanProduct.new(as_of: @as_of, center: center, loan_product: loan_product).execute!
              loan_amount = loans.sum(:amount)

              # 0: 1 - 15
              # 1: 16 - 30
              # 2: 31 - 60
              # 3: 61 - 90
              # 4 91 - 120
              # 5: 121 - 150
              # 6: > 150
              dddd            = {}
              dddd[:name]     = center.to_s
              dddd[:loans]    = []
              dddd[:par_bins] = []

              7.times do
                dddd[:par_bins] << { num_loans: 0, amount_due: 0.00, outstanding_balance_of_loans_with_due: 0.00 }
              end

              dddd[:num_loans]                              = 0.00
              dddd[:amount_due]                             = 0.00
              dddd[:outstanding_balance_of_loans_with_due]  = 0.00

              loans.each do |l|
                loan = {}
                loan[:name] = l.member.full_name_titleize
                loan[:date_released] = l.voucher_date_requested.strftime("%b %d, %Y")


                principal_paid = l.paid_principal_as_of(data[:as_of])
                temp_principal_cum_due = l.temp_total_principal_amount_due_as_of(@temp_as_of)

                #amount_due = unpaid_records.sum("principal") - l.loan_payments.approved.where("paid_at <= ?", data[:as_of]).sum(:paid_principal)
                amount_due = temp_principal_cum_due - principal_paid

                #outstanding_balance_of_loans_with_due = l.principal_balance
                #outstanding_balance_of_loans_with_due = l.principal_balance_as_of(data[:as_of])
                outstanding_balance_of_loans_with_due = 0
                if amount_due > 0
                  outstanding_balance_of_loans_with_due = l.principal_balance_as_of(@temp_as_of)
                end
                #outstanding_balance_of_loans_with_due = l.abs_unpaid_principal_balance_as_of(data[:as_of])
                unpaid_records = AmmortizationScheduleEntry.unpaid.where("loan_id = ? AND due_at <= ? AND is_void IS NULL", l.id, @temp_as_of)

                num_days = unpaid_records.size > 0 ? (@as_of.to_date - unpaid_records.first.due_at) : 0
                if amount_due < 0
                  amount_due = 0
                elsif amount_due > 0
                  #raise "here: #{num_days} #{amount_due.to_f}"
                end


                loan[:par_bins] = []
                7.times do
                  loan[:par_bins] << { num_loans: 0, amount_due: 0.00, outstanding_balance_of_loans_with_due: 0.00 }
                end

                if num_days >= 1 and num_days <= 15
                  dddd[:par_bins][0][:num_loans] += 1
                  dddd[:par_bins][0][:amount_due] += amount_due
                  dddd[:par_bins][0][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due
                  
                  data[:par_bins][0][:num_loans] += 1
                  data[:par_bins][0][:amount_due] += amount_due
                  data[:par_bins][0][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:num_loans] += 1
                  data[:amount_due] += amount_due
                  data[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due


                  d[:par_bins][0][:num_loans] += 1
                  d[:par_bins][0][:amount_due] += amount_due
                  d[:par_bins][0][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  d[:num_loans] += 1
                  d[:amount_due] += amount_due
                  d[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due


                  dd[:par_bins][0][:num_loans] += 1
                  dd[:par_bins][0][:amount_due] += amount_due
                  dd[:par_bins][0][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:num_loans] += 1
                  dd[:amount_due] += amount_due
                  dd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:par_bins][0][:num_loans] += 1
                  ddd[:par_bins][0][:amount_due] += amount_due
                  ddd[:par_bins][0][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:num_loans] += 1
                  ddd[:amount_due] += amount_due
                  ddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due
                  
                  dddd[:num_loans] += 1
                  dddd[:amount_due] += amount_due
                  dddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  loan[:par_bins][0][:num_loans] += 1
                  loan[:par_bins][0][:amount_due] += amount_due
                  loan[:par_bins][0][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due
                elsif num_days > 15 and num_days <= 30
                  dddd[:par_bins][1][:num_loans] += 1
                  dddd[:par_bins][1][:amount_due] += amount_due
                  dddd[:par_bins][1][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:par_bins][1][:num_loans] += 1
                  data[:par_bins][1][:amount_due] += amount_due
                  data[:par_bins][1][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:num_loans] += 1
                  data[:amount_due] += amount_due
                  data[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due


                  d[:par_bins][1][:num_loans] += 1
                  d[:par_bins][1][:amount_due] += amount_due
                  d[:par_bins][1][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  d[:num_loans] += 1
                  d[:amount_due] += amount_due
                  d[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:num_loans] += 1
                  dd[:amount_due] += amount_due
                  dd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:par_bins][1][:num_loans] += 1
                  dd[:par_bins][1][:amount_due] += amount_due
                  dd[:par_bins][1][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:num_loans] += 1
                  ddd[:amount_due] += amount_due
                  ddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:par_bins][1][:num_loans] += 1
                  ddd[:par_bins][1][:amount_due] += amount_due
                  ddd[:par_bins][1][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dddd[:num_loans] += 1
                  dddd[:amount_due] += amount_due
                  dddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  loan[:par_bins][1][:num_loans] += 1
                  loan[:par_bins][1][:amount_due] += amount_due
                  loan[:par_bins][1][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due
                elsif num_days > 30 and num_days <= 60
                  dddd[:par_bins][2][:num_loans] += 1
                  dddd[:par_bins][2][:amount_due] += amount_due
                  dddd[:par_bins][2][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:par_bins][2][:num_loans] += 1
                  data[:par_bins][2][:amount_due] += amount_due
                  data[:par_bins][2][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:num_loans] += 1
                  data[:amount_due] += amount_due
                  data[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due


                  d[:par_bins][2][:num_loans] += 1
                  d[:par_bins][2][:amount_due] += amount_due
                  d[:par_bins][2][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  d[:num_loans] += 1
                  d[:amount_due] += amount_due
                  d[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:num_loans] += 1
                  ddd[:amount_due] += amount_due
                  ddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:par_bins][2][:num_loans] += 1
                  ddd[:par_bins][2][:amount_due] += amount_due
                  ddd[:par_bins][2][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:num_loans] += 1
                  dd[:amount_due] += amount_due
                  dd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:par_bins][2][:num_loans] += 1
                  dd[:par_bins][2][:amount_due] += amount_due
                  dd[:par_bins][2][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dddd[:num_loans] += 1
                  dddd[:amount_due] += amount_due
                  dddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  loan[:par_bins][2][:num_loans] += 1
                  loan[:par_bins][2][:amount_due] += amount_due
                  loan[:par_bins][2][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due
                elsif num_days > 61 and num_days <= 90
                  dddd[:par_bins][3][:num_loans] += 1
                  dddd[:par_bins][3][:amount_due] += amount_due
                  dddd[:par_bins][3][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:par_bins][3][:num_loans] += 1
                  data[:par_bins][3][:amount_due] += amount_due
                  data[:par_bins][3][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:num_loans] += 1
                  data[:amount_due] += amount_due
                  data[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due


                  d[:par_bins][3][:num_loans] += 1
                  d[:par_bins][3][:amount_due] += amount_due
                  d[:par_bins][3][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  d[:num_loans] += 1
                  d[:amount_due] += amount_due
                  d[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:num_loans] += 1
                  ddd[:amount_due] += amount_due
                  ddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:par_bins][3][:num_loans] += 1
                  ddd[:par_bins][3][:amount_due] += amount_due
                  ddd[:par_bins][3][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:num_loans] += 1
                  dd[:amount_due] += amount_due
                  dd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:par_bins][3][:num_loans] += 1
                  dd[:par_bins][3][:amount_due] += amount_due
                  dd[:par_bins][3][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dddd[:num_loans] += 1
                  dddd[:amount_due] += amount_due
                  dddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  loan[:par_bins][3][:num_loans] += 1
                  loan[:par_bins][3][:amount_due] += amount_due
                  loan[:par_bins][3][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due
                elsif num_days > 91 and num_days <= 120
                  dddd[:par_bins][4][:num_loans] += 1
                  dddd[:par_bins][4][:amount_due] += amount_due
                  dddd[:par_bins][4][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:par_bins][4][:num_loans] += 1
                  data[:par_bins][4][:amount_due] += amount_due
                  data[:par_bins][4][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:num_loans] += 1
                  data[:amount_due] += amount_due
                  data[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due


                  d[:par_bins][4][:num_loans] += 1
                  d[:par_bins][4][:amount_due] += amount_due
                  d[:par_bins][4][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  d[:num_loans] += 1
                  d[:amount_due] += amount_due
                  d[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:num_loans] += 1
                  ddd[:amount_due] += amount_due
                  ddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:par_bins][4][:num_loans] += 1
                  ddd[:par_bins][4][:amount_due] += amount_due
                  ddd[:par_bins][4][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:num_loans] += 1
                  dd[:amount_due] += amount_due
                  dd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:par_bins][4][:num_loans] += 1
                  dd[:par_bins][4][:amount_due] += amount_due
                  dd[:par_bins][4][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dddd[:num_loans] += 1
                  dddd[:amount_due] += amount_due
                  dddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  loan[:par_bins][4][:num_loans] += 1
                  loan[:par_bins][4][:amount_due] += amount_due
                  loan[:par_bins][4][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due
                elsif num_days > 120 and num_days <= 150
                  dddd[:par_bins][5][:num_loans] += 1
                  dddd[:par_bins][5][:amount_due] += amount_due
                  dddd[:par_bins][5][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:par_bins][5][:num_loans] += 1
                  data[:par_bins][5][:amount_due] += amount_due
                  data[:par_bins][5][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:num_loans] += 1
                  data[:amount_due] += amount_due
                  data[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due


                  d[:par_bins][5][:num_loans] += 1
                  d[:par_bins][5][:amount_due] += amount_due
                  d[:par_bins][5][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  d[:num_loans] += 1
                  d[:amount_due] += amount_due
                  d[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:num_loans] += 1
                  ddd[:amount_due] += amount_due
                  ddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:par_bins][5][:num_loans] += 1
                  ddd[:par_bins][5][:amount_due] += amount_due
                  ddd[:par_bins][5][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:num_loans] += 1
                  dd[:amount_due] += amount_due
                  dd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:par_bins][5][:num_loans] += 1
                  dd[:par_bins][5][:amount_due] += amount_due
                  dd[:par_bins][5][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dddd[:num_loans] += 1
                  dddd[:amount_due] += amount_due
                  dddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  loan[:par_bins][5][:num_loans] += 1
                  loan[:par_bins][5][:amount_due] += amount_due
                  loan[:par_bins][5][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due
                elsif num_days > 150
                  dddd[:par_bins][6][:num_loans] += 1
                  dddd[:par_bins][6][:amount_due] += amount_due
                  dddd[:par_bins][6][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:par_bins][6][:num_loans] += 1
                  data[:par_bins][6][:amount_due] += amount_due
                  data[:par_bins][6][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  data[:num_loans] += 1
                  data[:amount_due] += amount_due
                  data[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due


                  d[:par_bins][6][:num_loans] += 1
                  d[:par_bins][6][:amount_due] += amount_due
                  d[:par_bins][6][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  d[:num_loans] += 1
                  d[:amount_due] += amount_due
                  d[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:num_loans] += 1
                  ddd[:amount_due] += amount_due
                  ddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  ddd[:par_bins][6][:num_loans] += 1
                  ddd[:par_bins][6][:amount_due] += amount_due
                  ddd[:par_bins][6][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:num_loans] += 1
                  dd[:amount_due] += amount_due
                  dd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dd[:par_bins][6][:num_loans] += 1
                  dd[:par_bins][6][:amount_due] += amount_due
                  dd[:par_bins][6][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  dddd[:num_loans] += 1
                  dddd[:amount_due] += amount_due
                  dddd[:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due

                  loan[:par_bins][6][:num_loans] += 1
                  loan[:par_bins][6][:amount_due] += amount_due
                  loan[:par_bins][6][:outstanding_balance_of_loans_with_due] += outstanding_balance_of_loans_with_due
                end

                dddd[:loans] << loan
              end

              ddd[:centers] << dddd
            end

            dd[:so_list] << ddd
          end

          d[:branches] << dd
        end

        data[:loan_products] << d
      end

      data
		end
	end
end

