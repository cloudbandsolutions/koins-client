module Reports
  class GenerateIncomeStatement
    def initialize(branch:, start_date:, end_date:)
      @branch             = branch
      @start_date         = start_date
      @end_date           = end_date
      @data               = {}
      @data[:company]     = Settings.company
      @data[:branch]      = @branch.name
      @data[:start_date]  = start_date
      @data[:end_date]    = end_date

      @revenue_major_groups = MajorGroup.where(id: Settings.revenue_major_groups)
      @expense_major_groups = MajorGroup.where(id: Settings.expense_major_groups)
    end

    def execute!
      build_revenue!
      build_expense!

      @data
    end

    private

    def build_revenue!
      @data[:revenue_major_groups] = []
      @revenue_major_groups.each do |major_group|
        major_group_data = {}
        major_group_data[:name] = major_group.name
        major_group_data[:code] = major_group.code
        major_group_data[:total] = 0.00
        major_group_data[:major_accounts] = []

        major_group.major_accounts.each do |major_account|
          major_account_data = {}
          major_account_data[:name] = major_account.name
          major_account_data[:code] = major_account.code
          major_account_data[:total] = 0.00
          major_account_data[:mother_accounting_codes] = []

          major_account.mother_accounting_codes.each do |mother_accounting_code|
            mother_accounting_code_data = {}
            mother_accounting_code_data[:name] = mother_accounting_code.name
            mother_accounting_code_data[:code] = mother_accounting_code.code
            mother_accounting_code_data[:total] = 0.00
            mother_accounting_code_data[:accounting_code_categories] = []

            mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
              accounting_code_category_data = {}
              accounting_code_category_data[:name] = accounting_code_category.name
              accounting_code_category_data[:sub_code] = accounting_code_category.sub_code
              accounting_code_category_data[:total] = 0.00
              accounting_code_category_data[:accounting_codes] = []

              accounting_code_category.accounting_codes.each do |accounting_code|
                accounting_code_data = {}
                accounting_code_data[:name] = accounting_code.name
                accounting_code_data[:code] = accounting_code.code

                total_debit   = JournalEntry.joins(:voucher).where("vouchers.status = ? AND journal_entries.post_type = ? AND journal_entries.accounting_code_id = ? AND branch_id = ? AND vouchers.date_prepared BETWEEN ? AND ?", 'approved', 'DR', accounting_code.id, @branch.id, @start_date, @end_date).sum(:amount)
                total_credit  = JournalEntry.joins(:voucher).where("vouchers.status = ? AND journal_entries.post_type = ? AND journal_entries.accounting_code_id = ? AND branch_id = ? AND vouchers.date_prepared BETWEEN ? AND ?", 'approved', 'CR', accounting_code.id, @branch.id, @start_date, @end_date).sum(:amount)
                total = 0.00

                if major_group.dc_code == 'DR'
                  total = total_debit - total_credit
                else
                  total = total_credit - total_debit
                end

                accounting_code_data[:total] = total
                accounting_code_category_data[:total] += total
                mother_accounting_code_data[:total] += total
                major_account_data[:total] += total
                major_group_data[:total] += total

                if total != 0
                  accounting_code_category_data[:accounting_codes] << accounting_code_data
                end
              end

              if accounting_code_category_data[:total] != 0
                mother_accounting_code_data[:accounting_code_categories] << accounting_code_category_data
              end
            end

            if mother_accounting_code_data[:total] != 0
              major_account_data[:mother_accounting_codes] << mother_accounting_code_data
            end
          end

          if major_account_data[:total] != 0
            major_group_data[:major_accounts] << major_account_data
          end
        end

        if major_group_data[:total] != 0
          @data[:revenue_major_groups] << major_group_data
        end
      end
    end

    def build_expense!
      @data[:expense_major_groups] = []
      @expense_major_groups.each do |major_group|
        major_group_data = {}
        major_group_data[:name] = major_group.name
        major_group_data[:code] = major_group.code
        major_group_data[:total] = 0.00
        major_group_data[:major_accounts] = []

        major_group.major_accounts.each do |major_account|
          major_account_data = {}
          major_account_data[:name] = major_account.name
          major_account_data[:code] = major_account.code
          major_account_data[:total] = 0.00
          major_account_data[:mother_accounting_codes] = []

          major_account.mother_accounting_codes.each do |mother_accounting_code|
            mother_accounting_code_data = {}
            mother_accounting_code_data[:name] = mother_accounting_code.name
            mother_accounting_code_data[:code] = mother_accounting_code.code
            mother_accounting_code_data[:total] = 0.00
            mother_accounting_code_data[:accounting_code_categories] = []

            mother_accounting_code.accounting_code_categories.each do |accounting_code_category|
              accounting_code_category_data = {}
              accounting_code_category_data[:name] = accounting_code_category.name
              accounting_code_category_data[:sub_code] = accounting_code_category.sub_code
              accounting_code_category_data[:total] = 0.00
              accounting_code_category_data[:accounting_codes] = []

              accounting_code_category.accounting_codes.each do |accounting_code|
                accounting_code_data = {}
                accounting_code_data[:name] = accounting_code.name
                accounting_code_data[:code] = accounting_code.code

                total_debit   = JournalEntry.joins(:voucher).where("vouchers.status = ? AND journal_entries.post_type = ? AND journal_entries.accounting_code_id = ? AND branch_id = ? AND vouchers.date_prepared BETWEEN ? AND ?", 'approved', 'DR', accounting_code.id, @branch.id, @start_date, @end_date).sum(:amount)
                total_credit  = JournalEntry.joins(:voucher).where("vouchers.status = ? AND journal_entries.post_type = ? AND journal_entries.accounting_code_id = ? AND branch_id = ? AND vouchers.date_prepared BETWEEN ? AND ?", 'approved', 'CR', accounting_code.id, @branch.id, @start_date, @end_date).sum(:amount)
                total = 0.00

                if major_group.dc_code == 'DR'
                  total = total_debit - total_credit
                else
                  total = total_credit - total_debit
                end

                accounting_code_data[:total] = total
                accounting_code_category_data[:total] += total
                mother_accounting_code_data[:total] += total
                major_account_data[:total] += total
                major_group_data[:total] += total

                if total != 0
                  accounting_code_category_data[:accounting_codes] << accounting_code_data
                end
              end

              if accounting_code_category_data[:total] != 0
                mother_accounting_code_data[:accounting_code_categories] << accounting_code_category_data
              end
            end

            if mother_accounting_code_data[:total] != 0
              major_account_data[:mother_accounting_codes] << mother_accounting_code_data
            end
          end

          if major_account_data[:total] != 0
            major_group_data[:major_accounts] << major_account_data
          end
        end

        if major_group_data[:total] != 0
          @data[:expense_major_groups] << major_group_data
        end
      end
    end
  end
end
