module Reports
	class MemberReportsExcel
		def initialize(data:, member_status:, end_date:, start_date:)
			@data     = data
      @start_date = start_date
      @end_date = end_date
      @member_status = member_status
    end	

		def execute!
			p = Axlsx::Package.new
      p.workbook do |wb|
        wb.add_worksheet do |sheet|
          title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
          label_cell = wb.styles.add_style b: true, font_name: "Calibri"
          currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right_bold = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
          percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
          left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
          underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
          header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
          date_format_cell = wb.styles.add_style format_code: "mm-dd-yyyy", font_name: "Calibri", alignment: { horizontal: :right }
          default_cell = wb.styles.add_style font_name: "Calibri"

          sheet.add_row ["", "Member Reports: #{@member_status}"], style: title_cell

          sheet.add_row []
          t_head = []
          t_head << ""
          t_head << "Name"
          t_head << "Recognition Date"
          t_head << "Branch"
          t_head << "Center"
          t_head << "Status"
          t_head << "Insurance Status"
          t_head << "Coverage Value"
          t_head << "LIF"
          t_head << "RF"
          sheet.add_row t_head, style: title_cell

          @data[:records].each_with_index do |record, i|
            t_member = []
            t_member << record[:index]
            t_member << record[:name]
            t_member << record[:recognition_date]
            t_member << record[:branch]
            t_member << record[:center]
            t_member << record[:status]
            t_member << record[:insurance_status]
            t_member << record[:coverage_value]
            t_member << record[:lif]
            t_member << record[:rf]
            sheet.add_row t_member, style: [nil, nil, date_format_cell, nil, nil, nil, nil, currency_cell_right, currency_cell_right, currency_cell_right] 
          end
          @data[:totals].each do |total|
            t_total = []
            t_total << total[:total_member_count]
            t_total << ""
            t_total << ""
            t_total << ""
            t_total << ""
            t_total << ""
            t_total << "Total"
            t_total << total[:total_coverage]
            t_total << total[:total_lif]
            t_total << total[:total_rf]
            sheet.add_row t_total, style: [title_cell, nil, nil, nil, nil, nil, title_cell, currency_cell_right_bold, currency_cell_right_bold, currency_cell_right_bold]
          end
        end
      end

      p
		end
	end
end
