module Reports
	class GenerateRepayments
    require 'application_helper'

		def initialize(branch_id:, so:, center_id:, loan_product_id:, as_of:, detailed: false)
      @current_working_date = ApplicationHelper.current_working_date
			@branch_id            = branch_id
			@center_id            = center_id
      @detailed             = detailed
			@so                   = so
			@loan_product_id      = loan_product_id
			@loan_products        = LoanProduct.all
      @branches             = Branch.all
      @as_of                = as_of.to_date
      @temp_as_of           = @as_of
      


      #if @current_working_date.to_date >= @as_of.to_date
      if @as_of.to_date >= @current_working_date.to_date
        #@temp_as_of =  @as_of.to_date - 1.day
        found_date = false
        @temp_as_of = Utils::DateUtil.new(date: @as_of.to_date).previous_business_day
        while found_date != true do
          if Holiday.where(holiday_at: @temp_as_of).count > 0
            @temp_as_of = Utils::DateUtil.new(date: @temp_as_of).previous_business_day
          else
            found_date = true
          end
        end
      end 

      if @loan_product_id.present?
        @loan_products = LoanProduct.where(id: @loan_product_id)
      end

      if @branch_id.present?
        @branches = Branch.where(id: @branch_id)
      end
		end

		def execute!
      data                  = {}
      data[:detailed]       = @detailed
      data[:cluster]        = @branches.try(:first).try(:cluster).try(:first).try(:to_s)
      data[:as_of]          = @as_of
      data[:company_name]   = Settings.company_name
      data[:loan_products]  = []
      data[:total_loan_amount]                 = 0.00
      data[:total_loan_balance]                = 0.00
      data[:total_interest_balance]            = 0.00
      data[:total_num_loans]                   = 0.00
      data[:total_num_members]                 = 0.00
      data[:total_principal_paid]              = 0.00
      data[:total_temp_principal_paid]         = 0.00    # for at par values
      data[:total_interest_amount]             = 0.00
      data[:total_interest_paid]               = 0.00
      data[:total_total_paid]                  = 0.00
      data[:total_cum_due]                     = 0.00
      data[:total_principal_cum_due]           = 0.00
      data[:total_amt_past_due]                = 0.00
      data[:total_principal_amt_past_due]      = 0.00
      data[:total_rr]                          = 0.00
      data[:total_portfolio]                   = 0.00
      data[:total_par_amount]                  = 0.00
      data[:total_par_rate]                    = 0.00

      @loan_products.each do |loan_product|
        loan_product_data                    = {}
        loan_product_data[:loan_product]     = loan_product.to_s
        loan_product_data[:loan_product_id]  = loan_product.id
        loan_product_data[:branches]         = []
        loan_product_data[:total_loan_amount]                 = 0.00
        loan_product_data[:total_loan_balance]                = 0.00
        loan_product_data[:total_interest_balance]            = 0.00
        loan_product_data[:total_num_loans]                   = 0.00
        loan_product_data[:total_num_members]                 = 0.00
        loan_product_data[:total_principal_paid]              = 0.00
        loan_product_data[:total_temp_principal_paid]         = 0.00    # for at par values
        loan_product_data[:total_interest_amount]             = 0.00
        loan_product_data[:total_interest_paid]               = 0.00
        loan_product_data[:total_total_paid]                  = 0.00
        loan_product_data[:total_cum_due]                     = 0.00
        loan_product_data[:total_principal_cum_due]           = 0.00
        loan_product_data[:total_amt_past_due]                = 0.00
        loan_product_data[:total_principal_amt_past_due]      = 0.00
        loan_product_data[:total_rr]                          = 0.00
        loan_product_data[:total_portfolio]                   = 0.00
        loan_product_data[:total_par_amount]                  = 0.00
        loan_product_data[:total_par_rate]                    = 0.00

        @branches.each do |branch|
          branch_data = {}
          branch_data[:name]                    = branch.to_s
          branch_data[:branch_id] = branch.id
          branch_data[:loan_amount]                       = 0.00
          branch_data[:total_loan_amount]                 = 0.00
          branch_data[:total_loan_balance]                = 0.00
          branch_data[:total_interest_balance]            = 0.00
          branch_data[:total_num_loans]                   = 0.00
          branch_data[:total_num_members]                 = 0.00
          branch_data[:total_principal_paid]              = 0.00
          branch_data[:total_temp_principal_paid]         = 0.00    # for at par values
          branch_data[:total_interest_amount]             = 0.00
          branch_data[:total_interest_paid]               = 0.00
          branch_data[:total_total_paid]                  = 0.00
          branch_data[:total_cum_due]                     = 0.00
          branch_data[:total_principal_cum_due]           = 0.00
          branch_data[:total_amt_past_due]                = 0.00
          branch_data[:total_principal_amt_past_due]      = 0.00
          branch_data[:total_rr]                          = 0.00
          branch_data[:branch_num_members]  = Member.where(
                                                      id: Loan.active.where(
                                                        loan_product_id: loan_product.id, 
                                                        branch_id: branch.id
                                                      ).pluck(:member_id).uniq
                                                    ).count

          so_list = []
          if @so.present?
            so_list << @so
          else
            so_list = Center.where(branch_id: branch.id).pluck(:officer_in_charge).uniq
          end

          branch_data[:so_list] = []
          so_list.each do |so|
            so_data       = {}
            so_data[:so]  = so

            centers = Center.where(officer_in_charge: so)
            if @center_id.present?
              centers = Center.where(id: @center_id)
            end

            so_data[:total_loan_amount]             = 0.00
            so_data[:total_loan_balance]            = 0.00
            so_data[:total_interest_balance]        = 0.00
            so_data[:total_num_loans]               = 0.00
            so_data[:total_num_members]             = 0.00
            so_data[:total_principal_paid]          = 0.00
            so_data[:total_temp_principal_paid]     = 0.00    # for at par values
            so_data[:total_interest_amount]         = 0.00
            so_data[:total_interest_paid]           = 0.00
            so_data[:total_total_paid]              = 0.00
            so_data[:total_cum_due]                 = 0.00
            so_data[:total_principal_cum_due]       = 0.00
            so_data[:total_amt_past_due]            = 0.00
            so_data[:total_principal_amt_past_due]  = 0.00
            so_data[:total_rr]                      = 0.00
            so_data[:centers]                       = []

            centers.each do |center|
              #loans       = Loan.active.joins(:member).where(
              #                "loans.center_id": center.id, 
              #                "loans.loan_product_id": loan_product.id
              #              ).where("loans.date_approved <= ?", @as_of).order("members.last_name")
              #loans       = ::Loans::FetchActiveLoans.new(as_of: @as_of).execute!
              @loans       = ::Loans::FetchActiveLoansByCenterAndLoanProduct.new(as_of: @as_of, center: center, loan_product: loan_product).execute!
              #@loans        = @loans.where("loans.center_id = ? AND loans.loan_product_id = ?", center.id, loan_product.id)
              loan_amount   = @loans.sum(:amount)

              center_data                           = {}
              center_data[:name]                    = center.to_s
              center_data[:loan_amount]             = loan_amount
              center_data[:loan_balance]            = 0.00
              center_data[:interest_balance]        = 0.00
              center_data[:num_loans]               = @loans.count
              center_data[:num_members]             = @loans.pluck(:member_id).uniq.size
              center_data[:principal_paid]          = 0.00
              center_data[:temp_principal_paid]     = 0.00    # for at par values
              center_data[:interest_amount]         = 0.00
              center_data[:interest_paid]           = 0.00
              center_data[:total_paid]              = 0.00
              center_data[:cum_due]                 = 0.00
              center_data[:principal_cum_due]       = 0.00
              center_data[:amt_past_due]            = 0.00
              center_data[:principal_amt_past_due]  = 0.00
              center_data[:rr]                      = 0.00
              center_data[:loans]                   = []

              so_data[:total_loan_amount]     += loan_amount
              so_data[:total_num_loans]       += center_data[:num_loans]
              so_data[:total_num_members]     += center_data[:num_members]

              branch_data[:loan_amount]       += loan_amount
              branch_data[:total_loan_amount] += loan_amount
              branch_data[:total_num_loans]   += center_data[:num_loans]
              branch_data[:total_num_members] += center_data[:num_members]

              loan_product_data[:total_num_loans]   += @loans.count
              loan_product_data[:total_num_members] +=  center_data[:num_members]

              data[:total_num_loans]              += @loans.count
              data[:total_num_members]            += center_data[:num_members]

              c_par_amount = 0
              @loans.each do |loan|
                principal_cum_due           = loan.temp_total_principal_amount_due_as_of(@as_of)      # actual
                temp_loan_cum_due           = loan.temp_total_amount_due_as_of(@temp_as_of)           # adjusted
                principal_amt_past_due      = loan.principal_amt_past_due_as_of(@temp_as_of)          # adjusted
                temp_principal_cum_due      = loan.temp_total_principal_amount_due_as_of(@temp_as_of) # adjusted

                loan_data                           = {}
                loan_data[:name]                    = loan.member.full_name_titleize
                loan_data[:member_id]               = loan.member.id
                loan_data[:loan_id]                 = loan.id
                loan_data[:date_released]           = loan.date_released_formatted
                loan_data[:loan_amount]             = loan.amount
                loan_data[:interest_amount]         = loan.total_interest
                loan_data[:principal_paid]          = loan.paid_principal_as_of(@as_of)
                loan_data[:interest_paid]           = loan.paid_interest_as_of(@as_of)
                loan_data[:cum_due]                 = loan.temp_total_amount_due_as_of(@as_of)
                loan_data[:principal_amt_past_due]  = principal_amt_past_due
                loan_data[:principal_cum_due]       = temp_principal_cum_due
                loan_data[:total_paid]              = loan_data[:principal_paid] + loan_data[:interest_paid]
                loan_data[:amt_past_due]            = (temp_loan_cum_due - loan_data[:total_paid])
                loan_data[:rr]                      = ((loan_data[:total_paid] / loan_data[:cum_due]) * 100).round(2)
                loan_data[:principal_rr]            = ((loan_data[:principal_paid] / loan_data[:principal_cum_due]) * 100).round(2)
                loan_data[:state]                   = "normal"
                loan_data[:loan_balance]            = loan_data[:loan_amount] - loan_data[:principal_paid]
                loan_data[:interest_balance]        = loan_data[:interest_amount] - loan_data[:interest_paid]
                loan_data[:portfolio]               = portfolio(loan)

                # TODO: compute par amount
                unpaid_records = loan.ammortization_schedule_entries.unpaid.where("due_at <= ? AND is_void IS NULL", @temp_as_of)
                num_days = unpaid_records.size > 0 ? (@as_of.to_date - unpaid_records.first.due_at) : 0

                if num_days >= 1
                  c_par_amount          += loan.principal_balance_as_of(@temp_as_of)
                else
                  c_par_amount          += 0
                end
                ##################################
                if loan_data[:amt_past_due] < 0
                  loan_data[:amt_past_due] = 0
                  loan_data[:state]                 = "advanced"
                end

                if loan_data[:amt_past_due] > 0
                  loan_data[:state]                 = "past due"
                end

                # Display state with name
                if ["past due", "advanced"].include?(loan_data[:state])
                  loan_data[:name] = "#{loan_data[:name]} (#{loan_data[:state]})"
                end

                if loan_data[:rr] > 100
                  loan_data[:rr] = 100
                end

                if loan_data[:principal_rr] > 100
                  loan_data[:principal_rr] = 100
                end

                center_data[:loans] << loan_data

                center_data[:principal_paid]          += loan_data[:principal_paid]
                center_data[:interest_amount]         += loan_data[:interest_amount]
                center_data[:interest_paid]           += loan_data[:interest_paid]
                center_data[:total_paid]              += loan_data[:total_paid]
                center_data[:cum_due]                 += loan_data[:cum_due]
                center_data[:amt_past_due]            += loan_data[:amt_past_due]
                center_data[:loan_balance]            += loan_data[:loan_balance]
                center_data[:interest_balance]        += loan_data[:interest_balance]
                center_data[:principal_cum_due]       += temp_principal_cum_due

                so_data[:total_principal_paid]        += loan_data[:principal_paid]
                so_data[:total_interest_amount]       += loan_data[:interest_amount]
                so_data[:total_interest_paid]         += loan_data[:interest_paid]
                so_data[:total_total_paid]            += loan_data[:total_paid]
                so_data[:total_cum_due]               += loan_data[:cum_due]
                so_data[:total_loan_balance]          += loan_data[:loan_balance]
                so_data[:total_interest_balance]      += loan_data[:interest_balance]
                so_data[:total_principal_cum_due]     += temp_principal_cum_due

                branch_data[:total_principal_paid]    += loan_data[:principal_paid]
                branch_data[:total_interest_amount]   += loan_data[:interest_amount]
                branch_data[:total_interest_paid]     += loan_data[:interest_paid]
                branch_data[:total_total_paid]        += loan_data[:total_paid]
                branch_data[:total_cum_due]           += loan_data[:cum_due]
                branch_data[:total_loan_balance]      += loan_data[:loan_balance]
                branch_data[:total_interest_balance]  += loan_data[:interest_balance]
                branch_data[:total_principal_cum_due] += temp_principal_cum_due

                loan_product_data[:total_loan_amount]       += loan.amount
                loan_product_data[:total_principal_paid]    += loan_data[:principal_paid]
                loan_product_data[:total_interest_amount]   += loan_data[:interest_amount]
                loan_product_data[:total_interest_paid]     += loan_data[:interest_paid]
                loan_product_data[:total_total_paid]        += loan_data[:total_paid]
                loan_product_data[:total_cum_due]           += loan_data[:cum_due]
                loan_product_data[:total_loan_balance]      += loan_data[:loan_balance]
                loan_product_data[:total_interest_balance]  += loan_data[:interest_balance]
                loan_product_data[:total_principal_cum_due] += temp_principal_cum_due
                loan_product_data[:total_portfolio]         += loan_data[:portfolio]

                if loan_product_data[:total_portfolio] > 0
                  loan_product_data[:total_par_rate] = ((loan_product_data[:total_par_amount] / loan_product_data[:total_portfolio]) * 100).round(2)
                else
                  loan_product_data[:total_par_rate] = 0.00
                end

                data[:total_loan_amount]       += loan.amount
                data[:total_principal_paid]    += loan_data[:principal_paid]
                data[:total_interest_amount]   += loan_data[:interest_amount]
                data[:total_interest_paid]     += loan_data[:interest_paid]
                data[:total_total_paid]        += loan_data[:total_paid]
                data[:total_cum_due]           += loan_data[:cum_due]
                data[:total_principal_cum_due] += temp_principal_cum_due
                data[:total_loan_balance]      += loan_data[:loan_balance]
                data[:total_interest_balance]  += loan_data[:interest_balance]
                data[:total_portfolio]         += loan_data[:portfolio]

                if data[:total_portfolio] > 0
                  data[:total_par_rate] = ((data[:total_par_amount] / data[:total_portfolio]) * 100).round(2)
                else
                  data[:total_par_rate] = 0.00
                end

                if loan_data[:principal_paid] > temp_principal_cum_due
                  center_data[:temp_principal_paid]             += temp_principal_cum_due
                  so_data[:total_temp_principal_paid]           += temp_principal_cum_due
                  branch_data[:total_temp_principal_paid]       += temp_principal_cum_due
                  loan_product_data[:total_temp_principal_paid] += temp_principal_cum_due
                  data[:total_temp_principal_paid] += temp_principal_cum_due
                else
                  center_data[:temp_principal_paid]             += loan_data[:principal_paid]
                  so_data[:total_temp_principal_paid]           += loan_data[:principal_paid]
                  branch_data[:total_temp_principal_paid]       += loan_data[:principal_paid]
                  loan_product_data[:total_temp_principal_paid] += loan_data[:principal_paid]
                  data[:total_temp_principal_paid] += loan_data[:principal_paid]
                end
              end

              if center_data[:amt_past_due] < 0
                center_data[:amt_past_due] = 0
              end

              center_data[:principal_amt_past_due]  = (center_data[:principal_cum_due] - center_data[:temp_principal_paid])
              center_data[:rr]                      = ((center_data[:total_paid] / center_data[:cum_due]) * 100).round(2)
              center_data[:principal_rr]            = ((center_data[:temp_principal_paid] / center_data[:principal_cum_due]) * 100).round(2)

              if center_data[:principal_amt_past_due] < 0
                center_data[:principal_amt_past_due] = 0
              end

              if center_data[:rr] > 100
                center_data[:rr] = 100
              end

              if center_data[:principal_rr] > 100
                center_data[:principal_rr] = 100
              end

              so_data[:total_amt_past_due]            += center_data[:amt_past_due]
              so_data[:total_principal_amt_past_due]  += center_data[:principal_amt_past_due]

              if center_data[:loan_amount] > 0
                so_data[:centers] << center_data
              end

              branch_data[:total_amt_past_due]            += center_data[:amt_past_due]
              branch_data[:total_principal_amt_past_due]  += center_data[:principal_amt_past_due]

              loan_product_data[:total_amt_past_due]            += center_data[:amt_past_due]
              loan_product_data[:total_principal_amt_past_due]  += center_data[:principal_amt_past_due]

              data[:total_amt_past_due]            += center_data[:amt_past_due]
              data[:total_principal_amt_past_due]  += center_data[:principal_amt_past_due]

              # TODO: Compute Par amount
              loan_product_data[:total_par_amount] += c_par_amount
              data[:total_par_amount] += c_par_amount
              ###############################
            end

            so_data[:total_rr] = ((so_data[:total_total_paid] / so_data[:total_cum_due]) * 100).round(2)
            if so_data[:total_rr] > 100
              so_data[:total_rr] = 100
            end

            branch_data[:total_rr] = ((branch_data[:total_total_paid] / branch_data[:total_cum_due]) * 100).round(2)
            if branch_data[:total_rr] > 100
              branch_data[:total_rr] = 100
            end

            so_data[:total_principal_rr] = ((so_data[:total_temp_principal_paid] / (so_data[:total_principal_cum_due])) * 100).round(2)
            if so_data[:total_principal_rr] > 100
              so_data[:total_principal_rr] = 100
            end

            branch_data[:total_principal_rr] = ((branch_data[:total_temp_principal_paid] / (branch_data[:total_principal_cum_due])) * 100).round(2)
            if branch_data[:total_principal_rr] > 100
              branch_data[:total_principal_rr] = 100
            end

            loan_product_data[:total_principal_rr] = ((loan_product_data[:total_temp_principal_paid] / (loan_product_data[:total_principal_cum_due])) * 100).round(2)
            if loan_product_data[:total_principal_rr] > 100
              loan_product_data[:total_principal_rr] = 100
            end

            data[:total_principal_rr] = ((data[:total_temp_principal_paid] / (data[:total_principal_cum_due])) * 100).round(2)
            if data[:total_principal_rr] > 100
              data[:total_principal_rr] = 100
            end

            if so_data[:total_loan_amount] > 0
              branch_data[:so_list] << so_data
            end
          end

          loan_product_data[:branches] << branch_data
        end

        data[:loan_products] << loan_product_data
      end

      data
		end

    def portfolio(loan)
      principal_due = loan.ammortization_schedule_entries.where("is_void IS NULL").sum(:principal)
      paid_principal = loan.paid_principal_as_of(@as_of)

      principal_due - paid_principal
    end

	end
end
