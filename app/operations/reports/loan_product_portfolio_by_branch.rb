module Reports
	class LoanProductPortfolioByBranch
		def initialize(loan_product:, branch:, year:)
			@loan_product = loan_product
			@branch = branch
			@year = year
		end

		def execute!
			ret = {}
      data_set = []
      records = []
      labels = Date::MONTHNAMES.slice(1, 12)
     
      # Overall branch portfolio
      total = 0
      labels.each_with_index do |e, i|
        month = e
        date = DateTime.new(@year, Date::MONTHNAMES.index(e));
        amount = LoanPayment.approved.joins(:loan).where("loans.loan_product_id = ? AND loans.branch_id = ? AND paid_at >= ? AND paid_at < ?", @loan_product.id, @branch.id, date, date.next_month).sum(:amount).to_f
        data_set << amount
        records << { month: month, amount: amount }
        total += amount
      end

      data = {
        labels: labels,
        datasets: [
          {
            label: "Portfolio for #{@branch.name}: #{@loan_product.name}",
            fillColor: "rgba(151,187,205,0.2)",
            strokeColor: "rgba(151,187,205,1)",
            pointColor: "rgba(151,187,205,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(151,187,205,1)",
            data: data_set
          }
        ]
      }

      ret = { data: data, records: records, total: total }
		end
	end
end