module Reports
  class GenerateAggregatedReport
    def initialize(report_date:)
      @report_date        = report_date
      @data               = {}
      @aggregated_report  = AggregatedReport.new(
                              report_date: @report_date
                            )
    end

    def execute!
      @data[:member_counts] = ::Reports::FetchAggregatedMemberCounts.new(
                                report_date: @report_date
                              ).execute!

      @aggregated_report.data = @data
      @aggregated_report
    end
  end
end
