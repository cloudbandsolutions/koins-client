module Reports
  class GenerateCollectionsClipReportExcel
    def initialize(branch:, start_date:, end_date:)
      @start_date = start_date
      @end_date = end_date
      @branch = branch

      #@members  = Member.pure_active.where("previous_mii_member_since <= ? AND insurance_status != ? AND member_type != ? and branch_id = ?", @end_date, "dormant", "GK", @branch).order("identification_number ASC")
      @members  = Member.where("previous_mii_member_since <= ? AND branch_id = ?", @end_date, @branch).order("identification_number ASC")
 
      @p        = Axlsx::Package.new
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          header  = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
          label_cell = wb.styles.add_style b: true, font_name: "Calibri"
          currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right_bold = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
          percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
          left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
          underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
          header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
          date_format_cell = wb.styles.add_style format_code: "mm-dd-yyyy", font_name: "Calibri", alignment: { horizontal: :right }
          default_cell = wb.styles.add_style font_name: "Calibri"

          sheet.add_row [ 
            "Number",
            "Name of Member",
            "Policy Number",
            "Certificate Number",
            "Sum Assure / Loan Amount",
            "Premium",
            "Premium Tax",
            "Amount Collected",
            "Official Receipt (voucher check number)",
            "OR Date (date of release)",
            "Check Number",
            "Date of Check",
          ], style: header

          @members.each_with_index do |member, index|
            @active_loans = []
            # current_date = @as_of
            # recognition_date = member.try(:previous_mii_member_since).try(:to_date)
            
            # if !recognition_date.nil?  
            #   seconds_between = (current_date.to_time - recognition_date.to_time).abs
            #   days_between = seconds_between / 60 / 60 / 24
            #   number_of_months = (days_between / 30.44).floor
            #   years = (days_between / 365.242199).floor
            #   months = number_of_months - (years * 12)
            #   if months < 3 && years < 1
            #     value = 2000
            #   elsif months >= 3 && years < 1 
            #     value = 6000
            #   elsif years >= 1 && years < 2
            #     value = 10000
            #   elsif years >= 2 && years < 3
            #     value = 30000
            #   elsif years >= 3
            #     value = 50000
            #   end
            # end  

            # insurance_type = InsuranceType.where(code: "LIF").first
            # insurance_account_transactions = InsuranceAccountTransaction.joins(:insurance_account).where("insurance_accounts.insurance_type_id = ? AND insurance_accounts.member_id = ?", insurance_type.id, member.id)
            # life = 0
            # life_amount = 0
            # insurance_account_transactions.where("transaction_date <= ?", @as_of).each do |iat|
            #   if iat.transaction_type == "withdraw" || iat.transaction_type == "reversed" || iat.transaction_type == "fund_transfer_withdraw" || iat.transaction_type == "reverse_deposit"
            #     life = (life_amount - iat.amount).abs
            #   elsif iat.transaction_type == "deposit" || iat.transaction_type == "fund_transfer_deposit" || iat.transaction_type == "reverse_withdraw"
            #     life = (life_amount + iat.amount).abs
            #   end
            #   life_amount = life
            # end
                
            if index == 0
              sheet.add_row [
                  "",
                  member.full_name_titleize,
                  member.identification_number,
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                ], style: [nil, nil, date_format_cell, currency_cell_right, nil, currency_cell_right, nil, currency_cell_right, nil, currency_cell_right, nil]
              else
                sheet.add_row [
                  "",
                  member.full_name_titleize,
                  member.identification_number,
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                  "",
                ], style: [nil, nil, date_format_cell, currency_cell_right, nil, currency_cell_right, nil, currency_cell_right, nil, currency_cell_right, nil]
            end

            loans = member.loans.where("date_approved >= ? AND date_approved <= ?",@start_date, @end_date)
            loans.each do |loan|
              accounting_entry = loan.accounting_entry
              if !accounting_entry.nil?
                clip = accounting_entry.journal_entries.where(accounting_code_id: 99).first
                if !clip.nil?
                  @active_loans << loan
                end
              end
            end


          #   # member.loans.joins(:member).insured.where("date_approved <= ?", @as_of).order("date_prepared ASC").each_with_index do |loan, i|
            @active_loans.each_with_index do |loan, i|  
              if !loan.nil?
                if loan.override_installment_interval == true
                  face_value = loan.original_loan_amount
                  approximated_date_released = (loan.maturity_date - (loan.original_num_installments).weeks).strftime("%B %d, %Y")
                else
                  face_value = loan.amount  
                  approximated_date_released = (loan.maturity_date - (loan.num_installments).weeks).strftime("%B %d, %Y")
                end
                
                lde = loan.loan_product.loan_deduction_entries.where(accounting_code_id: 99).first
                  if lde.present?
                    if lde.is_percent and !lde.use_term_map
                      p = (lde.val / 100).to_f
                      premium = (p * loan.amount)
                    elsif lde.is_percent and lde.use_term_map
                      case loan.num_installments
                        when 15
                          p = lde.t_val_15.to_f
                        when 25
                          p = lde.t_val_25.to_f
                        when 35
                          p = lde.t_val_35.to_f
                        when 50
                          p = lde.t_val_50.to_f
                      else
                        if loan.term == "semi-monthly" || loan.term == "monthly"
                          #p = lde.t_val_15
                          p = lde.t_val_50.to_f
                        else
                          p = lde.t_val_25.to_f
                        end
                      end

                      p = p / 100
                      premium = (p * loan.amount).round(2)
                    elsif lde.is_percent == false and lde.use_term_map == false
                      premium = lde.val.to_f
                    elsif lde.is_percent == false and lde.use_term_map == true
                      case loan.num_installments
                        when 15
                          p = lde.t_val_15.to_f
                        when 16..25
                          p = lde.t_val_25.to_f
                        when 35
                          p = lde.t_val_35.to_f
                        when 50
                          p = lde.t_val_50.to_f
                        else
                          #p = lde.t_val_25
                          if loan.term == "semi-monthly" || loan.term == "monthly"
                          p = lde.t_val_15.to_f
                          else
                          p = lde.t_val_25.to_f
                          end
                        end

                        premium = p
                    else
                      raise "Invalid loan deduction entry"
                    end
                  end
    
                if i == 0
                  sheet.add_row [
                    "",
                    "",
                    "",
                    loan.pn_number,
                    face_value,
                    premium,
                    "",
                    premium,
                    loan.voucher_check_number,
                    loan.date_of_release,
                    loan.bank_check_number,
                    loan.voucher_date_requested,
                  ], style: [nil, nil, nil, nil, currency_cell_right, currency_cell_right, nil, currency_cell_right, nil, nil, nil, date_format_cell,]
                else
                  sheet.add_row [
                    "",
                    "",
                    "",
                    loan.pn_number,
                    face_value,
                    premium,
                    "",
                    premium,
                    loan.voucher_check_number,
                    loan.date_of_release,
                    loan.bank_check_number,
                    loan.voucher_date_requested,
                  ], style: [nil, nil, nil, nil, currency_cell_right, currency_cell_right, nil, currency_cell_right, nil, nil, nil, date_format_cell,]
                end
              end
            end
          end
        end
      end

      @p
    end
  end
end
