module Reports
  class GenerateDailyReport
    require 'application_helper'
    def initialize(as_of:)
      @daily_report       = DailyReport.new
      @as_of              = ApplicationHelper.current_working_date

      if as_of
        @as_of  = as_of
      end

      @daily_report.as_of = @as_of
    end

    def execute!
      puts "Generating so member counts..."
      @daily_report.so_member_counts  = Reports::FetchSoMemberStats.new.execute!

      puts "Generating member stats..."
      @daily_report.member_stats  = Dashboard::MemberCounts.new.execute!

      puts "Generating loan product cycle count stats..."
      @daily_report.loan_product_cycle_count_stats  = Reports::GenerateLoanCycleStats.new.execute!

      puts "Generating repayments..."
      @daily_report.repayments  = Reports::GenerateRepayments.new(
                                    branch_id: nil,
                                    so: nil,
                                    center_id: nil,
                                    loan_product_id: nil,
                                    as_of: @as_of
                                  ).execute!

      puts "Generating par..."
      @daily_report.par                 = Reports::GeneratePar.new(
                                            branch_id: nil,
                                            so: nil,
                                            center_id: nil,
                                            loan_product_id: nil,
                                            as_of: @as_of
                                          ).execute!

      puts "Generating loan product stats..."
      @daily_report.loan_product_stats  = Dashboard::LoanProductStats.new(
                                            as_of: @as_of, 
                                            loan_product_id: nil
                                          ).execute!


      puts "Generating watchlist..."
      @daily_report.watchlist         = Dashboard::GenerateWatchlist.new(
                                          as_of: @as_of
                                        ).execute!

      puts "Generating active loans by loan product"
      @daily_report.active_loans_by_loan_product  = Loans::FetchActiveLoansByProduct.new(
                                                      loan_product_id: nil,
                                                      as_of: @as_of
                                                    ).execute!

      @daily_report.save!

      puts "Done!"
    end
  end
end
