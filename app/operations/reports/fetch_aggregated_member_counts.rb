module Reports
  class FetchAggregatedMemberCounts
    def initialize(report_date:)
      @report_date  = report_date
      @data         = {}
      @members      = Member.where.not(
                        status: ["pending", "archived"]
                      )

      @membership_types     = MembershipType.all
      @possible_loans       = Loan.where.not(status: ["pending", "deleted"]).uniq

      @data[:new_members]                 = {}
      @data[:new_members][:membership_types]  = []
      
      @data[:pure_savers_count] = 0

      # Active loans
      temp_member_ids = []
      @possible_loans.each do |loan|
        if loan.ammortization_schedule_entries.valid.order("due_at ASC").last.due_at <= @report_date
          temp_member_ids << loan.member_id
        end
      end

      temp_member_ids = temp_member_ids.uniq
      temp_members    = Member.where("id IN (?)", temp_member_ids)
      @data[:active_loaners_male]       = temp_members.where(gender: "Male").count
      @data[:active_loaners_female]     = temp_members.where(gender: "Female").count
      @data[:active_loaners_others]     = temp_members.where(gender: "Others").count
      @data[:active_loaners_total]      = @data[:active_loaners_male] + @data[:active_loaners_female] + @data[:active_loaners_others]
      @data[:active_loaners_member_ids] = temp_members.pluck(:id)

      # Pure savers
      temp_member_ids = []
      @possible_loans.each do |loan|
        if loan.ammortization_schedule_entries.valid.order("due_at ASC").last.due_at <= @report_date
          temp_member_ids << loan.member_id
        end
      end

      @data[:pure_savers_total]   = temp_member_ids.uniq.size
      @data[:pure_savers_male]    = @members.where(id: temp_member_ids.uniq, gender: "Male").count
      @data[:pure_savers_female]  = @members.where(id: temp_member_ids.uniq, gender: "Female").count
      @data[:pure_savers_others]  = @members.where(id: temp_member_ids.uniq, gender: "Others").count

      @data[:pure_savers_member_ids] = temp_member_ids

      @data[:active_members_total]  = @data[:active_loaners_total]  + @data[:pure_savers_total]
      @data[:active_members_male]   = @data[:active_loaners_male] + @data[:pure_savers_male]
      @data[:active_members_female] = @data[:active_loaners_female] + @data[:pure_savers_female]
      @data[:active_members_others] = @data[:active_loaners_others] + @data[:pure_savers_others]
    end

    def execute!
      @membership_types.each do |membership_type|
        count = MembershipPayment.paid.where(
                  "membership_type_id = ? AND member_id IN (?) AND paid_at = ?",
                  membership_type.id,
                  @members.pluck(:id),
                  @report_date
                ).count

        @data[:new_members][:membership_types] << {
          membership_type_id: membership_type.id,
          membership_type:    membership_type.to_s,
          count:          count  
        }
      end

      @data
    end
  end
end
