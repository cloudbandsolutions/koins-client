module Reports
  class GenerateListOfMemberForCic
    def initialize(start_date:, end_date:)
      @data ={}
      @start_date = start_date
      @end_date = end_date
      @date_start = start_date
      @member_details = MembershipPayment.where("membership_type_id = ? and paid_at >= ? and paid_at <= ?", 1,@start_date, @end_date)
      active_branch = Member.pluck("branch_id").first
      @data[:branch_code] =  Branch.find(active_branch).code
      @data[:data_list_of_member_info] = []
    end
    def execute!
      @member_details.each do |md|
        member_basic_info ={}
        member_basic_info[:member_id] = md.member.identification_number
        md.member.gender == "Female" ? a = 11 : a = 10
        member_basic_info[:member_gender_title] = a
        member_basic_info[:member_first_name] = md.member.first_name
        member_basic_info[:member_last_name] = md.member.last_name
        member_basic_info[:member_middle_name] = md.member.middle_name
        md.member.gender == "Female" ? b = "F" : b = "M"
        member_basic_info[:member_gender] = b
        member_basic_info[:member_date_of_birth] = md.member.date_of_birth
        
        if md.member.civil_status == "SINGLE" || md.member.civil_status == "May Kinakasama" 
          c = 1
        elsif md.member.civil_status == "Kasal"
          c = 2
        elsif md.member.civil_status == "Hiwalay"
          c = 3
        elsif md.member.civil_status == "Biyudo/a"
          c = 4
        else
          c = 1
        end
        
        member_basic_info[:member_civil_status] = c
        member_basic_info[:member_address] = md.member.address_street + " " + md.member.address_barangay + " " + md.member.address_city  
        
        member_basic_info[:member_tin_number] = 0

        @data[:data_list_of_member_info] << member_basic_info
        
      end
    @data      
    end
  end
end
