module Reports
  class GenerateLoanCycleStats
    def initialize
      @current_date   = ApplicationHelper.current_working_date.to_date
      @loan_products  = LoanProduct.all
      @data           = {}

      @data[:as_of]   = @current_date
      @data[:counts]  = []

      @loans          = Loan.active
      @loan_cycles    = @loans.pluck(:loan_cycle_count).sort.uniq

      @data[:loan_cycles] = @loan_cycles
      @data[:total]       = 0
    end

    def execute!
      @loan_products.each do |loan_product|
        d = {}
        d[:loan_product]  = { id: loan_product.id, code: loan_product.code }
        d[:counts]        = []
        d[:total]         = 0

        @loan_cycles.each do |c|
          dd          = {}
          dd[:cycle]  = c
          dd[:count]  = @loans.where(loan_product_id: loan_product.id, loan_cycle_count: c).count

          if dd[:count] > 0
            d[:total]     +=  dd[:count]
            @data[:total] +=  dd[:count]
            d[:counts]  <<  dd
          end
        end

        if d[:total] > 0
          @data[:counts] << d
        end
      end

      @data
    end
  end
end
