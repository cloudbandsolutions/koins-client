module Reports
  class FetchPpiReports
    def initialize
      @surveyquestion           = SurveyQuestion.where(survey_id:1)
      @surveyquestionoption     = SurveyQuestionOption.all
      @surveyrespondentanswer   = SurveyRespondentAnswer.all
      @data                     = {}
      @data[:surveyquestion]    = []
    end


    def execute!
      @surveyquestion.each do |q|
        tmp = {}
        tmp[:surveyquestionoption] = []

        tmp[:sqId]      = q.id
        tmp[:content]   = q.content

        surveyquestion_option = @surveyquestionoption.where(survey_question_id: q.id)
      
        surveyquestion_option.each do |sqo|

          dep = {}
          dep[:option] = sqo.option
          dep[:rescount] = @surveyrespondentanswer.where(survey_question_option_id: sqo.id).count
          tmp[:surveyquestionoption] << dep

        end

        @data[:surveyquestion] << tmp
    
      end


      @data
     end
  
    
  end 
end
