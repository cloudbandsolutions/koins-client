module Reports
  class FetchListOfResignDetails
    def initialize(resignation_type:, resignation_reason:, start_date:, end_date:)
      #@list_of_member       = Member.resigned
      @resignation_type     = resignation_type
      @resignation_reason   = resignation_reason
      @start_date           = start_date
      @end_date             = end_date

      @list_of_member = Member.where("resignation_type = ? and resignation_reason = ? and date_resigned >= ? and date_resigned <= ? ", @resignation_type, @resignation_reason, @start_date, @end_date).order(:date_resigned)

      @data = []

    end
    def execute!
      @list_of_member.each do |lm|
        tmp = {}
        tmp[:member_id] = lm[:id]
        tmp[:lastname] = lm[:last_name]
        tmp[:firstname] = lm[:first_name]
        tmp[:middlename] = lm[:middle_name]
        tmp[:center_name] = Center.find(lm[:center_id]).name 
        tmp[:date_resigned] = lm[:date_resigned]
        #tmp[:loan_product] = Loan.where(member_id: lm[:id], loan_product_id: 14).order("date_prepared ASC").last.loan_product.name
        x = Loan.joins(:loan_product).where("member_id = ? and loan_products.is_entry_point = 'true'" , lm[:id])
        if x.count == 0
          tmp[:loan_product] = 'No Entry Level Record'
        else
          tmp[:loan_product] = Loan.joins(:loan_product).where("member_id = ? and loan_products.is_entry_point = 'true'" , lm[:id]).order("date_prepared ASC").last.loan_product.name
          tmp[:loan_cycle] = Loan.joins(:loan_product).where("member_id = ? and loan_products.is_entry_point = 'true'" , lm[:id]).order("date_prepared ASC").last.loan_cycle_count
        end
        #tmp[:loan_cycle] = Loan.where(member_id: lm[:id], loan_product_id: 14).order("date_prepared ASC").last.loan_cycle_count
        
        #raise tmp[:center_name].inspect
        @data << tmp
      end
      @data

    end
  end
end
