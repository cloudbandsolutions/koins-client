module Reports
  class GenerateMonthlyDailyReport
    def initialize(year:)
      @year   = year
    end

    def execute!
      (1..12).each do |month|
        as_of = Date.civil(@year, month, -1)
        ::Reports::GenerateDailyReport.new(as_of: as_of).execute!
      end
    end
  end
end
