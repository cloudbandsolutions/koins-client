module Reports
  class FetchListOfProjectCategory
    def initialize
      @data = {}
      @data[:list] = []
      @projectCategory = ProjectTypeCategory.all 
    end

    def execute!
       @projectCategory.each do |pc|
          tmp = {}
          tmp[:projCateg] = pc.name
          tmp[:projTypeDetails] = []
          ptDetails = ProjectType.where(project_type_category_id: pc.id)
          ptDetails.each do |pt|
            tmpDetails = {}
            tmpDetails[:loanDetails] = []
            loan_count = Loan.where(project_type_id: pt.id, status: "active", loan_product_id: 14).count
            tmpDetails[:projType]      =  pt.name
            tmpDetails[:projTypeCount] =  loan_count
            
            loan_details = Loan.where(project_type_id: pt.id, status: "active", loan_product_id: 14)

            loan_details.each do |ld|
              ldDetails = {}
              ldDetails[:member_name] = ld.member.full_name
              ldDetails[:total_principal] = ld.amount
              tmpDetails[:loanDetails] << ldDetails 
            end
            tmpDetails[:totalInLoan] = tmpDetails[:loanDetails].sum{ |x| x[:total_principal].to_i }





            tmp[:projTypeDetails] << tmpDetails
          end
          tmp[:totalInCateg] = tmp[:projTypeDetails].sum{ |x| x[:projTypeCount].to_i }
          
          @data[:list] << tmp
          
        end
      @data
  

    end
  
  end
end
