module Reports
  class GenerateExpensesStatementOfAssets
    def initialize(branch_id:, center_id:, start_date:, end_date:, loan_product_id:)
      @branches       = Branch.select("*")
      @centers        = Center.select("*")
      @start_date     = start_date
      @end_date       = end_date

      if loan_product_id.present?
        @loan_product = LoanProduct.find(loan_product_id)
      end

      if branch_id.present?
        @branches = @branches.where(id: branch_id)
      end

      if center_id.present?
        @centers = @centers.where(id: center_id)
      end

      @data = {}
      @data[:start_date] = @start_date
      @data[:end_date] = @end_date

      #@loans = Loan.expenses.where(date_prepared: @start_date..@end_date)
      @loans = Loan.expenses.where("date_approved >= ? AND date_approved <= ?", @start_date, @end_date)

      if @loan_product
        @loans = @loans.where(loan_product_id: @loan_product.id)
      end

      @loans = @loans.joins(:member).order("members.last_name")
    end

    def execute!
      @data[:total_amount] = @loans.sum(:amount)
      @data[:records] = []

      @loans.each_with_index do |loan, i|
        record = {}
        record[:index] = i+1
        record[:name] = loan.member.full_name_titleize
        record[:amount] = loan.amount
        record[:date] = loan.date_approved_formatted
        record[:check_number] = loan.bank_check_number
        record[:name_of_person_in_check] = loan.member.to_s.upcase

        @data[:records] << record
      end

      @data
    end

    private
  end
end
