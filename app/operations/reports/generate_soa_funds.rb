module Reports
  class GenerateSoaFunds
    def initialize(center_id:, start_date:, end_date:, account_codes:)
      
#      raise center_id.inspect

      if end_date.blank?
        end_date = ApplicationHelper.current_working_date.to_date
      end

      if start_date.blank?
        start_date = (end_date - 1.month).to_date
      end

      @centers          = Center.select("*")

      if center_id.present?
        @centers = @centers.where(id: center_id)
      end

      @start_date       = start_date.to_date.strftime("%Y-%m-%d")
      @end_date         = end_date.to_date.strftime("%Y-%m-%d")
      @account_codes    = account_codes ||= Settings.account_codes

      @savings_account_codes    = SavingsType.all.pluck(:code)
      @insurance_account_codes  = InsuranceType.all.pluck(:code)
      @equity_account_codes     = EquityType.all.pluck(:code)
      @t_ids                    = []
    end

    def execute!
      transaction_array               = []
      s_date  = Date.parse(@start_date).to_time.to_date
      e_date  = Date.parse(@end_date).to_time.to_date

#      savings_account_transactions    = SavingsAccountTransaction
#                                          .approved
#                                          .joins(savings_account: [:savings_type, :member])
#                                          .where("savings_account_transactions.transacted_at AT TIME ZONE 'UTC' >= ?
#                                                  AND savings_account_transactions.transacted_at AT TIME ZONE 'UTC' <= ?
#                                                  AND members.status IN (?)
#                                                  AND members.center_id IN (?)",
#                                                  s_date,
#                                                  e_date,
#                                                  ['active', 'resigned'],
#                                                  @centers.pluck(:id)
#                                          ).order("members.last_name ASC")
#                                          .pluck(
#                                            "members.id, 
#                                            members.first_name, 
#                                            members.last_name, 
#                                            savings_types.code,
#                                            savings_account_transactions.amount,
#                                            savings_account_transactions.transaction_type,
#                                            savings_account_transactions.transacted_at,
#                                            savings_acco"
#                                          )

                                          
      savings_account_transactions    = SavingsAccountTransaction
                                          .approved
                                          .joins(savings_account: [:savings_type, :member])
                                          .where("savings_account_transactions.transaction_date >= ?
                                                  AND savings_account_transactions.transaction_date <= ?
                                                  AND members.status IN (?)
                                                  AND members.center_id IN (?)",
                                                  s_date,
                                                  e_date,
                                                  ['active', 'resigned'],
                                                  @centers.pluck(:id)
                                          ).order("members.last_name ASC")
                                          .pluck(
                                            "members.id, 
                                            members.first_name, 
                                            members.last_name, 
                                            savings_types.code,
                                            savings_account_transactions.amount,
                                            savings_account_transactions.transaction_type,
                                            savings_account_transactions.transacted_at,
                                            savings_account_transactions.id"
                                          )

      if (@account_codes & @savings_account_codes).size > 0
        transaction_array.push(*savings_account_transactions)
      end

      insurance_account_transactions    = InsuranceAccountTransaction
                                          .approved
                                          .joins(insurance_account: [:insurance_type, :member])
                                          .where("date(insurance_account_transactions.transaction_date) >= ?
                                                  AND date(insurance_account_transactions.transaction_date) <= ?
                                                  AND members.status IN (?)
                                                  AND members.center_id IN (?)",
                                                  s_date,
                                                  e_date,
                                                  ['active', 'resigned'],
                                                  @centers.pluck(:id)
                                          ).pluck(
                                            "members.id, 
                                            members.first_name, 
                                            members.last_name, 
                                            insurance_types.code,
                                            insurance_account_transactions.amount,
                                            insurance_account_transactions.transaction_type,
                                            insurance_account_transactions.transacted_at,
                                            insurance_account_transactions.id"
                                          )

      if (@account_codes & @insurance_account_codes).size > 0
        transaction_array.push(*insurance_account_transactions)
      end

      equity_account_transactions    = EquityAccountTransaction
                                          .approved
                                          .joins(equity_account: [:equity_type, :member])
                                          .where("equity_account_transactions.transacted_at AT TIME ZONE 'UTC' >= ?
                                                  AND equity_account_transactions.transacted_at AT TIME ZONE 'UTC' <= ?
                                                  AND members.status IN (?)
                                                  AND members.center_id IN (?)",
                                                  s_date,
                                                  e_date,
                                                  ['active'],
                                                  @centers.pluck(:id)
                                          ).pluck(
                                            "members.id, 
                                            members.first_name, 
                                            members.last_name, 
                                            equity_types.code,
                                            equity_account_transactions.amount,
                                            equity_account_transactions.transaction_type,
                                            equity_account_transactions.transacted_at,
                                            equity_account_transactions.id"
                                          )

      if (@account_codes & @equity_account_codes).size > 0
        transaction_array.push(*equity_account_transactions)
      end

      transaction_hashes  = transaction_array.map{ |o| { member_id: o[0], first_name: o[1], last_name: o[2], account_code: o[3], amount: o[4].to_f, transaction_type: o[5], date: o[6].to_date, t_id: o[7] } }

      members = transaction_hashes.map{ |h| { first_name: h[:first_name], last_name: h[:last_name], id: h[:member_id] } }.uniq

      grouped_transactions  = []
      members.each do |member|
        data  = {
          member: member,
          transactions: [],
          t_ids:  []
        }

        transactions_of_member  = transaction_hashes.select { |t| t[:member_id] == member[:id] }
        dates = (transactions_of_member.map { |h| h.except(:member_id, :first_name, :last_name, :account_code, :amount, :transaction_type) }).uniq.sort_by { |o| o[:date] }

        transactions = []
        dates.each do |date|
          d = {}
          d[:date]    = date[:date]
          d[:records] = []
          d[:t_ids]   = []
          @account_codes.each do |account_code|
            temp_record = {}
            temp_record[:account_code] = account_code
            temp_record[:dr_amount] = 0.00
            temp_record[:cr_amount] = 0.00
            records_array = (transactions_of_member.select { |t| t[:date] == date[:date] and t[:account_code] == account_code }).map { |h| h.except(:member_id, :first_name, :last_name, :date, :account_code) }
            records_array.each do |r|
              if !data[:t_ids].include? r[:t_id]
                if ["withdraw", "fund_transfer_withdraw", "reverse_deposit", "wp"].include? r[:transaction_type]
                  temp_record[:dr_amount] += r[:amount]
                else
                  temp_record[:cr_amount] += r[:amount]
                end

                data[:t_ids] << r[:t_id]
                d[:t_ids] <<  r[:t_id]
                @t_ids << r[:t_id]
              end
            end

            d[:records] << temp_record
          end

          if d[:t_ids].size > 0
            transactions << d
          end
        end

        data[:transactions] = transactions
        grouped_transactions << data
      end

      grouped_transactions
    end
  end
end
