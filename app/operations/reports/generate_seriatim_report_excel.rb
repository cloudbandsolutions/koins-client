module Reports
  class GenerateSeriatimReportExcel
    def initialize(as_of:, branch:)
      @as_of = as_of
      @branch = branch

      if !@as_of.nil? && !@branch.nil?
        @members  = Member.pure_active.where("previous_mii_member_since <= ? AND insurance_status != ? AND member_type != ? AND branch_id = ?", @as_of, "dormant", "GK", @branch).order("identification_number ASC")
      end

      @p        = Axlsx::Package.new
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          header  = wb.styles.add_style(alignment: {horizontal: :left}, b: true)
          title_cell = wb.styles.add_style alignment: { horizontal: :center }, b: true, font_name: "Calibri"
          label_cell = wb.styles.add_style b: true, font_name: "Calibri"
          currency_cell = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri"
          currency_cell_right_bold = wb.styles.add_style num_fmt: 3, alignment: { horizontal: :right }, format_code: "#,##0.00", font_name: "Calibri", b: true
          percent_cell = wb.styles.add_style num_fmt: 9, alignment: { horizontal: :left }, font_name: "Calibri"
          left_aligned_cell = wb.styles.add_style alignment: { horizontal: :left }, font_name: "Calibri"
          underline_cell = wb.styles.add_style u: true, font_name: "Calibri"
          header_cells = wb.styles.add_style b: true, alignment: { horizontal: :center }, font_name: "Calibri"
          date_format_cell = wb.styles.add_style format_code: "mm-dd-yyyy", font_name: "Calibri", alignment: { horizontal: :right }
          default_cell = wb.styles.add_style font_name: "Calibri"

          sheet.add_row [ 
            "Certificate Number",
            "Name of Member",
            "Date of Membership",
            "Basic Benefit",
            "Mode of Contribution (weekly/monthly)",
            "Contribution per week/month",
            "Member's contribution due & uncollected",
            "Total accumulated contributions (LIFE)",
            "Interest on equity value, if any",
            "Total Equity Value",
            "Rerserves",
            "Policy Number",
            "Policy/Effectivity Date",
            "Face Amount",
            "Modal Premium (annual,semi-annual,quarterly,monthly)",
            "Net Premiums due & uncollected",
            "Last due date",
            "Cash Value (Premium)",
            "Rerserves",
            "Loan maturity date",
            "Loan num of num installments",
            # "Dependent Name",
            # "Relationship to Member",
            # "Rerserves",
            # "Date of birth",
            # "Age"
          ], style: header

          @members.each_with_index do |member, index|
            @active_loans = []
            current_date = @as_of
            recognition_date = member.try(:previous_mii_member_since).try(:to_date)
            
            if !recognition_date.nil?  
              seconds_between = (current_date.to_time - recognition_date.to_time).abs
              days_between = seconds_between / 60 / 60 / 24
              number_of_months = (days_between / 30.44).floor
              years = (days_between / 365.242199).floor
              months = number_of_months - (years * 12)
              if months < 3 && years < 1
                value = 2000
              elsif months >= 3 && years < 1 
                value = 6000
              elsif years >= 1 && years < 2
                value = 10000
              elsif years >= 2 && years < 3
                value = 30000
              elsif years >= 3
                value = 50000
              end
            end  

            insurance_type = InsuranceType.where(code: "LIF").first
            insurance_account_transactions = InsuranceAccountTransaction.joins(:insurance_account).where("insurance_accounts.insurance_type_id = ? AND insurance_accounts.member_id = ?", insurance_type.id, member.id)
            life = 0
            life_amount = 0
            insurance_account_transactions.where("transaction_date <= ?", @as_of).each do |iat|
              if iat.transaction_type == "withdraw" || iat.transaction_type == "reversed" || iat.transaction_type == "fund_transfer_withdraw" || iat.transaction_type == "reverse_deposit"
                #life = (life_amount - iat.amount).abs
                life = ((life_amount < 0 ? 0 : life_amount) - (iat.amount < 0 ? 0 : iat.amount))
              elsif iat.transaction_type == "deposit" || iat.transaction_type == "fund_transfer_deposit" || iat.transaction_type == "reverse_withdraw"
                life = (life_amount + iat.amount).abs
              end
              life_amount = life
            end
                
            if index == 0
              sheet.add_row [
                  member.identification_number,
                  member.full_name_titleize,
                  member.try(:previous_mii_member_since).try(:to_date),
                  value,
                  "weekly",
                  20,
                  "",
                  life_amount,
                  "",
                  life_amount/2,
                  "",
                ], style: [nil, nil, date_format_cell, currency_cell_right, nil, currency_cell_right, nil, currency_cell_right, nil, currency_cell_right, nil]
              else
                sheet.add_row [
                  member.identification_number,
                  member.full_name_titleize,
                  member.try(:previous_mii_member_since).try(:to_date),
                  value,
                  "weekly",
                  20,
                  "",
                  life_amount,
                  "",
                  life_amount/2,
                  "",
                ], style: [nil, nil, date_format_cell, currency_cell_right, nil, currency_cell_right, nil, currency_cell_right, nil, currency_cell_right, nil]
            end

            loans = member.loans.where("status = ? AND date_approved <= ?","active", @as_of)
            loans.each do |loan|
              accounting_entry = loan.accounting_entry
              if !accounting_entry.nil?
                clip = accounting_entry.journal_entries.where(accounting_code_id: 99).first
                if !clip.nil?
                  @active_loans << loan
                end
              end
            end


            # member.loans.joins(:member).insured.where("date_approved <= ?", @as_of).order("date_prepared ASC").each_with_index do |loan, i|
            @active_loans.each_with_index do |loan, i|  
              if !loan.nil?
                if loan.override_installment_interval == true
                  face_value = loan.original_loan_amount
                  approximated_date_released = (loan.maturity_date - (loan.original_num_installments).weeks).strftime("%B %d, %Y")
                else
                  face_value = loan.amount  
                  approximated_date_released = (loan.maturity_date - (loan.num_installments).weeks).strftime("%B %d, %Y")
                end
                
                lde = loan.loan_product.loan_deduction_entries.where(accounting_code_id: 99).first
                  if lde.present?
                    if lde.is_percent and !lde.use_term_map
                      p = (lde.val / 100).to_f
                      premium = (p * loan.amount)
                    elsif lde.is_percent and lde.use_term_map
                      case loan.num_installments
                        when 15
                          p = lde.t_val_15.to_f
                        when 25
                          p = lde.t_val_25.to_f
                        when 35
                          p = lde.t_val_35.to_f
                        when 50
                          p = lde.t_val_50.to_f
                      else
                        if loan.term == "semi-monthly" || loan.term == "monthly"
                          #p = lde.t_val_15
                          p = lde.t_val_50.to_f
                        else
                          p = lde.t_val_25.to_f
                        end
                      end

                      p = p / 100
                      premium = (p * loan.amount).round(2)
                    elsif lde.is_percent == false and lde.use_term_map == false
                      premium = lde.val.to_f
                    elsif lde.is_percent == false and lde.use_term_map == true
                      case loan.num_installments
                        when 15
                          p = lde.t_val_15.to_f
                        when 16..25
                          p = lde.t_val_25.to_f
                        when 35
                          p = lde.t_val_35.to_f
                        when 50
                          p = lde.t_val_50.to_f
                        else
                          #p = lde.t_val_25
                          if loan.term == "semi-monthly" || loan.term == "monthly"
                          p = lde.t_val_15.to_f
                          else
                          p = lde.t_val_25.to_f
                          end
                        end

                        premium = p
                    else
                      raise "Invalid loan deduction entry"
                    end
                  end
    
                if i == 0
                  sheet.add_row [
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    loan.pn_number,
                    loan.date_approved,
                    face_value,
                    loan.term,
                    "",
                    "",
                    premium,
                    "",
                    loan.maturity_date,
                    loan.num_installments,
                  ], style: [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,  date_format_cell, currency_cell_right, nil, nil, nil, currency_cell_right, nil, date_format_cell, nil]
                else
                  sheet.add_row [
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    loan.pn_number,
                    loan.date_approved,
                    face_value,
                    loan.term,
                    "",
                    "",
                    premium,
                    "",
                    loan.maturity_date,
                    loan.num_installments,
                  ], style: [nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil, nil,  date_format_cell, currency_cell_right, nil, nil, nil, currency_cell_right, nil, date_format_cell, nil]
                end
              end
            end
            
            # if member.spouse_first_name.present?
            #   sheet.add_row [
            #     "",
            #     "",
            #     "",
            #     "",
            #     "",
            #     "",
            #     "",
            #     "",
            #     "",
            #     "",
            #     "",
            #     "",
            #     "",
            #     member.spouse_full_name_titleize,
            #     "Spouse",
            #     "",
            #     member.try(:spouse_date_of_birth),
            #     member.spouse_age
            #    ]   
            # end

            # member.legal_dependents.order("date_of_birth ASC").each_with_index do |legal_dependent, i|
            #   if i == 0
            #     sheet.add_row [
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       legal_dependent.full_name_titleize,
            #       "Child",
            #       "",
            #       legal_dependent.try(:date_of_birth),
            #       legal_dependent.age
            #     ]
            #   else
            #     sheet.add_row [
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       "",
            #       legal_dependent.full_name_titleize,
            #       "Child",
            #       "",
            #       legal_dependent.try(:date_of_birth),
            #       legal_dependent.age
            #     ]
            #   end
            # end
          end
        end
      end

      @p
    end
  end
end
