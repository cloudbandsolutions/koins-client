module Reports
  class GenerateFundsStatementOfAccount
    def initialize(branch_id:, center_id:, start_date:, end_date:)
      @branches       = Branch.select("*")
      @centers        = Center.select("*")
      @start_date     = Date.parse(start_date)
      @end_date       = Date.parse(end_date)
      @savings_types  = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types = EquityType.all
      @total_counter  = @savings_types.size + @insurance_types.size + @equity_types.size
      #@valid_reference_numbers = PaymentCollection.approved.where("created_at >= ? AND created_at <= ?", @start_date, @end_date).pluck(:reference_number)

      @total_cols = @savings_types.count + @insurance_types.count + @equity_types.count

      if branch_id.present?
        @branches = @branches.where(id: branch_id)
      end

      if center_id.present?
        @centers = @centers.where(id: center_id)
      end

      @data = {}
      @data[:start_date] = @start_date
      @data[:end_date] = @end_date
      @data[:account_codes] = @savings_types.pluck(:code) + @insurance_types.pluck(:code) + @equity_types.pluck(:code)
    end

    def execute!
      @data[:records] = []

      #savings_account_transactions = SavingsAccountTransaction.approved.where(
      #                                "voucher_reference_number IN (?) AND transaction_type IN (?)", 
      #                                @valid_reference_numbers, ["deposit", "withdraw", "wp"])
      savings_account_transactions = SavingsAccountTransaction.approved
                                      
      #insurance_account_transactions = InsuranceAccountTransaction.approved.where(
      #                                  "voucher_reference_number IN (?) AND transaction_type IN (?)",
      #                                  @valid_reference_numbers, ["deposit", "withdraw", "wp"])
      insurance_account_transactions = InsuranceAccountTransaction.approved

      #equity_account_transactions = EquityAccountTransaction.approved.where(
      #                                "voucher_reference_number IN (?) AND transaction_type IN (?)",
      #                                @valid_reference_numbers, ["deposit", "withdraw", "wp"])
      equity_account_transactions = EquityAccountTransaction.approved

     
      members = Member.active.where(center_id: @centers.pluck(:id)).order("members.last_name ASC")

      @centers.each do |center|
        record = {}
        record[:center] = center.to_s
        record[:member_records] = []

        members.where(center_id: center.id).each do |member|
          member_record = {}
          member_record[:member] = member.full_name_titleize
          member_record[:identification_number] = member.identification_number
          member_record[:profile_picture] = member.profile_picture.url(:thumb)
          member_record[:transactions] = []
          member_record[:total_transactions] = []

          (@total_cols * 2).times do
            member_record[:total_transactions] << 0
          end

          (@start_date..@end_date).each do |date|
            transaction_record = {}
            transaction_record[:date] = date.strftime("%B %d, %Y")
            transaction_record[:transactions] = []

            t_savings = savings_account_transactions
                          .joins(:savings_account)
                          .where("date(savings_account_transactions.created_at) = ? 
                                    AND savings_account_transactions.status= ? 
                                    AND savings_accounts.member_id = ?",
                                    date, 
                                    'approved', 
                                    member.id
                                  ).sum(:amount)

            t_insurance = insurance_account_transactions
                          .joins(:insurance_account)
                          .where("date(insurance_account_transactions.created_at) = ? 
                                    AND insurance_account_transactions.status= ? 
                                    AND insurance_accounts.member_id = ?",
                                    date, 
                                    'approved', 
                                    member.id
                                  ).sum(:amount)

            t_equity = equity_account_transactions
                          .joins(:equity_account)
                          .where("date(equity_account_transactions.created_at) = ? 
                                    AND equity_account_transactions.status= ? 
                                    AND equity_accounts.member_id = ?",
                                    date, 
                                    'approved', 
                                    member.id
                                  ).sum(:amount)


            total_counter = 0
            if (t_savings + t_insurance + t_equity) > 0
              @savings_types.each do |savings_type|
                t = {}
                t[:account_code] = savings_type.code
    
                t[:debit] = savings_account_transactions
                              .joins(:savings_account)
                              .where("date(savings_account_transactions.created_at) = ? 
                                        AND savings_account_transactions.status = ? 
                                        AND savings_account_transactions.transaction_type IN(?)
                                        AND savings_accounts.member_id = ?
                                        AND savings_accounts.savings_type_id = ?", 
                                        date, 
                                        'approved', 
                                        ["withdraw", "wp", "tax", "reverse_deposit"],
                                        member.id,
                                        savings_type.id
                                      ).sum(:amount)

                member_record[:total_transactions][total_counter] += t[:debit]
                total_counter += 1

                if t[:debit] == 0
                  t[:debit] = ""
                end

                t[:credit] = savings_account_transactions
                              .joins(:savings_account)
                              .where("date(savings_account_transactions.created_at) = ? 
                                        AND savings_account_transactions.status= ? 
                                        AND savings_account_transactions.transaction_type IN (?)
                                        AND savings_accounts.member_id = ?
                                        AND savings_accounts.savings_type_id = ?", 
                                        date, 
                                        'approved', 
                                        ["deposit", "interest", "reverse_withdraw"],
                                        member.id,
                                        savings_type.id
                                      ).sum(:amount)

                member_record[:total_transactions][total_counter] += t[:credit]
                total_counter += 1

                if t[:credit] == 0
                  t[:credit] = ""
                end

                transaction_record[:transactions] << t
              end

              @insurance_types.each do |insurance_type|
                t = {}
                t[:account_code] = insurance_type.code
    
                t[:debit] = insurance_account_transactions
                              .joins(:insurance_account)
                              .where("date(insurance_account_transactions.created_at) = ? 
                                        AND insurance_account_transactions.status = ? 
                                        AND insurance_account_transactions.transaction_type IN(?)
                                        AND insurance_accounts.member_id = ?
                                        AND insurance_accounts.insurance_type_id = ?", 
                                        date, 
                                        'approved', 
                                        ["withdraw", "wp", "tax", "reverse_deposit"],
                                        member.id,
                                        insurance_type.id
                                      ).sum(:amount).round(2)

                member_record[:total_transactions][total_counter] += t[:debit]
                total_counter += 1

                if t[:debit] == 0
                  t[:debit] = ""
                end

                t[:credit] = insurance_account_transactions
                              .joins(:insurance_account)
                              .where("date(insurance_account_transactions.created_at) = ? 
                                        AND insurance_account_transactions.status= ? 
                                        AND insurance_account_transactions.transaction_type IN (?)
                                        AND insurance_accounts.member_id = ?
                                        AND insurance_accounts.insurance_type_id = ?", 
                                        date, 
                                        'approved', 
                                        ["deposit", "interest", "reverse_withdraw"],
                                        member.id,
                                        insurance_type.id
                                      ).sum(:amount).round(2)

                member_record[:total_transactions][total_counter] += t[:credit]
                total_counter += 1

                if t[:credit] == 0
                  t[:credit] = ""
                end

                transaction_record[:transactions] << t
              end

              @equity_types.each do |equity_type|
                t = {}
                t[:account_code] = equity_type.code
    
                t[:debit] = equity_account_transactions
                              .joins(:equity_account)
                              .where("date(equity_account_transactions.created_at) = ? 
                                        AND equity_account_transactions.status = ? 
                                        AND equity_account_transactions.transaction_type IN(?)
                                        AND equity_accounts.member_id = ?
                                        AND equity_accounts.equity_type_id = ?", 
                                        date, 
                                        'approved', 
                                        ["withdraw", "wp", "tax", "reverse_deposit"],
                                        member.id,
                                        equity_type.id
                                      ).sum(:amount).round(2)

                member_record[:total_transactions][total_counter] += t[:debit]
                total_counter += 1

                if t[:debit] == 0
                  t[:debit] = ""
                end

                t[:credit] = equity_account_transactions
                              .joins(:equity_account)
                              .where("date(equity_account_transactions.created_at) = ? 
                                        AND equity_account_transactions.status= ? 
                                        AND equity_account_transactions.transaction_type IN (?)
                                        AND equity_accounts.member_id = ?
                                        AND equity_accounts.equity_type_id = ?", 
                                        date, 
                                        'approved', 
                                        ["deposit", "interest", "reverse_withdraw"],
                                        member.id,
                                        equity_type.id
                                      ).sum(:amount).round(2)

                member_record[:total_transactions][total_counter] += t[:credit]
                total_counter += 1

                if t[:credit] == 0
                  t[:credit] = ""
                end

                transaction_record[:transactions] << t
              end

              member_record[:transactions] << transaction_record
            end
          end

          record[:member_records] << member_record
        end

        if record[:member_records].size > 0
          @data[:records] << record
        end
      end

      @total_counter  = @data[:records][0][:member_records][0][:total_transactions].size
      @data[:grand_total] = []
      @total_counter.times do |i|
        @data[:grand_total] << 0
      end

      @data[:records].each do |record|
        record[:member_records].each do |member_record|
          @total_counter.times do |i|
            @data[:grand_total][i] += member_record[:total_transactions][i]
          end
        end
      end

      @data
    end
  end
end
