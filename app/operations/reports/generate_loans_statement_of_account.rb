module Reports
  class GenerateLoansStatementOfAccount
    def initialize(branch_id:, center_id:, start_date:, end_date:, loan_product_id:)
      @branches       = Branch.select("*")
      @centers        = Center.select("*")
      @start_date     = start_date
      @end_date       = end_date

      if loan_product_id.present?
        @loan_product = LoanProduct.find(loan_product_id)
      end

      if branch_id.present?
        @branches = @branches.where(id: branch_id)
      end

      if center_id.present?
        @centers = @centers.where(id: center_id)
      end

      @payment_collections        = PaymentCollection.approved.where("paid_at >= ? AND paid_at <= ?", @start_date, @end_date)
      @payment_collection_records = PaymentCollectionRecord.where(payment_collection_id: @payment_collections.pluck(:id))
      @collection_transactions    = CollectionTransaction.loan_payments.where(payment_collection_record_id: @payment_collection_records.pluck(:id))

      @data = {}
      @data[:start_date] = start_date
      @data[:end_date] = end_date
    end

    def execute!
      @data[:branches] = []
      @branches.each do |branch|
        @data[:branches] << build_branch_data(branch)
      end

      @data
    end

    private

    def build_branch_data(branch)
      branch_data = {}
      branch_data[:name] = branch.name
      branch_data[:total_principal] = 0.00
      branch_data[:total_interest] = 0.00
      branch_data[:total_amount] = 0.00
      branch_data[:centers] = []

      branch.centers.where(id: @centers.pluck(:id)).each do |center|
        center_data = build_center_data(center)
        branch_data[:total_principal] += center_data[:total_principal]
        branch_data[:total_interest] += center_data[:total_interest]
        branch_data[:total_amount] += center_data[:total_amount]
        branch_data[:centers] << center_data
      end

      branch_data
    end

    def build_center_data(center)
      center_data = {}
      center_data[:name] = center.name
      center_data[:total_principal] = 0.00
      center_data[:total_interest] = 0.00
      center_data[:total_amount] = 0.00
      center_data[:members] = []

      Member.active.where(center_id: center.id).each do |member|
        member_data = build_member_data(member)
        center_data[:total_principal] += member_data[:total_principal]
        center_data[:total_interest] += member_data[:total_interest]
        center_data[:total_amount] += member_data[:total_amount]
        center_data[:members] << member_data
      end

      center_data
    end

    def build_member_data(member)
      member_data = {}
      member_data[:name] = member.to_s
      member_data[:total_principal] = 0.00
      member_data[:total_interest] = 0.00
      member_data[:total_amount] = 0.00
      member_data[:records] = []

      @collection_transactions.joins(:payment_collection_record).where("payment_collection_records.member_id = ?", member.id).each do |collection_transaction|
        if @loan_product
          if collection_transaction.try(:loan_payment).try(:loan).try(:loan_product_id) == @loan_product.id
            record_data = {}

            record_data[:paid_at] = collection_transaction.payment_collection_record.payment_collection.paid_at
            record_data[:principal] = collection_transaction.loan_payment.paid_principal
            record_data[:interest] = collection_transaction.loan_payment.paid_interest
            record_data[:amount] = collection_transaction.amount

            member_data[:total_principal] += record_data[:principal]
            member_data[:total_interest] += record_data[:interest]
            member_data[:total_amount] += record_data[:amount]

            member_data[:records] << record_data
          end
        else
          record_data = {}

          record_data[:paid_at] = collection_transaction.payment_collection_record.payment_collection.paid_at
          record_data[:principal] = collection_transaction.loan_payment.paid_principal
          record_data[:interest] = collection_transaction.loan_payment.paid_interest
          record_data[:amount] = collection_transaction.amount

          member_data[:total_principal] += record_data[:principal]
          member_data[:total_interest] += record_data[:interest]
          member_data[:total_amount] += record_data[:amount]

          member_data[:records] << record_data
        end
      end

      member_data
    end
  end
end
