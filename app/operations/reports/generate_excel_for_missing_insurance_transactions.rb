module Reports
  class GenerateExcelForMissingInsuranceTransactions
    def initialize
      @p      = Axlsx::Package.new
      @insurance_account_transactions = InsuranceAccountTransaction.approved.where(insurance_account_id: nil)
    end

    def execute!
      @p.workbook do |wb|
        wb.add_worksheet do |sheet|
          sheet.add_row ["Id", "Member Identification Number", "Insurance Type", "Transaction Type", "Amount", "Date", "Accounting Reference Number"]

          @insurance_account_transactions.each do |t|
            sheet.add_row [t.id, "", "", t.transaction_type, t.amount, t.transacted_at, t.voucher_reference_number]
          end
        end
      end

      @p
    end
  end
end
