module Monitoring
  class DailyReportInsuranceAccountStatusesController < ApplicationController
    before_action :authenticate_user!

    def index
      @records = DailyReportInsuranceAccountStatus.select("*")

      @records = @records.order("as_of DESC").page(params[:page])
    end

    def show
      @report = DailyReportInsuranceAccountStatus.find(params[:id])
    end

    def download_excel
      @report = DailyReportInsuranceAccountStatus.find(params[:daily_report_insurance_account_status_id])

      filename = "Insurance Account Status.xlsx"
      report_package = Monitoring::GenerateExcelForInsuranceAccountStatus.new(report: @report).execute!
      report_package.serialize "#{Rails.root}/tmp/#{filename}"
      send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    end
  end
end
