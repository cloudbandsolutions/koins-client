module Monitoring
  class DailyReportsController < ApplicationController
    def index
      @branches = Branch.all
      @daily_reports  = DailyReport.select("*").order("daily_reports.as_of DESC")

      if params[:as_of].present?
        @as_of = params[:as_of]
        @daily_reports  = @daily_reports.where(as_of: @as_of)
      end

      @daily_reports = @daily_reports.page(params[:page])
    end

    def destroy
      @daily_report = DailyReport.find(params[:id])
      @daily_report.destroy!
      flash[:success] = "Successfully destroyed record"
      redirect_to monitoring_daily_reports_path
    end

    def show
      @daily_report = DailyReport.find(params[:id])
      @so_data      = Reports::GenerateSoListFromDailyReport.new(daily_report: @daily_report).execute!
    end
  end
end
