class LoanMoratoriaController < ApplicationController

  before_action :authenticate_user!, :load_defaults

  def load_defaults
    @ammortization_schedule_entry = AmmortizationScheduleEntry.find(params[:ammortization_schedule_entry_id])
    @loan = @ammortization_schedule_entry.loan

    if @loan.pending?
      flash[:error] = "Loan is still pending"
      redirect_to loan_path(@loan)
    end
  end

  def new
    @loan_moratorium = LoanMoratorium.new
  end

  def create
    @loan_moratorium = LoanMoratorium.new(loan_moratorium_params)
    @loan_moratorium.ammortization_schedule_entry = @ammortization_schedule_entry

    if @loan_moratorium.save
      Moratorium::Reamortize.new(
        loan_moratorium: @loan_moratorium,
        num_days: nil
      ).execute!
      flash[:success] = "Successfully delayed payment date to #{@loan_moratorium.new_due_at}"
      redirect_to loan_path(@loan)
    else
      flash.now[:error] = "Error in delaying amortization schedule"
      render :new
    end
  end

  def loan_moratorium_params
    params.require(:loan_moratorium).permit!
  end

end
