class SavingsAccountTransactionsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_role!

  def destroy
    savings_account_transaction = SavingsAccountTransaction.find(params[:id])
    savings_account = savings_account_transaction.savings_account

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Trying to destroy savings_account_transaction #{savings_account_transaction.id}.", 
      email: current_user.email, 
      username: current_user.username
    )

    savings_account_transaction.destroy!
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Successfully destroyed transaction.", 
      email: current_user.email, 
      username: current_user.username
    )

    flash[:success] = "Successfully destroyed transaction"
    redirect_to savings_account_path(savings_account)
  end

  private

  def authorize_role!
    if !["MIS"].include? current_user.role
      flash[:error] = "Unauthorized"
      redirect_to root_path
    end
  end
end
