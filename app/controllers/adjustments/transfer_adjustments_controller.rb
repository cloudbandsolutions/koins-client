module Adjustments
  class TransferAdjustmentsController < ApplicationController
    before_action :authenticate_user!
    before_action :check_user_role!

    def check_user_role!
      if !["MIS", "BK", "SBK"].include? current_user.role
        flash[:error] = "Cannot access this module"
        redirect_to root_path
      end
    end

    def index
      @transfer_adjustments = TransferAdjustment.select("*")

      @transfer_adjustments = @transfer_adjustments.page(params[:page])
    end

    def show
      @transfer_adjustment = TransferAdjustment.find(params[:id])
      @voucher  = Adjustments::ProduceVoucherForTransferAdjustment.new(
                    transfer_adjustment: @transfer_adjustment, user: current_user
                  ).execute!
    end

    def new
      @transfer_adjustment = TransferAdjustment.new
      @transfer_adjustment.branch = Branch.first
    end

    def create
      @transfer_adjustment = TransferAdjustment.new(transfer_adjustment_params) 
      @transfer_adjustment.prepared_by = current_user.full_name
      @transfer_adjustment.branch = Branch.first

      if @transfer_adjustment.save
        flash[:success] = "Successfully saved record"

        redirect_to adjustments_transfer_adjustment_path(@transfer_adjustment)
      else
        flash[:error] = "Error in saving record"

        render :new
      end
    end

    def edit
      @transfer_adjustment = TransferAdjustment.find(params[:id])
    end

    def update
      @transfer_adjustment = TransferAdjustment.find(params[:id])

      if @transfer_adjustment.update(transfer_adjustment_params)
        flash[:success] = "Successfully saved record"

        redirect_to adjustments_transfer_adjustment_path(@transfer_adjustment)
      else
        flash[:error] = "Error in saving record"

        render :edit
      end
    end

    def destroy
      @transfer_adjustment = TransferAdjustment.find(params[:id])

      if @transfer_adjustment.approved?
        flash[:error] = "Cannot destroy non-pending record"

        redirect_to adjustments_transfer_adjustment_path(@transfer_adjustment)
      else
        @transfer_adjustment.destroy!
        flash[:success] = "Successfully destroyed record"

        redirect_to adjustments_transfer_adjustments_path
      end
    end

    private

    def transfer_adjustment_params
      params.require(:transfer_adjustment).permit!
    end
  end
end
