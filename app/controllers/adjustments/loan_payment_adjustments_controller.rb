module Adjustments
  class LoanPaymentAdjustmentsController < ApplicationController
    before_action :authenticate_user!
    before_action :check_user_role!

    def check_user_role!
      if !["MIS", "BK", "SBK"].include? current_user.role
        flash[:error] = "Cannot access this module"
        redirect_to root_path
      end
    end

    def index
      @loan_payment_adjustments = LoanPaymentAdjustment.select("*")

      @loan_payment_adjustments = @loan_payment_adjustments.page(params[:page])
    end

    def show
      @loan_payment_adjustment = LoanPaymentAdjustment.find(params[:id])
      @voucher = Adjustments::ProduceVoucherForLoanPaymentAdjustment.new(loan_payment_adjustment: @loan_payment_adjustment, user: current_user).execute!
    end

    def new
      @loan_payment_adjustment = LoanPaymentAdjustment.new
    end

    def create
      @loan_payment_adjustment = LoanPaymentAdjustment.new(loan_payment_adjustment_params) 
      @loan_payment_adjustment.prepared_by = current_user.full_name

      if @loan_payment_adjustment.save
        flash[:success] = "Successfully saved record"

        redirect_to adjustments_loan_payment_adjustment_path(@loan_payment_adjustment)
      else
        flash[:error] = "Error in saving record"
        @errors = @loan_payment_adjustment.errors.messages

        render :new
      end
    end

    def edit
      @loan_payment_adjustment = LoanPaymentAdjustment.find(params[:id])
    end

    def update
      @loan_payment_adjustment = LoanPaymentAdjustment.find(params[:id])

      if @loan_payment_adjustment.update(loan_payment_adjustment_params)
        flash[:success] = "Successfully saved record"

        redirect_to adjustments_loan_payment_adjustment_path(@loan_payment_adjustment)
      else
        flash[:error] = "Error in saving record"

        render :edit
      end
    end

    def destroy
      @loan_payment_adjustment = LoanPaymentAdjustment.find(params[:id])

      if @loan_payment_adjustment.approved?
        flash[:error] = "Cannot destroy non-pending record"

        redirect_to adjustments_loan_payment_adjustment_path(@loan_payment_adjustment)
      else
        @loan_payment_adjustment.destroy!
        flash[:success] = "Successfully destroyed record"

        redirect_to adjustments_loan_payment_adjustments_path
      end
    end

    private

    def loan_payment_adjustment_params
      params.require(:loan_payment_adjustment).permit!
    end
  end
end
