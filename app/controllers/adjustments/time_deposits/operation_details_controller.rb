module Adjustments
  module TimeDeposits
    class OperationDetailsController < ApplicationController
      def index
        @data = TimeDepositWithdrawal.all
        #raise @data.inspect
      end
      def show
        id = params[:id]
        #@data = TimeDepositWithdrawal.find(id)
        @data = TimeDepositWithdrawal.where(id: id)
        
        g = @data.last
        if  g.status == "pending"
          @data.each do |d|
            @voucher = ::Adjustments::ProduceVoucherForTimeDepositWithdrwal.new(
                                        time_deposit_withdrawal: d, 
                                        user: current_user
                                        ).execute!
          end
        else
          
          @voucher = Voucher.where(reference_number: g.refference_number, book: "JVB").first
        end
            
      end


      def destroy
        id = params[:id]
        @withdraw_time_deposit = TimeDepositWithdrawal.find(id)

        @withdraw_time_deposit.destroy!
        UserActivity.create!(
          user_id: current_user.id,
          content: "Successfully destroyed time deposit withdrawal #{params[:id]}.",
          email: current_user.email,
          username: current_user.username
        )
        flash[:success] = "Successfully destroyed time deposit withdrawal!"
         redirect_to adjustments_time_deposits_operation_details_path

      end

    end
  end
end
