module Adjustments
  class BatchMoratoriaController < ApplicationController
    before_action :authenticate_user!
    before_action :check_user_role!
    before_action :load_object, only: [:show, :edit, :update, :destroy]

    def load_object
      @batch_moratorium = BatchMoratorium.find(params[:id])
    end

    def check_user_role!
      if !["MIS", "BK", "SBK"].include? current_user.role
        flash[:error] = "Cannot access this module"
        redirect_to root_path
      end
    end

    def index
      @batch_moratoriums  = BatchMoratorium.select("*")
    end

    def show
    end

    def new
      @batch_moratorium = BatchMoratorium.new

      if BatchMoratorium.pending.size > 0
        flash[:error] = "There are still pending records. Please approve or cancel them before continuing"
        redirect_to adjustments_batch_moratoria_path
      end
    end

    def create
      @batch_moratorium = BatchMoratorium.new(permit_params)
      @batch_moratorium.initialized_by  = current_user.full_name

      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Trying to create batch moratorium record", 
        email: current_user.email, 
        username: current_user.username
      )

      if @batch_moratorium.save
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully created batch moratorium record", 
          email: current_user.email, 
          username: current_user.username
        )

        flash[:success] = "Successfully saved record"
        redirect_to adjustments_batch_moratorium_path(@batch_moratorium)
      else
        flash[:error] = "Error in saving record"
        render :new
      end
    end

    def edit
    end

    def update
      @batch_moratorium.initialized_by  = current_user.full_name

      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Trying to update batch moratorium",
        email: current_user.email, 
        username: current_user.username
      )

      if @batch_moratorium.update(permit_params)
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully updated batch moratorium record", 
          email: current_user.email, 
          username: current_user.username
        )

        flash[:success] = "Successfully saved record"
        redirect_to adjustments_batch_moratorium_path(@batch_moratorium)
      else
        flash[:error] = "Error in saving record"
        render :new
      end
    end

    def destroy
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Trying to destroy batch moratorium",
        email: current_user.email, 
        username: current_user.username
      )

      if @batch_moratorium.approved?
        flash[:error] = "Cannot destroy approved record"
        redirect_to adjustments_batch_moratorium_path(@batch_moratorium.id)
      else
        @batch_moratorium.destroy!
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully destroyed batch moratorium",
          email: current_user.email, 
          username: current_user.username
        )

        flash[:success] = "Successfully destroyed batch moratorium"
        redirect_to adjustments_batch_moratoria_path
      end
    end

    def permit_params
      params.require(:batch_moratorium).permit!
    end
  end
end
