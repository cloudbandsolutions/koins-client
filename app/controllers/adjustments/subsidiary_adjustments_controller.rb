module Adjustments
  class SubsidiaryAdjustmentsController < ApplicationController
    before_action :authenticate_user!
    before_action :check_user_role!

    def check_user_role!
      if !["MIS", "BK", "SBK" , "OAS"].include? current_user.role
        flash[:error] = "Cannot access this module"
        redirect_to root_path
      end
    end

    def index
      @subsidiary_adjustments = SubsidiaryAdjustment.select("*")

      @subsidiary_adjustments = @subsidiary_adjustments.page(params[:page])
    end

    def show
      @subsidiary_adjustment = SubsidiaryAdjustment.find(params[:id])
      @voucher = Adjustments::ProduceVoucherForSubsidiaryAdjustment.new(subsidiary_adjustment: @subsidiary_adjustment, user: current_user).execute!
    end

    def new
      @subsidiary_adjustment = SubsidiaryAdjustment.new
    end

    def create
      @subsidiary_adjustment = SubsidiaryAdjustment.new(subsidiary_adjustment_params) 
      @subsidiary_adjustment.prepared_by = current_user.full_name

      if @subsidiary_adjustment.save
        flash[:success] = "Successfully saved record"

        redirect_to adjustments_subsidiary_adjustment_path(@subsidiary_adjustment)
      else
        flash[:error] = "Error in saving record"

        render :new
      end
    end

    def edit
      @subsidiary_adjustment = SubsidiaryAdjustment.find(params[:id])
    end

    def update
      @subsidiary_adjustment = SubsidiaryAdjustment.find(params[:id])

      if @subsidiary_adjustment.update(subsidiary_adjustment_params)
        flash[:success] = "Successfully saved record"

        redirect_to adjustments_subsidiary_adjustment_path(@subsidiary_adjustment)
      else
        flash[:error] = "Error in saving record"

        render :edit
      end
    end

    def destroy
      @subsidiary_adjustment = SubsidiaryAdjustment.find(params[:id])

      if @subsidiary_adjustment.approved?
        flash[:error] = "Cannot destroy non-pending record"

        redirect_to adjustments_subsidiary_adjustment_path(@subsidiary_adjustment)
      else
        @subsidiary_adjustment.destroy!
        flash[:success] = "Successfully destroyed record"

        redirect_to adjustments_subsidiary_adjustments_path
      end
    end

    private

    def subsidiary_adjustment_params
      params.require(:subsidiary_adjustment).permit!
    end
  end
end
