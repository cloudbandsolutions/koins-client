module Accounting
  class BalanceSheetsController < ApplicationController
    before_action :authenticate_user!
    before_action :check_user_role!

    def check_user_role!
      if !["MIS", "SBK"].include? current_user.role
        flash[:error] = "Invalid role"
        redirect_to root_path
      end
    end

    def index
      @balance_sheets = BalanceSheet.select("*").order("as_of ASC")
    end

    def show
      @balance_sheet  = BalanceSheet.find(params[:id])
    end

    def destroy
      @balance_sheet  = BalanceSheet.find(params[:id])
      @balance_sheet.destroy!
      flash[:success] = "Successfully destroyed record"
      redirect_to accounting_balance_sheets_path
    end
  end
end
