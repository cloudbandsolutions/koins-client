module Accounting
  class LoanPaymentsController < ApplicationController
    def index
      @loan_payments = LoanPayment.select("*").order("paid_at DESC")

      if !params[:status].blank?
        @status = params[:status]
        @loan_payments = @loan_payments.where(status: @status)
      end

      @loan_payments = @loan_payments.page(params[:page]).per(20)
    end
  end
end
