module Accounting
  class PatronageRefundsController < ApplicationController
    def index
      @patronage_refund_collection = PatronageRefundCollection.select("*")
      @current_working_date = ApplicationHelper.current_working_date
      @current_year = @current_working_date.year
    end


    def show
      @patronage_id = params[:id]
      
#      raise @patronage_id.inspect

      @data = ::Accounting::PatronageRefundCollectonDetails.new(patroange_refund_id: @patronage_id).execute!

    

      @patronage_refund_collection = PatronageRefundCollection.find(params[:id])
      #@patronage_refund_collection_records = PatronageRefundCollectionRecord.where(patronage_refund_collection_id:  params[:id]).order(" data ->> 'member_name' ASC"   )



      if @patronage_refund_collection.status == "approved"
        @voucher = Voucher.where(reference_number: @patronage_refund_collection.reference_number, book: "JVB").first
      else
        @voucher = ::Accounting::GenerateAccountingEntryForPartronageRefund.new(
                  patroange_refund_collection: @patronage_refund_collection,
                  user: current_user
                ).execute!
      end
    end

    def patronage_refund_excel
      @id = params[:id]
      excel = ::Accounting::GenerateExcelForPatronageRefund.new(patronage_id: @id).execute!
      filename = "patronage_refund.xlsx"
      
      excel.serialize "#{Rails.root}/tmp/#{filename}"
      send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    end

    def patronage_refund_collection_pdf
      @id = params[:id]      
      @patronage_refund_collection = PatronageRefundCollection.find(@id)
      @data = ::Accounting::PatronageRefundCollectonDetails.new(patroange_refund_id: @id).execute!
    
    end

  end
  
end
