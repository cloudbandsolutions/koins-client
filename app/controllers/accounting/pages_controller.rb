module Accounting
  class PagesController < ApplicationController
    before_action :authenticate_user!
    def interest_on_share_capital

    end

    def interest_on_share_capital_data
      @start_date = params[:start_date]
      @end_date   = params[:end_date]
      @equity_interest = params[:equity_interest]
      #raise @equity_interest.inspect
      @data       = ::Accounting::GenerateInterestShareCapital.new(start_date: @start_date,end_date: @end_date, equity_interest: @equity_interest).execute!

      render json: { data: @data }
    end

    def chart_of_accounts_pdf
      @data = ::Accounting::GenerateChartOfAccountsPdf.new.execute!    
    end
    
    def income_statement_pdf
      @data = ::Accounting::GenerateIncomeStatementPdf.new.execute!
    end

    def program_status_reports
    end

    def program_status_data
      @data = ::Accounting::GenerateProgramStatusReports.new.execute!

      # Render json when returning data
      render json: { data: @data }
    end

    def dashboard
      #@accounting_codes = AccountingCode.joins(mother_accounting_code: [{major_account: :major_group}])
    end

    def chart_of_accounts
    	UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Visited Chart of Accounts", email: current_user.email, username: current_user.username)
    end

    def year_end_closing
    	UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Visited Year End Closing", email: current_user.email, username: current_user.username)

      current_year  = ApplicationHelper.current_working_date.year
      if YearEndClosingRecord.where(year: current_year).count > 0
        flash[:error] = "Already closed for the year"
        redirect_to root_path
      elsif Settings.activate_microinsurance == true
        render "accounting/pages/year_end_closing_insurance"
      else
        render "accounting/pages/year_end_closing"
      end
    end

    def income_statement
    	UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Visited Income Statement", email: current_user.email, username: current_user.username)
    end
  end
end
