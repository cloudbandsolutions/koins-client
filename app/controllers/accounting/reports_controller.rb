module Accounting
  class ReportsController < ApplicationController
    before_action :authenticate_user!

    def trial_balance_pdf
      @start_date = params[:start_date]
      @end_date   = params[:end_date]
      @branch     = Branch.find(params[:branch_id])
      
      if params[:accounting_fund_id].blank?
        @accounting_fund = nil
      else
        @accounting_fund = AccountingFund.find(params[:accounting_fund_id])
      end

      @data = ::Accounting::GenerateTrialBalance.new(
                start_date: @start_date, 
                end_date: @end_date, 
                branch: @branch, 
                accounting_fund: @accounting_fund
              ).execute!
    end

    def trial_balance
    	if params[:download].present?
      		start_date  = params[:start_date]
          	end_date    = params[:end_date]
          	branch      = Branch.find(params[:branch])
         	if params[:accounting_fund_id].present?
            	accounting_fund = AccountingFund.find(params[:accounting_fund_id])
          	end
          	c_date      = ApplicationHelper.current_working_date

      		data = ::Accounting::GenerateTrialBalance.new(start_date: start_date, end_date: end_date, branch: branch, accounting_fund: accounting_fund).execute!

      		filename = "trial_balance.xlsx"
     		report_package = ::Accounting::TrialBalanceExcel.new(data: data, start_date: start_date, end_date: end_date, branch: branch, accounting_fund: accounting_fund).execute!
      		report_package.serialize "#{Rails.root}/tmp/#{filename}"

      		send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

      # elsif params[:download_pdf].present?
      else
      		render "accounting/reports/trial_balance"
    	end
    end

    def financial_position
      if params[:download].present?
        start_date  = params[:start_date]
        end_date    = params[:end_date]
        branch      = Branch.find(params[:branch])
        c_date      = ApplicationHelper.current_working_date
        detailed    = params[:detailed]

        data = ::Accounting::GenerateFinancialPosition.new(start_date: start_date, end_date: end_date, branch: branch).execute!

        filename = "Financial Position - As of #{end_date.to_date.strftime("%b %d, %Y")}.xlsx"
        report_package = ::Accounting::FinancialPositionExcel.new(data: data, start_date: start_date, end_date: end_date, branch: branch, detailed: detailed).execute!
        report_package.serialize "#{Rails.root}/tmp/#{filename}"

        send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

      # elsif params[:download_pdf].present?
      else
        render "accounting/reports/financial_position"
      end
    end

    def statement_of_comprehensive_inc
      if params[:download].present?
        start_date  = params[:start_date]
        end_date    = params[:end_date]
        branch      = Branch.find(params[:branch])
        c_date      = ApplicationHelper.current_working_date
        detailed    = params[:detailed]

        data = ::Accounting::GenerateStatementOfComprehensiveInc.new(start_date: start_date, end_date: end_date, branch: branch).execute!

        filename = "Statement Of Comprehensive Inc - As of #{end_date.to_date.strftime("%b %d, %Y")}.xlsx"
        report_package = ::Accounting::StatementOfComprehensiveIncExcel.new(data: data, start_date: start_date, end_date: end_date, branch: branch, detailed: detailed).execute!
        report_package.serialize "#{Rails.root}/tmp/#{filename}"

        send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

      # elsif params[:download_pdf].present?
      else
        render "accounting/reports/statement_of_comprehensive_inc"
      end
    end
  end
end
