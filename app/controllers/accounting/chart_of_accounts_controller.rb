module Accounting
  class ChartOfAccountsController < ApplicationController
    before_action :authenticate_user!  

    def download
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Downloaded chart of accounts", email: current_user.email, username: current_user.username)
      pdf = ChartOfAccountsPdf.new(nil, view_context)

      send_data pdf.render, filename: "Chart of Accounts.pdf", type: "application/pdf"
    end
  end
end
