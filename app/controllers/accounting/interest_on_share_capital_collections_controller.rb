module Accounting
  class InterestOnShareCapitalCollectionsController < ApplicationController

  def index
    @list_of_share_capital_collection = InterestOnShareCapitalCollection.all  
    @current_working_date = ApplicationHelper.current_working_date
    @current_year = @current_working_date.year
        
  end

  def show
    branch_id = params[:branch_id]
    center_id = params[:center_id]
    #jef = params[:id]
    @interest_transaction_details_id = params[:id]
    @data = ::Accounting::InterestOnShareCapitalDetails.new(share_capital_interest_id: @interest_transaction_details_id, branch_id: branch_id, center_id: center_id).execute!
    @interest_on_share_capital_collection = InterestOnShareCapitalCollection.find(params[:id])
    
    if @interest_on_share_capital_collection.status == "approved"
      @voucher = Voucher.where(reference_number: @interest_on_share_capital_collection.refference_number, book: "JVB").first
    else
      @voucher =  ::Accounting::GenerateAccountingEntryForInterestOnShareCapitalCollection.new(
                    interest_on_share_capital_collection: @interest_on_share_capital_collection, 
                    user: current_user
                  ).execute!
    end
  end

  def interest_on_share_capital_excel
    jef = params[:id]

  
    excel = ::Accounting::GenerateExcelForInterestOnShareCapital.new(interest_on_share_capital_id: jef).execute!
    filename = "interest_on_sharecapital.xlsx"
    
    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    
  end

  def interest_on_share_capital_pdf
    jef = params[:id]
#    raise jef.inspect

    @interest_on_share_capital_collection = InterestOnShareCapitalCollection.find(params[:id])
    @data = ::Accounting::InterestOnShareCapitalDetails.new(share_capital_interest_id: jef).execute!
  end

  end
end
