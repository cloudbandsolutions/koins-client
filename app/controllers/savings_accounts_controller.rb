class SavingsAccountsController < ApplicationController
  before_action :load_defaults, :authenticate_user!

  def load_defaults
    @members = Member.select("*")
    if current_user.role == "MIS"
      @branches = Branch.all
    else
      @branches = current_user.branches
    end
  end

  def manual_interest_tax_generation
  end

  def index
    @savings_accounts = SavingsAccount.active.includes(:member).order("members.last_name ASC")

    if params[:account_name].present?
      @account_name = params[:account_name]
      @savings_accounts = @savings_accounts.where("savings_accounts.account_number LIKE :q OR members.first_name LIKE :q OR members.last_name LIKE :q", q: "%#{@account_name}%")
    end

    if params[:savings_type_id].present?
      @savings_type = SavingsType.find(params[:savings_type_id])
      @savings_accounts = @savings_accounts.where(savings_type_id: @savings_type.id)
    end

    if params[:branch_id].present?
      @branch = Branch.find(params[:branch_id])
      @savings_accounts = @savings_accounts.where(branch_id: @branch.id)
    end

    if params[:center_id].present?
      @center = Center.find(params[:center_id])
      @savings_accounts = @savings_accounts.where(center_id: @center.id)
    end

    @savings_accounts = @savings_accounts.page(params[:page]).per(20)
  end

  def new
    @savings_account = SavingsAccount.new(balance: 0.00, particular: "Initial opening of account")
  end

  def create
    @savings_account = SavingsAccount.new(savings_account_params)
    @savings_account.balance = 0.00

    if @savings_account.save
      flash[:success] = "Successfully saved savings account record."
      redirect_to savings_account_path(@savings_account.id)
    else
      flash[:error] = "Error in saving savings account record."
      render :new
    end
  end

  def edit 
     @savings_account = SavingsAccountAccount.find(params[:id])
  end

  def update
    @savings_account = SavingsAccountAccount.find(params[:id])

    if @savings_account.update_attributes(savings_account_params)
      flash[:success] = "Successfully saved savings account record."
      redirect_to savings_account_path(@savings_account.id)
    else
      flash[:error] = "Error in saving savings account record."
      render :edit
    end
  end

  def show
    @savings_account = SavingsAccount.find(params[:id])
    @savings_account_transactions = SavingsAccountTransaction.where(
      "savings_account_id = ? AND status IN (?)", 
      @savings_account.id, 
      ["approved", "reversed"]
    ).order("transacted_at ASC, id ASC")


    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @savings_account_transactions = @savings_account_transactions.where(transacted_at: @start_date..@end_date)
    end

    if params[:status].present?
      @status = params[:status]
      @savings_account_transactions = @savings_account_transactions.where(status: @status)
    end

    @savings_account_transactions = @savings_account_transactions.page(params[:page]).per(10)
  end

  def savings_account_pdf
    @savings_account = SavingsAccount.find(params[:id])
    @savings_account_transactions = SavingsAccountTransaction.where(
      "savings_account_id = ? AND status IN (?)", 
      @savings_account.id, 
      ["approved", "reversed"]
    ).order("transacted_at ASC")

  end

  def edit 
     @savings_account = SavingsAccount.find(params[:id])
  end

  def update
    @savings_account = SavingsAccount.find(params[:id])

    if @savings_account.update_attributes(savings_account_params)
      flash[:success] = "Successfully saved savings account record."
      redirect_to savings_account_path(@savings_account.id)
    else
      flash[:error] = "Error in saving savings account record."
      render :edit
    end
  end
  
  def destroy
    @savings_account = SavingsAccount.find(params[:id])
    @savings_account.deactivate!
    flash[:success] = "Successfully removed savings account"
    redirect_to savings_accounts_path
  end

  # for api use
  def accounts
    savings_type_id = params[:savings_type_id]
    center_id = params[:center_id]

    savings_accounts = SavingsAccount.where(center_id: center_id, savings_type_id: savings_type_id)

    data = []
    savings_accounts.each do |savings_account|
      s = { to_s: savings_account.member.to_s }
      sa = JSON::parse(savings_account.to_json).merge(s)
      data << sa
    end

    render json: data
  end

  def print_slip_deposit
      savings_account_transaction = SavingsAccountTransaction.find(params[:id])

      render(
        pdf: "print_slip_deposit",
        template: "savings_accounts/print_slip_deposit",
        layout: false,
        page_size: 'Letter',
        locals: { savings_account_transaction: savings_account_transaction  }
      )
  end

  def print_slip_withdraw 
      savings_account_transaction = SavingsAccountTransaction.find(params[:id])

      render(
        pdf: "print_slip_withdraw",
        template: "savings_accounts/print_slip_withdraw",
        layout: false,
        page_size: 'Letter',
        locals: { savings_account_transaction: savings_account_transaction  }
      )
  end

  def savings_account_params 
    params.require(:savings_account).permit!
  end
end
