class CoopClosingFundsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults

  def index
    @coop_closing_funds = @coop_closing_funds.order("name ASC").page(params[:page])
  end

  def new
    @coop_closing_fund = CoopClosingFund.new
  end

  def create
    @coop_closing_fund = CoopClosingFund.new(coop_closing_fund_params)

    if @coop_closing_fund.save

      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully saved new coop_closing_fund.", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:success] = "Successfully saved coop_closing_fund transaction."
      redirect_to coop_closing_fund_path(@coop_closing_fund)
    else
      flash[:error] = "Error in saving coop_closing_fund transaction. #{@coop_closing_fund.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def edit
    @coop_closing_fund = CoopClosingFund.find(params[:id])

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "About to edit coop_closing_fund #{@coop_closing_fund.to_s.titlecase}", 
      email: current_user.email, 
      username: current_user.username, 
      record_reference_id: @coop_closing_fund.id
    )
  end

  def update
    @coop_closing_fund = CoopClosingFund.find(params[:id])

    if @coop_closing_fund.update(coop_closing_fund_params)
      flash[:success] = "Successfully saved coop_closing_fund transaction."

      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully updated coop_closing_fund #{@coop_closing_fund.to_s}", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @coop_closing_fund.id
      )

      redirect_to coop_closing_fund_path(@coop_closing_fund)
    else
      flash[:error] = "Error in saving coop_closing_fund transaction. #{@coop_closing_fund.errors.full_messages.to_sentence}"
      render :edit
    end
  end

  def destroy
    @coop_closing_fund = CoopClosingFund.find(params[:id])
    @coop_closing_fund.destroy!
    flash[:success] = "Successfully archived coop_closing_fund record."

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Successfully destroyed coop_closing_fund", 
      email: current_user.email, 
      username: current_user.username, 
    )

    redirect_to coop_closing_funds_path
  end

  def show
    @coop_closing_fund = CoopClosingFund.find(params[:id])
  end

  def load_defaults
  end 

  private

  def coop_closing_fund_params
    params.require(:coop_closing_fund).permit!
  end
end
