class HolidaysController < InheritedResources::Base

  private

    def holiday_params
      params.require(:holiday).permit(:name, :holiday_at)
    end
end

