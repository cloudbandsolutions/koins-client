class SurveysController < ApplicationController
  before_action :authenticate_user!

  def index
    @surveys = Survey.select("*")
    @surveys = @surveys.page(params[:page]).per(10)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed all survey records", email: current_user.email, username: current_user.username)
  end

  def show
    @survey = Survey.find(params[:id])
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying survey - #{@survey.title}", email: current_user.email, username: current_user.username, record_reference_id: @survey.id)
  end
end
