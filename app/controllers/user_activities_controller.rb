class UserActivitiesController < ApplicationController
  before_action :authenticate_user!

  def index
    @user_activities = UserActivity.select("*").order("created_at DESC")

    if params[:q].present?
      @q = params[:q]
      @user_activities = @user_activities.where("lower(content) LIKE :q", q: "%#{@q.downcase}%")
    end

    if params[:user_id].present?
      @user_id = params[:user_id]
      @user_activities = @user_activities.where(user_id: @user_id)
    end

    if params[:role].present?
      @role = params[:role]
      @user_activities = @user_activities.where(role: @role)
    end

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date   = params[:end_date]

      # @user_activities  = @user_activities.where("DATE(created_at) >= ? AND DATE(created_at) <= ?", @start_date, @end_date)
      @user_activities  = @user_activities.where("Date(user_activities.created_at) BETWEEN ? AND ?", @start_date, @end_date)
    end

    @user_activities = @user_activities.page(params[:page])
  end
end
