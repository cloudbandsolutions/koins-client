class InsuranceAccountTransactionsController < ApplicationController
  before_action :authenticate_user!
  before_action :authorize_role!

  def destroy
    insurance_account_transaction = InsuranceAccountTransaction.find(params[:id])
    insurance_account = insurance_account_transaction.insurance_account

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Trying to destroy insurance_account_transaction #{insurance_account_transaction.id}.", 
      email: current_user.email, 
      username: current_user.username
    )

    insurance_account_transaction.destroy!
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Successfully destroyed transaction.", 
      email: current_user.email, 
      username: current_user.username
    )

    flash[:success] = "Successfully destroyed transaction"
    redirect_to insurance_account_path(insurance_account)
  end

  private

  def authorize_role!
    if !["MIS"].include? current_user.role
      flash[:error] = "Unauthorized"
      redirect_to root_path
    end
  end
end
