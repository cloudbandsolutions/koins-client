class SurveyRespondentsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_survey
  before_action :load_members

  def new
    @survey_respondent = Surveys::GenerateNewSurveyRespondent.new(survey: @survey).execute!
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to create new survey respondent", email: current_user.email, username: current_user.username)
     
    if params[:member_id].present?
      member = Member.find(params[:member_id])
      @survey_respondent.member = member
    end
  end

  def create
    @survey_respondent = SurveyRespondent.new(survey_respondent_params)
    @survey_respondent.survey = @survey

    if @survey_respondent.save
      flash[:success] = "Successfully saved respondent"
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully created new survey respondent", email: current_user.email, username: current_user.username)
      redirect_to survey_survey_respondent_path(@survey, @survey_respondent)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to create new survey respondent", email: current_user.email, username: current_user.username)
      flash[:error] = "Error in saving respondent"
      render :new
    end
  end

  def show
    @survey_respondent = SurveyRespondent.find(params[:id])
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying survey respondents - #{@survey_respondent.member}", email: current_user.email, username: current_user.username, record_reference_id: @survey.id)
  end

  def edit
    @survey_respondent = SurveyRespondent.find(params[:id])
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "About to edit survey respondent", email: current_user.email, username: current_user.username, record_reference_id: @survey_respondent.id)
  end

  def update
    @survey_respondent = SurveyRespondent.find(params[:id])
    @survey_respondent.survey = @survey

    if @survey_respondent.update(survey_respondent_params)
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated survey respondent", email: current_user.email, username: current_user.username, record_reference_id: @survey_respondent.id)
      flash[:success] = "Successfully saved respondent"
      redirect_to survey_survey_respondent_path(@survey, @survey_respondent)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update survey respondent", email: current_user.email, username: current_user.username, record_reference_id: @survey_respondent.id)
      flash[:error] = "Error in saving respondent"
      render :edit
    end
  end

  def destroy
    @survey_respondent = SurveyRespondent.find(params[:id])
    @survey_respondent.destroy!
    flash[:success] = "Successfully destroyed respondent"
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully deleted survey respondent", email: current_user.email, username: current_user.username)
    redirect_to survey_path(@survey)
  end

  def load_survey
    @survey = Survey.find(params[:survey_id])
  end

  def load_members
    @members = Member.active_and_pending

    if @members.size == 0
      flash[:error] = "No members found"
      redirect_to surveys_path
    end
  end

  def survey_respondent_params
    params.require(:survey_respondent).permit!
  end
end
