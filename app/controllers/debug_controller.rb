class DebugController < ApplicationController
  before_action :authenticate_user!

  def similar_member_names
    @members  = Member.active.order("last_name ASC")
    @data     = Members::BuildSimilarMemberNamesData.new(members: @members).execute!
  end

  def recognition_dates
    @members          = Member.active.order("last_name ASC")
    @insurance_types  = InsuranceType.all.order("id ASC")
  end

  def missing_insurance_transactions
    @insurance_account_transactions = Debug::FetchMissingInsuranceTransactions.new.execute!
  end

  def invalid_memberships
    @records    = ::Debug::FetchInvalidMemberships.new.execute!
  end

  
end
