class ReinstatementsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults, :authenticate_user!
  before_action :load_record, only: [:edit, :update, :destroy, :show]
  before_action :load_types

  def load_record
    @reinstatement = Reinstatement.find(params[:id])
  end

  def load_types
    @branches         = Branch.all
    @centers          = Center.all
  end

  def index
    #@reinstatement_records = ReinstatementRecord.all.includes(:member).order("members.last_name")
    @reinstatements = Reinstatement.all.order("date_prepared DESC")
    @reinstatements = @reinstatements.page(params[:page]).per(20)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying all reinstatements", email: current_user.email, username: current_user.username)

    if params[:status].present?
      @status = params[:status]
      @reinstatements = @reinstatements.where(status: @status)
    end

    if params[:branch_id].present?
      @branch_id = params[:branch_id]
      @reinstatements = @reinstatements.where(branch_id: @branch_id)
    end
  end

  def edit
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to edit reinstatement - #{@reinstatement.branch}.", email: current_user.email, username: current_user.username, record_reference_id: @reinstatement.id)
    @members = Member.active.where("branch_id = ? AND is_reinstate = ?", @reinstatement.branch.id, false).all.order("last_name ASC")
  end

  def update
    if @reinstatement.update(reinstatement_params)
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated reinstatement - #{@reinstatement.branch}.", email: current_user.email, username: current_user.username, record_reference_id: @reinstatement.id)
      flash[:success] = "Successfully saved transaction."
      redirect_to reinstatement_path(@reinstatement)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update reinstatement", email: current_user.email, username: current_user.username, record_reference_id: @reinstatement.id)
      flash[:error] = "Error in saving transaction"
      render :edit
    end
  end


  def show 
    @reinstatement = Reinstatement.find(params[:id])
    @members = Member.where(branch_id: @reinstatement.branch.id).all
    @role = current_user.role
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying reinstatement for #{@reinstatement.branch}", email: current_user.email, username: current_user.username, record_reference_id: @reinstatement.id)
  end

  def destroy
   if !@reinstatement.pending?
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to destroy reinstatement", email: current_user.email, username: current_user.username, record_reference_id: @reinstatement.id)
      flash[:error] = "Cannot destroy non pending record"
      redirect_to reinstatement_path(@reinstatement)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully destroyed reinstatement.", email: current_user.email, username: current_user.username, record_reference_id: @reinstatement.id)
      @reinstatement.destroy!
      flash[:success] = "Successfully destroyed reinstatement."
      redirect_to reinstatements_path
    end
  end

  def approve
    reinstatement = Reinstatement.find(params[:reinstatement_id])
  end

  def load_defaults
    @centers = Center.all
   
    if params[:action] == 'index'
      if params[:q].present?
        @q = params[:q]
      end

      if params[:status].present?
        @status = params[:status]
      end

      if params[:branch_id].present?
        @branch_id = params[:branch_id]
        @branch = Branch.find(@branch_id)
      end
    end
  end

  def reinstatement_params
    params.require(:reinstatement).permit!
  end
end
