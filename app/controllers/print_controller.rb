class PrintController < ApplicationController
  before_action :authenticate_user!

  layout 'blank', except: [:pdf_loan_accounting_entry]


  def income_statements
    
  end
  
  def loan_accounting_entry
    @loan = Loan.find(params[:loan_id])
    @voucher  = ::Loans::ProduceVoucherForLoan.new(
                  loan: @loan,
                  user: current_user
                ).execute!

    filename = "Entry-#{@voucher.reference_number}.xlsx"
    report_package = Print::GenerateExcelForVoucher.new(voucher: @voucher).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def pdf_loan_accounting_entry
    @loan = Loan.find(params[:loan_id])
    @voucher  = ::Loans::ProduceVoucherForLoan.new(
                  loan: @loan,
                  user: current_user
                ).execute!

    render "vouchers/pdf"
  end

  def accounting_entry
    @voucher = Voucher.find(params[:id])
    filename = "Entry-#{@voucher.reference_number}.xlsx"
    report_package = Print::GenerateExcelForVoucher.new(voucher: @voucher).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def check
    @loan = Loan.find(params[:loan_id])

    render layout: 'blank'
  end

  def cash_management_deposit
    @payment_collection = PaymentCollection.find(params[:id])
    filename = "Deposit-#{@payment_collection.reference_number}.xlsx"
    report_package = Print::GenerateExcelForCashManagementDeposit.new(payment_collection: @payment_collection).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

   def cash_management_withdrawal
    @payment_collection = PaymentCollection.find(params[:id])
    filename = "Withdrawal-#{@payment_collection.reference_number}.xlsx"
    report_package = Print::GenerateExcelForCashManagementWithdrawal.new(payment_collection: @payment_collection).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

   def cash_management_fund_transfer
    @payment_collection = PaymentCollection.find(params[:id])
    filename = "Fund Transfer-#{@payment_collection.reference_number}.xlsx"
    report_package = Print::GenerateExcelForCashManagementFundTransfer.new(payment_collection: @payment_collection).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

   def cash_management_membership_payment
    @payment_collection = PaymentCollection.find(params[:id])
    filename = "Membership Payment-#{@payment_collection.reference_number}.xlsx"
    report_package = Print::GenerateExcelForCashManagementMembershipPayment.new(payment_collection: @payment_collection).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def survey
    @survey = Survey.find(params[:id])
    #UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Downloaded Books - #{@book}", email: current_user.email, username: current_user.username)
    filename = "Surveys.xlsx"
    report_package = Print::GenerateExcelForSurveys.new(survey: @survey).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def loan_master_list
    @loans = Loan.active
    filename = "Loan Master List.xlsx"
    report_package = Print::GenerateExcelForLoanMasterList.new(loans: @loans).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def summary_of_loan_release
    @loans = Loan.active_and_paid
    filename = "Summary of Loan Release.xlsx"
    report_package = Print::GenerateExcelForSummaryOfLoanRelease.new(loans: @loans).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def chart_of_accounts
    filename = "Chart of Accounts.xlsx"
    report_package = Print::GenerateExcelForChartOfAccounts.new().execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end
end
