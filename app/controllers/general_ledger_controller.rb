class GeneralLedgerController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults

  def general_ledger_pdf
    branch = nil
    if params[:branch_id].present?
      branch= Branch.find(params[:branch_id])
    end

    if params[:accounting_code_ids].present?
      accounting_code_ids = params[:accounting_code_ids].split(",")
    end

#    raise params[:start_date]

    @data = ::FinancialReports::GeneralLedger.new(
              start_date: params[:start_date],
              end_date: params[:end_date],
              branch: branch,
              accounting_code_ids: accounting_code_ids,
              user: 'pogi si ariel'
    
    ).execute!

  
#    @data = accounting_code_ids

   # render layout: "print"
   # render pdf: "general_ledger.pdf", layout: "blank", orientation: 'Landscape', page_size: 'Legal', grayscale: false, margin: { bottom: 15 }, footer: { html: { template: 'pdfs/footer.html.erb' } }, page_offset: 0
   # render json: { data: data, download_url: "#{general_ledger_pdf_path(start_date: params[:start_date], end_date: params[:end_date], branch_id: branch.try(:id))}" }

  end

  def load_defaults
  end

  def index
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Visited General Ledger", 
      email: current_user.email, 
      username: current_user.username
    )
  end

  # def generate_download_pdf_url
  #   branch = nil

  #   if params[:branch_id].present?
  #     branch = Branch.find(params[:branch_id])
  #   end

  #   if params[:accounting_code_ids].present?
  #     accounting_code_ids = params[:accounting_code_ids]
  #   end

  #   data  = ::FinancialReports::GenderalLedger.new(
  #             start_date: params[:start_date],
  #             end_date: params[:end_date],
  #             branch: branch,
  #             accounting_code_ids: accounting_code_ids
  #           ).execute!

  #   UserActivity.create!(
  #     user_id: current_user.id, 
  #     role: current_user.role, 
  #     content: "Downloaded General Ledger", 
  #     email: current_user.email, 
  #     username: current_user.username
  #   )

  #   render json: { data: data, download_url: "#{general_ledger_download_pdf_path(start_date: params[:start_date], end_date: params[:end_date], branch_id: branch.try(:id))}" }
  # end

  # def download_pdf
  #   branch = nil

  #   if params[:branch_id].present?
  #     branch = Branch.find(params[:branch_id])
  #   end

  #   if params[:accounting_code_ids].present?
  #     accounting_code_ids = params[:accounting_code_ids]
  #   end


  #   @data = ::FinancialReports::GeneralLedger.new(
  #             start_date: params[:start_date], 
  #             end_date: params[:end_date], 
  #             branch: branch, 
  #             accounting_code_ids: accounting_code_ids
  #           ).execute!

  #   UserActivity.create!(
  #     user_id: current_user.id, 
  #     role: current_user.role, 
  #     content: "Downloaded General Ledger", 
  #     email: current_user.email, 
  #     username: current_user.username
  #   )

  #   render pdf: "general_ledger.pdf", layout: "pdf", orientation: 'Landscape', page_size: 'Legal', grayscale: false, margin: { bottom: 15 }, footer: { html: { template: 'pdfs/footer.html.erb' } }, page_offset: 0
  # end

  def entries
    branch = nil

    if params[:branch_id].present?
      branch = Branch.find(params[:branch_id])
    end

    if params[:accounting_code_ids].present?
      accounting_code_ids = params[:accounting_code_ids]
    end

    data  = ::FinancialReports::GeneralLedger.new(
              start_date: params[:start_date], 
              end_date: params[:end_date], 
              branch: branch, 
              accounting_code_ids: accounting_code_ids, 
              user: current_user
            ).execute!

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Generating Financial Report: General Ledger", 
      email: current_user.email, 
      username: current_user.username
    )

    render json: { data: data }
  end

  def generate_download_excel_url
    branch = nil

    if params[:branch_id].present?
      branch = Branch.find(params[:branch_id])
    end

    if params[:accounting_code_ids].present?
      accounting_code_ids = params[:accounting_code_ids]
    end

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Downloaded Excel General Ledger", 
      email: current_user.email, 
      username: current_user.username
    )

    render json: { download_url: "#{general_ledger_download_excel_path(start_date: params[:start_date], end_date: params[:end_date], branch_id: branch.try(:id))}" }
  end

  def download_excel
    branch = nil

    if params[:branch_id].present?
      branch = Branch.find(params[:branch_id])
    end

    if params[:accounting_code_ids].present?
      accounting_code_ids = params[:accounting_code_ids]
    end
    filename = "General Ledger.xlsx"
    data  = ::FinancialReports::GeneralLedger.new(
              start_date: params[:start_date], 
              end_date: params[:end_date], 
              branch: branch, 
              accounting_code_ids: accounting_code_ids, 
              user: current_user
            ).execute!

    report_package  = ::FinancialReports::GenerateExcelForGeneralLedger.new(
                        data: data, 
                        start_date: params[:start_date], 
                        end_date: params[:end_date], 
                        user: current_user
                      ).execute!

    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end
  
  def general_pdf
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    #raise @start_date.inspect    
    @data = ::GeneralLedger::GenerateGeneralPdf.new(start_date: @start_date , end_date: @end_date).execute!
  end

end
