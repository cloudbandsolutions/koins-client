class BooksController < ApplicationController
  before_action :authenticate_user!
  def book_pdf
    @data = {}
    @book       = params[:book_type]
    @data[:start_date] = params[:start_date]
    @data[:end_date]   = params[:end_date]
    if Settings.activate_microloans
      @vouchers   = Voucher.approved_book_vouchers(params[:book_type]).order("reference_number ASC")
      accounting_fund_id = params[try(:accounting_fund_id)]
    elsif Settings.activate_microinsurance
      @vouchers   = Voucher.approved_book_vouchers(params[:book_type]).order("date_prepared ASC, sub_reference_number ASC ")
      accounting_fund_id = params[:accounting_fund_id]
    end
  
    start_date  = params[:start_date]
    end_date    = params[:end_date]

    if start_date.present? and end_date.present? and accounting_fund_id.present?
      @vouchers = @vouchers.where("date_prepared >= ? AND date_prepared <= ? AND accounting_fund_id = ?", start_date, end_date, accounting_fund_id).order("date_prepared ASC, sub_reference_number ASC")
    elsif start_date.present? and end_date.present?
      if Settings.activate_microinsurance
        @vouchers = @vouchers.where("date_prepared >= ? AND date_prepared <= ?", start_date, end_date).order("date_prepared ASC, sub_reference_number ASC")
      else Settings.activate_microloans
        @vouchers = @vouchers.where("date_prepared >= ? AND date_prepared <= ?", start_date, end_date).order("reference_number DESC")
      end
    elsif accounting_fund_id.present?
      @vouchers = @vouchers.where("accounting_fund_id = ?", accounting_fund_id).order("date_prepared ASC, sub_reference_number ASC ")
    end
    @data
  end

  def show
    if Voucher::BOOK_TYPES.include?(params[:book_type])
      @vouchers = Voucher.approved_book_vouchers(params[:book_type])
      @accounting_funds = AccountingFund.all

      if params[:q].present?
        @q = params[:q]
        @vouchers = @vouchers.where("reference_number LIKE :q OR particular LIKE :q", q: "%#{@q}%")
      end

      if params[:branch_id].present?
        @branch_id = params[:branch_id]
        @vouchers = @vouchers.where(branch_id: @branch_id)
      end

      if params[:accounting_fund_id].present?
        @accounting_fund_id = params[:accounting_fund_id]
        @vouchers = @vouchers.where(accounting_fund_id: @accounting_fund_id)
      end

      if params[:start_date].present? and params[:end_date].present?
        @start_date = params[:start_date]
        @end_date = params[:end_date]

        @vouchers = @vouchers.where(date_prepared: @start_date..@end_date)
      end
      
      if Settings.activate_microinsurance
        @vouchers = @vouchers.order("date_prepared ASC, sub_reference_number ASC").page(params[:page]).per(20)
      else Settings.activate_microinsurance
        @vouchers = @vouchers.order("reference_number DESC").page(params[:page]).per(20)
      end  
    else
      flash[:error] = "No such book"
      redirect_to vouchers_path
    end

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Displaying Books - #{params[:book_type]}", 
      email: current_user.email, 
      username: current_user.username
    )
  end

  def csv
    @book = params[:book_type]
    @vouchers = Voucher.approved_book_vouchers(params[:book_type])

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Downloaded Books - #{@book} as csv", 
      email: current_user.email, 
      username: current_user.username
    )

    filename  = "Book-#{@book}.csv"

    data =  Reports::GenerateCSVForBooks.new(
              book: @book,
              vouchers: @vouchers,
              user: current_user
            ).execute!

    send_data data, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=#{filename}"
  end

  def excel
    @book       = params[:book_type]
    if Settings.activate_microloans
      @vouchers   = Voucher.approved_book_vouchers(params[:book_type]).order("reference_number DESC")
      accounting_fund_id = params[try(:accounting_fund_id)]
    elsif Settings.activate_microinsurance
      @vouchers   = Voucher.approved_book_vouchers(params[:book_type]).order("date_prepared ASC, sub_reference_number ASC ")
      accounting_fund_id = params[:accounting_fund_id]
    end
  
    start_date  = params[:start_date]
    end_date    = params[:end_date]

    if start_date.present? and end_date.present? and accounting_fund_id.present?
      @vouchers = @vouchers.where("date_prepared >= ? AND date_prepared <= ? AND accounting_fund_id = ?", start_date, end_date, accounting_fund_id).order("date_prepared ASC, sub_reference_number ASC")
    elsif start_date.present? and end_date.present?
      if Settings.activate_microinsurance
        @vouchers = @vouchers.where("date_prepared >= ? AND date_prepared <= ?", start_date, end_date).order("date_prepared ASC, sub_reference_number ASC")
      else Settings.activate_microloans
        @vouchers = @vouchers.where("date_prepared >= ? AND date_prepared <= ?", start_date, end_date).order("reference_number ASC")
      end
    elsif accounting_fund_id.present?
      @vouchers = @vouchers.where("accounting_fund_id = ?", accounting_fund_id).order("date_prepared ASC, sub_reference_number ASC ")
    end

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Downloaded Books - #{@book}", 
      email: current_user.email, 
      username: current_user.username
    )

    filename        = "Book-#{@book}.xlsx"
    report_package  = Reports::GenerateExcelForBooks.new(
                        book: @book, 
                        vouchers: @vouchers, 
                        user: current_user,
                        start_date: start_date,
                        end_date: end_date,
                      ).execute!

    report_package.serialize "#{Rails.root}/tmp/#{filename}"

    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def print
    @vouchers = Voucher.book_vouchers(params[:book_type])
    @book = params[:book_type]

    render pdf: "book_#{params[:book_type]}.pdf", layout: "pdf", orientation: 'Landscape', page_size: 'Legal', grayscale: false, :margin => { :bottom => 15 }, :footer => { :html => { :template => 'pdfs/footer.html.erb' } }, page_offset: 0
  end
end
