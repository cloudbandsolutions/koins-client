class InternalController < ApplicationController
  before_action :authenticate_user!

  def centers
    if params[:branch_id].present?
      render json: Center.where(branch_id: params[:branch_id])
    else
      render json: Center.all
    end
  end

  def project_types
    if params[:project_type_category_id].present?
      render json: ProjectType.where(project_type_category_id: params[:project_type_category_id])
    else
      render json: ProjectType.all
    end
  end

  def default_processing_fee
    if params[:loan_product_id].present?
      render json: { val: LoanProduct.find(params[:loan_product_id]).default_processing_fee }
    else
      render json: { val: 0 }
    end
  end
end
