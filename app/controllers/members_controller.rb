class MembersController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults
  before_action :check_edit_privileges!, only: [:edit, :new, :create, :update]

  def transfer
    @member = Member.find(params[:member_id])
  end

  def check_edit_privileges!
    if !["MIS", "OAS", "BK", "REMOTE-OAS", "REMOTE-BK", "REMOTE-MIS", "REMOTE-FM"].include? current_user.role
      flash[:error] = "Unauthorized"
      redirect_to members_path
    end
  end

  def edit_resignation
    @member = Member.find(params[:member_id])

    if !@member.for_resignation?
      flash[:error] = "This member is not flagged for resignation"
      redirect_to member_path(@member)
    else
      @member_resignation = MemberResignation.where(member_id: @member.id).first
      @savings_types      = SavingsType.all
      @insurance_types    = InsuranceType.all
      @equity_types       = EquityType.all
    end
  end

  def update_resignation
    @member             = Member.find(params[:member_id])
    @member_resignation = MemberResignation.find(params[:member_resignation_id])
    @savings_types      = SavingsType.all
    @insurance_types    = InsuranceType.all
    @equity_types       = EquityType.all

    if @member_resignation.update(member_resignation_params)
      flash[:success] = "Successfully updated member resignation"
      redirect_to member_resignation_path(@member)
    else
      flash[:error] = "Error in updating member resignation"
      render :edit_resignation
    end
  end

  def destroy
    @member = Member.find(params[:id])
    @member.archive!
    flash[:success] = "Successfully archived member!"
    redirect_to members_path
  end

  def resignation
    @member = Member.find(params[:member_id])
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Trying to resign member.", 
      email: current_user.email, 
      username: current_user.username
    )

    if @member.status == 'resigned'
      @member_resignation         = MemberResignation.where(member_id: @member.id).first
      @voucher_for_loan_payments  = Voucher.where(book: 'JVB', reference_number: @member_resignation.reference_number).first
    elsif !@member.for_resignation?
      flash[:error] = "This member is not flagged for resignation"
      redirect_to member_path(@member)
    else
      @member_resignation         = MemberResignation.where(member_id: @member.id).first
      @voucher                    = Resignation::ProduceVoucherForResignationLoanPayments.new(
                                      member_resignation: @member_resignation, 
                                      user: current_user
                                    ).execute!

      @voucher_for_loan_payments  = @voucher
    end
  end

  def transactions
    @member = Member.find(params[:member_id])
    @time = Time.now
  end

  def index
    @members = Members::SearchCurrent.new(params: params, branches: @branches, i: 12).execute!
    @members = @members.order("members.last_name ASC").page(params[:page]).per(20)
  end

  def new
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Trying to create new member.", 
      email: current_user.email, 
      username: current_user.username
    )

    @member = Member.new(member_type: "Regular")

    if Settings.use_default_survey == true
      @survey = Survey.where(is_default: true).first
      @survey_respondent = Surveys::GenerateNewSurveyRespondent.new(survey: @survey).execute!
      @member.survey_respondents << @survey_respondent
    end

    @centers = []
  end

  def create
    @member = Member.new(member_params)
    @centers = Center.where(branch_id: @member.branch.try(:id))
    @survey = Survey.where(is_default: true).first

    if @member.save
      Members::GenerateMissingAccounts.new(member: @member).execute!
      @member.branch.update(member_counter: @member.branch.member_counter + 1)
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully saved new member.", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:success] = "Successfully saved member transaction."
      redirect_to member_path(@member)
    else
      flash[:error] = "Error in saving member transaction. #{@member.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def edit
    @member = Member.find(params[:id])

    if @member.gender == "Male"
      @member.gender = "Lalake"
    elsif @member.gender == "Female"
      @member.gender = "Babae"
    elsif @member.gender == "Others"
      @member.gender = "Iba pa"
    end

    if @member.civil_status == "Married" || @member.civil_status == "married"
      @member.civil_status = "Kasal"
    elsif @member.civil_status == "Single" || @member.civil_status == "single"
      @member.civil_status = "Single"
    elsif @member.civil_status == "Widowed" || @member.civil_status == "widowed"
      @member.civil_status = "Biyudo/a"
    elsif @member.civil_status == "Separated" || @member.civil_status == "separated"
      @member.civil_status = "Hiwalay"
    end

    @centers = Center.where(branch_id: @member.branch.try(:id))
    @survey = Survey.where(is_default: true).first
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "About to edit member #{@member.full_name.titlecase}", 
      email: current_user.email, 
      username: current_user.username, 
      record_reference_id: @member.id
    )

    # Check for people trying to edit record
    if ["OAS"].include?(current_user.role) and @member.is_editable != true
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Cannot edit member #{@member.full_name.titlecase}", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @member.id
      )

      flash[:error] = "Cannot edit member. Please notify BK."
      redirect_to member_path(@member)
    end
  end

  def update
    @member = Member.find(params[:id])
    @member.is_editable = false

    @centers = Center.where(branch_id: @member.branch.try(:id))
    @survey = Survey.where(is_default: true).first

    if @member.update(member_params)
      Members::GenerateMissingAccounts.new(member: @member).execute!
      Membership::ConfigureMembershipAfterMemberUpdate.new(member: @member).execute!
      flash[:success] = "Successfully saved member transaction."
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully updated member #{@member.full_name.titlecase}", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @member.id
      )

      redirect_to member_path(@member)
    else
      flash[:error] = "Error in saving member transaction. #{@member.errors.full_messages.to_sentence}"
      render :edit
    end
  end

  def destroy
    @member = Member.find(params[:id])

    if @member.has_active_transactions?
      flash[:error] = "Member still has active transactions"
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Cannot destroy member #{@member.full_name.titlecase}. Member still has active transaction.", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @member.id
      )

      redirect_to member_path(@member)
    else
      @member.archive!
      flash[:success] = "Successfully archived member record."
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully archived member #{@member.full_name.titlecase}", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @member.id
      )

      redirect_to members_path
    end

  end

  def show
    @member = Member.find(params[:id])
    @loan_products = LoanProduct.all
    # if Settings.activate_microloans
    #   Members::UpdateMemberInsuranceStatus.new(member: @member).execute!
    # end
  end

  # toggle methods
  def toggle_active
    member = Member.find(params[:member_id])
    member.toggle_active!
    flash[:success] = "Successfully updated member as active"
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Toggle member.", 
      email: current_user.email, 
      username: current_user.username, 
      record_reference_id: @member.id
    )

    redirect_to member_path(member)
  end

  def load_defaults
    if params[:branch_id].present?
      @branch = Branch.find(params[:branch_id])
    end

    if current_user.role == "MIS"
      @branches = Branch.all
    else
      @branches = current_user.branches
    end

    if params[:identification].present?
      @identification = params[:identification]
    end

    if params[:member_type].present?
      @member_type = params[:member_type]
    end

    if params[:status].present?
      @status = params[:status]
    end

    if params[:q].present?
      @q = params[:q]
    end

    if params[:branch_id].present?
      @branch = Branch.find(params[:branch_id])
    end

    if params[:center_id].present?
      @center = Center.find(params[:center_id])
    end

    if params[:member_type].present?
      @member_type = params[:member_type]
    end
  end 

  def import
    #Member.import(params[:file])
    file = params[:file]
    orig_file_name = file.original_filename
    orig_file_name_no_ext = File.basename("#{orig_file_name}", ".*")
    file_name = orig_file_name_no_ext.delete! "members "
    start_date_string = file_name.split("_").first
    end_date_string = file_name.split("_").last
    
    if !start_date_string.nil? && !end_date_string.nil?
      # for 2018 to 2019 only, change when 2020 up
      if start_date_string.include? "201"
        end_date = DateTime.parse(end_date_string).try(:to_date)
        start_date = DateTime.parse(start_date_string).try(:to_date)
      else
        end_date = nil
        start_date = nil
      end
    else  
      end_date = nil
      start_date = nil
    end
    
    @errors = []
    CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
      member = row.to_hash
      errors = Membership::ValidateMemberImportCsvFile.new(member: member).execute!
      errors.each do |e|
        @errors << e
      end
    end

    if @errors.size == 0
      Membership::LoadMembersFromCsvFile.new(file: file).execute!
      flash[:success] = "Successfully Import Member Record."
      redirect_to members_path
      if !start_date.nil? && !end_date.nil?
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported members. #{start_date.strftime("%b %d,%Y")} - #{end_date.strftime("%b %d,%Y")}", 
          email: current_user.email, 
          username: current_user.username
        )
      else
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported members.", 
          email: current_user.email, 
          username: current_user.username
        )
      end
    else
      redirect_to import_tools_path
      flash[:error] = "Error Importing Members"
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Failed to import members.", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:error] = @errors
    end  
  end

  def import_dependents
    file = params[:file]
    orig_file_name = file.original_filename
    orig_file_name_no_ext = File.basename("#{orig_file_name}", ".*")
    file_name = orig_file_name_no_ext.delete! "legal dependents"
    start_date_string = file_name.split("_").first
    end_date_string = file_name.split("_").last
    
    if !start_date_string.nil? && !end_date_string.nil?
      # for 2018 to 2019 only, change when 2020 up
      if start_date_string.include? "201"
        end_date = DateTime.parse(end_date_string).try(:to_date)
        start_date = DateTime.parse(start_date_string).try(:to_date)
      else
        end_date = nil
        start_date = nil
      end
    else  
      end_date = nil
      start_date = nil
    end

    @errors = []
    CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
      dependent = row.to_hash
      errors = Membership::ValidateDependentImportCsvFile.new(dependent: dependent).execute!
      errors.each do |e|
        @errors << e
      end
    end

    if @errors.size == 0
      Membership::LoadDependentsFromCsvFile.new(file: file).execute!
      flash[:success] = "Successfully Import Legal Dependents Record."
      if !start_date.nil? && !end_date.nil?
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported legal dependents. #{start_date.strftime("%b %d,%Y")} - #{end_date.strftime("%b %d,%Y")}", 
          email: current_user.email, 
          username: current_user.username
        )
      else
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported legal dependents.", 
          email: current_user.email, 
          username: current_user.username
        )
      end  

      redirect_to members_path
    else
      redirect_to import_dependents_path
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Failed to import legal dependents.", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:error] = "Error Importing Legal Dependents"
      flash[:error] = @errors
    end  
  end

  def import_beneficiaries
    file = params[:file]
    orig_file_name = file.original_filename
    orig_file_name_no_ext = File.basename("#{orig_file_name}", ".*")
    file_name = orig_file_name_no_ext.delete! "beneficiaries "
    start_date_string = file_name.split("_").first
    end_date_string = file_name.split("_").last
    
    if !start_date_string.nil? && !end_date_string.nil?
      if start_date_string.include? "201"
        end_date = DateTime.parse(end_date_string).try(:to_date)
        start_date = DateTime.parse(start_date_string).try(:to_date)
      else
        end_date = nil
        start_date = nil
      end
    else  
      end_date = nil
      start_date = nil
    end

    @errors = []
    CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
      beneficiary = row.to_hash
      errors = Membership::ValidateBeneficiaryImportCsvFile.new(beneficiary: beneficiary).execute!
      errors.each do |e|
        @errors << e
      end
    end

    if @errors.size == 0
      Membership::LoadBeneficiariesFromCsvFile.new(file: file).execute!
      flash[:success] = "Successfully Import Beneficiary Record."
      
      if !start_date.nil? && !end_date.nil?
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported beneficiaries. #{start_date.strftime("%b %d,%Y")} - #{end_date.strftime("%b %d,%Y")}", 
          email: current_user.email, 
          username: current_user.username
        )
      else
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported beneficiaries.", 
          email: current_user.email, 
          username: current_user.username
        )
      end

      redirect_to members_path
    else
      redirect_to import_beneficiary_path
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Failed to import beneficiaries.", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:error] = "Error Importing Beneficiary"
      flash[:error] = @errors
    end  
  end

  def import_correct_mii_recognition_date
    file = params[:file]
    @errors = []
    CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
      member_date = row.to_hash
      errors = Membership::ValidateCorrectMiiRecognitionDateImportCsvFile.new(member_date: member_date).execute!
      errors.each do |e|
        @errors << e
      end
    end

    if @errors.size == 0
      Membership::LoadCorrectMiiRecognitionDateFromCsvFile.new(file: file).execute!
      flash[:success] = "Successfully Import Correct MII Recognition Date."
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully Imported Correct MII Recognition Date.", 
        email: current_user.email, 
        username: current_user.username
      )

      redirect_to members_path
    else
      redirect_to import_correct_mii_recognition_date_path
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Failed to Import Correct MII Recognition Date..", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:error] = "Error Importing Correct MII Recognition Date."
      flash[:error] = @errors
    end  
  end

  def import_members_spouse_as_dependents
    file = params[:file]
    @errors = []
    CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
      spouse = row.to_hash
      errors = Membership::ValidateMembersSpouseImportCsvFile.new(spouse: spouse).execute!
      errors.each do |e|
        @errors << e
      end
    end

    if @errors.size == 0
      Membership::LoadMembersSpouseFromCsvFile.new(file: file).execute!
      flash[:success] = "Successfully Import Members Spouse as Dependents."
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully Imported Members Spouse as Dependents.", 
        email: current_user.email, 
        username: current_user.username
      )

      redirect_to members_path
    else
      redirect_to import_members_spouse_as_dependents_path
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Failed to Import Members Spouse as Dependents.", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:error] = "Error Importing Members Spouse as Dependents."
      flash[:error] = @errors
    end  
  end
  
  def change_beneficiaries_pdf
    @member = Member.find(params[:member_id])
  end

  def blip_form_pdf
    @member = Member.find(params[:member_id])
  end

  private

  def member_resignation_params
    params.require(:member_resignation).permit!
  end

  def member_params
    params.require(:member).permit!
  end
end
