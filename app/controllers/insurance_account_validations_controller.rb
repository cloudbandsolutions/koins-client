class InsuranceAccountValidationsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults, :authenticate_user!
  before_action :load_record, only: [:edit, :update, :destroy, :show]
  before_action :load_types

  def load_record
    @insurance_account_validation = InsuranceAccountValidation.find(params[:id])
  end

  def load_types
    @branches         = Branch.all
    @centers          = Center.all
  end

  def index
    @insurance_account_validations = InsuranceAccountValidation.all.order("date_prepared DESC")
    @insurance_account_validations = @insurance_account_validations.page(params[:page]).per(20)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying all Insurance Validation Accounts", email: current_user.email, username: current_user.username)
    
    if params[:q].present?
      @q = params[:q]
      @insurance_account_validations = @insurance_account_validations.joins(insurance_account_validation_records: :member).where("lower(members.first_name) LIKE :q OR lower(members.last_name) LIKE :q OR lower(members.middle_name) LIKE :q", q: "%#{@q.downcase}%")
    end
    
    if params[:status].present?
      @status = params[:status]
      @insurance_account_validations = @insurance_account_validations.where(status: @status)
    end

    if params[:branch_id].present?
      @branch_id = params[:branch_id]
      @insurance_account_validations = @insurance_account_validations.where(branch_id: @branch_id)
    end
  end

  def edit
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to edit Insurance Validation Account - #{@insurance_account_validation.branch}.", email: current_user.email, username: current_user.username, record_reference_id: @insurance_account_validation.id)
    @members = Member.active.where("branch_id = ?", @insurance_account_validation.branch.id).all.order("last_name ASC")
  end

  def update
    if @insurance_account_validation.update(insurance_account_validation_params)
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated Insurance Validation Account - #{@insurance_account_validation.branch}.", email: current_user.email, username: current_user.username, record_reference_id: @insurance_account_validation.id)
      flash[:success] = "Successfully saved transaction."
      redirect_to insurance_account_validation_path(@insurance_account_validation)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update Insurance Validation Account", email: current_user.email, username: current_user.username, record_reference_id: @insurance_account_validation.id)
      flash[:error] = "Error in saving transaction"
      render :edit
    end
  end


  def show 
    @insurance_account_validation = InsuranceAccountValidation.find(params[:id])

    @voucher  = InsuranceAccountValidations::ProduceVoucherForInterestDeposit.new(
                  insurance_account_validation: @insurance_account_validation,
                  is_remote: @insurance_account_validation.is_remote
                ).execute!

    @members = Member.where(branch_id: @insurance_account_validation.branch.id).all
    @role = current_user.role
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying Insurance Validation Account for #{@insurance_account_validation.branch}", email: current_user.email, username: current_user.username, record_reference_id: @insurance_account_validation.id)
  end

  def destroy
   if @insurance_account_validation.pending? || @insurance_account_validation.cancelled?
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully destroyed Insurance Validation Account.", email: current_user.email, username: current_user.username, record_reference_id: @insurance_account_validation.id)
      @insurance_account_validation.destroy!
      flash[:success] = "Successfully destroyed Insurance Validation Account."
      redirect_to insurance_account_validations_path
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to destroy Insurance Validation Account", email: current_user.email, username: current_user.username, record_reference_id: @insurance_account_validation.id)
      flash[:error] = "Cannot destroy record"
      redirect_to insurance_account_validation_path(@insurance_account_validation)
    end
  end

  def approve
    insurance_account_validation = InsuranceAccountValidation.find(params[:insurance_account_validation_id])
  end

  def withdrawal_pdf
    @insurance_account_validation_record = InsuranceAccountValidationRecord.find(params[:insurance_account_validation_record_id])
  end

  def pdf
    @insurance_account_validation = InsuranceAccountValidation.find(params[:insurance_account_validation_id])
    @voucher = InsuranceAccountValidations::ProduceVoucherForInterestDeposit.new(insurance_account_validation: @insurance_account_validation).execute!
  end

  def load_defaults
    @centers = Center.all
   
    if params[:action] == 'index'
      if params[:q].present?
        @q = params[:q]
      end

      if params[:status].present?
        @status = params[:status]
      end

      if params[:branch_id].present?
        @branch_id = params[:branch_id]
        @branch = Branch.find(@branch_id)
      end
    end
  end

  def insurance_account_validation_params
    params.require(:insurance_account_validation).permit!
  end

  

end
