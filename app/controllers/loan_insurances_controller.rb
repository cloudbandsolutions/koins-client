class LoanInsurancesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults, :authenticate_user!
  before_action :load_record, only: [:edit, :update, :destroy, :show]
  before_action :load_types

  def load_record
    @loan_insurance = LoanInsurance.find(params[:id])
  end

  def load_types
    @loan_insurance_types = LoanInsuranceType.all
    @branches         = Branch.all
    @centers          = Center.all
  end

  def index
    #@loan_insurance_records = LoanInsuranceRecord.all.includes(:member).order("members.last_name")
    @loan_insurances = LoanInsurance.all.order("date_prepared DESC")
    @loan_insurances = @loan_insurances.page(params[:page]).per(20)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying all loan insurances", email: current_user.email, username: current_user.username)
    
    if ["OAS", "BK"].include? current_user.role
      @loan_insurances = @loan_insurances.where(status: "pending")
    end

    if params[:loan_insurance_type_id].present?
      @loan_insurance_type_id = params[:loan_insurance_type_id]
      @loan_insurances = @loan_insurances.where(loan_insurance_type_id: @loan_insurance_type_id)
    end

    if params[:status].present?
      @status = params[:status]
      @loan_insurances = @loan_insurances.where(status: @status)
    end

    if params[:branch_id].present?
      @branch_id = params[:branch_id]
      @loan_insurances = @loan_insurances.where(branch_id: @branch_id)
    end

    if params[:collection_type].present?
      @collection_type = params[:collection_type]
      @loan_insurances = @loan_insurances.where(collection_type: @collection_type)
    end
  end

  def edit
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to edit loan insurance - #{@loan_insurance.branch}.", email: current_user.email, username: current_user.username, record_reference_id: @loan_insurance.id)
    @members = Member.where(branch_id: @loan_insurance.branch.id).all.order("last_name ASC")
  end

  def update
    if @loan_insurance.update(loan_insurance_params)
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated loan insurance - #{@loan_insurance.branch}.", email: current_user.email, username: current_user.username, record_reference_id: @loan_insurance.id)
      flash[:success] = "Successfully saved transaction."
      redirect_to loan_insurance_path(@loan_insurance)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update loan insurance", email: current_user.email, username: current_user.username, record_reference_id: @loan_insurance.id)
      flash[:error] = "Error in saving transaction"
      render :edit
    end
  end


  def show 
    @loan_insurance = LoanInsurance.find(params[:id])
    @members = Member.where(branch_id: @loan_insurance.branch.id).all
    @voucher = LoanInsurances::ProduceVoucherForLoanInsurance.new(loan_insurance: @loan_insurance, user: current_user).execute!
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying loan insurance for #{@loan_insurance.branch} - #{@loan_insurance.loan_insurance_type}", email: current_user.email, username: current_user.username, record_reference_id: @loan_insurance.id)
  end

  def destroy
   if !@loan_insurance.pending?
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to destroy loan insurance", email: current_user.email, username: current_user.username, record_reference_id: @loan_insurance.id)
      flash[:error] = "Cannot destroy non pending record"
      redirect_to loan_insurnace_path(@loan_insurance)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully destroyed loan insurance - #{@loan_insurance.branch}.", email: current_user.email, username: current_user.username, record_reference_id: @loan_insurance.id)
      @loan_insurance.destroy!
      flash[:success] = "Successfully destroyed loan insurance."
      redirect_to loan_insurances_path
    end
  end

  def approve
    loan_insurance = LoanInsurance.find(params[:loan_insurance_id])
  end

  def load_defaults
    @centers = Center.all
    @loan_insurance_types = LoanInsuranceType.all

    if params[:action] == 'index'
      if params[:q].present?
        @q = params[:q]
      end

      if params[:loan_insurance_type_id].present?
        @loan_insurance_type_id = params[:loan_insurance_type_id]
      end

      if params[:status].present?
        @status = params[:status]
      end

      if params[:branch_id].present?
        @branch_id = params[:branch_id]
        @branch = Branch.find(@branch_id)
      end

      if params[:collection_type].present?
        @collection_type = params[:collection_type]
      end
    end
  end

  def upload
    file = params[:file]
    branch = Branch.find(params[:branch_id])
    loan_insurance_type = LoanInsuranceType.find(params[:loan_insurance_type_id])
    date_prepared = params[:date_prepared]
    collection_type = params[:collection_type]
    prepared_by = current_user.full_name.upcase
    @errors = []
    CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
      loan_insurance_record = row.to_hash
      errors = LoanInsurances::ValidateLoanInsuranceRecordsCsvFile.new(loan_insurance_record: loan_insurance_record, branch: branch, loan_insurance_type: loan_insurance_type, date_prepared: date_prepared, collection_type: collection_type).execute!
      errors.each do |e|
        @errors << e
      end
    end

    if @errors.size == 0
      loan_insurance = LoanInsurances::LoadLoanInsuranceRecordsFromCsvFile.new(collection_type: collection_type, file: file, branch: branch, date_prepared: date_prepared, loan_insurance_type: loan_insurance_type, prepared_by: prepared_by).execute!
      flash[:success] = "Successfully Import Loan Insurance Record."
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully Import Loan Insurance record.", email: current_user.email, username: current_user.username)
      redirect_to edit_loan_insurance_path(loan_insurance.id)
    else
      redirect_to upload_loan_insurance_path
      flash[:error] = @errors
    end  
  end

  def loan_insurance_params
    params.require(:loan_insurance).permit!
  end
end
