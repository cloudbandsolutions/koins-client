# Used for cluster deployments only
module SystemManagement
  class UsersController < ApplicationController
    before_action :authenticate_user!
    before_action :check_user_role!

    def check_user_role!
      if !["SBK", "MIS", "REMOTE-MIS"].include?(current_user.role)
        flash[:error] = "Invalid access"
        redirect_to root_path
      end
    end

    def show
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed user profile", email: current_user.email, username: current_user.username)
      @user = User.find(params[:id])
    end

    def index
      @users = User.all

      if ApplicationHelper.remote_user?(current_user)
        @users  = @users.joins(:branches).where("branches.id IN (?)", current_user.branches.pluck(:id))
      end

      if params[:q].present?
        @q = params[:q]
        @users = @users.where("username LIKE :q OR email LIKE :q", q: "%#{@q}%")
      end

      if params[:role].present?
        @role = params[:role]
        @users = @users.where(role: @role)
      end

      if params[:branch_id].present?
      end

      @users = @users.page(params[:page])
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying all users.", email: current_user.email, username: current_user.username)
    end

    def new
      @user = User.new
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to create new user.", email: current_user.email, username: current_user.username)
    end

    def create
      @user = User.new(user_params)
      @user.cluster = @cluster
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to save new user.", email: current_user.email, username: current_user.username)

      if @user.save
        flash[:success] = "Successfully saved user for cluster #{@cluster}"
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully saved new user.", email: current_user.email, username: current_user.username)
        redirect_to system_management_user_path(@user)
      else
        flash[:error] = "Cannot save user for cluster #{@cluster}"
        render :new
      end
    end

    def edit
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to edit user", email: current_user.email, username: current_user.username)
      @user = User.find(params[:id])
    end

    def update
      @user = User.find(params[:id])
      @user.cluster = @cluster
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to update user", email: current_user.email, username: current_user.username)

      if @user.update(user_params)
        flash[:success] = "Successfully saved user for cluster #{@cluster}"
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successful update of user", email: current_user.email, username: current_user.username)
        redirect_to system_management_user_path(@user)
      else
        flash[:error] = "Cannot save user for cluster #{@cluster}"
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Unsuccessful update of user", email: current_user.email, username: current_user.username)
        render :edit
      end
    end
 
    def destroy
      @user = User.find(params[:id])
      if @user.branches.count == 0
        @user.destroy!
      else
        
      end
      flash[:success] = "Successfully removed user for cluster #{@cluster}"
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully destroy user", email: current_user.email, username: current_user.username)
      redirect_to system_management_users_path
    end

    def user_params
      params.require(:user).permit!
    end
  end
end
