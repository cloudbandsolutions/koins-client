module SystemManagement
  class CentersController < ApplicationController
    before_action :authenticate_user!
    before_action :load_defaults

    def load_defaults
      if current_user.role == "MIS"
        @branches = Branch.all
      else
        @branches = current_user.branches
      end
    end

    def show
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Viewed center information", 
        email: current_user.email, 
        username: current_user.username
      )
      @center = Center.find(params[:id])
    end

    def index
      @centers = Center.where("branch_id IN (?)", @branches.pluck(:id))

      if params[:q].present?
        @q = params[:q]
        @centers = @centers.where("lower(name) LIKE :q OR lower(code) LIKE :q", q: "%#{@q.downcase}%")
      end

      if params[:branch_id].present?
        @branch = Branch.find(params[:branch_id])
        @centers = @centers.where("branch_id = ?", @branch.id)
      end

      @centers = @centers.order("name ASC")
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Displaying all centers.", 
        email: current_user.email, 
        username: current_user.username
      )
    end

    def new
      @center = Center.new
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Trying to create new center.", 
        email: current_user.email, 
        username: current_user.username
      )
    end

    def create
      @center = Center.new(center_params)
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Trying to save new center.", 
        email: current_user.email, 
        username: current_user.username
      )
      
      if @center.save
        flash[:success] = "Successfully saved center"
        redirect_to system_management_center_path(@center)
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully saved new center.", 
          email: current_user.email, 
          username: current_user.username
        )
      else
        flash[:error] = "Cannot save center #{@center.errors.messages}"
        render :new
      end
    end

    def edit
      @center = Center.find(params[:id])
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Trying to edit center", 
        email: current_user.email, 
        username: current_user.username
      )
    end

    def update
      @center = Center.find(params[:id])
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Trying to update center", 
        email: current_user.email, 
        username: current_user.username
      )

      if @center.update(center_params)
        flash[:success] = "Successfully saved center for cluster #{@cluster}"
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successful update of center", 
          email: current_user.email, 
          username: current_user.username
        )
        redirect_to system_management_center_path(@center)
      else
        flash[:error] = "Cannot save center for cluster #{@cluster}"
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Unsuccessful update of center", 
          email: current_user.email, 
          username: current_user.username
        )
        render :edit
      end
    end

    def destroy
      @center = Center.find(params[:id])
      @center.destroy!
      flash[:success] = "Successfully removed center for cluster #{@cluster}"
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully destroy center", 
        email: current_user.email, 
        username: current_user.username
      )
      redirect_to system_management_centers_path
    end

    def center_params
      params.require(:center).permit!
    end
  end
end
