class BackgroundOperationsController < ApplicationController
  before_action :authenticate_user!

  def index
    @background_operations = BackgroundOperation.select("*").order("created_at DESC")

    if params[:status].present?
      @status = params[:status]
      @background_operations = @background_operations.where(status: @status)
    end

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date   = params[:end_date]

      @background_operations  = @background_operations.where("Date(background_operations.created_at) BETWEEN ? AND ?", @start_date, @end_date)
    end

    @background_operations = @background_operations.page(params[:page])
  end
end
