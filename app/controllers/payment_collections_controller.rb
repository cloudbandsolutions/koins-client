class PaymentCollectionsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_record, only: [:edit, :update, :destroy, :show, :payment_collection_pdf]
  before_action :load_types
  before_action :load_loan_products, only: [:edit, :update, :show]
  before_action :check_date_of_payment, only: [:new]

  def payment_collection_pdf
   @voucher = PaymentCollections::ProduceVoucherForPaymentCollection.new(payment_collection: @payment_collection).execute!

    if @payment_collection.reversed?
      @reversed_voucher = Voucher.where(branch_id: @payment_collection.branch.id, book: "JVB", reference_number: @payment_collection.reversed_reference_number).first
    end
      


  end

  def download_wp_list
    @payment_collection = PaymentCollection.find(params[:payment_collection_id])
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Downloaded WP list", 
      email: current_user.email, 
      username: current_user.username, 
      record_reference_id: @payment_collection.id
    )

    filename = "#{@payment_collection.center.to_s}_#{@payment_collection.paid_at.strftime("%m-%d-%Y")}_wp_list.xlsx"
    report_package =  PaymentCollections::GenerateWpListForBilling.new(
                        payment_collection: @payment_collection
                      ).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def download_excel
    @payment_collection = PaymentCollection.find(params[:payment_collection_id])
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Downloaded payment collection #{@payment_collection.payment_type}", 
      email: current_user.email, 
      username: current_user.username, 
      record_reference_id: @payment_collection.id
    )

    filename = "#{@payment_collection.center.to_s}_#{@payment_collection.paid_at.strftime("%m-%d-%Y")}_billing.xlsx"
    report_package =  PaymentCollections::GenerateExcelForBilling.new(
                        payment_collection: @payment_collection
                      ).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def check_date_of_payment
    date_of_payment = params[:date_of_payment]
    current_date    = Date.today

    if date_of_payment.to_date < current_date
      flash[:error] = "Cannot generate billing. #{date_of_payment} < #{current_date}"
      redirect_to payment_collections_path
    end
  end

  def load_loan_products
    @loan_products  = @payment_collection.loan_products.order(:is_entry_point)
  end

  def load_types
    only_savings_on_payments = [1,2,6]
    @savings_types    = SavingsType.find(only_savings_on_payments)
    #@insurance_types  = InsuranceType.all.reverse
    @insurance_types  = InsuranceType.where("code IN (?)", ["LIF", "RF"]).reverse
    @equity_types     = EquityType.all
    @branches         = Branch.all
    @centers          = []
  end

  def load_record
    @payment_collection = PaymentCollection.find(params[:id])
  end

  def show
    @voucher = PaymentCollections::ProduceVoucherForPaymentCollection.new(payment_collection: @payment_collection).execute!

    if @payment_collection.reversed?
      @reversed_voucher = Voucher.where(branch_id: @payment_collection.branch.id, book: "JVB", reference_number: @payment_collection.reversed_reference_number).first
    end

    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying payment collection #{@payment_collection.payment_type}", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
  end

  def index
    @payment_collections = PaymentCollection.billing
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed all billing records", email: current_user.email, username: current_user.username)

    # Accessibility:
    #   - OAS and BK: pending only
    #   - SBK: all
    if ["OAS", "BK"].include? current_user.role
      @payment_collections = @payment_collections.where(status: "pending")
    end

    if params[:branch_id].present?
      @branch_id = params[:branch_id]
      @payment_collections = @payment_collections.where(branch_id: params[:branch_id])
    end

    if params[:center_id].present?
      @center_id = params[:center_id]
      @payment_collections = @payment_collections.where(center_id: params[:center_id])
    end

    if params[:status].present?
      @status = params[:status]
      @payment_collections = @payment_collections.where("status = ?", @status)
    end

    if params[:or_number].present?
      @or_number = params[:or_number]
      @payment_collections = @payment_collections.where("or_number = ?", @or_number)
    end

    if params[:start_date].present?
      @start_date = params[:start_date]
      @payment_collections = @payment_collections.where("paid_at >= ?", @start_date)
    end

    if params[:end_date].present?
      @end_date = params[:end_date]
      @payment_collections = @payment_collections.where("paid_at <= ?", @end_date)
    end

    @payment_collections = @payment_collections.order(:paid_at).page(params[:page]).per(20)
  end

  def new
    if !params[:branch_id].present? or !params[:center_id].present? or !params[:date_of_payment].present?
      flash[:error] = "No branch or center or date of payment defined"
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to create new billing. No branch or center or date of payment defined.", email: current_user.email, username: current_user.username)
      redirect_to payment_collections_path
    else
      branch = Branch.find(params[:branch_id])
      center = Center.find(params[:center_id])
      date_of_payment = params[:date_of_payment]
      prepared_by = current_user.full_name.upcase

      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to create new billing", email: current_user.email, username: current_user.username)
      @payment_collection = PaymentCollections::CreatePaymentCollectionForBilling.new(branch: branch, center: center, paid_at: date_of_payment, prepared_by: prepared_by).execute!

      if @payment_collection.save
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully created new billing", 
          email: current_user.email, 
          username: current_user.username, 
          record_reference_id: @payment_collection.id
        )

        redirect_to edit_payment_collection_path(@payment_collection)
      else
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Failed to create new billing. #{@payment_collection.errors.messages}", 
          email: current_user.email, 
          username: current_user.username
        )

        flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
        redirect_to payment_collections_path
      end
    end
  end

  def edit
    if @payment_collection.or_number.include?("CHANGE-ME")
      @payment_collection.or_number = ""
    end
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "About to edit billing", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
  end

  def update
    if @payment_collection.update(payment_collection_params)
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully updated billing", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @payment_collection.id
      )

      PaymentCollections::ComputePaymentCollectionValues.new(payment_collection: @payment_collection).execute!
      flash[:success] = "Successfully saved collection."
      redirect_to payment_collection_path(@payment_collection)
    else
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Failed to update billing", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @payment_collection.id
      )

      flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
      @errors = @payment_collection.errors.messages
      render :edit
    end
  end

  def destroy
    if !@payment_collection.pending?
      flash[:error] = "Cannot destroy non pending record"
      redirect_to payment_collection_path(@payment_collection)
    else
      @payment_collection.destroy!
      flash[:success] = "Successfully destroyed payment collection."
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully deleted billing", email: current_user.email, username: current_user.username)
      redirect_to payment_collections_path
    end
  end

  def payment_collection_params
    params.require(:payment_collection).permit!
  end
end
