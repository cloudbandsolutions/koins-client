class ReportsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults


  def project_type_category_excel               
    excel = Reports::GenerateExcelForListOfProjectType.execute!
    filename  = "cic_report.xlsx"   
                              
    excel.serialize "#{Rails.root}/tmp/#{filename}" 
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                            
  end



  def project_type_category
    @data = ::Reports::FetchListOfProjectCategory.new.execute!
  end



  #je
  def additional_shares
  branch_id = params[:branch_id]
  center = params[:center_id]
  start_date= params[:start_date]
  end_date= params[:end_date]
  @data = Reports::AdditionalShares.new(branch_id: branch_id, center_id: center, start_date: start_date,end_date: end_date).execute! 
  end

  def additional_shares_excel
    branch_id  = params[:branch_id]
    center_id  = params[:center_id]
    start_date = params[:start_date]
    end_date   = params[:end_date]
    excel = ::Reports::GenerateExcelForAdditionalShares.new(branch_id: branch_id,center_id: center_id,start_date: start_date, end_date: end_date).execute!
    filename = "List_of_additional_share.xlsx"
    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end
  #
  def migs 
    @migs_date = nil
    if params[:migs_date].present?
      @migs_date = params[:migs_date]
      @data = ::Reports::FetchMigsData.new(migs_date: @migs_date).execute!
    end
    
  end

  def migs_pdf
    @migs_date = nil
    if params[:migs_date].present?
      @migs_date = params[:migs_date]
      @data = ::Reports::FetchMigsData.new(migs_date: @migs_date).execute!
    end
  end

  def monthly_incentives
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    @data = ::Reports::FetchMonthlyIncentivesData.new.execute!
  end
  def monthly_incentives_pdf
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    #raise @start_date.inspect
    @data = ::Reports::FetchMonthlyIncentivesData.new(start_date: @start_date ,end_date: @end_date).execute!
    #render json: { data: @data }
  end


  def monthly_interest_pdf
    @data ::Reports::BookPdf.new.execute!    
  end

  def x_weeks_to_pay_pdf
    num_weeks       = params[:num_weeks]
    loan_product_id = params[:loan_product_id]

    loan_product    = LoanProduct.where(id: loan_product_id).first

    @data  = Reports::FetchXWeeksToPayResults.new(num_weeks: num_weeks, loan_product: loan_product ).execute!


  end

  def list_of_member_for_cic
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    @data = ::Reports::GenerateListOfMemberForCic.new(start_date: @start_date, end_date: @end_date).execute!
    
  end

  def list_of_member_for_cic_excel
    @start_date = params[:start_date]   
    @end_date = params[:end_date]
    
    #@data = ::Reports::GenerateListOfMemberForCic.new(start_date: @start_date, end_date: @end_date).execute!
    excel = ::Reports::GenerateExcelListForCic.new(start_date: @start_date, end_date: @end_date).execute!
      date_details= @end_date.to_date.strftime("%Y%m%d")
      time_details= Time.now.strftime("%H%M%S")
      date_time =  "#{date_details}#{time_details}"

      @provider_code = Settings.cic_provider_code
    
    filename =  "#{@provider_code}_CSDF_#{date_time}.xlsx"
  
    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
   # send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end


  def list_of_resign_details
    @resignation_type = params[:resignation_type]
    @resignation_reason = params[:resignation_reason]
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    #raise @resignation_reason.inspect
    @data = ::Reports::FetchListOfResignDetails.new(
              resignation_type:   @resignation_type,
              resignation_reason: @resignation_reason,
              start_date:          @start_date,
              end_date:           @end_date
            ).execute!

  end

  def list_of_resign_details_pdf
    @resignation_type = params[:resignation_type]
    @resignation_reason = params[:resignation_reason]
    @start_date = params[:start_date]
    @end_date = params[:end_date]

    #raise @resignation_reason.inspect
    @data = ::Reports::FetchListOfResignDetails.new(
              resignation_type:   @resignation_type,
              resignation_reason: @resignation_reason,
              start_date:          @start_date,
              end_date:           @end_date
            ).execute!

  end



  def list_of_resign
    @type_of_resign = params[:type_of_resign]
    @start_date     = params[:start_date]
    $end_date       = params[:end_date]

    @data           = ::Reports::FetchListOfResigns.new(
                        type_of_resign: @type_of_resign,
                        start_date: @start_date,
                        end_date: @end_date
                      ).execute!
  end
  
   def list_of_resign_pdf
    @type_of_resign = params[:type_of_resign]
    @start_date     = params[:start_date]
    $end_date       = params[:end_date]

    @data           = ::Reports::FetchListOfResigns.new(
                        type_of_resign: @type_of_resign,
                        start_date: @start_date,
                        end_date: @end_date
                      ).execute!
  end

  def ppi
    @data = ::Reports::FetchPpiReports.new.execute! 
  end

  def ppi_pdf
    @data = ::Reports::FetchPpiReports.new.execute!
  end

  def drop_out_rate_by_month
    @current_working_date = ApplicationHelper.current_working_date
    @records              = nil
    @officers_in_charge   = Center.all.pluck(:officer_in_charge).uniq.sort
    @current_year         = @current_working_date.year
    @current_month        = @current_working_date.month
    @officer_in_charge    = nil

    if params[:current_month].present?
      @current_month  = params[:current_month].to_i
    end

    if params[:current_year].present?
      @current_year = params[:current_year].to_i
    end

    if params[:officer_in_charge].present?
      @officer_in_charge  = params[:officer_in_charge]
    end

    @records  = ::Special::Reports::DropOutMembersByMonth.new(
                  month: @current_month,
                  year: @current_year,
                  officer_in_charge: @officer_in_charge
                ).execute!
  end

  def mfi_new_and_resigned_detailed
    month = params[:month]
    year  = params[:year]

    @data = ::MonthlyReports::FetchNewAndResignedByMonthAndYear.new(
              month: month,
              year: year
            ).execute!
  end

  def mfi_new_and_resigned_member_counts
    @c_working_date = ApplicationHelper.current_working_date
    @data           = ::MonthlyReports::FetchMfiNewAndResignedMonthlyCounts.new(
                        year: @c_working_date.year, branch: Branch.last
                      ).execute!
  end

  def resigned_from_microinsurance
    @members = Member.resigned_from_microinsurance
  end

  def old_members
    @members  = Member.active.where("")
  end

  def missing_insurance_transactions
    filename = "missing_insurance_transactions.xlsx"
    report_package = Reports::GenerateExcelForMissingInsuranceTransactions.new.execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"

    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

  end


  def withdrawals
    @members = Member.active.order("last_name ASC")
    # @members = Member.active.joins(insurance_accounts: :insurance_account_transactions).where("insurance_account_transactions.transaction_type = ?", "withdraw")
    # if @start_date = params[:start_date].present? && @end_date = params[:end_date].present?
    #   @branch = params[:branch_id]
    #   @members = Member.active.joins(insurance_accounts: :insurance_account_transactions).where("insurance_account_transactions.transaction_type = ? AND transacted_at >= ? AND transacted_at <= ?", "withdraw", @start_date, @end_date)
    # end
  end

  def insured_loans
    # @data = Insurance::FetchInsuredLoans.new(start_date: @start_date, end_date: @end_date, loan_status: @loan_status).execute!

    if params[:start_date].present? and params[:end_date].present? and params[:loan_status].present? and params[:branch_id].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]
      @loan_status = params[:loan_status]
      @branch_id = params[:branch_id]

      @data = Insurance::FetchInsuredLoans.new(start_date: @start_date, end_date: @end_date, loan_status: @loan_status, branch_id: @branch_id).execute!
    end

    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed all insured loans", email: current_user.email, username: current_user.username)
  end

  def seriatim_report
    as_of = params[:as_of]
    branch = params[:branch]
  
    excel = Reports::GenerateSeriatimReportExcel.new(as_of: as_of, branch: branch,).execute!
    filename  = "seriatim_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def collections_clip_report
    branch = params[:branch]
    start_date = params[:start_date]
    end_date = params[:end_date]
  
    excel = Reports::GenerateCollectionsClipReportExcel.new(branch: branch, start_date: start_date, end_date: end_date).execute!
    filename  = "collections_clip_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def collections_blip_report
    branch = params[:branch]
    start_date = params[:start_date]
    end_date = params[:end_date]
  
    excel = Reports::GenerateCollectionsBlipReportExcel.new(branch: branch, start_date: start_date, end_date: end_date).execute!
    filename  = "collections_blip_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def validations_report
    branch = params[:branch]
    status = params[:status]
    start_date = params[:start_date]
    end_date = params[:end_date]
  
    excel = Reports::GenerateValidationsReportExcel.new(branch: branch, status: status, start_date: start_date, end_date: end_date).execute!
    filename  = "#{status}_validations_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def claims_report
    branch = params[:branch]
    category_of_cause_of_death_tpd_accident = params[:category_of_cause_of_death_tpd_accident]
    type_of_insurance_policy = params[:type_of_insurance_policy]
    classification_of_insured = params[:classification_of_insured]
    start_date = params[:start_date]
    end_date = params[:end_date]
  
    excel = Reports::GenerateClaimsReportExcel.new(branch: branch, category_of_cause_of_death_tpd_accident: category_of_cause_of_death_tpd_accident, type_of_insurance_policy: type_of_insurance_policy, classification_of_insured: classification_of_insured, start_date: start_date, end_date: end_date).execute!
    filename  = "BLIP_claims_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def clip_claims_report
    branch = params[:branch]
    cause_of_death = params[:cause_of_death]
    type_of_loan = params[:type_of_loan]
    start_date = params[:start_date]
    end_date = params[:end_date]
  
    excel = Reports::GenerateClipClaimsReportExcel.new(branch: branch, type_of_loan: type_of_loan, start_date: start_date, end_date: end_date).execute!
    filename  = "CLIP_claims_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def member_dependent_report
    start_date = params[:start_date]
    end_date = params[:end_date]
    branch = params[:branch]
    branch_name = Branch.where(id: branch).first.name
  
    excel = Reports::GenerateMemberDependentReportExcel.new(start_date: start_date, end_date: end_date, branch: branch).execute!
    filename  = "#{branch_name}_member_dependent_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def personal_documents_report
    start_date = params[:start_date]
    end_date = params[:end_date]
    branch = params[:branch]
    branch_name = Branch.where(id: branch).first.name
  
    excel = Reports::GeneratePersonalDocumentsReportExcel.new(start_date: start_date, end_date: end_date, branch: branch).execute!
    filename  = "#{branch_name}_personal_documents_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def cic
  end

  def cic_report
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    @provider_code = params[:provider_code]

    excel = Reports::GenerateCicReportExcel.new(provider_code: @provider_code, start_date: @start_date, end_date: @end_date).execute!
    filename  = "cic_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def print_insured_loans
    @data = Insurance::FetchInsuredLoans.new(start_date: @start_date, end_date: @end_date, loan_status: @loan_status, branch_id: params[:branch_id]).execute!

    if params[:start_date].present? and params[:end_date].present? and params[:loan_status].present? and params[:branch_id].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]
      @loan_status = params[:loan_status]
      @branch     = Branch.find(params[:branch_id])

      @data = Insurance::FetchInsuredLoans.new(start_date: @start_date, end_date: @end_date, loan_status: @loan_status, branch_id: @branch.id).execute!
    end

    filename = "#{@branch}_insured_loans.xlsx"
    report_package = Reports::GenerateExcelForInsuredLoans.new(data: @data, start_date: @start_date, end_date: @end_date, loan_status: @loan_status, branch_id: @branch.id).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"

    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Downloaded insured loans", email: current_user.email, username: current_user.username)

    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def download_csv_insured_loans
    @data = Insurance::FetchInsuredLoans.new(start_date: @start_date, end_date: @end_date, loan_status: @loan_status, branch_id: params[:branch_id]).execute!

    if params[:start_date].present? and params[:end_date].present? and params[:loan_status].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]
      @loan_status = params[:loan_status]
      @branch     = Branch.find(params[:branch_id])

      @data = Insurance::FetchInsuredLoans.new(start_date: @start_date, end_date: @end_date, loan_status: @loan_status, branch_id: @branch.id).execute!
    end

    send_data Reports::GenerateInsuredLoansCsv.new(data: @data).execute!, :type => 'text/csv; charset=utf-8; header=present', :disposition => "attachment; filename=#{@branch}_insured_loans.csv"
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully export download csv insured loans information.", email: current_user.email, username: current_user.username)
  end

  def index
  end

  def soa_funds
  end

  def loan_insurance_list
    @records  = Loan.active
  end

  def monthly_rr
  end


  def master_list_of_client_loans_pdf
        branch_id       = params[:branch_id]
        center_id       = params[:center_id]
        loan_product_id = params[:loan_product_id]
        as_of           = params[:as_of]

        if as_of.blank?
          as_of = ApplicationHelper.current_working_date
        end

        @data = Reports::GenerateMasterListOfClientLoans.new(
                          branch_id: branch_id,
                          center_id: center_id,
                          loan_product_id: loan_product_id,
                          as_of: as_of
                        ).execute!

  
  end


  def master_list_of_client_loans
  end

  def download_member_registry_excel
    filename        = "member_registry_#{Branch.first.to_s}.xlsx"
    report_package  = ::Reports::GenerateExcelForMemberRegistry.new.execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Downloaded member registry", 
      email: current_user.email, 
      username: current_user.username
    )

    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def loan_master_list
    @loans = Loan.active

    if params[:q].present?
      @q = params[:q]
      @loans = @loans.where("loans.pn_number LIKE :q OR lower(members.first_name) LIKE :q OR lower(members.last_name) LIKE :q", q: "%#{@q.downcase}%")
    end

    if params[:loan_product_id].present?
      @loan_product_id = params[:loan_product_id]
      @loan_product = LoanProduct.find(@loan_product_id)
      @loans = @loans.where(loan_product_id: @loan_product_id)
    end

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @loans = @loans.where(date_approved: @start_date..@end_date)
    end
  end

  def summary_of_loan_release
    @loans = Loan.active_and_paid

    if params[:q].present?
      @q = params[:q]
      @loans = @loans.where("loans.pn_number LIKE :q OR lower(members.first_name) LIKE :q OR lower(members.last_name) LIKE :q", q: "%#{@q.downcase}%")
    end

    if params[:loan_product_id].present?
      @loan_product_id = params[:loan_product_id]
      @loan_product = LoanProduct.find(@loan_product_id)
      @loans = @loans.where(loan_product_id: @loan_product_id)
    end

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @loans = @loans.where(date_approved: @start_date..@end_date)
    end
  end

  def summary_of_loan_release_pdf
    @branch = Branch.first.name
    @loans = Loan.active_and_paid

    if params[:q].present?
      @q = params[:q]
      @loans = @loans.where("loans.pn_number LIKE :q OR lower(members.first_name) LIKE :q OR lower(members.last_name) LIKE :q", q: "%#{@q.downcase}%")
    end

    if params[:loan_product_id].present?
      @loan_product_id = params[:loan_product_id]
      @loan_product = LoanProduct.find(@loan_product_id)
      @loans = @loans.where(loan_product_id: @loan_product_id)
    end

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @loans = @loans.where(date_approved: @start_date..@end_date)
    end
  end


  def or
    @vouchers = Voucher.approved_receipts

    if params[:q].present?
      @q = params[:q]
      @vouchers = @vouchers.where("or_number LIKE ?", "%#{@q}%")
    end

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @vouchers = @vouchers.where(date_prepared: @start_date..@end_date)
    end

    @vouchers = @vouchers.page(params[:page])
  end

  def accounts
    @savings_types = SavingsType.all
    @insurance_types = InsuranceType.all
    @equity_types = EquityType.all
  end

  def active_loaners
  end

  def portfolio
    @loan_products = LoanProduct.all
  end

  def print_checks
    @checks = Voucher.approved.with_checks

    if params[:branch_id].present?
      @branch = Branch.find(params[:branch_id])
      @checks = @checks.where(branch_id: @branch.id)
    end

    if params[:q].present?
      @q = params[:q]
      @checks = @checks.where("check_number LIKE :q", q: "%#{@q}%")
    end

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @checks = @checks.where(date_of_check: @start_date..@end_date)
    end

    if params[:start_number].present? and params[:end_number].present?
      @start_number = params[:start_number].to_i
      @end_number = params[:end_number].to_i

      @checks = @checks.where(check_number: @start_number..@end_number)
    end

    filename = "checks.xlsx"
    report_package = Reports::GenerateExcelForChecks.new(checks: @checks).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"

    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Downloaded checks", email: current_user.email, username: current_user.username)

    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def checks
    @branches = Branch.all
    @checks = Voucher.approved.with_checks

    if params[:q].present?
      @q = params[:q]
      @checks = @checks.where("check_number LIKE :q", q: "%#{@q}%")
    end

    if params[:branch_id].present?
      @branch = Branch.find(params[:branch_id])
      @checks = @checks.where(branch_id: @branch.id)
    end

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @checks = @checks.where(date_of_check: @start_date..@end_date)
    end

    if params[:start_number].present? and params[:end_number].present?
      @start_number = params[:start_number].to_i
      @end_number = params[:end_number].to_i

      @checks = @checks.where(check_number: @start_number..@end_number)
    end

    @checks = @checks.page(params[:page]).per(20)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed all checks records", email: current_user.email, username: current_user.username)
  end

  def load_defaults
    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]
    end

    if params[:branch_id].present?
      @branch = Branch.find(params[:branch_id])
    end

    # if current_user.role == "MIS"
    #   @branches = Branch.all
    # else
    #   @branches = current_user.branches
    # end
    @branches = Branch.all
  end
 
  def par_pdf
    branch_id       = params[:branch_id]
    so              = params[:so]
    center_id       = params[:center_id]
    loan_product_id = params[:loan_product_id]
    as_of           = params[:as_of]
    detailed        = params[:detailed]

    @data = Reports::GeneratePar.new(branch_id: branch_id, so: so, center_id: center_id, loan_product_id: loan_product_id, as_of: as_of, detailed: detailed).execute!

  end


  def personal_funds_pdf
    branch_id     = params[:branch_id]
    so            = params[:so]
    center_id     = params[:center_id]
    account_types = params[:account_types]
    as_of         = params[:as_of]
    detailed      = params[:detailed]

    @data = ::Reports::PersonalFunds.new(
              branch_id: branch_id, 
              so: so, 
              center_id: center_id, 
              account_types: account_types,
              as_of: as_of,
              detailed: detailed
            ).execute!
  end

  def personal_funds
    if params[:download].present?
      branch_id     = params[:branch_id]
      so            = params[:so]
      center_id     = params[:center_id]
      account_types = params[:account_types]
      as_of         = params[:as_of]
      detailed      = params[:detailed]

      data = Reports::PersonalFunds.new(
              branch_id: branch_id, 
              so: so, 
              center_id: center_id, 
              account_types: account_types,
              as_of: as_of,
              detailed: detailed
            ).execute!

      filename = "personal_funds.xlsx"
      report_package = Reports::PersonalFundsExcel.new(data: data, detailed: detailed).execute!
      report_package.serialize "#{Rails.root}/tmp/#{filename}"

      send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    else
      render "reports/personal_funds"
    end
  end

  def member_reports
    if params[:download].present?
      branch_id     = params[:branch_id]
      insurance_status = params[:insurance_status]
      member_type = params[:member_type]
      member_status = params[:member_status]
      start_date    = params[:start_date]
      end_date      = params[:end_date]

      data = Reports::MemberReports.new(
                  branch_id: branch_id,
                  member_status: member_status,
                  member_type: member_type,
                  insurance_status: insurance_status,
                  start_date: start_date,
                  end_date: end_date
                ).execute!

      filename = "member_report.xlsx"
      report_package = Reports::MemberReportsExcel.new(data: data, member_status: member_status, start_date: start_date, end_date: end_date).execute!
      report_package.serialize "#{Rails.root}/tmp/#{filename}"

      send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    else
      render "reports/member_reports"
    end
  end

  def member_quarterly_reports
    if params[:download].present?
      start_date    = params[:start_date]
      end_date      = params[:end_date]

      data = Reports::MemberQuarterlyReports.new(
                  start_date: start_date,
                  end_date: end_date
                ).execute!

      filename = "member_quarterly_report.xlsx"
      report_package = Reports::MemberQuarterlyReportsExcel.new(
                  data: data, 
                  start_date: start_date,
                  end_date: end_date
                  ).execute!
      report_package.serialize "#{Rails.root}/tmp/#{filename}"

      send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    else
      render "reports/member_quarterly_reports"
    end
  end

  def collection_reports
    if params[:download].present?
      branch_id     = params[:branch_id]
      start_date    = params[:start_date]
      end_date      = params[:end_date]

      data = Reports::CollectionReports.new(
                  branch_id: branch_id,
                  start_date: start_date,
                  end_date: end_date
                ).execute!

      filename = "collection_report.xlsx"
      report_package = Reports::CollectionReportsExcel.new(data: data, branch_id: branch_id, start_date: start_date, end_date: end_date).execute!
      report_package.serialize "#{Rails.root}/tmp/#{filename}"

      send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    else
      render "reports/collection_reports"
    end
  end

  def summary_of_certificates_and_policies
    if params[:download].present?
      branch_id     = params[:branch_id]
      plan_type = params[:plan_type]
      as_of      = params[:as_of]

      data = Reports::SummaryOfCertificatesAndPolicies.new(
                  branch_id: branch_id,
                  plan_type: plan_type,
                  as_of: as_of
                ).execute!

      filename = "summary_of_certificates_and_policies.xlsx"
      report_package = Reports::SummaryOfCertificatesAndPoliciesExcel.new(data: data, branch_id: branch_id, as_of: as_of, plan_type: plan_type).execute!
      report_package.serialize "#{Rails.root}/tmp/#{filename}"

      send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    else
      render "reports/summary_of_certificates_and_policies"
    end
  end

  def repayments_pdf
    branch_id       = params[:branch_id]
    so              = params[:so]
    center_id       = params[:center_id]
    loan_product_id = params[:loan_product_id]
    as_of           = params[:as_of]
    detailed        = params[:detailed]

    @data  = Reports::GenerateRepayments.new(
            branch_id: branch_id, 
            so: so, 
            center_id: center_id, 
            loan_product_id: loan_product_id, 
            as_of: as_of,
            detailed: detailed
            ).execute!
  end

  def repayments
    @loan_products = LoanProduct.all
  end

  def par
    @loan_products = LoanProduct.all
  end

  def aging_of_receivables
    @loan_products = LoanProduct.all
  end


  def loans_statement_of_accounts
  end

  def expenses_statement_of_assets
  end

  def funds_statement_of_accounts
  end

  def balance_sheet
  end

  # SNAPSHOTS
  def cluster_snapshots
    @snapshots = Snapshot.select("*").order("snapped_at DESC")
  end
end
