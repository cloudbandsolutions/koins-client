class PagesController < ApplicationController
  before_action :load_defaults, :authenticate_user!, except: [:next_closing_time]

  def transferred_patronage_refund_data
    patronage_refund_id = params[:patronage_refund_id]
    raise "jef".inspect
  end



  def transferred_patronage_refund
    @year = PatronageRefundCollection.where(status: "approved") 
  
  end
  
  def transferred_icpr_member_data
    share_capital_interest_id = params[:share_capital_interest_id]

    @data = ::Tools::FetchInterestOnShareCapitalDetails.new(share_capital_interest_id: share_capital_interest_id).execute!

    @voucher = ::Tools::GenerateAccountingEntryForTransferInterestOnShareCapital.new(interest_on_share_capital_id: 2, user: current_user).execute!
    render json: { data: @data }
  end

  def transferred_icpr_member
    @year = InterestOnShareCapitalCollection.where(status: "approved")
   # @voucher = ::Tools::GenerateAccountingEntryForTransferInterestOnShareCapital.new(interest_on_share_capital_id: 2, user: current_user).execute!

  end
  def transferred_interest_on_sharecapital_excel

    share_capital_interest_id = params[:id]
    excel = ::Tools::GenerateInterestOnShareCapitalDetailsExcel.new(share_capital_interest_id: share_capital_interest_id).execute!
    filename = "transferred_interest_on_share_capital.xlsx"
    excel.serialize "#{Rails.root}/tmp/#{filename}"

    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end


  def x_weeks_to_pay_pdf
    @data = ::Reports::GenerateXWeeksToPayPdf.new.execute!
  end

  def list_so_member_stats_details
    @so_name = params[:so_name]
    @stat_type = params[:stat_type]
    @data = ::Reports::FetchSoMemberStatsDetails.new(so_name: @so_name, stat_type: @stat_type).execute!
  end


  def list_of_lp_cycle_details
   @loan_product_id = params[:loan_product_id]
   @loan_cycle      = params[:loan_cycle]
   #raise @loan_cycle.inspect
  @data = ::Dashboard::GenerateLpCycleCountDetails.new(loan_product_id: @loan_product_id,loan_cycle: @loan_cycle).execute!

  end

  def double_fund_transfer
  end

  def invalid_memberships
    @invalid_memberships  = ::Cooperative::FetchInvalidMemberships.new.execute!
  end

  def year_end_records
    @year_end_closing_records = YearEndClosingRecord.select("*").order("year ASC")
  end

  def year_end_record
    @year_end_closing_record  = YearEndClosingRecord.find(params[:id])
  end

  def actions
  end

  def download_backup
    if !["MIS", "AM", "SBK", "BK", "FM", "CM"].include? current_user.role
      flash[:error] = "Only MIS can access this feature"
      redirect_to root_path
    else
      destination_directory = "#{Rails.root}/db_backup"
      filename              = "#{Settings.system_name}-#{Time.now.to_i.to_s}-#{ENV['RAILS_ENV']}.dump"
      destination_file      = "#{destination_directory}/#{filename}"

      pw    = ::ActiveRecord::Base.connection_config[:password]
      host  = ::ActiveRecord::Base.connection_config[:host]
      user  = ::ActiveRecord::Base.connection_config[:username]
      db    = ::ActiveRecord::Base.connection_config[:database]
      cmd = "PGPASSWORD=#{pw} pg_dump --host #{host} --username #{user} --verbose --clean --no-owner --no-acl --format=c #{db} > #{destination_file}"
      `#{cmd}`
      
      #`bash #{Rails.root}/backup_to_drive.sh`

      send_file destination_file, filename: "#{filename}"
    end
  end

  def pure_savers
    @active_loan_entry_points     = Loan.active
    @active_members               = Member.pure_active
    @pending_members              = Member.pending

    @member_ids_with_positive_savings         = SavingsAccount.where("balance >= 0").pluck(:member_id).uniq
    @member_ids_with_active_entry_point_loans = @active_loan_entry_points.pluck(:member_id)
    @member_ids_active_only                   = @member_ids_with_positive_savings | @member_ids_with_active_entry_point_loans

    @members = @active_members.where(id: @member_ids_with_positive_savings).where.not(id: @member_ids_with_active_entry_point_loans)
  end

  def resigned_members
    @members = Member.resigned.order("last_name ASC")
  end

  def active_loaners
    @active_loan_entry_points     = Loan.active
    @active_members               = Member.active
    @pending_members              = Member.pending

    @member_ids_with_positive_savings         = SavingsAccount.where("balance >= 0").pluck(:member_id).uniq
    @member_ids_with_active_entry_point_loans = @active_loan_entry_points.pluck(:member_id)
    @member_ids_active_only                   = @member_ids_with_positive_savings | @member_ids_with_active_entry_point_loans

    @members = @active_members.where(id: @member_ids_with_active_entry_point_loans)
  end

  def active_members
    @active_loan_entry_points     = Loan.active
    @active_members               = Member.active
    @pending_members              = Member.pending

    @member_ids_with_positive_savings         = SavingsAccount.where("balance >= 0").pluck(:member_id).uniq
    @member_ids_with_active_entry_point_loans = @active_loan_entry_points.pluck(:member_id)
    @member_ids_active_only                   = @member_ids_with_positive_savings | @member_ids_with_active_entry_point_loans

    @members = @active_members.where(id: @member_ids_active_only)
  end
  
  def pending_members
    @active_loan_entry_points     = Loan.active
    @active_members               = Member.active
    @pending_members              = Member.pending

    @member_ids_with_positive_savings         = SavingsAccount.where("balance >= 0").pluck(:member_id).uniq
    @member_ids_with_active_entry_point_loans = @active_loan_entry_points.pluck(:member_id)
    @member_ids_active_only                   = @member_ids_with_positive_savings | @member_ids_with_active_entry_point_loans

    @members = @pending_members
  end

  def members_for_reinsurance
    @members    = Insurance::FetchMembersForReinsurance.new.execute!
  end

  def insurance_exit_age_members
    @members    = Member.pure_active.where("date_of_birth::date <= ? AND member_type = ?", 774.months.ago, "Regular").order("branch_id ASC")
    @gk_members = Member.pure_active.where(
                    "id IN (?) AND date_of_birth::date <= ? AND member_type IN (?)",
                    InsuranceAccount.joins(:member).where("balance > 0").pluck(:member_id).uniq,
                    774.months.ago,
                    ["GK"]
                  )
  end


  def insurance_exit_age_members_pdf
    @members    = Member.active.where("date_of_birth::date <= ? AND member_type = ?", 774.months.ago, "Regular").order("branch_id ASC")
    @gk_members = Member.pure_active.where(
                    "id IN (?) AND date_of_birth::date <= ? AND member_type IN (?)",
                    InsuranceAccount.joins(:member).where("balance > 0").pluck(:member_id).uniq,
                    774.months.ago,
                    ["GK"]
                  )
  end

  def invalid_members
    @members_mii = Member.pure_active.where(previous_mii_member_since: nil)
    @members_mfi = Member.pure_active.where(previous_mfi_member_since: nil)
  end

  def index
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Visited / (dashboard)", email: current_user.email, username: current_user.username)
  end
  
  def load_defaults
    @members = Member.select("*")
  end

  def dashboard
    @members = Member.all
  end

  def missing_accounts
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed Missing Accounts", email: current_user.email, username: current_user.username)
  end

  def developer
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed Developer", email: current_user.email, username: current_user.username)
  end

  def export_tools
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed Export Tools", email: current_user.email, username: current_user.username)
  end

  def debug_invalid_loan_maintaining_balance
    @invalid_loans = Loans::FetchInvalidMaintainingBalanceLoans.new.execute!
  end

  def debug_invalid_loans
    @invalid_loans = []
    Loan.all.each do |loan|
      if !loan.valid?
        @invalid_loans << loan
      end
    end
  end

  def debug_unbalanced_loans
    @unbalanced_loans = []
    Loan.all.each do |loan|
    end
  end

  def get_flashes
    flash_results = []

    flash.each do |f|
      flash_results << f
    end

    render json: flash_results.to_json
  end

  def upload_loan_insurance
    @loan_insurance_types = LoanInsuranceType.all
  end

  def upload_insurance_fund_transfer
  end

  def upload_deposit
  end

  def next_closing_time
    closing_time = Time.now
    closing_hour  = Settings.cut_off_time_hour
    closing_min   = Settings.cut_off_time_minute
    if Time.now.hour > closing_hour
      closing_time = (Time.now + 1.day)
      closing_time = Time.zone.local(closing_time.year, closing_time.month, closing_time.day, closing_hour, closing_min)
    else
      closing_time = Time.zone.local(closing_time.year, closing_time.month, closing_time.day, closing_hour, closing_min)
    end

    render json: { closingTime: closing_time.strftime("%Y/%m/%d %H:%M:00") }
  end

  def inforce_members
    @members = Member.pure_active.where(insurance_status: "inforce").order("last_name ASC")
  end

  def lapsed_members
    @members = Member.pure_active.where(insurance_status: "lapsed").order("last_name ASC")
  end

  def reinstatement
    
  end
  
  #def general_pdf
    #@data = ::GeneralLedger::GenerateGeneralPdf.new.execute!
  #end
end
