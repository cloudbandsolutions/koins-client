module Account
  class UsersController < ApplicationController
    before_action :authenticate_user!

    def show
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed user profile", email: current_user.email, username: current_user.username)
    end

    def edit
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed edit user", email: current_user.email, username: current_user.username)
    end

    def update
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to update user", email: current_user.email, username: current_user.username)

      if current_user.update(user_params)
        flash[:success] = "Successfully updated profile"
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successful update of user", email: current_user.email, username: current_user.username)
        redirect_to root_path
      else
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Unsuccessful update of user", email: current_user.email, username: current_user.username)
        flash.now[:error] = "Cannot save user"
        render :edit
      end
    end

    def user_params
      params.require(:user).permit!
    end
  end
end
