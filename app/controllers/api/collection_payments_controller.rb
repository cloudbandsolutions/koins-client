module Api
  class CollectionPaymentsController < ApplicationController
    def index
    end

    def update
      collection_payment = CollectionPayment.find(params[:id])

      hash_object = ActiveSupport::JSON.decode(params[:collection_payment].to_json)

      if collection_payment.update(hash_object)
        render json: collection_payment
      else
        render json: { errors: collection_payment.errors.messages }
      end
    end

    def create
    end
  end
end
