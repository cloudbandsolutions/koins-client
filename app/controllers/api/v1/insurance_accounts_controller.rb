module Api
  module V1
    class InsuranceAccountsController < ApplicationController
      def rehash
        insurance_account  = InsuranceAccount.find(params[:insurance_account_id])
        insurance_account  = ::Insurance::RehashAccount.new(insurance_account: insurance_account).execute!

        render json: { message: "success" }
      end
    end
  end
end
