module Api
  module V1
    module Insurance
      class MembersController < ApplicationController
        before_action :authenticate_user!

        def resign
          member        = Member.where(id: params[:member_id]).first
          date_resigned = params[:date_resigned]
          reason        = params[:reason]
          errors        = ::Insurance::Members::ValidateResign.new(
                            member: member,
                            date_resigned: date_resigned
                          ).execute!

          if errors.size == 0
            ::Insurance::Members::Resign.new(
              member: member,
              date_resigned: date_resigned,
              reason: reason,
              resigned_by: current_user.full_name
            ).execute!

            render json: { id: member.id }
          else
            render json: { errors: errors }, status: 402
          end
        end

        def balik_kasapi_mii
          member        = Member.where(id: params[:member_id]).first
          new_recognition_date = params[:new_recognition_date]
          errors        = ::Insurance::Members::ValidateBalikKasapiMii.new(
                            member: member,
                            new_recognition_date: new_recognition_date
                          ).execute!

          if errors.size == 0
            ::Insurance::Members::BalikKasapiMii.new(
              member: member,
              new_recognition_date: new_recognition_date,
              balik_kasapi_by: current_user.full_name
            ).execute!

            render json: { id: member.id }
          else
            render json: { errors: errors }, status: 402
          end
        end

        def change_recognition_date
          member        = Member.where(id: params[:member_id]).first
          new_mii_recognition_date = params[:new_mii_recognition_date]
          errors        = ::Insurance::Members::ValidateChangeOfRecognitionDate.new(
                            member: member,
                            new_mii_recognition_date: new_mii_recognition_date
                          ).execute!

          if errors.size == 0
            ::Insurance::Members::ChangeOfRecognitionDate.new(
              member: member,
              new_mii_recognition_date: new_mii_recognition_date
            ).execute!

            render json: { id: member.id }
          else
            render json: { errors: errors }, status: 402
          end
        end
      end
    end 
  end
end