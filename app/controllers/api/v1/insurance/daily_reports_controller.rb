module Api
  module V1
    module Insurance
      class DailyReportsController < ApiController
        before_action :authenticate_user!

        def generate_daily_report_insurance_account_status
          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Generating insurance account status report", 
            email: current_user.email, 
            username: current_user.username
          )

          if params[:branch_id].blank?
            render json: { errors: ["Branch ID required"] }, status: 401
          else
            branch  = Branch.find(params[:branch_id])
            as_of   = ApplicationHelper.current_working_date
            report  = ::Reports::GenerateDailyReportInsuranceAccountStatuses.new(
                        branch: branch,
                        as_of: as_of
                      ).execute!

            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully generated insurance account status report", 
              email: current_user.email, 
              username: current_user.username,
              record_reference_id: report.id
            )
            render json: { id: report.id }
          end
        end
      end
    end
  end
end
