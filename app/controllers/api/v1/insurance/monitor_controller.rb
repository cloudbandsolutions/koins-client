module Api
  module V1
    module Insurance
      class MonitorController < ApiController
        before_action :authenticate_user!

        def priority_insurance_accounts
          data  = ::Insurance::FetchOverbalancedInsuranceAccounts.new.execute!

          render json: data
        end
      end
    end
  end
end
