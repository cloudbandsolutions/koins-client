module Api
  module V1
    class SyncLoanProductsController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:loan_products] = LoanProduct.all

        render json: { success: true, info: "Loan products", data: data }
      end
    end
  end
end
