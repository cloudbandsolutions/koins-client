module Api
  module V1
    class MemberCertificatesController < ApplicationController
      def flag_for_printing
        member_certificate  = MemberCertificate.find(params[:id])
        member_certificate.update!(
          is_printed: true,
          date_printed: ApplicationHelper.current_working_date
        )

        render json: { member_certificate: member_certificate }
      end
    end
  end
end
