module Api
  module V1
    class FeedbacksController < ApplicationController
      def create
        if params[:member_id].present?
          m = Member.find(params[:member_id])

          if !params[:feedback_option_id].blank?
            feedback_option = FeedbackOption.find(params[:feedback_option_id])
            Feedback.create!(member: m, content: feedback_option.content)

            if params[:others].present?
              Feedback.create!(member: m, content: params[:others])
            end

            render json: { message: "success" }
          elsif !params[:others].blank?
            Feedback.create!(member: m, content: params[:others])
            render json: { message: "success" }
          else
            render json: { message: "Feedback required" }, status: :unprocessable_entity
          end
        else
          render json: { message: "Member required" }, status: :unprocessable_entity
        end
      end
      def show

      end
    end
  end
end
