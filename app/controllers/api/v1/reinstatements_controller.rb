module Api
  module V1
    class ReinstatementsController < ApplicationController
      before_action :authenticate_user!

      def generate_transaction
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Generating reinstatement transaction", email: current_user.email, username: current_user.username)
        branch_id              = params[:branch_id] 
        date_prepared          = params[:date_prepared]
        prepared_by            = current_user.full_name

        errors = Reinstatements::ValidateNewReinstatementTransaction.new(branch_id: branch_id, date_prepared: date_prepared).execute!

        if errors.length == 0
          reinstatement = Reinstatements::CreateReinstatement.new(branch: Branch.find(branch_id), date_prepared: date_prepared, prepared_by: prepared_by).execute!

          if reinstatement.valid?
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully created reinstatement transaction.", email: current_user.email, username: current_user.username)
            reinstatement.save!
            render json: { messages: ["Successfully created transaction. Redirecting..."], reinstatement_id: reinstatement.id }
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to create reinstatement transaction.", email: current_user.email, username: current_user.username)
            errors << "Something went wrong"
            reinstatement.errors.messages.each do |m|
              errors << m
            end
            render json: { errors: errors } , status: 401
          end
        else
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Error in generating reinstatement transaction", email: current_user.email, username: current_user.username)
          render json: { errors: errors } , status: 401
        end
      end
      
      def delete_reinstatement_record
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Deleting reinstatement record", email: current_user.email, username: current_user.username)
        reinstatement_record = ReinstatementRecord.find(params[:reinstatement_record_id])
        reinstatement = reinstatement_record.reinstatement
        reinstatement_record.destroy!
        reinstatement.update!(updated_at: Time.now)
        render json: { message: "ok" }
      end

      def add_member
        reinstatement = Reinstatement.find(params[:id])
        member = Member.find(params[:member_id])
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Adding member #{member.to_s} to collection", email: current_user.email, username: current_user.username)
        reinstatement_record = Reinstatements::AddMemberToReinstatement.new(reinstatement: reinstatement, member: member).execute!
        reinstatement_record.save!
        render json: { message: "Successfully added member" }
      end

      def approve
        reinstatement = Reinstatement.find(params[:id])
        errors = []
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to approve reinstatement)", email: current_user.email, username: current_user.username)

        if ["MIS", "OM"].include? current_user.role
          errors = Reinstatements::ValidateReinstatementForApproval.new(reinstatement: reinstatement).execute!
          if errors.size == 0
            reinstatement = Reinstatements::ApproveReinstatement.new(reinstatement: reinstatement, user:  current_user).execute!
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved reinstatement)", email: current_user.email, username: current_user.username)
            render json: { message: "Successfully approved reinstatement" }
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve reinstatement", email: current_user.email, username: current_user.username)
            render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"
          render json: { message: "Unauthorized", errors: errors }, status: 401
        end
      end

      def reverse
        reinstatement = Reinstatement.find(params[:id])
        errors = []
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to reverse reinstatement #{reinstatement.branch}.", email: current_user.email, username: current_user.username, record_reference_id: reinstatement.id)
        
        if ["MIS", "OM"].include? current_user.role
          errors = Reinstatements::ValidateReinstatementForReversal.new(reinstatement: reinstatement).execute!
          if errors.size == 0
            Reinstatements::ReverseReinstatement.new(reinstatement: reinstatement, user: current_user).execute!
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully reversed reinstatement #{reinstatement.branch}", email: current_user.email, username: current_user.username, record_reference_id: reinstatement.id)
            render json: { message: "success" }
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to reverse reinstatement #{reinstatement.branch}", email: current_user.email, username: current_user.username, record_reference_id: reinstatement.id)
            render json: { message: "error", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"
          render json: { message: "Unauthorized", errors: errors }, status: 401  
        end
      end
    end
  end
end
