module Api
  module V1
    class DeceasedAndTotalAndPermanentDisabilitiesController < ApplicationController
      before_action :authenticate_user!

      def generate_transaction
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Generating deceased_and_total_and_permanent_disability transaction", email: current_user.email, username: current_user.username)
        branch_id              = params[:branch_id] 
        date_prepared          = params[:date_prepared]
        prepared_by            = current_user.full_name

        errors = DeceasedAndTotalAndPermanentDisabilities::ValidateNewDeceasedAndTotalAndPermanentDisabilityTransaction.new(branch_id: branch_id, date_prepared: date_prepared).execute!

        if errors.length == 0
          deceased_and_total_and_permanent_disability = DeceasedAndTotalAndPermanentDisabilities::CreateDeceasedAndTotalAndPermanentDisability.new(branch: Branch.find(branch_id), date_prepared: date_prepared, prepared_by: prepared_by).execute!

          if deceased_and_total_and_permanent_disability.valid?
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully created Deceased and Total and Permanent Disability transaction.", email: current_user.email, username: current_user.username)
            deceased_and_total_and_permanent_disability.save!
            render json: { messages: ["Successfully created transaction. Redirecting..."], deceased_and_total_and_permanent_disability_id: deceased_and_total_and_permanent_disability.id }
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to create Deceased and Total and Permanent Disability transaction.", email: current_user.email, username: current_user.username)
            errors << "Something went wrong"
            deceased_and_total_and_permanent_disability.errors.messages.each do |m|
              errors << m
            end
            render json: { errors: errors } , status: 401
          end
        else
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Error in generating Deceased and Total and Permanent Disability transaction", email: current_user.email, username: current_user.username)
          render json: { errors: errors } , status: 401
        end
      end
      
      def delete_deceased_and_total_and_permanent_disability_record
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Deleting Deceased and Total and Permanent Disability record", email: current_user.email, username: current_user.username)
        deceased_and_total_and_permanent_disability_record = DeceasedAndTotalAndPermanentDisabilityRecord.find(params[:deceased_and_total_and_permanent_disability_record_id])
        deceased_and_total_and_permanent_disability = deceased_and_total_and_permanent_disability_record.deceased_and_total_and_permanent_disability
        deceased_and_total_and_permanent_disability_record.destroy!
        deceased_and_total_and_permanent_disability.update!(updated_at: Time.now)
        render json: { message: "ok" }
      end

      def add_legal_dependent
        deceased_and_total_and_permanent_disability = DeceasedAndTotalAndPermanentDisability.find(params[:id])
        legal_dependent = LegalDependent.find(params[:legal_dependent_id])
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Adding legal dependent #{legal_dependent.to_s} to collection", email: current_user.email, username: current_user.username)
        deceased_and_total_and_permanent_disability_record = DeceasedAndTotalAndPermanentDisabilities::AddLegalDependentToDeceasedAndTotalAndPermanentDisability.new(deceased_and_total_and_permanent_disability: deceased_and_total_and_permanent_disability, legal_dependent: legal_dependent).execute!
        deceased_and_total_and_permanent_disability_record.save!
        render json: { message: "Successfully added legal dependent" }
      end

      def approve
        deceased_and_total_and_permanent_disability = DeceasedAndTotalAndPermanentDisability.find(params[:id])
        errors = []
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to approve Deceased and Total and Permanent Disability)", email: current_user.email, username: current_user.username)

        if ["MIS", "OM"].include? current_user.role
          errors = DeceasedAndTotalAndPermanentDisabilities::ValidateDeceasedAndTotalAndPermanentDisabilityForApproval.new(deceased_and_total_and_permanent_disability: deceased_and_total_and_permanent_disability).execute!
          if errors.size == 0
            deceased_and_total_and_permanent_disability = DeceasedAndTotalAndPermanentDisabilities::ApproveDeceasedAndTotalAndPermanentDisability.new(deceased_and_total_and_permanent_disability: deceased_and_total_and_permanent_disability, user:  current_user).execute!
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved Deceased and Total and Permanent Disability)", email: current_user.email, username: current_user.username)
            render json: { message: "Successfully approved Deceased and Total and Permanent Disability" }
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve Deceased and Total and Permanent Disability", email: current_user.email, username: current_user.username)
            render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"
          render json: { message: "Unauthorized", errors: errors }, status: 401
        end
      end

      def reverse
        deceased_and_total_and_permanent_disability = DeceasedAndTotalAndPermanentDisability.find(params[:id])
        errors = []
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to reverse Deceased and Total and Permanent Disability #{deceased_and_total_and_permanent_disability.branch}.", email: current_user.email, username: current_user.username, record_reference_id: deceased_and_total_and_permanent_disability.id)
        
        if ["MIS", "OM"].include? current_user.role
          errors = DeceasedAndTotalAndPermanentDisabilities::ValidateDeceasedAndTotalAndPermanentDisabilityForReversal.new(deceased_and_total_and_permanent_disability: deceased_and_total_and_permanent_disability).execute!
          if errors.size == 0
            DeceasedAndTotalAndPermanentDisabilities::ReverseDeceasedAndTotalAndPermanentDisability.new(deceased_and_total_and_permanent_disability: deceased_and_total_and_permanent_disability, user: current_user).execute!
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully reversed Deceased and Total and Permanent Disability #{deceased_and_total_and_permanent_disability.branch}", email: current_user.email, username: current_user.username, record_reference_id: deceased_and_total_and_permanent_disability.id)
            render json: { message: "success" }
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to reverse deceased_and_total_and_permanent_disability #{deceased_and_total_and_permanent_disability.branch}", email: current_user.email, username: current_user.username, record_reference_id: deceased_and_total_and_permanent_disability.id)
            render json: { message: "error", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"
          render json: { message: "Unauthorized", errors: errors }, status: 401  
        end
      end
    end
  end
end
