module Api
  module V1
    class SyncAccountingController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:major_groups] = MajorGroup.all
        data[:major_accounts] = MajorAccount.all
        data[:mother_accounting_codes] = MotherAccountingCode.all
        data[:accounting_codes] = AccountingCode.all
        data[:loan_deductions] = LoanDeduction.all

        render json: { success: true, info: "Accounting entries", data: data }
      end
    end
  end
end
