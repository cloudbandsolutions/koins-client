module Api
  module V1
    class LoanPaymentsController < ApplicationController
      before_action :authenticate_user!
      
      def add_wp_record
        loan_payment = LoanPayment.find(params[:loan_payment_id])
        member = Member.find(params[:member_id])
        savings_account = loan_payment.savings_account

        if(loan_payment.wp_records.pluck(:member_id).include?(member.id))
          render json: { message: "Duplicate member" }, status: 401
        else
          wp_record = WpRecord.new(amount: params[:amount], loan_payment: loan_payment, member: member)
          loan_payment.wp_records << wp_record

          total = 0.00
          loan_payment.wp_records.each do |wp_record|
            total += wp_record.amount
          end

          if((savings_account.balance - total) < savings_account.maintaining_balance)
            render json: { message: "Not enough funds to withdraw" }, status: 401
          else
            wp_record.save!
            loan_payment.update!(updated_at: Time.now.localtime)
            render json: { message: "Successfully added wp record", wp_amount: loan_payment.wp_amount.to_f }
          end
        end
      end

      def wp_records
        loan_payment = LoanPayment.find(params[:loan_payment_id])
        data = []

        loan_payment.wp_records.each do |wp_record|
          data << {member_full_name: wp_record.member.full_name, amount: wp_record.amount.to_f}
        end

        render json: { records: data }
      end

      def delete_wp_record
      end
    end
  end
end
