module Api
  module V1
    class SyncLoanDeductionEntriesController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        loan_product_ids = LoanProduct.all.pluck(:id)
        data[:loan_deduction_entries] = LoanDeductionEntry.where("loan_product_id IN (?)", loan_product_ids)

        render json: { success: true, info: "Loan deduction entries", data: data }
      end
    end
  end
end
