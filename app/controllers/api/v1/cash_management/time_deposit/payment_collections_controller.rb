module Api
  module V1
    module CashManagement
      module TimeDeposit
        class PaymentCollectionsController< ApiController
          before_action :authenticate_user!
          def approved_time_deposit_transaction
            payment_collection_id =  params[:pc_id]
            user = current_user  
           
            approved_payment = ::PaymentCollections::ApprovePaymentCollectionTimeDeposit.new(payment_collection: payment_collection_id, user: user).execute!



          end




          def generate_transaction
            date_of_payment = params[:start_date]
            branch_id = Branch.find(Settings.branch_ids)
            payment_type    = "time_deposit"
            book            = "CRB"
            prepared_by     = current_user.full_name
            particular      = "To Record TimeDeposit of _______ amount of _________ for ________"

            payment_collection = ::PaymentCollections::CreatePaymentCollectionForTimeDeposit.new(
                                  total_amount: 100,
                                  branch: branch_id[0][:id],
                                  date_of_payment: date_of_payment,
                                  payment_type: payment_type,
                                  book: book,
                                  prepared_by: prepared_by,
                                  particular: particular

            
                                 ).execute!
                                 
            render json: { messages: ["Successfully created transaction. Redirecting..."], payment_collection_id: payment_collection.id }


          end


          def save_member 
          

            member_account_id = params[:member_account_id]
            lock_in_amout = params[:lock_in_amout]
            start_date = params[:start_date]
            lock_in_period = params[:lock_in_period]
            payment_ids = params[:payment_ids]
            end_date = start_date.to_date + lock_in_period.to_i.day


            member_save = ::PaymentCollections::AddMemberToTimeDepositPaymentCollection.new(
                            member_account_id: member_account_id,
                            lock_in_amout: lock_in_amout,
                            start_date: start_date,
                            lock_in_period: lock_in_period,
                            payment_ids: payment_ids,
                            end_date: end_date
            
                          ).execute!

              

              @data = ::PaymentCollections::LoadTimeDepositList.new(payment_id: payment_ids).execute!
              render   json: { data: @data }


          
          end

          def save_payment_collection
            particular = params["particular"]
            
            book = params["book"]
            payment_id = params["payment_ids"]
            or_number = params["or_number"]
            total_time_deposit = DepositTimeTransaction.where(payment_collection_id: payment_id).sum(:amount)
            @data = PaymentCollection.find(payment_id).update(total_time_deposit_amount: total_time_deposit, or_number: or_number, book: book, particular: particular)

            render json:{ data: @data}
          
          end

        end
      end
    end
  end
end
