module Api
  module V1
    module CashManagement
      module Closing
        class MonthlyInterestAndTaxClosingRecordsController < ApiController
          before_action :authenticate_user!

          def create
            closing_month   = params[:closing_month]
            closing_year    = Date.today.year
            savings_type_id = params[:savings_type_id]
            special         = params[:special].present? ? true : false

            puts params.inspect

            errors = MonthlyClosing::ValidateMonthlyInterestAndTaxClosingRecord.new(
                                      closing_month: closing_month.to_i,
                                      savings_type_id: savings_type_id,
                                      special: special
                                    ).execute!

            if errors.size == 0
              records = []
              Branch.all.each do |branch|
                record = MonthlyClosing::GenerateMonthlyInterestAndTaxClosingRecord.new(
                                          closing_month: closing_month, 
                                          branch: branch,
                                          savings_type_id: savings_type_id,
                                          special: special
                                        ).execute!

                if !record.save
                  puts record.errors.messages
                else
                  records << record
                end
              end

              render json: {records: records}
            else
              render json: { errors: errors }, status: 401
            end
          end

          def approve
            record = MonthlyInterestAndTaxClosingRecord.find(params[:id])
            errors = MonthlyClosing::ValidateApproveMonthlyInterestAndTaxClosingRecord.new(monthly_interest_and_tax_closing_record: record).execute!

            if errors.size == 0
              MonthlyClosing::ApproveMonthlyInterestAndTaxClosingRecord.new(monthly_interest_and_tax_closing_record: record, user: current_user).execute!
              render json: { }
            else
              render json: { errors: errors }, status: 401
            end
          end

          def void
            record = MonthlyInterestAndTaxClosingRecord.find(params[:id])
            errors = MonthlyClosing::ValidateVoidMonthlyInterestAndTaxClosingRecord.new(monthly_interest_and_tax_closing_record: record).execute!

            if errors.size == 0
              MonthlyClosing::VoidMonthlyInterestAndTaxClosingRecord.new(monthly_interest_and_tax_closing_record: record, user: current_user).execute!
              render json: { }
            else
              render json: { errors: errors }, status: 401
            end
          end

          def destroy
            record = MonthlyInterestAndTaxClosingRecord.find(params[:id])

            if record.pending?
              record.destroy!
              render json: { }
            else
              render json: { errors: ["Cannot destroy non pending record"] }, status: 401
            end
          end
        end
      end
    end
  end
end
