module Api
  module V1
    module CashManagement
      module FundTransfer
        module Insurance
          class PaymentCollectionsController < ApiController
            before_action :authenticate_user!

            def generate_transaction
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Generating insurance fund transfer transaction", email: current_user.email, username: current_user.username)
              branch = Branch.where(id: params[:branch_id]).first
              date_of_payment = params[:date_of_payment]
              prepared_by = current_user.full_name

              errors = PaymentCollections::ValidateNewInsuranceFundTransferTransaction.new(branch: branch, date_of_payment: date_of_payment, prepared_by: prepared_by).execute!

              if errors.size > 0
                UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to generate insurance fund transfer transaction", email: current_user.email, username: current_user.username)
                render json: { errors: errors }, status: 401
              else
                payment_collection = PaymentCollections::CreatePaymentCollectionForInsuranceFundTransfer.new(branch: branch, date_of_payment: date_of_payment, prepared_by: prepared_by, members: []).execute!
                UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully generated insurance fund transfer transaction", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
                render json: { message: "ok", id: payment_collection.id }
              end
            end
          end
        end
      end
    end
  end
end
