module Api
  module V1
    module CashManagement
      module Deposits
        class PaymentCollectionsController < ApiController
          before_action :authenticate_user!

          def generate_transaction
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Generating deposit transaction", 
              email: current_user.email, 
              username: current_user.username
            )

            branch_id       = params[:branch_id] 
            date_of_payment = params[:date_of_payment]
            prepared_by     = current_user.full_name

            errors  = ::PaymentCollections::ValidateNewDepositTransaction.new(
                        branch_id: branch_id, 
                        date_of_payment: date_of_payment
                      ).execute!

            if errors.length == 0
              members = []
              payment_collection  = ::PaymentCollections::CreatePaymentCollectionForDeposits.new(
                                      branch: Branch.find(branch_id), 
                                      paid_at: date_of_payment, prepared_by: prepared_by, 
                                      members: members
                                    ).execute!

              if payment_collection.valid?
                UserActivity.create!(
                  user_id: current_user.id, 
                  role: current_user.role, 
                  content: "Successfully created deposit transaction.", 
                  email: current_user.email, 
                  username: current_user.username
                )

                if params[:for_resignation].present?
                  payment_collection.for_resignation = true
                end

                payment_collection.save!
                render json: { messages: ["Successfully created transaction. Redirecting..."], payment_collection_id: payment_collection.id }
              else
                UserActivity.create!(
                  user_id: current_user.id, 
                  role: current_user.role, 
                  content: "Failed to create deposit transaction.", 
                  email: current_user.email, 
                  username: current_user.username
                )

                errors << "Something went wrong"

                payment_collection.errors.messages.each do |m|
                  errors << m
                end
                render json: { errors: errors } , status: 401
              end
            else
              UserActivity.create!(
                user_id: current_user.id, 
                role: current_user.role, 
                content: "Error in generating deposit transaction", 
                email: current_user.email, 
                username: current_user.username
              )

              render json: { errors: errors } , status: 401
            end
          end
        end
      end
    end
  end
end
