module Api
  module V1
    module CashManagement
      module EquityWithdrawals
        class PaymentCollectionsController < ApiController
          before_action :authenticate_user!

          def generate_transaction
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Generating Equity withdrawal transaction", 
              email: current_user.email, 
              username: current_user.username
            )

            branch_id       = params[:branch_id] 
            date_of_payment = params[:date_of_payment]
            prepared_by     = current_user.full_name

            errors = PaymentCollections::ValidateNewEquityWithdrawalTransaction.new(branch_id: branch_id, date_of_payment: date_of_payment).execute!

            if errors.length == 0
              members = []
              payment_collection  = PaymentCollections::CreatePaymentCollectionForEquityWithdrawals.new(
                                      branch: Branch.find(branch_id), 
                                      paid_at: date_of_payment, prepared_by: prepared_by, members: members
                                    ).execute!

              if payment_collection.valid?
                payment_collection.save!
                UserActivity.create!(
                  user_id: current_user.id, 
                  role: current_user.role, 
                  content: "Successfully generated withdrawal transaction", 
                  email: current_user.email, 
                  username: current_user.username, 
                  record_reference_id: payment_collection.id
                )

                render json: { messages: ["Successfully created transaction. Redirecting..."], payment_collection_id: payment_collection.id }
              else
                errors << "Something went wrong"
                UserActivity.create!(
                  user_id: current_user.id, 
                  role: current_user.role, 
                  content: "Failed to generated withdrawal transaction", 
                  email: current_user.email, 
                  username: current_user.username
                )

                payment_collection.errors.messages.each do |m|
                  errors << m
                end
                render json: { errors: errors } , status: 401
              end
            else
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Validation failed for new withdrawal transaction", email: current_user.email, username: current_user.username)
              render json: { errors: errors } , status: 401
            end
          end
        end
      end
    end
  end
end

