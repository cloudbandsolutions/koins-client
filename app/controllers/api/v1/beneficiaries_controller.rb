module Api
  module V1
    class BeneficiariesController < ApiController
      def create
        beneficiary = Beneficiary.new(Members::NewBeneficiaryRecord.new(beneficiary_params: beneficiary_params).execute!)

        if beneficiary.save
          render json: { success: true, info: "Created beneficiary #{beneficiary.reference_number}", data: { beneficiary: beneficiary } }
        else
          render json: { success: false, info: "Error in creating beneficiary. Error: #{beneficiary.errors.messages}", data: { beneficiary: beneficiary } }
        end
      end

      def update
        beneficiary_hash = beneficiary_params
        beneficiary = Beneficiary.where(reference_number: beneficiary_hash[:reference_number]).first
        beneficiary_hash[:member] = Member.where(identification_number: beneficiary_hash[:member_identification_number]).first

        beneficiary_p = beneficiary_hash.except!(:member_identification_number)

        if beneficiary.update(beneficiary_p)
          render json: { success: true, info: "Updated beneficiary #{beneficiary.reference_number}", data: { beneficiary: beneficiary } }
        else
          render json: { success: false, info: "Error in updating beneficiary. Error: #{beneficiary.errors.messages}", data: { beneficiary: beneficiary } }
        end
      end

      def beneficiary_params
        params.require(:beneficiary).permit!
      end
    end
  end
end

