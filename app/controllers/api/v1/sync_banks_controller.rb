module Api
  module V1
    class SyncBanksController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:banks] = Bank.all

        render json: { success: true, info: "Banks", data: data }
      end
    end
  end
end
