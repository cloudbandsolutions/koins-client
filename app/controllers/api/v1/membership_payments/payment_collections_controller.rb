module Api
  module V1
    module MembershipPayments
      class PaymentCollectionsController < ApiController
        before_action :authenticate_user!

        def remove_payment_collection_record
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Removing payment collection record (Membership)", email: current_user.email, username: current_user.username)
          payment_collection = PaymentCollection.where(id: params[:payment_collection_id]).first
          payment_collection_record = PaymentCollectionRecord.where(id: params[:payment_collection_record_id]).first

          errors = Membership::ValidateRemovePaymentCollectionRecord.new(payment_collection: payment_collection, payment_collection_record: payment_collection_record).execute!

          if errors.size > 0
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to remove payment collection record (Membership)", email: current_user.email, username: current_user.username)
            render json: { errors: errors }, status: 401
          else
            Membership::RemovePaymentCollectionRecord.new(payment_collection: payment_collection, payment_collection_record: payment_collection_record).execute!
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully removed payment collection record (Membership)", email: current_user.email, username: current_user.username)
            render json: { message: "ok" }
          end
        end

        def generate_transaction
          branch_id       = params[:branch_id] 
          date_of_payment = params[:date_of_payment]
          prepared_by     = current_user.full_name

          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Generating payment collection (Membership)", email: current_user.email, username: current_user.username)

          errors = Membership::ValidateNewMembershipPaymentTransaction.new(branch_id: branch_id, date_of_payment: date_of_payment).execute!

          if errors.length == 0
            members = []
            payment_collection = PaymentCollections::CreatePaymentCollectionForMembershipPayment.new(
                                  branch: Branch.find(branch_id), 
                                  paid_at: date_of_payment, prepared_by: prepared_by, members: members).execute!

            if payment_collection.valid?
              payment_collection.save!
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully generated payment collection (Membership)", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { messages: ["Successfully created transaction. Redirecting..."], payment_collection_id: payment_collection.id }
            else
              errors << "Something went wrong"
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to generate payment collection (Membership)", email: current_user.email, username: current_user.username)
              payment_collection.errors.messages.each do |m|
                errors << m
              end
              render json: { errors: errors } , status: 401
            end
          else
            render json: { errors: errors } , status: 401
          end
        end
      end
    end
  end
end
