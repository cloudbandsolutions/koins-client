module Api
  module V1
    class PaymentCollectionsController < ApplicationController
      before_action :authenticate_user!

      def generate_new_billing
        branch_id = params[:branch_id]
        center_id = params[:center_id]
        paid_at = params[:date_of_payment]

        errors  = PaymentCollections::ValidatePaymentCollectionBillingCreation.new(
                    branch_id: branch_id,
                    center_id: center_id,
                    paid_at: paid_at
                  ).execute!

        if errors.size == 0
          branch = Branch.find(branch_id)
          center = Center.find(center_id)
          prepared_by = current_user.full_name

          payment_collection  = PaymentCollections::CreatePaymentCollectionForBilling.new(
                                  branch: branch, 
                                  center: center, 
                                  paid_at: paid_at, 
                                  prepared_by: prepared_by
                                ).execute!

          if payment_collection.save
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully saved new billing", 
              email: current_user.email, 
              username: current_user.username
            )

            render json: { messages: ["Success"], payment_collection_id: payment_collection.id }
          else
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Failed to create new billing. #{payment_collection.errors.messages}", 
              email: current_user.email, 
              username: current_user.username
            )

            render json: { errors: payment_collection.errors.messages }, status: 401
          end
        else
          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Failed to create new billing. No branch or center or date of payment defined.", 
            email: current_user.email, 
            username: current_user.username
          )

          render json: { errors: errors }, status: 401
        end
      end

      def delete_payment_collection_record
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Deleting payment collection record", email: current_user.email, username: current_user.username)
        payment_collection_record = PaymentCollectionRecord.find(params[:payment_collection_record_id])
        payment_collection = payment_collection_record.payment_collection
        payment_collection_record.destroy!
        payment_collection.update!(updated_at: Time.now)
        render json: { message: "ok" }
      end

      def add_member
        payment_collection = PaymentCollection.find(params[:id])
        member = Member.find(params[:member_id])
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Adding member #{member.to_s} to collection", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)

        if payment_collection.payment_type == "billing"
          raise "Feature not yet implemented"
        elsif ["membership", "insurance_fund_transfer", "deposit", "withdraw", "insurance_withdrawal", "equity_withdrawal", "insurance_remittance", "income_insurance"].include? payment_collection.payment_type
          collection_payment = PaymentCollections::AddMemberToPaymentCollection.new(payment_collection: payment_collection, member: member).execute!
          collection_payment.update!(updated_at: Time.now)
          render json: { message: "Successfully added member" }
        end
      end

      def approve
        payment_collection = PaymentCollection.find(params[:id])
        errors = []
       # raise params.inspect
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Trying to approve payment collection (#{payment_collection.payment_type})", 
          email: current_user.email, 
          username: current_user.username, 
          record_reference_id: payment_collection.id
        )

        if ["MIS", "BK", "SBK"].include? current_user.role
          if payment_collection.payment_type == "billing"
            errors = PaymentCollections::ValidatePaymentCollectionBillingApproval.new(payment_collection: payment_collection).execute!
            if errors.size == 0
              payment_collection  = PaymentCollections::ApprovePaymentCollectionBilling.new(
                                      payment_collection: payment_collection, 
                                      user: current_user
                                    ).execute!

              UserActivity.create!(
                user_id: current_user.id, 
                role: current_user.role, 
                content: "Successfully approved payment collection (#{payment_collection.payment_type})", 
                email: current_user.email, 
                username: current_user.username, 
                record_reference_id: payment_collection.id
              )

              render json: { message: "Successfully approved payment collection" }
            else
              UserActivity.create!(
                user_id: current_user.id, 
                role: current_user.role, 
                content: "Failed to approve payment collection (#{payment_collection.payment_type})", 
                email: current_user.email, 
                username: current_user.username, 
                record_reference_id: payment_collection.id
              )

              render json: { message: "Cannot approve this collection", errors: errors }, status: 401
            end
          elsif payment_collection.payment_type == "insurance_withdrawal"
            errors  = PaymentCollections::ValidatePaymentCollectionCashManagementInsuranceWithdrawalApproval.new(
                        payment_collection: payment_collection
                      ).execute!

            if errors.size == 0
              payment_collection  = PaymentCollections::ApprovePaymentCollectionCashManagementInsuranceWithdrawal.new(
                                      payment_collection: payment_collection, 
                                      user: current_user
                                    ).execute!

              UserActivity.create!(
                user_id: current_user.id, 
                role: current_user.role, 
                content: "Successfully approved payment collection (#{payment_collection.payment_type})", 
                email: current_user.email, 
                username: current_user.username, 
                record_reference_id: payment_collection.id
              )

              render json: { message: "ok" }
            else
              UserActivity.create!(
                user_id: current_user.id, 
                role: current_user.role, 
                content: "Failed to approve payment collection (#{payment_collection.payment_type})", 
                email: current_user.email, 
                username: current_user.username, 
                record_reference_id: payment_collection.id
              )

              render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
            end
          elsif payment_collection.payment_type == "equity_withdrawal"
            errors  = PaymentCollections::ValidatePaymentCollectionCashManagementEquityWithdrawalApproval.new(
                        payment_collection: payment_collection
                      ).execute!

            if errors.size == 0
              payment_collection  = PaymentCollections::ApprovePaymentCollectionCashManagementEquityWithdrawal.new(
                                      payment_collection: payment_collection, 
                                      user: current_user
                                    ).execute!

              UserActivity.create!(
                user_id: current_user.id, 
                role: current_user.role, 
                content: "Successfully approved payment collection (#{payment_collection.payment_type})", 
                email: current_user.email, 
                username: current_user.username, 
                record_reference_id: payment_collection.id
              )

              render json: { message: "ok" }
            else
              UserActivity.create!(
                user_id: current_user.id, 
                role: current_user.role, 
                content: "Failed to approve payment collection (#{payment_collection.payment_type})", 
                email: current_user.email, 
                username: current_user.username, 
                record_reference_id: payment_collection.id
              )

              render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
            end  
          elsif payment_collection.payment_type == "withdraw"
            errors  = PaymentCollections::ValidatePaymentCollectionCashManagementWithdrawApproval.new(
                        payment_collection: payment_collection
                      ).execute!

            if errors.size == 0
              payment_collection  = PaymentCollections::ApprovePaymentCollectionCashManagementWithdraw.new(
                                      payment_collection: payment_collection, 
                                      user: current_user
                                    ).execute!

              UserActivity.create!(
                user_id: current_user.id, 
                role: current_user.role, 
                content: "Successfully approved payment collection (#{payment_collection.payment_type})", 
                email: current_user.email, 
                username: current_user.username, 
                record_reference_id: payment_collection.id
              )

              render json: { message: "ok" }
            else
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
            end
          elsif payment_collection.payment_type == "deposit"
            errors = PaymentCollections::ValidatePaymentCollectionCashManagementDepositApproval.new(payment_collection: payment_collection).execute!
            if errors.size == 0
              payment_collection = PaymentCollections::ApprovePaymentCollectionCashManagementDeposit.new(payment_collection: payment_collection, user: current_user).execute!
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "ok" }
            else
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
            end
          elsif payment_collection.payment_type == "insurance_fund_transfer"
            errors = PaymentCollections::ValidatePaymentCollectionCashManagementFundTransferInsuranceApproval.new(payment_collection: payment_collection).execute!
            if errors.size == 0
              payment_collection = PaymentCollections::ApprovePaymentCollectionCashManagementInsuranceFundTransfer.new(payment_collection: payment_collection, user: current_user).execute!
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Successfully approved payment collection" }
            else
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
            end
          elsif payment_collection.payment_type == "membership"
            errors = PaymentCollections::ValidatePaymentCollectionMembershipPaymentApproval.new(payment_collection: payment_collection).execute!
            if errors.size == 0
              payment_collection = PaymentCollections::ApprovePaymentCollectionMembershipPayment.new(payment_collection: payment_collection, user:  current_user).execute!
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Successfully approved payment collection" }
            else
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
            end
          elsif payment_collection.payment_type == "insurance_remittance"
            errors = PaymentCollections::ValidatePaymentCollectionInsurancePaymentApproval.new(payment_collection: payment_collection).execute!
            if errors.size == 0
              payment_collection = PaymentCollections::ApprovePaymentCollectionInsurancePayment.new(payment_collection: payment_collection, user:  current_user).execute!
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Successfully approved payment collection" }
            else
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
            end
          elsif payment_collection.payment_type == "income_insurance"
            errors = PaymentCollections::ValidatePaymentCollectionIncomeInsurancePaymentApproval.new(payment_collection: payment_collection).execute!
            if errors.size == 0
              payment_collection = PaymentCollections::ApprovePaymentCollectionIncomeInsurancePayment.new(payment_collection: payment_collection, user:  current_user).execute!
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Successfully approved payment collection" }
            else
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve payment collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
              render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
            end  
          end
        else
          errors << "Unauthorized to perform this transaction"
          render json: { message: "Unauthorized", errors: errors }, status: 401
        end
      end

      def reverse
        payment_collection = PaymentCollection.find(params[:id])
        errors = PaymentCollections::ValidatePaymentCollectionBillingReversal.new(payment_collection: payment_collection).execute!
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to reverse collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)

        if errors.size == 0
          PaymentCollections::ReversePaymentCollection.new(payment_collection: payment_collection, user: current_user).execute!
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully reversed collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
          render json: { message: "success" }
        else
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to reverse collection (#{payment_collection.payment_type})", email: current_user.email, username: current_user.username, record_reference_id: payment_collection.id)
          render json: { message: "error", errors: errors }, status: 401
        end
      end
    end
  end
end
