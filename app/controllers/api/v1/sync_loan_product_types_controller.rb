module Api
  module V1
    class SyncLoanProductTypesController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:loan_product_types] = LoanProductType.all

        render json: { success: true, info: "Loan product types", data: data }
      end
    end
  end
end
