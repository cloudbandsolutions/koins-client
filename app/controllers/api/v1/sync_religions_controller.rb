module Api
  module V1
    class SyncReligionsController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:religions] = Religion.all

        render json: { success: true, info: "Religions", data: data }
      end
    end
  end
end

