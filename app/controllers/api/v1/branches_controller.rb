module Api
  module V1
    class BranchesController < ApiController
      def so_list
        branch_id = params[:branch_id]
        branch = Branch.find(branch_id)
        centers = branch.centers

        so_list = []
        centers.pluck(:officer_in_charge).uniq.each do |so|
          so_list << { name: so }
        end

        render json: { so_list: so_list }
      end

      def centers_by_branch
        branch = Branch.where(id: params[:branch_id]).first

        if branch
          render json: { centers: branch.centers }
        else
          render json: { centers: [] }
        end
      end

      def centers_by_so
        so = params[:so]
        centers = Center.where(officer_in_charge: so)

        render json: { centers: centers }
      end
    end
  end
end
