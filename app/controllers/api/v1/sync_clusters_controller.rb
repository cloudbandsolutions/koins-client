module Api
  module V1
    class SyncClustersController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:clusters] = Cluster.all

        render json: { success: true, info: "Clusters", data: data }
      end

      def single
        data = {}
        data[:cluster] = Cluster.find(params[:id])

        render json: { success: true, info: "Single cluster", data: data }
      end
    end
  end
end
