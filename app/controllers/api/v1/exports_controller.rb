module Api
  module V1
    class ExportsController < ApplicationController
      before_action :authenticate_user!

      def beneficiaries
      end
    end
  end
end
