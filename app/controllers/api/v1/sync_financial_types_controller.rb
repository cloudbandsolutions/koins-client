module Api
  module V1
    class SyncFinancialTypesController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:savings_types] = SavingsType.all
        data[:insurance_types] = InsuranceType.all
        data[:equity_types] = EquityType.all

        render json: { success: true, info: "Financial types", data: data }
      end
    end
  end
end
