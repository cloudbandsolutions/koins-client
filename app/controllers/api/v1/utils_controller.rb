module Api
  module V1
    class UtilsController < ApplicationController
      protect_from_forgery with: :null_session
      before_action :api_user_logged_in?

      def loan_product_types
        if params[:id].present?
          render json: { loan_product_type: LoanProductType.find(params[:id]) }
        else
          render json: { loan_product_types: LoanProductType.all }
        end
      end

      def member_resignation_particular_by_code
        resignation_code = params[:resignation_code]
        resignation_type = params[:resignation_type]

        particular = nil

        Settings.member_resignation_types.each do |member_resignation_type|
          if resignation_type == member_resignation_type.name
            member_resignation_type.particulars.each do |p|
              if p.code == resignation_code
                particular = p.name
              end
            end
          end
        end

        if particular
          render json: { particular: particular }
        else
          render json: { message: 'No particular found' }, status: 404
        end
      end

      def loan_products
        render json: LoanProduct.select("id,code").to_json
      end

      def member_resignation_type_particulars
        resignation_type = params[:resignation_type]
        particulars = []

        Settings.member_resignation_types.each do |member_resignation_type|
          if resignation_type == member_resignation_type.name
            member_resignation_type.particulars.each do |p|
              particulars << { code: p.code, name: p.name }
            end
          end
        end

        if particulars.size > 0
          render json: { particulars: particulars }
        else
          render json: { message: 'No particulars found' }, status: 404
        end
      end

      # TODO: Filter loan insurance types depending on user credentials
      def loan_insurance_types
        loan_insurance_types = LoanInsuranceType.select("*")

        render json: { success: true, info: "Loan Insurance Types list", data: { loan_insurance_types: loan_insurance_types } }
      end

      # TODO: Filter branches depending on user credentials
      def branches
        branches = Branch.select("*")

        render json: { success: true, info: "Branch list", data: { branches: branches } }
      end

      # TODO: Filter centers depending on user credentials
      def centers
        branch = Branch.find(params[:branch_id])

        centers = Center.where(branch_id: branch.id).order(:name)

        render json: { success: true, info: "Center list for #{branch}", data: { centers: centers } }
      end

      def heartbeat
        render json: { success: true, info: "alive", data: { } }
      end

      def process_moratorium_request
        center_ids = params[:center_ids]
        next_payment_date = params[:next_payment_date]
      end
    end
  end
end
