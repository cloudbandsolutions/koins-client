module Api
  module V1
    class VouchersController < ApplicationController
      def approve
        voucher = Voucher.find(params[:voucher_id])
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to approve accounting entry BOOK: #{voucher.book} Ref: #{voucher.reference_number}", email: current_user.email, username: current_user.username, record_reference_id: voucher.id)

        if voucher.status == "pending"
          #voucher.date_prepared = ApplicationHelper.current_working_date
          ::Accounting::ApproveVoucher.new(voucher: voucher, user: current_user).execute!
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved accounting entry BOOK: #{voucher.book} Ref: #{voucher.reference_number}", email: current_user.email, username: current_user.username, record_reference_id: voucher.id)
          render json: { message: "Successfully approved voucher!" }
        else
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve accounting entry BOOK: #{voucher.book} Ref: #{voucher.reference_number}", email: current_user.email, username: current_user.username, record_reference_id: voucher.id)
          render json: { message: "Cannot approve non-pending accounting entry" }, status: 401
        end
      end

      def reject
        voucher = Voucher.find(params[:voucher_id])

        if voucher.status == "pending"
          Vouchers::RejectVoucher.new(voucher: voucher, current_user: current_user).execute!
          render json: { message: "Successfully rejected voucher!" }
        else
          render json: { message: "Cannot approve non-pending accounting entry" }, status: 401
        end
      end
    end
  end
end
