module Api
  module V1
    class SyncAreasController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:areas] = Area.all

        render json: { success: true, info: "Areas", data: data }
      end
    end
  end
end
