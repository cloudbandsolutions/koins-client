module Api
  module V1
    class ReportsController < ActionController::API
      before_action :authenticate_user!
      require 'application_helper'

      def generate_mfi_monthly_member_counts
        current_date  = ApplicationHelper.current_working_date
        ::MonthlyReports::GenerateMfiNewAndResignedMonthlyCounts.new(
          year: current_date.year, 
          branch: Branch.last, 
          user: current_user
        ).execute!

        render json: { message: "ok" }
      end

      def transferred_members
        data  = ::Reports::FetchTransferredMembers.new.execute!

        render json: { data: data }
      end

      def x_weeks_to_pay
        num_weeks       = params[:num_weeks]
        loan_product_id = params[:loan_product_id]

        loan_product    = LoanProduct.where(id: loan_product_id).first

        data  = Reports::FetchXWeeksToPayResults.new(
                  num_weeks: num_weeks, 
                  loan_product: loan_product
                ).execute!

        render json: { data: data }
      end

      def soa_funds
        account_codes = Settings.account_codes

        if params[:account_types].present?
          account_codes = []
          params[:account_types].each do |account_type|
            if account_type == "Savings"
              SavingsType.all.each do |savings_type|
                account_codes << savings_type.code
              end
            elsif account_type == "Insurance"
              InsuranceType.all.each do |insurance_type|
                account_codes << insurance_type.code
              end
            elsif account_type == "Equity"
              EquityType.all.each do |equity_type|
                account_codes << equity_type.code
              end
            end
          end
        end

        if params[:start_date] == params[:end_date]
          render json: { errors: ["Invalid dates"] }, status: 402
        else
          data  = ::Reports::GenerateSoaFunds.new(
                    center_id: params[:center_id],
                    start_date: params[:start_date],
                    end_date: params[:end_date],
                    account_codes: account_codes
                  ).execute!

          render json: { data: data, account_codes: account_codes }
        end
      end

      def member_registry
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Member Registry", 
          email: current_user.email, 
          username: current_user.username
        )

        render json:  ::Members::GenerateRegistryOfMembers.new(
                        center: nil
                      ).execute!
      end

      def monthly_rr
        year = Date.today.year
        render json: { data: ::Reports::FetchMonthlyRr.new(year: year).execute! }
      end

      def master_list_of_client_loans
        branch_id       = params[:branch_id]
        center_id       = params[:center_id]
        loan_product_id = params[:loan_product_id]
        as_of           = params[:as_of]

        if as_of.blank?
          as_of = ApplicationHelper.current_working_date
        end

        data = Reports::GenerateMasterListOfClientLoans.new(
                          branch_id: branch_id,
                          center_id: center_id,
                          loan_product_id: loan_product_id,
                          as_of: as_of
                        ).execute!

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Funds SOA", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def funds_statement_of_accounts
        branch_id   = params[:branch_id]
        center_id   = params[:center_id]
        start_date  = params[:start_date]
        end_date    = params[:end_date]

        data = Reports::GenerateFundsStatementOfAccount.new(
                          branch_id: branch_id, 
                          center_id: center_id, 
                          start_date: start_date, 
                          end_date: end_date
                        ).execute!

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Funds SOA", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def expenses_statement_of_assets
        branch_id       = params[:branch_id]
        center_id       = params[:center_id]
        start_date      = params[:start_date]
        end_date        = params[:end_date]
        loan_product_id = params[:loan_product_id]

        data = Reports::GenerateExpensesStatementOfAssets.new(
                          branch_id: branch_id, 
                          center_id: center_id, 
                          start_date: start_date, 
                          end_date: end_date,
                          loan_product_id: loan_product_id
                        ).execute!

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Expenses SOA", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def loans_statement_of_accounts
        branch_id       = params[:branch_id]
        center_id       = params[:center_id]
        start_date      = params[:start_date]
        end_date        = params[:end_date]
        loan_product_id = params[:loan_product_id]

        data = Reports::GenerateLoansStatementOfAccount.new(
                          branch_id: branch_id, 
                          center_id: center_id, 
                          start_date: start_date, 
                          end_date: end_date,
                          loan_product_id: loan_product_id
                        ).execute!

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Loans SOA", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def personal_funds
        branch_id     = params[:branch_id]
        so            = params[:so]
        center_id     = params[:center_id]
        account_types = params[:account_types]
        as_of         = params[:as_of]
        detailed      = params[:detailed]

        data = Reports::PersonalFunds.new(
                  branch_id: branch_id, 
                  so: so, 
                  center_id: center_id, 
                  account_types: account_types,
                  as_of: as_of,
                  detailed: detailed                  
                ).execute!

        data[:download_url] = reports_personal_funds_path(
                                branch_id: branch_id, 
                                so: so, 
                                center_id: center_id, 
                                account_types: account_types, 
                                detailed: detailed,
                                download: true,
                                as_of: as_of
                              )

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Personal Funds", 
          email: current_user.email, 
          username: current_user.username
        )
        
        render json: data
      end

      def member_reports
        branch_id     = params[:branch_id]
        insurance_status = params[:insurance_status]
        member_type = params[:member_type]
        member_status = params[:member_status]
        start_date    = params[:start_date]
        end_date      = params[:end_date]

        data = Reports::MemberReports.new(
                  branch_id: branch_id,
                  member_type: member_type,
                  insurance_status: insurance_status,
                  member_status: member_status,
                  start_date: start_date,
                  end_date: end_date
                ).execute!

        data[:download_url] = member_reports_path(
                                branch_id: branch_id,
                                member_type: member_type,
                                insurance_status: insurance_status,
                                member_status: member_status,
                                start_date: start_date,
                                download: true,
                                end_date: end_date
                              )

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Member Reports", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def member_quarterly_reports
        start_date    = params[:start_date]
        end_date      = params[:end_date]

        data = Reports::MemberQuarterlyReports.new(
                  start_date: start_date,
                  end_date: end_date
                ).execute!

        data[:download_url] = member_quarterly_reports_path(
                                start_date: start_date,
                                download: true,
                                end_date: end_date
                              )

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Member Quarterly Reports", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def collection_reports
        branch_id     = params[:branch_id]
        start_date    = params[:start_date]
        end_date      = params[:end_date]

        data = Reports::CollectionReports.new(
                  branch_id: branch_id,
                  start_date: start_date,
                  end_date: end_date
                ).execute!

        data[:download_url] = collection_reports_path(
                                branch_id: branch_id,
                                start_date: start_date,
                                download: true,
                                end_date: end_date
                              )

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Collection Reports", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def summary_of_certificates_and_policies
        branch_id     = params[:branch_id]
        plan_type = params[:plan_type]
        as_of      = params[:as_of]

        data = Reports::SummaryOfCertificatesAndPolicies.new(
                  branch_id: branch_id,
                  as_of: as_of,
                  plan_type: plan_type
                ).execute!

        data[:download_url] = summary_of_certificates_and_policies_path(
                                branch_id: branch_id,
                                plan_type: plan_type,
                                download: true,
                                as_of: as_of
                              )

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Summary Of Certificates And Policies", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def repayments
        branch_id       = params[:branch_id]
        so              = params[:so]
        center_id       = params[:center_id]
        loan_product_id = params[:loan_product_id]
        as_of           = params[:as_of]
        detailed        = params[:detailed]
    

        #if DailyReport.where(as_of: as_of).count > 0 and !so.present? and !center_id.present? and !loan_product_id.present?
        #  data = DailyReport.where(as_of: as_of).last.repayments
        #else
          data  = Reports::GenerateRepayments.new(
                    branch_id: branch_id, 
                    so: so, 
                    center_id: center_id, 
                    loan_product_id: loan_product_id, 
                    as_of: as_of,
                    detailed: detailed
                  ).execute!
        #end

        data[:download_url] = reports_repayments_path(branch_id: branch_id, so: so, center_id: center_id, loan_product_id: loan_product_id, download: true)

        UserActivity.create!(
          user_id: current_user.try(:id), 
          role: current_user.role, 
          content: "Generating Report: Repayments", 
          email: current_user.email, username: 
          current_user.username
        )

        render json: data
      end

      def aging_of_receivables
        branch_id       = params[:branch_id]
        so              = params[:so]
        center_id       = params[:center_id]
        loan_product_id = params[:loan_product_id]
        as_of           = params[:as_of]

        data = Reports::GenerateAgingOfReceivables.new(branch_id: branch_id, so: so, center_id: center_id, loan_product_id: loan_product_id, as_of: as_of).execute!

        data[:download_url] = reports_par_path(branch_id: branch_id, so: so, center_id: center_id, loan_product_id: loan_product_id, download: true)

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: Aging of Receivables", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def par
        branch_id       = params[:branch_id]
        so              = params[:so]
        center_id       = params[:center_id]
        loan_product_id = params[:loan_product_id]
        as_of           = params[:as_of]
        detailed        = params[:detailed]

        data = Reports::GeneratePar.new(branch_id: branch_id, so: so, center_id: center_id, loan_product_id: loan_product_id, as_of: as_of, detailed: detailed).execute!

        data[:download_url] = reports_par_path(branch_id: branch_id, so: so, center_id: center_id, loan_product_id: loan_product_id, download: true)

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating Report: PAR", 
          email: current_user.email, 
          username: current_user.username
        )

        render json: data
      end

      def active_loaners_by_branch
        branch = Branch.find(params[:branch_id])
        year = Time.now.year
        data = Reports::ActiveLoaners.new(branch: branch, year: year).execute!

        render json: data
      end

      def loan_product_portfolio_by_branch
        loan_product = LoanProduct.find(params[:loan_product_id])
        branch = Branch.find(params[:branch_id])
        year = Time.now.year
        data = Reports::LoanProductPortfolioByBranch.new(loan_product: loan_product, branch: branch, year: year).execute!

        render json: data
      end

      def balance_sheet
        branch = Branch.find(params[:branch_id])
        start_date = params[:start_date]
        end_date = params[:end_date]
        data = Reports::GenerateBalanceSheet.new(branch: branch, start_date: start_date, end_date: end_date).execute!
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Generating Financial Report: Balance Sheet", email: current_user.email, username: current_user.username)
        render json: data
      end

      def income_statement
        branch = Branch.find(params[:branch_id])
        start_date = params[:start_date]
        end_date = params[:end_date]
        data = Reports::GenerateIncomeStatement.new(branch: branch, start_date: start_date, end_date: end_date).execute!
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Generating Financial Report: Income Statement", email: current_user.email, username: current_user.username)
        render json: data
      end
    end
  end
end

