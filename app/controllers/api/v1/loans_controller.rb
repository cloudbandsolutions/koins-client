module Api
  module V1
    class LoansController < ApiController
      before_action :authenticate_user!, only: [:approve, :reverse]
      require 'application_helper'

      def portfolio
        loan  = Loan.where(uuid: params[:uuid]).first
        as_of = params[:as_of].try(:to_date) || Date.today

        portfolio = 0.00

        principal_due   = loan.ammortization_schedule_entries.where("is_void IS NULL").sum(:principal)
        paid_principal  = loan.paid_principal_as_of(as_of)

        render json: {
          id: loan.uuid,
          portfolio: (principal_due - paid_principal).to_f.round(2)
        }
      end

      def make_individual_payment 
        loan    = Loan.find(params[:loan_id])
        errors  = ::Loans::ValidateIndividualLoanPayment.new(
                    amount: params[:amount],
                    loan: loan,
                    particular: params[:particular],
                  ).execute!

        if errors.size > 0
          render json: { errors: errors } , status: 401
        else
          payment_collection  = ::PaymentCollections::CreatePaymentCollectionForIndividualPayment.new(
                                  amount: params[:amount],
                                  loan: loan,
                                  particular: params[:particular],
                                  paid_at:  ApplicationHelper.current_working_date,
                                  prepared_by:  current_user.full_name
                                ).execute!

          if params[:debit_accounting_code_id].present?
            debit_accounting_code = AccountingCode.find(params[:debit_accounting_code_id])
            payment_collection.debit_accounting_code  = debit_accounting_code
          end

          if payment_collection.save
            render json: { message: "ok", payment_collection_id: payment_collection.id }
          else
            render json: { errors: payment_collection.errors.messages }, status: 401
          end
        end
      end

      def by_batch_transaction_reference_number
        batch_transaction_reference_number = params[:batch_transaction_reference_number]
        branch_id = params[:branch_id]

        branch = Branch.find(branch_id)

        loans = Loans::ByBatchTransactionReferenceNumber.new(batch_transaction_reference_number: batch_transaction_reference_number, branch: branch).execute!

        render json: { loans: loans }
      end

      def reamortize
        loan                  = Loan.find(params[:id])
        new_interest_rate     = params[:new_interest_rate].to_f
        new_num_installments  = params[:new_num_installments].to_i
        new_term              = params[:new_term]
        paid_at               = ApplicationHelper.current_working_date

        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Trying to reamortize loan #{loan.pn_number}", 
          email: current_user.email, username: current_user.username, 
          record_reference_id: loan.id
        )

        ::Loans::ReamortizeLoan.new(
          loan: loan,
          new_interest_rate: new_interest_rate,
          new_num_installments: new_num_installments,
          new_term: new_term,
          paid_at: paid_at,
          user: current_user
        ).execute!

        # update remaining balance
        Loan.find(params[:id]).update_remaining_balance!

        render json: { message: "ok" }
      end

      def loan_application_voucher_particular
        member = Member.find(params[:member_id])
        amount = params[:amount]
        loan_product = LoanProduct.find(params[:loan_product_id]) if !params[:loan_product_id].blank?
        check_number = params[:check_number]
        clip_number = params[:clip_number]
        voucher_check_number = params[:voucher_check_number]

        result = "Release of Loan - #{member} cv# #{voucher_check_number} ck# #{check_number} clip# #{clip_number}"

        render json: { success: true, info: "voucher particular", data: { text: result } }
      end

      def writeoff
        loan = Loan.find(params[:loan_id])
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to writeoff loan with PN number #{loan.pn_number}", email: current_user.email, username: current_user.username, record_reference_id: loan.id)
        errors = Loans::ValidateLoanForWriteoff.new(loan: loan).execute!

        if errors.size > 0
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to writeoff loan with PN number #{loan.pn_number}", email: current_user.email, username: current_user.username, record_reference_id: loan.id)
          render json: { errors: errors }, status: 401
        else
          Loans::WriteoffLoan.new(loan: loan, user: current_user).execute!
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully written off loan with PN number #{loan.pn_number}", email: current_user.email, username: current_user.username, record_reference_id: loan.id)
          render json: { message: "ok" }
        end
      end

      def reverse
        loan = Loan.find(params[:loan_id])
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to reverse loan with PN number #{loan.pn_number}", email: current_user.email, username: current_user.username, record_reference_id: loan.id)
        errors = Loans::ValidateLoanForReversal.new(loan: loan).execute!

        if errors.size > 0
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to reverse loan with PN number #{loan.pn_number}", email: current_user.email, username: current_user.username, record_reference_id: loan.id)
          render json: { errors: errors }, status: 401
        else
          Loans::ReverseLoan.new(loan: loan).execute!
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully reversed loan with PN number #{loan.pn_number}", email: current_user.email, username: current_user.username, record_reference_id: loan.id)
          render json: { message: "Successfully reversed loan!" }
        end
      end

      def approve
        loan = Loan.find(params[:loan_id])
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to approve loan with PN number #{loan.pn_number}", email: current_user.email, username: current_user.username, record_reference_id: loan.id)
        errors = Loans::ValidateLoanForApproval.new(loan: loan, user: current_user).execute!

        if errors.size > 0
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve loan with PN number #{loan.pn_number}", email: current_user.email, username: current_user.username, record_reference_id: loan.id)
          render json: { errors: errors }, status: 401
        else
          Loans::ApproveLoan.new(loan: loan, user: current_user).execute!
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved loan with PN number #{loan.pn_number}", email: current_user.email, username: current_user.username, record_reference_id: loan.id)
          render json: { message: "Successfully approved loan!" }
        end
      end
    end
  end
end
