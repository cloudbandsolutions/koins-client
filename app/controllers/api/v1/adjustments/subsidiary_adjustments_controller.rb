module Api
  module V1
    module Adjustments
      class SubsidiaryAdjustmentsController < ApiController
        before_action :authenticate_user!

        def create
          branch      = Branch.where(id: params[:branch_id]).first
          particular  = params[:particular]

          errors  = ::Adjustments::ValidateCreateSubsidiaryAdjustment.new(
                      branch: branch,
                      particular: particular
                    ).execute!


          if errors.size > 0
            render json: { errors: errors }, status: 500
          else
            record  = ::Adjustments::CreateNewSubsidiaryAdjustment.new(
                        user: current_user,
                        branch: branch,
                        particular: particular
                      ).execute!

            render json: { id: record.id }
          end
        end

        def approve
          record = SubsidiaryAdjustment.find(params[:id])

          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Trying to approve subsidiary adjustment", 
            email: current_user.email, 
            username: current_user.username, 
            record_reference_id: record.id
          )

          errors  = ::Adjustments::ValidateApproveSubsidiaryAdjustment.new(
                      subsidiary_adjustment: record, 
                      user: current_user
                    ).execute!

          if errors.size > 0
            render json: { errors: errors }, status: 401
          else
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, content: "Successfully approved subsidiary adjustment", 
              email: current_user.email, 
              username: current_user.username, 
              record_reference_id: record.id
            )

            ::Adjustments::ApproveSubsidiaryAdjustment.new(
              subsidiary_adjustment: record, 
              user: current_user
            ).execute!

            render json: { message: "ok" }
          end
        end
      end
    end
  end
end
