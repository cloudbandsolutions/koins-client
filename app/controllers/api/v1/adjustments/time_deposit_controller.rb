module Api
  module V1
    module Adjustments
      class TimeDepositController < ApiController
        def withdraw
          savings_account =  params[:savings_transaction]
          withdraw_date = params[:withdraw_date]
          savings_account = SavingsAccountTransaction.find(params[:savings_transaction]).savings_account_id
          savings_account_transaction =   SavingsAccountTransaction.where(savings_account_id: savings_account).order(:transaction_date).last
         
          @data = ::Adjustments::WithdrawalTimeDeposit.new(savings_account_transaction: savings_account_transaction, withdraw_date: withdraw_date).execute! 


          render json: { id: @data.id }



        end
        
        def approved
          savings_account =  params[:savings_transaction]
          time_deposit_withdrawal_details = TimeDepositWithdrawal.find(savings_account)
          user = current_user
      
          @data = ::Adjustments::ApproveTimeDepositWithdrawal.new(time_deposit_id: time_deposit_withdrawal_details, user: user).execute!
          
          render json: { id: savings_account}

        end


      end
    end
  end
end
