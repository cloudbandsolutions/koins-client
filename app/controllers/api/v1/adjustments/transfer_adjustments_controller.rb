module Api
  module V1
    module Adjustments
      class TransferAdjustmentsController < ApiController
        before_action :authenticate_user!

        def approve
          record = TransferAdjustment.find(params[:id])

          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Trying to approve transfer adjustment", 
            email: current_user.email, 
            username: current_user.username, 
            record_reference_id: record.id
          )

          errors = ::Adjustments::ValidateApproveTransferAdjustment.new(transfer_adjustment: record, user: current_user).execute!

          if errors.size > 0
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Error in approving transfer adjustment", 
              email: current_user.email, 
              username: current_user.username, 
              record_reference_id: record.id
            )

            render json: { errors: errors }, status: 401
          else
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, content: "Successfully approved transfer adjustment", 
              email: current_user.email, 
              username: current_user.username, 
              record_reference_id: record.id
            )

            ::Adjustments::ApproveTransferAdjustment.new(transfer_adjustment: record, user: current_user).execute!

            render json: { message: "ok" }
          end
        end
      end
    end
  end
end
