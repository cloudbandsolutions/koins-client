module Api
  module V1
    module Adjustments
      class SubsidiaryAdjustmentRecordsController < ApplicationController
        before_action :authenticate_user!
        before_action :load_subsidiary_adjustment, except: [:delete]

        def load_subsidiary_adjustment
          @subsidiary_adjustment  = SubsidiaryAdjustment.find(params[:subsidiary_adjustment_id])
        end

        def delete
          subsidiary_adjustment_record  = SubsidiaryAdjustmentRecord.find(params[:id])
          subsidiary_adjustment         = subsidiary_adjustment_record.subsidiary_adjustment
          subsidiary_adjustment_record.destroy!

          render json: { id: subsidiary_adjustment.id }
        end

        def create
          member          = Member.where(id: params[:member_id]).first
          account_code    = params[:account_code]
          adjustment_type = params[:adjustment_type]
          amount          = params[:amount]
          accounting_code = AccountingCode.where(id: params[:accounting_code_id]).first

          errors  = ::Adjustments::ValidateCreateSubsidiaryAdjustmentRecord.new(
                      member: member,
                      account_code: account_code,
                      adjustment_type: adjustment_type,
                      amount: amount.try(:to_f),
                      accounting_code: accounting_code,
                      subsidiary_adjustment: @subsidiary_adjustment
                    ).execute!

          if errors.size > 0
            render json: { errors: errors }, status: 500
          else
            record  = ::Adjustments::CreateSubsidiaryAdjustmentRecord.new(
                        member: member,
                        account_code: account_code,
                        adjustment_type: adjustment_type,
                        amount: amount.try(:to_f),
                        accounting_code: accounting_code,
                        subsidiary_adjustment: @subsidiary_adjustment
                      ).execute!

            render json: { id: @subsidiary_adjustment.id }
          end
        end
      end
    end
  end
end
