module Api
  module V1
    module Adjustments
      class BatchMoratoriaController < ApiController
        def approve
          batch_moratorium  = BatchMoratorium.find(params[:id])
          ::Adjustments::ApproveBatchMoratorium.new(batch_moratorium: batch_moratorium, user: current_user).execute!

          render json: { message: "ok" }
        end
      end
    end
  end
end
