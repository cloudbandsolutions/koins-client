module Api
  module V1
    class SyncCentersController < ApplicationController
      before_action :verify_api_key!

      def by_branch
        branch = Branch.find(params[:branch_id])

        data = {}
        data[:centers] = Center.where(branch_id: branch.id)

        render json: { success: true, info: "Centers for branch #{branch.name}", data: data }
      end
    end
  end
end
