module Api
  module V1
    class ValuesController < ApplicationController
      protect_from_forgery with: :null_session

      def religions
        render json: Religion.all.map{ |r| { value: r.name, label: r.name } }  
      end
    end
  end
end
