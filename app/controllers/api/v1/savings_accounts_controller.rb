module Api
  module V1
    class SavingsAccountsController < ApplicationController
      def rehash
        savings_account  = SavingsAccount.find(params[:savings_account_id])
        savings_account  = Savings::RehashAccount.new(savings_account: savings_account).execute!

        render json: { message: "success" }
      end

      def generate_interest_and_tax_transactions
        current_month = Date.today.month
        #current_month = 7
        current_day = Date.today.day

        if MonthlyInterestAndTaxClosingRecord.where(status: "approved", month: current_month).count > 0
          render json: { message: "Already closed for month #{current_month}" }, status: 422
        else
          Savings::GenerateInterestAndTaxTransactions.new(month: 7).execute!

          render json: { message: "success" }
        end
      end
    end
  end
end
