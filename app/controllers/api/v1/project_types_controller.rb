module Api
  module V1
    class ProjectTypesController < ApplicationController
      protect_from_forgery with: :null_session
      
      def project_type_categories
        project_type_categories = ProjectTypeCategory.select("*")

        render json: { success: true, info: "Project type categories", data: { project_type_categories: project_type_categories } }
      end

      def by_project_type_category_id
        project_type_category_id = params[:project_type_category_id]
        project_types = ProjectType.where(project_type_category_id: project_type_category_id)

        render json: { success: true, info: "Project types", data: { project_types: project_types } }
      end
    end
  end
end
