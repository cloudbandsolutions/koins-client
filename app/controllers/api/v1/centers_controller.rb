module Api
  module V1
    class CentersController < ApplicationController
      protect_from_forgery with: :null_session
      before_action :verify_api_key!

      def create
        center = Center.new(center_params)

        if center.save
          render json: { success: true, info: "Created center", data: { center: center } }
        else
          render json: { success: false, info: "Error in creating center. #{center.errors.messages}", data: { } }, status: 500
        end
      end

      def center_params
        params.require(:center).permit!
      end
    end
  end
end
