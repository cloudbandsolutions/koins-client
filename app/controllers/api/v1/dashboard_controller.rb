module Api
  module V1
    class DashboardController < ApplicationController
      require 'application_helper'

      def send_daily_report
        daily_report  = DailyReport.last
        if ::Postings::PostStatistics.new(daily_report: daily_report).execute!
          render json: { message: "sent" }
        else
          render json: { message: "fail" }, status: 401
        end
      end

      def generate_monthly_daily_report
        ::Reports::GenerateMonthlyDailyReport.new(year: Date.today.year).execute!
        render json: { message: "ok" }
      end

      def active_loans_count
        if params[:as_of].present?
          @as_of  = params[:as_of]
        else
          @as_of  = ApplicationHelper.current_working_date
        end

        render json: { data: Loans::FetchActiveLoansByProduct.new(loan_product_id: params[:loan_product_id], as_of: @as_of).execute! }
      end

      def member_counts
        #as_of = ApplicationHelper::current_working_date

        #if DailyReport.where(as_of: as_of).count > 0
        #  render json: { data: DailyReport.where(as_of: as_of).last.member_stats }
        #else
        #  render json: { data: Dashboard::MemberCounts.new.execute! }
        #end

        #render json: { data: DailyReport.last.try(:member_stats) }
        ::Dashboard::MemberCounts.new.execute!
        summary = Summary.member_counts.order("as_of ASC").last
        data    = nil
        as_of   = nil

        if summary
          data  = summary.try(:meta)
          as_of = summary.as_of.strftime("%b %d, %Y")
        end

        render json: { as_of: as_of, data: Summary.member_counts.order("as_of ASC").last.try(:meta) }
      end

      def generate_daily_report
        Reports::GenerateDailyReport.new(as_of: nil).execute!
        Dashboard::MemberCounts.new.execute!
        render json: { message: "ok" }
      end

      def loan_product_stats
        #as_of = ApplicationHelper::current_working_date

        #render json: { data: Dashboard::LoanProductStats.new(as_of: as_of, loan_product_id: nil).execute! }
        #if DailyReport.where(as_of: as_of).count > 0
        #  render json: { data: DailyReport.where(as_of: as_of).last.loan_product_stats }
        #else
        #  render json: { data: Dashboard::LoanProductStats.new(as_of: as_of, loan_product_id: nil).execute! }
        #end

        render json: { data: DailyReport.last.try(:loan_product_stats) }
      end

      def watchlist
        #as_of = ApplicationHelper::current_working_date

        #if DailyReport.where(as_of: as_of).count > 0
        #  watchlist = DailyReport.where(as_of: as_of).last.watchlist
        #  if watchlist
        #    render json: { data: watchlist }
        #  else
        #    render json: { data: Dashboard::GenerateWatchlist.new(as_of: as_of).execute! }
        #  end
        #else
        #  render json: { data: Dashboard::GenerateWatchlist.new(as_of: as_of).execute! }
        #end

        render json: { data: DailyReport.last.try(:watchlist) }
      end
    end
  end
end
