module Api
  module V1
    class SavingsAccountTransactionsController < ApplicationController
      before_action :authenticate_user!
      
      def add_wp_record
        savings_account_transaction = SavingsAccountTransaction.find(params[:savings_account_transaction_id])
        member = Member.find(params[:member_id])
        savings_account = savings_account_transaction.savings_account

        if(savings_account_transaction.wp_records.pluck(:member_id).include?(member.id))
          render json: { message: "Duplicate member" }, status: 401
        else
          wp_record = WpRecord.new(amount: params[:amount], savings_account_transaction: savings_account_transaction, member: member)
          savings_account_transaction.wp_records << wp_record

          total = 0.00
          savings_account_transaction.wp_records.each do |wp_record|
            total += wp_record.amount
          end

          if((savings_account.balance - total) < savings_account.maintaining_balance)
            render json: { message: "Not enough funds to withdraw" }, status: 401
          else
            wp_record.save!
            savings_account_transaction.update!(updated_at: Time.now.localtime)
            render json: { message: "Successfully added wp record", amount: savings_account_transaction.amount.to_f }
          end
        end
      end

      def wp_records
        savings_account_transaction = SavingsAccountTransaction.find(params[:savings_account_transaction_id])
        data = []

        savings_account_transaction.wp_records.each do |wp_record|
          data << {member_full_name: wp_record.member.full_name, amount: wp_record.amount.to_f}
        end

        render json: { records: data }
      end

      def delete_wp_record
      end
    end
  end
end

