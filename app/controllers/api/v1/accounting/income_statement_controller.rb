module Api
  module V1
    module Accounting
      class IncomeStatementController < ApplicationController
        before_action :authenticate_user!
        def generate_income_statement
          data = ::Accounting::GenerateIncomeStatement.new(as_of: ApplicationHelper.current_working_date, prepared_by: current_user.to_s).execute!

          render json: { data: data }
        end
      end
    end
  end
end
