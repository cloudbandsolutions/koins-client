module Api
  module V1
    module Accounting
      class InterestOnShareCapitalCollectionsController < ApplicationController
        def equity_rate
          render json: { equity_rate: InterestOnShareCapitalCollection.find(params[:id]).equity_rate }
        end

        def regenerate
          raise "jef".inspect
        end

        def generate
          @year = params[:end_date]
          first_day_of_the_year = "#{@year}-01-01"
          end_date_of_the_year = "#{@year}-12-31"
          

          @start_date = first_day_of_the_year
          @end_date   = end_date_of_the_year

          @equity_interest = params[:equity_interest]
          #raise @equity_interest.inspect
          @data       = ::Accounting::GenerateInterestShareCapital.new(start_date: @start_date,end_date: @end_date, equity_interest: @equity_interest).execute!
          ::Accounting::SaveInterestOnShareCapitalCollections.new(data: @data).execute!
          render json: { data: @data }
        end

        def approved
          @insurance_id = params[:equity_id]
          @record = InterestOnShareCapitalCollection.find(@insurance_id)
          @data = ::Accounting::ApproveInterestOnShareCapitalCollection.new(interest_on_share_capital_collection: @record, user: current_user ).execute!

          render json: { data: @data }
          
        end

        def approved_cleared
          @selected_year= params[:selected_years]
          @data = ::Tools::ApprovedAccountingEntryForTransferOnShareCapital.new(share_capital_interest_id: @selected_year).execute!
          render json: {  data: @data }
        end

      end
    end
  end
end
