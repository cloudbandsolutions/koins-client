module Api
  module V1
    module Accounting
      class PatronageRefundsController < ApplicationController
        def generate
          patronage_rate          = params[:patronage_rate]
          last_month_of_the_year  = "#{params[:as_of]}-12-31"
          as_of                   = last_month_of_the_year.to_date
          
         
          errors  = ::Accounting::PatronageRefunds::ValidateSavePatronageRefund.new(
                      params: params
                    ).execute!

          if errors.size > 0
            render json: { errors: errors }, status: 402
          else
            @data = ::Accounting::GeneratePatronageRefund.new(
                      patronage_rate: patronage_rate, 
                      as_of: as_of
                    ).execute!

            ::Accounting::SavePatronageRefundCollections.new(
              data: @data, 
              as_of: as_of, 
              patronage_rate: patronage_rate
            ).execute!
            
            render json: {data: @data}
          end
        end

        def approve
          id      = params[:id]
          record  = PatronageRefundCollection.find(id)
          @data   = ::Accounting::ApprovePatronageRefund.new(
                      patronage_refund_collection: record, 
                      user: current_user
                    ).execute!
        
          render json: {record: record}
          
        end
      end
    end
  end
end
