module Api
  module V1
    module Accounting
      class ReportsController < ApiController
        before_action :secure_api
        
        def monthly_incentives
          start_date = params[:start_date]
          end_date = params[:end_date]
          #raise end_date.inspect
          data = ::Reports::FetchMonthlyIncentivesData.new(start_date: start_date, end_date: end_date).execute!
          #data[:download_url] = accounting_monthly_incentives_path(
            #current_month: current_month,
            #current_year: current_year
          #) 
          render json: { data: data }

        end

        def monthly_incentives_pdf
          start_date = params[:start_date]
          end_date = params[:end_date]
          #raise end_date.inspect
          #data = ::Reports::FetchMonthlyIncentivesData.new(start_date: start_date, end_date: end_date).execute!
          #raise start_date.inspect
          #raise end_date.inspect
          #data = ::Reports::FetchMonthlyIncentivesData.new.execute!
          
          data = ::Reports::FetchMonthlyIncentivesData.new(start_date: start_date ,end_date: end_date).execute!
          #raise data.inspect
          #data[:download_url] = accounting_monthly_incentives_pdf_path(
            #current_month: current_month,
            #current_year: current_year
          #) 
          render json: { data: data }

        end





        def trial_balance
          start_date  = params[:start_date]
          end_date    = params[:end_date]
          branch      = Branch.find(params[:branch_id])
          if params[:accounting_fund_id].present?
            accounting_fund = AccountingFund.find(params[:accounting_fund_id])
          end
          c_date      = ApplicationHelper.current_working_date

          if start_date.blank? or end_date.blank?
            render json: { errors: ["Start date required", "End date required"] }, status: 401
          else
            data = ::Accounting::GenerateTrialBalance.new(start_date: start_date, end_date: end_date, branch: branch, accounting_fund: accounting_fund).execute!
            data[:download_url] = accounting_reports_trial_balance_path(
                                branch: branch, 
                                start_date: start_date, 
                                download: true,
                                end_date: end_date,
                                accounting_fund_id: accounting_fund
                              )

            render json: { data: data }
          end
        end

        def financial_position
          start_date  = params[:start_date]
          end_date    = params[:end_date]
          branch      = Branch.find(params[:branch_id])
          detailed    = params[:detailed]
          c_date      = ApplicationHelper.current_working_date

          if start_date.blank? or end_date.blank?
            render json: { errors: ["Start date required", "End date required"] }, status: 401
          else
            data = ::Accounting::GenerateFinancialPosition.new(start_date: start_date, end_date: end_date, branch: branch).execute!
            data[:download_url] = accounting_reports_financial_position_path(
                                branch: branch, 
                                start_date: start_date, 
                                download: true,
                                end_date: end_date,
                                detailed: detailed,
                              )

            render json: { data: data }
          end
        end


        def statement_of_comprehensive_inc
          start_date  = params[:start_date]
          end_date    = params[:end_date]
          detailed    = params[:detailed]
          branch      = Branch.find(params[:branch_id])
          c_date      = ApplicationHelper.current_working_date

          if start_date.blank? or end_date.blank?
            render json: { errors: ["Start date required", "End date required"] }, status: 401
          else
            data = ::Accounting::GenerateStatementOfComprehensiveInc.new(start_date: start_date, end_date: end_date, branch: branch).execute!
            data[:download_url] = accounting_reports_statement_of_comprehensive_inc_path(
                                branch: branch, 
                                start_date: start_date, 
                                download: true,
                                end_date: end_date,
                                detailed: detailed,
                              )

            render json: { data: data }
          end
        end
      end
    end
  end
end
