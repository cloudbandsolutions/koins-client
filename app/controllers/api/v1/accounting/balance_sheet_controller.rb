module Api
  module V1
    module Accounting
      class BalanceSheetController < ApplicationController
        before_action :authenticate_user!

        def generate_balance_sheet
          current_working_date  = ApplicationHelper.current_working_date


          prepared_by           = current_user.to_s
          #raise prepared_by.inspect
          data  = ::Accounting::GenerateBalanceSheet.new(
                    as_of: current_working_date, 
                    prepared_by: prepared_by
                  ).execute!

          balance_sheet = BalanceSheet.new(
                            data: data,
                            as_of: current_working_date,
                            prepared_by:  prepared_by,
                            branch_name: Branch.first.to_s
                          )

          if balance_sheet.save
            render json: { data: data, balance_sheet: balance_sheet }
          else
            render json: { errors: balance_sheet.errors.messages }, status: 401
          end
        end
      end
    end
  end
end
