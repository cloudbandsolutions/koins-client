module Api
  module V1
    module Accounting
      class ClosingController < ApplicationController
        before_action :authenticate_user!
        def generate_year_end_closing
          current_date  = ApplicationHelper.current_working_date
          data =  Closing::GenerateYearEndClosing.new(
                    year: current_date.year, 
                    prepared_by: current_user.full_name
                  ).execute!

          render json: { data: data }
        end

        def approve_year_end
          current_date  = ApplicationHelper.current_working_date
          data =  Closing::GenerateYearEndClosing.new(
                    year: current_date.year, 
                    prepared_by: current_user.full_name
                  ).execute!

          Closing::ApproveYearEndClosing.new(
            data: data, 
            user: current_user,
            year: current_date.year
          ).execute!

          render json: { message: "ok" }
        end

        def approve_year_end_insurance
          @start_date = "2017-01-01".to_date
          @end_date = "2017-12-31".to_date
          current_date  = ApplicationHelper.current_working_date
          data =  Closing::GenerateYearEndClosingInsurance.new(
                    start_date: @start_date,
                    end_date: @end_date,
                    year: current_date.year,
                    prepared_by: current_user.full_name
                  ).execute!

          Closing::ApproveYearEndClosingInsurance.new(
            data: data, 
            user: current_user,
            year: current_date.year
          ).execute!

          render json: { message: "ok" }
        end

        def generate_year_end_closing_insurance
          @start_date = "2017-01-01".to_date
          @end_date = "2017-12-31".to_date
          current_date  = ApplicationHelper.current_working_date
   
          data =  Closing::GenerateYearEndClosingInsurance.new(
                    year: current_date.year, 
                    start_date: @start_date,
                    end_date: @end_date,
                    prepared_by: current_user.full_name
                  ).execute!

          # if params[:start_date].present? and params[:end_date].present?
          #   data =  Closing::GenerateYearEndClosingInsurance.new(
          #           year: nil,
          #           start_date: @start_date,
          #           end_date: @end_date, 
          #           prepared_by: current_user.full_name
          #         ).execute!
          # end

          render json: { data: data }  
        end
      end
    end
  end
end
