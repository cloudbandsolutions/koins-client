module Api
  module V1
    class MembersController < ApiController
      def members_by_type
        member_type = params[:member_type]

        valid_member_types = ["pure_savers", "active_loaners", "active", "pending"]

        if !valid_member_types.include?(member_type)
          render json: { message: "invalid type" } , status: 401
        else
          render json: { data: Dashboard::GetMembersByType.new(member_type).execute! }
        end
      end

      def create_missing_accounts
        Member.all.each do |member|
          SavingsType.all.each do |savings_type|
            if SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id).count == 0
              SavingsAccount.create!(savings_type: savings_type, member: member, account_number: "#{SecureRandom.hex(4)}")
            end
          end
          InsuranceType.all.each do |insurance_type|
            if InsuranceAccount.where(member_id: member.id, insurance_type_id: insurance_type.id).count == 0
              InsuranceAccount.create!(insurance_type: insurance_type, member: member, account_number: "#{SecureRandom.hex(4)}")
            end
          end
          EquityType.all.each do |equity_type|
            if EquityAccount.where(member_id: member.id, equity_type_id: equity_type.id).count == 0
              EquityAccount.create!(equity_type: equity_type, member: member, account_number: "#{SecureRandom.hex(4)}")
            end
          end
        end

        render json: { status: "ok" }
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Generate Missing Accounts.", email: current_user.email, username: current_user.username)
      end
      
      def get_accounts
        member_id = params[:member_id]
        member = Member.find(member_id)
        accounts = Members::GetAccounts.new(member: member).execute!

        render json: { accounts: accounts }
      end

      def get_members
        #members = Member.active_and_pending.order("last_name ASC")
        if Settings.activate_microloans
          members  = Member.where(status: "active").order("last_name ASC")
        else
          members  = Member.active.order("last_name ASC")
        end

        if params[:q].present?
          name = params[:q]
          members = members.where(
                      "lower(first_name) LIKE :name OR lower(middle_name) LIKE :name OR lower(last_name) LIKE :name", name: "#{name.downcase}%"
                    )
        end

        if params[:center_id].present?
          members = members.where('center_id = ?', params[:center_id])
        end

        members_data = []
        members.each do |member|
          members_data << { id: member.id, name: member.full_name_with_center }
        end

        render json: { success: true, info: 'member list', data: { members: members_data } }
      end

      def index
        members = Member.select("*")

        if params[:center_id].present?
          members = members.where('center_id = ?', params[:center_id])
        end

        members_data = []
        members.each do |member|
          members_data << { id: member.id, name: member.to_s }
        end

        render json: { success: true, info: 'member list', data: { members: members_data } }
      end

      def create
        member = Member.new(Members::NewMemberRecord.new(member_params: member_params).execute!)

        if member.save
          render json: { success: true, info: "Created member #{member.identification_number}", data: { member: member } }
        else
          render json: { success: false, info: "Error in creating member. Error: #{member.errors.messages}", data: { member: member } }
        end
      end

      def update
        member_hash = member_params
        member = Member.where(identification_number: member_hash[:identification_number]).first
        branch = Branch.where(code: member_hash[:branch_code]).first
        center = Center.where(code: member_hash[:center_code]).first

        member_hash[:branch] = branch
        member_hash[:center] = center

        member_p = member_hash.except!(:branch_code, :center_code)

        if member.update(member_p)
          render json: { success: true, info: "Updated member #{member.identification_number}", data: { member: member } }
        else
          render json: { success: false, info: "Error in updating member. Error: #{member.errors.messages}", data: { member: member } }
        end
      end

      def member_params
        params.require(:member).permit!
      end
    end
  end
end
