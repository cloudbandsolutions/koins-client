module Api
  module V1
    class SyncLoanInsuranceDeductionEntriesController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        loan_product_ids = LoanProduct.all.pluck(:id)
        data[:loan_insurance_deduction_entries] = LoanInsuranceDeductionEntry.where("loan_product_id IN (?)", loan_product_ids)

        render json: { success: true, info: "Loan insurance deduction entries", data: data }
      end
    end
  end
end
