module Api
  module V1
    module Members
      class MemberTransferRequestsController < ApplicationController
        before_action :authenticate_user!

        def clean_transfer_records
          ::Transfer::CleanTransferRecords.new.execute!
          render json: { message: "ok" }
        end

        def approve_pending_records
          ::Transfer::ApprovePendingRecords.new.execute!
          render json: { message: "ok" }
        end

        def cancel_transfer_records
          ::Members::CancelPendingTransferRecords.new.execute!
          render json: { message: "ok" }
        end

        def generate_transfer_records
          center_id             = params[:center_id]
          center                = Center.find(center_id)
          current_working_date  = ApplicationHelper.current_working_date

          errors  = ::Members::ValidateGenerateTransferRecords.new(
                      center: center
                    ).execute!

          if errors.size > 0
            render json: { errors: errors }, status: 401
          else
            ::Members::GenerateTransferRecords.new(
              center: center,
              date_requested: current_working_date
            ).execute!

            render json: { message: "ok" }
          end
        end

        def request_transfer
          member_id             = params[:member_id]
          current_working_date  = ApplicationHelper.current_working_date

          errors  = ::Members::ValidateNewMemberTransferRequest.new(
                      member_id: member_id
                    ).execute!

          if errors.size > 0
            render json: { errors: errors }, status: 401
          else
            member_transfer_request = ::Members::CreateNewMemberTransferRequest.new(
                                        member_id: member_id,
                                        date_requested: current_working_date
                                      ).execute!

            render json: { id: member_transfer_request.id }
          end
        end

        def approve_request_transfer
          member_transfer_request_id  = params[:member_transfer_request_id]
          member_transfer_request     = MemberTransferRequest.where(
                                          id: member_transfer_request_id
                                        ).first

          current_working_date        = ApplicationHelper.current_working_date

          errors  = ::Members::ValidateApproveMemberTransferRequest.new(
                      member_transfer_request: member_transfer_request
                    ).execute!

          if errors.size > 0
            render json: { errors: errors }, status: 401
          else
            ::Members::ApproveTransferRequest.new(
              member_transfer_request: member_transfer_request
            )

            render json: { message: "ok" }
          end
        end

        def cancel_request_transfer
          member_id             = params[:member_id]
          current_working_date  = ApplicationHelper.current_working_date

          errors  = ::Members::ValidateCancelMemberTransferRequest.new(
                      member_id: member_id
                    ).execute!

          if errors.size > 0
            render json: { errors: errors }, status: 401
          else
            ::Members::CancelMemberTransferRequest.new(
              member_id: member_id
            ).execute!

            render json: { message: "ok" }
          end
        end
      end
    end
  end
end
