module Api
  module V1
    module Members
      class ResignationController < ApiController
        before_action :authenticate_user!

        def reinstate_member
          member_id = Member.find(params[:member_id])

          ::Members::RestoreResignedMember.new(member: member_id).execute!

          UserActivity.create!(
            user_id: current_user.id,
            role:    current_user.role,
            content: "Balik kasapi of #{member_id.to_s}",
            email:   current_user.email,
            username: current_user.username
          )
               
          render json: { id: member_id }
          
        end



        def resign
          member              = Member.find(params[:member_id])
          date_resigned       = params[:date_resigned].to_date
          resignation_type    = params[:resignation_type]
          resignation_code    = params[:resignation_code]
          resignation_reason  = params[:reason]
          particular          = params[:particular]

          errors  = ::Resignation::ValidateForResignation.new(
                      member: member
                    ).execute!

          if errors.size > 0
            render json: { errors: errors }, status: 401
          else
            ::Resignation::ResignMember.new(
              member: member, 
              date_resigned: date_resigned,
              resignation_type: params[:resignation_type],
              resignation_code: params[:resignation_code],
              resignation_reason: params[:reason],
              particular: params[:particular],
              user: current_user
            ).execute!

            #::RequestRecords::CreateResignationRequestRecord.new(
            #  member: member,
            #  user: current_user,
            #  resignation_type: resignation_type,
            #  resignation_code: resignation_code,
            #  reason: resignation_reason,
            #  particular: particular,
            #  date_resigned: date_resigned
            #).execute!

            #member.update!(status: "for-resignation")

            render json: { id: member.id }
          end
        end

        def approve
          member_resignation  = MemberResignation.find(params[:id])
          member              = member_resignation.member
          errors              = ::Resignation::ValidateForApproveMemberResignation.new(
                                  member_resignation: member_resignation
                                ).execute!

          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Trying to approve resignation of member #{member.to_s}", 
            email: current_user.email, 
            username: current_user.username
          )

          if errors.size > 0
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Failed to approve resignation of member #{member.to_s}", 
              email: current_user.email, username: current_user.username
            )

            render json: { errors: errors }, status: 401
          else
            Resignation::ApproveMemberResignation.new(
              member_resignation: member_resignation,
              user: current_user
            ).execute!

            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully approved resignation of member #{member.to_s}", 
              email: current_user.email, 
              username: current_user.username
            )

            render json: { message: "ok" }
          end
        end

        def revert_resignation
          member            = Member.find(params[:member_id])
          errors  = Resignation::ValidateMemberForRevertResignation.new(
                      member: member
                    ).execute!

          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Trying to revert resignation of member #{member.to_s}", 
            email: current_user.email, 
            username: current_user.username
          )

          if errors.size > 0
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Failed to revert resignation of member #{member.to_s}", 
              email: current_user.email, username: current_user.username
            )

            render json: { errors: errors }, status: 401
          else
            Resignation::RevertResignation.new(member: member).execute!
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully reverted resignation of member #{member.to_s}", 
              email: current_user.email, 
              username: current_user.username
            )
            
            render json: { message: "ok" }
          end
        end

        def flag_for_resignation
          member                  = Member.find(params[:member_id])
          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Trying to flag member #{member.to_s} for resignation", 
            email: current_user.email, 
            username: current_user.username
          )

          resignation_type        = params[:resignation_type]
          date_resigned           = params[:date_resigned]
          resignation_code        = params[:resignation_code]
          resignation_particular  = get_resignation_particular(resignation_code, resignation_type)

          errors = Resignation::ValidateMemberForResignation.new(
                    member: member, 
                    resignation_type: resignation_type, 
                    resignation_code: resignation_code, 
                    resignation_particular: resignation_particular, 
                    date_resigned: date_resigned
                  ).execute!

          if errors.size > 0
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Failed to flag member #{member.to_s} for resignation", 
              email: current_user.email, 
              username: current_user.username
            )

            render json: { errors: errors }, status: 401
          else
            Resignation::FlagMemberForResignation.new(
              member: member, 
              resignation_type: resignation_type, 
              resignation_code: resignation_code, 
              resignation_particular: resignation_particular, 
              date_resigned: date_resigned, 
              user: current_user
            ).execute!

            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully flagged member #{member.to_s} for resignation", 
              email: current_user.email, 
              username: current_user.username
            )
            
            render json: { message: "ok" }
          end
        end

        private

        def get_resignation_particular(code, resignation_type)
          particular = nil

          Settings.member_resignation_types.each do |r_type|
            r_type.particulars.each do |p|
              if p.code == code and r_type.name == resignation_type
                particular = p.name
              end
            end
          end

          particular
        end
      end
    end
  end
end
