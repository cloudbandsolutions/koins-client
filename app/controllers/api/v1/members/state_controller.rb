module Api
  module V1
    module Members
      class StateController < ApiController
        before_action :authenticate_user!

        def update_editable
          begin
            if ["BK", "MIS"].include?(current_user.role)
              member = Member.find(params[:member_id])

              state = params[:state] == "true" ? true : false
              if member.update(is_editable: state)
                if state
                  render json: { message: "Member is now editable" }
                  UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Toggle editable for member #{member.full_name.titlecase}", email: current_user.email, username: current_user.username, record_reference_id: member.id)
                else
                  render json: { message: "Member is now not editable" }
                  UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Toggle not editable for member #{member.full_name.titlecase}", email: current_user.email, username: current_user.username, record_reference_id: member.id)
                end
              else
                render json: { message: "Errors: #{member.errors.messages}" }, status: 400
              end
            else
              render json: { message: "invalid access" }, status: 400
            end
          rescue => e
            render json: { message: "Something went wrong", e: e }, status: 500
          end
        end
      end
    end
  end
end
