module Api
  module V1
    class MemberSharesController < ApplicationController
      def flag_for_printing
        member_share  = MemberShare.find(params[:id])
        member_share.update!(
          printed: true,
          date_printed: ApplicationHelper.current_working_date
        )

        render json: { member_share: member_share }
      end
    end
  end
end
