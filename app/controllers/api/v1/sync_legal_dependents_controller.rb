module Api
  module V1
    class SyncLegalDependentsController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}

        if params[:cluster_id].present?
          begin
            cluster = Cluster.find(params[:cluster_id])
            branches = Branch.where(cluster_id: cluster.id)
            data[:legal_dependents] = Members::CurrentBranchesLegalDependents.new(branch_ids: branches.pluck(:id)).execute!
            render json: { success: true, info: "LegalDependents for cluster #{cluster}", data: data }
          rescue ActiveRecord::RecordNotFound => e
            render json: { success: false, info: "cluster_id not found", data: {} }, status: 404
          end
        else
          render json: { success: false, info: "cluster_id required", data: {} }, status: 500
        end
      end
    end
  end
end

