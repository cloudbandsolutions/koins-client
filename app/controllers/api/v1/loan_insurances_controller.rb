module Api
  module V1
    class LoanInsurancesController < ApplicationController
      before_action :authenticate_user!

      def generate_transaction
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Generating loan insurance transaction", email: current_user.email, username: current_user.username)
        collection_type        = params[:collection_type]
        branch_id              = params[:branch_id] 
        date_prepared          = params[:date_of_payment]
        loan_insurance_type_id = params[:loan_insurance_type_id]
        prepared_by            = current_user.full_name

        errors = LoanInsurances::ValidateNewLoanInsuranceTransaction.new(branch_id: branch_id, date_prepared: date_prepared, loan_insurance_type_id: loan_insurance_type_id, collection_type: collection_type).execute!

        if errors.length == 0
          loan_insurance = LoanInsurances::CreateLoanInsurance.new(collection_type: collection_type,
                                branch: Branch.find(branch_id), loan_insurance_type: LoanInsuranceType.find(loan_insurance_type_id),
                                date_prepared: date_prepared, prepared_by: prepared_by).execute!

          if loan_insurance.valid?
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully created loan insurance transaction.", email: current_user.email, username: current_user.username)
            loan_insurance.save!
            render json: { messages: ["Successfully created transaction. Redirecting..."], loan_insurance_id: loan_insurance.id }
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to create loan insurance transaction.", email: current_user.email, username: current_user.username)
            errors << "Something went wrong"
            loan_insurance.errors.messages.each do |m|
              errors << m
            end
            render json: { errors: errors } , status: 401
          end
        else
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Error in generating loan insurance transaction", email: current_user.email, username: current_user.username)
          render json: { errors: errors } , status: 401
        end
      end
      
      def delete_loan_insurance_record
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Deleting loan insurance record", email: current_user.email, username: current_user.username)
        loan_insurance_record = LoanInsuranceRecord.find(params[:loan_insurance_record_id])
        loan_insurance = loan_insurance_record.loan_insurance
        loan_insurance_record.destroy!
        loan_insurance.update!(updated_at: Time.now)
        render json: { message: "ok" }
      end

      def add_member
        loan_insurance = LoanInsurance.find(params[:id])
        member = Member.find(params[:member_id])
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Adding member #{member.to_s} to collection", email: current_user.email, username: current_user.username)
        loan_insurance_record = LoanInsurances::AddMemberToLoanInsurance.new(loan_insurance: loan_insurance, member: member).execute!
        loan_insurance_record.save!
        render json: { message: "Successfully added member" }
      end

      def approve
        loan_insurance = LoanInsurance.find(params[:id])
        errors = []
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to approve loan insurance)", email: current_user.email, username: current_user.username)

        if ["MIS", "BK", "SBK"].include? current_user.role
          errors = LoanInsurances::ValidateLoanInsuranceForApproval.new(loan_insurance: loan_insurance).execute!
          if errors.size == 0
            loan_insurance = LoanInsurances::ApproveLoanInsurance.new(loan_insurance: loan_insurance, user:  current_user).execute!
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved loan insurance)", email: current_user.email, username: current_user.username)
            render json: { message: "Successfully approved loan insurance" }
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to approve loan insurance", email: current_user.email, username: current_user.username)
            render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"
          render json: { message: "Unauthorized", errors: errors }, status: 401
        end
      end

      def reverse
        loan_insurance = LoanInsurance.find(params[:id])
        errors = LoanInsurances::ValidateLoanInsuranceForReversal.new(loan_insurance: loan_insurance).execute!
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to reverse loan insurance #{loan_insurance.branch} - #{loan_insurance.loan_insurance_type}.", email: current_user.email, username: current_user.username, record_reference_id: loan_insurance.id)

        if errors.size == 0
          LoanInsurances::ReverseLoanInsurance.new(loan_insurance: loan_insurance, user: current_user).execute!
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully reversed loan insurance #{loan_insurance.branch} - #{loan_insurance.loan_insurance_type}", email: current_user.email, username: current_user.username, record_reference_id: loan_insurance.id)
          render json: { message: "success" }
        else
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to reverse loan insurance #{loan_insurance.branch} - #{loan_insurance.loan_insurance_type}", email: current_user.email, username: current_user.username, record_reference_id: loan_insurance.id)
          render json: { message: "error", errors: errors }, status: 401
        end
      end
    end
  end
end
