module Api
  module V1
    class MoratoriumController < ApiController
      before_action :authenticate_user!

      def approve
        batch_moratorium = BatchMoratorium.find(params[:batch_moratorium_id])
        center_ids = params[:center_ids]
      end

      def init
        center_ids          = params[:center_ids]
        number_of_days      = params[:number_of_days]
        start_of_moratorium = params[:start_of_moratorium]
        batch_moratorium_id = params[:batch_moratorium_id]
        reason              = params[:reason]
        date_initialized    = Date.today

        if center_ids.present?
          @centers = Center.where(id: center_ids)
        else
          @centers = Center.all
        end

        errors = Moratorium::ValidateBatchMoratoriumInitialization.new(
                  date_initialized: date_initialized,
                  center_ids: center_ids,
                  number_of_days: number_of_days,
                  start_of_moratorium: start_of_moratorium).execute!

        if errors.size > 0
          render json: { errors: errors }, status: 401
        else
          @batch_moratorium = Moratorium::InitMoratorium.new(
                                batch_moratorium_id: batch_moratorium_id,
                                date_initialized: Date.today,
                                center_ids: center_ids,
                                number_of_days: number_of_days,
                                start_of_moratorium: start_of_moratorium,
                                initialized_by: current_user.full_name,
                                reason: reason).execute!

          @data = Moratorium::InitMoratoriumScheduleChange.new(
                    batch_moratorium: @batch_moratorium,
                    centers: @centers).execute!

          render json: { data: @data, batch_moratorium: @batch_moratorium }
        end
      end
    end
  end
end
