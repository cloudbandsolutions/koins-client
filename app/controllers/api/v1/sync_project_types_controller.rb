module Api
  module V1
    class SyncProjectTypesController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:project_type_categories] = ProjectTypeCategory.all
        data[:project_types] = ProjectType.all

        render json: { success: true, info: "Project types", data: data }
      end
    end
  end
end
