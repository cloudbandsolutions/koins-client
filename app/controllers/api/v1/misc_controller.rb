module Api
  module V1
    class MiscController < ApiController
      def download_loan_product_ep_cycle_count_max_amounts
        loan_product_ep_cycle_count_max_amounts = LoanProductEpCycleCountMaxAmount.all

        csvfile = CSV.generate do |csv|
          csv << ["id", "loan_product_id", "cycle_count", "max_amount"]

          loan_product_ep_cycle_count_max_amounts.each do |lpec|
            csv << [lpec.id, lpec.loan_product_id, lpec.cycle_count, lpec.max_amount]
          end
        end

        send_data csvfile, type: 'text/csv; harset=iso-8859-1; header=present', :disposition => "attachment;filename=loan-product-ep-cycle-count-max-amounts.csv"
      end

      def download_loan_product_dependencies
        loan_product_dependencies = LoanProductDependency.all

        csvfile = CSV.generate do |csv|
          csv << ["id", "cycle_count", "dependent_loan_product_id", "loan_product_id"]
          loan_product_dependencies.each do |lpd|
            csv << [lpd.id, lpd.cycle_count, lpd.dependent_loan_product_id, lpd.loan_product_id]
          end
        end

        send_data csvfile, type: 'text/csv; harset=iso-8859-1; header=present', :disposition => "attachment;filename=loan-product-dependencies.csv"
      end

      def download_loan_product_types
        loan_product_types = LoanProductType.all

        csvfile = CSV.generate do |csv|
          csv << ["id", "name", "loan_product_id", "processing_fee_15", "processing_fee_25", "processing_fee_35", "processing_fee_50", "fixed_processing_fee", "interest_rate"]
          loan_product_types.each do |lpt|
            csv << [lpt.id, lpt.name, lpt.loan_product_id, lpt.processing_fee_15, lpt.processing_fee_25, lpt.processing_fee_35, lpt.processing_fee_50, lpt.fixed_processing_fee, lpt.interest_rate]
          end
        end

        send_data csvfile, type: 'text/csv; harset=iso-8859-1; header=present', :disposition => "attachment;filename=loan-product-types.csv"
      end

      def download_loan_insurance_deduction_entries
        loan_insurance_deduction_entries = LoanInsuranceDeductionEntry.all

        csvfile = CSV.generate do |csv|
          csv << ["id", "loan_product_id", "insurance_type_id", "val", "is_percent", "accounting_code_id", "is_one_time_deduction"]
          
          loan_insurance_deduction_entries.each do |o|
            csv << [o.id, o.loan_product_id, o.insurance_type_id, o.val, o.is_percent, o.accounting_code_id, o.is_one_time_deduction]
          end
        end

        send_data csvfile, type: 'text/csv; harset=iso-8859-1; header=present', :disposition => "attachment;filename=loan-insurance-deduction-entries.csv"
      end

      def download_loan_deduction_entries
        loan_deduction_entries = LoanDeductionEntry.all

        csvfile = CSV.generate do |csv|
          csv << ["id", "loan_product_id", "loan_deduction_id", "val", "is_percent", "accounting_code_id", "is_one_time_deduction", "use_term_map", "t_val_15", "t_val_25", "t_val_50", "t_val_35"]

          loan_deduction_entries.each do |lde|
            csv << [lde.id, lde.loan_product_id, lde.loan_deduction_id, lde.val, lde.is_percent, lde.accounting_code_id, lde.is_one_time_deduction, lde.use_term_map, lde.t_val_15, lde.t_val_25, lde.t_val_50, lde.t_val_35]
          end
        end

        send_data csvfile, type: 'text/csv; harset=iso-8859-1; header=present', :disposition => "attachment;filename=loan-deduction-entries.csv"
      end

      def download_loan_deductions
        loan_deductions = LoanDeduction.all 

        csvfile = CSV.generate do |csv|
          csv << ["id", "name", "code", "is_one_time_fee"]
          loan_deductions.each do |ld|
            csv << [ld.id, ld.name, ld.code, ld.is_one_time_fee]
          end
        end

        send_data csvfile, type: 'text/csv; harset=iso-8859-1; header=present', :disposition => "attachment;filename=loan-deductions.csv"
      end
    end
  end
end
