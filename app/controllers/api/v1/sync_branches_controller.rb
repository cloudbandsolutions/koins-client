module Api
  module V1
    class SyncBranchesController < ApplicationController
      before_action :verify_api_key!

      def sync_all
        data = {}
        data[:branches] = Branch.all

        render json: { success: true, info: "Branches", data: data }
      end

      def by_cluster
        data = {}
        cluster = Cluster.find(params[:cluster_id])
        data[:branches] = Branch.where(cluster_id: cluster.id)

        render json: { success: true, info: "Branches", data: data }
      end
    end
  end
end

