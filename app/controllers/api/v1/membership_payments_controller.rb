module Api
  module V1
    class MembershipPaymentsController < ApplicationController
      before_action :authenticate_user!, only: [:approve]

      def approve
        member = Member.find(params[:member_id])
        membership_payment = MembershipPayment.find(params[:membership_payment_id])
        Membership::ApproveMembershipPayment.new(membership_payment: membership_payment, current_user: current_user).execute!

        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved membership payment", email: current_user.email, username: current_user.username, record_reference_id: membership_payment.id)

        render json: { message: "Successfully approved payment" }
      end
    end
  end
end
