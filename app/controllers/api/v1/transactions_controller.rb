module Api
  module V1
    class TransactionsController < ApplicationController
      def show
        transaction_number = params[:transaction_number] 
        transaction_category = params[:transaction_category]

        if transaction_category == 'insurance'
          transaction = InsuranceAccountTransaction.where(transaction_number: transaction_number).first

          render json: { success: true, info: 'transaction object', data: { transaction: transaction, transaction_number: transaction_number, transaction_category: transaction_category } }
        else
          render json: { success: false, info: 'invalid', data: { } }, status: 404
        end
      end
    end
  end
end
