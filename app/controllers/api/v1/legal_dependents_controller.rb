module Api
  module V1
    class LegalDependentsController < ApiController
      def create
        legal_dependent = LegalDependent.new(Members::NewLegalDependentRecord.new(legal_dependent_params: legal_dependent_params).execute!)

        if legal_dependent.save
          render json: { success: true, info: "Created legal_dependent #{legal_dependent.reference_number}", data: { legal_dependent: legal_dependent } }
        else
          render json: { success: false, info: "Error in creating legal_dependent. Error: #{legal_dependent.errors.messages}", data: { legal_dependent: legal_dependent } }
        end
      end

      def update
        legal_dependent_hash = legal_dependent_params
        legal_dependent = LegalDependent.where(reference_number: legal_dependent_hash[:reference_number]).first
        legal_dependent_hash[:member] = Member.where(identification_number: legal_dependent_hash[:member_identification_number]).first

        legal_dependent_p = legal_dependent_hash.except!(:member_identification_number)

        if legal_dependent.update(legal_dependent_p)
          render json: { success: true, info: "Updated legal_dependent #{legal_dependent.reference_number}", data: { legal_dependent: legal_dependent } }
        else
          render json: { success: false, info: "Error in updating legal_dependent. Error: #{legal_dependent.errors.messages}", data: { legal_dependent: legal_dependent } }
        end
      end

      def legal_dependent_params
        params.require(:legal_dependent).permit!
      end
    end
  end
end
