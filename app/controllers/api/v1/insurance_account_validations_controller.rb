module Api
  module V1
    class InsuranceAccountValidationsController < ApplicationController
      before_action :authenticate_user!

      def cancel
        insurance_account_validation = InsuranceAccountValidation.find(params[:id])
        errors = []

        if ["MIS", "AO", "CM", "FM", "REMOTE-FM", "OM"].include? current_user.role
          errors  = InsuranceAccountValidations::ValidateInsuranceAccountValidationForCancellation.new(
                      insurance_account_validation: insurance_account_validation
                    ).execute!

          if errors.size == 0
            insurance_account_validation  = InsuranceAccountValidations::CancelInsuranceAccountValidation.new(
                                              insurance_account_validation: insurance_account_validation,
                                              user:  current_user
                                            ).execute!
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully cancelled insurance validation account)", 
              email: current_user.email, 
              username: current_user.username
            )

            render json: { message: "Successfully cancelled insurance_account_validation" }
          else
            render json: { message: "Cannot cancel this transaction", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"

          render json: { message: "Unauthorized", errors: errors }, status: 401
        end
      end

      def cancel_member
        member         = Member.where(id: params[:member_id]).first
        insurance_account_validation_record = InsuranceAccountValidationRecord.where(id: params[:insurance_account_validation_record_id]).first
        insurance_account_validation = InsuranceAccountValidation.find(params[:id])
        date_cancelled = params[:date_cancelled]
        reason         = params[:reason]
        errors = []

        if ["MIS", "OAS", "REMOTE-OAS", "REMOTE-BK", "AO", "OM"].include? current_user.role
          errors  = InsuranceAccountValidations::ValidateInsuranceAccountValidationMemberForCancellation.new(
                      member: member,
                      date_cancelled: date_cancelled,
                      reason:reason,
                    ).execute!

          if errors.size == 0
            insurance_account_validation  = InsuranceAccountValidations::CancelMemberToInsuranceAccountValidation.new(
                                              insurance_account_validation_id: insurance_account_validation.id,
                                              member: member.id,
                                              date_cancelled: date_cancelled,
                                              reason:reason, 
                                              user:  current_user
                                            ).execute!
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully cancelled member to insurance validation account)", 
              email: current_user.email, 
              username: current_user.username
            )

            render json: { message: "Successfully cancelled member to insurance_account_validation" }
          else
            render json: { message: "Cannot cancel this transaction", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"

          render json: { message: "Unauthorized", errors: errors }, status: 401
        end
      end

      def check
        insurance_account_validation = InsuranceAccountValidation.find(params[:id])
        errors = []

        if ["MIS", "CM", "FM", "REMOTE-FM"].include? current_user.role
          errors  = InsuranceAccountValidations::ValidateInsuranceAccountValidationForChecking.new(
                      insurance_account_validation: insurance_account_validation
                    ).execute!

          if errors.size == 0
            insurance_account_validation  = InsuranceAccountValidations::CheckInsuranceAccountValidation.new(
                                              insurance_account_validation: insurance_account_validation, 
                                              user:  current_user
                                            ).execute!
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully validated insurance validation account)", 
              email: current_user.email, 
              username: current_user.username
            )

            render json: { message: "Successfully checked insurance_account_validation" }
          else
            render json: { message: "Cannot check this transaction", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"

          render json: { message: "Unauthorized", errors: errors }, status: 401
        end
      end

      def validate
        insurance_account_validation = InsuranceAccountValidation.find(params[:id])
        errors = []

        if ["MIS", "OM", "AO"].include? current_user.role
          errors  = InsuranceAccountValidations::ValidateInsuranceAccountValidationForValidation.new(
                      insurance_account_validation: insurance_account_validation
                    ).execute!

          if errors.size == 0
            insurance_account_validation  = InsuranceAccountValidations::ValidateInsuranceAccountValidation.new(
                                              insurance_account_validation: insurance_account_validation, 
                                              user:  current_user
                                            ).execute!
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully validated insurance validation account)", 
              email: current_user.email, 
              username: current_user.username
            )

            render json: { message: "Successfully validated insurance_account_validation" }
          else
            render json: { message: "Cannot validate this transaction", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"

          render json: { message: "Unauthorized", errors: errors }, status: 401
        end
      end

      def generate_transaction
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Generating insurance validation account transaction", 
          email: current_user.email, 
          username: current_user.username
        )

        branch_id              = params[:branch_id] 
        date_prepared          = params[:date_prepared]
        prepared_by            = current_user.full_name

        errors = []

        if ["MIS", "OAS", "CM", "REMOTE-OAS", "REMOTE-BK"].include? current_user.role
          errors = InsuranceAccountValidations::ValidateNewInsuranceAccountValidationTransaction.new(branch_id: branch_id, date_prepared: date_prepared).execute!

          if errors.length == 0
            insurance_account_validation  = InsuranceAccountValidations::CreateInsuranceAccountValidation.new(
                                              branch: Branch.find(branch_id), 
                                              date_prepared: date_prepared, 
                                              prepared_by: prepared_by,
                                              is_remote: User::REMOTE_ROLES.include?(current_user.role)
                                            ).execute!

            if insurance_account_validation.valid?
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully created insurance validation account transaction.", email: current_user.email, username: current_user.username)
              insurance_account_validation.save!
              render json: { messages: ["Successfully created transaction. Redirecting..."], insurance_account_validation_id: insurance_account_validation.id }
            else
              UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to create insurance_account_validation transaction.", email: current_user.email, username: current_user.username)
              errors << "Something went wrong"
              insurance_account_validation.errors.messages.each do |m|
                errors << m
              end
              render json: { errors: errors } , status: 401
            end
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Error in generating insurance validation account transaction", email: current_user.email, username: current_user.username)
            render json: { errors: errors } , status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"

          render json: { message: "Unauthorized", errors: errors }, status: 401
        end      
      end
      
      def delete_insurance_account_validation_record
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Deleting insurance validation account record", 
          email: current_user.email, 
          username: current_user.username
        )

        insurance_account_validation_record = InsuranceAccountValidationRecord.find(params[:insurance_account_validation_record_id])
        insurance_account_validation = insurance_account_validation_record.insurance_account_validation
        insurance_account_validation_record.destroy!
        insurance_account_validation.update!(updated_at: Time.now)

        render json: { message: "ok" }
      end

      def add_member
        insurance_account_validation = InsuranceAccountValidation.find(params[:id])
        member = Member.find(params[:member_id])
        resignation_date = params[:resignation_date]
        member_classification = params[:member_classification]
        
        errors  = InsuranceAccountValidations::ValidateMember.new(
                    member: member, 
                    insurance_account_validation: insurance_account_validation, 
                    resignation_date: resignation_date
                  ).execute!

        if errors.length == 0
          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Adding member #{member.to_s} to collection", 
            email: current_user.email, 
            username: current_user.username
          )

          insurance_account_validation_record = InsuranceAccountValidations::AddMemberToInsuranceAccountValidation.new(
                                                  insurance_account_validation: insurance_account_validation, 
                                                  member: member, 
                                                  resignation_date: resignation_date,
                                                  member_classification: member_classification
                                                ).execute!

          insurance_account_validation_record.save!
          render json: { message: "Successfully added member" }
        else
          render json: { errors: errors } , status: 401
        end
      end

      def approve
        insurance_account_validation = InsuranceAccountValidation.find(params[:id])
        errors = []

        if ["MIS", "BK"].include? current_user.role
          errors =  InsuranceAccountValidations::ValidateInsuranceAccountValidationForApproval.new(
                      insurance_account_validation: insurance_account_validation
                    ).execute!
          if errors.size == 0
            insurance_account_validation  = InsuranceAccountValidations::ApproveInsuranceAccountValidation.new(
                                              insurance_account_validation: insurance_account_validation, 
                                              user:  current_user
                                            ).execute!

            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully approved insurance validation account", 
              email: current_user.email, 
              username: current_user.username
            )

            render json: { message: "Successfully approved insurance_account_validation" }
          else
            render json: { message: "Cannot approve this transaction", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"
          render json: { message: "Unauthorized", errors: errors }, status: 401
        end
      end

      def reverse
        insurance_account_validation = InsuranceAccountValidation.find(params[:id])
        errors = []
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to reverse insurance validation account #{insurance_account_validation.branch}.", email: current_user.email, username: current_user.username, record_reference_id: insurance_account_validation.id)
        
        if ["MIS", "ACC"].include? current_user.role
          errors = InsuranceAccountValidations::ValidateInsuranceAccountValidationForReversal.new(insurance_account_validation: insurance_account_validation).execute!
          if errors.size == 0
            InsuranceAccountValidations::ReverseInsuranceAccountValidation.new(insurance_account_validation: insurance_account_validation, user: current_user).execute!
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully reversed insurance validation account #{insurance_account_validation.branch}", email: current_user.email, username: current_user.username, record_reference_id: insurance_account_validation.id)
            render json: { message: "success" }
          else
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to reverse insurance validation account #{insurance_account_validation.branch}", email: current_user.email, username: current_user.username, record_reference_id: insurance_account_validation.id)
            render json: { message: "error", errors: errors }, status: 401
          end
        else
          errors << "Unauthorized to perform this transaction"
          render json: { message: "Unauthorized", errors: errors }, status: 401  
        end
      end
    end
  end
end