module Api
  module V1
    class HeadOfficeController < ApiController
      before_action :authenticate_user!
      def send_final_daily_report
        if !daily_report
          render json: { errors: ["No daily report found"] }, status: 404
        else
          if ::Postings::PostStatistics.new(daily_report: daily_report).execute!
            render json: { message: "ok" }
          else
            render json: { errors: ["Error in sending daily report"] }, status: 500
          end
        end
      end

      def send_daily_report
        daily_report  = DailyReport.where(id: params[:id]).first

        if !daily_report
          render json: { errors: ["No daily report found"] }, status: 404
        else
          if ::Postings::PostStatistics.new(daily_report: daily_report).execute!
            render json: { message: "ok" }
          else
            render json: { errors: ["Error in sending daily report"] }, status: 500
          end
        end
      end
    end
  end
end
