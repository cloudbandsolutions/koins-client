module Api
  module V1
    module Transfer
      class CenterTransferRecordsController < ApplicationController
        def approve
          center_transfer_record  = CenterTransferRecord.find(params[:id])
          user                    = current_user

          # TODO: Validate
          ::Transfer::ApproveCenterTransferRecord.new(
            center_transfer_record: center_transfer_record,
            user: user
          ).execute!

          render json: { message: "ok" }
        end
      end
    end
  end
end
