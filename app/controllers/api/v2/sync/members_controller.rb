module Api
  module V2
    module Sync
      class MembersController < ApiController
        def index
          page              = params[:page].try(:to_i) || 1
          per               = params[:per].try(:to_i)  || 10
          num_pages         = Member.all.page(page).per(per).total_pages
          members = ::Builders::BuildMembers.new(
                      page: page,
                      per: per
                    ).execute!

          next_page = page + 1
          if next_page > num_pages
            next_page = -1
          end

          render  json: { 
                    data: members, 
                    next_page: next_page, 
                    total_pages: num_pages 
                  }
        end
      end
    end
  end
end
