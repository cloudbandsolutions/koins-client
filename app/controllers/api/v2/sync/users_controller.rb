module Api
  module V2
    module Sync
      class UsersController < ApiController
        def index
          users = ::Builders::BuildUsers.new.execute!

          render json: { data: users }
        end
      end
    end
  end
end
