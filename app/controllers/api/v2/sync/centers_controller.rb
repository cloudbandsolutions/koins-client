module Api
  module V2
    module Sync
      class CentersController < ApiController
        def index
          centers = ::Builders::BuildCenters.new.execute!

          render  json: { data: centers }
        end
      end
    end
  end
end
