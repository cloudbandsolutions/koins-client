module Api
  module V2
    module Sync
      class AccountingEntriesController < ApiController
        def index
          accounting_entries  = ::Builders::BuildAccountingEntries.new.execute!

          render  json: { 
                    data: accounting_entries
                  }
        end
      end
    end
  end
end
