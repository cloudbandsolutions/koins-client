module Api
  module V2
    class MembersController < ApiController
      def index
        data = []
        Member.all.each do |m|
          data << m.to_hash
        end

        render json: { members: data }
      end
    end
  end
end
