class MemberSharesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_member, except: [:index, :shares_for_printing,:shares_printed_pdf]

  def index
    #@member_shares  = MemberShare.joins(:member).order("member_shares.printed, members.last_name ASC, members.status ASC")
    @member_shares = MemberShare.joins(:member).order("member_shares.date_printed DESC, members.status, member_shares.date_of_issue ,members.last_name ASC")

    if params[:q].present?
      @q  = params[:q]

      @member_shares  = @member_shares.where("member_shares.certificate_number LIKE ?", "#{@q}%")
    end

    if params[:start_date_of_issue].present? and params[:end_date_of_issue].present?
      @start_date_of_issue = params[:start_date_of_issue]
      @end_date_of_issue   = params[:end_date_of_issue]


      @member_shares  = @member_shares.where("member_shares.date_of_issue >= ? AND member_shares.date_of_issue <= ?", @start_date_of_issue, @end_date_of_issue)
    end

    if params[:start_date_printed].present? and params[:end_date_printed].present?
      @start_date_printed = params[:start_date_printed]
      @end_date_printed   = params[:end_date_printed]

      @member_shares  = @member_shares.where("member_shares.date_printed >= ? AND member_shares.date_printed <= ?", @start_date_printed, @end_date_printed)
    end
    
    @limit = 100
    @counter = 0
    if params[:page].present?
      @counter = (params[:page].to_i - 1)  * @limit
    end
    @member_shares  = @member_shares.page(params[:page]).per(@limit)
  end


  def shares_printed_pdf
    @member_shared = MemberShare.joins(:member => :center).order("member_shares.date_of_issue ASC, members.status, member_shares.date_printed,members.last_name ASC")

    if params[:start_date_of_issue].present? and params[:end_date_of_issue].present?
      @start_date_of_issue = params[:start_date_of_issue]
      @end_date_of_issue   = params[:end_date_of_issue]

      @member_shared  = @member_shared.where("member_shares.date_of_issue >= ? AND member_shares.date_of_issue <= ?", @start_date_of_issue, @end_date_of_issue)
    end

    if params[:start_date_printed].present? and params[:end_date_printed].present?
      @start_date_printed = params[:start_date_printed]
      @end_date_printed   = params[:end_date_printed]

      @member_shared  = @member_shared.where("member_shares.date_printed >= ? AND member_shares.date_printed <= ?", @start_date_printed, @end_date_printed)
    end
  end

  def load_member
    @member = Member.find(params[:member_id])
    #if !@member.active?
    if !["active", "resigned", "resign"].include?(@member.status)
      flash[:error] = "Cannot issue share to non-active member"
      redirect_to member_path(@member)
    end
  end

  def shares_for_printing
    #printed_member_shares = MemberShare.where(printed: true , is_void: '')
    #printed_member_ids    = Member.where(id: printed_member_shares.pluck(:member_id))
    #@members_for_printing = Member.active.where.not(id: printed_member_ids).order("previous_mfi_member_since ASC, last_name ASC")
    @data = {}
    @data[:sfp] = []
    x = Member.joins(:equity_accounts).where("members.status = 'active'").order("equity_accounts.updated_at ASC")
  
    x.each do |y|
      sfp = {}
      eq = EquityAccount.where("member_id = ?" , y.id).sum(:balance).to_i
      eq_date = EquityAccount.where("member_id = ?" , y.id).pluck(:updated_at).to_s
      eqm = eq/100
      mem = Member.joins(:member_shares).where("members.id = ? and member_shares.is_void IS NULL" , y.id).sum(:number_of_shares)
      if eqm > mem
        sfp[:full_name] = y.full_name
        sfp[:id] = y.id
        sfp_center = y.center_id
        sfp[:center] = Center.find(sfp_center).name
        sfp[:shares_printed] = mem
        sfp[:shares_for_printing] = eqm - mem
        sfp[:update_date] = eq_date.to_date 
        @data[:sfp] << sfp
        
      end
    end
    @data
  end

  def pdf
    @member_share = MemberShare.find(params[:member_share_id])
  end

  def excel
    @member_share = MemberShare.find(params[:member_share_id])
    filename = "share_#{@member_share.certificate_number}.xlsx"
    report_package  = Members::GenerateMemberShareCertificateExcel.new(member: @member, member_share: @member_share).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def new
    @x = EquityAccountTransaction.joins(:equity_account).where("equity_accounts.member_id = ?", @member.id).last
    x_count = (@x.amount/100).to_i
    #@member_share = MemberShare.new(number_of_shares: 1 , date_of_issue: @member.date_accepted_coop)
    @member_share = MemberShare.new(number_of_shares: x_count , date_of_issue: @x.transacted_at)
  end

  def create
    @member_share = MemberShare.new(member_share_params)
    @member_share.member = @member

    if @member_share.save
      flash[:success] = "Successfully saved member share"
      redirect_to member_member_share_path(@member, @member_share)
    else
      flash[:error] = "Cannot save member share"
      render :new
    end
  end

  def edit
    @member_share = MemberShare.find(params[:id])
  end

  def update
    @member_share = MemberShare.find(params[:id])

    if @member_share.update(member_share_params)
      flash[:success] = "Successfully saved member share"
      redirect_to member_member_share_path(@member, @member_share)
    else
      flash[:error] = "Cannot save member share"
      render :edit
    end
  end

  def destroy
    @member_share = MemberShare.find(params[:id])
    @member_share.destroy!
    flash[:success] = "Successfully deleted member share"
    redirect_to member_path(@member)
  end

  def show
    @member_share = MemberShare.find(params[:id])
  end

  def member_share_params
    params.require(:member_share).permit!
  end
end
