class BuildVersionsController < ApplicationController
  before_action :load_defaults
  before_action :authenticate_user!

  def load_defaults
  end

	def index	
    @build_versions = BuildVersion.all.order("build_versions.created_at DESC")
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Viewed all build version records.", email: current_user.email, username: current_user.username)
	end

	def new
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to create new build version.", email: current_user.email, username: current_user.username)
		@build_version = BuildVersion.new
	end

	def create
		@build_version = BuildVersion.new(build_version_params)

   	if @build_version.save
   	 	flash[:success] = "Successfully saved record."
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully created new build version.", email: current_user.email, username: current_user.username)
      redirect_to build_versions_path
    else
     	flash[:error] = "Error in saving record."
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to create new build version.", email: current_user.email, username: current_user.username)
      render :new
   	end
	end

	def edit
    @build_version = BuildVersion.find(params[:id])
    @build_category = BuildCategory.new
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "About to edit build version", email: current_user.email, username: current_user.username, record_reference_id: @build_version.id)
  end

  def update
   @build_version = BuildVersion.find(params[:id])

    if @build_version.update(build_version_params)
      flash[:success] = "Successfully saved record."
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated build version", email: current_user.email, username: current_user.username, record_reference_id: @build_version.id)
      redirect_to build_versions_path
    else
      flash[:error] = "Error in saving record."
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update build version", email: current_user.email, username: current_user.username, record_reference_id: @survey_respondent.id)
      render :new
    end
  end

	 def show
    @build_version = BuildVersion.find(params[:id])
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying build_version - #{@build_version.build_number}", email: current_user.email, username: current_user.username, record_reference_id: @build_version.id)
  end

	def build_version_params
    params.require(:build_version).permit!
  end

end
