class TrialBalanceController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults

  def index
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Visited Trial Balance", email: current_user.email, username: current_user.username)
  end

  def load_defaults
    if params[:branch_id].present?
      branch = Branch.find(params[:branch_id])
    end
  end

  def generate_download_pdf_url
    branch = nil

    if params[:branch_id].present?
      branch = Branch.find(params[:branch_id])
    end

    data = FinancialReports::TrialBalance.get_entries(params[:start_date], params[:end_date], branch)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Downloaded Trial Balance", email: current_user.email, username: current_user.username)
    render json: { data: data, download_url: "#{trial_balance_download_pdf_path(start_date: params[:start_date], end_date: params[:end_date], branch_id: branch.try(:id))}" }
  end

  def download_pdf
    branch = nil

    if params[:branch_id].present?
      branch = Branch.find(params[:branch_id])
    end

    @data = FinancialReports::TrialBalance.get_entries(params[:start_date], params[:end_date], branch)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Downloaded Trial Balance", email: current_user.email, username: current_user.username)
    render pdf: "trial_balance.pdf", layout: "pdf", orientation: 'Landscape', page_size: 'Legal', grayscale: false, margin: { bottom: 15 }, footer: { html: { template: 'pdfs/footer.html.erb' } }, page_offset: 0
  end

  def entries
    branch = nil

    if params[:branch_id].present?
      branch = Branch.find(params[:branch_id])
    end

    data = FinancialReports::TrialBalance.get_entries(params[:start_date], params[:end_date], branch)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Generating Financial Report: Trial Balance", email: current_user.email, username: current_user.username)
    render json: { data: data }
  end

  def generate_download_excel_url
    branch = nil

    if params[:branch_id].present?
      branch = Branch.find(params[:branch_id])
    end

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Downloaded Excel Trial Balance", 
      email: current_user.email, 
      username: current_user.username
    )

    render json: { download_url: "#{trial_balance_download_excel_path(start_date: params[:start_date], end_date: params[:end_date], branch_id: branch.try(:id))}" }
  end

  def download_excel
    branch = nil

    if params[:branch_id].present?
      branch = Branch.find(params[:branch_id])
    end
    filename = "Trial Balance.xlsx"
    data = FinancialReports::TrialBalance.get_entries(
            params[:start_date], 
            params[:end_date], 
            branch)
    
    report_package =  FinancialReports::GenerateExcelForTrialBalance.new(
                        data: data, 
                        start_date: params[:start_date], 
                        end_date: params[:end_date],
                        user: current_user
                      ).execute!

    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end
end
