class InsuranceRemittancesController < ApplicationController
  before_action :load_defaults, :authenticate_user!
  before_action :load_insurance_remittance, only: [:edit, :update, :destroy]

  def load_defaults
    @branches = Branch.all
  end

  def load_insurance_remittance
    @insurance_remittance = InsuranceRemittance.find(params[:id])
  end

  def index
    @insurance_remittances = InsuranceRemittance.all
  end

  def new
    if !params[:insurance_remittance][:branch].present? or !params[:insurance_remittance][:center].present?
      flash[:error] = "Please specify a branch and center"
      redirect_to insurance_remittances_path
    elsif !params[:insurance_remittance][:start_date].present? or !params[:insurance_remittance][:end_date].present?
      flash[:error] = "Please specify a start and end date"
      redirect_to insurance_remittances_path
    else
      branch = Branch.find(params[:insurance_remittance][:branch])
      center = Center.find(params[:insurance_remittance][:center])
      start_date = params[:insurance_remittance][:start_date]
      end_date = params[:insurance_remittance][:end_date]

      insurance_remittance = Insurance::BuildInsuranceRemittance.new(start_date: start_date, end_date: end_date, branch: branch, center: center).execute!
      redirect_to edit_insurance_remittance_path(insurance_remittance)
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end
end
