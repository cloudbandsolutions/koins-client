class ApiController < ActionController::Base
  protect_from_forgery with: :null_session

  def secure_api
    if !user_signed_in?
      render json: { message: "Unauthorized" }, status: 401
    end
  end
end
