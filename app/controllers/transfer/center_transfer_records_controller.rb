module Transfer
  class CenterTransferRecordsController < ApplicationController
    before_action :authenticate_user!
    before_action :load_object, only: [:edit, :update, :show, :destroy]

    def load_object
      @center_transfer_record = CenterTransferRecord.find(params[:id])
    end

    def index
      @center_transfer_records  = CenterTransferRecord.select("*")
    end

    def new
      @center_transfer_record = CenterTransferRecord.new
    end

    def create
      @center_transfer_record = CenterTransferRecord.new(center_transfer_record_params)

      data  = Paperclip.io_adapters.for(@center_transfer_record.file).read
      @center_transfer_record.branch      = @center_transfer_record.center.try(:branch)
      @center_transfer_record.data        = data
      @center_transfer_record.prepared_by = current_user.full_name

      if @center_transfer_record.save
        flash[:success] = "Successfully created record"
        redirect_to transfer_center_transfer_record_path(@center_transfer_record)
      else
        Rails.logger.error @center_transfer_record.errors.messages
        flash[:error] = "Error in saving center transfer record"
        render :new
      end
    end

    def edit
    end

    def update
      data  = Paperclip.io_adapters.for(@center_transfer_record.file).read
      @center_transfer_record.branch      = @center_transfer_record.center.try(:branch)
      @center_transfer_record.data        = data
      @center_transfer_record.prepared_by = current_user.full_name

      if @center_transfer_record.update(center_transfer_record_params)
        flash[:success] = "Successfully updated record"
        redirect_to transfer_center_transfer_record_path(@center_transfer_record)
      else
        flash[:error] = "Error in saving center transfer record"
        render :edit
      end
    end

    def show
      @voucher  = ::Transfer::ProduceCenterTransferRecordAccountingEntry.new(
                    center_transfer_record: @center_transfer_record,
                    user: current_user
                  ).execute!
    end

    def destroy
      if @center_transfer_record.pending?
        @center_transfer_record.destroy!
        flash[:success] = "Successfully destroyed record"
        redirect_to transfer_center_transfer_records_path
      else
        flash[:error] = "Error in destroying center transfer record"
        redirect_to transfer_center_transfer_record_path(@center_transfer_record)
      end
    end

    private

    def center_transfer_record_params
      params.require(:center_transfer_record).permit!
    end
  end
end
