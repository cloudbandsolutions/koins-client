class DeceasedAndTotalAndPermanentDisabilitiesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults, :authenticate_user!
  before_action :load_record, only: [:edit, :update, :destroy, :show]
  before_action :load_types

  def load_record
    @deceased_and_total_and_permanent_disability = DeceasedAndTotalAndPermanentDisability.find(params[:id])
  end

  def load_types
    @branches         = Branch.all
    @centers          = Center.all
  end

  def index
    @deceased_and_total_and_permanent_disabilities = DeceasedAndTotalAndPermanentDisability.all.order("date_prepared DESC")
    @deceased_and_total_and_permanent_disabilities = @deceased_and_total_and_permanent_disabilities.page(params[:page]).per(20)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying all Deceased and Total and Permanent Disabilities", email: current_user.email, username: current_user.username)

    if params[:status].present?
      @status = params[:status]
      @deceased_and_total_and_permanent_disabilities = @deceased_and_total_and_permanent_disabilities.where(status: @status)
    end

    if params[:branch_id].present?
      @branch_id = params[:branch_id]
      @deceased_and_total_and_permanent_disabilities = @deceased_and_total_and_permanent_disabilities.where(branch_id: @branch_id)
    end
  end

  def edit
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to edit Deceased and Total and Permanent Disability - #{@deceased_and_total_and_permanent_disability.branch}.", email: current_user.email, username: current_user.username, record_reference_id: @deceased_and_total_and_permanent_disability.id)
    @legal_dependents = LegalDependent.joins(:member).where("members.status = ? AND members.branch_id = ? AND is_deceased != ? AND is_tpd != ?", "active", @deceased_and_total_and_permanent_disability.branch.id, true, true).all.order("last_name ASC")
  end

  def update
    if @deceased_and_total_and_permanent_disability.update(deceased_and_total_and_permanent_disability_params)
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated deceased and_total_and_permanent_disability - #{@deceased_and_total_and_permanent_disability.branch}.", email: current_user.email, username: current_user.username, record_reference_id: @deceased_and_total_and_permanent_disability.id)
      flash[:success] = "Successfully saved transaction."
      redirect_to deceased_and_total_and_permanent_disability_path(@deceased_and_total_and_permanent_disability)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update Deceased and Total and Permanent Disability", email: current_user.email, username: current_user.username, record_reference_id: @deceased_and_total_and_permanent_disability.id)
      flash[:error] = "Error in saving transaction"
      render :edit
    end
  end


  def show 
    @deceased_and_total_and_permanent_disability = DeceasedAndTotalAndPermanentDisability.find(params[:id])
    @legal_dependents = LegalDependent.where(branch_id: @deceased_and_total_and_permanent_disability.branch.id).all
    @role = current_user.role
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying Deceased and Total and Permanent Disability for #{@deceased_and_total_and_permanent_disability.branch}", email: current_user.email, username: current_user.username, record_reference_id: @deceased_and_total_and_permanent_disability.id)
  end

  def destroy
   if !@deceased_and_total_and_permanent_disability.pending?
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to destroy Deceased and Total and Permanent Disability", email: current_user.email, username: current_user.username, record_reference_id: @deceased_and_total_and_permanent_disability.id)
      flash[:error] = "Cannot destroy non pending record"
      redirect_to deceased_and_total_and_permanent_disability_path(@deceased_and_total_and_permanent_disability)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully destroyed Deceased and Total and Permanent Disability.", email: current_user.email, username: current_user.username, record_reference_id: @deceased_and_total_and_permanent_disability.id)
      @deceased_and_total_and_permanent_disability.destroy!
      flash[:success] = "Successfully destroyed Deceased and Total and Permanent Disability."
      redirect_to deceased_and_total_and_permanent_disabilities_path
    end
  end

  def approve
    deceased_and_total_and_permanent_disability = deceased_and_total_and_permanent_disability.find(params[:deceased_and_total_and_permanent_disability_id])
  end

  def load_defaults
    @centers = Center.all
   
    if params[:action] == 'index'
      if params[:q].present?
        @q = params[:q]
      end

      if params[:status].present?
        @status = params[:status]
      end

      if params[:branch_id].present?
        @branch_id = params[:branch_id]
        @branch = Branch.find(@branch_id)
      end
    end
  end

  def deceased_and_total_and_permanent_disability_params
    params.require(:deceased_and_total_and_permanent_disability).permit!
  end
end
