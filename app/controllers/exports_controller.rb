class ExportsController < ApplicationController
  before_action :authenticate_user!

  def dependents_report
    @members  = Member.active
    excel     = Members::GenerateMemberDependentsReportExcel.new(
                  members: @members
                ).execute!

    filename  = "dependents_report.xlsx"

    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def members
    if params[:start_date].present? && params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      #@members = Member.where(created_at: @start_date.beginning_of_day..@end_date.end_of_day)
      @members = Member.where("Date(members.updated_at) >= ? AND Date(members.updated_at) <= ?", @start_date, @end_date)
      send_data Members::GenerateMembersCsv.new(members: @members).execute!, :type => 'text/csv; charset=utf-8; header=present', :disposition => "attachment; filename=members #{@start_date}_#{@end_date}.csv"
    else
      @members = Member.all
      send_data Members::GenerateMembersCsv.new(members: @members).execute!, :type => 'text/csv; charset=utf-8; header=present', :disposition => "attachment; filename=members.csv"
    end
    
    
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully export members information.", email: current_user.email, username: current_user.username)
  end

  def beneficiaries
    if params[:start_date].present? && params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      #@members = Member.where(created_at: @start_date.beginning_of_day..@end_date.end_of_day)
      @beneficiaries = Beneficiary.where("Date(beneficiaries.updated_at) >= ? AND Date(beneficiaries.updated_at) <= ?", @start_date, @end_date)
      send_data Members::GenerateBeneficiariesCsv.new(beneficiaries: @beneficiaries).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: "attachment; filename=beneficiaries #{@start_date}_#{@end_date}.csv"
    else
      @beneficiaries = Beneficiary.all
      send_data Members::GenerateBeneficiariesCsv.new(beneficiaries: @beneficiaries).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: "attachment; filename=beneficiaries.csv"
    end
    
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully export beneficiaries informaton.", email: current_user.email, username: current_user.username)
  end

  def legal_dependents
    if params[:start_date].present? && params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      #@members = Member.where(created_at: @start_date.beginning_of_day..@end_date.end_of_day)
      @legal_dependents = LegalDependent.where("Date(legal_dependents.updated_at) >= ? AND Date(legal_dependents.updated_at) <= ?", @start_date, @end_date)
      send_data Members::GenerateLegalDependentsCsv.new(legal_dependents: @legal_dependents).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: "attachment; filename=legal dependents #{@start_date}_#{@end_date}.csv"
    else
      @legal_dependents = LegalDependent.all
      send_data Members::GenerateLegalDependentsCsv.new(legal_dependents: @legal_dependents).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: "attachment; filename=legal dependents.csv"
    end

    
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully export legal dependents information.", email: current_user.email, username: current_user.username)
  end

  def insurance_accounts
    if params[:start_date].present? && params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      #@members = Member.where(created_at: @start_date.beginning_of_day..@end_date.end_of_day)
      @insurance_accounts = InsuranceAccount.where("Date(insurance_accounts.updated_at) >= ? AND Date(insurance_accounts.updated_at) <= ?", @start_date, @end_date)
      send_data Members::GenerateInsuranceAccountsCsv.new(insurance_accounts: @insurance_accounts).execute!, :type => 'text/csv; charset=utf-8; header=present', :disposition => "attachment; filename=insurance accounts #{@start_date}_#{@end_date}.csv"
    else
      @insurance_accounts = InsuranceAccount.all
      send_data Members::GenerateInsuranceAccountsCsv.new(insurance_accounts: @insurance_accounts).execute!, :type => 'text/csv; charset=utf-8; header=present', :disposition => "attachment; filename=insurance accounts.csv"
    end

    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully export insurance accounts information.", email: current_user.email, username: current_user.username)
  end

  def insurance_account_transactions
     if params[:start_date].present? && params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      #@members = Member.where(created_at: @start_date.beginning_of_day..@end_date.end_of_day)
      @insurance_account_transactions = InsuranceAccountTransaction.where("Date(insurance_account_transactions.updated_at) >= ? AND Date(insurance_account_transactions.updated_at) <= ?", @start_date, @end_date)
      send_data Members::GenerateInsuranceAccountTransactionsCsv.new(insurance_account_transactions: @insurance_account_transactions).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: "attachment; filename=insurance account transactions #{@start_date}_#{@end_date}.csv"
    else
      @insurance_account_transactions = InsuranceAccountTransaction.all
      send_data Members::GenerateInsuranceAccountTransactionsCsv.new(insurance_account_transactions: @insurance_account_transactions).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: "attachment; filename=insurance account transactions.csv"
    end   

    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully export insurance account transactions informaton.", email: current_user.email, username: current_user.username)
  end

  def withdrawal_for_hiip
    # Only for HIIP 250 withdrawal amount
    if params[:start_date].present? && params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]
      
      @data = ::Insurance::FetchPaymentCollectionWithdrawalForHiip.new().execute!

      # @savings_account_transactions = SavingsAccountTransaction.where("amount = ? AND transaction_type = ? AND lower(particular) not like lower(?) AND lower(particular) not like lower(?) AND lower(particular) not like lower(?)", 250, "withdraw", "%#{"rmsi"}%", "%#{"kok"}%", "%#{"kalinga"}%")
      send_data Members::GenerateWithdrawalForHiipCsv.new(data: @data).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: "attachment; filename=withdrawal_for_hiip.csv"
    else  
      @data = ::Insurance::FetchPaymentCollectionWithdrawalForHiip.new().execute!

      # @savings_account_transactions = SavingsAccountTransaction.where("amount = ? AND transaction_type = ? AND lower(particular) not like lower(?) AND lower(particular) not like lower(?) AND lower(particular) not like lower(?)", 250, "withdraw", "%#{"rmsi"}%", "%#{"kok"}%", "%#{"kalinga"}%")
      send_data Members::GenerateWithdrawalForHiipCsv.new(data: @data).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: "attachment; filename=withdrawal_for_hiip.csv"
    end

      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully export withdrawal for hiip informaton.", email: current_user.email, username: current_user.username)
  end

  def insured_loans
     if params[:start_date].present? && params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @data = Members::FetchInsuredLoansForCsv.new(start_date: @start_date, end_date: @end_date).execute!
    else
      @data = Members::FetchInsuredLoansForCsv.new(start_date: @start_date, end_date: @end_date).execute!
    end

    send_data Members::GenerateInsuredLoansCsv.new(data: @data).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: 'attachment; filename=insured_loans.csv'
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully export insured loans informaton.", email: current_user.email, username: current_user.username)
  end  

  def beneficiaries_excel
    center        = Center.where(id: params[:center_id]).first
    beneficiaries = Members::GenerateBeneficiaries.new(center: center).execute!
    excel         = Members::GenerateBeneficiariesExcel.new(data: beneficiaries, center: center).execute!
    filename      = "beneficiaries_#{center.try(:to_s)}.xlsx"
    
    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def members_per_branch_excel
    branch        = Branch.where(id: params[:branch_id]).first
    members       = Member.where(branch_id: branch.id).order("created_at asc")
    excel         = Members::GenerateMembersPerBranchExcel.new(members: members, branch: branch).execute!
    filename      = "members_#{branch.try(:to_s)}.xlsx"
    
    excel.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def members_spouse_export_csv
    branch        = Branch.where(id: params[:branch_id]).first
    members       = Member.where(branch_id: branch.id).order("created_at asc")
    
    send_data Members::GenerateMembersSpouseCsv.new(members: members).execute!, type: 'text/csv; charset=utf-8; header=present', disposition: 'attachment; filename=branch_member_spouse.csv'
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully export members spouse information.", email: current_user.email, username: current_user.username)
  end
end
