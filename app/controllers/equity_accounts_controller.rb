class EquityAccountsController < ApplicationController
  before_action :load_defaults, :authenticate_user!

  def load_defaults
    @members = Member.select("*")
    if current_user.role == "MIS"
      @branches = Branch.all
    else
      @branches = current_user.branches
    end
  end

  def index
    @equity_accounts = EquityAccount.active.includes(:member).order("members.last_name ASC")

    if params[:account_name].present?
      @account_name = params[:account_name]
      @equity_accounts = @equity_accounts.where("equity_accounts.account_number LIKE :q OR members.first_name LIKE :q OR members.last_name LIKE :q", q: "%#{@account_name}%")
    end

    if params[:equity_type_id].present?
      @equity_type = EquityType.find(params[:equity_type_id])
      @equity_accounts = @equity_accounts.where(equity_type_id: @equity_type.id)
    end

    if params[:branch_id].present?
      @branch = Branch.find(params[:branch_id])
      @equity_accounts = @equity_accounts.where(branch_id: @branch.id)
    end

    if params[:center_id].present?
      @center = Center.find(params[:center_id])
      @equity_accounts = @equity_accounts.where(center_id: @center.id)
    end

    @equity_accounts = @equity_accounts.page(params[:page]).per(20)
  end

  def new
    @equity_account = EquityAccount.new(balance: 0, particular: "Initial opening of account")
  end

  def create
    @equity_account = EquityAccount.new(equity_account_params)

    if @equity_account.save
      flash[:success] = "Successfully saved equity account record."
      redirect_to equity_account_path(@equity_account.id)
    else
      flash.now[:error] = "Error in saving equity account record."
      render :new
    end
  end

  def edit 
     @equity_account = EquityAccount.find(params[:id])
  end

  def update
    @equity_account = EquityAccount.find(params[:id])

    if @equity_account.update_attributes(equity_account_params)
      flash[:success] = "Successfully saved equity account record."
      redirect_to equity_account_path(@equity_account.id)
    else
      flash[:error] = "Error in saving equity account record."
      render :edit
    end
  end


  # TODO: Insert other validations here before destroying equity account
  def destroy
    @equity_account = EquityAccount.find(params[:id])

    if @equity_account.member.has_active_loans?
      flash[:error] = "Cannot destroy account. Member still has active loans"
      redirect_to equity_account_path(@equity_account)
    elsif @equity_account.member.has_pending_loans?
      flash[:error] = "Cannot destroy account. Member still has pending loans"
      redirect_to equity_account_path(@equity_account)
    else
      @equity_account.destroy!
      flash[:success] = "Successfully removed equity account"
      redirect_to equity_accounts_path
    end
  end

  # for api use
  def accounts
    equity_type_id = params[:equity_type_id]
    center_id = params[:center_id]

    equity_accounts = EquityAccount.where(center_id: center_id, equity_type_id: equity_type_id)

    data = []
    equity_accounts.each do |equity_account|
      s = { to_s: equity_account.member.to_s }
      sa = JSON::parse(equity_account.to_json).merge(s)
      data << sa
    end

    render json: data
  end

  def show
    @equity_account = EquityAccount.find(params[:id])
    @equity_account_transactions = EquityAccountTransaction.where("equity_account_id = ? AND amount > 0 AND status IN (?)", @equity_account.id, ["approved", "reversed"]).order("id ASC")

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @equity_account_transactions = @equity_account_transactions.where(transacted_at: @start_date..@end_date)
    end

    if params[:status].present?
      @status = params[:status]
      @equity_account_transactions = @equity_account_transactions.where(status: @status)
    end

    @equity_account_transactions = @equity_account_transactions.page(params[:page]).per(20)
  end


  def print_slip_deposit
      equity_account_transaction = EquityAccountTransaction.find(params[:id])

      render(
        pdf: "print_slip_deposit",
        template: "equity_accounts/print_slip_deposit",
        layout: false,
        page_size: 'Letter',
        locals: { equity_account_transaction: equity_account_transaction  }
      )
  end

  def print_slip_withdraw 
      equity_account_transaction = EquityAccountTransaction.find(params[:id])

      render(
        pdf: "print_slip_withdraw",
        template: "equity_accounts/print_slip_withdraw",
        layout: false,
        page_size: 'Letter',
        locals: { equity_account_transaction: equity_account_transaction  }
      )
  end
  
  def equity_accounts_pdf
    @equity_account = EquityAccount.find(params[:id])
    @equity_account_transactions = EquityAccountTransaction.where(
      "equity_account_id = ? AND status IN (?)", 
      @equity_account.id, 
      ["approved", "reversed"]
    ).order("transacted_at ASC")

  end

   
  def equity_account_params 
    params.require(:equity_account).permit!
  end
end
