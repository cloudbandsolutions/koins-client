class LoansController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults, :authenticate_user!
  before_action :validate_writeoff, only: [:writeoff]

  def loan_payment_breakdowns
    @loan = Loan.find(params[:loan_id])
   #raise @loan.inspect
      @data = Loans::LoanPaymentBreakdown.new(loan_id: params[:loan_id]).execute!
  end

  def validate_writeoff
    if !["MIS", "SBK"].include? current_user.role
      flash[:error] = "Cannot perform writeoff with your role"
      redirect_to loans_path
    end
  end

  def loan_ledger_pdf
    @loan           = Loan.find(params[:loan_id])
    #@loan_payments  = LoanPayment.where(loan_id: @loan.id, is_void: nil, status: "approved").order("paid_at ASC")
    @loan_payments  = ::Loans::LoanOperation.loan_payments(@loan)
    @member         = @loan.member
    @voucher        = Loans::ProduceVoucherForLoan.new(
                        loan: @loan, 
                        user: current_user
                      ).execute!
  end

  def loan_ledger
    loan            = Loan.find(params[:loan_id])
    member          = loan.member
    filename        = "loan-ledger-#{member.identification_number}-#{loan.loan_product.to_s}.xlsx"
    report_package  = Loans::GenerateExcelLoanLedger.new(
                        loan: loan, 
                        user_prepared_by: current_user
                      ).execute!

    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def index
    @loans = Loan.for_processing.includes(:member).order("members.last_name")
    @loans = @loans.page(params[:page]).per(20)
    @ref_nums = Loan.where.not(batch_transaction_reference_number: nil).pluck(:batch_transaction_reference_number).uniq
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying all loans for member #{@member.to_s}", email: current_user.email, username: current_user.username)
    
    if ["OAS", "BK"].include? current_user.role
      @loans = @loans.where(status: "pending")
    elsif ["SBK", "FM", "AM", "CM", "SO"].include? current_user.role
      @loans = @loans.where(status: ["active", "paid"])
    end

    if params[:q].present?
      @q = params[:q]
      @loans = @loans.where("loans.pn_number LIKE :q OR lower(members.first_name) LIKE :q OR lower(members.last_name) LIKE :q", q: "%#{@q.downcase}%")
    end

    if params[:member_type].present?
      @member_type = params[:member_type]
      @loans = @loans.where("members.member_type = ?", @member_type)
    end

    if params[:branch_id].present?
      @branch_id = params[:branch_id]
      @loans = @loans.where("members.branch_id = ?", @branch_id)
    end

    if params[:center_id].present?
      @center_id = params[:center_id]
      @loans = @loans.where("members.center_id = ?", @center_id)
    end

    if params[:loan_product_id].present?
      @loan_product_id = params[:loan_product_id]
      @loans = @loans.where(loan_product_id: @loan_product_id)
    end

    if params[:status].present?
      @status = params[:status]
      @loans = @loans.where(status: @status)
    end
  end

  def new_loan_application
    @member = Member.find(params[:member_id])
    @loan_product = LoanProduct.find(params[:loan_product_id])
    redirect_to new_member_loan_path(@member, loan_product_id: @loan_product.id)
  end

  def show 
    @loan = Loan.find(params[:id])

    if !["pending", "active", "paid", "deleted", "for-transfer", "transferred"].include?(@loan.status)
      flash[:error] = "Invalid status for loan"
      redirect_to loans_path
    end
    #@voucher = Loans::LoanVoucher.get_voucher(@loan, current_user)
    @voucher = Loans::ProduceVoucherForLoan.new(loan: @loan, user: current_user).execute!
  end

  def writeoff
    loan = Loan.find(params[:loan_id])

    if loan.active?
      Loans::WriteoffLoan.new(loan: loan, user: current_user).execute!
      flash[:success] = "Successfully writen off loan"
      redirect_to loan_path(loan.id)
    else
      flash[:error] = "Cannot writeoff non-active loan"
      redirect_to loan_path(loan.id)
    end
  end

  def print_check
    loan            = Loan.find(params[:loan_id])
    voucher         = Loans::ProduceVoucherForLoan.new(loan: loan, user: current_user).execute!
    filename        = "CHECK_#{loan.member.last_name}_#{loan.loan_product.to_s}_#{loan.voucher_date_requested.year}#{loan.voucher_date_requested.month.to_s.rjust(2, "0")}#{loan.voucher_date_requested.day.to_s.rjust(2, "0")}.xlsx"
    report_package  = Loans::CheckExcel.new(loan: loan, voucher: voucher).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def print_check_voucher
    loan            = Loan.find(params[:loan_id])
    voucher         = Loans::ProduceVoucherForLoan.new(loan: loan, user: current_user).execute!
    filename        = "CHECK_VOUCHER_#{loan.member.last_name}_#{loan.loan_product.to_s}_#{loan.voucher_date_requested.year}#{loan.voucher_date_requested.month.to_s.rjust(2, "0")}#{loan.voucher_date_requested.day.to_s.rjust(2, "0")}.xlsx"
    report_package  = Loans::CheckVoucherExcel.new(loan: loan, voucher: voucher).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def print_amortization
    loan = Loan.find(params[:loan_id])
    filename = "#{loan.member.last_name}_#{loan.loan_product.to_s}_amortization.xlsx"

    report_package = Loans::LoanAmortizationExcel.new(loan: loan).execute!

    report_package.serialize "#{Rails.root}/tmp/#{filename}"

    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def destroy
    loan = Loan.find(params[:id])

    if loan.loan_payments.where(status: "pending").size > 0
      flash[:error] = "Loan still has pending payments"
      redirect_to loan_path(loan)
    else
      loan.update(status: 'deleted', pn_number: "DELETED-#{Time.now}-#{loan.pn_number}")
      #loan.destroy!
      flash[:success] = "Successfully deleted loan"
      redirect_to loans_path
    end
  end

  def approve
    loan = Loan.find(params[:loan_id])

    if loan.first_date_of_payment.nil?
      flash[:error] = "Cannot approve unamortized loan"
      redirect_to loan_path(loan)
    else
      #Loan::Transaction.approve_loan!(loan, current_user)
      Loans::ApproveLoan.new(loan: loan, user: current_user).execute!
      flash[:success] = "Successfully approved loan"
      redirect_to loan_path(loan)
    end
  end

  def reverse
    loan = Loan.find(params[:loan_id])

    if !loan.no_payment_yet?
      flash[:error] = "Cannot reverse currently active loan"
      redirect_to loan_path(loan)
    else
      Loans::ReverseLoan.new(loan: loan).execute!
      flash[:success] = "Successfully reversed loan"
      redirect_to loan_path(loan)
    end
  end

  def load_defaults
    @members = Member.where("status != 'archived'").order(:last_name)
    @centers = Center.all
    @loan_products = LoanProduct.all

    if params[:action] == 'index'
      if params[:q].present?
        @q = params[:q]
      end

      if params[:member_type].present?
        @member_type = params[:member_type]
      end

      if params[:branch_id].present?
        @branch_id = params[:branch_id]
        @branch = Branch.find(@branch_id)
      end

      if params[:center_id].present?
        @center_id = params[:center_id]
        @center = Center.find(@center_id)
      end

      if params[:loan_product_id].present?
        @loan_product_id = params[:loan_product_id]
      end

      if params[:status].present?
        @status = params[:status]
      end
    end
  end

  def loan_params
    params.require(:loan).permit!
  end
end
