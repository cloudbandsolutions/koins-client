class AccountingsController < ApplicationController
  before_action :authenticate_user!

  def index
  end

  def general_ledger
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    @branch_id = params[:branch_id]

    @branch = Branch.find(@branch_id)

    @journal_entries = []

    Voucher.where(branch_id: @branch_id, status: "approved", date_prepared: @start_date..@end_date).order("date_prepared DESC").each do |v|
      v.journal_entries.each do |je|
        je.date_prepared = v.date_prepared
        je.particular = v.particular
        @journal_entries << je
      end
    end
  end

  def books
    @start_date = params[:start_date]
    @end_date = params[:end_date]
    @branch_id = params[:branch_id]
    @book = params[:book]

    @branch = Branch.find(@branch_id)

    @vouchers = Voucher.where(book: @book, branch_id: @branch_id, status: "approved", date_prepared: @start_date..@end_date).order("date_prepared DESC")
  end

  def trial_balance 

  end
end
