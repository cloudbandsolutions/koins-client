class LoanPaymentsController < ApplicationController
  before_action :load_defaults, :authenticate_user!

  def new
    if @loan.loan_payments.where(status: 'pending').count > 0
      flash[:error] = "There are still pending payments. Please approve before making another one."
      redirect_to loan_path(@loan)
    else
      @loan_payment = LoanPayment.new(cp_amount: 0.00, 
                        wp_amount: 0.00, 
                        loan_payment_amount: @loan.next_payment,
                        deposit_amount: 0.00)

      # Build savings account transactions
      deposit_amount = 0.00
      SavingsType.all.each do |savings_type|
        member = @loan.member

        savings_account = SavingsAccount.where(member_id: member.id, savings_type_id: savings_type.id).first

        if !savings_account.nil?
          @loan_payment.savings_account_transactions << SavingsAccountTransaction.new(
                                                          amount: 0.00,
                                                          transaction_type: "deposit",
                                                          particular: "deposit from loan payment",
                                                          savings_account: savings_account)
        end
      end

      # Build insurance deposit entries
      total_insurance_deposits = 0.00
      cycle_count = 0
      if !MemberLoanCycle.where(member_id: @loan.member.id, loan_product_id: @loan.loan_product.id).first.try(:cycle).nil?
        cycle_count = MemberLoanCycle.where(member_id: @loan.member.id, loan_product_id: @loan.loan_product.id).first.cycle
      end

      if cycle_count == 1
        @loan.loan_product.loan_insurance_deduction_entries.each do |loan_insurance_deduction_entry|
          insurance_account = InsuranceAccount.where(member_id: @loan.member.id, insurance_type_id: loan_insurance_deduction_entry.insurance_type.id).first
          if !insurance_account.nil?
            amount = loan_insurance_deduction_entry.val
            @loan_payment.loan_payment_insurance_account_deposits << LoanPaymentInsuranceAccountDeposit.new(
                                                                        amount: amount,
                                                                        insurance_account_id: insurance_account.id
                                                                      )

            total_insurance_deposits += amount
          end
        end
      end

      @loan_payment.cp_amount = total_insurance_deposits + @loan_payment.loan_payment_amount
      @loan_payment.particular = "Payment of Loan/Deposit of Funds - #{@loan.member} d/s #{Date.today.month.to_i}/#{Date.today.day.to_i} #{@loan_payment.cp_amount} + wp 0.00 = #{@loan_payment.cp_amount} Dep. = #{@loan_payment.cp_amount}" 
    end
  end

  def show
    @loan_payment = LoanPayment.find(params[:id])
  end

  def create
    @loan_payment = LoanPayment.new(loan_payment_params)
    @loan_payment.loan = @loan

    if @loan_payment.cp_amount.nil? && @loan_payment.wp_amount.nil?
      flash[:error] = "No Cash or Withdraw payment specified."
      render :new
    elsif @loan_payment.save
      flash[:success] = "Successfully saved loan payment. Please approve before posting."
      redirect_to loan_path(@loan)
    else
      #raise @loan_payment.errors.messages.inspect
      flash[:error] = "Cannot save loan payment."
      render :new
    end
  end

  def edit
    @loan_payment = LoanPayment.find(params[:id])
  end

  def update
    @loan_payment = LoanPayment.find(params[:id])

    if @loan_payment.status != 'pending'
      flash[:error] = "Cannot edit non-pending loan payment."
      redirect_to loan_path(@loan)
    end

    if @loan_payment.update(loan_payment_params)
      flash[:success] = "Successfully edited loan payment. Please approve before posting."
      redirect_to loan_path(@loan)
    else
      raise @loan_payment.wp_voucher_reference_number.inspect
      flash[:error] = "Error in updating loan payment."
      render :edit
    end
  end

  def destroy
    @loan_payment = LoanPayment.find(params[:id])

    if @loan_payment.status == 'approved'
      flash[:error] = "Cannot destroy approved loan payment."
      redirect_to loan_path(@loan)
    else
      @loan_payment.destroy!
      flash[:success] = "Successfully deleted loan payment."
      redirect_to loan_path(@loan)
    end
  end

  def approve_payment
    @loan_payment = LoanPayment.find(params[:loan_payment_id])

    if @loan_payment.status == "approved"
      flash[:error] = "Cannot approve already approved loan payment"
      redirect_to loan_loan_payment_path(@loan, @loan_payment)
    else
      ActiveRecord::Base.transaction do
        # Loans::LoanPaymentOperation.approve_loan_payment!(@loan_payment, current_user.full_name, current_user.full_name)
        Loans::ApproveLoanPayment.new(loan_payment: @loan_payment, approved_by: current_user.full_name, prepared_by: current_user.full_name).execute!
      end

      flash[:success] = "Successfully approved loan payment."
      redirect_to loan_loan_payment_path(@loan, @loan_payment)
    end
  end

  def reverse_payment
    @loan_payment = LoanPayment.find(params[:loan_payment_id])

    if @loan_payment.status == "reversed"
      flash[:error] = "Cannot reverse already reversed loan payment"
      redirect_to loan_loan_payment_path(@loan, @loan_payment)
    else
      # Loans::LoanPaymentOperation.reverse_loan_payment!(@loan_payment)
      # Loans::LoanPaymentOperation.generate_reversed_vouchers_for_loan_payment!(@loan_payment)
      Loans::ReverseLoanPayment.new(loan_payment: @loan_payment).execute!
      Loans::GenerateReversedVouchersForLoanPayment.new(loan_payment: @loan_payment).execute!

      flash[:success] = "Successfully reversed loan payment."
      redirect_to loan_loan_payment_path(@loan, @loan_payment)
    end
  end

  def load_defaults
    @loan = Loan.find(params[:loan_id])
    @savings_accounts = SavingsAccount.where("member_id IN (?)", [@loan.member.id, @loan.co_maker_one.try(:id)])
  end

  def loan_payment_params
    params.require(:loan_payment).permit!
  end
end
