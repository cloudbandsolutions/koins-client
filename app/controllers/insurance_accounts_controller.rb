class InsuranceAccountsController < ApplicationController
  before_action :load_defaults, :authenticate_user!

  def load_defaults
    @members = Member.select("*")
    if current_user.role == "MIS"
      @branches = Branch.all
    else
      @branches = current_user.branches
    end
  end

  def claims_copy_pdf
    @insurance_account = InsuranceAccount.find(params[:insurance_account_id])
    @member = @insurance_account.member
    @lif = InsuranceType.where(code: "LIF").first
    @lif_insurance_account = @member.insurance_accounts.where(insurance_type_id: @lif.id, member_id: @member.id).first
    @rf = InsuranceType.where(code: "RF").first
    @rf_insurance_account = @member.insurance_accounts.where(insurance_type_id: @rf.id, member_id: @member.id).first

    @payment_meta = Insurance::GenerateInsuranceAccountDetailsForLifAndRf.new(
                      member: @member, 
                      lif_insurance_account: @lif_insurance_account, 
                      rf_insurance_account: @rf_insurance_account
                    ).execute!
  end

  def index
    @insurance_accounts = InsuranceAccount.active.includes(:member).order("members.last_name ASC")

    if params[:account_name].present?
      @account_name = params[:account_name]
      @insurance_accounts = @insurance_accounts.where("insurance_accounts.account_number LIKE :q OR members.first_name LIKE :q OR members.last_name LIKE :q", q: "%#{@account_name}%")
    end

    if params[:insurance_type_id].present?
      @insurance_type = InsuranceType.find(params[:insurance_type_id])
      @insurance_accounts = @insurance_accounts.where(insurance_type_id: @insurance_type.id)
    end

    if params[:branch_id].present?
      @branch = Branch.find(params[:branch_id])
      @insurance_accounts = @insurance_accounts.where(branch_id: @branch.id)
    end

    if params[:center_id].present?
      @center = Center.find(params[:center_id])
      @insurance_accounts = @insurance_accounts.where(center_id: @center.id)
    end

    @insurance_accounts = @insurance_accounts.page(params[:page]).per(20)
  end

  def new
    @insurance_account = InsuranceAccount.new(balance: 0, particular: "Initial opening of account")
  end

  def create
    @insurance_account = InsuranceAccount.new(insurance_account_params)

    if @insurance_account.save
      flash[:success] = "Successfully saved insurance account record."
      redirect_to insurance_account_path(@insurance_account.id)
    else
      flash.now[:error] = "Error in saving insurance account record."
      render :new
    end
  end

  def edit 
     @insurance_account = InsuranceAccount.find(params[:id])
  end

  def update
    @insurance_account = InsuranceAccount.find(params[:id])

    if @insurance_account.update_attributes(insurance_account_params)
      flash[:success] = "Successfully saved insurance account record."
      redirect_to insurance_account_path(@insurance_account.id)
    else
      flash[:error] = "Error in saving insurance account record."
      render :edit
    end
  end


  # TODO: Insert other validations here before destroying insurance account
  def destroy
    @insurance_account = InsuranceAccount.find(params[:id])

    if @insurance_account.member.has_active_loans?
      flash[:error] = "Cannot destroy account. Member still has active loans"
      redirect_to insurance_account_path(@insurance_account)
    elsif @insurance_account.member.has_pending_loans?
      flash[:error] = "Cannot destroy account. Member still has pending loans"
      redirect_to insurance_account_path(@insurance_account)
    else
      @insurance_account.destroy!
      flash[:success] = "Successfully removed insurance account"
      redirect_to insurance_accounts_path
    end
  end

  # for api use
  def accounts
    insurance_type_id = params[:insurance_type_id]
    center_id = params[:center_id]

    insurance_accounts = InsuranceAccount.where(center_id: center_id, insurance_type_id: insurance_type_id)

    data = []
    insurance_accounts.each do |insurance_account|
      s = { to_s: insurance_account.member.to_s }
      sa = JSON::parse(insurance_account.to_json).merge(s)
      data << sa
    end

    render json: data
  end

  def show
    @insurance_account = InsuranceAccount.find(params[:id])
    @insurance_account_transactions = InsuranceAccountTransaction.where(
                                        "insurance_account_id = ? AND amount > 0 AND status IN (?)", 
                                        @insurance_account.id, ["approved", "reversed"]
                                      ).order("transacted_at ASC")

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @insurance_account_transactions = @insurance_account_transactions.where(transacted_at: @start_date..@end_date)
    end

    if params[:status].present?
      @status = params[:status]
      @insurance_account_transactions = @insurance_account_transactions.where(status: @status)
    end

    @insurance_account_transactions = @insurance_account_transactions.page(params[:page]).per(20)

    if @insurance_account.insurance_type.code == "LIF" || @insurance_account.insurance_type.code == "RF"
      @payment_meta = Insurance::GenerateInsuranceAccountStatus.new(insurance_account: @insurance_account).execute!
    end
  end

  def insurance_account_pdf
     
    @insurance_account = InsuranceAccount.find(params[:id])
    @insurance_account_transactions = InsuranceAccountTransaction.where(
                                        "insurance_account_id = ? AND amount > 0 AND status IN (?)", 
                                        @insurance_account.id, ["approved", "reversed"]
                                      ).order("transacted_at ASC")

  
    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @insurance_account_transactions = @insurance_account_transactions.where(transacted_at: @start_date..@end_date)
    end

    if params[:status].present?
      @status = params[:status]
      @insurance_account_transactions = @insurance_account_transactions.where(status: @status)
    end

    # @insurance_account_transactions = @insurance_account_transactions

    @payment_meta = Insurance::GenerateInsuranceAccountStatus.new(insurance_account: @insurance_account).execute!
 
  end


  def print_slip_deposit
      insurance_account_transaction = InsuranceAccountTransaction.find(params[:id])

      render(
        pdf: "print_slip_deposit",
        template: "insurance_accounts/print_slip_deposit",
        layout: false,
        page_size: 'Letter',
        locals: { insurance_account_transaction: insurance_account_transaction  }
      )
  end

  def print_slip_withdraw 
      insurance_account_transaction = InsuranceAccountTransaction.find(params[:id])

      render(
        pdf: "print_slip_withdraw",
        template: "insurance_accounts/print_slip_withdraw",
        layout: false,
        page_size: 'Letter',
        locals: { insurance_account_transaction: insurance_account_transaction  }
      )
  end

  def import_insurance_accounts
    #Member.import(params[:file])
    file = params[:file]
    orig_file_name = file.original_filename
    orig_file_name_no_ext = File.basename("#{orig_file_name}", ".*")
    file_name = orig_file_name_no_ext.delete! "insurance accounts"
    start_date_string = file_name.split("_").first
    end_date_string = file_name.split("_").last
    
    if !start_date_string.nil? && !end_date_string.nil?
      if start_date_string.include? "201"
        end_date = DateTime.parse(end_date_string).try(:to_date)
        start_date = DateTime.parse(start_date_string).try(:to_date)
      else
        end_date = nil
        start_date = nil
      end
    else  
      end_date = nil
      start_date = nil
    end

    @errors = []
    CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
      insurance_account = row.to_hash
      errors = InsuranceTransactions::ValidateInsuranceAccountsImportCsvFile.new(insurance_account: insurance_account).execute!
      errors.each do |e|
        @errors << e
      end
    end

    if @errors.size == 0
      InsuranceTransactions::LoadInsuranceAccountsFromCsvFile.new(file: file).execute!
      flash[:success] = "Successfully Import Insurance Accounts for Members."
      redirect_to members_path
      
      if !start_date.nil? && !end_date.nil?
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported insurance accounts for members. #{start_date.strftime("%b %d,%Y")} - #{end_date.strftime("%b %d,%Y")}", 
          email: current_user.email, 
          username: current_user.username
          )
      else
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported insurance accounts for members.", 
          email: current_user.email, 
          username: current_user.username
          )
      end
    else
      redirect_to import_insurance_accounts_path
      flash[:error] = "Error Importing Insurance Accounts for Members"
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Failed to import insurance accounts for members.", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:error] = @errors
    end  
  end

  def import_insurance_account_transactions
    file = params[:file]
    orig_file_name = file.original_filename
    orig_file_name_no_ext = File.basename("#{orig_file_name}", ".*")
    file_name = orig_file_name_no_ext.delete! "insurance account transactions"
    start_date_string = file_name.split("_").first
    end_date_string = file_name.split("_").last
    
    if !start_date_string.nil? && !end_date_string.nil?
      if start_date_string.include? "201"
        end_date = DateTime.parse(end_date_string).try(:to_date)
        start_date = DateTime.parse(start_date_string).try(:to_date)
      else
        end_date = nil
        start_date = nil
      end
    else  
      end_date = nil
      start_date = nil
    end

    @errors = []
    CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
      insurance_account_transaction = row.to_hash
      errors = InsuranceTransactions::ValidateInsuranceAccountTransactionsImportCsvFile.new(insurance_account_transaction: insurance_account_transaction).execute!
      errors.each do |e|
        @errors << e
      end
    end

    if @errors.size == 0

      file_path_to_save = "#{Rails.root}/tmp/#{Time.now.to_i}-insurance-transactions.csv"
      File.write(file_path_to_save, file.read)

      args = {
        file: file_path_to_save,
        user_full_name: current_user.full_name
      }

      ProcessImportInsuranceAccountTransaction.perform_later(args)
      # InsuranceTransactions::LoadInsuranceAccountTransactionsFromCsvFile.new(file: file).execute!
      flash[:success] = "Successfully Import Insurance Account Transactions For Members."
      
      if !start_date.nil? && !end_date.nil?
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported insurance account transactions for members. #{start_date.strftime("%b %d,%Y")} - #{end_date.strftime("%b %d,%Y")}", 
          email: current_user.email, 
          username: current_user.username
        )
      else
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Successfully imported insurance account transactions for members.", 
          email: current_user.email, 
          username: current_user.username
        )
      end

      redirect_to members_path
    else
      redirect_to import_insurance_account_transactions_path
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Failed to import insurance account transactions for members.", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:error] = "Error Importing Insurance Account Transactions For Members"
      flash[:error] = @errors
    end  
  end

  def import_insured_loans
    file = params[:file]
    @errors = []
    CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
      insured_loan = row.to_hash
      errors = InsuranceTransactions::ValidateInsuredLoansImportCsvFile.new(insured_loan: insured_loan).execute!
      errors.each do |e|
        @errors << e
      end
    end

    if @errors.size == 0
      InsuranceTransactions::LoadInsuredLoansFromCsvFile.new(file: file).execute!
      flash[:success] = "Successfully Import Insured Loans For Members."
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully imported insured loans for members.", 
        email: current_user.email, 
        username: current_user.username
      )

      redirect_to members_path
    else
      redirect_to import_insured_loans_path
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Failed to import insured loans for members.", 
        email: current_user.email, 
        username: current_user.username
      )

      flash[:error] = "Error Importing Insured loans For Members"
      flash[:error] = @errors
    end  
  end

  def insurance_account_params 
    params.require(:insurance_account).permit!
  end
end

