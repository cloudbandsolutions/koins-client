class AnnouncementsController < ApplicationController
  require 'application_helper'
  before_action :authenticate_user!
  before_action :check_edit_privileges!, only: [:edit, :new, :create, :update]

  def check_edit_privileges!
    if !["MIS", "SBK", "AM", "CM", "FM"].include? current_user.role
      flash[:error] = "Unauthorized"
      redirect_to announcements_path
    end
  end

  def index
    @announcements  = Announcement.select("*").order("announced_on DESC")
  end

  def new
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Trying to create new announcement.", 
      email: current_user.email, 
      username: current_user.username
    )
    @announcement = Announcement.new
  end

  def create
    @announcement = Announcement.new(announcement_params)
    @announcement.announced_on  = ApplicationHelper.current_working_date
    @announcement.announced_by  = "#{current_user.full_name} (#{current_user.role})"

    if @announcement.save
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully saved new announcement.", 
        email: current_user.email, 
        username: current_user.username
      )
      flash[:success] = "Successfully saved announcement transaction."

      redirect_to announcement_path(@announcement)
    else
      flash[:error] = "Error in saving announcement. #{@announcement.errors.full_messages.to_sentence}"
      render :new
    end
  end

  def edit
    @announcement = Announcement.find(params[:id])
  end

  def update
    @announcement = Announcement.find(params[:id])

    if @announcement.update(announcement_params)
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Successfully updated announcement", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @announcement.id
      )
      flash[:success] = "Successfully saved announcement transaction."
      redirect_to announcement_path(@announcement)
    else
      flash[:error] = "Error in saving announcement. #{@announcement.errors.full_messages.to_sentence}"
      render :edit
    end
  end

  def show
    @announcement = Announcement.find(params[:id])
  end

  def destroy
    @announcement = Announcement.find(params[:id])
    @announcement.destroy!
    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Successfully destroyed announcement #{params[:id]}.", 
      email: current_user.email, 
      username: current_user.username
    )
    flash[:success] = "Successfully destroyed announcement!"
    redirect_to announcements_path
  end

  private

  def announcement_params
    params.require(:announcement).permit!
  end
end
