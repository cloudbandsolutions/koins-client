class MemberTransferRequestsController < ApplicationController
  before_action :authenticate_user!

  def download_transferred
    filename  = "#{Branch.first.to_s}_for_transfer.json"
    path      = "#{Rails.root}/tmp"
    data      = ::Transfer::BuildTransferredMembers.new.execute!
    file      = ::Utils::WriteToJsonFile.new(path: path, filename: filename, data: data).execute!

    send_file file, filename: filename, type: "text/json"
  end

  def index
    @member_transfer_requests = MemberTransferRequest.select("*")
    @pending_data             = ::Transfer::BuildPendingTransferDetails.new.execute!
  end

  def edit
    @member_transfer_request  = MemberTransferRequest.find(params[:id])
  end

  def update
    @member_transfer_request  = MemberTransferRequest.find(params[:id])

    if @member_transfer_request.update(member_transfer_request_params)
      flash[:success] = "Successfully updated member transfer request"
      redirect_to member_transfer_request_path(@member_transfer_request)
    else
      flash[:errors]  = "#{@member_transfer_request.errors.messages}"
      render :edit
    end
  end

  def show
    @member_transfer_request  = MemberTransferRequest.find(params[:id])
    @voucher                  = ::Transfer::ProduceAccountingEntry.new(
                                  member_transfer_request: @member_transfer_request,
                                  user: current_user
                                ).execute!
  end

  private
  
  def member_transfer_request_params
    params.require(:member_transfer_request).permit!
  end
end
