module Members
  class ClaimsController < ApplicationController
    before_action :authenticate_user!
    before_action :load_defaults
    before_action :check_edit_privileges!, only: [:new, :create, :edit, :update]

    def check_edit_privileges!
      if !["MIS", "OAS", "BK"].include? current_user.role
        flash[:error] = "Unauthorized"
        redirect_to claims_path
      end
    end

    def load_defaults
      @member = Member.find(params[:member_id])
    end

    def index
      @claims = Claim.all
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Displaying all claims for member #{@member.to_s}", 
        email: current_user.email, 
        username: current_user.username
      )
    end

    def new
      @claim = Claim.new(member: @member)
      UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Initializing new claim for member #{@member.to_s}", 
          email: current_user.email, 
          username: current_user.username
      )
    end

    def create
      @claim = Claim.new(claim_params)
      @claim.member = @member
      @errors = []
      @errors = Claims::ValidateClaimDuplication.new(claim: @claim).execute!

      if @errors.count <= 0
        if @claim.save
          flash[:success] = "Successfully created claim"
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully saved new claim for member #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @claim.id)
          redirect_to claim_path(@claim)
        else
          flash[:error] = "Error in creating claim"
          render :new
        end
      else
        flash[:error] = "Error in creating claim : #{@errors}"
        render :new
      end
    end

    def edit
      @claim = Claim.find(params[:id])
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to edit claim for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @claim.id)
    end

    def show
      @claim = Claim.find(params[:id])
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying claim for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @claim.id)
    end

    def update
      @claim = Claim.find(params[:id])
      @claim.member = @member
      UserActivity.create!(
        user_id: current_user.id, role: current_user.role, 
        content: "Trying to update claim for #{@member.to_s}.", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @claim.id
      )

      if @claim.update(claim_params)
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated claim for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @claim.id)

        # update the remaining balance
        flash[:success] = "Successfully updated claim"
        redirect_to claim_path(@claim)
      else
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update claim for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @claim.id)
        flash[:error] = "Error in saving claim"
        render :new
      end
    end

    def claim_params
      params.require(:claim).permit!
    end

  end
end
