module Members
  class MembershipPaymentsController < ApplicationController
    before_action :authenticate_user!
    before_action :load_defaults, except: [:show, :edit, :update]

    def show
      @member = Member.find(params[:member_id])
      @membership_payment = MembershipPayment.find(params[:id])
    end

    def load_defaults
      @member = Member.find(params[:member_id])

      if params[:membership_type_id].blank?
        flash[:error] = "No membership type specified"
        redirect_to member_path(@member)
      else
        @membership_type = MembershipType.find(params[:membership_type_id])
        if MembershipPayment.where(member_id: @member.id, membership_type_id: @membership_type.id, status: "paid").count > 0
          flash[:error] = "Member is already part of #{@membership_type}"
          redirect_to member_path(@member)
        end
      end
    end

    def approve
      @membership_payment = MembershipPayment.find(params[:id])
      @membership_payment.update!(status: 'paid')
      @membership_payment.membership_account_payments.where(amount: 0.00).destroy_all
      @membership_payment.membership_account_payments.each do |membership_account_payment|
      end
    end

    def new
      @membership_payment = MembershipPayment.new(amount_paid: @membership_type.fee)
      SavingsType.all.each do |savings_type|
        @membership_payment.membership_account_payments << MembershipAccountPayment.new(amount: 0.00, deposit_type: "savings", account_type: savings_type.code)
      end

      InsuranceType.all.each do |insurance_type|
        @membership_payment.membership_account_payments << MembershipAccountPayment.new(amount: 0.00, deposit_type: "insurance", account_type: insurance_type.code)
      end

      EquityType.all.each do |equity_type|
        @membership_payment.membership_account_payments << MembershipAccountPayment.new(amount: 0.00, deposit_type: "equity", account_type: equity_type.code)
      end
    end

    def create
      @membership_payment = MembershipPayment.new(membership_payment_params)
      @membership_payment.member = @member
      @membership_payment.membership_type = @membership_type

      if @membership_payment.save
        flash[:success] = "Successfully saved payment for membership #{@membership_type}"
        redirect_to member_path(@member)
      else
        flash[:error] = "Error in paying for membership #{@membership_type}"
        render :new
      end
    end

    def edit
      @membership_payment = MembershipPayment.find(params[:id])
      @member = Member.find(params[:member_id])
    end

    def update
      @membership_payment = MembershipPayment.find(params[:id])
      @member = Member.find(params[:member_id])

      if @membership_payment.update(membership_payment_params)
        flash[:success] = "Successfully saved payment for membership #{@membership_type}"
        redirect_to member_path(@member)
      else
        flash[:error] = "Error in paying for membership #{@membership_type}"
        render :new
      end
    end

    def membership_payment_params
      params.require(:membership_payment).permit!
    end
  end
end
