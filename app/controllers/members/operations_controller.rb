module Members
  class OperationsController < ApiController
    before_action :authenticate_user!

    def resign
      member = Member.find(params[:member_id])

      errors = Members::ValidateMemberForResignation.new(member: member).execute!

      if errors.size > 0
        render json: { errors: errors }, status: 401
      else
        Members::ResignMember.new(member: member).execute!
        render json: { message: "ok" }
      end
    end
  end
end
