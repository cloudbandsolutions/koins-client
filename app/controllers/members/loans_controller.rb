module Members
  class LoansController < ApplicationController
    before_action :authenticate_user!
    before_action :load_defaults
    before_action :check_edit_privileges!, only: [:new, :create, :edit, :update]

    def check_edit_privileges!
      if !["MIS", "OAS", "BK"].include? current_user.role
        flash[:error] = "Unauthorized"
        redirect_to loans_path
      end
    end

    def load_defaults
      @member = Member.find(params[:member_id])
      @members = @member.possible_co_makers
      @project_type_categories = ProjectTypeCategory.select("*")
      @project_types = []
    end

    def index
      @loan_products = LoanProduct.all
      @loans = Loan.all
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Displaying all loans for member #{@member.to_s}", 
        email: current_user.email, 
        username: current_user.username
      )
    end

    def new

      if params[:loan_product_id].blank?
        flash[:error] = "No loan product specified"

        redirect_to loans_path
      else
        @loan_product = LoanProduct.find(params[:loan_product_id])
        @loan = Loans::InitNewLoan.new(loan_product: @loan_product, member: @member).execute!
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Initializing new loan for member #{@member.to_s}", 
          email: current_user.email, 
          username: current_user.username
        )

        if Loans::ValidateNewNonEntryPointLoanApplication.new(member: @member, loan_product: @loan_product).execute! == false

          flash[:error] = "Member still has active/pending entry point loans"
          redirect_to loans_path
        end

        if Loans::ValidateSimilarLoanProductLoanApplication.new(member: @member, loan_product: @loan_product).execute! == false

          flash[:error] = "Member still has pending loan application with loan product #{@loan_product}"
          redirect_to loans_path
        end

        if Loans::ValidateNewLoanApplicationWithActiveLoans.new(member: @member, loan_product: @loan_product).execute! == false

          flash[:error] = "Member still has active loan application with loan product #{@loan_product}"
          redirect_to loans_path
        end
      
        c = MemberLoanCycle.where(member_id: @member.id, loan_product_id: @loan_product.id).last

        if c.nil?
          @loan.loan_cycle_count = 1
        else
          @loan.loan_cycle_count = c.cycle + 1
        end

        @members = @member.possible_co_makers

        if !params[:loan_product_id].blank?
          @loan_product = LoanProduct.find(params[:loan_product_id])
        end

        @loan.num_installments = 25
        @loan.original_num_installments = 25
      end
    end

    def create
      @loan = Loan.new(loan_params)
      @loan.member = @member
      @loan_product = @loan.loan_product
      @loan.amount = 0.00 if @loan.amount.nil?

      if !@loan.project_type_category.nil?
        @project_types = @loan.project_type_category.project_types
      else
        @project_types = []
      end

      if Loans::ValidateNewLoanApplicationWithEpCycleCountMaxAmounts.new(member: @member, loan: @loan).execute! == false
        flash[:error] = "Member cannot avail loan #{@loan_product} due to cycle count and max amount"
        render :new
      elsif !@loan_product.max_loan_amount.nil? and @loan_product.max_loan_amount != 0.00 and @loan.amount > @loan_product.max_loan_amount
        flash[:error] = "Loan amount cannot exceed P#{@loan_product.max_loan_amount}"
        render :new
      elsif @loan.save
        flash[:success] = "Successfully created loan"
        redirect_to loan_path(@loan)
      else
        flash[:error] = "Error in creating loan"
        render :new
      end
    end

    def edit
      @loan = Loan.find(params[:id])
      @loan_product = @loan.loan_product
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to edit loan #{@loan.pn_number} for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @loan.id)

      if @loan.paid?
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Cannot edit loan #{@loan.pn_number} for #{@member.to_s}. Loan is already paid", email: current_user.email, username: current_user.username, record_reference_id: @loan.id)
        flash[:error] = "Cannot edit completed loan"
        redirect_to loan_path(@loan)
      end
    end

    def show
      @loan = Loan.find(params[:id])
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying loan #{@loan.pn_number} for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @loan.id)
    end

    def update
      @loan = Loan.find(params[:id])
      @loan.member = @member
      @loan_product = @loan.loan_product
      UserActivity.create!(
        user_id: current_user.id, role: current_user.role, 
        content: "Trying to update loan #{@loan.pn_number} for #{@member.to_s}.", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @loan.id
      )

      if !@loan.project_type_category.nil?
        @project_types = @loan.project_type_category.project_types
      else
        @project_types = []
      end

      if @loan.update(loan_params)
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated loan #{@loan.pn_number} for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @loan.id)

        # Reschedule amortization if loan is not active
        if !@loan.active?
          @loan.initialize_amortization_schedule!
        end

        # update the remaining balance
        @loan.update_remaining_balance!
        flash[:success] = "Successfully updated loan"
        redirect_to loan_path(@loan)
      else
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update loan #{@loan.pn_number} for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @loan.id)
        flash[:error] = "Error in saving loan"
        render :new
      end
    end

    def print_check
      @loan = Loan.find(params[:loan_id])
      if @loan.pending?
        flash[:error] = "Cannot print check for pending loan. Please approve first"
        redirect_to loan_path(@loan)
      else
        file = Loans::GenerateLoanCheck.new(loan: @loan).execute!
        send_data file.to_stream.read, type: "application/xlsx", filename: "#{@loan}_check.xlsx"
      end
    end

    def print_voucher
      @loan = Loan.find(params[:loan_id])
      if @loan.pending?
        flash[:error] = "Cannot print voucher for pending loan. Please approve first"
        redirect_to loan_path(@loan)
      else
        @voucher = Voucher.where(reference_number: @loan.voucher_reference_number).first
        file = Loans::GenerateLoanVoucher.new(loan: @loan, voucher: @voucher).execute!
        send_data file.to_stream.read, type: "application/xlsx", filename: "#{@loan}_check.xlsx"
      end
    end

    def loan_params
      params.require(:loan).permit!
    end

  end
end
