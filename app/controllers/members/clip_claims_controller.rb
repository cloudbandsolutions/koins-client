module Members
  class ClipClaimsController < ApplicationController
    before_action :authenticate_user!
    before_action :load_defaults
    before_action :check_edit_privileges!, only: [:new, :create, :edit, :update]

    def check_edit_privileges!
      if !["MIS", "OAS", "BK"].include? current_user.role
        flash[:error] = "Unauthorized"
        redirect_to clip_claims_path
      end
    end

    def load_defaults
      @member = Member.find(params[:member_id])
    end

    def index
      @clip_claims = ClipClaim.all
      UserActivity.create!(
        user_id: current_user.id, 
        role: current_user.role, 
        content: "Displaying all clip claims for member #{@member.to_s}", 
        email: current_user.email, 
        username: current_user.username
      )
    end

    def new
      @clip_claim = ClipClaim.new(member: @member)
      UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "Initializing new clip claim for member #{@member.to_s}", 
          email: current_user.email, 
          username: current_user.username
      )
    end

    def create
      @clip_claim = ClipClaim.new(clip_claim_params)
      @clip_claim.member = @member
      @errors = []
      @errors = Claims::ValidateClipClaimDuplication.new(clip_claim: @clip_claim).execute!

      if @errors.count <= 0
        if @clip_claim.save
          flash[:success] = "Successfully created clip claim"
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully saved new clip claim for member #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @clip_claim.id)
          redirect_to clip_claim_path(@clip_claim)
        else
          flash[:error] = "Error in creating claim"
          render :new
        end
      else
        flash[:error] = "Error in creating clip claim : #{@errors}"
        render :new
      end
    end

    def edit
      @clip_claim = ClipClaim.find(params[:id])
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to edit clip claim for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @clip_claim.id)
    end

    def show
      @clip_claim = ClipClaim.find(params[:id])
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying clip claim for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @clip_claim.id)
    end

    def update
      @clip_claim = ClipClaim.find(params[:id])
      @clip_claim.member = @member
      UserActivity.create!(
        user_id: current_user.id, role: current_user.role, 
        content: "Trying to update clip claim for #{@member.to_s}.", 
        email: current_user.email, 
        username: current_user.username, 
        record_reference_id: @clip_claim.id
      )

      if @clip_claim.update(clip_claim_params)
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated clip claim for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @clip_claim.id)

        # update the remaining balance
        flash[:success] = "Successfully updated claim"
        redirect_to clip_claim_path(@clip_claim)
      else
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update clip claim for #{@member.to_s}.", email: current_user.email, username: current_user.username, record_reference_id: @clip_claim.id)
        flash[:error] = "Error in saving clip claim"
        render :new
      end
    end

    def clip_claim_params
      params.require(:clip_claim).permit!
    end

  end
end
