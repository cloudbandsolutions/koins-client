class VouchersController < ApplicationController
  helper_method :sort_column, :sort_direction
  before_action :authenticate_user!
  before_action :load_defaults

  def load_defaults
    if params[:voucher_id].present?
      @voucher = Voucher.find(params[:voucher_id])
    end

    if current_user.role == "MIS"
      @branches = Branch.all
    else
      @branches = current_user.branches
    end
  end

  def print
    @voucher = Voucher.find(params[:id])
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Print accounting entry #{@voucher.book}", email: current_user.email, username: current_user.username, record_reference_id: @voucher.id)
    render pdf: "#{@voucher.master_reference_number}", layout: "pdf", orientation: 'Portrait', page_size: 'Letter', grayscale: false
  end

  def index
    @accounting_funds = AccountingFund.all
    @vouchers = Voucher.select("*").order("updated_at DESC")

    UserActivity.create!(
      user_id: current_user.id, 
      role: current_user.role, 
      content: "Viewed all accounting entries", 
      email: current_user.email, 
      username: current_user.username
    )

    if params[:book_type].present?
      @book_type = params[:book_type]
      @vouchers = @vouchers.where(book: @book_type)
    end

    if params[:status].present?
      @status = params[:status]
      @vouchers = @vouchers.where(status: @status)
    end

    if params[:q].present?
      @q = params[:q]
      @vouchers = @vouchers.where("id = ? OR reference_number LIKE ? OR or_number LIKE ?", @q, "%#{@q}%", "%#{@q}%")
    end

    if params[:start_date].present? and params[:end_date].present?
      @start_date = params[:start_date]
      @end_date = params[:end_date]

      @vouchers = @vouchers.where(date_prepared: @start_date..@end_date)
    end

    if params[:accounting_fund_id].present?
      @accounting_fund_id = params[:accounting_fund_id]
      @vouchers = @vouchers.where(accounting_fund_id: @accounting_fund_id)
    end

    if params[:branch_id].present?
      @branch_id = params[:branch_id]
      @branch = Branch.find(@branch_id)
      @vouchers = @vouchers.where(branch_id: @branch_id)
    end

    @vouchers = @vouchers.order(sort_column + " " + sort_direction).page(params[:page]).per(50)
  end

  def show
    @voucher = Voucher.find(params[:id])
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying accounting entry #{@voucher.book}", email: current_user.email, username: current_user.username, record_reference_id: @voucher.id)
  end

  def new
    @voucher = Voucher.new(branch: Branch.first)
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Trying to create new accounting entry", email: current_user.email, username: current_user.username)
  end

  def create
    @voucher = Voucher.new(voucher_params)
    @voucher.prepared_by = current_user.full_name

    if @voucher.save
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully saved new accounting entry", email: current_user.email, username: current_user.username)
      flash[:success] = "Successfully saved voucher"
      redirect_to voucher_path(@voucher.id)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to create new accounting entry.", email: current_user.email, username: current_user.username)
      flash[:error] = "Error in saving voucher"
      render :new
    end
  end

  def edit
    @voucher = Voucher.find(params[:id])
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "About to edit accounting entry", email: current_user.email, username: current_user.username, record_reference_id: @voucher.id)
  end

  def pdf
    @voucher = Voucher.find(params[:voucher_id])
  end

  def check_pdf
    @voucher = Voucher.find(params[:voucher_id])
  end

  def update
    @voucher = Voucher.find(params[:id])
    @voucher.prepared_by = current_user.full_name

    if @voucher.update(voucher_params)
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated accounting entry", email: current_user.email, username: current_user.username, record_reference_id: @voucher.id)
      flash[:success] = "Successfully saved voucher"
      redirect_to voucher_path(@voucher.id)
    else
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update accounting entry", email: current_user.email, username: current_user.username, record_reference_id: @voucher.id)
      flash[:error] = "Error in saving voucher"
      render :edit
    end
  end

  def destroy
    voucher = Voucher.find(params[:id])
    #voucher.update!(status: "rejected")
    voucher.destroy!
    UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully rejected accounting entry", email: current_user.email, username: current_user.username)
    flash[:success] = "Successfully rejected voucher"
    redirect_to vouchers_path
  end

  def approve
    ActiveRecord::Base.transaction do
      Vouchers::VoucherApproval.new(voucher: @voucher,approve_by: current_user.full_name).execute!
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully approved accounting entry", email: current_user.email, username: current_user.username)
      flash[:success] = "Successfully approved voucher"
      redirect_to voucher_path(@voucher)
    end
  end

  def voucher_params
    params.require(:voucher).permit!
  end

  private

  def sort_column
    Voucher.column_names.include?(params[:sort]) ? params[:sort] : "reference_number"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
end
