class MemberCertificatesController < ApplicationController
  before_action :authenticate_user!
  before_action :load_member, except: [:index, :certificates_for_printing,:certificates_printed_pdf]

  def index
    @member_certificates = MemberCertificate.joins(:member).order("member_certificates.date_issue ASC, members.status, member_certificates.date_printed,members.last_name ASC")

    if params[:q].present?
      @q  = params[:q]

      @member_certificates  = @member_certificates.where("member_certificates.certificate_number LIKE ?", "#{@q}%")
    end

    if params[:start_date_of_issue].present? and params[:end_date_of_issue].present?
      @start_date_of_issue = params[:start_date_of_issue]
      @end_date_of_issue   = params[:end_date_of_issue]

      @member_certificates  = @member_certificates.where("member_certificates.date_issue >= ? AND member_certificates.date_issue <= ?", @start_date_of_issue, @end_date_of_issue)
    end

    if params[:start_date_printed].present? and params[:end_date_printed].present?
      @start_date_printed = params[:start_date_printed]
      @end_date_printed   = params[:end_date_printed]

      @member_certificates  = @member_certificates.where("member_certificates.date_printed >= ? AND member_certificates.date_printed <= ?", @start_date_printed, @end_date_printed)
    end
    
    @limit = 100
    @counter = 0
    if params[:page].present?
      @counter = (params[:page].to_i - 1)  * @limit
    end
    @member_certificates  = @member_certificates.page(params[:page]).per(@limit)
  end


  def certificates_printed_pdf
    @member_certificated = MemberCertificate.joins(:member => :center).order("member_certificates.date_issue ASC, members.status, member_certificates.date_printed,members.last_name ASC")

    if params[:q].present?
      @q  = params[:q]

      @member_certificates  = @member_certificates.where("member_certificates.certificate_number LIKE ?", "#{@q}%")
    end

    if params[:start_date_of_issue].present? and params[:end_date_of_issue].present?
      @start_date_of_issue = params[:start_date_of_issue]
      @end_date_of_issue   = params[:end_date_of_issue]

      @member_certificated  = @member_certificated.where("member_certificates.date_issue >= ? AND member_certificates.date_issue <= ?", @start_date_of_issue, @end_date_of_issue)
    end

    if params[:start_date_printed].present? and params[:end_date_printed].present?
      @start_date_printed = params[:start_date_printed]
      @end_date_printed   = params[:end_date_printed]

      @member_certificated  = @member_certificated.where("member_certificates.date_printed >= ? AND member_certificates.date_printed <= ?", @start_date_printed, @end_date_printed)
    end
  end

  def load_member
    @member = Member.find(params[:member_id])
    if !["active", "resigned", "resign"].include?(@member.status)
      flash[:error] = "Cannot issue certificate to non-active member"
      redirect_to member_path(@member)
    end
  end

  def certificates_for_printing
    if params[:branch_id].present?
      branch_id = params[:branch_id]
      printed_member_certificates = MemberCertificate.where(is_printed: true , is_void: '')
      members = Member.where(branch_id: branch_id)
      printed_member_ids    = members.where(id: printed_member_certificates.pluck(:member_id))
      @members_for_printing = members.active.where.not(id: printed_member_ids).order("previous_mii_member_since ASC, last_name ASC")
      # @members_for_printing = Member.pure_active.where(branch_id: 41)
    end
  end

  def pdf
    @member_certificate = MemberCertificate.find(params[:member_certificate_id])
  end

  def excel
    @member_certificate = MemberCertificate.find(params[:member_certificate_id])
    filename = "certificate_#{@member_certificate.certificate_number}.xlsx"
    report_package  = Members::GenerateMemberCertificateCertificateExcel.new(member: @member, member_certificate: @member_certificate).execute!
    report_package.serialize "#{Rails.root}/tmp/#{filename}"
    send_file "#{Rails.root}/tmp/#{filename}", filename: "#{filename}", type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
  end

  def new
    @member_certificate = MemberCertificate.new(number_of_certificates: 1, date_issue: @member.previous_mii_member_since)
  end

  def create
    @member_certificate = MemberCertificate.new(member_certificate_params)
    @member_certificate.member = @member

    if @member_certificate.save
      flash[:success] = "Successfully saved member certificate"
      redirect_to member_member_certificate_path(@member, @member_certificate)
    else
      flash[:error] = "Cannot save member certificate"
      render :new
    end
  end

  def edit
    @member_certificate = MemberCertificate.find(params[:id])
  end

  def update
    @member_certificate = MemberCertificate.find(params[:id])

    if @member_certificate.update(member_certificate_params)
      flash[:success] = "Successfully saved member certificate"
      redirect_to member_member_certificate_path(@member, @member_certificate)
    else
      flash[:error] = "Cannot save member certificate"
      render :edit
    end
  end

  def destroy
    @member_certificate = MemberCertificate.find(params[:id])
    @member_certificate.destroy!
    flash[:success] = "Successfully deleted member certificate"
    redirect_to member_path(@member)
  end

  def show
    @member_certificate = MemberCertificate.find(params[:id])
  end

  def member_certificate_params
    params.require(:member_certificate).permit!
  end
end
