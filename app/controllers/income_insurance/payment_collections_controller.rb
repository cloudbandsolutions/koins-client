module IncomeInsurance
  class PaymentCollectionsController < ApplicationController
    before_action :authenticate_user!
    before_action :load_record, only: [:edit, :update, :destroy, :show]
    before_action :load_types

    def load_record
      @payment_collection = PaymentCollection.find(params[:id])
    end

    def load_types
      @insurance_types  = InsuranceType.where(code: "HIIP")
      @branches         = Branch.all
      @centers          = Center.all
    end 

    def index
      @payment_collections = PaymentCollection.income_insurance
      if !["MIS", "BK", "SBK"].include? current_user.role
        @payment_collections = @payment_collections.where(status: "pending")
      end

      if params[:or_number].present?
        @or_number = params[:or_number]
        @payment_collections = @payment_collections.where("or_number LIKE ?", "#{@or_number}%")
      end

      if params[:start_date].present?
        @start_date = params[:start_date]
        @payment_collections = @payment_collections.where("paid_at >= ?", @start_date)
      end

      if params[:end_date].present?
        @end_date = params[:end_date]
        @payment_collections = @payment_collections.where("paid_at <= ?", @end_date)
      end

      if params[:center_id].present?
        @center_id = params[:center_id]
        @payment_collections = @payment_collections.where(center_id: params[:center_id])
      end

      if params[:branch_id].present?
        @branch_id = params[:branch_id]
        @payment_collections = @payment_collections.where(branch_id: params[:branch_id])
      end

      if params[:t_status].present?
        @status = params[:t_status]
        @payment_collections=  @payment_collections.where(status: @status)
      end
    end

    def new
      if !params[:branch_id].present? or !params[:date_of_payment].present?
        flash[:error] = "No branch or center or date of payment defined"
        redirect_to income_insurance_payment_collections_path
      else
        branch = Branch.find(params[:branch_id])
        center = params[:center_id]
        date_of_payment = params[:date_of_payment]
        prepared_by = current_user.full_name.upcase

        @payment_collection = PaymentCollections::CreatePaymentCollectionForIncomeInsurance.new(branch: branch, center: center, paid_at: date_of_payment, prepared_by: prepared_by).execute!

        if @payment_collection.save
          redirect_to edit_income_insurance_payment_collection_path(@payment_collection)
        else
          flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
          redirect_to income_insurance_payment_collections_path
        end
      end
    end

    def edit
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Edit payment collection (Deposits)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
      if @payment_collection.or_number.include?("CHANGE-ME")
        @payment_collection.or_number = ""
      end
      @members = Member.pure_active.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id)).order("last_name ASC")
    end

    def update
      if @payment_collection.update(payment_collection_params)
        flash[:success] = "Successfully saved collection."
        redirect_to income_insurance_payment_collection_path(@payment_collection)
      else
        flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
        render :edit
      end
    end

    def show
      # @members = Member.where(branch_id: @payment_collection.branch.id, center_id: @payment_collection.center.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
      @members = Member.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
      @voucher = PaymentCollections::ProduceVoucherForIncomeInsurancePaymentCollection.new(payment_collection: @payment_collection).execute!
    end

    def pdf
      @payment_collection = PaymentCollection.find(params[:payment_collection_id])
      @insurance_types     = InsuranceType.where(code: "HIIP")
      @members = Member.where(branch_id: @payment_collection.branch.id, center_id: @payment_collection.center.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
      @voucher = PaymentCollections::ProduceVoucherForIncomeInsurancePaymentCollection.new(payment_collection: @payment_collection).execute!
    end

    def destroy
      if !@payment_collection.pending?
        flash[:error] = "Cannot destroy non pending record"
        redirect_to income_insurance_payment_collection_path(@payment_collection)
      else
        @payment_collection.destroy!
        flash[:success] = "Successfully destroyed payment collection."
        redirect_to income_insurance_payment_collections_path
      end
    end

    def upload
      file = params[:file]
      branch = Branch.find(params[:branch_id])
      center = Center.find(params[:center_id])
      paid_at = params[:paid_at]
      prepared_by = current_user.full_name.upcase
      @errors = []
      CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
        payment_collection = row.to_hash
        errors = PaymentCollections::ValidateIncomeInsuranceRemittanceCsvFile.new(payment_collection: payment_collection, branch: branch, center: center, paid_at: paid_at).execute!
        errors.each do |e|
          @errors << e
        end
      end

      if @errors.size == 0
        PaymentCollections::LoadIncomeInsuranceRemittanceFromCsvFile.new(file: file, branch: branch, center: center, paid_at: paid_at, prepared_by: prepared_by).execute!
        flash[:success] = "Successfully Import Income Insurance Record."
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully Import Income Insurance.", email: current_user.email, username: current_user.username)
        redirect_to income_insurance_payment_collections_path
      else
        redirect_to upload_income_insurance_path
        flash[:error] = @errors

      end  
    end

    def payment_collection_params
      params.require(:payment_collection).permit!
    end
  end
end
