module MembershipPayments
  class PaymentCollectionsController < ApplicationController
    before_action :authenticate_user!
    before_action :load_record, only: [:edit, :update, :destroy, :show]
    before_action :load_types

    def load_record
      @payment_collection = PaymentCollection.find(params[:id])
    end

    def load_types
      #@savings_types    = SavingsType.all
      @savings_types    = SavingsType.where(code: "PSA")
      @insurance_types  = InsuranceType.all
      @equity_types     = EquityType.all
      @branches         = Branch.all
      @centers          = []
      @membership_types = MembershipType.all
    end

    def pdf
      @payment_collection = PaymentCollection.find(params[:payment_collection_id])
      @savings_types       = SavingsType.all
      @insurance_types     = InsuranceType.all
      @equity_types        = EquityType.all
      @membership_types    = MembershipType.all
    end

    def index
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displayed all payment collections (Membership Payments)", email: current_user.email, username: current_user.username)
      @payment_collections = PaymentCollection.membership.page params[:page]

      if ["OAS", "BK"].include? current_user.role
        @payment_collections = @payment_collections.where(status: "pending")
      end

      if params[:or_number].present?
        @or_number = params[:or_number]
        @payment_collections = @payment_collections.where("or_number LIKE ?", "#{@or_number}%")
      end

      if params[:start_date].present?
        @start_date = params[:start_date]
        @payment_collections = @payment_collections.where("paid_at >= ?", @start_date)
      end

      if params[:end_date].present?
        @end_date = params[:end_date]
        @payment_collections = @payment_collections.where("paid_at <= ?", @end_date)
      end

      if params[:branch_id].present?
        @branch_id = params[:branch_id]
        @payment_collections = @payment_collections.where(branch_id: params[:branch_id])
      end

      if params[:t_status].present?
        @status = params[:t_status]
        @payment_collections = @payment_collections.where(status: @status)
      end
    end

    def new
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "New payment collection (Membership Payments)", email: current_user.email, username: current_user.username)
      if !params[:branch_id].present? or !params[:date_of_payment].present?
        flash[:error] = "No branch or date of payment defined"
        redirect_to membership_payments_payment_collections_path
      else
        branch = Branch.find(params[:branch_id])
        date_of_payment = params[:date_of_payment]
        prepared_by = current_user.full_name.upcase

        members = []
        if params[:member_id].present?
          members << Member.find(params[:member_id])
        end

        @payment_collection = PaymentCollections::CreatePaymentCollectionForMembershipPayment.new(branch: branch, paid_at: date_of_payment, prepared_by: prepared_by, members: members).execute!

        if @payment_collection.save
          redirect_to edit_membership_payments_payment_collection_path(@payment_collection)
        else
          flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
          redirect_to membership_payments_payment_collections_path
        end
      end
    end

    def edit
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Edit payment collection (Membership Payments)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
      #@members = Member.pending.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id)).order("last_name ASC")
      @members = Member.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id)).order("last_name ASC")
      if @payment_collection.or_number.include?("CHANGE-ME")
        @payment_collection.or_number = ""
      end
    end

    def update
      #@members = Member.pending.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
      @members = Member.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
      if @payment_collection.update(payment_collection_params)
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated payment collection (Membership Payments)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
        flash[:success] = "Successfully saved collection."
        redirect_to membership_payments_payment_collection_path(@payment_collection)
      else
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update payment collection (Membership Payments)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
        flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
        render :edit
      end
    end

    def show
      UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying payment collection (Membership Payments)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
      @members = Member.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
      @voucher = PaymentCollections::ProduceVoucherForMembershipPaymentPaymentCollection.new(payment_collection: @payment_collection).execute!
    end

    def destroy
      if !@payment_collection.pending?
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to destroy payment collection (Membership Payments)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
        flash[:error] = "Cannot destroy non pending record"
        redirect_to membership_payments_payment_collection_path(@payment_collection)
      else
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully destroyed payment collection (Membership Payments)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
        @payment_collection.destroy!
        flash[:success] = "Successfully destroyed payment collection."
        redirect_to membership_payments_payment_collections_path
      end
    end

    def payment_collection_params
      params.require(:payment_collection).permit!
    end
  end
end
