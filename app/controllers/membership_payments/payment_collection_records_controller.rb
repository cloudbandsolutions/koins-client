module MembershipPayments
  class PaymentCollectionRecordsController < ApplicationController
    before_action :authenticate_user!
    before_action :load_payment_collection
    before_action :load_types

    def load_payment_collection
      @payment_collection = PaymentCollection.find(params[:payment_collection_id])
    end

    def load_types
      @savings_types    = SavingsType.all
      @insurance_types  = InsuranceType.all
      @equity_types     = EquityType.all
      @branches         = Branch.all
      @centers          = Center.all
      @membership_types = MembershipType.all
    end

    def destroy
      @payment_collection_record = PaymentCollectionRecord.find(params[:id])
      @payment_collection_record.destroy!
      @payment_collection.update!(updated_at: Time.now)
      flash[:success] = "Successfully destroyed record"
      redirect_to membership_payments_payment_collection_path(@payment_collection)
    end

    def payment_collection_record_params
      params.reequire(:payment_collection_record).permit!
    end
  end
end
