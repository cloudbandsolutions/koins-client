module CashManagement
  module Deposits
    class InterestAndTaxClosingRecordsController < ApplicationController
      before_action :authenticate_user!
      before_action :load_types

      def load_types
        @savings_types    = SavingsType.all
        @branches         = Branch.all
        @centers          = Center.all
      end
 
      def index
      end

      def show
      end
    end
  end
end

