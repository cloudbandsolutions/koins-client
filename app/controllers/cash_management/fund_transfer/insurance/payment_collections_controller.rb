module CashManagement
  module FundTransfer
    module Insurance
      class PaymentCollectionsController < ApplicationController
        before_action :authenticate_user!
        before_action :load_record, only: [:edit, :update, :destroy, :show]
        before_action :load_types

        def load_record
          @payment_collection = PaymentCollection.find(params[:id])
        end

        def load_types
          @insurance_types  = InsuranceType.all
          @branches         = Branch.all
          @centers          = Center.all
        end

        def pdf
          @payment_collection = PaymentCollection.find(params[:payment_collection_id])
          @insurance_types     = InsuranceType.all
        end

        def index
          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Displayed all payment collections (Insurance Fund Transfer)", 
            email: current_user.email, 
            username: current_user.username
          )
          @payment_collections = PaymentCollection.insurance_fund_transfer.order("created_at DESC").page params[:page]

          if ["OAS", "BK"].include? current_user.role
            @payment_collections = @payment_collections.where(status: "pending")
          end

          if params[:start_date].present?
            @start_date = params[:start_date]
            @payment_collections = @payment_collections.where("paid_at >= ?", @start_date)
          end

          if params[:end_date].present?
            @end_date = params[:end_date]
            @payment_collections = @payment_collections.where("paid_at <= ?", @end_date)
          end

          if params[:branch_id].present?
            @branch_id = params[:branch_id]
            @payment_collections = @payment_collections.where(branch_id: params[:branch_id])
          end

          if params[:status].present?
            @status = params[:status]
            @payment_collections=  @payment_collections.where(status: @status)
          end
        end

        def new
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "New payment collection (Insurance Fund Transfer)", email: current_user.email, username: current_user.username)
          if !params[:branch_id].present? or !params[:center_id].present? or !params[:date_of_payment].present?
            flash[:error] = "No branch or center or date of payment defined"
            redirect_to cash_management_fund_transfer_insurance_payment_collections_path
          else
            branch = Branch.find(params[:branch_id])
            center = Center.find(params[:center_id])
            date_of_payment = params[:date_of_payment]
            prepared_by = current_user.full_name.upcase

            members = []
            if params[:member_id].present?
              members << Member.find(params[:member_id])
            end

            @payment_collection = PaymentCollections::CreatePaymentCollectionForInsuranceFundTransfer.new(branch: branch, center: center, paid_at: date_of_payment, prepared_by: prepared_by, members: members).execute!

            if @payment_collection.save
              redirect_to cash_management_fund_transfer_insurance_payment_collection_path(@payment_collection)
            else
              flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
              redirect_to cash_management_fund_transfer_insurance_payment_collections_path
            end
          end
        end

        def edit
          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Edit payment collection (Insurance Fund Transfer)", 
            email: current_user.email, 
            username: current_user.username, 
            record_reference_id: @payment_collection.id
          )

          @members = Member.pure_active.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id)).order("last_name ASC")
          if @payment_collection.or_number.include?("CHANGE-ME")
            @payment_collection.or_number = ""
          end
        end

        def update
          @members = Member.pure_active.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
          if @payment_collection.update(payment_collection_params)
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully updated payment collection (Insurance Fund Transfer)", 
              email: current_user.email, 
              username: current_user.username, 
              record_reference_id: @payment_collection.id
            )

            flash[:success] = "Successfully saved collection."
            redirect_to cash_management_fund_transfer_insurance_payment_collection_path(@payment_collection)
          else
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Failed to update payment collection (Insurance Fund Transfer)", 
              email: current_user.email, 
              username: current_user.username, 
              record_reference_id: @payment_collection.id
            )

            flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
            render :edit
          end
        end

        def show
          UserActivity.create!(
            user_id: current_user.id, 
            role: current_user.role, 
            content: "Displaying payment collection (Insurance Fund Transfer)", 
            email: current_user.email, 
            username: current_user.username, 
            record_reference_id: @payment_collection.id
          )

          @members = Member.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
        end

        def destroy
          if !@payment_collection.pending?
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Failed to destroy payment collection (Insurance Fund Transfer)", 
              email: current_user.email, 
              username: current_user.username, 
              record_reference_id: @payment_collection.id
            )

            flash[:error] = "Cannot destroy non pending record"
            redirect_to cash_management_fund_transfer_insurance_payment_collection_path(@payment_collection)
          else
            UserActivity.create!(
              user_id: current_user.id, 
              role: current_user.role, 
              content: "Successfully destroyed payment collection (Insurance Fund Transfer)", 
              email: current_user.email, 
              username: current_user.username, 
              record_reference_id: @payment_collection.id
            )
            @payment_collection.destroy!
            flash[:success] = "Successfully destroyed payment collection."
            redirect_to cash_management_fund_transfer_insurance_payment_collections_path
          end
        end

        def upload
          file = params[:file]
          branch = Branch.find(params[:branch_id])
          paid_at = params[:paid_at]
          prepared_by = current_user.full_name.upcase
          @errors = []
          CSV.foreach(file.path, {:headers => true, :encoding => 'windows-1251:utf-8'}) do |row|
            payment_collection = row.to_hash
            errors = PaymentCollections::ValidateInsuranceFundTransferFromCsvFile.new(payment_collection: payment_collection, branch: branch, paid_at: paid_at).execute!
            errors.each do |e|
              @errors << e
            end
          end

          if @errors.size == 0
            PaymentCollections::LoadInsuranceFundTransferFromCsvFile.new(file: file, branch: branch, paid_at: paid_at, prepared_by: prepared_by).execute!
            flash[:success] = "Successfully Import Insurance Fund Transfer Record."
            UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully Import Insurance Fund Transfer.", email: current_user.email, username: current_user.username)
            redirect_to cash_management_fund_transfer_insurance_payment_collections_path
          else
            redirect_to upload_insurance_fund_transfer_path
            flash[:error] = @errors
          end  
        end

        def payment_collection_params
          params.require(:payment_collection).permit!
        end
      end
    end
  end
end
