module CashManagement
  module Operations
    class MonthlyInterestAndTaxClosingRecordsController < ApplicationController
      before_action :authenticate_user!
      def index
        @monthly_interest_and_tax_closing_records = MonthlyInterestAndTaxClosingRecord.select("*")
      end

      def show
        @monthly_interest_and_tax_closing_record = MonthlyInterestAndTaxClosingRecord.find(params[:id])
        @voucher = MonthlyClosing::ProduceVoucherForMonthlyInterestAndTaxClosingRecord.new(monthly_interest_and_tax_closing_record: @monthly_interest_and_tax_closing_record, user: current_user).execute!
        @closing_date = @monthly_interest_and_tax_closing_record.closing_date
        @last_working_day = Utils::GetLastWorkingDay.new(some_date: (@closing_date - 1.month)).execute!

        if @monthly_interest_and_tax_closing_record.void?
          @reverse_voucher = Voucher.where(
                              book: 'JVB', 
                              reference_number: @monthly_interest_and_tax_closing_record.reverse_voucher_reference_number
                            ).first
        end
      end
    end
  end
end
