module CashManagement
  module TimeDeposits
    class OperationDetailsController < ApplicationController
      before_action :authenticate_user!

      def index
        @data =  PaymentCollection.where(payment_type: "time_deposit").order("payment_collections.status DESC, payment_collections.paid_at" )
      end

      def show
        id = params[:id]
        
        @data = DepositTimeTransaction.where(payment_collection_id: id)


        @payment_collection = PaymentCollection.find(id)

        if @payment_collection.status == "approved"
          @voucher = Voucher.where(reference_number: @payment_collection.reference_number, book:  @payment_collection.book).first

        else

          @voucher = ::PaymentCollections::GenerateAccountingEntryForTimeDepositCollection.new(payment_collection_id: id).execute!

        end


      end

      def edit
       id = params[:id]
        @edit_date = PaymentCollection.find(id)

        @data = ::PaymentCollections::LoadTimeDepositList.new(payment_id: id).execute!
       # render json: {data: @data}
    
      
      end

      def list_of_time_deposit
        @c_working_date = ApplicationHelper.current_working_date

        @c_working_date_details = @c_working_date.to_date
        @list_time_deposit = DepositTimeTransaction.where("status = ? and end_date <= ?", "lock-in", @c_working_date_details)

        @voucher = ::PaymentCollections::GenerateAccountingEntryTimeDepositInterest.new(user: current_user).execute!
      end  

    end
  end
end
