module CashManagement
  module Withdrawals
    class PaymentCollectionsController < ApplicationController
      before_action :authenticate_user!
      before_action :load_record, only: [:edit, :update, :destroy, :show]
      before_action :load_types

      def load_record
        @payment_collection = PaymentCollection.find(params[:id])
      end

      def load_types
        @savings_types    = SavingsType.where("code <> 'Time Deposit'")
        #@savings_types    = SavingsType.all
        @insurance_types  = InsuranceType.all
        @equity_types     = EquityType.all
        @branches         = Branch.all
        @centers          = Center.all
      end

      def pdf
        @payment_collection = PaymentCollection.find(params[:payment_collection_id])
        @savings_types       = SavingsType.all
        @insurance_types     = InsuranceType.all
        @equity_types        = EquityType.all
      end

      def print_pdf
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displayed all payment collections (Withdrawals)", email: current_user.email, username: current_user.username)
        @payment_collections = PaymentCollection.withdraw.order("created_at DESC")

        if ["OAS", "BK"].include? current_user.role
          @payment_collections = @payment_collections.where(status: "pending")
        end

        if params[:q].present?
          @q = params[:q]
          @payment_collections = @payment_collections.where("check_number LIKE :q OR check_voucher_number LIKE :q", q: "#{@q}%")
        end

        if params[:start_date].present?
          @start_date = params[:start_date]
          @payment_collections = @payment_collections.where("paid_at >= ?", @start_date)
        end

        if params[:end_date].present?
          @end_date = params[:end_date]
          @payment_collections = @payment_collections.where("paid_at <= ?", @end_date)
        end

        if params[:branch_id].present?
          @branch_id = params[:branch_id]
          @payment_collections = @payment_collections.where(branch_id: params[:branch_id])
        end

        if params[:t_status].present?
          @status = params[:t_status]
          @payment_collections=  @payment_collections.where(status: @status)
        end

        if params[:center_id].present?
          @center_id  = params[:center_id]
          @payment_collections  = @payment_collections.joins(payment_collection_records: :member).where("members.center_id = ?", @center_id)
        end
      end


      def index
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displayed all payment collections (Withdrawals)", email: current_user.email, username: current_user.username)
        @payment_collections = PaymentCollection.withdraw.order("created_at DESC").page params[:page]

        if ["OAS", "BK"].include? current_user.role
          @payment_collections = @payment_collections.where(status: "pending")
        end

        if params[:q].present?
          @q = params[:q]
          @payment_collections = @payment_collections.where("check_number LIKE :q OR check_voucher_number LIKE :q", q: "#{@q}%")
        end

        if params[:start_date].present?
          @start_date = params[:start_date]
          @payment_collections = @payment_collections.where("paid_at >= ?", @start_date)
        end

        if params[:end_date].present?
          @end_date = params[:end_date]
          @payment_collections = @payment_collections.where("paid_at <= ?", @end_date)
        end

        if params[:branch_id].present?
          @branch_id = params[:branch_id]
          @payment_collections = @payment_collections.where(branch_id: params[:branch_id])
        end

        if params[:t_status].present?
          @status = params[:t_status]
          @payment_collections=  @payment_collections.where(status: @status)
        end

        if params[:center_id].present?
          @center_id  = params[:center_id]
          @payment_collections  = @payment_collections.joins(payment_collection_records: :member).where("members.center_id = ?", @center_id)
        end
      end

      def new
        UserActivity.create!(
          user_id: current_user.id, 
          role: current_user.role, 
          content: "New payment collection (Withdrawals)", 
          email: current_user.email, 
          username: current_user.username
        )

        if !params[:branch_id].present? or !params[:date_of_payment].present?
          flash[:error] = "No branch or date of payment defined"
          redirect_to cash_management_withdrawals_payment_collections_path
        else
          branch = Branch.find(params[:branch_id])

          center = nil
          if params[:center_id].present?
            center = Center.find(params[:center_id])
          end

          date_of_payment = params[:date_of_payment]
          prepared_by = current_user.full_name.upcase

          members = []
          if params[:member_id].present?
            members << Member.find(params[:member_id])
          end

          @payment_collection = PaymentCollections::CreatePaymentCollectionForWithdrawals.new(branch: branch, center: center, paid_at: date_of_payment, prepared_by: prepared_by, members: members).execute!

          if @payment_collection.save
            redirect_to cash_management_withdrawals_payment_collection_path(@payment_collection)
          else
            flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
            redirect_to cash_management_withdrawals_payment_collections_path
          end
        end
      end

      def edit
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Edit payment collection (Withdrawals)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
        if @payment_collection.or_number.include?("CHANGE-ME")
          @payment_collection.or_number = ""
        end
        @members = Member.pure_active.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id)).order("last_name ASC")
      end

      def update
        @members = Member.pure_active.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
        if @payment_collection.update(payment_collection_params)
          flash[:success] = "Successfully saved collection."
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully updated payment collection (Withdrawals)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
          redirect_to cash_management_withdrawals_payment_collection_path(@payment_collection)
        else
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to update payment collection (Withdrawals)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id) 

          Rails.logger.debug(@payment_collection.inspect)
          flash[:error] = "Error in saving collection. #{@payment_collection.errors.messages}"
          @errors = @payment_collection.errors.messages
          render :edit
        end
      end

      def show
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Displaying payment collection (Withdrawals)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
        @members = Member.where(branch_id: @payment_collection.branch.id).where.not(id: @payment_collection.payment_collection_records.pluck(:member_id))
        if @payment_collection.center_id.present?
          @members = @members.where(center_id: @payment_collection.center.id)
        end
        @voucher = PaymentCollections::ProduceVoucherForWithdrawalPaymentCollection.new(payment_collection: @payment_collection).execute!
      end

      def destroy
        UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Destroying payment collection (Withdrawals)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
        if !@payment_collection.pending?
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Failed to destroy payment collection (Withdrawals)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
          flash[:error] = "Cannot destroy non pending record"
          redirect_to cash_management_withdrawals_payment_collection_path(@payment_collection)
        else
          UserActivity.create!(user_id: current_user.id, role: current_user.role, content: "Successfully destroyed payment collection (Withdrawals)", email: current_user.email, username: current_user.username, record_reference_id: @payment_collection.id)
          @payment_collection.destroy!
          flash[:success] = "Successfully destroyed payment collection."
          redirect_to cash_management_withdrawals_payment_collections_path
        end
      end

      def payment_collection_params
        params.require(:payment_collection).permit!
      end
    end
  end
end
