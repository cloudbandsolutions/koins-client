class NegativeLoanPaymentsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_defaults

  def new
    if NegativeLoanPayment.pending.where(loan_id: @loan.id).size > 0
      flash[:error] = "There are still pending negative loan payments for this loan"
      redirect_to loan_path(@loan)
    else
      @negative_loan_payment  = NegativeLoanPayment.new
    end
  end

  def create
    @negative_loan_payment  = NegativeLoanPayment.new(negative_loan_payment_params)
    @negative_loan_payment.generated_by = current_user.full_name
    @negative_loan_payment.loan = @loan
    @negative_loan_payment.transacted_at = @current_working_date

    if @negative_loan_payment.save
      flash[:success] = "Successfully saved record"
      redirect_to loan_path(@loan)
    else
      flash[:error] = "Error in saving record. #{@negative_loan_payment.errors.messages}"
      render :new
    end
  end

  def edit
    @negative_loan_payment = NegativeLoanPayment.find(params[:id])
  end

  def update
    @negative_loan_payment = NegativeLoanPayment.find(params[:id])
    @negative_loan_payment.generated_by = current_user.full_name
    @negative_loan_payment.loan = @loan
    @negative_loan_payment.transacted_at = @current_working_date

    if @negative_loan_payment.update(negative_loan_payment_params)
      flash[:success] = "Successfully saved record"
      redirect_to loan_path(@loan)
    else
      flash[:error] = "Error in saving record"
      render :edit
    end
  end

  def destroy
    @negative_loan_payment = NegativeLoanPayment.find(params[:id])

    if @negative_loan_payment.approved?
      flash[:error] = "Cannot destroy approved record"
      redirect_to loan_path(@loan)
    else
      @negative_loan_payment.destroy!
      flash[:success] = "Successfully destroyed record"
      redirect_to loan_path(@loan)
    end
  end

  def load_defaults
    @loan = Loan.find(params[:loan_id])
    @current_working_date = ApplicationHelper.current_working_date
  end

  def negative_loan_payment_params
    params.require(:negative_loan_payment).permit!
  end
end
