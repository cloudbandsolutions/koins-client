class BranchesController < ApplicationController
  before_action :authenticate_user!

  def index
    @branches = Branch.select("*") 
  end
end
