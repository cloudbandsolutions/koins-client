class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  layout :layout_by_resource

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :check_for_daily_closing
  before_action :preload_branches

  rescue_from "ActionController::RoutingError", with: :handle_routing_errors

  def preload_branches
    if user_signed_in?
      if current_user.role == "MIS"
        if current_user.branches.size > 0
          @branches = current_user.branches
        else
          @branches = Branch.all
        end
      else
        @branches = current_user.branches
      end
    end
  end

  # Error handlers
  def handle_routing_errors

    render json: { errors: ['no such route'] }, status: 404
  end

  def check_for_daily_closing
    now = Time.now
    cut_off_hour  = Settings.cut_off_time_hour
    #if (cut_off_hour..24).cover? now.hour or (0..6).cover? now.hour
    if now.hour >= cut_off_hour
      if user_signed_in?
        if !["MIS", "ACC", "AO", "REMOTE-BK", "REMOTE-FM"].include? current_user.role
          flash.now[:error] = "Can only operate between 7:00 am to #{cut_off_hour}:00 pm"
          sign_out(current_user)
        end
      end
    end
  end

  def verify_api_key!
    if !params[:api_key].present?
      render json: { success: false, info: "Valid api key required", data: { } }, status: 401
    elsif AppKey.where(api_key: params[:api_key]).count == 0
      render json: { success: false, info: "No app foud with api_key #{params[:api_key]}", data: { } }, status: 404
    end
  end

  def authenticate_mis_user!
    if current_user.role != "MIS"
      flash[:error] = "Only MIS can access this module"
      redirect_to root_path
    end
  end

  protected

  FOR_PDF_PATH = [
    "vouchers",
    "member_shares",
    "cash_management/deposits/payment_collections",
    "cash_management/withdrawals/payment_collections",
    "cash_management/insurance_withdrawals/payment_collections",
    "cash_management/fund_transfer/insurance/payment_collections",
    "insurance/payment_collections",
    "membership_payments/payment_collections",
    "print/pdf_loan_accounting_entry",
    "members",
    "loans",
    "reports",
    "accounting/reports",
    "general_ledger",
    "claims",
    "insurance_accounts",
    "pages",
    "accounting/pages",
    "cash_management/operations/monthly_interest_and_tax_closing_records",
    "books",
    "insurance_account_validations",
    "savings_accounts",
    "insurance_accounts",
    "equity_accounts",
    "voucher_summary",
    "clip_claims",
    "payment_collections",
    "accounting/interest_on_share_capital_collections",
    "accounting/patronage_refunds",
    "member_certificates",
  ]

  FOR_PDF = [
    "insurance_exit_age_members_pdf",
    "pdf",
    "change_beneficiaries_pdf",
    "loan_ledger_pdf",
    "pdf_loan_accounting_entry",
    "ppi_pdf",
    "trial_balance_pdf",
    "general_ledger_pdf",
    "claim_validation_pdf",
    "claim_loa_pdf",
    "claims_copy_pdf",
    "chart_of_accounts_pdf",
    "monthly_interest_pdf",
    "income_statement_pdf",
    "book_pdf",
    "x_weeks_to_pay_pdf",
    "withdrawal_pdf",
    "general_pdf",
    "monthly_incentives_pdf",
    "savings_account_pdf",
    "insurance_account_pdf",
    "list_of_resign_pdf",
    "list_of_resign_details_pdf",
    "personal_funds_pdf",
    "par_pdf",
    "master_list_of_client_loans_pdf",
    "repayments_pdf",
    "equity_accounts_pdf",
    "print_pdf",
    "voucher_summary_pdf",
    "summary_of_loan_release_pdf",
    "shares_printed_pdf",
    "clip_claim_validation_pdf",
    "payment_collection_pdf",
    "interest_on_share_capital_pdf",
    "patronage_refund_collection_pdf",
    "clip_claim_loa_pdf",
    "migs_pdf",
    "blip_form_pdf",
    "check_pdf"
  ]

  def layout_by_resource
    if devise_controller?
      "landing"
    elsif FOR_PDF_PATH.include?(params[:controller]) && FOR_PDF.include?(params[:action])
      "print"
    else
      "application"
    end
  end

  def api_user_logged_in?
    if !user_signed_in?
      render status: 401, json: { success: false, info: "User not logged in" }
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys:[:email, :password, :password_confirmation, :username, :first_name,])
    devise_parameter_sanitizer.permit(:sign_in, keys:[:login, :username, :email, :password])
    devise_parameter_sanitizer.permit(:account_update, keys:[:username, :email, :password, :password_confirmation, :current_password])
  end
end
