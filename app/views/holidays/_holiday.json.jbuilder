json.extract! holiday, :id, :name, :holiday_at, :created_at, :updated_at
json.url holiday_url(holiday, format: :json)