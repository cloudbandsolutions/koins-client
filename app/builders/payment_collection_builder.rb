class PaymentCollectionBuilder
  attr_accessor :branch, :center, :paid_at, :prepared_by, :payment_collection_records, :active_loans, :loan_products, :active_members, :inactive_members

  def initialize(branch:, center:, paid_at:, prepared_by:)
    @branch       = branch
    @center       = center
    @paid_at      = paid_at
    @prepared_by  = prepared_by

    @savings_types    = SavingsType.all
    @insurance_types  = InsuranceType.all
    @equity_types     = EquityType.all
    @active_loans     = Loan.active.joins(:member).where("members.center_id = ?", center.id).order("members.last_name")
    @active_members   = Member.where(center_id: @center.id, id: @active_loans.pluck(:member_id).uniq)
    @inactive_members = Member.where(center_id: @center.id).where.not(id: @active_members.pluck(:id))
    @loan_products    = LoanProduct.where(id: @active_loans.pluck(:loan_product_id).uniq)
    @particular       = "Payment of Loan/Deposit of Funds #{center}"
    @entry_level_lps  = @loan_products.where(is_entry_point: true)
    @normal_lps       = @loan_products.where.not(is_entry_point: true)
    @payment_collection = PaymentCollection.new(
                            branch: @branch,
                            center: @center,
                            particular: @particular,
                            or_number: "#{Time.now.to_i}",
                            prepared_by: @prepared_by,
                            paid_at: @paid_at
                          )
  end

  def execute!
    build_payment_collection_records
    @payment_collection
  end

  private

  def build_payment_collection_records
    @active_loans.each do |loan|
      next_payment = loan.next_payment(@paid_at)
      loan_payment = LoanPayment.new(
                        amount: next_payment,
                        paid_at: paid_at,
                        loan: loan,
                        loan_payment_amount: next_payment,
                        cp_amount: next_payment,
                        wp_amount: 0,
                        deposit_amount: 0,
                        savings_account: loan.member.default_savings_account,
                        particular: @particular,
                        wp_particular: "n/a"
                      )

      cycle_count = 0
      member_loan_cycle = MemberLoanCycle.where(member_id: loan.member.id,
                                                loan_product_id: loan.loan_product.id).first.try(:cycle)
      if !member_loan_cycle.nil?
        cycle_count = member_loan_cycle
      end

      payment_collection_record = PaymentCollectionRecord.new(
                                    member: loan.member,
                                    loan_product: loan.loan_product,
                                  )

      # Transaction for loan payment
      collection_transaction =  CollectionTransaction.new(
                                  amount: next_payment,
                                  loan_payment: loan_payment,
                                  account_type: "LOAN_PAYMENT"
                                )

      payment_collection_record.collection_transactions << collection_transaction

      # Transactions for savings
      @savings_types.each do |savings_type|
        amount = 0

        if savings_type.is_default == true and loan.loan_product.is_entry_level?
          # TODO: Make this configurable
          # amount = Loans::LoanPaymentOperation.regular_deposit_amount(loan.amount)
          amount = Loans::RegularDepositAmountForLoanPayment.new(loan_amount: loan.amount).execute!
        end

        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: savings_type.code,
                                    account_type: "SAVINGS"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      # Transactions for insurance
      @insurance_types.each do |insurance_type|
        amount = 0.00
        loan.loan_product.loan_insurance_deduction_entries.each do |loan_insurance_deduction_entry|
          if insurance_type.id == loan_insurance_deduction_entry.insurance_type.id
            amount = cycle_count == 1 ? loan_insurance_deduction_entry.val : 0.00
          end
        end

        collection_transaction =  CollectionTransaction.new(
                                    amount: amount,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      # Transactions for equity
      @equity_types.each do |equity_type|
        collection_transaction =  CollectionTransaction.new(
                                    amount: 0.00,
                                    account_type_code: equity_type.code,
                                    account_type: "EQUITY"
                                  )
        
        payment_collection_record.collection_transactions << collection_transaction
      end

      # Transactions for withdraw payments
      collection_transaction =  CollectionTransaction.new(
                                  amount: 0.00,
                                  account_type: "WP"
                                )

      payment_collection_record.collection_transactions << collection_transaction

      @payment_collection.payment_collection_records << payment_collection_record
    end

    # For non-paying members
    # Choose entry point loan.
    # Business Rule: A Payment Collection will always have an entry point loan

    entry_level_lp = @loan_products.where(is_entry_point: true).first
    @inactive_members.each do |member|
      payment_collection_record = PaymentCollectionRecord.new(
                                    member: member,
                                    loan_product: entry_level_lp
                                  )

      # Transactions for savings
      @savings_types.each do |savings_type|
        collection_transaction =  CollectionTransaction.new(
                                    amount: 0.00,
                                    account_type_code: savings_type.code,
                                    account_type: "SAVINGS"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      # Transactions for insurance
      @insurance_types.each do |insurance_type|
        collection_transaction =  CollectionTransaction.new(
                                    amount: 0.00,
                                    account_type_code: insurance_type.code,
                                    account_type: "INSURANCE"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      # Transactions for equity
      @equity_types.each do |equity_type|
        collection_transaction =  CollectionTransaction.new(
                                    amount: 0.00,
                                    account_type_code: equity_type.code,
                                    account_type: "EQUITY"
                                  )

        payment_collection_record.collection_transactions << collection_transaction
      end

      # Transactions for withdraw payments
      collection_transaction =  CollectionTransaction.new(
                                  amount: 0.00,
                                  account_type: "WP"
                                )

      payment_collection_record.collection_transactions << collection_transaction

      @payment_collection.payment_collection_records << payment_collection_record
    end
  end
end
