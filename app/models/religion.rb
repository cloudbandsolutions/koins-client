class Religion < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :members

  def to_s
    name
  end
end
