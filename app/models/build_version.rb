class BuildVersion < ApplicationRecord
  has_many :build_categories
	
  validates :build_date, presence: true
  validates :build_number, presence: true
	accepts_nested_attributes_for :build_categories, reject_if: :all_blank, allow_destroy: true
end
