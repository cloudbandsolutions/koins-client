class LoanProductType < ApplicationRecord
  belongs_to :loan_product

  validates :name, presence: true, uniqueness: true
  validates :interest_rate, presence: true, numericality: true
  validates :processing_fee_15, presence: true, numericality: true
  validates :processing_fee_25, presence: true, numericality: true
  validates :processing_fee_35, presence: true, numericality: true
  validates :processing_fee_50, presence: true, numericality: true

  def to_s
    name
  end
end
