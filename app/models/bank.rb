class Bank < ApplicationRecord
  belongs_to :accounting_code

  has_many :branch_banks
  has_many :branches, through: :branch_banks

  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  def to_s
    name
  end
end
