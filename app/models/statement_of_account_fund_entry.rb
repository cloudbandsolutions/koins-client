class StatementOfAccountFundEntry < ApplicationRecord
  ACCOUNT_TYPES = ["savings", "insurance", "equity"]
  
  belongs_to :member
  belongs_to :branch
  belongs_to :center

  validates :debit_amount, presence: true, numericality: true
  validates :credit_amount, presence: true, numericality: true
  validates :member, presence: true
  validates :branch, presence: true
  validates :center, presence: true
  validates :date_of_transaction, presence: true
  validates :accout_type_reference_id, presence: true
  validates :account_reference_id, presence: true
end
