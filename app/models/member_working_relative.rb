class MemberWorkingRelative < ApplicationRecord
  belongs_to :member
  
  validates :name, presence: true
  validates :relationship, presence: true
end
