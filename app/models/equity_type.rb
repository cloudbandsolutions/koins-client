class EquityType < ApplicationRecord
  belongs_to :withdraw_accounting_code, class_name: 'AccountingCode', foreign_key: 'withdraw_accounting_code_id'
  belongs_to :deposit_accounting_code, class_name: 'AccountingCode', foreign_key: 'deposit_accounting_code_id'

  has_many :equity_accounts
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  validates :monthly_tax_rate, presence: true, numericality: true
  validates :monthly_interest_rate, presence: true, numericality: true

  def to_s
    name
  end
end
