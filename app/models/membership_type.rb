class MembershipType < ApplicationRecord
  belongs_to :dr_accounting_entry, class_name: "AccountingCode", foreign_key: "dr_accounting_entry_id"
  belongs_to :cr_accounting_entry, class_name: "AccountingCode", foreign_key: "cr_accounting_entry_id"
  has_many :membership_payments

  validates :name, presence: true, uniqueness: true
  validates :fee, presence: true, numericality: true
  validates :dr_accounting_entry, presence: true
  validates :cr_accounting_entry, presence: true

  before_validation :load_defaults

  def load_defaults
    self.name = self.name.upcase if !self.name.nil?
  end

  def to_s
    name
  end
end
