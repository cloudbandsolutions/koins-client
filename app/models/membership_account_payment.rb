class MembershipAccountPayment < ApplicationRecord
  DEPOSIT_TYPES = ["savings", "insurance", "equity"]
  belongs_to :membership_payment
  
  validates :amount, presence: true, numericality: true
  validates :deposit_type, presence: true, inclusion: { in: DEPOSIT_TYPES }
  validates :account_type, presence: true
end
