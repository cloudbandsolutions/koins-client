class Announcement < ApplicationRecord
  validates :title, presence: true
  validates :announced_on, presence: true
  validates :content, presence: true
  validates :announced_by, presence: true
  validates :uuid, presence: true, uniqueness: true

  before_validation :load_defaults

  has_attached_file :banner,
    styles: { 
      display: "450x150#", 
    },
    default_url: "/assets/:attachment/missing_:style.jpg"
  validates_attachment_content_type :banner, content_type: %w(image/jpeg image/jpg image/png image/bmp)

  def load_defaults
    if self.uuid.blank?
      self.uuid = SecureRandom.uuid
    end
  end

  def to_s
    "#{title} - #{announced_on.strftime("%B %d, %Y")}"
  end
end
