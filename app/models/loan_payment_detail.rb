class LoanPaymentDetail < ApplicationRecord
  belongs_to :loan_payment

  validates :loan_payment, presence: true
  validates :paid_at, presence: true
  validates :total_amount, presence: true, numericality: true

  before_validation :load_defaults

  def load_defaults
    self.paid_at = self.loan_payment.try(:paid_at)
  end
end
