class RequestRecord < ApplicationRecord
  STATUSES      = ["pending", "approved"]
  REQUEST_TYPES = ["resignation"]

  validates :uuid, presence: true, uniqueness: true
  validates :info, presence: true
  validates :prepared_by, presence: true
  validates :date_requested, presence: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :approved_by, presence: true, if: :approved?
  validates :date_approved, presence: true, if: :approved?
  validates :request_type, presence: true, inclusion: { in: REQUEST_TYPES }

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
      self.status = "pending"
    end
  end

  def approved?
    self.status == "approved"
  end

  def pending?
    self.status == "pending"
  end
end
