class SubsidiaryAdjustmentEntry < ApplicationRecord
  belongs_to :subsidiary_adjustment
  belongs_to :accounting_code

  validates :accounting_code, presence: true
  validates :amount, presence: true, numericality: true
  validates :post_type, presence: true, inclusion: { in: ["DR", "CR"] }
end
