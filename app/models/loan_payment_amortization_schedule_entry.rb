class LoanPaymentAmortizationScheduleEntry < ApplicationRecord
  belongs_to :loan_payment
  belongs_to :ammortization_schedule_entry

  validates :principal_paid, presence: true, numericality: true
  validates :interest_paid, presence: true, numericality: true
end
