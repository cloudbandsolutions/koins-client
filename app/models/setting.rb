class Setting < ApplicationRecord
  validates :key, presence: true, uniqueness: true
  validates :val, presence: true

  def to_s
    "#{key} - #{val}"
  end
end
