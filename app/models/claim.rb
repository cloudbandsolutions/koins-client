class Claim < ApplicationRecord
	INSURANCE_POLICY_TYPES = ["Basic Life", "Accidental Death", "MVAH", "TPD"]
	INSURED_CLASSIFICATION = ["Member", "Legal Dependent (Spouse)", "Legal Dependent (Child)", "Legal Dependent (Parent)"]
	CATEGORY_OF_CAUSE_OF_DEATH_TPD_ACCIDENT = ["Cardiovascular", "Respiratory", "Hematological", "Gastro Intestinal", "Gynecological", "Neurological", "Suicide", "Motor Vehicular Accident", "Accidental Death"]
	GENDER = ["Male", "Female"]

	belongs_to :branch
	belongs_to :center
	belongs_to :member

	validates :member, presence: true
	validates	:date_reported, presence: true
	# validates :branch, presence: true
	# validates :center, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.uuid.nil?
      self.uuid = "#{SecureRandom.uuid}"
    end
  end

	def age
    	if self.date_of_birth.nil?
      		"Please set date of birth"
    	else
      		begin
        		now = self.date_of_death_tpd_accident
        		now.year - self.date_of_birth.year - (self.date_of_birth.to_date.change(:year => now.year) > now ? 1 : 0)
      		rescue Exception
        		"Invalid date of birth: #{self.date_of_birth}"
      	end
    end
  end

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    {
      id: self.uuid,
      member_id: self.member.uuid,
      center_id: self.member.center.uuid,
      branch_id: self.member.branch.uuid,
      date_prepared: self.date_prepared,
      type_of_insurance_policy: self.type_of_insurance_policy,
      policy_number: self.policy_number,
      date_of_birth: self.date_of_birth,
      name_of_insured: self.name_of_insured,
      beneficiary: self.beneficiary,
      gender: self.gender,
      age: self.age,
      classification_of_insured: self.classification_of_insured,
      date_of_policy_issue: self.date_of_policy_issue,
      face_amount: self.face_amount,
      date_of_death_tpd_accident: self.date_of_death_tpd_accident,
      arrears: self.arrears,
      cause_of_death_tpd_accident: self.cause_of_death_tpd_accident,
      amount_benefit_payable: self.amount_benefit_payable,
      equity_value: self.equity_value,
      retirement_fund: self.retirement_fund,
      updated_at: self.updated_at,
      prepared_by: self.prepared_by,
      length_of_stay: self.length_of_stay,
      created_at: self.created_at,
      returned_contribution: self.returned_contribution,
      total_amount_payable: self.total_amount_payable,
      order_of_child: self.order_of_child,
      category_of_cause_of_death_tpd_accident: self.category_of_cause_of_death_tpd_accident,
      date_reported: self.date_reported,
      date_paid: self.date_paid
    } 
  end
end

                                       