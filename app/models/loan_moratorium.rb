class LoanMoratorium < ApplicationRecord
  belongs_to :ammortization_schedule_entry

  validates :ammortization_schedule_entry, presence: true
  validates :previous_due_at, presence: true
  validates :new_due_at, presence: true, moratorium_date: true
  validates :reason, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.previous_due_at = self.ammortization_schedule_entry.due_at
      self.amount_delayed = self.ammortization_schedule_entry.remaining_balance
    end
  end
end
