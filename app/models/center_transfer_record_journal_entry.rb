class CenterTransferRecordJournalEntry < ApplicationRecord
  POST_TYPES = %w(DR CR)

  belongs_to :center_transfer_record
  belongs_to :accounting_code

  validates :accounting_code, presence: true
  validates :amount, presence: true, numericality: true
  validates :post_type, presence: true, inclusion: { in: POST_TYPES }
end
