class InsuranceRemittanceRecord < ApplicationRecord
  belongs_to :member
  belongs_to :insurance_type
  belongs_to :insurance_remittance

  validates :uuid, presence: true, uniqueness: true
  validates :amount, presence: true, numericality: true
  validates :insurance_type, presence: true
  validates :member, presence: true
  validates :insurance_type, presence: true

  def insurance_account_transaction
    InsuranceAccountTransaction.where(uuid: self.uuid).first
  end
end
