class LoanProductDependency < ApplicationRecord
  belongs_to :loan_product
  belongs_to :dependent_loan_product, class_name: "LoanProduct", foreign_key: "dependent_loan_product_id"

  validates :cycle_count, presence: true, numericality: true
end
