class LoanInsuranceType < ApplicationRecord
	has_many :loan_insurances
	belongs_to :accounting_code, class_name: 'AccountingCode', foreign_key: 'accounting_code_id'
	belongs_to :cash_debit_accounting_code, class_name: 'AccountingCode', foreign_key: 'cash_debit_accounting_code_id'
	belongs_to :cash_credit_accounting_code, class_name: 'AccountingCode', foreign_key: 'cash_credit_accounting_code_id'
	belongs_to :unremitted_debit_accounting_code, class_name: 'AccountingCode', foreign_key: 'unremitted_debit_accounting_code_id'
	belongs_to :unremitted_credit_accounting_code, class_name: 'AccountingCode', foreign_key: 'unremitted_credit_accounting_code_id'
	belongs_to :accounting_fund

	validates :name, presence: true

	def to_s
    name
  end
end
