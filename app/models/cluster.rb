class Cluster < ApplicationRecord
  belongs_to :area

  has_many :branches

  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  validates :abbreviation, presence: true, uniqueness: true
  validates :address, presence: true, uniqueness: true


  def num_branches
    self.branches.count
  end

  def to_s
    name
  end

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    {
      id: self.uuid,
      name: self.name,
      short_name: self.code,
      area_id: self.area.uuid
    }
  end
end
