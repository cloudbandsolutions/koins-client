class MemberTransferRequest < ApplicationRecord
  belongs_to :member

  validates :member, presence: true
  validates :uuid, presence: true, uniqueness: true
  validates :status, presence: true, inclusion: { in: ["pending", "approved", "processed"] }
  validates :date_of_request, presence: true

  has_many :member_loan_transfer_requests, dependent: :delete_all
  accepts_nested_attributes_for :member_loan_transfer_requests

  has_many :member_savings_transfer_requests, dependent: :delete_all
  accepts_nested_attributes_for :member_savings_transfer_requests

  has_many :member_equity_transfer_requests, dependent: :delete_all
  accepts_nested_attributes_for :member_equity_transfer_requests

  has_many :member_insurance_transfer_requests, dependent: :delete_all
  accepts_nested_attributes_for :member_insurance_transfer_requests

  before_validation :load_defaults

  scope :pending, -> { where(status: "pending") }
  scope :approved, -> { where(status: "approved") }
  scope :processed, -> { where(status: "processed") }

  def pending?
    self.status == 'pending'
  end

  def approved?
    self.status == 'approved'
  end

  def processed?
    self.status == 'processed'
  end

  def load_defaults
    if self.new_record?
      self.uuid           = SecureRandom.uuid
      self.status         = "pending"
    end
  end

  def approve!
    self.update!(status: "approved")
  end
end
