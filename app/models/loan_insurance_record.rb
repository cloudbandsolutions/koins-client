class LoanInsuranceRecord < ApplicationRecord
  LOAN_CATEGORIES = ["PURE IT", "PROJECT LOAN", "UTILITY/EDUCATIONAL LOAN", "MULTIPURPOSE LOAN", "EMERGENCY LOAN","K - NEGOSYO", "K - EDUKASYON", "K - MAGGAGAWA", "K - KASANGKAPAN", "K - KALAMIDAD", "K - NHA W1", "K - NHA W2", "K - BENEPISYO W1", "K - BENEPISYO W2", "K - BAHAY W1", "K - BAHAY W2", "K - BAHAY W3", "K - KALUSUGAN W1", "K - KALUSUGAN W2", "K - KALUSUGAN W3", "K - KALUSUGAN W4", "K - KALUSUGAN W5", "K - KALUSUGAN W6", "K - PWD"]
  
  belongs_to :member
  belongs_to :loan_insurance

	validates :member, presence: true  
  # validates :date_released, presence: true
  # validates :maturity_date, presence: true
  # validates :loan_amount, presence: true
  # validates :loan_term, presence: true
  # validates :loan_insurance_amount, presence: true
  # validates :loan_category, presence: true
  # validates :loan_insurance_id, presence: true

  before_validation :load_defaults

  # after_save do
  #   loan_insurance.touch
  # end

  def load_defaults
    if self.new_record? and reference_number.nil?
      self.reference_number = "#{SecureRandom.hex(4)}"
    end

    # if self.uuid.nil?
    #   self.uuid = "#{SecureRandom.uuid}"
    # end
  end

  def pending?
    self.status == "pending"
  end

  def approved?
    self.status == "approved"
  end
end
