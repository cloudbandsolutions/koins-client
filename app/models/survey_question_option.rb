class SurveyQuestionOption < ApplicationRecord
  belongs_to :survey_question

  validates :uuid, presence: true, uniqueness: true
  validates :priority, presence: true, uniqueness: { scope: :survey_question_id }
  validates :option, presence: true, uniqueness: { scope: :survey_question_id }
  validates :score, presence: true, numericality: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record? and self.uuid.nil?
      self.uuid = SecureRandom.uuid
    end
  end

  def num_respondents
    SurveyRespondentAnswer.where(survey_question_option_id: self.id).count
  end

  def to_s
    option
  end
end
