class ClipClaim < ApplicationRecord
	GENDER = ["Male", "Female"]
	CREDITORS_NAME = ["KCOOP", "JVOMFI", "CAPS-R"]
  CAUSE_OF_DEATH = ["Cardiovascular", "Respiratory", "Hematological", "Gastro Intestinal", "Gynecological", "Neurological", "Suicide", "Others"]
  TYPES_OF_LOAN = ["K_NEGOSYO", "K-KABUHAYAN"]

	belongs_to :branch
	belongs_to :center
	belongs_to :member

	validates :member, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.uuid.nil?
      self.uuid = "#{SecureRandom.uuid}"
    end
  end

	def age
    if self.date_of_birth.nil?
      "Please set date of birth"
    else
      begin
        now = self.date_of_death
        now.year - self.date_of_birth.year - (self.date_of_birth.to_date.change(:year => now.year) > now ? 1 : 0)
      rescue Exception
        "Invalid date of birth: #{self.date_of_birth}"
      end
    end
	end

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    {
      id: self.uuid,
      member_id: self.member.uuid,
       center_id: self.member.center.uuid,
      branch_id: self.member.branch.uuid,
      date_prepared: self.date_prepared,
      creditors_name: self.creditors_name,
      policy_number: self.policy_number,
      date_of_birth: self.date_of_birth,
      member_name: self.member_name,
      beneficiary: self.beneficiary,
      gender: self.gender,
      age: self.age,
      date_of_death: self.date_of_death,
      cause_of_death: self.cause_of_death,
      effective_date_of_coverage: self.effective_date_of_coverage,
      expiration_date_of_coverage: self.expiration_date_of_coverage,
      amount_of_loan: self.amount_of_loan,
      terms: self.terms,
      amount_payable_to_beneficiary: self.amount_payable_to_beneficiary,
      prepared_by: self.prepared_by,
      created_at: self.created_at,
      updated_at: self.updated_at,
      amount_payable_to_creditor: self.amount_payable_to_creditor,
      type_of_loan: self.type_of_loan

    } 
  end
end
