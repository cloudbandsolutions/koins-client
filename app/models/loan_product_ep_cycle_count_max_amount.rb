class LoanProductEpCycleCountMaxAmount < ApplicationRecord
  belongs_to :loan_product

  validates :cycle_count, presence: true, numericality: true
  validates :max_amount, presence: true, numericality: true
end
