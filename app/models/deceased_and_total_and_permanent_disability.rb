class DeceasedAndTotalAndPermanentDisability < ApplicationRecord
	STATUSES = ["pending", "approved", "reversed"]

	belongs_to :branch

	has_many :deceased_and_total_and_permanent_disability_records, dependent: :destroy
 	accepts_nested_attributes_for :deceased_and_total_and_permanent_disability_records

	validates :branch, presence: true
	validates :date_prepared, presence: true
	validates :prepared_by, presence: true
	validates :status, presence: true, inclusion: { in: STATUSES }

	before_validation :load_defaults


	def pending?
		self.status == "pending"
	end

	def approved?
		self.status == "approved"
	end

	def reversed?
		self.status == "reversed"
	end

	def load_defaults
		if self.new_record?
		  self.status = "pending"
		end
	end
end
