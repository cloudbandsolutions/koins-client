class CenterTransferRecord < ApplicationRecord
  STATUSES = ["pending", "approved"]

  belongs_to :branch
  belongs_to :center

  has_many :center_transfer_record_journal_entries, dependent: :delete_all
  accepts_nested_attributes_for :center_transfer_record_journal_entries

  validates :uuid, presence: true, uniqueness: true
  validates :branch, presence: true
  validates :center, presence: true
  validates :data, presence: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :prepared_by, presence: true
  validates :approved_by, presence: true, if: :approved?
  validates :reference_number, presence: true, if: :approved?
  validates :particular, presence: true

	has_attached_file :file
  validates_attachment_content_type :file, content_type: ["text/plain"]
  validates :file, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
      self.status = "pending" if self.status.blank?
    end
  end

  def pending?
    self.status == "pending"
  end

  def approved?
    self.status == "approved"
  end

  def approve!
    self.update!(status: 'approved')
  end
end
