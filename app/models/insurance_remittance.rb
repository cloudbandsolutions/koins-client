class InsuranceRemittance < ApplicationRecord
  STATUSES = ["pending", "approved", "reversed"]

  belongs_to :branch
  belongs_to :center
  has_many :insurance_remittance_records

  validates :uuid, presence: true, uniqueness: true
  validates :or_number, presence: true, uniqueness: true
  validates :particular, presence: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :voucher_reference_number, presence: true, if: :approved?
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :amount, presence: true, numericality: true

  before_validation :load_defaults

  def members
    Member.where(id: insurance_remittance_records.pluck(:member_id))
  end

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
      self.status = "pending"
    end

    self.amount = 0.00
    self.insurance_remittance_records.each do |insurance_remittance_record|
      self.amount += insurance_remittance_record.amount
    end
  end

  def approved?
    self.status == "approved"
  end
end
