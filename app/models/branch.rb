class Branch < ApplicationRecord
  belongs_to :cluster
  belongs_to :bank

  has_many :branch_banks
  has_many :banks, through: :branch_banks
  has_many :user_branches
  has_many :users, through: :user_branches
  has_many :centers

  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  validates :abbreviation, presence: true, uniqueness: true
  validates :address, presence: true, uniqueness: true
  validates :member_counter, presence: true, numericality: true
  validates :cluster, presence: true
  validates :bank, presence: true

  before_validation :load_defaults

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    {
      id: self.uuid,
      name: self.name,
      short_name: self.code,
      cluster_id: self.cluster.uuid
    }
  end

  def load_defaults
    if self.new_record?
      self.member_counter = 0
    end

    if self.uuid.blank?
      self.uuid = SecureRandom.uuid
    end
  end

  def to_s
    name
  end

  def paid_amounts
    LoanPayment.joins(:loan => {:member => :branch}).where("branches.id = :id", :id => self.id).sum(:amount).to_f
  end
end
