class MemberWeeklyExpense < ApplicationRecord
  belongs_to :member
  belongs_to :loan
  validates :amount, presence: true, numericality: true
  validates :source, presence: true

  before_validation :load_defaults
    
  def load_defaults
    if self.loan
      self.member = self.loan.member
    end
  end
end
