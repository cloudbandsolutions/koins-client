class Member < ApplicationRecord
  STATUSES = [
    "blacklisted", 
    "whitelisted", 
    "active", 
    "pending", 
    "resign", 
    "archived", 
    "resigned", 
    "for-resignation", 
    "dormant", 
    "for-withdrawal",
    "for-transfer",
    "transferred",
    "cleared"
  ]

  INSURANCE_STATUS = ["inforce", "lapsed", "resigned", "dormant", "pending", "cleared"]
  MEMBER_TYPES = ["Regular", "GK", "Kaagapay"]
  GENDERS = ["Male", "Female", "Others", "Babae", "Lalake", "Iba pa"]

  belongs_to :center
  belongs_to :branch
  belongs_to :religion

  has_many :insurance_account_validation_records

  has_many :member_withdrawals, dependent: :delete_all

  has_many :savings_accounts, dependent: :delete_all
  accepts_nested_attributes_for :savings_accounts
  has_many :insurance_accounts, dependent: :delete_all
  accepts_nested_attributes_for :insurance_accounts
  has_many :equity_accounts, dependent: :delete_all
  accepts_nested_attributes_for :equity_accounts
  has_many :legal_dependents, dependent: :delete_all
  accepts_nested_attributes_for :legal_dependents, reject_if: :all_blank, allow_destroy: true
  has_many :beneficiaries, dependent: :delete_all
  accepts_nested_attributes_for :beneficiaries, reject_if: :all_blank, allow_destroy: true
  
  has_many :loan_insurance_records, dependent: :delete_all 
  has_many :loans, dependent: :delete_all
  has_many :membership_payments, dependent: :delete_all
  has_many :member_shares, dependent: :delete_all
  has_many :member_resignations, dependent: :delete_all
  has_many :subsidiary_adjustment_records, dependent: :delete_all
  has_many :claims, dependent: :delete_all
  has_many :clip_claims, dependent: :delete_all
  has_many :insurance_account_validation_cancellations, dependent: :delete_all
  has_many :member_certificates, dependent: :delete_all

  # has_many :member_working_relatives, dependent: :delete_all
  # accepts_nested_attributes_for :member_working_relatives

  has_many :member_weekly_incomes, dependent: :delete_all
  accepts_nested_attributes_for :member_weekly_incomes

  has_many :member_weekly_expenses, dependent: :delete_all
  accepts_nested_attributes_for :member_weekly_expenses

  has_many :survey_respondents, dependent: :delete_all
  accepts_nested_attributes_for :survey_respondents

  has_many :member_attachment_files, dependent: :delete_all
  accepts_nested_attributes_for :member_attachment_files, reject_if: :all_blank, allow_destroy: true

  has_many :member_transfer_requests, dependent: :delete_all

  has_attached_file :profile_picture,
    styles: { 
      thumb: "80x80#", 
      standard: "150x150#",
      large: "250x250#" },
    default_url: "/assets/:attachment/missing_:style.png"
  validates_attachment_content_type :profile_picture, content_type: %w(image/jpeg image/jpg image/png image/bmp)

  has_attached_file :signature,
    styles: {
      medium: "300x150!",
      list: "630x200!" },
    default_url: "/assets/:attachment/missing_:style.png"
  validates_attachment_content_type :profile_picture, content_type: %w(image/jpeg image/jpg image/png image/bmp)

  # validates :last_name, presence: true
  # validates :first_name, presence: true
  # validates :first_name, presence: true, uniqueness: { scope: [:middle_name, :last_name, :center_id] }
  # validates :last_name, presence: true, uniqueness: { scope: [:first_name, :middle_name, :center_id] }
  validates :address_street, presence: true
  validates :address_barangay, presence: true
  validates :address_city, presence: true
  validates :civil_status, presence: true
  # for checking of new member if age is above 59 years old
  # validates :date_of_birth, :check_date_of_birth, presence: true, if: :new_record?
  validates :date_of_birth, presence: true
  validates :identification_number, presence: true, uniqueness: true
  validates :status, presence: true, inclusion: { in: Member::STATUSES }
  #validates :insurance_status, presence: true, inclusion: { in: Member::INSURANCE_STATUS }
  validates :member_type, presence: true, inclusion: { in: Member::MEMBER_TYPES }
  validates :gender, presence: true, inclusion: { in: Member::GENDERS }
  validates :center, presence: true
  validates :branch, presence: true

  VALID_SSS_FORMAT = /\A^[0-9]{11}\z/
  VALID_PAG_IBIG_FORMAT = /\A^[0-9]{13}\z/
  VALID_PHIL_HEALTH_FORMAT = /\A^[0-9]{12}\z/

  #validates :sss_number, format: { with: VALID_SSS_FORMAT, message: "Should have 11 numbers" }, allow_blank: true
  #validates :phil_health_number, format: { with: VALID_PHIL_HEALTH_FORMAT, message: "Should have 12 numbers" }, allow_blank: true
  #validates :pag_ibig_number, format: { with: VALID_PAG_IBIG_FORMAT, message: "Should have 13 numbers" }, allow_blank: true

  scope :associates, -> { where(member_type: "Associate", status: "active") }
  scope :regulars, -> { where(member_type: "Regular", status: "active") }
  scope :active, -> { where(status: ["active", "for-resignation", "resign", "resigned"]) }
  scope :pure_active, -> { where(status: "active") }
  scope :active_and_pending, -> { where(status: ["active", "pending"]) }
  scope :resigned, -> { where(status: ["resign", "resigned"]) }
  scope :active_and_resigned, -> { where(status: ["active", "resigned"]).order("last_name ASC") }
  scope :current, -> { where("status NOT IN (?)", ["archived"]) }
  scope :pending, -> { where(status: "pending") }
  scope :for_transfer, -> { where(status: "for-transfer") }
  scope :resigned_from_microinsurance, -> { where(insurance_status: 'resigned').order("last_name ASC") }
  scope :archived, -> { where(status: "archived") }

  after_update :update_active_and_pending_loans
  after_save :process_membership
  #after_save :update_accounts

  # check if new member is above 59 years old
  # def check_date_of_birth
  #   if self.date_of_birth.present?
  #     years_old = ((Time.now.to_date - self.date_of_birth)/365.242199).to_i
  #     max_age = Settings.max_age_for_new_member ||= 59
  #     if years_old > max_age 
  #       errors.add(:date_of_birth, "New member must be 59 years old below.")
  #     else
  #       now = Time.now.utc.to_date
  #       now.year - self.date_of_birth.year - (self.date_of_birth.to_date.change(:year => now.year) > now ? 1 : 0)
  #     end
  #   end
  # end
  def to_membership_date_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end
    {
      id: self.uuid,
      branch_id: self.branch.uuid,
      first_name: self.first_name,
      middle_name: self.middle_name,
      last_name: self.last_name,
      #kcoop_mem_date: self.previous_mfi_member_since,
      #mba_mem_date: self.previous_mii_member_since


    }
  end
  
  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    {
      id: self.uuid,
      center_id: self.center.uuid,
      branch_id: self.branch.uuid,
      first_name: self.first_name,
      middle_name: self.middle_name,
      last_name: self.last_name,
      gender: self.gender,
      date_of_birth: self.date_of_birth,
      civil_status: self.civil_status,
      home_number: self.home_number,
      mobile_number: self.mobile_number,
      processed_by: self.processed_by,
      approved_by: self.approved_by,
      identification_number: self.identification_number,
      place_of_birth: self.place_of_birth,
      status: self.status,
      member_type: self.member_type,
      religion: self.religion.to_s,
      insurance_status: self.insurance_status,
      data: {
        address: {
          street: self.address_street,
          district: self.address_barangay,
          city: self.address_city
        },
        spouse: {
          first_name: self.spouse_first_name,
          middle_name: self.spouse_middle_name,
          last_name: self.spouse_last_name,
          date_of_birth: self.spouse_date_of_birth,
          occupation: self.spouse_occupation
        },
        government_identification_numbers: {
          sss_number: self.sss_number,
          pag_ibig_number: self.pag_ibig_number,
          phil_health_number: self.phil_health_number,
          tin_number: self.tin_number
        },
        num_children_elementary: self.num_children_elementary || 0,
        num_children_high_school: self.num_children_high_school || 0,
        num_children_college: self.num_children_college || 0,
        num_children: self.num_children || 0,
        reason_for_joining: self.reason_for_joining,
        housing: {
          type: self.type_of_housing,
          num_months: self.num_months_housing,
          num_years: self.num_years_housing,
          proof: self.proof_of_housing
        },
        resignation: {
          type: self.resignation_type,
          code: self.resignation_code,
          reason: self.resignation_reason,
          accounting_reference_number: self.resignation_accounting_reference_number
        },
        banks: [
          {
            bank: self.bank,
            type: self.bank_account_type
          },
        ],
        is_experienced_with_microfinance: self.is_experienced_with_microfinance,
        recognition_date: self.previous_mii_member_since
      },
      date_resigned: self.date_resigned,
      insurance_date_resigned: self.insurance_date_resigned,
      meta: self.meta
    }
  end

  def transferred?
    self.status == "transferred"
  end

  def cleared?
    self.status == "cleared"
  end

  def member_transfer_request
    self.member_transfer_requests.first
  end

  def for_transfer?
    self.status == "for-transfer"
  end

  def transfered?
    self.status == "transfered"
  end

  def recognition_date_formatted
    if previous_mii_member_since.present?
      previous_mii_member_since.strftime("%B %d, %Y")
    else
      insurance_membership_type_name = Settings.insurance_membership_type_name
      d = nil
      self.memberships.each do |membership|
        if membership[:membership_type].name == insurance_membership_type_name
          d = membership[:membership_payment].try(:paid_at).try(:to_date)
        end
      end

      if !d.nil?
        d.strftime("%B %d, %Y")
      else
        "Not found"
      end
    end
  end

  def resigned?
    ["resigned", "resign"].include?(self.status)
  end

  def resignation_cause
    mr = MemberResignation.where(member_id: self.id).first
    if mr
      mr.resignation_type
    else
      "INVALID"
    end
  end

  def equity_balance
    self.equity_accounts.sum(:balance)
  end

  def initial_payment_equity
    EquityAccountTransaction.where(equity_account_id: self.equity_accounts.pluck(:id), status: 'approved').first.try(:amount)
  end

  def total_life
    self.insurance_accounts.joins(:insurance_type).where("insurance_types.code = ?", "LIF").first.balance
  end

  def num_dependents
    self.legal_dependents.count
  end

  def highest_education
  end
  
  def member_type_special_loan_fund?
    Settings.special_loan_fund_member_type.include?(self.member_type)
  end

  def net_weekly_income
    self.member_weekly_incomes.sum(:amount) - self.member_weekly_expenses.sum(:amount)
  end

  def sss_number_formatted
    if sss_number.present?
      "#{sss_number[0..1]}-#{sss_number[2..8]}-#{sss_number[9..10]}"
    end
  end

  def phil_health_number_formatted
    if phil_health_number.present?
      "#{phil_health_number[0..1]}-#{phil_health_number[2..9]}-#{phil_health_number[10..11]}"
    end
  end

  def pag_ibig_number_formatted
    if pag_ibig_number.present?
      "#{pag_ibig_number[0..3]}-#{pag_ibig_number[4..7]}-#{pag_ibig_number[8..11]}"
    end
  end

  def archived?
    self.status == 'archived'
  end

  def resigned?
    self.status == 'resigned'
  end

  def active_resigned?
    self.status == "resigned" or self.status == "active"
  end 

  def for_resignation?
    self.status == 'for-resignation'
  end

  def active?
    self.status == 'active'
  end

  def pending?
    self.status == 'pending'
  end

  def has_sss_number?
    self.sss_number.present?
  end

  def equity_amount
    self.equity_accounts.sum(:balance)
  end

  def mii_since
    self.previous_mii_member_since.blank? ? "n/a" : self.previous_mii_member_since.strftime("%b %d, %Y")
  end

  def process_membership
    if !self.previous_mii_member_since.nil?
      membership_types = MembershipType.where(activated_for_insurance: true)
      membership_types.each do |membership_type|
        membership_payment = MembershipPayment.where(member_id: self.id, membership_type_id: membership_type.id).first
        if membership_payment.nil?
          MembershipPayment.create!(membership_type: membership_type, member: self, paid_at: self.previous_mii_member_since, amount_paid: 0.00, status: "paid")
        else
          membership_payment.update!(paid_at: self.previous_mii_member_since)
        end
      end
    end

    if !self.previous_mfi_member_since.nil? and self.auto_active == true
      membership_types = MembershipType.where.not(activated_for_insurance: true)
      membership_types.each do |membership_type|
        membership_payment = MembershipPayment.where(member_id: self.id, membership_type_id: membership_type.id).first
        if membership_payment.nil?
          MembershipPayment.create!(membership_type: membership_type, member: self, paid_at: self.previous_mfi_member_since, amount_paid: 0.00, status: "paid")
        else
          membership_payment.update!(paid_at: self.previous_mfi_member_since)
        end
      end
    end
  end

  def loaner_status
    current_date = Date.today
    if Loan.active.where(member_id: self.id).count > 0
      result = 'active loaner'
    elsif Loan.active.where(member_id: self.id).count == 0 and Loan.paid.where(member_id: self.id).count > 0
      lastet_paid_loan = Loan.paid.where(member_id: self.id).last

      if current_date - latest_paid_loan.first_date_of_payment < 6.months
        result = 'inactive loaner'
      else
        result = 'lie low'
      end
    else
      result = 'lie low'
    end

    result
  end

  def saver_status
    current_date = Date.today
    latest_transaction = SavingsAccountTransaction.approved.where(savings_account_id: self.savings_accounts.pluck(:id)).last
    if !latest_transaction.nil?
      if (current_date - latest_transaction.transacted_at) > 2.years
        'dormant'
      else
        'active'
      end
    else
      'pending'
    end
  end

  def equity_status
  end

  def bod_res_num_acc
    ""
  end

  def occupation
    ""
  end

  def date_accepted_coop
    membership_type_name  = Settings.cooperative_membership_type
    if membership_type_name.present?
      membership_type     = MembershipType.where(name: membership_type_name).first
      membership_payment  = MembershipPayment.paid.where(
                              member_id: self.id,
                              membership_type_id: membership_type.try(:id)
                            ).first

      if membership_payment.present?
        membership_payment.paid_at.strftime("%B %d, %Y")
      else
        "payment not found"
      end
    else
      "not configured"
    end
  end

  def is_member?(membership_type)
    MembershipPayment.where(member_id: self.id, membership_type_id: membership_type.id, status: 'paid').count > 0
  end

  def memberships
    data = []
    MembershipType.all.each do |membership_type|
      d = {}
      d[:membership_type] = membership_type

      membership_payment  = MembershipPayment.where(
                              status: ["paid", "pending"],
                              member_id: self.id, 
                              membership_type_id: membership_type.id
                            ).first

      if membership_payment.nil?
        d[:paid] = false
        d[:membership_payment] = membership_payment
      elsif membership_payment.status == "paid"
        d[:paid] = true
        d[:membership_payment] = membership_payment
      elsif membership_payment.status == "pending"
        d[:paid] = false
        d[:membership_payment] = membership_payment
      end

      data << d
    end

    data
  end

  def spouse_full_name
    "#{spouse_first_name.try(:upcase)} #{self.spouse_middle_name.try(:upcase)} #{self.spouse_last_name.try(:upcase)}"
  end

  def spouse_full_name_titleize
    "#{self.spouse_last_name.try(:titleize)}, #{self.spouse_middle_name.try(:titleize)} #{self.spouse_first_name.try(:titleize)}"  
  end

  def update_active_and_pending_loans
    self.loans.active_and_pending.each do |loan|
      loan.update!(branch: self.branch, center: self.center)
    end
  end

  def savings_balance
    self.savings_accounts.where("savings_type_id <> ?", 3).sum(:balance)
  end


  def cbu_balance
    self.savings_accounts.where("savings_type_id = ?", 3).sum(:balance)
  end



  def insurance_balance
    self.insurance_accounts.sum(:balance)
  end

  def equity_balance
    self.equity_accounts.sum(:balance)
  end

  def loan_balance
    self.loans.active.sum(:remaining_balance)
  end

  def toggle_active!
    self.update!(status: "active")
  end

  def has_active_loans?
    self.loans.active.count > 0 ? true : false
  end

  def has_pending_loans?
    self.loans.pending.count > 0 ? true : false
  end

  # WARNING: deprecated
  def has_active_transactions?
    self.loans.active.count > 0 ? true : false
  end

  def full_address
    "#{address_street} ST., BARANGAY #{address_barangay}, #{address_city} CITY"
  end

  def full_address_upcase
    "#{address_street.upcase}, #{address_barangay.upcase}, #{address_city.upcase}, PH"
  end

  def latest_active_loan(loan_product)
    self.loans.active.where("loan_product_id = ?", loan_product.id).first
  end

  # Initiate some default values before validation
  before_validation :load_defaults

  def loan_accounts_by_loan_product(loan_product_id)
    Loan.where(member_id: self.id, loan_product_id: loan_product_id)
  end

  def to_s
    full_name_titleize
    full_name_titleize
    full_name_formatted
  end

  def age
    if self.date_of_birth.nil?
      "Please set date of birth"
    else
      begin
        now = Time.now.utc.to_date
        now.year - self.date_of_birth.year - ((now.month > self.date_of_birth.month || (now.month == self.date_of_birth.month && now.day >= self.date_of_birth.day)) ? 0 : 1)
        #now.year - self.date_of_birth.year - (self.date_of_birth.to_date.change(:year => now.year) > now ? 1 : 0)
      rescue Exception
        "Invalid date of birth: #{self.date_of_birth}"
      end
    end
  end

  def spouse_age
    if self.spouse_date_of_birth.nil?
      0
    else
      begin
        now = Time.now.utc.to_date
        now.year - self.spouse_date_of_birth.year - ((now.month > self.spouse_date_of_birth.month || (now.month == self.spouse_date_of_birth.month && now.day >= self.spouse_date_of_birth.day)) ? 0 : 1)
        #((Time.zone.now - spouse_date_of_birth.to_time) / 1.year.seconds).floor
      rescue Exception
        "Invalid date of birth: #{self.spouse_date_of_birth}"
      end
    end
  end

  def length_of_stay
    if self.previous_mii_member_since.present?
      now = Time.now
      seconds_between = (now.to_time - self.previous_mii_member_since.to_time).abs 
      days_between = seconds_between / 60 / 60 / 24
      number_of_days = days_between.floor
      number_of_months = (days_between / 30.44).floor
      years = (days_between / 365.242199).floor
      months = number_of_months - (years * 12)
      if years < 1
        if months > 1
          "#{months} MONTHS"
        elsif months == 1
          "#{months} MONTH"
        elsif months < 1
          if number_of_days == 1 
            "#{number_of_days} DAY"
          elsif number_of_days > 1
            "#{number_of_days} DAYS"
          elsif number_of_days < 1
            nil          
          end
        end    
      else
        if years == 1 && months == 0 
          "#{years} YEAR"
        elsif years == 1 && months == 1
          "#{years} YEAR, #{months} MONTH"
        elsif years == 1 && months > 1
          "#{years} YEAR, #{months} MONTHS"
        elsif years > 1 && months > 1
          "#{years} YEARS, #{months} MONTHS"
        elsif years > 1 && months == 1
          "#{years} YEARS, #{months} MONTH"
        elsif years > 1 && months < 1
          "#{years} YEARS"    
        end
      end
    end
  end

  def full_name_middle_initial
    if self.middle_name == ""
      "#{last_name.titleize}, #{first_name.titleize}"
    else
      "#{last_name.titleize}, #{first_name.titleize} #{middle_name[0].try(:titleize)}."
    end
  end

  def full_name_middle_initial_without_period
    if self.middle_name == ""
      "#{first_name.titleize} #{last_name.titleize}"
    else
      "#{first_name.titleize} #{middle_name[0].try(:titleize)}. #{last_name.titleize}"
    end
  end

  def check_name
    "#{first_name.upcase} #{middle_name.upcase} #{last_name.upcase}"
  end

  def full_name_titleize
    "#{last_name.titleize}, #{first_name.titleize} #{middle_name.titleize}"  
  end

  def full_name_formatted
    "#{first_name.titleize} #{middle_name.titleize} #{last_name.titleize}"  
  end

  def full_name
    #"#{first_name.upcase} #{middle_name.upcase} #{last_name.upcase}"
    "#{last_name.upcase}, #{first_name.upcase} #{middle_name.upcase}"
  end

  def full_name_with_center
    "#{last_name.upcase}, #{first_name.upcase} #{middle_name.upcase} (#{self.center.try(:to_s)})"
  end

  def list_name
    "#{last_name.upcase}, #{first_name.upcase} #{middle_name.upcase}"
  end

  def is_associate?
    if self.member_type == "associate"
      true
    else
      false
    end
  end

  def approve!
    self.update!(status: "active")
  end

  def archive!
    self.update!(
      identification_number: "#{self.identification_number}-archived-#{Time.now.to_i}", 
      status: "archived",
      insurance_status: "dormant",
      first_name: "#{self.first_name}-archived-#{Time.now.to_i}",
      last_name: "#{self.last_name}-archived-#{Time.now.to_i}"
    )
  end

  def possible_co_makers
    Member.where("id != ? AND center_id = ?", self.id, self.center_id)
  end

  def default_savings_account
    default_savings_type = SavingsType.where(is_default: true).first

    if default_savings_type.nil?
      default_savings_type = SavingsType.first
    end

    SavingsAccount.where(member_id: self.id, savings_type_id: default_savings_type.id).first
  end

  def load_defaults
    if self.new_record?
      if !self.branch.nil?
        self.identification_number  = Members::GenerateMemberIdentificationNumber.new(
                                        cluster_code: self.branch.cluster.code, 
                                        branch_code: self.branch.code, 
                                        branch: self.branch
                                      ).execute! if self.identification_number.blank?
      end

      if self.status.nil?
        self.status = "pending"
      end

      if self.is_reinstate.nil?
        self.is_reinstate = false
      end
    end

    if self.insurance_status.nil?
      self.insurance_status = "dormant"
    end

    if self.middle_name.nil?
      self.middle_name = ""
    end

    if self.uuid.nil?
      self.uuid = SecureRandom.uuid
    end

    self.first_name = self.first_name.upcase if !self.first_name.nil?
    self.middle_name = self.middle_name.upcase if !self.middle_name.nil?
    self.last_name = self.last_name.upcase if !self.last_name.nil?

    if self.gender == "Lalake"
      self.gender = "Male"
    end

    if self.gender == "Babae"
      self.gender = "Female"
    end

    if self.gender == "Iba pa"
      self.gender = "Others"
    end

    if self.auto_active == true and self.status == 'pending'
      self.status = 'active'
    end
  end

  def toggle_archived
    self.status = "archived"
    self.save!
  end

  def has_insurance_account_type?(insurance_type)
    InsuranceAccount.where(member_id: self.id, insurance_type_id: insurance_type.id).count > 0 ? true : false
  end

  after_save :set_default_primary_beneficiary

  def set_default_primary_beneficiary 
    if self.beneficiaries.count == 1 
      self.beneficiaries.first.update(is_primary: true)
    end
  end

  def to_hash
    m = self.attributes.except!("id", "profile_picture_file_name", "profile_picture_content_type", "profile_picture_file_size", "profile_picture_updated_at", "signature_file_name", "signature_content_type", "signature_file_size", "signature_updated_at")
    m[:center_uuid] = Center.find(self.center_id).uuid
    m[:branch_code] = Branch.find(self.branch_id).code

    return m
  end

  def member_status_color
    if self.status == "pending"
      "red"
    end
  end

  def length_of_housing_occupancy
    if self.num_years_housing.nil? && self.num_months_housing.nil?
      ""
    elsif self.num_years_housing.nil? && !self.num_months_housing.nil?
      "#{num_months_housing} MONTH/S"
    elsif self.num_months_housing.nil? && !self.num_years_housing.nil?
      "#{num_years_housing} YEAR/S"
    elsif !self.num_years_housing.nil? && !self.num_months_housing.nil? 
      "#{num_years_housing} YEAR/S, #{num_months_housing} MONTH/S"
    end        
  end

  def length_of_business
    if self.num_years_business.nil? && self.num_months_business.nil?
      ""
    elsif self.num_years_business.nil? && !self.num_months_business.nil?
      "#{num_months_business} MONTH/S"
    elsif self.num_months_business.nil? && !self.num_years_business.nil?
      "#{num_years_business} YEAR/S"
    elsif !self.num_years_business.nil? && !self.num_months_business.nil? 
      "#{num_years_business} YEAR/S, #{num_months_business} MONTH/S"
    end        
  end

  def equity_value
    self.insurance_accounts.joins(:insurance_type).where("insurance_types.code = ?", "LIF").first.balance    
  end

  def retirement_fund
    self.insurance_accounts.joins(:insurance_type).where("insurance_types.code = ?", "RF").first.balance    
  end

  def sss_number_details
    gov_identification_type = 10
    id_split = self.sss_number.split("-").join("").to_s 
    id_number = id_split.match(/\d+/).to_s
    if id_number.length == gov_identification_type

     "#{id_number}"
    else

    
     "Invalid"
    end



  end

  def tin_number_details

    #gov_identification_type = 10 
    id_split = self.tin_number.split("-").join("").to_s 
    id_number = id_split.match(/\d+/).to_s
    if id_number.length == 9 || id_number.length == 12

     "#{id_number}"
    else

     "Invalid"
    end


  end

  def phil_health_number_details
    gov_identification_type = 13 
    id_split = self.phil_health_number.split("-").join("").to_s 
    id_number = id_split.match(/\d+/).to_s
    if id_number.length == gov_identification_type

     "#{id_number}"
    else

     "Invalid"
    end
  end






end
