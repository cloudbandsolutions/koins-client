class FiscalYearAccountingCode < ApplicationRecord
  belongs_to :accounting_code

  validates :year, presence: true, uniqueness: { scope: :accounting_code_id }
  validates :accounting_code, presence: true
  validates :beginning_balance, presence: true, numericality: true
  validates :ending_balance, presence: true, numericality: true
  validates :beginning_debit, presence: true, numericality: true
  validates :beginning_credit, presence: true, numericality: true
  validates :ending_debit, presence: true, numericality: true
  validates :ending_credit, presence: true, numericality: true
end
