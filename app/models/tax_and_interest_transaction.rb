class TaxAndInterestTransaction < ApplicationRecord
  belongs_to :savings_account
  belongs_to :monthly_interest_and_tax_closing_record

  validates :savings_account, presence: true
  #validates :monthly_interest_and_tax_closing_record, presence: true
  validates :tax_amount, presence: true, numericality: true
  validates :interest_amount, presence: true, numericality: true
  validates :uuid, presence: true, uniqueness: true
  validates :monthly_tax_rate, presence: true, numericality: true
  validates :monthly_interest_rate, presence: true, numericality: true
  validates :annual_interest_rate, presence: true, numericality: true
  validates :beginning_balance, presence: true, numericality: true
  validates :ending_balance, presence: true, numericality: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid

      if !self.savings_account.nil? and !self.interest_amount.nil? and !self.tax_amount.nil?
        self.beginning_balance = self.savings_account.balance
        self.ending_balance = self.beginning_balance + self.interest_amount - self.tax_amount
      end
    end
  end
end
