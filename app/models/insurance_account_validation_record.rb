class InsuranceAccountValidationRecord < ApplicationRecord
	MEMBER_CLASSIFICATION = ["RESIGNED", "DECEASED", "EXIT AGE (Cash)", "EXIT AGE (GK)"]
	
	belongs_to :member
 	belongs_to :insurance_account_validation

	validates :member, presence: true
	validates :resignation_date, presence: true
  	validates :insurance_account_validation, presence: true

 	before_validation :load_defaults

  	after_save do
    	insurance_account_validation.touch
  	end

	def load_defaults
		if self.new_record?
	  		self.status = "pending"
		end
	end

	def pending?
		self.status == "pending"
	end

	def approved?
		self.status == "approved"
	end

	def is_void?
		self.is_void == true
	end

	def to_version_2_hash
	    if self.uuid.blank?
	      self.update!(uuid: "#{SecureRandom.uuid}")
	    end
		    {
		      id: self.uuid,
		      insurance_account_validation_id: self.insurance_account_validation.uuid,
		      member_id: self.member.uuid,
		      center_id: self.member.center.uuid,
		      status: self.status, 
		      transaction_number: self.transaction_number,
		      rf: self.rf,
		      lif_50_percent: self.lif_50_percent,
		      advance_lif: self.advance_lif,
		      advance_rf: self.advance_rf,
		      interest: self.interest,
		      total: self.total,
		      created_at: self.created_at,
		      updated_at: self.updated_at,
		      resignation_date: self.resignation_date,
		      member_classification: self.member_classification,
		      equity_interest: self.equity_interest,
		      is_void: self.is_void
		    } 
  	end
end