class MemberWithdrawal < ApplicationRecord
  STATUSES  = ["pending", "approved"]
  belongs_to :member

  validates :uuid, presence: true, uniqueness: true
  validates :reference_number, presence: true, if: :approved?
  validates :paid_at, presence: true
  validates :book, presence: true, if: :approved?
  validates :prepared_by, presence: true
  validates :approved_by, presence: true, if: :approved?
  validates :total_withdrawals, presence: true, numericality: true
  validates :total_payments, presence: true, numericality: true

  has_many :member_withdrawal_loans, dependent: :delete_all
  accepts_nested_attributes_for :member_withdrawal_loans

  has_many :member_withdrawal_fund_balances, dependent: :delete_all
  accepts_nested_attributes_for :member_withdrawal_fund_balances

  def pending?
    self.status == "pending"
  end

  def approved?
    self.status == "approved"
  end

  def load_defaults
    if self.new_record?
      self.status = 'pending'
      self.uuid = SecureRandom.uuid
    end
  end
end
