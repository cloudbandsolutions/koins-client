class LoanPaymentAdjustmentRecord < ApplicationRecord
  belongs_to :loan_payment_adjustment
  belongs_to :loan_payment

  validates :new_amount, presence: true, numericality: true
  validates :loan_payment, presence: true
end
