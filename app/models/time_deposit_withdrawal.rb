class TimeDepositWithdrawal < ApplicationRecord
  belongs_to :savings_account


  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
      self.status = "pending"
    end

    if self.status.nil?
      self.status = "pending"
    end
  
  end
end
