class LoanDeductionEntry < ApplicationRecord
  belongs_to :loan_product
  belongs_to :loan_deduction
  belongs_to :accounting_code

  validates :accounting_code, presence: true
  validates :val, presence: true, numericality: true
  validates :loan_deduction, presence: true
  validates :t_val_15, presence: true, numericality: true
  validates :t_val_25, presence: true, numericality: true
  validates :t_val_35, presence: true, numericality: true
  validates :t_val_50, presence: true, numericality: true
end
