class MonthlyTrialBalance < ApplicationRecord
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :data, presence: true
end
