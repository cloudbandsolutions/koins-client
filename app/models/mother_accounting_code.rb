class MotherAccountingCode < ApplicationRecord
  belongs_to :major_account

  has_many :accounting_code_categories

  validates :name, presence: true, uniqueness: true
  validates :subcode, presence: true

  before_validation :load_defaults

  def load_defaults
    # build the code from major_group + major_account
    if !self.major_account.nil? and !self.subcode.nil?
      self.code = "#{self.major_account.major_group.code}#{self.major_account.code}#{self.subcode}"
    end
    self.code = "#{self.major_account.major_group.code}#{self.major_account.code}#{self.subcode}"
  end

  def to_s
    name
  end

  def to_s_trial_balance
    "#{code} - #{name}"
  end
end
