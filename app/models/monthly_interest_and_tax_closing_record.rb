class MonthlyInterestAndTaxClosingRecord < ApplicationRecord
  STATUSES = %w(pending approved void)

  validates :total_interest, presence: true, numericality: true
  validates :total_tax, presence: true, numericality: true
  validates :month, presence: true
  validates :closed_at, presence: true
  validates :closing_date, presence: true

  belongs_to :branch

  has_many :tax_and_interest_transactions, dependent: :delete_all

  before_validation :load_defaults

  scope :pending, -> { where(status: 'pending') }
  scope :approved, -> { where(status: 'approved') }
  scope :void, -> { where(status: 'void') }

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
      self.closed_at = Time.now
      self.closing_date = closed_at if self.closing_date.nil?
      self.year = self.closing_date.year
      self.month = self.closing_date.month
      self.status = "pending"
    end
  end

  def pending?
    self.status == 'pending'
  end

  def approved?
    self.status == 'approved'
  end

  def void?
    self.status == 'void'
  end
end
