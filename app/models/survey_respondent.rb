class SurveyRespondent < ApplicationRecord
  belongs_to :member

  belongs_to :survey

  has_many :survey_respondent_answers, dependent: :destroy
  accepts_nested_attributes_for :survey_respondent_answers

  validates :date_answered, presence: true

  before_validation :load_defaults
  
  def total_score
    survey_respondent_answers.joins(:survey_question_option).sum("survey_question_options.score")
  end

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
    end
  end
end
