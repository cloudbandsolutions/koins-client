class RegularDepositInterval < ApplicationRecord
  has_many :regular_deposit_amounts, dependent: :destroy
  accepts_nested_attributes_for :regular_deposit_amounts

  validates :name, presence: true, uniqueness: true
  validates :interval, presence: true, numericality: true, uniqueness: true

  def num_entries
    regular_deposit_amounts.count
  end

  def to_s
    name
  end
end
