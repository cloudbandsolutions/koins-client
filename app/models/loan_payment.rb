class LoanPayment < ApplicationRecord
  require 'application_helper'

  PAYMENT_TYPES = ["interest", "principal"]
  STATUSES = ["pending", "approved", "reversed"]

  belongs_to :loan
  belongs_to :co_maker, class_name: 'Member', foreign_key: :co_maker_id
  belongs_to :savings_account
  belongs_to :co_maker_savings_account, class_name: 'SavingsAccount', foreign_key: :co_maker_savings_account_id
  belongs_to :deposit_savings_account, class_name: 'SavingsAccount', foreign_key: :deposit_savings_account_id
  belongs_to :collection_payment, dependent: :destroy

  has_many :savings_account_transactions
  has_many :loan_payment_amortization_schedule_entries
  has_many :wp_records, dependent: :destroy
  has_many :loan_payment_insurance_account_deposits

  accepts_nested_attributes_for :wp_records
  accepts_nested_attributes_for :savings_account_transactions, allow_destroy: true
  accepts_nested_attributes_for :loan_payment_insurance_account_deposits, allow_destroy: true

  validates :uuid, presence: true, uniqueness: true
  validates :paid_at, presence: true
  validates :cp_amount, presence: true, numericality: true
  validates :amount, presence: true, numericality: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :paid_interest, presence: true, numericality: true
  validates :paid_principal, presence: true, numericality: true
  validates :loan_payment_amount, numericality: true, presence: true, loan_payment_amount_greater_than_remaining_balance: true
  validates :wp_amount, numericality: true, withdraw_payment_amount: true, if: :has_wp_amount?
  validates :particular, presence: true, if: :has_cp_amount?
  validates :wp_particular, presence: true, if: :has_wp_amount?
  validates :deposit_amount, numericality: true, if: :has_deposit_amount?

  #validate :insurance_deposit_less_than_amount
  validate :wp_amount, :has_withdrawal_funds, if: :has_wp_amount?

  before_validation :load_defaults

  scope :approved, -> { where(status: "approved") }
  scope :pending, -> { where(status: "pending") }

  def to_version_2_hash
    new_uuid      = "#{SecureRandom.uuid}"
    amort_entries = build_amort_entries

    total_interest_paid   = 0.00
    total_principal_paid  = 0.00

    amort_entries.each do |o|
      total_principal_paid += o[:principal_paid]
      total_interest_paid += o[:interest_paid]
    end

    total_principal_paid  = total_principal_paid.round(2)
    total_interest_paid   = total_interest_paid.round(2)

    {
      id: new_uuid,
      subsidiary_id: self.loan.uuid,
      subsidiary_type: "Loan",
      amount: self.loan_payment_amount,
      transaction_type: "loan_payment",
      status: self.status,
      transacted_at: self.paid_at,
      data: {
        old_uuid: self.uuid,
        amort_entries: build_amort_entries,
        total_principal_paid: total_principal_paid,
        total_interest_paid: total_interest_paid,
        amount_due: self.loan_payment_amount,
        particular: "",
        approved_by: "SYSTEM"
      }
    }
  end

  def build_amort_entries
    amort_entries = []

    lpas  = LoanPaymentAmortizationScheduleEntry.where(
              loan_payment_id: self.id
            )

    lpas.each do |o|
      amort_entries << {
        id: o.ammortization_schedule_entry.uuid,
        due_date: o.ammortization_schedule_entry.due_at.try(:to_date),
        principal_paid: o.principal_paid,
        interest_paid: o.interest_paid
      }
    end

    amort_entries
  end

  def to_s
    "#{self.paid_at.strftime("%b %d, %Y")} - #{self.loan.member.full_name_titleize} - #{self.amount}"
  end

  def approved?
    self.status == "approved"
  end

  def reversed?
    self.status == "reversed"
  end

  def valid_for_reversal?
    valid = true
    self.savings_account_transactions.each do |savings_account_transaction|
      if !savings_account_transaction.valid_for_reversal?
        valid = false
      end
    end

    valid
  end

  def has_withdrawal_funds
    #savings_account = self.loan.member.savings_accounts.where("savings_type_id = ?", self.loan.loan_product.savings_type.id).first
    result = self.savings_account.balance - self.wp_amount
    if result < self.savings_account.maintaining_balance
      errors.add(:loan_payment_amount, "member's savings is below maintaining balance")
    end
  end

  def has_insurance_deposit?
    sum = 0
    self.loan_payment_insurance_account_deposits.each do |deposit|
      sum += deposit.amount
    end

    sum > 0 ? true : false
  end

  def is_not_part_of_collection?
    self.collection_payment_id.nil? || self.collection_payment.nil? ? true : false
  end
  
  def is_part_of_collection?
    !self.collection_payment_id.nil? || !self.collection_payment.nil? ? true : false
  end

  def pending?
    self.status == 'pending' ? true : false
  end

  def no_payment?
    l_total_amount = self.cp_amount

    if self.has_wp_amount?
      l_total_amount += self.wp_amount
    end

    r_total_amount = 0.00

    if self.has_deposit_amount?
      r_total_amount += self.deposit_amount
    end

    if self.has_insurance_deposit?
      r_total_amount +=  self.total_insurance_deposit
    end

    if self.has_loan_payment_amount?
      r_total_amount += self.loan_payment_amount
    end

    l_total_amount == 0.00 and r_total_amount == 0.00 ? true : false
  end

  # Should have more funds for paying than insurance deposit
  def insurance_deposit_less_than_amount
    total_amount = self.cp_amount

    if self.has_wp_amount?
      total_amount += self.wp_amount
    end

    if self.total_insurance_deposit > total_amount
      errors.add(:insurance_particular, "Not enough funds for insurance: Total amount: #{total_amount} Total insurance deposit: #{total_insurance_deposit}")  
    end
  end

  def total_insurance_deposit
    sum = 0
    self.loan_payment_insurance_account_deposits.each do |deposit|
      sum += deposit.amount
    end

    sum
  end

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
      self.status = "pending" if self.status.nil?
      self.paid_principal = 0 if self.paid_principal.nil?
      self.paid_interest = 0 if self.paid_interest.nil?
    end

    self.wp_amount ||= 0.00
    self.loan_payment_amount ||= 0.00

    self.deposit_amount = 0.00.to_f
    self.savings_account_transactions.each_with_index do |savings_account_transaction, i|
      if savings_account_transaction.transaction_type == "deposit"
        self.deposit_amount += savings_account_transaction.amount.to_f
      end
    end

    # Set the default savings account of user (for WP)
    default_savings_account = SavingsAccount.joins(:savings_type).where("savings_types.id = ? AND savings_types.is_default = ? AND savings_accounts.member_id = ?", self.loan.loan_product.savings_type.id, true, self.loan.member.id).first
    if self.savings_account.nil?
      self.savings_account = default_savings_account
    end

    if self.cp_amount.nil? || self.cp_amount.blank?
      self.cp_amount = 0.00
    end 
   
    # Compute for amount
    total_amount = 0
    
    # Cash
    total_amount += self.cp_amount

    # Withdraw Payment
    if self.has_wp_amount?
      total_amount += self.wp_amount
    end

    self.amount = total_amount

    if !self.paid_at.nil?
      self.wp_particular = "Withdrawal payments #{self.paid_at.strftime("%b %d %Y")}"
    end
  end

  def has_deposit_amount?
    if self.deposit_amount.nil? or self.deposit_amount == 0
      false
    else
      true
    end
  end

  def has_wp_amount?
    self.wp_amount.nil? || self.savings_account.nil? || (self.wp_amount == 0) ? false : true
  end

  def has_cp_amount?
    self.cp_amount.nil? || (self.cp_amount == 0) ? false : true
  end

  def has_loan_payment_amount?
    self.loan_payment_amount.nil? || (self.loan_payment_amount == 0) ? false : true
  end

  def payment_stats
    temp_amount             = loan_payment_amount
    stats                   = { principal: 0, interest: 0, ase: [], loan_product: loan.loan_product }
    #ammortization_schedule  = AmmortizationScheduleEntry.where(loan_id: loan.id, paid: false).order(:created_at)
    ammortization_schedule  = loan.ammortization_schedule_entries.where(paid: [nil, false], is_void: nil).order(:due_at)

    ammortization_schedule.each do |as|
      if temp_amount > 0
        ase_paid_principal = 0.00
        ase_paid_interest = 0.00

        if Settings.try(:loan_payments_principal_first) == true
          if as.principal_paid?
            remaining_interest = as.remaining_interest

            if temp_amount <= remaining_interest
              as.paid_interest += temp_amount

              if temp_amount == remaining_interest
                as.paid = true
              end

              stats[:interest] += temp_amount
              ase_paid_principal += temp_amount

              temp_amount = 0
            else
              as.paid_interest += remaining_interest
              as.paid = true

              stats[:interest] += remaining_interest
              ase_paid_interest += remaining_interest

              temp_amount -= remaining_interest
            end
          else
            remaining_principal = as.remaining_principal

            if temp_amount <= remaining_principal
              as.paid_principal += temp_amount
              
              stats[:principal] += temp_amount
              ase_paid_principal += temp_amount

              temp_amount = 0
            else
              as.paid_principal += remaining_principal

              stats[:principal] += remaining_principal
              ase_paid_principal += remaining_principal

              temp_amount -= remaining_principal

              remaining_interest = as.remaining_interest

              if temp_amount <= remaining_interest
                as.paid_interest += temp_amount

                if temp_amount == remaining_interest
                  as.paid = true
                end

                stats[:interest] += temp_amount
                ase_paid_interest += temp_amount

                temp_amount = 0
              else
                as.paid_interest += remaining_interest
                as.paid = true

                stats[:interest] += remaining_interest
                ase_paid_interest += remaining_interest

                temp_amount -= remaining_interest
              end
            end
          end
        else
          if as.interest_paid?
            remaining_principal = as.remaining_principal

            if temp_amount <= remaining_principal
              as.paid_principal += temp_amount

              if temp_amount == remaining_principal
                as.paid = true
              end

              stats[:principal] += temp_amount
              ase_paid_principal += temp_amount

              temp_amount = 0
            else
              as.paid_principal += remaining_principal
              as.paid = true

              stats[:principal] += remaining_principal
              ase_paid_principal += remaining_principal

              temp_amount -= remaining_principal
            end
          else
            remaining_interest = as.remaining_interest

            if temp_amount <= remaining_interest
              as.paid_interest += temp_amount

              stats[:interest] += temp_amount
              ase_paid_interest += temp_amount

              temp_amount = 0
            else
              as.paid_interest += remaining_interest

              stats[:interest] += remaining_interest
              ase_paid_interest += remaining_interest

              temp_amount -= remaining_interest

              remaining_principal = as.remaining_principal

              if temp_amount <= remaining_principal
                as.paid_principal += temp_amount

                if temp_amount == remaining_principal
                  as.paid = true
                end

                stats[:principal] += temp_amount
                ase_paid_principal += temp_amount

                temp_amount = 0
              else
                as.paid_principal += remaining_principal
                as.paid = true

                stats[:principal] += remaining_principal
                ase_paid_principal += remaining_principal

                temp_amount -= remaining_principal
              end
            end
          end
        end

        stats[:ase] << { ase: as, principal_paid: ase_paid_principal, interest_paid: ase_paid_interest }
      end
    end

    stats
  end

  def approve_payment!
    ammortization_schedule =  AmmortizationScheduleEntry.unpaid.where(
                                loan_id: loan.id,
                                is_void: nil
                              ).order("due_at ASC")
    temp_amount = self.loan_payment_amount
    stats = { principal: 0, interest: 0, ase: [], loan_product: self.loan.loan_product }

    ammortization_schedule.each do |as|
      if temp_amount > 0
        ase_paid_principal = 0.00
        ase_paid_interest = 0.00

        if Settings.try(:loan_payments_principal_first) == true
          if as.principal_paid?
            remaining_interest = as.remaining_interest

            if temp_amount <= remaining_interest
              as.paid_interest += temp_amount

              if temp_amount == remaining_interest
                as.paid = true
              end

              stats[:interest] += temp_amount
              ase_paid_interest += temp_amount

              temp_amount = 0
            else
              as.paid_interest += remaining_interest
              as.paid = true

              stats[:interest] += remaining_interest
              ase_paid_interest += remaining_interest

              temp_amount -= remaining_interest
            end
          else
            remaining_principal = as.remaining_principal

            if temp_amount <= remaining_principal
              as.paid_principal += temp_amount

              stats[:principal] += temp_amount
              ase_paid_principal += temp_amount

              temp_amount = 0
            else
              as.paid_principal += remaining_principal

              stats[:principal] += remaining_principal
              ase_paid_principal += remaining_principal

              temp_amount -= remaining_principal
              remaining_interest = as.remaining_interest

              if temp_amount <= remaining_interest
                as.paid_interest += temp_amount

                if temp_amount == remaining_interest
                  as.paid = true
                end

                stats[:interest] += temp_amount
                ase_paid_interest += temp_amount

                temp_amount = 0
              else
                as.paid_interest += remaining_interest
                as.paid = true

                stats[:interest] += remaining_interest
                ase_paid_interest += remaining_interest

                temp_amount -= remaining_interest
              end
            end
          end
        else
          if as.interest_paid?
            remaining_principal = as.remaining_principal

            if temp_amount <= remaining_principal
              as.paid_principal += temp_amount

              if temp_amount == remaining_principal
                as.paid = true
              end

              stats[:principal] += temp_amount
              ase_paid_principal += temp_amount

              temp_amount = 0
            else
              as.paid_principal += remaining_principal
              as.paid = true

              stats[:principal] += remaining_principal
              ase_paid_principal += remaining_principal

              temp_amount -= remaining_principal
            end
          else
            remaining_interest = as.remaining_interest

            if temp_amount <= remaining_interest
              as.paid_interest += temp_amount

              stats[:interest] += temp_amount
              ase_paid_interest += temp_amount

              temp_amount = 0
            else
              as.paid_interest += remaining_interest

              stats[:interest] += remaining_interest
              ase_paid_interest += remaining_interest

              temp_amount -= remaining_interest
              remaining_principal = as.remaining_principal

              if temp_amount <= remaining_principal
                as.paid_principal += temp_amount

                if temp_amount == remaining_principal
                  as.paid = true
                end

                stats[:principal] += temp_amount
                ase_paid_principal += temp_amount

                temp_amount = 0
              else
                as.paid_principal += remaining_principal
                as.paid = true

                stats[:principal] += remaining_principal
                ase_paid_principal += remaining_principal

                temp_amount -= remaining_principal
              end
            end
          end
        end

        stats[:ase] << { ase: as, principal_paid: ase_paid_principal, interest_paid: ase_paid_interest }
      end
    end

    c_working_date = ApplicationHelper.current_working_date

    self.update!(paid_at: c_working_date, status: "approved", paid_interest: stats[:interest], paid_principal: stats[:principal])

    stats[:ase].each do |a|
      if a[:ase].is_fully_paid?
        a[:ase].paid = true
      end

      a[:ase].save!
      LoanPaymentAmortizationScheduleEntry.create!(
        loan_payment: self, 
        ammortization_schedule_entry: a[:ase], 
        principal_paid: a[:principal_paid], 
        interest_paid: a[:interest_paid]
      )
    end

    # update loan remaining balance
    next_remaining_balance = loan.remaining_balance - self.loan_payment_amount

    if next_remaining_balance == 0
      loan.update!(remaining_balance: next_remaining_balance, status: "paid")
    else
      loan.update!(remaining_balance: next_remaining_balance)
    end

    stats
  end
end
