class Voucher < ApplicationRecord
  BOOK_TYPES = %w(JVB CRB CDB MISC)
  STATUSES = %w(pending approved rejected)
  VOUCHER_OPTIONS = ["Bank Check Number", "Date Requested", "Bank", "Check Amount", "Check Voucher Number", "Check Name", "Name on Check", "Others"]
  ACCOUNTING_FUNDS = ["Mutual Benefit Fund", "General Fund",  "Optional Fund"]

  belongs_to :branch
  belongs_to :accounting_fund

  has_many :voucher_options
  accepts_nested_attributes_for :voucher_options

  has_many :journal_entries, dependent: :destroy
  accepts_nested_attributes_for :journal_entries

  validates :uuid, presence: true, uniqueness: true
  validates :date_prepared, presence: true
  validates :particular, presence: true
  validates :book, presence: true, inclusion: { in: BOOK_TYPES }
#  validates :master_reference_number, presence: true, uniqueness: true
  validates :branch, presence: true
  validates :or_number, presence: true, if: :crb?
  validate :should_have_journal_entries
  #validate :journal_entry_uniqueness
  validate :non_negative_values
  validate :balanced_entries

  before_validation :load_defaults

  before_destroy :destroy_related_records

  scope :approved_disbursements,  -> { where(status: 'approved', book: 'CDB').order('date_prepared ASC') }
  scope :approved_receipts,       -> { where(status: 'approved', book: 'CRB').order('date_prepared ASC') }
  scope :disbursments,            -> { where(book: 'CDB') }
  scope :approved,                -> { where(status: 'approved') }
  scope :pending,                 -> { where(status: 'pending') }
  scope :with_checks,             -> { where.not(check_number: "") }
  scope :approved_with_year_end,  -> { where(status: 'approved', is_year_end_closing: true) }

  scope :approved_book_vouchers,  ->(book) { where(status: 'approved', book: book) }

  def cash_in_bank
  end

  def to_version_2_hash
    {
      id: self.uuid,
      date_prepared: self.date_prepared,
      date_posted: self.date_posted,
      branch_id: self.branch.uuid,
      book: self.book,
      reference_number: self.reference_number,
      particular: self.particular,
      approved_by: self.approved_by,
      prepared_by: self.prepared_by,
      status: self.status,
      accounting_fund_id: self.accounting_fund.try(:uuid),
      data: {
        or_number: self.or_number,
        check_number: self.check_number,
        check_voucher_number: self.check_voucher_number,
        date_of_check: self.date_of_check,
        sub_reference_number: self.sub_reference_number,
        payee: self.payee
      }
    }
  end

  def transaction_type
    payment_collection = PaymentCollection.where(reference_number: self.reference_number).first

    t = nil
    if payment_collection
      t = payment_collection.payment_type.upcase
    end

    t
  end

  def get_center
    payment_collection = PaymentCollection.where(reference_number: self.reference_number).first
    center = nil

    if payment_collection
      center = payment_collection.center
    end

    center
  end

  def amount
    credit_amount
  end

  def credit_amount
    self.journal_entries.where(post_type: 'CR').sum(:amount)
  end

  def crb?
    self.book == "CRB"
  end

   def cdb? 
    self.book == "CDB"
  end

  def jvb?
    self.book == "JVB"
  end

  def should_have_journal_entries
    if self.journal_entries.size < 2
      errors.add(:particular, "should have at least two journal entries")
    end
  end

  def journal_entry_uniqueness
    if self.journal_entries.map(&:accounting_code_id).count != self.journal_entries.map(&:accounting_code_id).uniq.count
      errors.add(:reference_number, "duplicate entries found #{self.journal_entries.join(' ').capitalize << '.'}")
      errors.add(:particular, "duplicate entries found #{self.journal_entries.join(' ').capitalize << '.'}")
    end
  end

  def non_negative_values
    self.journal_entries.each do |journal_entry|
      temp_amount = journal_entry.amount ||= 0.00
      if temp_amount < 0
        errors.add(:reference_number, "Negative amount for entry #{journal_entry.uuid} - #{journal_entry.amount.to_f} - #{journal_entry.accounting_code}")
      end
    end
  end

  def balanced_entries
    dr_sum = journal_entries.map { |assoc| (assoc.post_type == 'DR') ? assoc.amount : 0 }.inject{ |dr_sum, x| dr_sum + x }
    cr_sum = journal_entries.map { |assoc| (assoc.post_type == 'CR') ? assoc.amount : 0 }.inject{ |cr_sum, x| cr_sum + x }

    if dr_sum != cr_sum
      errors.add(:reference_number, "unbalanced entry: DR Amount = #{dr_sum.to_f} CR Amount = #{cr_sum.to_f}")
      errors.add(:particular, "unbalanced entry: DR Amount = #{dr_sum.to_f} CR Amount = #{cr_sum.to_f}")
    end
  end

  def pending?
    self.status == 'pending'
  end

  def approved?
    self.status == 'approved'
  end

  def self.book_vouchers(book)
    Voucher.where(book: book).order("date_prepared ASC")
  end

  def total_debit
    self.journal_entries.where(post_type: "DR").sum(:amount).to_f
  end

  def total_credit
    self.journal_entries.where(post_type: "CR").sum(:amount).to_f
  end

  def destroy_related_records
    # Destroy loans
    loan = Loan.where(voucher_reference_number: self.reference_number).first

    if !loan.blank?
      loan.destroy!
    end
  end

  def debit_credit_amounts
    result = { debit: 0.00, credit: 0.00 }
    self.journal_entries.each do |journal_entry|
      result[:debit] += journal_entry.amount if journal_entry.post_type == 'DR'
      result[:credit] += journal_entry.amount if journal_entry.post_type == 'CR'
    end

    result
  end

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
      self.status = "pending" if self.status.nil?
      self.master_reference_number = "#{branch.try(:code)}#{self.book}#{reference_number}-#{Time.now.to_i.to_s}"
    end

  end

  def total_debit
    amount = 0
    self.journal_entries.each do |je|
      if je.post_type == 'DR'
        amount += je.amount
      end
    end

    amount
  end

  def panel_change 
   if status == "pending" 
    "panel panel-danger"
   elsif status == "approved"
    "panel panel-info"
   else
    "panel panel-danger"
   end
  end

  def total_credit
    amount = 0
    self.journal_entries.each do |je|
      if je.post_type == 'CR'
        amount += je.amount
      end
    end

    amount
  end
end
