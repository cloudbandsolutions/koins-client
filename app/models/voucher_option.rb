class VoucherOption < ApplicationRecord
  belongs_to :voucher

  validates :name, presence: true
  validates :val, presence: true

  before_validation :load_defaults

  scope :with_checks, -> { where(name: "Bank Check Number") }

  def load_defaults
    self.val = "DEFAULT" if val.nil?
  end

  def to_s
    name
  end
end
