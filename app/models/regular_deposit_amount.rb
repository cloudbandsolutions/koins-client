class RegularDepositAmount < ApplicationRecord
  belongs_to :regular_deposit_interval

  validates :min_amount, presence: true, numericality: true
  validates :max_amount, presence: true, numericality: true
  validates :deposit_amount, presence: true, numericality: true
end
