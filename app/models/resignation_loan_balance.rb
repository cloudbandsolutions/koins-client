class ResignationLoanBalance < ApplicationRecord
  belongs_to :loan
  belongs_to :member_resignation

  has_many :resignation_loan_balance_payments, dependent: :destroy
  accepts_nested_attributes_for :resignation_loan_balance_payments

  validates :uuid, presence: true, uniqueness: true
  validates :balance, presence: true, numericality: true
  validates :interest_balance, presence: true, numericality: true
  validates :principal_balance, presence: true, numericality: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
    end
  end
end
