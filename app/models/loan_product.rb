class LoanProduct < ApplicationRecord
  belongs_to :amount_released_accounting_code, class_name: "AccountingCode", foreign_key: "amount_released_accounting_code_id"
  belongs_to :accounting_code_transaction, class_name: "AccountingCode", foreign_key: "accounting_code_transaction_id"
  belongs_to :accounting_code_processing_fee, class_name: "AccountingCode", foreign_key: "accounting_code_processing_fee_id"
  belongs_to :interest_accounting_code, class_name: "AccountingCode", foreign_key: "interest_accounting_code_id"
  belongs_to :writeoff_dr_accounting_code, class_name: "AccountingCode", foreign_key: "writeoff_dr_accounting_code_id"
  belongs_to :writeoff_cr_accounting_code, class_name: "AccountingCode", foreign_key: "writeoff_cr_accounting_code_id"
  belongs_to :savings_type

  has_many :loan_deduction_entries
  accepts_nested_attributes_for :loan_deduction_entries, allow_destroy: true
  has_many :loan_insurance_deduction_entries
  accepts_nested_attributes_for :loan_insurance_deduction_entries, allow_destroy: true
  has_many :loan_product_types
  accepts_nested_attributes_for :loan_product_types, allow_destroy: true
  has_many :loan_product_dependencies
  accepts_nested_attributes_for :loan_product_dependencies, allow_destroy: true
  has_many :loan_product_ep_cycle_count_max_amounts
  accepts_nested_attributes_for :loan_product_ep_cycle_count_max_amounts, allow_destroy: true
  has_many :loan_product_membership_payments
  accepts_nested_attributes_for :loan_product_membership_payments, allow_destroy: true

  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  validates :accounting_code_transaction, presence: true
  validates :interest_accounting_code, presence: true
  validates :writeoff_dr_accounting_code, presence: true
  validates :writeoff_cr_accounting_code, presence: true
  validates :next_application_interval, presence: true, numericality: true
  validates :savings_type, presence: true
  validates :default_maintaining_balance_val, presence: true, numericality: true

  validate :unique_accounting_entries_for_wp_and_insurance_deposits
  validate :unique_loan_deductions
  validate :unique_loan_deduction_accounting_codes
  validate :unique_loan_insurance_deduction_entry_accounting_codes
  validate :unique_insurance_types

  default_scope { order(:priority) }
  before_validation :load_defaults

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    {
      id: self.uuid,
      name: self.name,
      max_loan_amount:  self.max_loan_amount || 0.00,
      min_loan_amount:  5000,
      denomination: 1000,
      insured: self.insured,
      is_entry_point: self.is_entry_point,
      monthly_interest_rate: 0.05
    }
  end

  def membership_types
    MembershipType.where(id: self.loan_product_membership_payments.pluck(:membership_type_id))
  end

  def membership_type_names
    MembershipType.where(id: self.loan_product_membership_payments.pluck(:membership_type_id)).pluck(:name)
  end

  def has_dependent_loan_product?
    self.dependent_loan_product.nil? ? false :true
  end

  def unique_accounting_entries_for_wp_and_insurance_deposits
    loan_deduction_accounting_codes = self.loan_deduction_entries.map(&:accounting_code_id)
    loan_insurance_deduction_accounting_codes = self.loan_insurance_deduction_entries.map(&:accounting_code_id)

    intersection = loan_deduction_accounting_codes & loan_insurance_deduction_accounting_codes

    if intersection.size > 0
      errors.add(:name, "duplicate accounting codes detected for loan deduction and insurance deposits")
    end
  end
  
  def unique_loan_deductions
    if self.loan_deduction_entries.map(&:loan_deduction_id).size != self.loan_deduction_entries.map(&:loan_deduction_id).uniq.size
      errors.add(:name, "duplicate loan deductions detected")
    end
  end

  def unique_loan_deduction_accounting_codes
    if self.loan_deduction_entries.map(&:accounting_code_id).size != self.loan_deduction_entries.map(&:accounting_code_id).uniq.size
      errors.add(:name, "duplicate accounting codes detected for loan deductions")
    end
  end

  def unique_loan_insurance_deduction_entry_accounting_codes
    if self.loan_insurance_deduction_entries.map(&:accounting_code_id).size != self.loan_insurance_deduction_entries.map(&:accounting_code_id).uniq.size
      errors.add(:name, "duplicate accounting codes detected for loan insurance deductions")
    end
  end

  def is_entry_level?
    is_entry_point
  end
  
  def unique_insurance_types
    #raise loan_insurance_deduction_entries.map(&:insurance_type_id).inspect
    if self.loan_insurance_deduction_entries.map(&:insurance_type_id).size != self.loan_insurance_deduction_entries.map(&:insurance_type_id).uniq.size
      errors.add(:name, "duplicate insurance types detected")
    end
  end

  def load_defaults
    if self.max_loan_amount.nil?
      self.max_loan_amount = 0.00
    end
  end

  def to_s
    name
  end
end
