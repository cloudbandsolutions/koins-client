class PaymentCollectionRecord < ApplicationRecord
  belongs_to :member
  belongs_to :payment_collection
  belongs_to :loan_product

  validates :member, presence: true
  #validates :loan_product, presence: true
  validates :uuid, presence: true, uniqueness: true
  validates :total_cp_amount, presence: true, numericality: true
  validates :total_wp_amount, presence: true, numericality: true
  validates :total_principal_amount, presence: true, numericality: true
  validates :total_interest_amount, presence: true, numericality: true

  has_many :collection_transactions, dependent: :destroy
  accepts_nested_attributes_for :collection_transactions

  before_validation :load_defaults

  after_save do
    payment_collection.touch
  end

  def to_version_2_billing_hash
    hash  = {
      id: self.uuid,
      total_amount_received: (self.total_cp_amount + self.total_wp_amount),
      member_id: self.member.uuid,
      account_transaction_collection_id: self.payment_collection.uuid,
      data: {
        total_cp_amount: self.total_cp_amount,
        total_wp_amount: self.total_wp_amount,
        total_principal_amount: self.total_principal_amount,
        total_interest_amount: self.total_interest_amount,
        loan_product_id: self.loan_product.uuid,
        transactions: []
      }
    }

    self.collection_transactions.each do |o|
      hash[:data][:transactions] << {
        id: o.uuid,
        amount: o.amount,
        data: {
          loan_product_id: self.loan_product.uuid,
          account_type: o.account_type,
          account_type_code: o.account_type_code
        }
      }
    end

    hash
  end

  def loan_payment_ids
    ct = collection_transactions.where(account_type: "LOAN_PAYMENT").pluck(:loan_payment_id)
  end

  def pending_loan_payment?
    ct = collection_transactions.where(account_type: "LOAN_PAYMENT").first
    if ct
      if ct.loan_payment
        if ct.loan_payment.status == 'pending'
          true
        else
          false
        end
      else
        false
      end
    else
      false
    end
  end

  def loan_payment
    ct = collection_transactions.where(account_type: "LOAN_PAYMENT").first

    ct.loan_payment
  end

  def lp_amount_by_member
    ct = collection_transactions.where(account_type: "LOAN_PAYMENT").first
    ct.nil? ? 0.00 : ct.amount
  end

  def wp_id
    ct = collection_transactions.where(account_type: "WP").first
    ct.nil? ? "" : ct.id
  end

  def ct_id_for_insurance(insurance_type)
    ct  = collection_transactions.where(account_type: "INSURANCE", account_type_code: insurance_type.code).first
    ct.nil? ? "" : ct.id
  end

  def ct_id_for_savings(savings_type)
    ct = collection_transactions.where(account_type: "SAVINGS", account_type_code: savings_type.code).first
    ct.nil? ? "" : ct.id
  end

  def savings_by_type(savings_type)
    ct = collection_transactions.where(account_type: "SAVINGS", account_type_code: savings_type.code).first
    ct.nil? ? 0.00 : ct.amount
  end

  def insurance_by_type(insurance_type)
    ct = collection_transactions.where(account_type: "INSURANCE", account_type_code: insurance_type.code).first
    ct.nil? ? 0.00 : ct.amount
  end

  def equity_by_type(equity_type)
    ct = collection_transactions.where(account_type: "EQUITY", account_type_code: equity_type.code).first
    ct.nil? ? 0.00 : ct.amount
  end

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
    end
   
    if !self.payment_collection.nil?
      compute_values if self.payment_collection.pending?
    else
      compute_values
    end
  end

  def has_entry_point_loans?
    entry_point_loan_product_ids = LoanProduct.where(is_entry_point: true).pluck(:id)
    loan_ids = self.collection_transactions.joins(:loan_payment).pluck("loan_payments.loan_id").uniq
    Loan.joins(:loan_product).where("loans.id IN (?) AND loan_products.is_entry_point = ?" , loan_ids, true).count > 0
  end

  def compute_values
    self.total_cp_amount = 0.00
    self.total_wp_amount = 0.00
    self.total_principal_amount = 0.00
    self.total_interest_amount = 0.00
    self.collection_transactions.each do |collection_transaction|
      if collection_transaction.account_type == "WP" and !collection_transaction.amount.nil?
        self.total_wp_amount += collection_transaction.amount
        self.total_cp_amount -= collection_transaction.amount
      else
        if !collection_transaction.amount.nil?
          self.total_cp_amount += collection_transaction.amount
          if collection_transaction.account_type == "LOAN_PAYMENT"
            loan_payment = collection_transaction.loan_payment 
            if loan_payment.pending?
              self.total_principal_amount += loan_payment.payment_stats[:principal]
              self.total_interest_amount += loan_payment.payment_stats[:interest]
            end
          end
        end
      end
    end
  end
end
