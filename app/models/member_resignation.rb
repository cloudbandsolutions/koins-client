class MemberResignation < ApplicationRecord
  STATUSES = ['pending', 'approved']
  RESIGNATION_TYPES = ["automatic", "voluntary", "involuntary"]

  belongs_to :member
  belongs_to :payment_collection

  has_many :resignation_loan_balances, dependent: :destroy
  accepts_nested_attributes_for :resignation_loan_balances

  has_many :resignation_withdrawals, dependent: :destroy
  accepts_nested_attributes_for :resignation_withdrawals

  validates :uuid, presence: true, uniqueness: true
  validates :resignation_type, presence: true, inclusion: { in: RESIGNATION_TYPES }
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :date_resigned, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
      self.status = 'pending'
    end

    if !self.or_number.nil?
      self.or_number = self.or_number.rjust(6, "0")
    end
  end
end
