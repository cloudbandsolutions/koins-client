class MajorAccount < ApplicationRecord
  belongs_to :major_group

  has_many :mother_accounting_codes

  validates :name, presence: true, uniqueness: true
  validates :code, presence: true

  def to_s
    name
  end
end
