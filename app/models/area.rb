class Area < ApplicationRecord
  has_many :clusters

  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  validates :abbreviation, presence: true, uniqueness: true
  validates :address, presence: true, uniqueness: true

  before_validation :load_defaults

  def load_defaults
    if self.uuid.blank?
      self.uuid = SecureRandom.uuid
    end
  end

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    {
      id: self.uuid,
      name: self.name,
      short_name: self.code
    }
  end
end
