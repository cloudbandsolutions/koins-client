class LoanInsurance < ApplicationRecord
	STATUSES = ["pending", "approved", "reversed"]
  COLLECTION_TYPES = ["Unremitted", "Cash"]

	belongs_to :branch
  belongs_to :loan_insurance_type

	has_many :loan_insurance_records, dependent: :destroy
  accepts_nested_attributes_for :loan_insurance_records

  validates :branch, presence: true
  validates :loan_insurance_type, presence: true
  validates :uuid, presence: true, uniqueness: true
  validates :collection_type, presence: true
  validates :reference_number, presence: true, if: :approved?
  validates :date_prepared, presence: true
  validates :total_amount, presence: true, numericality: true
  validates :prepared_by, presence: true
  validates :status, presence: true, inclusion: { in: STATUSES }

  before_validation :load_defaults

  def cash?
    self.collection_type == "Cash"
  end

  def unremitted?
    self.collection_type == "Unremitted"
  end

  def pending?
    self.status == "pending"
  end

  def approved?
    self.status == "approved"
  end

  def reversed?
    self.status == "reversed"
  end

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
      self.status = "pending"
      self.or_number = "#{Time.now.to_i} CHANGE-ME" if self.or_number.nil?
    end

    if self.or_number.present?
      self.or_number = self.or_number.rjust(5, "0")
    end
	end
end
