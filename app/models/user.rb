class User < ApplicationRecord
  REMOTE_ROLES = [
    "REMOTE-OAS",
    "REMOTE-BK",
    "REMOTE-MIS",
    "REMOTE-FM"
  ]

  ROLES = [
    "OAS",
    "BK",
    "SBK",
    "CM",
    "SO",
    "FM",
    "AM",
    "MIS",
    "ACC",
    "REMOTE-OAS",
    "REMOTE-BK",
    "REMOTE-MIS",
    "OM",
    "REMOTE-FM",
    "AO"
  ]

  belongs_to :cluster

  has_many :user_centers
  has_many :centers, through: :user_centers
  has_many :batch_transactions
  has_many :user_branches
  has_many :branches, through: :user_branches

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :username, :presence => true, :uniqueness => {
    :case_sensitive => false
  }
  validates :identification_number, presence: true, uniqueness: true
  validates :role, presence: true, inclusion: { in: ROLES }
  accepts_nested_attributes_for :branches
  attr_accessor :login

  before_validation :load_defaults

  has_attached_file :avatar,
                    styles: { thumb: "100x100#", profile: "200x200#" },
                    default_url: ":attachment/missing_:style.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  def not_superuser?
    not_superuser == true
  end
  
  def full_name
    "#{first_name} #{last_name}"
  end

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    {
      id: self.uuid,
      username: self.username,
      email: self.email,
      roles: [self.role],
      first_name: self.first_name,
      last_name: self.last_name,
      identification_number: self.identification_number,
      encrypted_password: self.encrypted_password
    }
  end

  def load_defaults
    if self.first_name.present? and self.last_name.present?
      self.first_name = self.first_name.upcase 
      self.last_name  = self.last_name.upcase 
    end

    if self.uuid.blank?
      self.uuid = SecureRandom.uuid
    end
  end

  def is_accounting?
    self.role == "ACC"
  end

  def full_name_upcase
    "#{first_name.try(:upcase)} #{last_name.try(:upcase)}"
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def to_s
    full_name_upcase
  end

  def has_role(roles)
    roles.include? self.role
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      if conditions[:username].nil?
        where(conditions).first
      else
        where(username: conditions[:username]).first
      end
    end
  end
end
