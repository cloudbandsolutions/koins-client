class AccountingCode < ApplicationRecord
  validates :beginning_balance, presence: true, numericality: true
  validates :ending_balance, presence: true, numericality: true
  validates :beginning_debit, presence: true, numericality: true
  validates :ending_debit, presence: true, numericality: true
  validates :beginning_credit, presence: true, numericality: true
  validates :ending_credit, presence: true, numericality: true
  validates :accounting_code_category, presence: true

  belongs_to :accounting_code_category

  before_validation :load_defaults
  before_save :build_code

  def build_code
    if self.code.nil?
      self.code = "#{self.accounting_code_category.code}#{self.sub_code}"
    end
  end

  def mother_accounting_code
    accounting_code_category.mother_accounting_code
  end

  def load_defaults
    if self.new_record?
      self.beginning_balance = 0.00
      self.ending_balance = 0.00
      self.beginning_debit = 0.00
      self.ending_debit = 0.00
      self.beginning_credit = 0.00
      self.ending_credit = 0.00
    end
  end

  def debit?
    self.accounting_code_category.mother_accounting_code.major_account.major_group.dc_code == "DR"
  end

  def credit?
    self.accounting_code_category.mother_accounting_code.major_account.major_group.dc_code == "CR"
  end

  def to_s
    "#{code} - #{name}"
  end

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    major_group = self.accounting_code_category.mother_accounting_code.major_account.major_group.to_s

    # Rename MEMBERS' EQUITY to simply EQUITIES for 2.0
    if major_group == "MEMBERS' EQUITY"
      major_group = "EQUITIES"
    end

    {
      id: self.uuid,
      name: self.name,
      code: self.code,
      category: major_group,
      data: {
        major_account: "#{self.accounting_code_category.mother_accounting_code.major_account.to_s}"
      }
    }
  end

  def temp_ending_balance
  end

  def to_s_trial_balance
    "#{code} - #{name}"
  end
end
