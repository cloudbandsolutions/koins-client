class MemberWithdrawalLoan < ApplicationRecord
  belongs_to :member_withdrawal
  belongs_to :loan

  validates :total_balance, presence: true, numericality: true
  validates :principal_balance, presence: true, numericality: true
  validates :interest_balance, presence: true, numericality: true
  validates :payment_amount, presence: true, numericality: true
end
