class NegativeLoanPayment < ApplicationRecord
  STATUS = ["pending", "approved"]
  belongs_to :loan
  belongs_to :loan_payment

  validates :loan, presence: true
  validates :amount, presence: true, numericality: true
  validates :generated_by, presence: true
  validates :approved_by, presence: true, if: :approved?
  validates :status, presence: true, inclusion: { in: STATUS }
  validates :particular, presence: true

  before_validation :load_defaults

  scope :pending, -> { where(status: "pending") }
  scope :approved, -> { where(status: "approved") }

  def approved?
    self.status == "approved"
  end

  def pending?
    self.status == "pending"
  end

  def load_defaults
    if self.new_record?
      self.status = "pending"
    end
  end
end
