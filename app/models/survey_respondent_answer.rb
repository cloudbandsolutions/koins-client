class SurveyRespondentAnswer < ApplicationRecord
  belongs_to :survey_respondent
  belongs_to :survey_question
  belongs_to :survey_question_option

  validates :survey_question, presence: true
  validates :survey_question_option, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
    end
  end
end
