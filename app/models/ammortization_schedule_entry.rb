class AmmortizationScheduleEntry < ApplicationRecord
  belongs_to :loan

  has_many :loan_moratoriums
  has_many :loan_payment_amortization_schedule_entries

  validates :uuid, presence: true, uniqueness: true
  validates :amount, presence: true, numericality: true
  validates :principal, presence: true, numericality: true
  validates :interest, presence: true, numericality: true
  validates :paid_principal, presence: true, numericality: true
  validates :paid_interest, presence: true, numericality: true

  scope :unpaid, -> { where(paid: false, is_void: nil).order("due_at ASC") }
  scope :paid, -> { where(paid: true, is_void: nil).order("due_at ASC") }
  scope :valid, -> { where("is_void IS NULL") }
 
  before_validation :load_defaults

  def to_version_2_hash
    {
      id: self.uuid,
      amount_due: self.amount,
      principal: self.principal,
      interest: self.interest,
      principal_paid: 0.00,
      interest_paid: 0.00,
      principal_balance: 0.00,
      interest_balance: 0.00,
      due_date: self.due_at,
      is_paid: false,
      loan_id: self.loan.uuid,
      data: {
      }
    }
  end
  
  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.hex(6)}"
      self.paid_principal = 0.00 if self.paid_principal.nil?
      self.paid_interest = 0.00 if self.paid_interest.nil?
      self.paid = 'f' if self.paid.nil?
    end
  end

  def is_fully_paid?
    self.total_payment == self.amount
  end

  def total_payment
    paid_interest + paid_principal
  end

  def interest_paid?
    paid_interest == self.interest ? true : false
  end

  def principal_paid?
    paid_principal == self.principal ? true : false
  end

  def remaining_balance
    remaining_interest + remaining_principal
  end

  def remaining_principal
    principal - paid_principal
  end

  def remaining_interest
    interest - paid_interest
  end
end
