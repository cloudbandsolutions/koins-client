class MemberMembershipPayment < ApplicationRecord
  belongs_to :member
  belongs_to :membership_type

  validates :amount_paid, presence: true, numericality: true
  validates :paid_at, presence: true
  validates :membership_type, presence: true
end
