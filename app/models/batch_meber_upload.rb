class BatchMeberUpload < ApplicationRecord
	STATUSES = ["pending", "done"]

	before_validation :load_defaults

	def load_defaults
		if self.new_record?
			self.status = "pending"
		end
	end
end
