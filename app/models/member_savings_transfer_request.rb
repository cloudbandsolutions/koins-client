class MemberSavingsTransferRequest < ApplicationRecord
  belongs_to :savings_account
  belongs_to :member_transfer_request
  belongs_to :dr_accounting_code, class_name: "AccountingCode", foreign_key: "dr_accounting_code_id"
  belongs_to :cr_accounting_code, class_name: "AccountingCode", foreign_key: "cr_accounting_code_id"

  validates :amount, presence: true, numericality: true
  validates :savings_account, presence: true

  def complete?
    dr_accounting_code.present? and cr_accounting_code.present?
  end
end
