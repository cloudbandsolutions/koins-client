class JournalEntry < ApplicationRecord
  attr_accessor :date_prepared, :particular
  POST_TYPES = %w(DR CR)

  default_scope { order("accounting_code_id ASC") }

  belongs_to :accounting_code
  belongs_to :voucher

  validates :uuid, presence: true
  validates :amount, presence: true, numericality: true
  validates :post_type, presence: true, inclusion: { in: POST_TYPES }
  validates :accounting_code, presence: true

  scope :debit_entries, -> { where(post_type: 'DR') }
  scope :credit_entries, -> { where(post_type: 'CR') }
  scope :approved_entries, -> { joins(:voucher).where("vouchers.status = ?", 'approved') }
  
  scope :approved_entries_by_date_range_and_accounting_code, ->(start_date, end_date, accounting_code) { 
    joins(:voucher).where(
      "vouchers.status = ? AND vouchers.date_prepared >= ? AND vouchers.date_prepared <= ? AND journal_entries.accounting_code_id = ?", 
      'approved', 
      start_date, 
      end_date, 
      accounting_code.id
    ) 
  }

  scope :approved_entries_by_date_range, ->(start_date, end_date) { 
    joins(:voucher).where(
      "vouchers.status = ? AND vouchers.date_prepared >= ? AND vouchers.date_prepared <=?", 
      'approved', 
      start_date, 
      end_date
    ) 
  }

  scope :approved_entries_by_date_range_and_branch, ->(start_date, end_date, branch) { 
    joins(:voucher).where(
      "vouchers.status = ? AND vouchers.date_prepared >= ? AND vouchers.date_prepared <= ? AND vouchers.branch_id = ?", 
      'approved', 
      start_date, 
      end_date, 
      branch.id
    ) 
  }

  scope :approved_entries_by_date_range_and_branch_and_accounting_code, ->(start_date, end_date, branch, accounting_code) { 
    joins(:voucher).where(
      "vouchers.status = ? AND vouchers.date_prepared >= ? AND vouchers.date_prepared <= ? AND vouchers.branch_id = ? AND journal_entries.accounting_code_id = ?", 
      'approved', 
      start_date, 
      end_date, 
      branch.id, 
      accounting_code.id
    ) 
  }

  scope :approved_entries_by_max_date_and_branch_and_accounting_code, ->(max_date, branch, accounting_code) { 
    joins(:voucher).where(
      "vouchers.status = ? AND vouchers.date_prepared <= ? AND vouchers.branch_id = ? AND journal_entries.accounting_code_id = ?", 
      'approved', 
      max_date, 
      branch.id, 
      accounting_code.id
    ) 
  }

  scope :approved_debit_entries_by_month_and_year_and_accounting_codes, ->(month, year, accounting_codes) { 
    joins(:voucher).where(
      "vouchers.status = ? AND extract(month from vouchers.date_prepared) = ? AND extract(year from vouchers.date_prepared) = ? AND journal_entries.accounting_code_id IN (?) AND post_type = 'DR'", 
      'approved', 
      month, 
      year, 
      accounting_codes
    ) 
  }

  scope :approved_credit_entries_by_month_and_year_and_accounting_codes, ->(month, year, accounting_codes) { 
    joins(:voucher).where(
      "vouchers.status = ? AND extract(month from vouchers.date_prepared) = ? AND extract(year from vouchers.date_prepared) = ? AND journal_entries.accounting_code_id IN (?) AND post_type = 'CR'", 
      'approved', 
      month, 
      year, 
      accounting_codes
    ) 
  }

  scope :approved_entries_by_max_date_and_accounting_code, ->(max_date, accounting_code) { 
    joins(:voucher).where(
      "vouchers.status = ? AND vouchers.date_prepared <= ? AND journal_entries.accounting_code_id = ?", 
      'approved', 
      max_date, 
      accounting_code.id
    ) 
  }

  scope :approved_entries_by_max_date, ->(max_date) { 
    joins(:voucher).where(
      "vouchers.status = ? AND vouchers.date_prepared <= ?",
      'approved', 
      max_date
    ) 
  }

  scope :order_accounting, -> { joins(:accounting_code).order("accounting_codes.code ASC") }

  def to_version_2_hash
    uuid_regex  = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/
    old_uuid    = ""

    if !self.valid?
      raise "INVALID JOURNAL ENTRY #{self.id}"
    end

    if self.uuid.blank?
      self.update!(uuid: "#{SecureRandom.uuid}")
    elsif !uuid_regex.match?(self.uuid.to_s.downcase)
      old_uuid = self.uuid
      self.update!(uuid: "#{SecureRandom.uuid}")
    end

    {
      id: self.uuid,
      post_type: self.post_type,
      accounting_code_id: self.accounting_code.uuid,
      amount: self.amount,
      accounting_entry_id: self.voucher.uuid,
      data: {
        is_year_end_closing: self.is_year_end_closing,
        old_uuid: old_uuid
      }
    }
  end


  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
    end
  end

  def to_s
    "#{post_type} - #{accounting_code} - #{amount}"
  end
end
