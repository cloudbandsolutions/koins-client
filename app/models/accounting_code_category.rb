class AccountingCodeCategory < ApplicationRecord
  belongs_to :mother_accounting_code

  has_many :accounting_codes

  validates :name, presence: true
  validates :mother_accounting_code, presence: true
  validates :sub_code, presence: true

  before_save :build_code

  def build_code
    self.code = "#{self.mother_accounting_code.code}#{self.sub_code}"
  end

  def to_s
    name
  end
end
