class WpRecord < ApplicationRecord
  belongs_to :member
  belongs_to :loan_payment
  belongs_to :savings_account_transaction

  validates :member, presence: true
  validates :amount, presence: true, numericality: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
    end
  end
end
