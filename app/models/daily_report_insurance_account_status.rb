class DailyReportInsuranceAccountStatus < ApplicationRecord
  belongs_to :branch

  validates :uuid, presence: true, uniqueness: true
  validates :as_of, presence: true
  validates :data, presence: true
  validates :generated_hour, presence: true
  validates :generated_min, presence: true
  validates :branch, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
      self.generated_hour = Time.now.hour
      self.generated_min  = Time.now.min
    end
  end

  def generated_time
    "#{generated_hour.to_s.rjust(2, "0")}:#{generated_min.to_s.rjust(2, "0")}"
  end
end
