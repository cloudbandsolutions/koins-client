class LoanPaymentAdjustment < ApplicationRecord
  belongs_to :branch

  validates :branch, presence: true
  validates :uuid, presence: true, uniqueness: true
  validates :status, presence: true, inclusion: { in: ["pending", "approved"] }
  validates :prepared_by, presence: true
  validates :approved_by, presence: true, if: :approved?
  validates :date_approved, presence: true, if: :approved?
  validates :voucher_reference_number, presence: true, if: :approved?
  validates :particular, presence: true

  has_many :loan_payment_adjustment_records, dependent: :delete_all
  accepts_nested_attributes_for :loan_payment_adjustment_records

  before_validation :load_defaults

  def approved?
    self.status == 'approved'
  end

  def pending?
    self.status == 'pending'
  end

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
      self.status = "pending"
    end
  end
end
