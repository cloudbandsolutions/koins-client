class Global < ApplicationRecord
  belongs_to :loan_processing_fee_account_code, class_name: "AccountingCode", foreign_key: "loan_processing_fee_account_code_id"
end
