class SavingsType < ApplicationRecord
  belongs_to :withdraw_accounting_code, class_name: 'AccountingCode', foreign_key: 'withdraw_accounting_code_id'
  belongs_to :deposit_accounting_code, class_name: 'AccountingCode', foreign_key: 'deposit_accounting_code_id'
  belongs_to :expense_accounting_code, class_name: 'AccountingCode', foreign_key: 'expense_accounting_code_id'
  belongs_to :tax_accounting_code, class_name: 'AccountingCode', foreign_key: 'tax_accounting_code_id'
  belongs_to :interest_accounting_code, class_name: 'AccountingCode', foreign_key: 'interest_accounting_code_id'

  has_many :savings_accounts

  validates :name, presence: true
  validates :code, presence: true, uniqueness: true
  validates :monthly_tax_rate, presence: true, numericality: true
  validates :monthly_interest_rate, presence: true, numericality: true
  validates :annual_interest_rate, presence: true, numericality: true

  scope :default, -> { where(is_default: true) }

  def to_s
    name
  end

end
