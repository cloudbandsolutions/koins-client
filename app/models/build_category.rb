class BuildCategory < ApplicationRecord
	CATEGORIES = ["General" ,"Reports", "Misc", "Accounting", "MIS", "Loans", "Cash Management", "Membership", "Collection/Billing", "Miscellaneous"]
	
	belongs_to :build_version
	has_many :build_contents
	accepts_nested_attributes_for :build_contents, reject_if: :all_blank, allow_destroy: true
end
