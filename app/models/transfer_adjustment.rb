class TransferAdjustment < ApplicationRecord
  ACCOUNT_TYPES = [
    "SAVINGS",
    "INSURANCE",
    "EQUITY"
  ]

  TRANSACTION_TYPES = [
    "ADD",
    "DEDUCT"
  ]

  belongs_to :branch
  belongs_to :member

  has_many :transfer_adjustment_records, dependent: :delete_all
  accepts_nested_attributes_for :transfer_adjustment_records

  before_validation :load_defaults

  validates :branch, presence: true
  validates :member, presence: true
  validates :uuid, presence: true, uniqueness: true
  validates :status, presence: true, inclusion: { in: ["pending", "approved"] }
  validates :prepared_by, presence: true
  validates :approved_by, presence: true, if: :approved?
  validates :date_approved, presence: true, if: :approved?
  validates :accounting_entry_reference_number, presence: true, if: :approved?
  validates :particular, presence: true
  validates :transaction_type, presence: true, inclusion: { in: TRANSACTION_TYPES }
  validates :amount, presence: true, numericality: true
  #validates :account_type, presence: true, inclusion: { in: ACCOUNT_TYPES }
  validates :account_code, presence: true

  validate :has_more_than_one_record
  validate :balanced_entries

  def voucher_reference_number
    self.accounting_entry_reference_number
  end

  def add?
    self.transaction_type == "ADD"
  end

  def deduct?
    self.transaction_type == "DEDUCT"
  end

  def has_more_than_one_record
    if transfer_adjustment_records.size == 0
      errors.add(:particular, "This should have more than one entry for records")
    end
  end

  def balanced_entries
    transfer_amount = self.amount ||= 0.00
    records_amount  = 0.00

    transfer_adjustment_records.each do |r|
      if r.amount.present?
        records_amount  +=  r.amount
      else
        records_amount  +=  0
      end
    end

    if transfer_amount  != records_amount
      errors.add(:particular, "Unbalanced entries. #{transfer_amount} #{records_amount}")
    end
  end

  def approved?
    self.status == 'approved'
  end

  def pending?
    self.status == 'pending'
  end

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
      self.status = "pending"
    end
  end
end
