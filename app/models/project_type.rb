class ProjectType < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  belongs_to :project_type_category

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: SecureRandom.uuid)
    end

    {
      id: self.uuid,
      name: self.name,
      code: self.code,
      project_type_category_id: self.project_type_category.try(:uuid)
    }
  end
end
