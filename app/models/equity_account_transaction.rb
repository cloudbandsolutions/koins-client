class EquityAccountTransaction < ApplicationRecord
  TRANSACTION_TYPES = %w(withdraw deposit reversed fund_transfer_withdraw fund_transfer_deposit reverse_withdraw reverse_deposit)
  STATUSES = %w(pending approved reversed)

  belongs_to :equity_account
  belongs_to :bank
  belongs_to :accounting_code

  validates :transaction_number, presence: true, uniqueness: true
  validates :transaction_type, presence: true, inclusion: { in: TRANSACTION_TYPES }
  validates :amount, presence: true, numericality: true, equity_transaction_amount: true
  validates :transacted_at, presence: true
  validates :equity_account, presence: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :bank, presence: true
  validates :accounting_code, presence: true

  scope :approved_deposits, -> { where(status: "approved", transaction_type: %w(deposit)) }
  scope :approved_withdrawals, -> { where(status: "approved", transaction_type: %w(withdraw wp)) }
  scope :approved, -> { where(status: ["approved", "reversed"]) }

  before_validation :load_defaults
  before_save :adjust_balances

  def to_version_2_hash
    t_transaction_type    = self.transaction_type
    t_is_withdraw_payment = false
    t_is_interest         = false
    t_status              = self.status

    t_for_resignation = (self.for_resignation == true)

    if t_transaction_type == "wp"
      t_transaction_type    = "withdraw"
      t_is_withdraw_payment = true
    elsif t_transaction_type == "interest"
      t_transaction_type  = "deposit"
      t_is_interest       = true
    elsif t_transaction_type == "reverse_deposit"
      t_transaction_type  = "withdraw"
    elsif t_transaction_type == "reverse_withdraw"
      t_transaction_type  = "deposit"
    elsif t_transaction_type == "fund_transfer_deposit"
      t_transaction_type  = "deposit"
      t_is_fund_transfer  = true
    end

    {
      id: self.uuid,
      subsidiary_id: self.equity_account.uuid,
      subsidiary_type: "MemberAccount",
      amount: self.amount,
      transaction_type: t_transaction_type,
      transacted_at: self.created_at,
      status: t_status,
      data: {
        accounting_entry_reference_number: self.voucher_reference_number,
        accounting_entry_particular: self.particular,
        beginning_balance: 0.00,
        ending_balance: 0.00
      }
    }
  end

  def valid_for_reversal?
    valid = true

    current_balance = self.equity_account.balance
    amount = self.amount

    if self.transaction_type == "deposit" or self.transaction_type == "interest" or self.transaction_type == "fund_transfer_deposit"
      if current_balance - amount < 0
        valid = false
      end
    end

    valid
  end

  def adjust_balances
    if self.status == "approved"
      if ["deposit", "fund_transfer_deposit", "interest"].include? self.transaction_type
        self.beginning_balance = EquityAccount.find(self.equity_account.id).balance
        self.ending_balance = self.beginning_balance + self.amount
      elsif ["withdraw", "wp", "tax", "fund_transfer_withdraw"].include? self.transaction_type
        self.beginning_balance = EquityAccount.find(self.equity_account.id).balance
        self.ending_balance = self.beginning_balance - self.amount
      end
    end
  end

  def load_defaults
    if self.new_record?
      self.transaction_number = SecureRandom.hex(4)
      self.uuid = SecureRandom.uuid

      #if self.voucher_reference_number.nil?
      #  self.voucher_reference_number = VoucherService.voucher_number
      #end

      if self.transacted_at.nil?
        self.transacted_at = Time.now
      end

      if self.status.nil?
        self.status = "pending"
      end

      if !self.equity_account.nil?
        self.bank = self.equity_account.branch.bank
        self.accounting_code = self.bank.accounting_code
      end
    end

    self.transaction_date = self.transacted_at
  end

  def approve!(approved_by)
    if self.status == "pending"
      self.update!(status: "approved")
      self.generate_updates!
    else
      raise "Cannot approve non-pending transaction"
    end
  end

  def generate_updates!
    sa = EquityAccount.find(self.equity_account_id)
    if self.transaction_type == "withdraw" or self.transaction_type == 'fund_transfer_withdraw' or self.transaction_type == 'reverse_deposit'

      updated_balance = sa.balance - self.amount  
      sa.update!(balance: updated_balance)
    elsif self.transaction_type == "deposit" or self.transaction_type == 'fund_transfer_deposit' or self.transaction_type == 'reverse_withdraw'

      updated_balance = sa.balance + self.amount
      sa.update!(balance: updated_balance)
    end
  end

  def reverse!
    sa = EquityAccount.find(self.equity_account_id)
    if self.transaction_type == "withdraw" or self.transaction_type == "wp" or self.transaction_type == "tax" or self.transaction_type == "fund_transfer_withdraw"

      updated_balance = sa.balance + self.amount  

      if updated_balance < 0
        raise "Balance is below 0"
      end

      sa.update!(balance: updated_balance)
    elsif self.transaction_type == "deposit" or self.transaction_type == "interest" or self.transaction_type == "fund_transfer_deposit"

      updated_balance = sa.balance - self.amount
      sa.update!(balance: updated_balance)
    end
  end

  def to_s
    transaction_number
  end
end
