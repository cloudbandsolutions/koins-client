class SubsidiaryAdjustmentRecord < ApplicationRecord
  ADJUSTMENT_TYPES = ["credit", "debit"]
  ADJUSTMENT_TYPES_SELECT = {
    credit: "Adj. to Add",
    debit: "Adj. to Deduct"
  }

  belongs_to :member
  belongs_to :subsidiary_adjustment
  belongs_to :accounting_code

  validates :member, presence: true
  validates :adjustment_type, presence: true, inclusion: { in: ADJUSTMENT_TYPES }
  validates :amount, presence: true, numericality: true
  validates :account_code, presence: true, inclusion: { in: Settings.account_codes }
  validates :accounting_code, presence: true

  before_validation :load_defaults

  def load_defaults
    # Check Savings
    savings_account = SavingsAccount.joins(:savings_type).where("savings_types.code = ? AND savings_accounts.member_id = ?", account_code, member.id).first
    if savings_account
      self.beginning_balance = savings_account.balance

      if self.adjustment_type == 'credit'
        self.ending_balance = savings_account.balance + self.amount
      elsif self.adjustment_type == 'debit'
        self.ending_balance = savings_account.balance - self.amount
      end
    end

    # Check Insurance
    insurance_account = InsuranceAccount.joins(:insurance_type).where("insurance_types.code = ? AND insurance_accounts.member_id = ?", account_code, member.id).first
    if insurance_account
      self.beginning_balance = insurance_account.balance

      if self.adjustment_type == 'credit'
        self.ending_balance = insurance_account.balance + self.amount
      elsif self.adjustment_type == 'debit'
        self.ending_balance = insurance_account.balance - self.amount
      end
    end

    # Check Equity
    equity_account = EquityAccount.joins(:equity_type).where("equity_types.code = ? AND equity_accounts.member_id = ?", account_code, member.id).first
    if equity_account
      self.beginning_balance = equity_account.balance

      if self.adjustment_type == 'credit'
        self.ending_balance = equity_account.balance + self.amount
      elsif self.adjustment_type == 'debit'
        self.ending_balance = equity_account.balance - self.amount
      end
    end
  end
end
