class LegalDependent < ApplicationRecord
  RELATIONSHIP = ["Spouse", "Child", "Parent", "Sibling"]
  # RELATIONSHIP = ["ASAWA", "ANAK", "MAGULANG", "KAPATID"]

  belongs_to :member

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :date_of_birth, presence: true
  # validates :relationship, presence: true
  validates :reference_number, presence: true, uniqueness: true

  before_validation :load_defaults

  def to_version_2_hash
    {
      id: self.uuid,
      first_name: self.first_name,
      middle_name: self.middle_name,
      last_name: self.last_name,
      date_of_birth: self.date_of_birth,
      member_id: self.member.uuid,
      relationship: self.relationship,
      data: {
        educational_attainment: self.educational_attainment,
        course: self.course,
        is_deceased: self.is_deceased,
        is_tpd: self.is_tpd
      }
    }
  end
  
  def full_name_with_member
    "#{last_name.upcase}, #{first_name.upcase} #{middle_name.upcase} (#{self.member.full_name.try(:to_s)})"
  end

  def full_name_upcase
    "#{first_name.try(:upcase)} #{middle_name.try(:upcase)} #{last_name.try(:upcase)}"
  end

  def full_name_titleize
    "#{last_name.try(:titleize)}, #{first_name.try(:titleize)} #{middle_name.try(:titleize)}"  
  end

  def age
    if self.date_of_birth.nil?
      "Please set date of birth"
    else
      begin
        now = Time.now.utc.to_date
        now.year - self.date_of_birth.year - ((now.month > self.date_of_birth.month || (now.month == self.date_of_birth.month && now.day >= self.date_of_birth.day)) ? 0 : 1)
        #((Time.zone.now - date_of_birth.to_time) / 1.year.seconds).floor
      rescue Exception
        "ERR IN AGE"
      end
    end
  end

  def date_of_birth_formatted
    if self.date_of_birth.nil?
      "Please set date of birth"
    else
      begin
        self.date_of_birth.strftime("%b %d, %Y")
      rescue Exception
        "ERR IN DATE OF BIRTH: #{self.date_of_birth}"
      end
    end
  end

  def load_defaults
    if self.new_record? and self.reference_number.nil?
      self.reference_number = "#{SecureRandom.hex(4)}"
    end

    if self.uuid.nil?
      self.uuid = "#{SecureRandom.uuid}"
    end
  end

  def to_hash
    b = self.attributes.except!("id", "member_id")
    b[:member_identification_number] = self.member.identification_number

    return b
  end
end
