class SurveyQuestion < ApplicationRecord
  belongs_to :survey

  has_many :survey_question_options, dependent: :destroy
  accepts_nested_attributes_for :survey_question_options

  validates :uuid, presence: true, uniqueness: true
  #validates :survey, presence: true
  validates :priority, presence: true, uniqueness: { scope: :survey_id }
  validates :content, presence: true

  before_validation :load_defaults

  scope :question_count, -> { count }

  def load_defaults
    if self.new_record? and self.uuid.nil?
      self.uuid = SecureRandom.uuid
    end
  end

  def options
    survey_question_options.order(:priority).pluck(:option).join(",")
  end
end
