class Loan < ApplicationRecord
  PAYMENT_TYPES = %w(cash)
  PAYMENT_TERMS = %w(weekly monthly semi-monthly)
  LOAN_STATUSES = %w(pending active paid writeoff lay_low inactive deleted for-transfer transferred)
  LOAN_INSTALLMENT_INTERVALS = [15, 25, 35, 50]
  LOAN_CATEGORIES = ["default", "micro", "small", "medium"]

  def self.installment_intervals(m_term)
    if m_term == "weekly"
      LOAN_INSTALLMENT_INTERVALS
    else
      [1,2,3,4,5,6,7,8,9,10, 11, 12]
    end
  end

  belongs_to :branch
  belongs_to :center
  belongs_to :member
  belongs_to :loan_product
  belongs_to :loan_product_type
  belongs_to :project_type
  belongs_to :project_type_category
  belongs_to :bank
  belongs_to :co_maker_one, class_name: 'Member', foreign_key: 'co_maker_one_id'

  has_many :ammortization_schedule_entries
  has_many :loan_payments
  has_many :loan_membership_payments
  accepts_nested_attributes_for :loan_membership_payments

  has_many :negative_loan_payments

  has_many :member_weekly_expenses
  accepts_nested_attributes_for :member_weekly_expenses

  has_many :member_weekly_incomes
  accepts_nested_attributes_for :member_weekly_incomes

  validates :pn_number, presence: true, uniqueness: true
  validates :date_prepared, presence: true
  validates :amount, presence: true, numericality: { greater_than: 0 }
  validates :branch, presence: true
  validates :num_installments, presence: true
  validates :term, presence: true
  validates :payment_type, presence: true, inclusion: { in: PAYMENT_TYPES }
  #validates :co_maker_two, presence: true
  validates :loan_product, presence: true, loan_product_uniqueness: true
  validates :center, presence: true
  validates :interest_rate, presence: true, numericality: true
  validates :voucher_date_requested, presence: true
  validates :member, presence: true, savings_account_for_loan: true
  validates :loan_product, presence: true
  validates :voucher_payee, presence: true
  validates :voucher_particular, presence: true
  validates :bank, presence: true
  validates :processing_fee, presence: true
  validates :installment_override, presence: true, if: :override_installment_interval?
  validates :loan_product_type, presence: true
  validates :original_loan_amount, presence: true, numericality: true, if: :override_installment_interval?
  #validates :original_num_installments, presence: true, numericality: true, inclusion: { in: LOAN_INSTALLMENT_INTERVALS }, if: :override_installment_interval?
  validates :original_num_installments, presence: true, numericality: true, if: :override_installment_interval?
  validates :old_principal_balance, presence: true, numericality: true, if: :override_installment_interval?
  validates :old_interest_balance, presence: true, numericality: true, if: :override_installment_interval?
  validate :co_maker_one, :already_co_maker
  validate :loan_product, :check_next_application_interval, if: :new_record?
  validate :installment_override_less_than_or_equal_to_original_num_installments, if: :override_installment_interval?

  validate :first_date_of_payment_is_greater_than_today

  before_validation :load_defaults
  before_save :load_amount_released
  after_create :initialize_amortization_schedule!
  after_save :update_maintaining_balance!

  scope :active_and_pending,    -> { where(status: ["active", "pending"]) }
  scope :expenses,              -> { where.not(status: ["pending", "deleted"]) }
  scope :for_processing,        -> { where.not(status: 'deleted') }
  scope :active,                -> { where(status: "active") }
  scope :active_and_paid,       -> { where(status: ["paid", "active"]) }
  scope :pending,               -> { where(status: "pending") }
  scope :paid,                  -> { where(status: "paid") }
  scope :writeoff,              -> { where(status: "writeoff") }
  scope :current,               -> { where.not(status: "paid") }
  scope :pending_entry_points,  -> { joins(:loan_product).where("loan_products.is_entry_point = ? AND loans.status = ?", true, "pending") }
  scope :active_entry_points,   -> { joins(:loan_product).where("loan_products.is_entry_point = ? AND loans.status = ?", true, "active") }
  scope :active_and_paid_entry_points,   -> { joins(:loan_product).where("loan_products.is_entry_point = ? AND loans.status IN (?)", true, ["active", "paid"]) }
  scope :entry_points,          -> {joins(:loan_product).where("loan_products.is_entry_point = ?", true) }
  scope :released,              -> { where(status: ["active", "paid"]) }
  scope :overdue,               -> { where("status = ? AND maturity_date < ?", "active", Time.now) }
  scope :insured,               -> { where("loans.status IN (?) AND loans.is_insured = ?", ["active", "paid", "writeoff"], true) }
  scope :transferred,           -> { where("loans.status IN (?) and loans.override_installment_interval = ?", ["active", "paid", "deleted", "writeoff"], true) }

  def to_version_2_hash
    #co_makers = []
    if self.co_maker_one.present?
      co_maker_one = {
        value: self.co_maker_one.uuid,
        label: self.co_maker_one.first_name + ', ' + self.co_maker_one.last_name + ', '+ self.co_maker_one.middle_name,
        id: self.co_maker_one.uuid,
        first_name: self.co_maker_one.first_name,
        middle_name: self.co_maker_one.middle_name,
        last_name: self.co_maker_one.last_name
      }
    end

    #if self.co_maker_one.present?
    #  co_makers << {
    #    first_name: self.co_maker_one.first_name,
    #    middle_name: self.co_maker_one.middle_name,
    #    last_name: self.co_maker_one.last_name,
    #    primary: true
    #  }
    #end

    #if self.co_maker_two.present?
    #  co_makers << {
    #    first_name: self.co_maker_two,
    #    middle_name: "",
    #    last_name: "",
    #    is_primary: false
    #  }
    #end

    voucher = Voucher.where(
                reference_number: self.voucher_reference_number,
                book: self.book
              ).first

    {
      id: self.uuid,
      center_id: self.center.uuid,
      branch_id: self.center.branch.uuid,
      date_prepared: self.date_prepared,
      date_approved: self.date_approved,
      date_released: self.date_of_release,
      date_completed: self.date_completed,
      first_date_of_payment: self.first_date_of_payment,
      member_id: self.member.uuid,
      principal: self.amount,
      interest: self.ammortization_schedule_entries.where(is_void: nil).sum(:interest),
      principal_paid: 0.00,
      principal_balance: 0.00,
      interest_paid: 0.00,
      interest_balance: 0.00,
      status: self.status,
      loan_product_id: self.loan_product.uuid,
      term: self.term,
      pn_number: self.pn_number,
      payment_type: self.payment_type,
      num_installments: self.num_installments,
      monthly_interest_rate: (self.interest_rate / 100 / 12).to_f,
      project_type_id: self.project_type.try(:uuid),
      particular: "",
      data: {
        clip_number: self.clip_number,
        voucher: {
          bank: self.bank.try(:to_s),
          bank_check_number: self.bank_check_number,
          check_number: self.voucher_check_number,
          payee: self.voucher_payee,
          date_requested: self.voucher_date_requested,
          date_of_check: voucher.try(:date_of_check),
          particular: self.voucher_particular
        },
        accounting_entry: {
          reference_number: self.voucher_reference_number,
          particular: self.voucher_particular,
          book: self.book
        },
        beneficiary: {
          first_name: self.beneficiary_first_name,
          middle_name: self.beneficiary_middle_name,
          last_name: self.beneficiary_last_name,
          date_of_birth: self.beneficiary_date_of_birth,
          relationship: self.beneficiary_relationship
        },
        #co_makers: co_makers,
        co_maker_one: co_maker_one,
        co_maker_two: self.co_maker_two
      }
    }
  end

  def accounting_entry
    Voucher.where(book: self.book, reference_number: voucher_reference_number).try(:first)
  end

  def beneficiary
    "#{beneficiary_first_name.try(:titleize)} #{beneficiary_middle_name.try(:titleize)} #{beneficiary_last_name.try(:titleize)}"
  end

  def first_date_of_payment_is_greater_than_today
    if !self.first_date_of_payment.nil? and self.first_date_of_payment < Date.today and !self.active?
      #errors.add(:first_date_of_payment, "should be greater than today")
    end
  end

  def installment_override_less_than_or_equal_to_original_num_installments
    if !installment_override.nil?
      if installment_override > original_num_installments
        errors.add(:installment_override, "should be less than or equal to original installment")
        errors.add(:original_num_installments, "should be greater than new installment")
      end
    end
  end

  def update_maintaining_balance!
    if self.active? or self.paid?
      savings_account = SavingsAccount.where(member_id: self.member.id, savings_type: self.loan_product.savings_type.id).first
      Savings::UpdateMaintainingBalance.new(savings_account: savings_account).execute!
    end
  end

  def override_installment_interval?
    self.override_installment_interval == true
  end

  def not_override_installment_interval?
    self.override_installment_interval != true
  end

  def self.good_standing
    good_standing_loans = []
    Loan.active.each do |loan|
      if Time.now <= loan.ammortization_schedule_entries.where(paid: false).where("is_void IS NULL").first.due_at
        good_standing_loans << loan
      end
    end

    good_standing_loans
  end

  def self.par
    par_loans = []
    Loan.active.each do |loan|
      amortization_schedule_entries = loan.ammortization_schedule_entries.where("paid = ? AND due_at <= ? AND is_void IS NULL", false, Time.now)
      if amortization_schedule_entries.count > 0 and amortization_schedule_entries.last.due_at <= Time.now
        par_loans << loan
      end
    end

    par_loans
  end

  def self.overdue
    overdue_loans = []

    Loan.active.each do |loan|
      if Time.now > loan.ammortization_schedule_entries.where("is_void IS NULL").last.due_at
        overdue_loans << loan
      end
    end

    overdue_loans
  end

  def date_released
    if !date_of_release
      if voucher_reference_number.present?
        Voucher.where(book: self.book, reference_number: voucher_reference_number).try(:first).try(:date_prepared)
      else
        nil
      end
    else
      date_of_release
    end
  end

  def date_approved_formatted
    if date_approved
      date_approved.strftime("%B-%d-%Y")
    else
      "N/A"
    end
  end

  def maturity_date_formatted
    if maturity_date
      maturity_date.strftime("%m/%d/%Y")
    else
      "N/A"
    end
  end

  def date_released_formatted
    dr = date_released

    if dr
      return dr.strftime("%B-%d-%Y")
    else
      "N/A"
    end
  end

  def entry_level?
  end

  def check_next_application_interval
    if !self.loan_product.nil? and !self.member.nil?
      i = self.loan_product.next_application_interval
      latest_active_loan = self.member.latest_active_loan(self.loan_product)

      if !latest_active_loan.nil?
        if latest_active_loan.pending_payment_count > i
          errors.add(:loan_product, "cannot apply yet for this loan")
          errors.add(:pn_number, "cannot apply yet for this loan")
        end
      end
    end
  end

  # TODO: refactor this
  def already_co_maker
    if self.co_maker_one.present?
      if Loan.active.where("loan_product_id = ? AND co_maker_one_id = ?", self.loan_product_id, self.co_maker_one.id).count > 1
        #errors.add(:co_maker_one, "this member is already a co maker")
      end
    end
  end

  def pending_payment_count
    self.ammortization_schedule_entries.unpaid.count
  end

  def writeoff?
    self.status == "writeoff" ? true : false
  end

  def pending?
    self.status == "pending" ? true : false
  end

  def deleted?
    self.status == "deleted" ? true : false
  end

  def paid?
    self.status == "paid" ? true : false
  end

  def active?
    self.status == "active" ? true : false
  end

  def moratorium_entries
    entries = []

    self.ammortization_schedule_entries.where("is_void IS NULL").each do |a|
      a.loan_moratoriums.each do |lm|
        entries << lm
      end
    end

    entries
  end

  def amount_paid
    paid_principal + paid_interest
  end

  def amount_paid_as_of(as_of)
    paid_principal_as_of(as_of) + paid_interest_as_of(as_of)
  end

  def pending_loan_payments
    LoanPayment.where(loan_id: self.id, status: "pending").order(:paid_at)
  end

  def savings_accounts_for_wp
    SavingsAccount.where("member_id IN (?)", [self.member.id, self.co_maker_one.id])
  end

  def next_payment(date)
    #raise ammortization_schedule_entries.unpaid.inspect
    a = 0.00
    self.ammortization_schedule_entries.unpaid.where("due_at <= ?", date).each do |ase|
      a +=  ase.remaining_balance
    end

    a
  end

  def paid_principal
    self.ammortization_schedule_entries.where("is_void IS NULL").sum(:paid_principal)
  end

  def paid_principal_as_of(as_of)
    #self.ammortization_schedule_entries.where("due_at <= ?", as_of).sum(:paid_principal)
    #self.loan_payments.approved.where("paid_at <= ? AND is_void IS NULL", as_of).sum(:paid_principal)
    self.loan_payments.approved.where("paid_at <= ? AND is_void IS NULL", as_of).sum(:paid_principal)
  end

  def paid_interest
    self.ammortization_schedule_entries.where("is_void IS NULL").sum(:paid_interest)
  end

  def paid_interest_as_of(as_of)
    #self.ammortization_schedule_entries.where("due_at <= ?", as_of).sum(:paid_interest)
    self.loan_payments.approved.where("paid_at <= ? AND is_void IS NULL", as_of).sum(:paid_interest)
  end

  def principal_balance_as_of(as_of)
    #self.ammortization_schedule_entries.where("due_at <= ?", as_of).sum(:principal) - paid_principal_as_of(as_of)
    #self.ammortization_schedule_entries.where("due_at <= ?", as_of).sum(:principal) - self.loan_payments.approved.where("paid_at <= ?", as_of).sum(:paid_principal)
    self.ammortization_schedule_entries.where("is_void IS NULL").sum(:principal) - self.loan_payments.approved.where("paid_at <= ?", as_of).sum(:paid_principal)
  end

  def abs_unpaid_principal_balance_as_of(as_of)
    self.ammortization_schedule_entries.where("due_at <= ? AND paid = ? AND is_void IS NULL", as_of, false).sum(:principal)
  end

  def principal_amt_past_due_as_of(as_of)
    due_principal = self.ammortization_schedule_entries.where("due_at <= ? AND is_void IS NULL", as_of).sum(:principal)
    temp_amount = due_principal - paid_principal_as_of(as_of)

    if temp_amount < 0
      temp_amount = 0
    end

    temp_amount
  end

  def principal_balance
    self.ammortization_schedule_entries.where("is_void IS NULL").sum(:principal) - paid_principal
  end

  def interest_rate_per_month
    self.interest_rate / 12
  end

  def interest_balance_as_of(as_of)
    self.ammortization_schedule_entries.where("due_at <= ? AND is_void IS NULL", as_of).sum(:interest) - paid_interest_as_of(as_of)
  end

  def interest_balance
    self.ammortization_schedule_entries.where("is_void IS NULL").sum(:interest) - paid_interest
  end

  def no_payment_yet?
    self.loan_payments.where(status: "pending").count == 0 ? true : false
  end

  def initialize_amortization_schedule!
    #interest_rate = self.loan_product_type.interest_rate
    interest_rate = self.interest_rate

    schedule =  Loans::CreateAmortizationSchedule.new(
                  loan: self, 
                  interest_rate: interest_rate, 
                  principal: amount, 
                  term: num_installments, 
                  mode_of_payment: term
                ).execute!

    if !self.first_date_of_payment.nil?
      date_counter = self.first_date_of_payment
    end

    amortization_schedule = []
    schedule[:payments].each do |p|
      ase = AmmortizationScheduleEntry.new(
              amount: p[:principal] + p[:interest],
              principal: p[:principal],
              interest: p[:interest]
            )

      if p[:principal] == 0 and p[:interest] == 0
        ase.paid = true
      end

      if !self.first_date_of_payment.nil?
        ase.due_at = date_counter
        if self.term == "weekly"
          date_counter = date_counter + 1.week
        elsif self.term == "monthly"
          date_counter = date_counter + 1.month
        elsif self.term == "semi-monthly"
          date_counter = date_counter + 15.days
        end
      end

      if !ase.valid?
        raise ase.errors.inspect
      end

      amortization_schedule << ase
    end

    # Delete old ammortization_schedule_entries
    if self.ammortization_schedule_entries.count > 0
      old_ammortization_schedule_entries = self.ammortization_schedule_entries
      old_ammortization_schedule_entries.destroy_all
    end

    self.ammortization_schedule_entries = amortization_schedule
  end

  def next_loan_payment
    if ammortization_schedule_entries.paid.size == 0
      ammortization_schedule_entries.first
    else
      ammortization_schedule_entries.paid.last
    end
  end

  def remaining_num_installments
    self.ammortization_schedule_entries.unpaid.count
  end

  def maturity_date
    self.ammortization_schedule_entries.where("is_void IS NULL").try(:last).try(:due_at)
  end

  def load_defaults
    if self.new_record?
      if self.interest_rate.blank?
        self.interest_rate = self.loan_product_type.interest_rate if !self.loan_product.nil?
      end
      self.status = "pending" if self.status.nil?
      self.date_prepared = Time.now
      self.voucher_payee = self.try(:member).try(:full_name_formatted)
      #self.voucher_reference_number = VoucherService.voucher_number("CDB")
      self.remaining_balance = 0.00 if self.remaining_balance.blank?
      self.amount = 0.00 if self.amount.nil?

      if !self.member.nil?
        member_sa = SavingsAccount.active.where(member_id: self.member.id, savings_type_id: self.loan_product.savings_type.id).first
        
        # Create savings account if nil
        if member_sa.nil?
          sa = SavingsAccount.create!(member: self.member, savings_type: self.loan_product.savings_type, account_number: SecureRandom.hex(4))
        end
      end
    end

    self.uuid = SecureRandom.uuid if self.uuid.nil?
    self.branch = self.member.try(:branch)
    self.center = self.member.try(:center)
    self.member_full_name = member.to_s if !self.member.nil?

    # Check if loan_product has loan_insurance_deduction_entries.
    # If so, this loan should be insured
    if self.loan_product.present?
      self.is_insured = true if self.loan_product.loan_insurance_deduction_entries.size > 0
    end

    # maintaining balance for this loan
    if self.override_maintaining_balance == true
      self.maintaining_balance_val = 0.00
    else
      # NOTE: Amount basis for override
      amount_basis = self.amount

      if self.override_installment_interval == true
        amount_basis = self.original_loan_amount
      end

      if self.loan_product.maintaining_balance_exception == true
        maintaining_balance_exception_minimum_amount = self.loan_product.maintaining_balance_exception_minimum_amount.nil? ? 0 : self.loan_product.maintaining_balance_exception_minimum_amount
        #if amount_basis >= self.loan_product.maintaining_balance_exception_minimum_amount

        if amount_basis >= maintaining_balance_exception_minimum_amount
          self.maintaining_balance_val = (self.loan_product.default_maintaining_balance_val / 100) * amount_basis if (!amount_basis.nil? and !self.loan_product.nil?)
        else
          self.maintaining_balance_val = 0.00
        end
      else
        self.maintaining_balance_val = (self.loan_product.default_maintaining_balance_val / 100) * amount_basis if (!amount_basis.nil? and !self.loan_product.nil?)
      end
    end

    self.amount ||= 0.00

    # ID Deductions

    # Set the processing fee
    if !self.loan_product.nil? and !self.num_installments.nil?
      if self.business_permit_available == true
        self.processing_fee = self.loan_product_type.fixed_processing_fee
      elsif self.loan_product.always_have_processing_fee == true
        if self.loan_product_type.nil?
          self.processing_fee = 0
        else
          if self.num_installments == 15
            self.processing_fee = (self.loan_product_type.processing_fee_15 / 100) * self.amount
          elsif self.num_installments == 25
            self.processing_fee = (self.loan_product_type.processing_fee_25 / 100) * self.amount
          elsif self.num_installments == 35
            self.processing_fee = (self.loan_product_type.processing_fee_35 / 100) * self.amount
          elsif self.num_installments == 50
            self.processing_fee = (self.loan_product_type.processing_fee_50 / 100) * self.amount
          else
            if self.term == "semi-monthly" || self.term == "monthly"
              self.processing_fee  = (self.loan_product_type.processing_fee_15 / 100) * self.amount
            else
              self.processing_fee  = (self.loan_product_type.processing_fee_25 / 100) * self.amount
            end
          end
        end
      else
        if self.loan_product.waive_processing_fee_for_existing_entry_point_loans == true and self.loan_product.is_entry_point == true
          self.processing_fee = 0.00
        elsif self.loan_product.is_entry_point != true and Loan.active_entry_points.where("member_id = ?", self.member.id).count > 0
          self.processing_fee = 0.00
        elsif Loan.active_entry_points.where("member_id = ?", self.member.id).count > 0 and self.loan_product.is_entry_point != true and self.loan_product.waive_processing_fee_for_existing_entry_point_loans == true
          self.processing_fee = 0.00
        elsif Loan.active_entry_points.where("member_id = ?", self.member.id).count == 0 and self.loan_product.is_entry_point != true and self.loan_product.waive_processing_fee_for_existing_entry_point_loans == true
          self.processing_fee = 0.00
        else
          if self.loan_product_type.nil?
            self.processing_fee = 0
          else
            if self.num_installments == 15
              self.processing_fee = (self.loan_product_type.processing_fee_15 / 100) * self.amount
            elsif self.num_installments == 25
              self.processing_fee = (self.loan_product_type.processing_fee_25 / 100) * self.amount
            elsif self.num_installments == 35
              self.processing_fee = (self.loan_product_type.processing_fee_35 / 100) * self.amount
            elsif self.num_installments == 50
              self.processing_fee = (self.loan_product_type.processing_fee_50 / 100) * self.amount
            else
              #self.processing_fee  = (self.loan_product_type.processing_fee_25 / 100) * self.amount
              if self.term == "semi-monthly" || self.term == "monthly"
                self.processing_fee  = (self.loan_product_type.processing_fee_15 / 100) * self.amount
              else
                self.processing_fee  = (self.loan_product_type.processing_fee_25 / 100) * self.amount
              end
            end
          end
        end
      end
    end

    # Override processing fee if set to true
    self.processing_fee = 0.00 if self.override_processing_fee == true

    # Override Installment Interval
    if self.override_installment_interval == true
      self.num_installments = self.installment_override
      self.processing_fee = 0.00
      self.amount = self.old_principal_balance if self.amount.blank?
    end
  end

  def load_amount_released
    self.amount_released = Loans::ComputeAmountReleased.new(loan: self).execute! if self.pending?
  end

  def deduction_amount
    amount_released = 0.00
    if !self.amount_released.nil?
      amount_released = self.amount_released
    end
    self.amount - amount_released
  end

  def eir
    amount_released = 0.00
    if !self.amount_released.nil?
      amount_released = self.amount_released
    end
    result = ((self.amount - amount_released) + self.total_interest) / self.amount

    result
  end

  def update_remaining_balance!
    temp_remaining_balance = 0.00
    self.ammortization_schedule_entries.unpaid.each do |ase|
      temp_remaining_balance += ase.remaining_balance
    end

    self.update!(remaining_balance: temp_remaining_balance)
  end

  def total_interest
    self.temp_total_amount_due - self.amount
  end

  def temp_total_amount_due
    self.ammortization_schedule_entries.where("is_void IS NULL").sum(:amount)
  end

  def temp_total_amount_due_as_of(as_of)
    self.ammortization_schedule_entries.where("due_at <= ? AND is_void IS NULL", as_of).sum(:amount)
  end

  def temp_total_principal_paid_as_of(as_of)
    total = 0.00

    self.ammortization_schedule_entries.where("due_at <= ? AND is_void IS NULL". as_of).each do |a|
      total+= a.paid_principal
    end

    total
  end

  def temp_total_principal_amount_due_as_of(as_of)
    temp = self.ammortization_schedule_entries.where("due_at <= ? AND is_void IS NULL", as_of).sum(:principal)
  end

  def to_s
    pn_number
  end
end
