class MemberShare < ApplicationRecord
  STATUSES = ["pending", "approved"]

  validates :uuid, presence: true, uniqueness: true
  validates :certificate_number, presence: true, uniqueness: true
  validates :date_of_issue, presence: true
  validates :number_of_shares, presence: true, numericality: true
  validates :status, presence: true, inclusion: { in: STATUSES }

  belongs_to :member

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}" if self.uuid.nil?
      self.status = "pending"
      #self.number_of_shares = 1
      self.is_void != true
    end
  end

  def to_version_2_hash
    {
      id: self.uuid,
      member_id: self.member.uuid,
      certificate_number: self.certificate_number,
      date_of_issue: self.date_of_issue,
      number_of_shares: self.number_of_shares,
      data: {
        printed: self.printed,
        date_printed: self.date_printed
      }
    }
  end

  def to_transfer_hash
    {
      uuid: self.uuid,
      member_id: self.member.uuid,
      certificate_number: self.certificate_number,
      date_of_issue: self.date_of_issue,
      printed: self.printed,
      date_printed: self.date_printed,
      number_of_shares: self.number_of_shares
    }
  end


  def to_s
    certificate_number
  end
end
