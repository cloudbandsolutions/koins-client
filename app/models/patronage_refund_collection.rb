class PatronageRefundCollection < ApplicationRecord
  has_many :patronage_refund_collection_records
  validates :total_loan_interest_amount, presence: true, numericality: true
  validates :total_interest_amount, presence: true, numericality: true
  validates :patronage_rate, presence: true, numericality: true
 
  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.status = "pending"
    end
  end

end
