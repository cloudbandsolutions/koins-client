class UserActivity < ApplicationRecord
  validates :user_id, presence: true
  validates :uuid, presence: true, uniqueness: true
  validates :email, presence: true
  validates :username, presence: true
  
  before_validation :load_defaults

  def user
    User.where(id: self.user_id).first
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
    end
  end
end
