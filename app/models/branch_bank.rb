class BranchBank < ApplicationRecord
  belongs_to :branch
  belongs_to :bank

  def to_s
    bank
  end
end
