class MemberCertificate < ApplicationRecord
	belongs_to :member

 	validates :uuid, presence: true, uniqueness: true
 	validates :certificate_number, presence: true, uniqueness: true
 	validates :date_issue, presence: true
 	validates :number_of_certificates, presence: true, numericality: true
	
 	before_validation :load_defaults

 	def load_defaults
    	if self.new_record?
      	self.uuid = "#{SecureRandom.uuid}" if self.uuid.nil?
      	self.number_of_certificates = 1
      	self.is_void != true
    	end
  	end

  	def to_s
    	certificate_number
  	end
end
