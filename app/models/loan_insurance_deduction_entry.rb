class LoanInsuranceDeductionEntry < ApplicationRecord
  belongs_to :loan_product
  belongs_to :insurance_type
  belongs_to :accounting_code

  validates :loan_product, presence: true
  validates :insurance_type, presence: true
  validates :val, presence: true, numericality: true
  validates :accounting_code, presence: true
end
