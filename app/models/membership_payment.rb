class MembershipPayment < ApplicationRecord
  STATUSES = ["pending", "paid", "void"]
  belongs_to :membership_type
  belongs_to :member
  belongs_to :loan
  
  paginates_per 10
  max_paginates_per 100

  has_many :membership_account_payments
  accepts_nested_attributes_for :membership_account_payments

  validates :membership_type, presence: true
  validates :member, presence: true
  validates :paid_at, presence: true
  validates :uuid, presence: true, uniqueness: true
  validates :amount_paid, presence: true, numericality: true
  validates :status, presence: true, inclusion: { in: STATUSES }

  scope :paid, -> { where(status: "paid") }
  scope :pending, -> { where(status: "pending") }
  scope :void, -> { where(status: "void") }

  before_validation :load_defaults

  def load_defaults
    require 'application_helper'

    if self.new_record?
      self.paid_at = ApplicatioHelper.current_working_date if self.paid_at.nil?
      self.uuid = SecureRandom.uuid
      self.amount_paid = membership_type.try(:fee)
      self.status = "pending" if self.status.nil?
    end
  end

  def to_version_2_hash
    if self.membership_type.name == "K-KOOP"
      mem_type = "Cooperative"
    else
      mem_type = "Insurance"
    end
    {
      id: self.uuid,
      member_id: self.member.uuid,
      membership_name: self.membership_type.name,
      membership_type: mem_type,
      amount: self.amount_paid,
      date_paid: self.paid_at,
      status: self.status
    }
  end

  def paid?
    self.status == "paid"
  end

  def pending?
    self.status == "pending"
  end
end
