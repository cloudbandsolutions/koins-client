class TransferAdjustmentRecord < ApplicationRecord
  belongs_to :accounting_code
  belongs_to :transfer_adjustment

  validates :accounting_code, presence: true
  validates :amount, presence: true, numericality: true
end
