class YearEndClosingRecord < ApplicationRecord
  belongs_to :voucher, optional: true
end
