class SavingsAccount < ApplicationRecord
  attr_accessor :particular

  STATUSES = ["active", "inactive"]

  belongs_to :savings_type
  belongs_to :member
  belongs_to :branch
  belongs_to :center

  has_many :savings_account_transactions

  validates :account_number, presence: true, uniqueness: true
  validates :balance, presence: true, numericality: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :maintaining_balance, presence: true, numericality: true
  validates :savings_type, presence: true, uniqueness: { scope: :member_id }
  validates :member, presence: true
  validates :branch, presence: true
  validates :center, presence: true

  before_validation :load_defaults

  scope :active, -> { where(status: "active") }
  scope :inactive, -> { where(status: "inactive") }

  def to_version_2_hash
    {
      id: self.uuid,
      member_id: self.member.uuid,
      account_type: "SAVINGS",
      account_subtype: self.savings_type.to_s,
      balance: self.balance,
      center_id: self.member.center.uuid,
      branch_id: self.member.branch.uuid,
      status: self.status,
      maintaining_balance: self.maintaining_balance
    }
  end

  def to_s
    "#{member.full_name}"  
  end

  def load_defaults
    if self.new_record?
      self.status = "active"
      self.maintaining_balance = 0.00

      if self.uuid.nil?
        self.uuid = "#{SecureRandom.uuid}"
      end

      if self.balance.nil?
        self.balance = 0.00
      end

      if branch.nil?
        self.branch = self.member.try(:branch)
      end

      if center.nil?
        self.center = self.member.try(:center)
      end
    end
  end

  def zero_out!
    self.update(maintaining_balance: 0.00)
  end

  def activate!
    self.update!(status: "active")
  end

  def deactivate!
    self.update!(status: "inactive")
  end

  def to_hash
    sa = self.attributes.except!("id", "branch_id", "center_id", "member_id", "savings_type_id")
    sa[:branch_code] = self.branch.code
    sa[:center_code] = self.center.code
    sa[:member_identification_number] = self.member.identification_number
    sa[:savings_type_code] = self.savings_type.code

    sa
  end
end
