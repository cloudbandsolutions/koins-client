class SavingsAccountTransaction < ApplicationRecord
  TRANSACTION_TYPES = %w(withdraw wp deposit reversed interest tax fund_transfer_withdraw fund_transfer_deposit reverse_withdraw reverse_deposit)
  STATUSES = %w(pending approved reversed)

  DR_TYPES = [
    "withdraw",
    "wp",
    "tax",
  ]

  CR_TYPES = [
  ]

  belongs_to :savings_account
  belongs_to :loan_payment
  belongs_to :member
  belongs_to :savings_type
  belongs_to :bank
  belongs_to :accounting_code

  has_many :wp_records, dependent: :destroy
  accepts_nested_attributes_for :wp_records

  validates :savings_account, presence: true
  validates :transaction_number, presence: true, uniqueness: true
  validates :transaction_type, presence: true, inclusion: { in: TRANSACTION_TYPES }
  validates :amount, presence: true, numericality: true, sa_transaction_amount: true
  validates :transacted_at, presence: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :bank, presence: true
  validates :accounting_code, presence: true

  before_validation :load_defaults
  before_save :adjust_balances

  scope :approved_deposits, -> { where(status: "approved", transaction_type: %w(deposit)) }
  scope :approved_withdrawals, -> { where(status: "approved", transaction_type: %w(withdraw wp)) }
  scope :approved, -> { where(status: ["approved", "reversed"]) }
  scope :approved_and_reversed, -> { where(status: ["approved", "reversed"]) }
  scope :approved_credits, -> { where(status: "approved", transaction_type: ["deposit", "interest"]) }
  scope :approved_debits, -> { where(status: "approved", transaction_type: ["withdraw","wp", "tax"]) }

  def to_version_2_hash
    t_transaction_type    = self.transaction_type
    t_is_withdraw_payment = false
    t_is_interest         = false
    t_status              = self.status
    t_is_fund_transfer    = false

    if t_transaction_type == "wp"
      t_transaction_type    = "withdraw"
      t_is_withdraw_payment = true
    elsif t_transaction_type == "interest"
      t_transaction_type  = "deposit"
      t_is_interest       = true
    elsif t_transaction_type == "reverse_deposit"
      t_transaction_type  = "withdraw"
    elsif t_transaction_type == "reverse_withdraw"
      t_transaction_type  = "deposit"
    end

    t_for_resignation = (self.for_resignation == true)

    {
      id: self.uuid,
      subsidiary_id: self.savings_account.uuid,
      subsidiary_type: "MemberAccount",
      amount: self.amount,
      transaction_type: t_transaction_type,
      transacted_at: self.created_at,
      status: t_status,
      data: {
        is_withdraw_payment: t_is_withdraw_payment,
        is_fund_transfer: false,
        is_interest: t_is_interest,
        is_for_resignation: t_for_resignation,
        accounting_entry_reference_number: self.voucher_reference_number,
        accounting_entry_particular: self.particular,
        beginning_balance: 0.00,
        ending_balance: 0.00
      }
    }
  end

  def credit?
    ["deposit", "fund_transfer_deposit", "interest"].include? self.transaction_type
  end

  def debit?
    ["withdraw", "wp", "tax", "fund_transfer_withdraw"].include? self.transaction_type
  end

  def adjust_balances
    if self.status == "approved"
      if ["deposit", "fund_transfer_deposit", "interest", "reverse_withdraw"].include? self.transaction_type
        self.beginning_balance = SavingsAccount.find(self.savings_account.id).balance
        self.ending_balance = self.beginning_balance + self.amount
      elsif ["withdraw", "wp", "tax", "fund_transfer_withdraw", "reverse_deposit"].include? self.transaction_type
        self.beginning_balance = SavingsAccount.find(self.savings_account.id).balance
        self.ending_balance = self.beginning_balance - self.amount
      end
    end
  end

  def load_defaults
    if self.new_record?
      #self.transaction_number = SecureRandom.hex(4)
      self.transaction_number = "#{SecureRandom.uuid}"
      self.uuid = SecureRandom.uuid

      #if self.voucher_reference_number.nil?
      #  self.voucher_reference_number = VoucherService.voucher_number
      #end

      if self.transacted_at.nil?
        self.transacted_at = Time.now
        self.transaction_date = Time.now
      end

      if self.status.nil?
        self.status = "pending"
      end

      if self.is_withdraw_payment.nil?
        self.is_withdraw_payment = 'f'
      end

      if !self.savings_account.nil?
        self.bank = self.savings_account.branch.bank
        self.accounting_code = self.bank.accounting_code
      end
    end

    self.transaction_date = self.transacted_at
    self.member = self.savings_account.member if self.member.nil?
    self.savings_type = self.savings_account.savings_type if self.savings_type.nil?
  end

  def valid_for_reversal?
    valid = true

    current_balance = self.savings_account.balance
    maintaining_balance = self.savings_account.maintaining_balance
    amount = self.amount

    if self.transaction_type == "deposit" or self.transaction_type == "interest" or self.transaction_type == "fund_transfer_deposit"
      if current_balance - amount < maintaining_balance
        valid = false
      end
    end

    valid
  end

  def not_approved?
    self.status != "approved"
  end

  def approve!(approved_by)
    if self.status == "pending"
      self.update!(status: "approved", updated_at: self.transaction_date)
      self.generate_updates!
    else
      raise "Cannot approve non-pending transaction"
    end
  end

  def generate_updates!
    sa = SavingsAccount.find(self.savings_account_id)
    if self.transaction_type == "withdraw" or self.transaction_type == "wp" or self.transaction_type == "tax" or self.transaction_type == "fund_transfer_withdraw" or self.transaction_type == "reverse_deposit"

      updated_balance = sa.balance - self.amount  
      sa.update!(balance: updated_balance)
    elsif self.transaction_type == "deposit" or self.transaction_type == "interest" or self.transaction_type == "fund_transfer_deposit" or self.transaction_type == "reverse_withdraw"

      updated_balance = sa.balance + self.amount
      sa.update!(balance: updated_balance)
    end
  end

  def to_s
    transaction_number
  end
end
