class AppKey < ApplicationRecord
  validates :app_name, presence: true, uniqueness: true
  validates :api_key, presence: true, uniqueness: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.api_key = SecureRandom.hex
    end
  end

  def to_s
    app_name
  end
end
