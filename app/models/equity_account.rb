class EquityAccount < ApplicationRecord
  attr_accessor :particular

  STATUSES = ["active", "inactive"]

  belongs_to :equity_type
  belongs_to :member
  belongs_to :branch
  belongs_to :center

  has_many :equity_account_transactions

  validates :account_number, presence: true, uniqueness: true
  validates :balance, presence: true, numericality: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :equity_type, presence: true, uniqueness: { scope: :member_id }
  validates :member, presence: true
  validates :branch, presence: true
  validates :center, presence: true

  scope :active, -> { where(status: "active") }
  scope :inactive, -> { where(status: "inactive") }

  before_validation :load_defaults

  def to_version_2_hash
    {
      id: self.uuid,
      member_id: self.member.uuid,
      account_type: "EQUITY",
      account_subtype: self.equity_type.to_s,
      balance: self.balance,
      center_id: self.member.center.uuid,
      branch_id: self.member.branch.uuid,
      status: self.status,
      maintaining_balance: 0.00
    }
  end

  def to_s
    "#{account_number} - #{member.full_name}"  
  end

  def load_defaults
    if self.new_record?
      self.status = 'active'
      self.uuid = "#{SecureRandom.uuid}"
      self.balance = 0.00
      self.branch = self.member.try(:branch)
      self.center = self.member.try(:center)
    end
  end

  def activate!
    self.update!(status: "active")
  end

  def deactivate!
    self.update!(status: "inactive")
  end
end
