class MemberWithdrawalFundBalance < ApplicationRecord
  belongs_to :member_withdrawal
  validates :account_type, presence: true
  validates :amount, presence: true, numericality: true
  validates :withdrawal_amount, presence: true, numericality: true
end
