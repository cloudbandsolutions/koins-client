class BackgroundOperation < ApplicationRecord
	STATUSES=['done', 'error','processing']
	validates :status, presence: true, inclusion: {in: STATUSES}
	validates :started_at, presence: true
	validates :operation_type, presence: true

	def done?
    	self.status == 'done'
  	end
end
