class MemberAttachmentFile < ApplicationRecord
	belongs_to :member

	has_attached_file :file_photo,
    styles: {
      # medium: "300x150!",
      # list: "630x200!",
      large: "700x550!" },
    :convert_options => { :thumb => '-quality 50' },
    default_url: "/assets/:attachment/missing_:style.png"
  
  validates_attachment_content_type :file_photo, content_type: %w(image/jpeg image/jpg image/png image/bmp)
  validates_attachment_presence :file_photo
  validates :title, presence: true

  def to_s
    title
  end
end
