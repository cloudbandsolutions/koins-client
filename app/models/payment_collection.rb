class PaymentCollection < ApplicationRecord
  STATUSES = ["pending", "approved", "reversed"]
  PAYMENT_TYPES = [
    "billing", 
    "membership", 
    "insurance_remittance", 
    "insurance_fund_transfer",
    "insurance_withdrawal",
    "deposit", 
    "withdraw",
    "remittance",
    "income_insurance",
    "time_deposit",
    "equity_withdrawal"
  ]

  belongs_to :branch
  belongs_to :center
  belongs_to :accounting_fund

  belongs_to :use_withdrawal_accounting_code, class_name: 'AccountingCode', foreign_key: 'use_withdrawal_accounting_code_id'
  belongs_to :debit_accounting_code, class_name: 'AccountingCode', foreign_key: 'debit_accounting_code_id'
  belongs_to :credit_accounting_code, class_name: 'AccountingCode', foreign_key: 'credit_accounting_code_id'
  belongs_to :override_default_savings_dr_accounting_code, class_name: 'AccountingCode', foreign_key: 'override_default_savings_dr_accounting_code_id'

  paginates_per 10
  max_paginates_per 100

  has_many :payment_collection_records, dependent: :destroy
  accepts_nested_attributes_for :payment_collection_records

  validates :branch, presence: true
  #validates :center, presence: true, if: :withdraw?
  validates :uuid, presence: true, uniqueness: true
  validates :reference_number, presence: true, if: :approved?
  validates :or_number, presence: true, uniqueness: true, if: :for_or
  validates :total_amount, presence: true, numericality: true
  validates :total_wp_amount, presence: true, numericality: true
  validates :total_cp_amount, presence: true, numericality: true
  validates :total_principal_amount, presence: true, numericality: true
  validates :total_interest_amount, presence: true, numericality: true
  validates :prepared_by, presence: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :payment_type, presence: true, inclusion: { in: PAYMENT_TYPES }
  validates :paid_at, presence: true

  scope :approved, -> { where(status: "approved") }
  scope :pending, -> { where(status: "pending") }
  scope :reversed, -> { where(status: "reversed") }
  scope :billing, -> { where(payment_type: "billing") }
  scope :membership, -> { where(payment_type: "membership") }
  scope :insurance_remittance, -> { where(payment_type: "insurance_remittance") }
  scope :income_insurance, -> { where(payment_type: "income_insurance") }
  scope :insurance_fund_transfer, -> { where(payment_type: "insurance_fund_transfer") }
  scope :deposit, -> { where(payment_type: "deposit") }
  scope :deposit_for_resignation, -> { where(payment_type: "deposit", for_resignation: true) }
  scope :withdraw_for_resignation, -> { where(payment_type: "withdraw", for_resignation: true) }
  scope :withdraw, -> { where(payment_type: "withdraw") }
  scope :approved_billing, -> { where(status: "approved", payment_type: "billing") }
  scope :insurance_withdrawal, -> { where(payment_type: "insurance_withdrawal") }
  scope :equity_withdrawal, -> { where(payment_type: "equity_withdrawal") }
  scope :remittance, -> { where(payment_type: "remittance") }

  before_validation :load_defaults

  def to_version_2_billing_hash
    book  = "CRB"

    if self.payment_type == "billing" && self.special_report?
      book = "JVB"
    end

    {
      id: self.uuid,
      or_number: self.or_number,
      total_amount: self.total_amount,
      branch_id: self.branch.uuid,
      center_id: self.center.uuid,
      status: self.status,
      transacted_at: self.paid_at,
      collection_type: self.payment_type,
      data: {
        accounting_entry_reference_number: self.reference_number,
        accounting_entry_particular: self.particular,
        accounting_entry_book: book,
        total_wp_amount: self.total_wp_amount,
        total_cp_amount: self.total_cp_amount,
        total_principal_amount: self.total_principal_amount,
        total_interest_amount: self.total_interest_amount,
        prepared_by: self.prepared_by,
        approved_by: self.approved_by,
        check_number: self.check_number,
        check_voucher_number: self.check_voucher_number,
        date_of_check: self.date_of_check,
        check_name: self.check_name,
        for_resignation: self.for_resignation,
        collected_by: self.collected_by,
        checked_by: self.checked_by,
        encoded_by: self.encoded_by,
        is_special_report: self.is_special_report
      }
    }
  end

  def for_adjustment?
    self.for_adjustment == true
  end

  def centers
    d = []
    self.payment_collection_records.joins(member: :center).each do |pcr|
      d << pcr.member.center.id
    end

    Center.where(id: d.uniq).pluck(:name)
  end

  def correct_savings_deposits!
    temp_total_amount = 0.00
    savings_transactions = CollectionTransaction.where(account_type: "SAVINGS", payment_collection_record_id: self.payment_collection_records.pluck(:id))
    savings_transactions.each do |savings_transaction|
      amount          = savings_transaction.amount
      member          = savings_transaction.payment_collection_record.member
      savings_type    = SavingsType.where(code: savings_transaction.account_type_code).first
      savings_account = SavingsAccount.where(savings_type_id: savings_type.id, member_id: member.id).first

      if amount > 0
        temp_total_amount += amount
        savings_account_transaction = SavingsAccountTransaction.create!(
                                        amount: amount,
                                        transacted_at: self.paid_at,
                                        transaction_type: "deposit",
                                        particular: self.particular,
                                        voucher_reference_number: self.reference_number,
                                        savings_account: savings_account
                                      )

        savings_account_transaction.approve!(self.approved_by)
      end
    end
  end

  def correct_missing_loan_payments!
    payment_collection_records.each do |record|
      if self.status == "approved" and record.pending_loan_payment?
        if record.loan_payment
          record.loan_payment.approve_payment!
        end
      end
    end
  end

  def special_report?
    self.is_special_report == true
  end

  def for_or
    if ["membership", "insurance_remittance", "deposit"].include?(self.payment_type)
      if self.approved?
        if self.book != 'JVB'
          true
        end
      end
    end
  end

  def non_withdrawal?
    payment_type != "withdraw"
  end

  def insurance_withdrawal?
    payment_type == "insurance_withdrawal"
  end

  def equity_withdrawal?
    payment_type == "equity_withdrawal"
  end

  def withdraw?
    payment_type == "withdraw"
  end

  def insurance_remittance?
    payment_type == "insurance_remittance"
  end

  def income_insurance?
    payment_type == "income_insurance"
  end


  def deposit?
    payment_type == "deposit"
  end

  def billing?
    payment_type == "billing"
  end

  def membership?
    payment_type == "membership"
  end

  def insurance_fund_transfer?
    payment_type == "insurance_fund_transfer"
  end

  def has_paid_loans?
    ret = false
    self.loans.each do |loan|
      if loan.paid?
        ret = true
      end
    end

    ret
  end

  def loans
    loans = []
    self.payment_collection_records.each do |payment_collection_record|
      payment_collection_record.collection_transactions.where(account_type: "LOAN_PAYMENT").each do |collection_transaction|
        loans << collection_transaction.loan_payment.loan
      end
    end

    loans
  end

  def total_savings_balance(savings_type, loan_product)
    member_ids = self.payment_collection_records.where(loan_product_id: loan_product.id).pluck(:member_id).uniq
    SavingsAccount.where(savings_type_id: savings_type.try(:id), member_id: member_ids).sum(:balance)
  end

  def total_loan_amount_released(loan_product)
    member_ids = self.payment_collection_records.where(loan_product_id: loan_product.id).pluck(:member_id).uniq
    Loan.active.where(loan_product_id: loan_product.id, member_id: member_ids).sum(:amount_released)
  end

  def total_loan_amount_balance(loan_product)
    member_ids = self.payment_collection_records.where(loan_product_id: loan_product.id).pluck(:member_id).uniq
    Loan.active.where(loan_product_id: loan_product.id, member_id: member_ids).sum(:remaining_balance)
  end

  def total_amount_by_loan_product(loan_product)
    payment_collection_records.where(loan_product_id: loan_product.id).sum(:total_cp_amount) + payment_collection_records.where(loan_product_id: loan_product.id).sum(:total_wp_amount)
  end

  def total_cp_amount_by_loan_product(loan_product)
    payment_collection_records.where(loan_product_id: loan_product.id).sum(:total_cp_amount)
  end

  def total_wp_amount_by_loan_product(loan_product)
    payment_collection_records.where(loan_product_id: loan_product.id).sum(:total_wp_amount)
  end

  def total_amount_by_insurance_type_and_loan_product(insurance_type, loan_product)
    t = 0.00
    payment_collection_records.where(loan_product_id: loan_product.id).each do |a|
      t += a.collection_transactions.where(account_type: "INSURANCE", account_type_code: insurance_type.code).sum(:amount)
    end

    t
  end

  def total_amount_by_equity_type_and_loan_product(equity_type, loan_product)
    t = 0.00
    payment_collection_records.where(loan_product_id: loan_product.id).each do |a|
      t += a.collection_transactions.where(account_type: "EQUITY", account_type_code: equity_type.code).sum(:amount)
    end

    t
  end

  def total_amount_by_savings_type_and_loan_product(savings_type, loan_product)
    t = 0.00
    payment_collection_records.where(loan_product_id: loan_product.id).each do |a|
      t += a.collection_transactions.where(account_type: "SAVINGS", account_type_code: savings_type.code).sum(:amount)
    end

    t
  end

  def total_by_savings_type(savings_type)
    t = 0.00
    payment_collection_records.each do |a|
      t += a.collection_transactions.where(account_type: "SAVINGS", account_type_code: savings_type.code).sum(:amount)
    end

    t
  end

  def total_by_insurance_type(insurance_type)
    t = 0.00
    payment_collection_records.each do |a|
      t += a.collection_transactions.where(account_type: "INSURANCE", account_type_code: insurance_type.code).sum(:amount)
    end

    t
  end

  def total_by_equity_type(equity_type)
    t = 0.00
    payment_collection_records.each do |a|
      t += a.collection_transactions.where(account_type: "EQUITY", account_type_code: equity_type.code).sum(:amount)
    end

    t
  end

  def total_lp_by_loan_product(loan_product)
    t = 0.00
    payment_collection_records.where(loan_product_id: loan_product.id).each do |a|
      t += a.collection_transactions.where(account_type: "LOAN_PAYMENT").sum(:amount)
    end

    t
  end

  def total_loan_balance_by_loan_product(loan_product)
    member_ids = payment_collection_records.where(loan_product_id: loan_product.id).pluck(:member_id)
    Loan.active.where(loan_product_id: loan_product.id, member_id: member_ids).sum(:remaining_balance)
  end

  def total_loan_amount_by_loan_product(loan_product)
    member_ids = payment_collection_records.where(loan_product_id: loan_product.id).pluck(:member_id)
    Loan.active.where(loan_product_id: loan_product.id, member_id: member_ids).sum(:amount)
  end

  def total_cbu_by_loan_product(loan_product)
    member_ids = payment_collection_records.where(loan_product_id: loan_product.id).pluck(:member_id)
    SavingsAccount.where(member_id: member_ids, savings_type_id: SavingsType.default).sum(:balance)
  end

  def records_by_loan_product(loan_product)
    #payment_collection_records.where(loan_product_id: loan_product.id).joins(:member).order("members.last_name")
    payment_collection_records.where(loan_product_id: loan_product.id).sort_by{ |o| o.member.last_name }
  end

  def loan_products
    LoanProduct.where(id: self.payment_collection_records.pluck(:loan_product_id).uniq)
  end

  def pending?
    self.status == "pending"
  end

  def approved?
    self.status == "approved"
  end

  def reversed?
    self.status == "reversed"
  end

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
      self.status = "pending"
      self.payment_type = "billing" if self.payment_type.nil?
      self.or_number = "#{Time.now.to_i} CHANGE-ME" if self.or_number.nil?
    end

    if self.or_number.present?
      self.or_number = self.or_number.rjust(5, "0")
    end

    compute_values if self.pending?
  end

  def compute_values
    self.total_principal_amount = 0.00
    self.total_interest_amount = 0.00
    self.total_wp_amount = 0.00
    self.total_cp_amount = 0.00
    self.total_amount = 0.00
    self.payment_collection_records.each do |payment_collection_record|
      payment_collection_record.collection_transactions.each do |collection_transaction|
        if collection_transaction.account_type == "WP" and !collection_transaction.amount.nil?
          self.total_wp_amount += collection_transaction.amount
          self.total_cp_amount -= collection_transaction.amount
        else
          if !collection_transaction.amount.nil?
            self.total_cp_amount += collection_transaction.amount
            if collection_transaction.account_type == "LOAN_PAYMENT"
              loan_payment = collection_transaction.loan_payment 
              self.total_principal_amount += loan_payment.payment_stats[:principal]
              self.total_interest_amount += loan_payment.payment_stats[:interest]
            end
          end
        end
      end
    end

    self.total_amount = self.total_wp_amount + self.total_cp_amount
  end

  def accounting_entry
    Voucher.where(particular: self.particular, reference_number: self.reference_number, date_posted: self.paid_at).try(:first)
  end
end
