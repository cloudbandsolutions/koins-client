class LoanDeduction < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  before_validation :load_defaults

  def load_defaults
    if self.is_one_time_fee.nil?
      self.is_one_time_fee = 'f'
    end
  end

  def to_s
    name
  end
end
