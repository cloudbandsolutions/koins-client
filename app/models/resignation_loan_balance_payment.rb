class ResignationLoanBalancePayment < ApplicationRecord
  ACCOUNT_TYPES = ["SAVINGS", "EQUITY", "INSURANCE"]
  belongs_to :resignation_loan_balance
  validates :amount, presence: true, numericality: true
  validates :uuid, presence: true, uniqueness: true
  validates :account_type_code, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
    end
  end
end
