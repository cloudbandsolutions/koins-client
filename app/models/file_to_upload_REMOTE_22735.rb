class FileToUpload < ApplicationRecord
	PROCESS_TYPES = ["members", "legal_dependents", "beneficiaries", "insurance_accounts", "insurance_account_transactions"]

	def load_defaults
    if self.new_record?
      self.status = 'processing'
    end
  end
end
