class FeedbackOption < ApplicationRecord
  validates :content, presence: true
end
