class CollectionTransaction < ApplicationRecord
  ACCOUNT_TYPES = [
    "LOAN_PAYMENT", 
    "SAVINGS", 
    "INSURANCE", 
    "EQUITY", 
    "WP", 
    "MEMBERSHIP_PAYMENT", 
    "INSURANCE_FUND_TRANSFER", 
    "ID_PAYMENT"
  ]

  belongs_to :payment_collection_record
  belongs_to :loan_payment, dependent: :destroy

  validates :amount, presence: true, numericality: true
  validates :account_type, presence: true, inclusion: { in: ACCOUNT_TYPES }

  validate :enough_funds
  validate :positive_amount

  validate :validate_loan_amount_payment, if: :loan_payment?

  before_validation :load_defaults

  scope :loan_payments, -> { where(account_type: "LOAN_PAYMENT") }

  after_save do
    if self.account_type == "LOAN_PAYMENT"
      self.loan_payment.update!(loan_payment_amount: self.amount)
    end

    payment_collection_record.touch
  end

  def validate_loan_amount_payment
    loan  = loan_payment.loan
    if amount > loan.remaining_balance
      errors.add(:amount, "Overpayment: Loan ID: #{loan.id} Remaining Balance: #{loan.remaining_balance} Amount Suggested: #{amount}")
    end
  end

  def loan_payment?
    self.account_type == "LOAN_PAYMENT"
  end

  def positive_amount
    if !amount.nil?
      if amount < 0
        errors.add(:amount, "Number should be positive")
      end
    end
  end

  def enough_funds
    if self.account_type == "WP" and !payment_collection_record.nil?
      savings_account = SavingsAccount.joins(:savings_type).where("savings_accounts.member_id = ? AND savings_types.is_default = ?", payment_collection_record.member.id, true).first
      raise "No savings account found" if savings_account.nil?

#      if (savings_account.balance - amount) < savings_account.maintaining_balance
#        errors.add(:amount, "Not enough funds to withdraw for member #{savings_account.member.full_name_titleize}")
#      end
    end

    if ["SAVINGS", "INSURANCE", "EQUITY"].include? self.account_type and !payment_collection_record.nil? and !payment_collection_record.try(:payment_collection).nil? and ["withdraw", "insurance_withdrawal"].include? payment_collection_record.try(:payment_collection).try(:payment_type)
      if self.account_type == "SAVINGS"
        savings_type = SavingsType.where(code: self.account_type_code).first
        savings_account = SavingsAccount.joins(:savings_type).where("savings_accounts.member_id = ? AND savings_types.id = ?", payment_collection_record.member.id, savings_type.id).first
        raise "No savings account found" if savings_account.nil?

        if (savings_account.balance - amount) < savings_account.maintaining_balance
          errors.add(:amount, "Not enough funds to withdraw #{self.amount}. Maintaining balance: #{savings_account.maintaining_balance}")
        end
#      elsif self.account_type == "INSURANCE"
#        insurance_type = InsuranceType.where(code: self.account_type_code).first
#        insurance_account = InsuranceAccount.joins(:insurance_type).where("insurance_accounts.member_id = ? AND insurance_types.id = ?", payment_collection_record.member.id, insurance_type.id).first
#        raise "No insurance account found" if insurance_account.nil?
#
#        if (insurance_account.balance - amount) < 0
#          errors.add(:amount, "Not enough funds to withdraw #{self.amount}. Current Balance: #{insurance_account.balance}. Result: #{insurance_account.balance - amount}")
#        end
      elsif self.account_type == "EQUITY"
        equity_type = EquityType.where(code: self.account_type_code).first
        equity_account = EquityAccount.joins(:equity_type).where("equity_accounts.member_id = ? AND equity_types.id = ?", payment_collection_record.member.id, equity_type.id).first
        raise "No equity account found" if equity_account.nil?

        if (equity_account.balance - amount) < 0
          errors.add(:amount, "Not enough funds to withdraw #{self.amount}. Current Balance: #{equity_account.balance}")
        end
      end
    end
  end

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}" if self.uuid.nil?
    end
  end

  def is_loan_payment?
    loan_payment.nil?
  end
end
