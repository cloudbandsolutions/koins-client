class DeceasedAndTotalAndPermanentDisabilityRecord < ApplicationRecord
	CLASSIFICATIONS = ["Deceased", "Total and Permanent Disability"] 

	belongs_to :legal_dependent
 	belongs_to :deceased_and_total_and_permanent_disability

	validates :legal_dependent, presence: true
  
 	before_validation :load_defaults

  	after_save do
    	deceased_and_total_and_permanent_disability.touch
  	end

	def load_defaults
		if self.new_record?
	  		self.status = "pending"
		end
	end

	def pending?
		self.status == "pending"
	end

	def approved?
		self.status == "approved"
	end
end
