class AccountingFund < ApplicationRecord
	validates :name, presence: true, uniqueness: true

  has_many :vouchers

  before_validation :load_defaults

  def prefix
    self.name.split(" ").map{ |o| o.split('').first.try(:upcase) }.join
  end

  def to_s
    name
  end

  def load_defaults
    if self.uuid.blank?
      self.uuid = "#{SecureRandom.uuid}"
    end
  end

  def to_version_2_hash
    {
      id: self.uuid,
      name: self.name
    }
  end
end
