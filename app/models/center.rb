class Center < ApplicationRecord
  MEETING_DAYS = %w(Monday Tuesday Wednesday Thursday Friday Saturday Sunday)

  belongs_to :branch
  belongs_to :user

  has_many :members
  has_many :collection_payments

  validates :uuid, presence: true
  validates :name, presence: true, uniqueness: { scope: :branch_id }
  validates :code, presence: true, uniqueness: { scope: :branch_id }
  #validates :abbreviation, presence: true, uniqueness: true
  validates :meeting_day, presence: true, inclusion: { in: MEETING_DAYS }
  validates :branch, presence: true

  before_validation :load_defaults

  def to_version_2_hash
    {
      id: self.uuid,
      name: self.name,
      short_name: self.code,
      branch_id: self.branch.uuid
    }
  end

  def load_defaults
    self.name = self.name.upcase if !self.name.nil?

    if self.new_record?
      self.code = SecureRandom.uuid
      self.uuid = SecureRandom.uuid
    end

    if !self.user.blank?
      self.officer_in_charge = user.full_name_upcase
    else
      self.officer_in_charge = nil
    end

    if self.officer_in_charge.present?
      self.officer_in_charge = self.officer_in_charge.upcase
    end

    self.abbreviation = self.name if !self.name.nil?
  end

  def active_member_count
    members.where(id: Loan.active.where(center_id: self.id).pluck(:member_id).uniq).count
    #members.where(status: 'active').count
    #Loan.joins(:member).where("members.center_id = ?", self.id).pluck("members.id").uniq.count
  end

  def pending_member_count
    members.where(status: "pending").count
  end

  def member_count
    Member.joins(:center).count
  end

  def pending_collection_payments
    self.collection_payments.where(status: "pending")
  end

  def users
    UserBranch.where(branch_id: self.branch.id)
  end

  def to_s
    name
  end

  def to_hash
    data = {}
    data[:name] = self.name
    data[:code] = self.code
    data[:abbreviation] = self.abbreviation
    data[:address] = self.address
    data[:meeting_day] = self.meeting_day
    data[:branch_id] = self.branch_id

    data
  end
end
