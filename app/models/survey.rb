class Survey < ActiveRecord::Base
  has_many :survey_questions, dependent: :destroy
  accepts_nested_attributes_for :survey_questions, allow_destroy: true

  has_many :survey_respondents, dependent: :destroy

  validates :title, presence: true, uniqueness: true
  validates :uuid, presence: true, uniqueness: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record? and self.uuid.nil?
      self.uuid = "#{SecureRandom.uuid}"
    end
  end

  def num_questions
    survey_questions.count
  end

  def num_respondents
    survey_respondents.count
  end

  def to_s
    title
  end
end
