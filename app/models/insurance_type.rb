class InsuranceType < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  validates :monthly_tax_rate, presence: true, numericality: true
  validates :monthly_interest_rate, presence: true, numericality: true
  validates :default_periodic_payment, presence: true, numericality: true

  belongs_to :withdraw_accounting_code, class_name: 'AccountingCode', foreign_key: 'withdraw_accounting_code_id'
  belongs_to :deposit_accounting_code, class_name: 'AccountingCode', foreign_key: 'deposit_accounting_code_id'

  validates :resign_rule_num_years, presence: true, numericality: true, if: :use_resign_rules?
  validates :resign_rule_withdrawal_percentage, presence: true, numericality: true, if: :use_resign_rules?

  def use_resign_rules?
    self.use_resign_rules == true
  end

  def to_s
    name
  end
end
