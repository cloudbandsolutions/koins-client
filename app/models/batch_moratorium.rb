class BatchMoratorium < ApplicationRecord
  STATUSES = ["pending", "approved"]

  validates :uuid, presence: true, uniqueness: true
  validates :date_initialized,  presence: true
  #validates :start_of_moratorium, presence: true
  validates :status, presence: true, inclusion: { in: STATUSES }
  validates :number_of_days, presence: true
  validates :initialized_by, presence: true

  belongs_to :center

  before_validation :load_defaults

  scope :pending, -> { where(status: "pending") }

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
      self.status = "pending"
    end
  end

  def pending?
    self.status ==  "pending"
  end

  def approved?
    self.status ==  "approved"
  end

  def approve!(temp_approved_by)
    self.update!(
      approved_by: temp_approved_by,
      status: "approved"
    )
  end
end
