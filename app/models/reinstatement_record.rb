class ReinstatementRecord < ApplicationRecord
  	belongs_to :member
 	belongs_to :reinstatement

	validates :member, presence: true  
  
 	before_validation :load_defaults

  	after_save do
    	reinstatement.touch
  	end

	def load_defaults
		if self.new_record?
	  		self.status = "pending"
		end
	end

	def pending?
		self.status == "pending"
	end

	def approved?
		self.status == "approved"
	end
end
