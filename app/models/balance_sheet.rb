class BalanceSheet < ApplicationRecord
  validates :as_of, presence: true
  validates :data, presence: true
  validates :branch_name, presence: true

  # A = L + E
  def status
    self.data['assets_entries']['total_net'].to_f == self.data['liabilities_entries']['total_net'].to_f + data['equity_entries']['total_net'].to_f + (data['revenue_entries']['total_net'].to_f - data['expenses_entries']['total_net'].to_f)
  end
end
