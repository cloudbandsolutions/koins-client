class SubsidiaryAdjustment < ApplicationRecord
  belongs_to :branch
  belongs_to :accounting_fund

  validates :branch, presence: true
  validates :uuid, presence: true, uniqueness: true
  validates :status, presence: true, inclusion: { in: ["pending", "approved"] }
  validates :prepared_by, presence: true
  validates :approved_by, presence: true, if: :approved?
  validates :date_approved, presence: true, if: :approved?
  validates :voucher_reference_number, presence: true, if: :approved?
  validates :particular, presence: true

  has_many :subsidiary_adjustment_records, dependent: :delete_all
  accepts_nested_attributes_for :subsidiary_adjustment_records

  has_many :subsidiary_adjustment_entries, dependent: :delete_all
  accepts_nested_attributes_for :subsidiary_adjustment_entries

  before_validation :load_defaults

  validate :has_more_than_one_record, if: :approved?
  validate :particular, :balanced_entries, if: :approved?

  def has_more_than_one_record
    if subsidiary_adjustment_records.size == 0
      errors.add(:particular, "This should have more than one entry for records")
    end
  end

  def approved?
    self.status == 'approved'
  end

  def pending?
    self.status == 'pending'
  end

  def load_defaults
    if self.new_record?
      self.uuid = SecureRandom.uuid
      self.status = "pending"
    end
  end

  def balanced_entries
    dr = 0
    cr = 0

    subsidiary_adjustment_records.each do |r|
      if r.adjustment_type == 'credit'
        dr += r.amount
      elsif r.adjustment_type == 'debit'
        cr += r.amount
      end
    end

    if cr != dr
      errors.add(:particular, "Unbalanced entries")
    end
  end
end
