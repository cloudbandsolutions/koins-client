class InsuranceAccountValidation < ApplicationRecord
	STATUSES = ["pending", "for-approval", "approved", "reversed", "for-validation", "cancelled"]

	belongs_to :branch

	has_many :insurance_account_validation_records, dependent: :destroy
	has_many :insurance_account_validation_cancellations, dependent: :destroy
 	accepts_nested_attributes_for :insurance_account_validation_records

	validates :branch, presence: true
	validates :date_prepared, presence: true
	validates :prepared_by, presence: true
	# validates :or_number, presence: true
	validates :status, presence: true, inclusion: { in: STATUSES }
	validates :total_rf, presence: true, numericality: true
	validates :total_50_percent_lif, presence: true, numericality: true
	validates :total_advance_lif, presence: true, numericality: true
	validates :total_advance_rf, presence: true, numericality: true
	validates :total_interest, presence: true, numericality: true
	validates :total, presence: true, numericality: true
	# validates :total_equity_interest, presence: true, numericality: true

	before_validation :load_defaults

  	def validated?
    	self.validated_by.present? && self.date_validated.present?
  	end

  	def for_approval?
    	self.status == "for-approval"
  	end

  	def for_validation?
    	self.status == "for-validation"
  	end

	def pending?
		self.status == "pending"
	end

	def cancelled?
		self.status == "cancelled"
	end

	def approved?
		self.status == "approved"
	end

	def reversed?
		self.status == "reversed"
	end

	def load_defaults
		if self.new_record?
		  self.status = "pending"
		end
   
    	if self.uuid.nil?
      		self.uuid = "#{SecureRandom.uuid}"
    	end

		compute_values if self.pending? || self.cancelled? 
	end

	def compute_values
	    self.total_rf = 0.00
	    self.total_50_percent_lif = 0.00
	    self.total_equity_interest = 0.00
	    self.total_advance_lif = 0.00
	    self.total_advance_rf = 0.00
	    self.total_interest = 0.00
	    self.total_policy_loan = 0.00
	    self.total = 0.00
	    self.insurance_account_validation_records.each do |insurance_account_validation_record|
	        if !insurance_account_validation_record.rf.nil? && !insurance_account_validation_record.lif_50_percent.nil? && !insurance_account_validation_record.equity_interest.nil? && !insurance_account_validation_record.advance_lif.nil? && !insurance_account_validation_record.advance_rf.nil? && !insurance_account_validation_record.interest.nil? && !insurance_account_validation_record.policy_loan.nil? && !insurance_account_validation_record.total.nil? 
	            self.total_rf += insurance_account_validation_record.rf
	            self.total_50_percent_lif += insurance_account_validation_record.lif_50_percent
	            self.total_equity_interest += insurance_account_validation_record.equity_interest
	            self.total_advance_lif += insurance_account_validation_record.advance_lif
	            self.total_advance_rf += insurance_account_validation_record.advance_rf
	            self.total_interest += insurance_account_validation_record.interest
	            self.total_policy_loan += insurance_account_validation_record.policy_loan
	            self.total += insurance_account_validation_record.total
	        end  
	    end
  	end

  	def to_version_2_hash
	    if self.uuid.nil?
      		self.update!(uuid: "#{SecureRandom.uuid}")
    	end

    {
      id: self.uuid,
      branch_id: self.branch.uuid,
      date_prepared: self.date_prepared,
      status: self.status,
      prepared_by: self.prepared_by,
      approved_by: self.approved_by,
      created_at: self.created_at,
      updated_at: self.updated_at,
      particular: self.particular,
      reference_number: self.reference_number,
      total: self.total,
      or_number: self.or_number,
      date_approved: self.date_approved,
      date_validated: self.date_validated,
      validated_by: self.validated_by,
      date_checked: self.date_checked,
      checked_by: self.checked_by,
      date_cancelled: self.date_cancelled,
      cancelled_by: self.cancelled_by,
      is_remote: self.is_remote,
      total_rf: self.total_rf,
      total_50_percent_lif: self.total_50_percent_lif,
      total_advance_lif: self.total_advance_lif,
      total_advance_rf: self.total_advance_rf,
      total_interest: self.total_interest,
      total_equity_interest: self.total_equity_interest
    } 
  end
end
