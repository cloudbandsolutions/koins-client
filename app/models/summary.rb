class Summary < ApplicationRecord
  SUMMARY_TYPES = [
    "MEMBER_COUNTS"
  ]

  validates :summary_type, presence: true, inclusion: { in: SUMMARY_TYPES }
  validates :as_of, presence: true
  validates :meta, presence: true

  scope :member_counts, -> { where(summary_type: "MEMBER_COUNTS") }
end
