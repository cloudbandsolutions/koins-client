class LoanProductMembershipPayment < ApplicationRecord
  belongs_to :loan_product
  belongs_to :membership_type

  validates :loan_product, presence: true
  validates :membership_type, presence: true
end
