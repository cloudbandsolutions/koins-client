class MonthlyReport < ApplicationRecord
  REPORT_TYPES = [
    "new-mfi-member-count", 
    "resigned-mfi-member-count"
  ]

  validates :generated_by, presence: true
  validates :date_generated, presence: true
  validates :data, presence: true
  validates :month, presence: true, numericality: true
  validates :year, presence: true, numericality: true
  validates :branch_code, presence: true
  validates :report_type, presence: true, inclusion: { in: REPORT_TYPES }

  scope :new_mfi_member_count, -> { where(report_type: "new-mfi-member-count") }
  scope :resigned_mfi_member_count, -> { where(report_type: "resigned-mfi-member-count") }
end
