class MajorGroup < ApplicationRecord
  DC_CODES = %w(DR CR)

  has_many :major_accounts

  validates :name, presence: true, uniqueness: true
  validates :code, presence: true
  validates :dc_code, presence: true, inclusion: { in: DC_CODES }

  def to_s
    name
  end
end
