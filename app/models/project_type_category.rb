class ProjectTypeCategory < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true

  has_many :project_types

  def to_version_2_hash
    if self.uuid.blank?
      self.update!(uuid: SecureRandom.uuid)
    end

    {
      id: self.uuid,
      name: self.name,
      code: self.code
    }
  end
end
