class AggregatedReport < ApplicationRecord
  validates :uuid, presence: true, uniqueness: true
  validates :data, presence: true
  validates :report_date, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.uuid = "#{SecureRandom.uuid}"
    end
  end
end
