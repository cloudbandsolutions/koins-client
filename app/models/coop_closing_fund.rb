class CoopClosingFund < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :ratio, presence: true, numericality: true

  def to_s
    name
  end
end
