class Beneficiary < ApplicationRecord
  RELATIONSHIP = ["Asawa", "Anak", "Magulang", "Kapatid", "Iba pa"]
  belongs_to :member

  validates :reference_number, presence: true, uniqueness: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  #validates :date_of_birth, :check_date_of_birth, presence: true, if: :new_record?
  validates :relationship, presence: true
  #validates :beneficiary_for, presence: true, inclusion: { in: ["microinsurance", "microfinance"] }

  before_validation :load_defaults

  def to_version_2_hash
    {
      id: self.uuid,
      member_id: self.member.uuid,
      first_name: self.first_name,
      middle_name: self.middle_name,
      last_name: self.last_name,
      relationship: self.relationship,
      date_of_birth: self.date_of_birth,
      is_primary: self.is_primary || nil,
      is_deceased: self.is_deceased || nil
    }
  end

  def date_of_birth_formatted
    if self.date_of_birth.nil?
      "Please set date of birth"
    else
      begin
        self.date_of_birth.strftime("%b %d, %Y")
      rescue Exception
        "ERR IN DATE OF BIRTH: #{self.date_of_birth}"
      end
    end
  end

  def full_name_upcase
    "#{first_name.try(:upcase)} #{middle_name.try(:upcase)} #{last_name.try(:upcase)}"
  end

  def load_defaults
    if self.new_record? and reference_number.nil?
      self.reference_number = "#{SecureRandom.hex(4)}"
    end

    if self.uuid.nil?
      self.uuid = "#{SecureRandom.uuid}"
    end
  end

  def check_date_of_birth
    if self.date_of_birth.present?
      years_old = ((Time.now.to_date - self.date_of_birth)/365.242199).to_i
      max_age = Settings.max_age_for_microinsurance_beneficiary ||= 18
      if max_age > years_old 
        errors.add(:date_of_birth, "Beneficiary must be 18 years old above.")
      else
        now = Time.now.utc.to_date
        now.year - self.date_of_birth.year - (self.date_of_birth.to_date.change(:year => now.year) > now ? 1 : 0)
      end
    end
  end

  def age
    if self.date_of_birth.nil?
      "Please set date of birth"
    else
      begin
        now = Time.now.utc.to_date
        now.year - self.date_of_birth.year - ((now.month > self.date_of_birth.month || (now.month == self.date_of_birth.month && now.day >= self.date_of_birth.day)) ? 0 : 1)
        # now.year - self.date_of_birth.year - (self.date_of_birth.to_date.change(:year => now.year) > now ? 1 : 0)
      rescue Exception
        "ERR IN DATE OF BIRTH: #{self.date_of_birth}"
      end
    end
  end

  def to_hash
    b = self.attributes.except!("id", "member_id")
    b[:member_identification_number] = self.member.identification_number

    return b
  end

end
