class MemberLoanCycle < ApplicationRecord
  belongs_to :member
  belongs_to :loan_product

  validates :loan_product, presence: true
  validates :member, presence: true
  validates :cycle, presence: true, numericality: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
    end
  end

  def cycle_count
    self.cycle
  end

  def to_version_2_hash
    {
      loan_product_id: self.loan_product.uuid,
      member_id: self.member.uuid,
      cycle: self.cycle
    }
  end
end
