class LoanPaymentInsuranceAccountDeposit < ApplicationRecord
  belongs_to :loan_payment
  belongs_to :insurance_account
  


  validates :amount, presence: true, numericality: true


end
