class InterestOnShareCapitalCollection < ApplicationRecord
  has_many :interest_on_share_capital_collection_records
  validates :total_equity_amount, presence: true, numericality: true

  validates :total_interest_amount, presence: true, numericality: true 

  validates :collection_date, presence: true

  before_validation :load_defaults

  def load_defaults
    if self.new_record?
      self.status = "pending"
    end
  end
end
