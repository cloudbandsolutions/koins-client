module ApplicationHelper
  def resign_types
    data = []
    Settings.member_resignation_types.each do |r|
      data << r[:name]
    end

    data
  end

  def age(dob)
    now = Time.now.utc.to_date
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end

  def insurance_account_monitor_enabled?
    Settings.insurance_type_priority and Settings.insurance_type_secondary
  end

  def self.remote_user?(user)
    ["REMOTE-OAS", "REMOTE-BK", "REMOTE-MIS", "REMOTE-FM"].include?(user.role)
  end

  def self.not_remote_user?(user)
    !["REMOTE-OAS", "REMOTE-BK", "REMOTE-MIS", "REMOTE-FM"].include?(user.role)
  end
  
  def remote_user?(user)
    ["REMOTE-OAS", "REMOTE-BK", "REMOTE-MIS", "REMOTE-FM"].include?(user.role)
  end

  def not_remote_user?(user)
    !["REMOTE-OAS", "REMOTE-BK", "REMOTE-MIS", "REMOTE-FM"].include?(user.role)
  end

  def current_loan_cycle_counts
    data  = Reports::GenerateLoanCycleStats.new.execute!
  end

  def latest_announcement
    Announcement.last
  end

  def latest_daily_report_timestamp
    daily_report  = DailyReport.last
    if daily_report.blank?
      "n/a"
    else
      hour  = daily_report.created_at.hour
      min   = daily_report.created_at.min
      daily_report.as_of.strftime("%B %d, %Y #{hour}:#{min.to_s.rjust(2,'0')}")
    end
  end

  def status_class(member)
    if member.pending?
      'negative'
    elsif member.active?
      'positive'
    else
      'default'
    end
  end

  def book_full_name(book)
    if book == "JVB"
      "General Journal"
    elsif book == "CRB"
      "Cash Receipts"
    elsif book == "CDB"
      "Cash Disbursements"
    end
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, {:sort => column, :direction => direction}, {:class => css_class}
  end

  def max_age_for_child_legal_dependent
    child_max_age = Settings.max_age_for_child_legal_dependent

    if child_max_age.nil?
      child_max_age = 21
    end

    child_max_age
  end

  def min_age_for_parent_legal_dependent
    parent_min_age = Settings.min_age_for_parent_legal_dependent

    if parent_min_age.nil?
      parent_min_age = 60
    end

    parent_min_age
  end

  def max_age_for_beneficiary
    max_age = Settings.max_age_for_microinsurance_beneficiary

    if max_age.nil?
      max_age = 18
    end

    max_age
  end


  def current_month_start
    d = ApplicationHelper.current_working_date
    current_month = d.month
    current_year  = d.year
    Date.civil(current_year, current_month, 1).strftime("%Y-%m-%d")
  end

  def current_month_end
    d = ApplicationHelper.current_working_date
    current_month = d.month
    current_year  = d.year
    Date.civil(current_year, current_month, -1).strftime("%Y-%m-%d")
  end

  def self.current_working_date
    c_date = Date.today
    temp_time = Time.now

    if Settings.cut_off_time_hour and Settings.cut_off_time_minute
      current_time, cut_off_time = Time.new(2000, 1, 1, temp_time.hour, temp_time.min), Time.new(2000, 1, 1, Settings.cut_off_time_hour, Settings.cut_off_time_minute)
      if current_time <= cut_off_time
        found_date = false

        c_date = Utils::DateUtil.new(date: Date.today).previous_business_day
        while found_date != true do
          if Holiday.where(holiday_at: c_date).count > 0
            c_date = Utils::DateUtil.new(date: c_date).previous_business_day
          else
            found_date = true
          end
        end
      end
    end

    if Settings.is_real_time == true
      Time.new
    else
      Time.new(c_date.year, c_date.month, c_date.day, temp_time.hour, temp_time.min)
    end
  end

  def month_to_filipino(m)
    if m == 1
      return "enero"
    elsif m == 2
      return "pebrero"
    elsif m == 3
      return "marso"
    elsif m == 4
      return "abril"
    elsif m == 5
      return "mayo"
    elsif m == 6
      return "hunyo"
    elsif m == 7
      return "hulyo"
    elsif m == 8
      return "agosto"
    elsif m == 9
      return "setyembre"
    elsif m == 10
      return "oktubre"
    elsif m == 11
      return "nobyembre"
    elsif m == 12
      return "disyembre"
    else
      return "invalid"
    end
  end

  def blacklist_field?(field_name)
    Settings.member_blacklisted_fields.include?(field_name)
  end


  def ui_authorized?(roles:, authorized:, user:)
    (authorized and roles.include? user.role) ? true : false
  end

  def total_collections
    PaymentCollection.approved.sum(:total_amount)
  end

  def next_day_of_center_meeting(center)
    meeting_day = center.meeting_day
    n = Date.today

    if meeting_day == Date::DAYNAMES[Date.today.wday]
      n = n + 1.day
    else
      date  = Date.parse(meeting_day)
      delta = date > Date.today ? 0 : 7

      if meeting_day == "Friday"
        n = date + delta + 3.day
      else
        n = date + delta + 1.day
      end
    end

    n
  end

  # Last weekday of the month
  def last_working_day
    day = Date.today.end_of_month + 1.day
    loop do
      day = day.prev_day
      break unless day.saturday? or day.sunday?
    end

    day.strftime("%b %d, %Y")
  end

  def resource_name
    :user
  end

  def resource
    @resource || User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def tr_status(status)
    if status == "pending"
      'rejected'
    end
  end

  def is_within_closing_period?
    now = Time.now
    if (19..24).cover? now.hour or (0..6).cover? now.hour
      return true
    else
      return false
    end
  end
end
