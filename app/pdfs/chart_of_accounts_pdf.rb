class ChartOfAccountsPdf < Prawn::Document

  def initialize(major_groups, view)
    super()

    header("Chart of Accounts")

  end

  def header(title)
    bounding_box [bounds.left, bounds.top + 36], width: bounds.width do
      stroke_color 'FFFFFF'
      stroke_bounds
      stroke do
        fill_color '36DA5C'
        fill_and_stroke_rectangle [cursor-80,cursor], bounds.width + 120, 80
        fill_color '000000'
      end

      move_down 32

      font_size 16
      font "Helvetica", style: :bold
      text title, color: "e2e2e2"
    end
  end

end
