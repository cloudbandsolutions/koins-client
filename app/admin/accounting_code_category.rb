ActiveAdmin.register AccountingCodeCategory do 

  menu parent: "Accounting", priority: 4

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code
  filter :mother_accounting_code

  index do
    column :major_group do |accounting_code|
      accounting_code.mother_accounting_code.major_account.major_group
    end
    column :major_account do |accounting_code|
      accounting_code.mother_accounting_code.major_account
    end
    column :mother_accounting_code
    column :name
    column :code
    column :dc do |accounting_code|
      accounting_code.mother_accounting_code.major_account.major_group.dc_code
    end
    actions
  end

  form do |f|
    f.inputs "Accounting Code Category Details" do
      f.input :mother_accounting_code
      f.input :name, as: :string
      f.input :sub_code, as: :string
    end
    f.actions
  end

  show do
    attributes_table do
      row :major_group do |accounting_code|
        accounting_code.mother_accounting_code.major_account.major_group
      end
      row :major_account do |accounting_code|
        accounting_code.mother_accounting_code.major_account.major_group
      end
      row :mother_accounting_code
      row :name
      row :code
      row :dc do |accounting_code|
        accounting_code.mother_accounting_code.major_account.major_group.dc_code
      end
    end
  end
end

