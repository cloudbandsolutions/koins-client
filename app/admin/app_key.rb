ActiveAdmin.register AppKey do
  controller do
    def permitted_params
      params.permit!
    end
  end

  form do |f|
    f.inputs "App Key Details" do
      f.input :app_name
    end

    f.actions
  end
end
