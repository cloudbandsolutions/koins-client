ActiveAdmin.register Holiday do 
   menu parent: "Maintenance"

   csv do
    column :id
    column :name
    column :holiday_at
   end

   controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :holiday_at

  form do |f|
    f.inputs "Details" do
      f.input :name, as: :string
      f.input :holiday_at
    end

    f.actions
  end

  index do 
    column :name
    column :holiday_at

    actions
  end

end
