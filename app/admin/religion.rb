ActiveAdmin.register Religion do 
 menu parent: "Maintenance"

 csv do
  column :id
  column :name
 end

 controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name

  form do |f|
    f.inputs "Religion Details" do
      f.input :name, as: :string
    end
    f.actions
  end

  index do 
    column :name

    actions
  end

   show do |ad|
    attributes_table do 
      row :name
    end
  end
end
