ActiveAdmin.register RegularDepositInterval do 
  menu parent: "Maintenance"

  csv do
    column :id
    column :name
    column :interval
  end

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name

  index do 
    column :name
    column :interval
    column :num_entries

    actions
  end

  form do |f|
    f.inputs "Regular Deposit Interval Details" do
      f.input :name, as: :string
      f.input :interval
    end

    f.inputs "Amount Entries" do
      f.has_many :regular_deposit_amounts, allow_destroy: true do |ff|
        ff.input :min_amount
        ff.input :max_amount
        ff.input :deposit_amount
      end
    end

    f.actions
  end

  show do |f|
    attributes_table do
      row :name
      row :interval
    end

    panel "Amount Entries" do
      table_for regular_deposit_interval.regular_deposit_amounts.order(:min_amount) do
        column :min_amount do |o|
          number_to_currency(o.min_amount, unit: "P")
        end
        column :max_amount do |o|
          number_to_currency(o.max_amount, unit: "P")
        end
        column :deposit_amount do |o|
          number_to_currency(o.deposit_amount, unit: "P")
        end
      end
    end
  end
end
