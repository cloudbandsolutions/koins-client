ActiveAdmin.register ProjectType do 
  menu parent: "Maintenance"

  csv do
    column :id
    column :name
    column :code
    column :project_type_category_id
  end

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code

  form do |f|
    f.inputs "Project Type Details" do
      f.input :project_type_category, hint: "Project Type Category"
      f.input :name, as: :string
      f.input :code, as: :string
    end
    f.actions
  end

  index do 
    column :name
    column :code 
    column :project_type_category

    actions
  end
end
