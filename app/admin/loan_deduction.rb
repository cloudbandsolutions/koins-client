ActiveAdmin.register LoanDeduction do 

  menu parent: "Finance"

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code

  form do |f|
    f.inputs "Loan Deduction Details" do
      f.input :name, as: :string
      f.input :code, as: :string
      f.input :is_one_time_fee
    end
    f.actions
  end

  index do
    column :name
    column :code
    column :is_one_time_fee
    actions
  end
end
