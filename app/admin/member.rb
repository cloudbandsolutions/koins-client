ActiveAdmin.register Member do 
  controller do
    def permitted_params
      params.permit!
    end
  end
 
  filter :member_type
  filter :status, as: :select, collection: Member::STATUSES
  filter :address_city
  filter :identification_number
  filter :first_name
  filter :last_name
  filter :branch
  filter :center

  index do
    column :identification_number
    column :full_name 
    column :member_type
    column :status

    actions defaults: true do |member|
      if member.status == "pending"
        link_to "Approve", approve_admin_member_path(member), method: :put, data: { confirm: "Are you sure?" }
      end
    end
  end

  member_action :approve, method: :put do
    member = Member.find(params[:id])
    member.approve!
    redirect_to action: :index, notice: "Successfully approved member"
  end

  form do |f|
    f.inputs "Details" do
      f.input :previous_mfi_member_since
      f.input :previous_mii_member_since
      f.input :member_type, as: :select, collection: Member::MEMBER_TYPES
      f.input :status, as: :select, collection: Member::STATUSES
      f.input :first_name, as: :string
      f.input :middle_name, as: :string
      f.input :last_name, as: :string
      f.input :address_street, as: :string
      f.input :address_barangay, as: :string
      f.input :address_city, as: :string
      f.input :type_of_housing, as: :select, collection: Settings.housing_types
      f.input :num_years_housing
      f.input :num_months_housing
      f.input :proof_of_housing, as: :string
      f.input :date_of_birth
      f.input :place_of_birth, as: :string
      f.input :gender, as: :select, collection: Member::GENDERS
      f.input :civil_status, as: :select, collection: ["Single", "May Kinakasama", "Kasal", "Hiwalay", "Biyudo/a"] 
      f.input :religion
      f.input :num_children
      f.input :num_children_elementary
      f.input :num_children_high_school
      f.input :num_children_college   
      f.input :mobile_number, as: :string
      f.input :home_number, as: :string
      f.input :bank, as: :string
      f.input :bank_account_type, as: :select, collection: Settings.bank_account_types
      f.input :sss_number, as: :string
      f.input :pag_ibig_number, as: :string
      f.input :phil_health_number, as: :string
      f.input :tin_number, as: :string
      f.input :spouse_first_name, as: :string
      f.input :spouse_middle_name, as: :string
      f.input :spouse_last_name, as: :string
      f.input :spouse_date_of_birth
      f.input :spouse_occupation, as: :string
      f.input :num_years_business
      f.input :num_months_business
    end

    f.has_many :beneficiaries do |ff|
      ff.input :first_name, as: :string
      ff.input :middle_name, as: :string
      ff.input :last_name, as: :string
      ff.input :date_of_birth
      ff.input :relationship, as: :select, collection: Beneficiary::RELATIONSHIP
    end
    f.has_many :legal_dependents do |dep|
      dep.input :first_name, as: :string
      dep.input :middle_name, as: :string
      dep.input :last_name, as: :string
      dep.input :date_of_birth
      dep.input :course, as: :string
      dep.input :educational_attainment, as: :select, collection: ["elementary", "high school", "college"]
    end
    f.inputs "Images" do 
      f.input :profile_picture, as: :file
    end
    f.actions
  end

  show do |ad|
    attributes_table do 
      row :previous_mfi_member_since
      row :previous_mii_member_since
      row :member_type
      row :status
      row :first_name
      row :middle_name
      row :last_name
      row :address_street
      row :address_barangay
      row :address_city
      row :type_of_housing
      row :num_years_housing
      row :num_months_housing
      row :proof_of_housing
      row :date_of_birth
      row :place_of_birth
      row :gender
      row :civil_status
      row :mobile_number
      row :religion
      row :num_children
      row :num_children_elementary
      row :num_children_high_school
      row :num_children_college   
      row :mobile_number
      row :home_number
      row :bank
      row :bank_account_type
      row :sss_number
      row :pag_ibig_number
      row :phil_health_number
      row :tin_number
      row :spouse_first_name
      row :spouse_middle_name
      row :spouse_last_name
      row :spouse_date_of_birth
      row :spouse_occupation
      row :num_years_business
      row :num_months_business
    end

    div class: 'panel' do
      h3 'Beneficiaries'
      div class: 'attributes_table' do
        table do
          tr do
            th class: 'sa' do 
              'First Name'
            end
            th class: 'sa' do
              'Middle Name'
            end
            th class: 'sa' do
              'Last Name'
            end
            th class: 'sa' do
              'Birthday'
            end
            th class: 'sa' do
              'Relationship'
            end
          end
          member.beneficiaries.each do |p|
            tr do
              td class: 'sb' do
                p.first_name
              end
              td class: 'sb' do
                p.middle_name
              end
              td class: 'sb' do
                p.last_name
              end
              td class: 'sb' do
                p.date_of_birth
              end
              td class: 'sb' do
                p.relationship
              end
            end
          end
        end
      end
    end
    div class: 'panel' do
      h3 'legal_dependents'
      div class: 'attributes_table' do
        table do
          tr do
            th class: 'sa' do 
              'First Name'
            end
            th class: 'sa' do
              'Middle Name'
            end
            th class: 'sa' do
              'Last Name'
            end
            th class: 'sa' do
              'Birthday'
            end
          end
          member.legal_dependents.each do |p|
            tr do
              td class: 'sb' do
                p.first_name
              end
              td class: 'sb' do
                p.middle_name
              end
              td class: 'sb' do
                p.last_name
              end
              td class: 'sb' do
                p.date_of_birth
              end
            end
          end
        end
      end
    end
  end
end
