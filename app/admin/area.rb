ActiveAdmin.register Area do 
   menu parent: "Maintenance"

   csv do
    column :id
    column :name
    column :code
    column :abbreviation
    column :address
   end

   controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code

  form do |f|
    f.inputs "Details" do
      f.input :name, as: :string
      f.input :code, as: :string
      f.input :abbreviation, as: :string
      f.input :address, as: :string      
    end

    f.actions
  end

  index do 
    column :name
    column :code 
    column :abbreviation
    column :address

    actions
  end

end
