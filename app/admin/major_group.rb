ActiveAdmin.register MajorGroup do 
  menu parent: "Accounting", priority: 1
  
  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code
  filter :dc_code, as: :select

  form do |f|
    f.inputs "Major Group Details" do
      f.input :name, as: :string
      f.input :code, as: :string
      f.input :dc_code, as: :select
    end
    f.actions
  end

  index do
    column :name
    column :code
    column :dc_code
    actions
  end

  show do
    attributes_table do
      row :name
      row :code
      row :dc_code
    end
  end
end
