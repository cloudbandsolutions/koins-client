ActiveAdmin.register SavingsType do 
  menu parent: "Finance"
   controller do
    def permitted_params
      params.permit!
    end
  end

  csv do
    column :id
    column :name
    column :code
    column :is_default
    column :withdraw_accounting_code_id
    column :deposit_accounting_code_id
    column :fund_transfer_withdraw_accounting_code_id
    column :fund_transfer_deposit_accounting_code_id
    column :monthly_interest_rate
    column :monthly_tax_rate
    column :expense_accounting_code_id
    column :tax_accounting_code_id
    column :interest_accounting_code_id
    column :annual_interest_rate
  end

  filter :name
  filter :code

  index do 
    column :name
    column :code 
    column :is_default
    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :name, as: :string
      f.input :code, as: :string
      f.input :annual_interest_rate
      f.input :monthly_tax_rate, hint: "Value should be from 0 - 100 (i.e. 2 equals 2%)"
      f.input :monthly_interest_rate, hint: "Value should be from 0 - 100 (i.e. 2 equals 2%)"
      f.input :is_default
      f.input :withdraw_accounting_code, hint: "Withdraw Accounting Entry"
      f.input :deposit_accounting_code, hint: "Deposit Accounting Entry"
      f.input :expense_accounting_code, hint: "Expense Accounting Code"
      f.input :tax_accounting_code, hint: "Tax Accounting Code"
      f.input :interest_accounting_code, hint: "Interest Accounting Code"
    end
    f.actions
  end
end
