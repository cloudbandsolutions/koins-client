ActiveAdmin.register SavingsAccount do 
  menu parent: "Finance"

  controller do
    def scoped_collection
      super.includes :member
    end

    def permitted_params
      params.permit!
    end
  end

  filter :account_number
  filter :balance
  filter :status
  filter :member, collection: proc { Member.order(:last_name) }

  index do 
    column :member, sortable: "members.last_name"
    column :savings_type
    column :account_number
    column :balance do |sa|
      number_to_currency(sa.balance, unit: "P")
    end
    column :status

    actions defaults: true do |savings_account|
      if savings_account.status == "active"
        link_to "Deactivate", deactivate_admin_savings_account_path(savings_account), method: :put, data: { confirm: "Are you sure?" }
      elsif savings_account.status == "inactive"
        link_to "Activate", activate_admin_savings_account_path(savings_account), method: :put, data: { configm: "Are you sure?" }
      end
    end
  end

  member_action :activate, method: :put do
    savings_account = SavingsAccount.find(params[:id])
    savings_account.activate!
    redirect_to action: :index, notice: "Successfully activated account"
  end

  member_action :deactivate, method: :put do
    savings_account = SavingsAccount.find(params[:id])
    savings_account.deactivate!
    redirect_to action: :index, notice: "Successfully deactivated account"
  end
end
