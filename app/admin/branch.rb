ActiveAdmin.register Branch do 
  menu parent: "Maintenance"
  
  csv do
    column :id
    column :name
    column :code
    column :abbreviation
    column :address
    column :cluster_id
    column :bank_id
    column :member_counter
  end

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code
  #filter :bank, as: :select, collection: Bank.all

  index do 
    column :cluster
    column :bank
    column :name
    column :code 
    column :abbreviation
    column :address

    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :cluster
      f.input :name
      f.input :code
      f.input :abbreviation
      f.input :bank, hint: "The default bank for this branch", label: "Default Bank"
      #f.input :banks, as: :check_boxes, hint: "Other bank options"
      f.input :address
    end

    f.actions
  end

  show do
    attributes_table do
      row :cluster
      row :name
      row :code
      row :abbreviation
      row :bank, label: "Default Bank"
      # row :banks do |branch|
      #   branch.branch_banks.map { |f| f.bank }.join ', '
      # end
      row :address
    end
  end

end
