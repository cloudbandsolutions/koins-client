ActiveAdmin.register Cluster do 
  menu parent: "Maintenance"

  csv do
    column :id
    column :name
    column :code
    column :abbreviation
    column :address
    column :area_id
  end

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code

  form do |f|
    f.inputs "Details" do
      f.input :area
      f.input :name, as: :string
      f.input :code, as: :string
      f.input :abbreviation, as: :string
      f.input :address, as: :string      
    end

    f.actions
  end

  index do 
    column :area
    column :name
    column :code
    column :abbreviation
    column :num_branches, label: "# of branches"
    column :address

    actions
  end

end
