ActiveAdmin.register EquityAccount do 
  menu parent: "Finance"

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :member
  filter :account_number
  filter :equity_type
  filter :balance

  index do 
    column :member
    column :account_number
    column :balance
    column :equity_type

    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :equity_type
      f.input :member
      f.input :account_number, as: :string
    end

    f.actions
  end
end

