ActiveAdmin.register Bank do 
  menu parent: "Finance"

  csv do
    column :id
    column :name
    column :code
    column :accounting_code_id
  end

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code
  filter :accounting_code

  index do
    column :name
    column :code
    column :accounting_code
    actions
  end

  form do |f|
    f.inputs "Bank Details" do
      f.input :name, as: :string
      f.input :code, as: :string,hint: "Short name for this bank"
      f.input :accounting_code, hint: "Accounting code to be used for loan transactions"
    end
    f.actions
  end
end
