ActiveAdmin.register MembershipType do 
 menu parent: "Maintenance"

 csv do
  column :id
  column :name
  column :dr_accounting_entry
  column :cr_accounting_entry
  column :activated_for_insurance
  column :activated_for_loans
  column :fee
 end

 controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name

  index do 
    column :name
    column :fee
    column :dr_accounting_entry
    column :cr_accounting_entry
    column :activated_for_insurance
    column :activated_for_loans
    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :name, as: :string
      f.input :fee
      f.input :dr_accounting_entry, as: :select2
      f.input :cr_accounting_entry, as: :select2
      f.input :activated_for_insurance
      f.input :activated_for_loans
    end

    f.actions
  end
end
