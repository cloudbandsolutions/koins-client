ActiveAdmin.register LoanInsuranceType do 
 menu parent: "Finance"

 csv do
  column :id
  column :name
  column :accounting_code_id
  column :cash_dedit_accounting_code_id
  column :cash_credit_accounting_code_id
  column :unremitted_debit_accounting_code_id
  column :unremitted_credit_accounting_code_id
 end

 controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name

  form do |f|
    f.inputs "Loan Insurance Type Details" do
      f.input :name, as: :string
      f.input :accounting_code, hint: "Accounting Entry"
      f.input :cash_debit_accounting_code, hint: "Cash Credit Accounting Entry"
      f.input :cash_credit_accounting_code, hint: "Cash Debit Accounting Entry"
      f.input :unremitted_debit_accounting_code, hint: "Unremitted Debit Accounting Entry"
      f.input :unremitted_credit_accounting_code, hint: "Unremitted Credit Accounting Entry"
      f.input :accounting_fund
    end
    f.actions
  end

  index do 
    column :name

    actions
  end

   show do |ad|
    attributes_table do 
      row :name
    end
  end
end