ActiveAdmin.register MajorAccount do 
  menu parent: "Accounting", priority: 2

  csv do
    column :id
    column :name
    column :code
    column :major_group_id
  end
  
  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code
  filter :major_group

  form do |f|
    f.inputs "Major Account Details" do
      f.input :major_group
      f.input :name, as: :string
      f.input :code, as: :string
    end
    f.actions
  end

  index do
    column :major_group
    column :name
    column :code
    actions
  end

  show do
    attributes_table do
      row :major_group
      row :name
      row :code
    end
  end
end

