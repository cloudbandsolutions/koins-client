ActiveAdmin.register User do
  menu parent: "User Management"

  controller do
    def permitted_params
      params.permit!
    end
  end


  index do
    selectable_column
    column :email
    column :identification_number
    column :name do |u|
      u.full_name
    end
    actions
  end

  filter :email
  filter :identification_number
  filter :first_name
  filter :last_name
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :email, as: :string
      f.input :username, as: :string
      f.input :role, as: :select, collection: User::ROLES
      f.input :cluster, as: :select, collection: Cluster.all, hint: "Set this if user is part of cluster operaetions"
      f.input :password
      f.input :password_confirmation
      f.input :identification_number, as: :string
      f.input :first_name, as: :string
      f.input :last_name, as: :string
    end
    f.actions
  end

end

