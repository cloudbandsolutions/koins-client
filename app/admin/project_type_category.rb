ActiveAdmin.register ProjectTypeCategory do 
  menu parent: "Maintenance"

  csv do
    column :id
    column :name
    column :code
  end

   controller do
    def permitted_params
      params.permit!
    end
  end

    filter :name
  filter :code

  form do |f|
    f.inputs "Project Type Category Details" do
      f.input :name, as: :string
      f.input :code, as: :string
    end
    f.actions
  end

  index do 
    column :name
    column :code 
  

    actions
  end
end
