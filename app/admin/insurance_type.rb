ActiveAdmin.register InsuranceType do 
  menu parent: "Finance"
  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code

  index do 
    column :name
    column :code 
    column :default_periodic_payment
    column :is_default
    column :use_resign_rules
    column :resign_rule_num_years
    column :resign_rule_withdrawal_percentage do |o|
      if o.resign_rule_withdrawal_percentage.to_i > 0
        "#{o.resign_rule_withdrawal_percentage} %"
      end
    end
    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :name, as: :string
      f.input :code, as: :string
      f.input :default_periodic_payment
      f.input :monthly_tax_rate, hint: "Value should be from 0 - 100 (i.e. 2 equals 2%)"
      f.input :monthly_interest_rate, hint: "Value should be from 0 - 100 (i.e. 2 equals 2%)"
      f.input :is_default
      f.input :withdraw_accounting_code, hint: "Withdraw Accounting Entry", as: :select
      f.input :deposit_accounting_code, hint: "Deposit Accounting Entry", as: :select
      f.input :use_resign_rules
      f.input :resign_rule_num_years
      f.input :resign_rule_withdrawal_percentage
    end
    f.actions
  end
end

