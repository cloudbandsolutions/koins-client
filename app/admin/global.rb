ActiveAdmin.register Global do 
   menu parent: "Maintenance"
   controller do
    def permitted_params
      params.permit!
    end
  end

  filter :loan_processing_fee_account_code

  index do 
    column :created_at
    column :updated_at
    actions
  end

end

