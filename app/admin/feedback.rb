ActiveAdmin.register Feedback do
  menu parent: "Maintenance"
  actions :index, :show

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :member
  filter :content

  index do
    column :member
    column :content
    actions
  end
end
