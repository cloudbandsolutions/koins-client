ActiveAdmin.register Center do 
 menu parent: "Maintenance"

 csv do
  column :id
  column :uuid
  column :name
  column :code
  column :abbreviation
  column :address
  column :branch_id
  column :meeting_day
  column :officer_in_charge
 end

 controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code
  filter :branch

  index do 
    column :branch
    column :name
    column :code 
    column :abbreviation
    column :address
    column :meeting_day

    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :name, as: :string
      f.input :code, as: :string
      f.input :abbreviation, as: :string
      f.input :address, as: :string
      f.input :branch
      f.input :meeting_day, as: :select, collection: Center::MEETING_DAYS
    end

    f.actions
  end

end
