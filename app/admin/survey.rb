ActiveAdmin.register Survey do
  controller do
    def permitted_params
      params.permit!
    end
  end

  index do
    column :title
    column :is_default
    actions
  end

  filter :title

  form do |f|
    f.inputs "Survey Details" do
      f.input :title
      f.input :is_default, as: :boolean
    end

    f.inputs "Questions" do
      f.has_many :survey_questions, allow_destroy: true do |ff|
        ff.input :content, as: :string
        ff.input :priority

        ff.has_many :survey_question_options, allow_destroy: true, header: "Options" do |fff|
          fff.input :option, as: :string
          fff.input :score
          fff.input :priority
        end
      end
    end

    f.actions
  end

  show do |f|
    attributes_table do
      row :title
      row :is_default
      row :uuid
      row :is_default
    end

    panel "Questions" do
      table_for survey.survey_questions.order(:priority) do
        column :content
        column :options
      end
    end
  end
end
