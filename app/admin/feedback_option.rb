ActiveAdmin.register FeedbackOption do 
  menu parent: "Maintenance"

  csv do
    column :id
    column :content
  end

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :content

  form do |f|
    f.inputs "Details" do
      f.input :content, as: :string
    end
    f.actions
  end

  index do
    column :content
    actions
  end
end
