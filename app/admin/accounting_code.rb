ActiveAdmin.register AccountingCode do 

  menu parent: "Accounting", priority: 5

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code
  filter :main_code
  filter :accounting_code_category

  index do
    column :accounting_code_category
    column :name
    column :code
    column :dc do |accounting_code|
      accounting_code.try(:accounting_code_category).try(:mother_accounting_code).try(:major_account).try(:major_group).try(:dc_code)
    end
    actions
  end

  form do |f|
    f.inputs "Accounting Code Details" do
      f.input :accounting_code_category
      f.input :name, as: :string
      f.input :sub_code, as: :string
    end
    f.actions
  end

  show do
    attributes_table do
      row :major_group do |accounting_code|
        accounting_code.accounting_code_category.mother_accounting_code.major_account.major_group
      end
      row :major_account do |accounting_code|
        accounting_code.accounting_code_category.mother_accounting_code.major_account.major_group
      end
      row :mother_accounting_code do |accounting_code|
        accounting_code.accounting_code_category.mother_accounting_code
      end
      row :accounting_code_category
      row :name
      row :code
      row :dc do |accounting_code|
        accounting_code.accounting_code_category.mother_accounting_code.major_account.major_group.dc_code
      end
    end
  end
end
