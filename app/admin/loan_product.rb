ActiveAdmin.register LoanProduct do 
  menu parent: "Finance"

  controller do
    def permitted_params
      params.permit!
    end
  end

  filter :name
  filter :code

  csv do
    column :id
    column :name
    column :code
    column :insured
    column :description
    column :amount_released_accounting_code_id
    column :accounting_code_transaction_id
    column :savings_type_id
    column :default_maintaining_balance_val
    column :interest_accounting_code_id
    column :accounting_code_processing_fee_id
    column :next_application_interval
    column :priority
    column :always_have_processing_fee
    column :is_entry_point
    column :maintaining_balance_exception
    column :maintaining_balance_exception_minimum_amount
    column :waive_processing_fee_for_existing_entry_point_loans
    column :maintaining_balance_exception_cycle_count
    column :max_loan_amount
    column :writeoff_dr_accounting_code_id
    column :writeoff_cr_accounting_code_id
    column :overridable_interest_rate
    column :default_annual_interest_rate
  end

  index do
    column :name
    column :code
    column :insured
    column :max_loan_amount do |lp|
      number_to_currency(lp.max_loan_amount, unit: "P") if !lp.max_loan_amount.nil?
    end
    column :is_entry_point
    column :overridable_interest_rate
    column :default_annual_interest_rate
    column :priority

    actions
  end

  form do |f|
    f.inputs "Loan Product Details" do
      f.input :name, as: :string, hint: "Official name for this loan product"
      f.input :code, as: :string, hint: "Short name for this loan product"
      f.input :insured, as: :boolean, hint: "Allows loans for this type to be insured by MI"
      f.input :is_entry_point
      f.input :always_have_processing_fee
      f.input :overridable_interest_rate
      f.input :default_annual_interest_rate
      f.input :priority, hint: "Priority for display purposes"
      f.input :amount_released_accounting_code, label: "Accounting Entry for Amount Released"
      f.input :use_amount_released_accounting_code, hint: "Loan will use accounting entry for amount released as credit entry"
      f.input :accounting_code_transaction, label: "Accounting Entry for Receivables"
      f.input :accounting_code_processing_fee, label: "Accounting Entry for Processing Fee"
      f.input :writeoff_dr_accounting_code, label: "DR Writeoff Entry"
      f.input :writeoff_cr_accounting_code, label: "CR Writeoff Entry"
      f.input :interest_accounting_code, label: "Accounting Entry for Interest"
      f.input :savings_type, hint: "Members should have a savings account of this type to avail this loan product"
      f.input :default_maintaining_balance_val, hint: "Default maintaining balance. Percentage (i.e. 2 is 2%)"
      f.input :waive_processing_fee_for_existing_entry_point_loans
      f.input :always_have_processing_fee
      f.input :max_loan_amount
      f.input :next_application_interval, hint: "Number of amortization payments left before member can avail the same loan again"
      f.input :maintaining_balance_exception_cycle_count
      f.input :maintaining_balance_exception_minimum_amount
      f.input :maintaining_balance_exception
      f.input :description
    end

    f.inputs "Loan Product Types" do
      f.has_many :loan_product_types, allow_destroy: true do |ff|
        ff.input :name, as: :string
        ff.input :interest_rate, hint: "Value should be from 0 - 100 (i.e. 60 equals 60%)", label: "Annual Interest Rate"
        ff.input :processing_fee_15, hint: "Value should be from 0 - 100 (i.e. 2 equals 2% of total loan amount)", label: "Processing Fee (15)"
        ff.input :processing_fee_25, hint: "Value should be from 0 - 100 (i.e. 2 equals 2% of total loan amount)", label: "Processing Fee (25)"
        ff.input :processing_fee_35, hint: "Value should be from 0 - 100 (i.e. 2 equals 2% of total loan amount)", label: "Processing Fee (35)"
        ff.input :processing_fee_50, hint: "Value should be from 0 - 100 (i.e. 2 equals 2% of total loan amount)", label: "Processing Fee (50)"
        ff.input :fixed_processing_fee, hint: "Fixed value (i.e. 1500). Only applicable if user has business permit", label: "Processing Fee (fixed)"
      end
    end

    f.inputs "Entry Point Cycle Count Max Amount" do
      f.has_many :loan_product_ep_cycle_count_max_amounts, allow_destroy: true do |ff|
        ff.input :cycle_count
        ff.input :max_amount
      end
    end

    f.inputs "Loan Deduction Entries" do
      f.has_many :loan_deduction_entries, allow_destroy: true do |ff|
        ff.input :loan_deduction
        ff.input :accounting_code
        ff.input :is_percent
        ff.input :is_one_time_deduction
        ff.input :skip_for_special_loan_fund
        ff.input :use_for_special_loan_fund
        ff.input :val
        ff.input :use_term_map
        ff.input :t_val_15
        ff.input :t_val_25
        ff.input :t_val_35
        ff.input :t_val_50
      end
    end

    f.inputs "Membership Payments" do
      f.has_many :loan_product_membership_payments, allow_destroy: true do |ff|
        ff.input :membership_type
      end
    end

    f.inputs "Loan Product Dependencies" do
      f.has_many :loan_product_dependencies, foreign_key: 'dependent_loan_product_id', allow_destroy: true do |ff|
        ff.input :dependent_loan_product, collection: LoanProduct.all
        ff.input :cycle_count
      end
    end

    f.inputs "Loan Insurance Deduction Entries" do
      f.has_many :loan_insurance_deduction_entries, allow_destroy: true do |ff|
        ff.input :insurance_type
        ff.input :accounting_code
        ff.input :val
        ff.input :is_one_time_deduction
        #ff.input :is_percent
      end
    end

    f.actions
  end

  show do |lp|
    attributes_table do
      row :name
      row :code
      row :insured
      row :is_entry_point
      row :always_have_processing_fee
      row :overridable_interest_rate
      row :default_annual_interest_rate
      row :priority
      row :description
      row :amount_released_accounting_code
      row :accounting_code_transaction
      row :accounting_code_processing_fee
      row :interest_accounting_code
      row :savings_type
      row :next_application_interval
      row :membership_types do |loan_product|
        loan_product.membership_type_names.join(", ")
      end
      row :default_maintaining_balance_val do
        "#{lp.default_maintaining_balance_val.round(3)}%"
      end
      row :maintaining_balance_exception
      row :maintaining_balance_exception_minimum_amount do
        "#{number_to_currency(lp.maintaining_balance_exception_minimum_amount, unit: "P")}"
      end
    end

    div class: 'panel' do
      h3 'Loan Product Types'
      div class: 'attributes_table' do
        table do
          tr do
            th "Name"
          end
          lp.loan_product_types.each do |loan_product_type|
            tr do
              td loan_product_type.name
            end
          end
        end
      end
    end

    div class: 'panel' do
      h3 'Loan Deductions'
      div class: 'attributes_table' do
        table do
          tr do
            th "Loan Deduction"
            th "Accouting Code"
            th "Use for Special Loan Fund"
            th "Skip for Special Loan Fund"
            th "Value"
            th "Is Percent"
            th "One time?"
          end
          lp.loan_deduction_entries.each do |lde|
            tr do
              td lde.loan_deduction
              td lde.accounting_code
              td lde.use_for_special_loan_fund
              td lde.skip_for_special_loan_fund
              td do
                if lde.is_percent == true
                  number_to_currency(lde.val, unit: "%")
                else
                  number_to_currency(lde.val, unit: "P")
                end
              end
              td lde.is_percent
              td lde.is_one_time_deduction
            end
          end
        end
      end
    end
    div class: 'panel' do
      h3 'Membership Fees'
      div class: 'attributes_table' do
        table do
          tr do
            th "Membership Type"
            th "Fee"
          end
          lp.loan_product_membership_payments.each do |lpmp|
            tr do
              td lpmp.membership_type
              td number_to_currency(lpmp.membership_type.fee, unit: "P")
            end
          end
        end
      end
    end
    div class: 'panel' do
      h3 'Loan Insurance Deductions'
      div class: 'attributes_table' do
        table do
          tr do
            th "Insurance Type"
            th "Accouting Code"
            th "Value"
            th "One time?"
          end
          lp.loan_insurance_deduction_entries.each do |lde|
            tr do
              td lde.insurance_type
              td lde.accounting_code
              td do
                number_to_currency(lde.val, unit: "P")
              end
              td lde.is_one_time_deduction
            end
          end
        end
      end
    end
  end
end
