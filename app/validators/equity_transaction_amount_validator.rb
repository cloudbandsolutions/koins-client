class EquityTransactionAmountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !record.equity_account.nil? && !record.transaction_type.nil?
      # TODO: Check for maintaining balance
      if record.transaction_type == "withdraw"
        if value > 0
          value_after_withdrawal = record.equity_account.balance - value
          if value_after_withdrawal < 0
            record.errors[attribute] << (options[:message] || "not enough funds to withdraw for equity transaction")
          end
        end
      end
    end
  end
end

