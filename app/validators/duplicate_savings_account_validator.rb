class DuplicateSavingsAccountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.nil? || record.member.nil? || record.savings_type.nil?
      record.errors[attribute] << (options[:message] || "Invalid")
    elsif SavingsAccount.where(member_id: record.member.id, savings_type_id: record.savings_type.id).count > 0 && record.new_record?
      record.errors[attribute] << (options[:message] || "cannot create duplicate savings account type")
    end
  end
end
