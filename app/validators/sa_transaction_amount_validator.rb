class SaTransactionAmountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !record.savings_account.nil? && !record.transaction_type.nil?
      # TODO: Check for maintaining balance
      if record.transaction_type == "withdraw" || record.transaction_type == "wp"
        if value > 0
          value_after_withdrawal = record.savings_account.balance - value
          if value_after_withdrawal < record.savings_account.maintaining_balance
            record.errors[attribute] << (options[:message] || "not enough funds to withdraw for savings transaction. Value after withdrawal: #{value_after_withdrawal.to_f}. Maintaining Balance: #{record.savings_account.maintaining_balance.to_f}. Transaction type: #{record.transaction_type}")
          end
        end
      end
    end
  end
end
