class SavingsAccountForLoanValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)

    if value.nil?
      record.errors[attribute] << (options[:message] || "member is required")
    elsif record.loan_product.blank?
      record.errors[:loan_product] << (options[:message] || "loan_product is required")
    else
      member_sa = SavingsAccount.where(member_id: record.member.id, savings_type_id: record.loan_product.savings_type.id).first

      if member_sa.nil?
        record.errors[attribute] << (options[:message] || "member has no savings account for this loan product")
        record.errors[:loan_product] << (options[:message] || "member has no savings account for this loan product")
      end
    end
  end
end
