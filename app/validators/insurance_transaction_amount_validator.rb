class InsuranceTransactionAmountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if !record.insurance_account.nil? && !record.transaction_type.nil?
      # TODO: Check for maintaining balance
      if record.transaction_type == "withdraw"
        if value > 0
          value_after_withdrawal = record.insurance_account.balance - value
          if value_after_withdrawal < 0
            record.errors[attribute] << (options[:message] || "not enough funds to withdraw for insurance")
          end
        end
      end
    end
  end
end

