class LoanProductUniquenessValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.nil? || record.member.nil?
      record.errors[attribute] << (options[:message] || "member is required")
    else
      m = Member.find(record.member.id)
      if Loan.where(member_id: m.id, loan_product_id: value.id, status: 'active').count > 0 && record.new_record?
        record.errors[attribute] << (options[:message] || "member still has an active loan for this loan product")
      end

      if Loan.where(member_id: m.id, loan_product_id: value.id, status: 'pending').count > 0 and record.new_record?
        record.errors[attribute] << (options[:message] || "member still has a pending loan for this product")
      end
    end
  end
end
