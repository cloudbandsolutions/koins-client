class LoanProductInsuranceAccountAvailableValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.nil? || record.member.nil?
      record.errors[attribute] << (options[:message] || "member is required")
    else
      m = Member.find(record.member.id)
      if Loan.where(member_id: m.id, loan_product_id: value.id, status: 'active').count > 0 && record.new_record?
        record.errors[attribute] << (options[:message] || "member still has an active loan for this loan product")
      end
      
      insurance_type_ids = value.loan_insurance_deduction_entries.pluck(:insurance_type_id)
      member_insurance_accounts = InsuranceAccount.where("member_id = ? AND insurance_type_id IN (?)", m.id, insurance_type_ids)

      if insurance_type_ids.count != member_insurance_accounts.count
        record.errors[attribute] << (options[:message] || "member does not have insurance accounts for this product")
      end
    end
  end
end
