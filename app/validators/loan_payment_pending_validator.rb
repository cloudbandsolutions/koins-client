class LoanPaymentPendingValidator < ActiveModel::Validator
  def validate(record)
    if record.loan.loan_payments.where(status: 'pending').count > 0
      record.errors[:amount] << (options[:message] || "pending payments still present")
    end
  end
end

