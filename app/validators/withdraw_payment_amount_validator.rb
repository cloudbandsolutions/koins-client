class WithdrawPaymentAmountValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if record.amount.blank?
      record.errors[attribute] << (options[:message] || "amount is required")
    elsif value > record.amount
      record.errors[attribute] << (options[:message] || "withdraw amount cannot be greater than specified amount")
    end

    if !record.savings_account.blank?
      if (record.savings_account.balance - value) <= 0
        record.errors[attribute] << (options[:message] || "not enough funds for savings account") 
      elsif (record.savings_account.balance - value) < record.savings_account.maintaining_balance
        record.errors[attribute] << (options[:message] || "withdraw amount cannot be below savings account maintaining balance")
      end
    end
  end
end
