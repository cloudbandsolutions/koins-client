class LoanPaymentAmountGreaterThanRemainingBalanceValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value > record.loan.remaining_balance
      record.errors[attribute] << (options[:message] || "cannot process amount (#{value}) greater than loan's remaining balance for loan #{record.loan.id}")
    end
  end
end
