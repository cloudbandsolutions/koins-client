bundle exec rake sanitizers:sanitize_savings_account_transactions RAILS_ENV=$1
bundle exec rake sanitizers:sanitize_insurance_account_transactions RAILS_ENV=$1
bundle exec rake sanitizers:sanitize_equity_account_transactions RAILS_ENV=$1
bundle exec rake sanitizers:sanitize_vouchers RAILS_ENV=$1
